﻿using XCloud.Application.Service;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.GenericOrders;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderToGenericAdaptorService : ISalesAppService
{
    Task CreateOrUpdateGenericOrderAsync(string orderId);
}

public class OrderToGenericAdaptorService : SalesAppService, IOrderToGenericAdaptorService
{
    private readonly IOrderService _orderService;
    private readonly IOrderItemService _orderItemService;
    private readonly IGenericOrderService _genericOrderService;
    private readonly IOrderReviewService _orderReviewService;
    private readonly ISkuService _skuService;
    private readonly IGoodsPictureService _goodsPictureService;

    public OrderToGenericAdaptorService(IOrderService orderService,
        IGenericOrderService genericOrderService,
        IOrderReviewService orderReviewService,
        ISkuService skuService,
        IGoodsPictureService goodsPictureService, IOrderItemService orderItemService)
    {
        _orderService = orderService;
        _genericOrderService = genericOrderService;
        _orderReviewService = orderReviewService;
        _skuService = skuService;
        _goodsPictureService = goodsPictureService;
        _orderItemService = orderItemService;
    }

    private async Task<GenericOrderItemDto[]> BuildItemsAsync(OrderDto order)
    {
        await this._orderService.AttachOrderItemsAsync(new[] { order });

        await this._orderItemService.AttachOrderItemSkuAsync(order.Items);

        var skus = order.Items.Select(x => x.Sku).WhereNotNull().ToArray();

        await this._skuService.AttachGoodsAsync(skus);

        var goods = skus.Select(x => x.Goods).WhereNotNull().ToArray();

        await this._goodsPictureService.AttachImagesAsync(goods);

        return order.Items.Select(x => new GenericOrderItemDto()
        {
            GoodsName = $"{x.Sku?.Goods?.Name ?? "--"}-{x.Sku?.Name ?? "--"}",
            Pictures = x.Sku?.Goods?.GoodsPictures?
                .SortGoodsPictures(x.SkuId)
                .Select(d => d.StorageMeta)
                .ToArray(),
            Quantity = x.Quantity,
            Price = x.TotalPrice
        }).ToArray();
    }

    public async Task CreateOrUpdateGenericOrderAsync(string orderId)
    {
        var order = await this._orderService.GetRequiredByIdAsync(orderId);

        var genericOrder =
            await this._genericOrderService.GetOrCreateAsync<Order>(this.CommonUtils,
                order.UserId, order.Id);

        var dto = new GenericOrderDto()
        {
            Id = genericOrder.Id,
            Paid = false,
            Serviced = false,
            ToService = false,
            Reviewed = false,
            Finished = false,
            FinishedTime = default,
            Closed = false,
            ClosedTime = default,
            ItemsJson = default,
            ExtraDataJson = default
        };

        dto.Paid = order.IsPaid();
        dto.Serviced = order.IsShipped();
        dto.ToService = order.IsToService();

        if (order.IsFinished())
        {
            dto.Finished = true;
            dto.FinishedTime = order.CompleteTime;

            var review = await this._orderReviewService.GetReviewByOrderIdAsync(order.Id);
            dto.Reviewed = review != null;
        }

        if (order.IsClosed())
        {
            dto.Closed = true;
            dto.ClosedTime = default;
        }

        if (string.IsNullOrWhiteSpace(dto.ItemsJson))
        {
            var items = await this.BuildItemsAsync(order);
            dto.ItemsJson = this.JsonDataSerializer.SerializeToString(items);
        }

        await this._genericOrderService.UpdateAsync(dto);
    }
}