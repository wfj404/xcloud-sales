import { useRequest } from 'ahooks';
import { Button, Divider, Form, message, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StorePickupSettings } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XServiceTime from '../components/service_time';

export default () => {
  const querySettings = useRequest(apiClient.mallManager.pickupGetSettings, {
    manual: true,
    onSuccess: (e) => {
      _settings(e.data?.Data || {});
    },
  });
  const [settings, _settings] = useState<StorePickupSettings>({});
  const [loadingSaveSettings, _loadingSaveSettings] = useState(false);

  const saveSettings = () => {
    _loadingSaveSettings(true);
    apiClient.mallManager
      .pickupSaveSettings({
        ...settings,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          querySettings.run({});
        });
      })
      .finally(() => {
        _loadingSaveSettings(false);
      });
  };

  useEffect(() => {
    querySettings.run({});
  }, []);

  return (
    <>
      <div>
        <div
          style={{
            marginBottom: 20,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <h2>自提</h2>
          <Button
            type={'primary'}
            loading={loadingSaveSettings}
            onClick={() => {
              saveSettings();
            }}
          >
            保存设置
          </Button>
        </div>
        <div
          style={{
            marginBottom: 10,
          }}
        >
          <Form
            labelCol={{ span: 3 }}
            style={{
              marginBottom: 10,
            }}
          >
            <Form.Item
              label={'支持客户自提'}
              tooltip={'用户可在购买商品时选择自提'}
            >
              <Switch
                loading={loadingSaveSettings || querySettings.loading}
                checked={settings.Enabled}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    Enabled: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label={'支持货到付款'} tooltip={'需要上门收款'}>
              <Switch
                checked={settings.PayAfterShippedSupported}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    PayAfterShippedSupported: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
          <Divider type="horizontal" />
          <XServiceTime
            data={settings.ServiceTime || {}}
            ok={(x) => {
              _settings({
                ...settings,
                ServiceTime: { ...x },
              });
            }}
          />
        </div>
      </div>
    </>
  );
};
