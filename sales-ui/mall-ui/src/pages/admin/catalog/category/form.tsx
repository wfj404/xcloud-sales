import {
  Button,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Space,
  Spin,
  Switch,
  TreeSelect,
} from 'antd';
import { useEffect, useState } from 'react';

import XSinglePictureUploader from '@/components/upload/single_picture_form';
import { getFlatNodes, getTreeSelectOptionsV2 } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { AntDesignTreeNode, CategoryDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: CategoryDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [tree, _tree] = useState<AntDesignTreeNode[]>([]);
  const [expandKeys, _expandKeys] = useState<any[]>([]);

  const [model, _model] = useState<CategoryDto>({});

  const queryTree = () => {
    _loading(true);

    apiClient.mallAdmin
      .categoryQueryAntdTree({})
      .then((res) => {
        handleResponse(res, () => {
          const data = res.data.Data || [];

          _tree(data);
          const keys = data.flatMap((x) => getFlatNodes(x)).map((x) => x.key);
          _expandKeys(keys);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const save = (row: CategoryDto) => {
    row.Name = row.Name?.trim();

    if (isStringEmpty(row.Name)) {
      return;
    }

    _loading(true);

    apiClient.mallAdmin
      .categorySaveCategory({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...data,
    });
  }, [data]);

  useEffect(() => {
    if (show) {
      queryTree();
    } else {
      _model({});
    }
  }, [show]);

  return (
    <>
      <Modal
        title={'分类'}
        confirmLoading={loading}
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <Button
              type={'primary'}
              loading={loading}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label={'图片'}>
              <XSinglePictureUploader
                loadingSave={loading}
                data={model.Picture || {}}
                ok={(res) => {
                  _model({
                    ...model,
                    PictureMetaId: res.Id,
                    Picture: res,
                  });
                }}
                remove={() => {
                  if (confirm('确定删除图片吗？')) {
                    _model({
                      ...model,
                      PictureMetaId: undefined,
                      Picture: undefined,
                    });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="父级">
              <TreeSelect
                //disabled={!isStringEmpty(model.Id)}
                placeholder="无父级分类则视为顶级分类"
                treeData={tree.map((x) => getTreeSelectOptionsV2(x))}
                treeExpandedKeys={expandKeys}
                onTreeExpand={(e) => _expandKeys(e)}
                allowClear
                value={model.ParentId || undefined}
                onChange={(e) => {
                  if (!isStringEmpty(model.Id) && e == model.Id) {
                    message.error('无法指定该节点');
                    return;
                  }

                  _model({
                    ...model,
                    ParentId: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="名称" required>
              <Input
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                required
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
              {isStringEmpty(model.Name) && (
                <p style={{ color: 'red' }}>必填</p>
              )}
            </Form.Item>
            <Form.Item label="SeoName">
              <Input
                maxLength={20}
                value={model.SeoName || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    SeoName: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="描述">
              <Input.TextArea
                maxLength={50}
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="排序">
              <InputNumber
                value={model.DisplayOrder}
                onChange={(e) => {
                  _model({
                    ...model,
                    DisplayOrder: e || 0,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="推荐">
              <Switch
                checked={model.Recommend}
                onChange={(e) => {
                  _model({
                    ...model,
                    Recommend: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="可用">
              <Switch
                checked={model.Published}
                onChange={(e) => {
                  _model({
                    ...model,
                    Published: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="首页展示">
              <Switch
                checked={model.ShowOnHomePage}
                onChange={(e) => {
                  _model({
                    ...model,
                    ShowOnHomePage: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
