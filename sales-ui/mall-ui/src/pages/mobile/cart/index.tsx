import { useLocalStorageState, useRequest, useTitle } from 'ahooks';
import { sum } from 'lodash-es';
import React, { useEffect } from 'react';
import { useSnapshot } from 'umi';

import XEmpty from '@/components/common/empty';
import XLoading from '@/components/common/loading/dots';
import XStore from '@/components/mobile/current_store/store';
import { storeState, StoreType } from '@/store';
import { isSkuAndGoodsAvailable } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  formatMoney,
  handleResponse,
  isArrayEmpty,
  parseJsonOrEmpty,
} from '@/utils/utils';
import { RadioButtonUncheckedOutlined } from '@mui/icons-material';
import TaskAltOutlinedIcon from '@mui/icons-material/TaskAltOutlined';
import {
  Box,
  Checkbox,
  Container,
  FormControlLabel,
  Paper,
  Stack,
  Typography,
} from '@mui/material';

import XCheckout from './checkout';
import XItem from './item';

export default () => {
  useTitle('购物车');

  const app = useSnapshot(storeState) as StoreType;

  const queryShoppingCartsRequest = useRequest(
    apiClient.mall.shoppingCartListShoppingCarts,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {});
      },
    },
  );

  const shoppingcart_items = queryShoppingCartsRequest.data?.data?.Data || [];

  const [cachedSelected, _cachedSelected] = useLocalStorageState<number[]>(
    'shopping-cart.selected.cache',
    {
      serializer: (x) => JSON.stringify(x || []),
      deserializer: (x) => parseJsonOrEmpty(x) || [],
      defaultValue: [],
    },
  );
  const selected_ids = cachedSelected || [];

  //-----------------------------------------------------

  const active_items = shoppingcart_items.filter(
    (x) => isSkuAndGoodsAvailable(x.Sku) && isArrayEmpty(x.Waring),
  );
  const active_selected_items = active_items.filter(
    (x) => selected_ids.indexOf(x.Id || 0) >= 0,
  );
  const active_selected_ids = active_selected_items.map((x) => x.Id || 0);

  useEffect(() => {
    queryShoppingCartsRequest.run();
  }, []);

  const renderSelectAll = () => {
    if (isArrayEmpty(active_items)) {
      return null;
    }

    const selectAll = active_items.every(
      (x) => active_selected_ids.indexOf(x.Id || 0) >= 0,
    );

    return (
      <FormControlLabel
        control={
          <Checkbox
            checked={selectAll}
            onChange={(e) => {
              _cachedSelected((x) =>
                e.target.checked ? active_items.map((x) => x.Id || 0) : [],
              );
            }}
            icon={<RadioButtonUncheckedOutlined />}
            checkedIcon={<TaskAltOutlinedIcon color="success" />}
          />
        }
        label={'全选'}
      />
    );
  };

  const renderTotalBox = () => {
    if (active_selected_items.length <= 0) {
      return renderSelectAll();
    }

    const originPrice = sum(
      active_selected_items.map<number>(
        (x) => (x.Sku?.PriceModel?.BasePrice || 0) * (x.Quantity || 0),
      ),
    );

    const finalPrice = sum(
      active_selected_items.map<number>(
        (x) => (x.Sku?.PriceModel?.FinalPrice || 0) * (x.Quantity || 0),
      ),
    );

    const offset = originPrice - finalPrice;

    return (
      <Box>
        <b>{active_selected_items.length}</b>个商品，总计
        <b>{formatMoney(finalPrice)}</b>。
        {offset > 0 && (
          <span>
            优惠<b>{formatMoney(offset)}</b>
          </span>
        )}
      </Box>
    );
  };

  const renderHeader = () => {
    return (
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          py: 3,
          px: 1,
        }}
      >
        <Stack
          direction={'row'}
          alignItems="flex-end"
          justifyContent={'flex-start'}
          spacing={1}
        >
          <Typography variant="h2">购物车</Typography>
          {shoppingcart_items.length > 0 && (
            <Typography variant="h3">({shoppingcart_items.length})</Typography>
          )}
        </Stack>
        <Box sx={{}}>
          <XStore />
        </Box>
      </Box>
    );
  };

  const renderFooter = () => {
    if (isArrayEmpty(shoppingcart_items)) {
      return null;
    }

    return (
      <Box
        sx={{
          position: 'fixed',
          left: 0,
          right: 0,
          bottom: `${app.bottomHeight + 5}px`,
          zIndex: 10,
        }}
      >
        <Container disableGutters maxWidth="sm">
          <Paper
            sx={{
              px: 1,
              py: 1,
              mx: 1,
              border: (theme) => `2px solid ${theme.palette.error.main}`,
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
            elevation={2}
          >
            <Box>{renderTotalBox()}</Box>
            <XCheckout selectedItems={active_selected_items} />
          </Paper>
        </Container>
      </Box>
    );
  };

  const renderContent = () => {
    if (queryShoppingCartsRequest.loading) {
      return <XLoading />;
    }

    if (isArrayEmpty(shoppingcart_items)) {
      return <XEmpty />;
    }

    return shoppingcart_items?.map((x, index) => (
      <Box key={index}>
        <XItem
          model={x}
          checked={active_selected_ids.indexOf(x.Id || 0) >= 0}
          onUpdate={() => {
            app.queryShoppingCartCount();
            queryShoppingCartsRequest.run();
          }}
          onSelect={(id, checked) => {
            const datalist = [...active_selected_ids.filter((x) => x != id)];
            if (checked) {
              datalist.push(id);
            }
            _cachedSelected(datalist);
          }}
        />
      </Box>
    ));
  };

  return (
    <>
      <Container maxWidth="sm" disableGutters>
        <Box sx={{ px: 1, py: 2, mb: 3 }}>
          {renderHeader()}

          {renderContent()}

          {renderFooter()}
        </Box>
      </Container>
    </>
  );
};
