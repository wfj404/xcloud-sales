using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json;
using Volo.Abp.Json;
using Volo.Abp.Json.SystemTextJson;
using Volo.Abp.Modularity;
using XCloud.Core.Json;
using XCloud.Core.Json.NewtonsoftJson;

namespace XCloud.AspNetMvc.Json;

public static class JsonExtension
{
    private static void ConfigNewtonsoftJson(ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();
        var dateFormat = config.GetDateformatOrDefault();

        //newtonsoft.json
        context.Services.Configure<MvcNewtonsoftJsonOptions>(option =>
        {
            option.SerializerSettings.DateFormatString = dateFormat;
            option.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            option.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            option.SerializerSettings.MaxDepth = 10;
            //option.SerializerSettings.Converters.Add(new StringEnumConverter());
            option.UseMemberCasing();
        });

        context.Services.RemoveAll<INewtonsoftJsonOptionAccessor>();
        context.Services.AddTransient<INewtonsoftJsonOptionAccessor, MvcNewtonsoftJsonOptionAccessor>();
    }

    private static void ConfigSystemTextJson(ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();
        var dateFormat = config.GetDateformatOrDefault();

        //system.text.json
        context.Services.Configure<JsonOptions>(option =>
        {
            option.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
            option.JsonSerializerOptions.PropertyNamingPolicy = null;
            option.JsonSerializerOptions.DictionaryKeyPolicy = null;
            option.JsonSerializerOptions.DefaultIgnoreCondition =
                System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull;
            option.JsonSerializerOptions.ReferenceHandler =
                System.Text.Json.Serialization.ReferenceHandler.IgnoreCycles;
            option.JsonSerializerOptions.MaxDepth = 30;
            //option.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        });

        //setting abp json
        context.Services.Configure<AbpSystemTextJsonSerializerOptions>(option =>
        {
            option.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
            option.JsonSerializerOptions.PropertyNamingPolicy = null;
            option.JsonSerializerOptions.DictionaryKeyPolicy = null;
            option.JsonSerializerOptions.DefaultIgnoreCondition =
                System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull;
            option.JsonSerializerOptions.ReferenceHandler =
                System.Text.Json.Serialization.ReferenceHandler.IgnoreCycles;
            option.JsonSerializerOptions.MaxDepth = 30;
        });
    }

    public static void ConfigJsonProvider(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();
        var dateFormat = config.GetDateformatOrDefault();

        ConfigNewtonsoftJson(context);
        ConfigSystemTextJson(context);

        //abp json settings
        context.Services.Configure<AbpJsonOptions>(option => { option.OutputDateTimeFormat = dateFormat; });
    }
}