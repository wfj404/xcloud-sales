import {
    OrderStatus, OrderStatusList, PaymentMethod, PaymentStatus, ShippingMethod, ShippingStatus
} from '@/utils/biz';
import { OrderDto } from '@/utils/swagger';
import { Box, StepContent, Typography } from '@mui/material';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Stepper from '@mui/material/Stepper';

interface IEvent {
  name?: string;
  time?: string | null | undefined;
  desc?: string;
  completed?: boolean;
  active?: boolean;
  disabled?: boolean;
  error?: boolean;
  icon?: any;
  show?: boolean;
}

function XStepper(props: { model: OrderDto }) {
  const { model } = props;

  //支付节点
  const paymentNode: IEvent = {
    active: model.PaymentStatusId != PaymentStatus.Paid,
    completed: model.PaymentStatusId == PaymentStatus.Paid,
  };
  //发货
  const deliveryNode: IEvent = {
    active: model.ShippingStatusId == ShippingStatus.Shipping,
    completed: model.ShippingStatusId == ShippingStatus.Shipped,
  };
  //完成
  const finishedNode: IEvent = {
    completed: model.OrderStatusId === OrderStatus.Finished,
  };

  const events: IEvent[] = [
    {
      name: '下单',
      show: true,
      completed: true,
    },
    {
      name: '付款',
      show: model.PaymentMethodId == PaymentMethod.Online,
      ...paymentNode,
    },
    {
      name: '配送',
      show: model.ShippingMethodId != ShippingMethod.Pickup,
      ...deliveryNode,
    },
    {
      name: '自提',
      show: model.ShippingMethodId == ShippingMethod.Pickup,
      ...deliveryNode,
    },
    {
      name: '付款',
      show: model.PaymentMethodId == PaymentMethod.Offline,
      ...paymentNode,
    },
    {
      name: '完成',
      show: true,
      ...finishedNode,
    },
  ];

  return (
    <Box sx={{}}>
      <Stepper activeStep={1} alternativeLabel>
        {events
          .filter((x) => x.show)
          .map((x, index) => (
            <Step
              key={index}
              completed={x.completed}
              active={x.active}
              disabled={x.disabled}
            >
              <StepLabel
                optional={x.time}
                StepIconComponent={x.icon}
                error={x.error}
              >
                {x.name}
              </StepLabel>
              {x.desc && <StepContent>{x.desc}</StepContent>}
            </Step>
          ))}
      </Stepper>
    </Box>
  );
}

export default function (props: { model: OrderDto }) {
  const { model } = props;

  return (
    <Box sx={{}}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          py: 5,
        }}
      >
        <Typography variant="h2" sx={{ display: 'inline' }}>
          {OrderStatusList.find((x) => x.id == model.OrderStatusId)?.name ||
            '--'}
        </Typography>
      </Box>
      <XStepper model={model} />
    </Box>
  );
}
