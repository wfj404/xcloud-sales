import { Button, Col, Dropdown, Row, Space, Tooltip } from 'antd';
import { pullAt } from 'lodash-es';

import { OrderValidatorRule } from '@/utils/swagger';
import { GlobalContainer, parseJsonOrEmpty } from '@/utils/utils';
import { DeleteOutlined, DownOutlined } from '@ant-design/icons';

import XGoodsBrand from './conditions/goods_brand';
import XGoodsCategory from './conditions/goods_category';
import XOrderPrice from './conditions/order_price';
import XOrderStore from './conditions/store';
import { OrderValidatorType, OrderValidatorTypeOptions } from './utils';

export default ({
  conditionList,
  ok,
  disabled,
}: {
  conditionList: OrderValidatorRule[];
  ok: (d: OrderValidatorRule[]) => void;
  disabled?: boolean;
}) => {
  const renderConditionMenu = (x: OrderValidatorRule, i: number) => {
    const type: OrderValidatorType = x.ValidatorType as OrderValidatorType;

    const parameter = parseJsonOrEmpty(x.RuleJson) || {};

    const setParameter = (e: any) => {
      const rule: OrderValidatorRule = {
        ...x,
        RuleJson: JSON.stringify(e || {}),
      };
      const datalist = [...conditionList];
      datalist[i] = rule;
      ok(datalist);
    };

    if (type == 'order-price') {
      return (
        <XOrderPrice
          rule={parameter}
          ok={(e) => {
            setParameter(e);
          }}
        />
      );
    }
    if (type == 'goods-brand') {
      return (
        <XGoodsBrand
          rule={parameter}
          ok={(e) => {
            setParameter(e);
          }}
        />
      );
    }
    if (type == 'goods-category') {
      return (
        <XGoodsCategory
          rule={parameter}
          ok={(e) => {
            setParameter(e);
          }}
        />
      );
    }
    if (type == 'store') {
      return (
        <XOrderStore
          rule={parameter}
          ok={(e) => {
            setParameter(e);
          }}
        />
      );
    }
    return (
      <Button block danger type={'dashed'}>
        无效配置
      </Button>
    );
  };

  return (
    <>
      <div style={{ marginBottom: 10 }}>
        {conditionList.map((x, i) => {
          return (
            <Row key={i} gutter={10} style={{ marginBottom: 10 }}>
              <Col flex={4}>{renderConditionMenu(x, i)}</Col>
              <Col flex={1}>
                <Tooltip title={'删除当前条件'}>
                  <Button
                    block
                    danger
                    icon={<DeleteOutlined />}
                    type={'dashed'}
                    onClick={() => {
                      const list = [...conditionList];
                      pullAt(list, i);
                      ok(list);
                    }}
                  ></Button>
                </Tooltip>
              </Col>
            </Row>
          );
        })}
      </div>
      <div style={{}}>
        <Dropdown
          disabled={disabled}
          menu={{
            items: OrderValidatorTypeOptions.map((x) => ({
              label: x.label,
              key: x.key,
              icon: x.icon,
              disabled: x.disabled,
              onClick: () => {
                if (conditionList.some((d) => d.ValidatorType == x.key)) {
                  GlobalContainer.message?.error('限定条件已存在');
                  return;
                }
                const newRule: OrderValidatorRule = {
                  ValidatorType: x.key,
                };
                const list: OrderValidatorRule[] = [...conditionList, newRule];
                ok(list);
              },
            })),
          }}
        >
          <Button type="dashed" block>
            <Space>
              添加使用条件
              <DownOutlined />
            </Space>
          </Button>
        </Dropdown>
      </div>
    </>
  );
};
