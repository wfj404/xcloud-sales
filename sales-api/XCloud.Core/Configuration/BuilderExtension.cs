﻿using System.Text;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Modularity;
using XCloud.Core.Exceptions;

namespace XCloud.Core.Configuration;

public class AppEntryModuleAccessor
{
    public Type EntryType { get; }

    public AppEntryModuleAccessor(Type type)
    {
        this.EntryType = type ?? throw new ArgumentNullException(nameof(type));
    }
}

[ExposeServices(typeof(AppConfig))]
public class AppConfig : IScopedDependency
{
    private readonly IConfiguration _configuration;
    private readonly IMemoryCache _memoryCache;

    public AppConfig(IConfiguration configuration, IMemoryCache memoryCache)
    {
        _configuration = configuration;
        _memoryCache = memoryCache;
    }

    private readonly Encoding _defaultEncoding = Encoding.UTF8;

    private Encoding GetEncoding()
    {
        var key = $"{nameof(AppConfig)}.{nameof(GetEncoding)}";

        return this._memoryCache.GetOrCreate(key, (x) =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1);

            var encodingStr = _configuration["app:config:encoding"];
            if (string.IsNullOrWhiteSpace(encodingStr))
            {
                return _defaultEncoding;
            }

            return Encoding.GetEncoding(encodingStr);
        });
    }

    [NotNull] public Encoding AppEncoding => GetEncoding() ?? _defaultEncoding;
}

public static class BuilderExtension
{
    public static string GetAppName(this IServiceCollection serviceCollection)
    {
        return GetAppName(
            serviceCollection.GetConfiguration(),
            serviceCollection.GetSingletonInstanceOrNull<IAbpApplication>(),
            serviceCollection.GetSingletonInstanceOrNull<AppEntryModuleAccessor>(),
            serviceCollection.GetSingletonInstanceOrNull<IWebHostEnvironment>());
    }

    public static string GetAppName(this IServiceProvider serviceProvider)
    {
        return GetAppName(
            serviceProvider.GetRequiredService<IConfiguration>(),
            serviceProvider.GetRequiredService<IAbpApplication>(),
            serviceProvider.GetService<AppEntryModuleAccessor>(),
            serviceProvider.GetService<IWebHostEnvironment>());
    }

    private static string GetAppName(
        IConfiguration config,
        IAbpApplication application = null,
        AppEntryModuleAccessor entryModuleAccessor = null,
        IWebHostEnvironment webHostEnvironmentOrNull = default)
    {
        var name = config["app:name"];

        if (!string.IsNullOrWhiteSpace(name))
            return name;

        name = application?.StartupModuleType?.Assembly?.GetName()
            ?.Name;

        if (!string.IsNullOrWhiteSpace(name))
            return name;

        name = entryModuleAccessor?.EntryType?.Assembly?.GetName()?.Name;

        if (!string.IsNullOrWhiteSpace(name))
            return name;

        name = webHostEnvironmentOrNull?.ApplicationName;

        if (!string.IsNullOrWhiteSpace(name))
            return name;

        throw new ConfigException("fail to get app name");
    }

    /// <summary>
    /// 添加builder，重复添加只会返回第一个
    /// </summary>
    public static void AddXCloudBuilder<EntryModule>(this IServiceCollection services) where EntryModule : AbpModule
    {
        var builder = GetXCloudBuilderOrDefault(services);

        if (builder == null)
        {
            builder = new XCloudBuilder<EntryModule>(services);

            services.AddSingleton<IXCloudBuilder>(builder);

            var entryModuleAccessor = new AppEntryModuleAccessor(typeof(EntryModule));
            services.AddObjectAccessor(entryModuleAccessor);
            services.AddSingleton(entryModuleAccessor);

            foreach (var ass in builder.AllModuleAssemblies)
            {
                //builder.Services.AutoRegister(new Assembly[] { ass });
            }
        }
    }

    public static IXCloudBuilder GetXCloudBuilderOrDefault(this IServiceCollection services)
    {
        var res = services.GetSingletonInstanceOrNull<IXCloudBuilder>();
        return res;
    }

    public static IXCloudBuilder GetRequiredXCloudBuilder(this IServiceCollection services)
    {
        var res = GetXCloudBuilderOrDefault(services);

        if (res == null)
            throw new AbpException("请先注册x-cloud builder");

        return res;
    }

    private static string GetObjectPropertyKey<T>()
    {
        var t = typeof(T);

        var res = $"object-{t.Namespace}-{t.Name}-from-{t.Assembly.FullName}";

        res = res.Replace(' '.ToString(), string.Empty).Trim();

        return res;
    }

    /// <summary>
    /// 保存到builder
    /// </summary>
    public static void SetObject<T>(this IXCloudBuilder builder, T obj)
    {
        var key = GetObjectPropertyKey<T>();

        builder.ExtraProperties[key] = obj;
    }

    /// <summary>
    /// 从builder中获取对象
    /// </summary>
    public static T GetObject<T>(this IXCloudBuilder builder)
    {
        var key = GetObjectPropertyKey<T>();

        var obj = builder.ExtraProperties[key];

        /*
         if (obj != null && obj.GetType().IsAssignableToType<T>())
        {
            var data = (T)obj;
            return data;
        }
         */

        if (obj != null && obj is T res)
        {
            return res;
        }

        return default;
    }
}