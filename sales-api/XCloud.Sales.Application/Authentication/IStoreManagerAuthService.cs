using JetBrains.Annotations;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context.Contributors;

namespace XCloud.Sales.Application.Authentication;

public interface IStoreManagerAuthService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<StoreManagerAuthResult> GetStoreManagerAsync([CanBeNull] string storeId = null);
}

public class StoreManagerAuthService : SalesAppService, IStoreManagerAuthService
{
    private readonly IStoreManagerAccountService _storeManagerService;
    private readonly IUserAuthService _userAuthService;
    private readonly IRequestCacheProvider _cacheProvider;
    private readonly UserSelectedStoreSelectorContributor _userSelectedStoreSelectorContributor;

    public StoreManagerAuthService(
        IStoreManagerAccountService storeManagerService,
        IRequestCacheProvider cacheProvider,
        IUserAuthService userAuthService,
        UserSelectedStoreSelectorContributor userSelectedStoreSelectorContributor)
    {
        _storeManagerService = storeManagerService;
        _cacheProvider = cacheProvider;
        _userAuthService = userAuthService;
        _userSelectedStoreSelectorContributor = userSelectedStoreSelectorContributor;
    }

    private async Task<StoreManagerAuthResult> GetStoreManagerImplAsync(string storeId)
    {
        var res = new StoreManagerAuthResult();

        if (string.IsNullOrWhiteSpace(storeId))
        {
            storeId = await this._userSelectedStoreSelectorContributor.GetStoreIdOrEmptyAsync();
        }

        if (string.IsNullOrWhiteSpace(storeId))
        {
            res.SetError("please select store");
            return res;
        }

        var userAuthResult = await this._userAuthService.GetAuthUserAsync();

        if (userAuthResult.HasError())
        {
            res.SetPropertyAsJson(userAuthResult, this.JsonDataSerializer);
            return res.SetError("user auth failed");
        }

        var user = userAuthResult.GetRequiredUser();

        var storeManager = await _storeManagerService.GetStoreManagerAuthValidationDataAsync(
            storeId,
            user.Id,
            new CacheStrategy { Cache = true });

        if (storeManager == null)
        {
            res.SetError(nameof(res.StoreManager));
            return res;
        }

        storeManager.User = user;
        res.SetData(storeManager);

        return res;
    }

    public async Task<StoreManagerAuthResult> GetStoreManagerAsync(string storeId)
    {
        return await this._cacheProvider.GetOrSetAsync(async () => await this.GetStoreManagerImplAsync(storeId));
    }
}