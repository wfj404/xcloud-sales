﻿using Masuit.Tools;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Orders.PlaceOrder;

namespace XCloud.Sales.Application.Service.Coupons;

public interface IOrderCouponService : ISalesAppService
{
    Task TryMarkCouponsAsUsedAsync(PlaceOrderDataHolder holder);

    Task TryUseCouponAsync(PlaceOrderDataHolder holder);
}

public class OrderCouponService : SalesAppService, IOrderCouponService
{
    private readonly CouponUtils _couponUtils;
    private readonly IUserCouponService _userCouponService;
    private readonly ICouponService _couponService;

    public OrderCouponService(CouponUtils couponUtils,
        IUserCouponService userCouponService,
        ICouponService couponService)
    {
        _couponUtils = couponUtils;
        _userCouponService = userCouponService;
        _couponService = couponService;
    }

    public async Task TryMarkCouponsAsUsedAsync(PlaceOrderDataHolder holder)
    {
        if (ValidateHelper.IsEmptyCollection(holder.Order.OrderCouponRecords))
            return;

        foreach (var m in holder.Order.OrderCouponRecords)
        {
            await this._userCouponService.MarkUserCouponAsUsedAsync(m.UserCouponId);
        }
    }

    public async Task TryUseCouponAsync(PlaceOrderDataHolder holder)
    {
        try
        {
            holder.Order.CouponPrice = default;

            if (holder.Request.UserCouponId <= 0)
            {
                holder.Order.OrderCouponRecords ??= [];
                return;
            }

            var couponList = new List<OrderCouponRecordDto>();
            var couponPrice = default(decimal);

            foreach (var userCouponId in new[] { holder.Request.UserCouponId })
            {
                var couponUserMapping = await _userCouponService.GetByIdAsync(userCouponId);
                if (couponUserMapping == null)
                {
                    holder.CouponErrors.Add("user coupon not exist");
                    continue;
                }

                if (!couponUserMapping.IsValid(holder.Now, out var error))
                {
                    holder.CouponErrors.Add(error);
                    continue;
                }

                var coupon = await _couponService.GetByIdAsync(couponUserMapping.CouponId);
                if (coupon == null)
                {
                    holder.CouponErrors.Add("coupon not exist");
                    continue;
                }

                coupon.ValidationErrors =
                    await this._couponUtils.GetValidationErrorsAsync(coupon, holder.Order);

                if (coupon.ValidationErrors.Any())
                {
                    holder.CouponErrors.AddRange(coupon.ValidationErrors);
                    continue;
                }

                couponList.Add(new OrderCouponRecordDto
                {
                    UserCouponId = couponUserMapping.Id,
                    CouponUserMapping = couponUserMapping,
                    Coupon = coupon,
                });

                if (coupon.CouponType == CouponTypes.FreeShippingFee)
                {
                    holder.Order.ShippingFee = decimal.Zero;
                }
                else
                {
                    couponPrice += coupon.Price;
                }
            }

            holder.Order.OrderCouponRecords = couponList.ToArray();
            holder.Order.CouponPrice = couponPrice;
        }
        catch (Exception e)
        {
            this.Logger.LogError(message: e.Message, exception: e);
            holder.CouponErrors.Add("failed to use coupon");
        }
    }
}