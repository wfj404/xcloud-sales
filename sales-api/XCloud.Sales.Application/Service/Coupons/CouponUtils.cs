using Volo.Abp.Timing;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Domain.Coupons;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Orders.Validator;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Coupons;

[ExposeServices(typeof(CouponUtils))]
public class CouponUtils : SalesAppService
{
    private readonly IClock _clock;
    private readonly OrderValidatorUtils _orderValidatorUtils;

    public CouponUtils(IClock clock, OrderValidatorUtils orderValidatorUtils)
    {
        _clock = clock;
        _orderValidatorUtils = orderValidatorUtils;
    }

    public async Task<string[]> GetValidationErrorsAsync(Coupon coupon, OrderDto order)
    {
        var rules = JsonDataSerializer.DeserializeFromString<OrderValidatorRule[]>(
            coupon.OrderConditionRulesJson);

        var errorList = new List<string>();

        if (ValidateHelper.IsNotEmptyCollection(rules))
        {
            var validationTask =
                _orderValidatorUtils.ValidateOrderConditionsAsync(
                    new OrderConditionCheckDto(order), rules);

            await foreach (var response in validationTask)
            {
                if (response.HasError())
                {
                    errorList.Add(response.Error.Message);
                }
            }
        }

        return errorList.ToArray();
    }
}