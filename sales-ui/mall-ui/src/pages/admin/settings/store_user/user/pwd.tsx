import { useRequest } from 'ahooks';
import { Button, Input, message, Modal } from 'antd';
import { useState } from 'react';

import { apiClient } from '@/utils/client';
import { UserDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({ model, ok }: { model: UserDto; ok: () => void }) => {
  const [open, _open] = useState(false);
  const [pwd, _pwd] = useState<string>('');

  const updatePwdRequest = useRequest(apiClient.sys.manageUserSetPassword, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        message.success('修改成功');
        ok();
      });
    },
  });

  const updatePwd = () => {
    if (!pwd || isStringEmpty(pwd)) {
      message.info('放弃修改密码');
      return;
    }
    if (pwd.length < 5) {
      message.error('密码长度不能小于5位');
      return;
    }

    updatePwdRequest.run({
      Id: model.Id,
      Password: pwd,
    });
  };

  return (
    <>
      <Modal
        open={open}
        onCancel={() => _open(false)}
        confirmLoading={updatePwdRequest.loading}
        onOk={() => {
          updatePwd();
        }}
        title="修改用户密码"
      >
        <Input.Password
          width={'100%'}
          size="large"
          maxLength={20}
          placeholder="输入密码"
          value={pwd}
          onChange={(e) => {
            _pwd(e.target.value);
          }}
          status={isStringEmpty(pwd) ? 'error' : undefined}
        />
      </Modal>
      <Button
        type="link"
        onClick={() => {
          _open(true);
        }}
      >
        修改密码
      </Button>
    </>
  );
};
