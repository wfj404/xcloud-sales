﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Domain.Grades;
using XCloud.Sales.Application.Domain.Users;

namespace XCloud.Sales.Application.Service.Users;

public class QueryBalancePagingInput : PagedRequest
{
    public int UserId { get; set; }
    public int? ActionType { get; set; }
}

public class QueryPointsPagingInput : PagedRequest
{
    public int UserId { get; set; }
    public int? ActionType { get; set; }
    public string OrderId { get; set; }
}

public class PointsHistoryDto : PointsHistory, IEntityDto
{
    //
}

public class BalanceHistoryDto : BalanceHistory, IEntityDto
{
    public BalanceHistoryDto()
    {
        //
    }

    public BalanceHistoryDto(int userId, decimal amount, BalanceActionType actionType, string comment) : this()
    {
        UserId = userId;
        Balance = amount;
        ActionType = (int)actionType;
        Message = comment ?? string.Empty;
    }
}

public class StoreUserDto : StoreUser, IEntityDto<int>
{
    public Grade Grade { get; set; }

    public UserDto User { get; set; }
}

public class UpdateUserStatusInput : IEntityDto<int>
{
    public int Id { get; set; }
    public bool? IsActive { get; set; }
}

public class QueryUserPagingInput : PagedRequest
{
    public string[] GlobalUserIds { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public bool? Active { get; set; }
    public string GradeId { get; set; }
    public bool OnlyWithShoppingCart { get; set; }
}