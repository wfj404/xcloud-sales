﻿using System.Threading;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Database;

[ExposeServices(typeof(SalesDatabaseHealthCheck), typeof(IHealthCheck))]
public class SalesDatabaseHealthCheck : IHealthCheck, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public SalesDatabaseHealthCheck(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
        CancellationToken cancellationToken = default)
    {
        using var s = _serviceProvider.CreateScope();

        var logger = s.ServiceProvider.GetRequiredService<ILogger<SalesDatabaseHealthCheck>>();

        using var uow = s.ServiceProvider.GetRequiredService<IUnitOfWorkManager>()
            .Begin(requiresNew: true, isTransactional: true);

        try
        {
            var repository = s.ServiceProvider.GetRequiredService<ISalesRepository<StoreUser>>();

            var count = await repository.CountAsync(cancellationToken);

            logger.LogInformation(message: $"{nameof(StoreUser)}.count={count}");

            await uow.CompleteAsync(cancellationToken);

            return HealthCheckResult.Healthy();
        }
        catch (Exception e)
        {
            logger.LogError(message: e.Message, exception: e);

            await uow.RollbackAsync(cancellationToken);

            return HealthCheckResult.Unhealthy(e.Message);
        }
    }
}