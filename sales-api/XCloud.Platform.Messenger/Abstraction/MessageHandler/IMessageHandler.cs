﻿using System;
using System.Threading.Tasks;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction.MessageHandler;

public abstract class MessageContextBase
{
    public IMessengerServer MessengerServer { get; set; }
    public IServiceProvider ScopedServiceProvider { get; set; }
    public MessageDto Message { get; set; }
}

public class ClientMessageContext : MessageContextBase
{
    public ClientMessageContext(IMessengerServer messengerServer)
    {
        MessengerServer = messengerServer;
    }

    public IConnection Connection { get; set; }
}

public class TransportMessageContext : MessageContextBase
{
    public TransportMessageContext(IMessengerServer messengerServer)
    {
        MessengerServer = messengerServer;
    }
}

public interface IMessageHandler
{
    string MessageType { get; }
    
    int Sort { get; }

    Task HandleMessageFromClientAsync(ClientMessageContext context);
    Task HandleMessageFromTransportAsync(TransportMessageContext context);
}