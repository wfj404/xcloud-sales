import { SkuDto } from '@/utils/swagger';
import { Box, Typography } from '@mui/material';
import { take } from 'lodash-es';
import XSkuPriceRow from './sku_price_row';

export default (props: { skus: SkuDto[]; count?: number }) => {
  const { skus, count } = props;

  const takeCount = count || 1;
  const skusCount: number = skus.length;

  return (
    <>
      <Box sx={{}}>
        {skusCount <= 0 && (
          <Typography variant='caption' color='text.secondary'>
            暂无可售
          </Typography>
        )}
        {take(skus, takeCount).map((x, index) => (
          <Box sx={{}} key={index}>
            <XSkuPriceRow model={x} />
          </Box>
        ))}
        {skusCount > takeCount && (
          <Typography
            variant='caption'
            color='text.disabled'
          >{`一共${skusCount}种款式`}</Typography>
        )}
      </Box>
    </>
  );
};
