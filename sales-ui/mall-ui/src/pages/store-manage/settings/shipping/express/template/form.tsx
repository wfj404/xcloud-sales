import { Form, Input, InputNumber, message, Modal } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { FreightTemplateDto } from '@/utils/swagger';
import { handleResponse, parseJsonOrEmpty } from '@/utils/utils';

import XAreaSelectorMultiple from '../../components/area_selector_multiple';
import XDeliveryRuleEditor from './delivery_rules_editor';

export default ({
  show,
  data,
  hide,
  ok,
}: {
  show: boolean;
  data: FreightTemplateDto;
  hide: () => void;
  ok: () => void;
}) => {
  const [model, _model] = useState<FreightTemplateDto>({});
  const [loading, _loading] = useState(false);

  const save = (row: FreightTemplateDto) => {
    _loading(true);
    apiClient.mallManager
      .freightTemplateSave({
        ...row,
        RulesJson: JSON.stringify(row.Rules || []),
        ExcludedAreaJson: JSON.stringify(row.ExcludedAreas || []),
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    if (data) {
      _model({
        ...data,
        Rules: parseJsonOrEmpty(data.RulesJson) || [],
        ExcludedAreas: parseJsonOrEmpty(data.ExcludedAreaJson) || [],
      });
    } else {
      _model({});
    }
  }, [data]);

  useEffect(() => {
    console.log(model);
  }, [model]);

  return (
    <>
      <Modal
        forceRender
        confirmLoading={loading}
        width={'95%'}
        open={show}
        title={'运费模板'}
        onOk={() => {
          save(model);
        }}
        onCancel={() => {
          hide();
        }}
      >
        <Form labelCol={{ span: 3 }}>
          <Form.Item label={'名称'}>
            <Input
              value={model.Name || ''}
              onChange={(e) => {
                _model({
                  ...model,
                  Name: e.target.value,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'描述'}>
            <Input.TextArea
              value={model.Description || ''}
              onChange={(e) => {
                _model({
                  ...model,
                  Description: e.target.value,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'包邮价格'}>
            <InputNumber
              value={model.FreeShippingMinOrderTotalPrice}
              onChange={(e) => {
                _model({
                  ...model,
                  FreeShippingMinOrderTotalPrice: e || 0,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'地区设置'}>
            <XDeliveryRuleEditor
              delivery_rules={model.Rules || []}
              ok={(e) => {
                _model({
                  ...model,
                  Rules: e,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'不配送地区'}>
            <XAreaSelectorMultiple
              area_list={model.ExcludedAreas || []}
              ok={(e) => {
                _model({
                  ...model,
                  ExcludedAreas: e,
                });
              }}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
