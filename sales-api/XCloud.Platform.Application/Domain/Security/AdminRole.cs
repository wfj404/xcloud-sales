﻿namespace XCloud.Platform.Application.Domain.Security;

/// <summary>
/// 用户角色关联
/// </summary>
public class AdminRole : PlatformBaseEntity, IHasCreationTime
{
    public string RoleId { get; set; }

    public string AdminId { get; set; }

    public DateTime CreationTime { get; set; }
}