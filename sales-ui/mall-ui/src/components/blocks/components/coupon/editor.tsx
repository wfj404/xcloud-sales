import { Form, Input, InputNumber, Switch } from 'antd';

import { CouponBlockProps } from './utils';

export default function ({
  data,
  onChange,
}: {
  data: CouponBlockProps;
  onChange: (d: CouponBlockProps) => void;
}) {
  if (!data || !onChange) {
    return null;
  }

  return (
    <>
      <Form labelCol={{ span: 3 }}>
        <Form.Item label={'标题'}>
          <Input
            value={data?.title}
            onChange={(e) => {
              onChange && onChange({ ...data, title: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'描述'}>
          <Input
            value={data?.description}
            onChange={(e) => {
              onChange && onChange({ ...data, description: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'最多显示数量'}>
          <InputNumber
            value={data?.count}
            onChange={(e) => {
              onChange && onChange({ ...data, count: e || undefined });
            }}
          />
        </Form.Item>
        <Form.Item label={'hide if empty'}>
          <Switch
            checked={data?.hide_if_empty}
            onChange={(e) => {
              onChange && onChange({ ...data, hide_if_empty: e });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
}
