using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Orders.Validator.Validators;

public class GoodsCategoriesValidatorParameter : IRuleParameter
{
    public CategoryDto[] SupportedCategories { get; set; }
}

[ExposeServices(typeof(IOrderValidator))]
public class GoodsCategoriesValidator : OrderValidatorBase<GoodsCategoriesValidatorParameter>, IOrderValidator,
    ITransientDependency
{
    public int Order => default;

    public string ValidatorType => OrderConditionValidatorTypes.GoodsCategories;

    public Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule)
    {
        return Task.FromResult(ValidatorType == rule.ValidatorType);
    }

    public async Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule)
    {
        await Task.CompletedTask;

        if (!string.IsNullOrWhiteSpace(rule.Description))
            return rule.Description;

        var param = ParseRuleParameterOrDefault(rule.RuleJson);

        if (param != null && ValidateHelper.IsNotEmptyCollection(param.SupportedCategories))
        {
            var names = param.SupportedCategories.Select(x => x.Name).Distinct().ToArray();
            return $"assign to {string.Join(',', names)}";
        }

        return string.Empty;
    }

    public async Task<OrderConditionValidationResponse> IsConditionMatchedAsync(OrderConditionCheckDto input,
        OrderValidatorRule rule)
    {
        await Task.CompletedTask;

        var param = ParseRequiredRuleParameter(rule.RuleJson);

        if (param == null || ValidateHelper.IsEmptyCollection(param.SupportedCategories))
            return new OrderConditionValidationResponse();

        var categoriesInOrder = input.Order?.Items?.Select(x => x.Sku?.Goods?.CategoryId).WhereNotEmpty().Distinct()
            .ToArray();

        if (categoriesInOrder == null || ValidateHelper.IsEmptyCollection(categoriesInOrder))
            return new OrderConditionValidationResponse().SetError("brands in order is not matched");

        var supportedCategoriesIds = param.SupportedCategories.Ids();

        if (!categoriesInOrder.All(x => supportedCategoriesIds.Contains(x)))
        {
            return new OrderConditionValidationResponse().SetError("goods brand not supported");
        }

        return new OrderConditionValidationResponse();
    }
}