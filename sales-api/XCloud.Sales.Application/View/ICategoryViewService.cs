using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(CategoryViewService))]
public class CategoryViewService : SalesAppService
{
    private readonly ICategoryService _categoryService;
    private readonly ISalesRepository<Category> _repository;

    public CategoryViewService(ICategoryService categoryService,
        ISalesRepository<Category> repository)
    {
        _categoryService = categoryService;
        _repository = repository;
    }

    private async Task<CategoryDto[]> ByParentIdAsync(string storeId, string parentId)
    {
        var db = await this._repository.GetDbContextAsync();

        var categoryIdsQuery = this.BuildStoreCategoryIdsQuery(db, storeId);

        var query = db.Set<Category>().AsNoTracking().Where(x => categoryIdsQuery.Contains(x.Id));

        query = query.Where(x => x.Published);

        if (string.IsNullOrWhiteSpace(parentId))
        {
            query = query.Where(x => x.ParentId == null || x.ParentId == string.Empty);
        }
        else
        {
            query = query.Where(x => x.ParentId == parentId);
        }

        var data = await query
            .OrderByDescending(x => x.DisplayOrder)
            .ThenBy(x => x.CreationTime)
            .ToArrayAsync();

        var datalist = data.MapArrayTo<Category, CategoryDto>(ObjectMapper);

        await _categoryService.AttachPictureMetaAsync(datalist);

        return datalist;
    }

    public async Task<CategoryDto[]> ByParentIdAsync(string storeId, string parentId, CacheStrategy cacheStrategy)
    {
        var key = $"store.{storeId}.category.view.by.parent.${parentId}";
        var option = new CacheOption<CategoryDto[]>(key, TimeSpan.FromMinutes(3));

        var children =
            await CacheProvider.ExecuteWithPolicyAsync(() => ByParentIdAsync(storeId, parentId), option, cacheStrategy);

        children ??= Array.Empty<CategoryDto>();

        return children;
    }

    public async Task<CategoryDto> ByIdAsync(string id, CacheStrategy cacheStrategy)
    {
        var key = $"category.view.by.id=${id}";
        var option = new CacheOption<CategoryDto>(key, TimeSpan.FromMinutes(5));

        var data =
            await CacheProvider.ExecuteWithPolicyAsync(() => this._categoryService.GetByIdAsync(id), option,
                cacheStrategy);

        if (!data.Published)
            return null;

        return data;
    }

    private IQueryable<string> BuildStoreCategoryIdsQuery(DbContext db, string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var query = from mapping in db.Set<StoreGoodsMapping>().AsNoTracking()
            join sku in db.Set<Sku>().AsNoTracking()
                on mapping.SkuId equals sku.Id
            join goods in db.Set<Goods>().AsNoTracking()
                on sku.GoodsId equals goods.Id
            select new { goods, sku, mapping };

        query = query.Where(x => x.mapping.StoreId == storeId);
        query = query.Where(x => x.goods.Published && x.sku.IsActive && x.mapping.Active);
        query = query.Where(x => x.goods.CategoryId != null && x.goods.CategoryId != string.Empty);

        var categoryIdsQuery = query.Select(x => x.goods.CategoryId).Distinct();

        return categoryIdsQuery;
    }

    private async Task<CategoryDto[]> GetStoreAvailableRootCategoryAsync(string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var db = await this._repository.GetDbContextAsync();

        var categoryIdsQuery = BuildStoreCategoryIdsQuery(db, storeId);

        var datalist = await db.Set<Category>().AsNoTracking()
            .Where(x => x.ParentId == null || x.ParentId == string.Empty)
            .Where(x => x.Published)
            .Where(x => categoryIdsQuery.Contains(x.Id))
            .OrderByDescending(x => x.CreationTime)
            .TakeUpTo5000()
            .ToArrayAsync();

        datalist = datalist.OrderByDescending(x => x.DisplayOrder).ToArray();

        return datalist.MapArrayTo<Category, CategoryDto>(this.ObjectMapper);
    }

    public async Task<CategoryDto[]> GetStoreRootCategoriesAsync(string storeId, CacheStrategy cacheStrategy)
    {
        var key = $"category.root.data.store={storeId}";
        var option = new CacheOption<CategoryDto[]>(key, TimeSpan.FromMinutes(10));

        var response = await this.CacheProvider.ExecuteWithPolicyAsync(
            () => this.GetStoreAvailableRootCategoryAsync(storeId), option, cacheStrategy);

        response ??= [];

        return response;
    }
}