﻿using System;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Core.Configuration;
using XCloud.Core.Extension;
using XCloud.Platform.Messenger.Abstraction;

namespace XCloud.Platform.Messenger.Configuration;

public static class ConfigurationExtension
{
    public static MessengerBuilder AddMessenger(this IXCloudBuilder builder)
    {
        return new MessengerBuilder(builder.Services);
    }

    public static MessengerBuilder AddDefaultServerProvider(this MessengerBuilder builder)
    {
        var guid = Guid.NewGuid().ToString();
        var hostName = System.Net.Dns.GetHostName();
        var instanceId = $"messenger.server.{guid}@{hostName}".RemoveWhitespace();

        builder.Services.AddSingleton<IMessengerServer>(provider => new MessengerServer(provider, instanceId));
        return builder;
    }
}