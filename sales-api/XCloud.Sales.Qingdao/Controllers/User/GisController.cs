﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Service;
using XCloud.Sales.Qingdao.Service.Stores;

namespace XCloud.Sales.Qingdao.Controllers.User;

[Route("api/mall/qingdao/gis")]
public class GisController : ShopBaseController
{
    private readonly QingdaoLocationBasedStoreSelectorContributor _qingdaoLocationBasedStoreSelectorContributor;
    private readonly IStoreService _storeService;
    private readonly GisClient _gisClient;
    private readonly Sh3HClient _sh3HClient;

    public GisController(
        QingdaoLocationBasedStoreSelectorContributor qingdaoLocationBasedStoreSelectorContributor,
        IStoreService storeService,
        GisClient gisClient, Sh3HClient sh3HClient)
    {
        _qingdaoLocationBasedStoreSelectorContributor = qingdaoLocationBasedStoreSelectorContributor;
        _storeService = storeService;
        _gisClient = gisClient;
        _sh3HClient = sh3HClient;
    }

    [NotNull]
    [ItemCanBeNull]
    [NonAction]
    private async Task<WapOrganization> ResolveWapOrgAsync([NotNull] GetOrgByLocationInput dto)
    {
        var point = await this._gisClient.TransformCoordinatesToPipelineTypeAsync(dto.Location);

        if (point != null)
        {
            var authId = await this._gisClient.GetAuthCodeAsync();

            if (!string.IsNullOrWhiteSpace(authId))
            {
                var area = await this._gisClient.GetGisAreaAsync(authId, dto.BizType ?? GisBizType.GONGSHUI, point);

                if (area != null && !string.IsNullOrWhiteSpace(area.ECode))
                {
                    var org = await this._sh3HClient.GetOrganizationByCodeAsync(area.ECode);

                    if (org != null)
                    {
                        return org;
                    }
                }
            }
        }

        return null;
    }

    [NotNull]
    [ItemNotNull]
    [NonAction]
    private async Task<WapOrganization> HandleMappingAsync(
        [NotNull] GetOrgByLocationInput dto,
        [NotNull] WapOrganization organization)
    {
        var config = this.Configuration.GetRequired3HConfig();

        if (dto.BizType == GisBizType.PAISHUI && config.PsRedirection.Any())
        {
            var orgPath =
                await this._sh3HClient.ResolveParentOrganizationWithCacheAsync(organization.organizationId);
            foreach (var m in orgPath.Reverse())
            {
                var redirection = config.PsRedirection.FirstOrDefault(x => x.From == m.organizationCode);
                if (redirection != null && !string.IsNullOrWhiteSpace(redirection.To))
                {
                    var redirectionTarget = await this._sh3HClient.GetOrganizationByCodeAsync(redirection.To);
                    if (redirectionTarget != null)
                    {
                        return redirectionTarget;
                    }
                }
            }
        }

        return organization;
    }

    [NotNull]
    [ItemCanBeNull]
    [NonAction]
    private async Task<WapOrganization> HandleFallbackAsync([NotNull] GetOrgByLocationInput dto)
    {
        var config = this.Configuration.GetRequired3HConfig();

        if (dto.BizType == GisBizType.PAISHUI && !string.IsNullOrWhiteSpace(config.PsDefaultStation))
        {
            var defaultOrg = await this._sh3HClient.GetOrganizationByCodeAsync(config.PsDefaultStation);
            return defaultOrg;
        }
        else if (dto.BizType == GisBizType.GONGSHUI && !string.IsNullOrWhiteSpace(config.GsDefaultStation))
        {
            var defaultOrg = await this._sh3HClient.GetOrganizationByCodeAsync(config.GsDefaultStation);
            return defaultOrg;
        }

        return null;
    }

    [HttpPost("get-org-by-location")]
    public async Task<ApiResponse<WapOrganization>> GetOrgByLocationAsync([FromBody] GetOrgByLocationInput dto)
    {
        if (string.IsNullOrWhiteSpace(dto.BizType))
            throw new ArgumentNullException(nameof(dto.BizType));

        if (dto.Location == null)
            throw new ArgumentNullException(nameof(dto.Location));

        var logs = new StringBuilder();

        try
        {
            logs.AppendLine(nameof(GetOrgByLocationAsync));
            logs.AppendLine("request-data:");
            logs.AppendLine(this.JsonDataSerializer.SerializeToString(dto));

            var org = await this.ResolveWapOrgAsync(dto);

            if (org != null)
            {
                logs.AppendLine($"地理推断组织：{org.organizationName}");
                org = await HandleMappingAsync(dto, org);
                logs.AppendLine($"映射规则处理后组织：{org.organizationName}");

                return new ApiResponse<WapOrganization>(org);
            }
            else
            {
                logs.AppendLine("地理推断组织失败");
            }

            org = await HandleFallbackAsync(dto);
            logs.AppendLine($"降级后的默认组织：{org?.organizationName}");

            return new ApiResponse<WapOrganization>(org);
        }
        catch (Exception e)
        {
            logs.AppendLine(e.Message);
            throw;
        }
        finally
        {
            this.Logger.LogInformation(message: logs.ToString());
        }
    }

    [HttpPost("current-store")]
    public async Task<ApiResponse<StoreDto>> CurrentStoreAsync()
    {
        var storeId = await this._qingdaoLocationBasedStoreSelectorContributor.GetStoreIdOrEmptyAsync();

        if (!string.IsNullOrWhiteSpace(storeId))
        {
            var store = await this._storeService.GetByIdAsync(storeId);
            if (store != null)
            {
                return new ApiResponse<StoreDto>(store);
            }
        }

        return new ApiResponse<StoreDto>();
    }
}