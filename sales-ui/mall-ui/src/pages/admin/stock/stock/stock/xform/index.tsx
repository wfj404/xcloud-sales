import { Form, Input, message, Modal, Select } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StockDto, StockItemDto, SupplierDto, WarehouseDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XItems from './items';

export default (props: {
  show: boolean;
  hide: () => void;
  data: StockDto;
  ok: () => void;
}) => {
  const { show, hide, data, ok } = props;
  const [loading, _loading] = useState(false);
  const [warehouses, _warehouses] = useState<WarehouseDto[]>([]);
  const [suppliers, _suppliers] = useState<SupplierDto[]>([]);

  const [form, _form] = useState<StockDto>({});

  const checkGoodsInput = (m: StockItemDto) => {
    if (m.SkuId && m.Quantity && m.Quantity > 0 && m.Price && m.Price > 0) {
    } else {
      return false;
    }

    return true;
  };

  const save = (row: StockDto) => {
    if (!row.Items || row.Items.length <= 0) {
      message.error('请添加商品');
      return;
    }
    if (row.Items.some((x) => !checkGoodsInput(x))) {
      message.error('请输入采购信息');
      return;
    }

    _loading(true);
    apiClient.mallAdmin
      .stockInsertStock({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const loadData = () => {
    apiClient.mallAdmin.supplierQueryAll({}).then((res) => {
      handleResponse(res, () => {
        _suppliers(res.data.Data || []);
      });
    });

    apiClient.mallAdmin.warehouseQueryAll({}).then((res) => {
      handleResponse(res, () => {
        _warehouses(res.data.Data || []);
      });
    });
  };

  useEffect(() => {
    show && loadData();
  }, [show]);

  useEffect(() => {
    _form({
      ...data,
    });
  }, [data]);

  return (
    <>
      <Modal
        confirmLoading={loading}
        open={show}
        onCancel={() => hide()}
        onOk={() => save(form)}
        width={'90%'}
        title="添加采购单"
        forceRender
        destroyOnClose
      >
        <Form
          labelCol={{ flex: '110px' }}
          labelAlign="right"
          wrapperCol={{ flex: 1 }}
        >
          <Form.Item label="供应商" name="SupplierId">
            <Select
              allowClear
              placeholder="请选择供货商"
              value={form.SupplierId}
              onChange={(e) => {
                _form((x) => ({
                  ...x,
                  SupplierId: e,
                }));
              }}
              onClear={() => {
                _form({ ...form, SupplierId: undefined });
              }}
              options={suppliers.map((x) => ({
                label: x.Name,
                value: x.Id,
              }))}
            ></Select>
          </Form.Item>
          <Form.Item label="目标仓库">
            <Select
              allowClear
              placeholder="请选择目标仓库"
              value={form.WarehouseId}
              onChange={(e) => {
                _form((x) => ({
                  ...x,
                  WarehouseId: e,
                }));
              }}
              onClear={() => {
                _form({ ...form, WarehouseId: undefined });
              }}
              options={warehouses.map((x) => ({
                label: x.Name,
                value: x.Id,
              }))}
            ></Select>
          </Form.Item>
          <Form.Item label="备注" name="Remark" rules={[{ max: 500 }]}>
            <Input.TextArea placeholder="请输入采购备注" />
          </Form.Item>
        </Form>
        {show && (
          <XItems
            items={form.Items || []}
            onChange={(e: StockItemDto[]) => {
              _form({
                ...form,
                Items: e,
              });
            }}
          />
        )}
      </Modal>
    </>
  );
};
