﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Service.SerialNos;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Service.Warehouses;

public interface IStockService : ISalesAppService
{
    Task<StockDto[]> AttachWarehouseAsync(StockDto[] data);

    Task<StockDto[]> AttachSuppliersAsync(StockDto[] data);

    Task<StockDto[]> AttachItemsAsync(StockDto[] data);

    Task<int> QuerySkuWarehouseStockAsync(string skuId);

    Task DeleteUnApprovedStockAsync(string stockId);

    Task<PagedResponse<StockDto>> QueryPagingAsync(QueryWarehouseStockPagingInput dto);

    Task ApproveStockAsync(string stockId);

    Task InsertWarehouseStockAsync(StockDto dto);
}

public class StockService : SalesAppService, IStockService
{
    private readonly ISalesRepository<Stock> _salesRepository;
    private readonly ISerialNoService _serialNoService;

    public StockService(ISalesRepository<Stock> salesRepository, ISerialNoService serialNoService)
    {
        _salesRepository = salesRepository;
        _serialNoService = serialNoService;
    }

    public async Task<StockDto[]> AttachWarehouseAsync(StockDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _salesRepository.GetDbContextAsync();

        var warehouseIds = data
            .Where(x => !string.IsNullOrWhiteSpace(x.WarehouseId))
            .Select(x => x.WarehouseId).Distinct().ToArray();
        if (warehouseIds.Any())
        {
            var warehouses = await db.Set<Warehouse>().Where(x => warehouseIds.Contains(x.Id)).ToArrayAsync();
            foreach (var m in data)
            {
                var warehouse = warehouses.FirstOrDefault(x => x.Id == m.WarehouseId);
                if (warehouse == null)
                    continue;
                m.Warehouse = ObjectMapper.Map<Warehouse, WarehouseDto>(warehouse);
            }
        }

        return data;
    }

    public async Task<StockDto[]> AttachSuppliersAsync(StockDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _salesRepository.GetDbContextAsync();

        var supplierIds = data
            .Where(x => !string.IsNullOrWhiteSpace(x.SupplierId))
            .Select(x => x.SupplierId).Distinct().ToArray();

        if (supplierIds.Any())
        {
            var suppliers = await db.Set<Supplier>().Where(x => supplierIds.Contains(x.Id)).ToArrayAsync();
            foreach (var m in data)
            {
                var supplier = suppliers.FirstOrDefault(x => x.Id == m.SupplierId);
                if (supplier == null)
                    continue;
                m.Supplier = ObjectMapper.Map<Supplier, SupplierDto>(supplier);
            }
        }

        return data;
    }

    public async Task<StockDto[]> AttachItemsAsync(StockDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _salesRepository.GetDbContextAsync();

        var allItems = await db.Set<StockItem>().AsNoTracking()
            .Where(x => ids.Contains(x.StockId)).ToArrayAsync();

        foreach (var m in data)
        {
            var items = allItems.Where(x => x.StockId == m.Id).ToArray();
            m.Items = items.Select(x => ObjectMapper.Map<StockItem, StockItemDto>(x))
                .ToArray();
        }

        return data;
    }

    public async Task<int> QuerySkuWarehouseStockAsync(string skuId)
    {
        if (string.IsNullOrWhiteSpace(skuId))
            throw new ArgumentNullException(nameof(skuId));

        var now = Clock.Now;

        var db = await _salesRepository.GetDbContextAsync();

        var query = from item in db.Set<StockItem>().AsNoTracking()
            join stock in db.Set<Stock>().AsNoTracking()
                on item.StockId equals stock.Id
            select new { item, stock };

        query = query.Where(x => x.stock.Approved);
        query = query.Where(x => x.item.SkuId == skuId);
        query = query.Where(x => x.item.Quantity > x.item.DeductQuantity);

        var count = await query.SumAsync(x => x.item.Quantity - x.item.DeductQuantity);

        return count;
    }

    public async Task DeleteUnApprovedStockAsync(string stockId)
    {
        if (string.IsNullOrWhiteSpace(stockId))
            throw new ArgumentNullException(nameof(stockId));

        var entity = await _salesRepository.FirstOrDefaultAsync(x => x.Id == stockId);
        if (entity == null)
            throw new EntityNotFoundException(nameof(DeleteUnApprovedStockAsync));

        if (entity.Approved)
            throw new UserFriendlyException("this stock is approved,can't be deleted");

        await _salesRepository.DeleteAsync(entity);
    }

    private IQueryable<Stock> BuildStockQuery(DbContext db, QueryWarehouseStockPagingInput dto)
    {
        var query = db.Set<Stock>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.SerialNo))
            query = query.Where(x => x.No == dto.SerialNo);

        if (!string.IsNullOrWhiteSpace(dto.SupplierId))
            query = query.Where(x => x.SupplierId == dto.SupplierId);

        if (!string.IsNullOrWhiteSpace(dto.WarehouseId))
            query = query.Where(x => x.WarehouseId == dto.WarehouseId);

        if (dto.Approved != null)
            query = query.Where(x => x.Approved == dto.Approved.Value);

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime <= dto.EndTime.Value);

        return query;
    }

    public async Task<PagedResponse<StockDto>> QueryPagingAsync(QueryWarehouseStockPagingInput dto)
    {
        var db = await _salesRepository.GetDbContextAsync();

        var query = BuildStockQuery(db, dto);

        var count = await query.CountOrDefaultAsync(dto);

        var stocks = await query.OrderByDescending(x => x.CreationTime).PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var dtoList = stocks.Select(x => ObjectMapper.Map<Stock, StockDto>(x)).ToArray();

        return new PagedResponse<StockDto>(dtoList, dto, count);
    }

    private string MonthString() => Clock.Now.ToString("yyyyMM");

    private async Task<string> GenerateNoAsync()
    {
        var month = MonthString();

        var serialNo =
            await _serialNoService.CreateSerialNoAsync(
                new CreateSerialNoDto($"warehouse-stock-no@{month}"));

        return $"{month}{serialNo.ToString().PadLeft(9, '0')}";
    }

    private async Task CheckInsertWarehouseStockInputAsync(StockDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (ValidateHelper.IsEmptyCollection(dto.Items))
            throw new ArgumentNullException(nameof(dto.Items));

        foreach (var m in dto.Items)
        {
            if (string.IsNullOrWhiteSpace(m.SkuId))
                throw new UserFriendlyException("goods information is lost");

            if (m.Quantity <= 0)
                throw new UserFriendlyException(nameof(m.Quantity));

            if (m.Price < 0)
                throw new UserFriendlyException(nameof(m.Price));
        }

        await Task.CompletedTask;
    }

    public async Task InsertWarehouseStockAsync(StockDto dto)
    {
        await CheckInsertWarehouseStockInputAsync(dto);

        var db = await _salesRepository.GetDbContextAsync();

        var entity = ObjectMapper.Map<StockDto, Stock>(dto);
        entity.Id = GuidGenerator.CreateGuidString();
        entity.No = await GenerateNoAsync();
        entity.Approved = false;
        entity.ApprovedByUserId = string.Empty;
        entity.ApprovedTime = null;
        entity.CreationTime = Clock.Now;

        var items = dto.Items.Select(x => ObjectMapper.Map<StockItemDto, StockItem>(x))
            .ToArray();
        foreach (var m in items)
        {
            m.Id = GuidGenerator.CreateGuidString();
            m.StockId = entity.Id;
            m.DeductQuantity = default;
            m.LastModificationTime = null;
        }

        db.Set<Stock>().Add(entity);
        db.Set<StockItem>().AddRange(items);

        await db.SaveChangesAsync();
    }

    public async Task ApproveStockAsync(string stockId)
    {
        if (string.IsNullOrWhiteSpace(stockId))
            throw new ArgumentNullException(nameof(ApproveStockAsync));

        var db = await _salesRepository.GetDbContextAsync();

        var entity = await db.Set<Stock>().FirstOrDefaultAsync(x => x.Id == stockId);
        if (entity == null)
            throw new EntityNotFoundException(nameof(stockId));

        if (entity.Approved)
            return;

        var items = await db.Set<StockItem>().Where(x => x.StockId == stockId).ToArrayAsync();
        if (!items.Any())
            throw new EntityNotFoundException("stock items not found");

        var skuIds = items.Select(x => x.SkuId).Distinct().ToArray();

        var skuList = await db.Set<Sku>().IgnoreQueryFilters()
            .Where(x => skuIds.Contains(x.Id))
            .ToArrayAsync();

        foreach (var m in items)
        {
            var sku = skuList.FirstOrDefault(x => x.Id == m.SkuId);
            if (sku == null)
                throw new UserFriendlyException("goods spec combination not exist");
        }

        entity.Approved = true;
        entity.ApprovedTime = Clock.Now;

        await db.TrySaveChangesAsync();
    }
}