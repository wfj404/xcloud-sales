using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Queue;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Orders;

public static class OrderExtension
{
    public static async Task UpdateOrderAndTriggerChangedEventAsync(this ISalesRepository<Order> repository,
        Order order,
        SalesEventBusService salesEventBusService)
    {
        await repository.UpdateAsync(order, autoSave: true);

        await salesEventBusService.NotifyOrderUpdatedAsync(order);
    }

    public static bool IsOfflinePayment(this Order order) =>
        order.PaymentMethodId == SalesConstants.PaymentMethod.Offline;

    public static bool IsOnlinePayment(this Order order) =>
        order.PaymentMethodId == SalesConstants.PaymentMethod.Online;

    public static bool IsNormalGoodsOrder(this Order order) =>
        order.GoodsType == GoodsTypeEnum.Goods;

    public static bool IsFinished(this Order order) =>
        order.OrderStatusId == SalesConstants.OrderStatus.Finished;

    public static bool IsClosed(this Order order) =>
        order.OrderStatusId == SalesConstants.OrderStatus.Closed;

    public static bool IsPending(this Order order) =>
        order.OrderStatusId == SalesConstants.OrderStatus.Pending;

    public static bool IsProcessing(this Order order) =>
        order.OrderStatusId == SalesConstants.OrderStatus.Processing;

    public static bool IsPaid(this Order order) => order.PaymentStatusId == SalesConstants.PaymentStatus.Paid;

    public static bool IsShipped(this Order order) => order.ShippingStatusId == SalesConstants.ShippingStatus.Shipped;

    public static bool IsShipping(this Order order) => order.ShippingStatusId == SalesConstants.ShippingStatus.Shipping;

    public static bool IsAfterSales(this Order order) => order.OrderStatusId == SalesConstants.OrderStatus.AfterSales;

    public static bool IsToService(this Order order)
    {
        var paymentOk = order.IsPaid() || order.IsOfflinePayment();

        return paymentOk && !order.IsShipped();
    }

    public static IQueryable<Order> WhereToService(this IQueryable<Order> query)
    {
        //payment is fine
        query = query.Where(x =>
            x.PaymentStatusId == SalesConstants.PaymentStatus.Paid ||
            x.PaymentMethodId == SalesConstants.PaymentMethod.Offline);

        query = query.Where(x => x.ShippingStatusId != SalesConstants.ShippingStatus.Shipped);

        return query;
    }
}