﻿using JetBrains.Annotations;
using XCloud.Application.Utils;

namespace XCloud.Sales.Application.Service.Shipping.Delivery.ShortDistance;

[TypeIdentityName("store-short-distance-delivery-settings")]
public class StoreShortDistanceDeliverySettings : ShippingSettingsBase
{
    public decimal? MinimalOrderPrice { get; set; }

    public int? MaxDistance { get; set; }

    [Obsolete] public string CalculatePriceType { get; set; }

    public TieredPrice TieredPrice { get; set; }

    private ServiceTimeDto _serviceTime;

    [NotNull]
    public ServiceTimeDto ServiceTime
    {
        get => this._serviceTime ??= new ServiceTimeDto();
        set => this._serviceTime = value;
    }
}