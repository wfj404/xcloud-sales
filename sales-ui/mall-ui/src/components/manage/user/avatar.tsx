import { Avatar, Popover, Space } from 'antd';

import { resolveAvatar } from '@/utils/storage';
import { UserDto } from '@/utils/swagger';
import { simpleString } from '@/utils/utils';

import XUserCard from './user_card';

export default ({ model }: { model?: UserDto | null }) => {
  if (!model) {
    return null;
  }

  const avatarUrl = resolveAvatar(model.Avatar, {
    width: 100,
    height: 100,
  });

  const nickName = simpleString(
    [model.NickName, model.IdentityName, model.Id]
      .filter((x) => x && x.length > 0)
      .at(0) || '--',
    10,
  );

  return (
    <>
      <Popover content={<XUserCard model={model} />}>
        <Space direction="horizontal">
          <Avatar size={'small'} shape="square" src={avatarUrl}>
            {nickName}
          </Avatar>
          <span>
            <a>{nickName}</a>
          </span>
        </Space>
      </Popover>
    </>
  );
};
