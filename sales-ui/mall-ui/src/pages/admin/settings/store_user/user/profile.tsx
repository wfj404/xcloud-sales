import { Modal } from 'antd';

import XProfile from '@/components/manage/user/profile';
import { isStringEmpty } from '@/utils/utils';

export default ({
  userId,
  hide,
}: {
  userId?: string | null;
  hide: () => void;
}) => {
  const show = !isStringEmpty(userId);

  return (
    <>
      <Modal
        title={'用户信息'}
        width={'90%'}
        open={show}
        onCancel={() => {
          hide();
        }}
        onOk={() => {
          hide();
        }}
      >
        <XProfile userId={userId || ''} />
      </Modal>
    </>
  );
};
