using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Mapping;
using XCloud.Platform.WeChat.Domain;

namespace XCloud.Platform.WeChat.Mapping;

public class WxUnionMap : PlatformEntityTypeConfiguration<WxUnion>
{
    public override void Configure(EntityTypeBuilder<WxUnion> builder)
    {
        builder.ToTable("sys_wx_union");
        
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.Platform).IsRequired().HasMaxLength(100);
        builder.Property(x => x.AppId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.OpenId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.UnionId).IsRequired().HasMaxLength(100);

        base.Configure(builder);
    }
}