﻿using System.ComponentModel;

namespace XCloud.Platform.Application.Core;

public static class PlatformSharedConstants
{
    public const string ServiceName = "platform";

    public static class Roles
    {
        /// <summary>
        /// 管理员
        /// </summary>
        public static int ManagerRole => (int)MemberRoleEnum.管理员;

        /// <summary>
        /// 管理员或者普通成员
        /// </summary>
        public static int MemberRole => (int)(MemberRoleEnum.管理员 | MemberRoleEnum.普通成员);

        /// <summary>
        /// 所有
        /// </summary>
        public static int AnyRole => (int)(MemberRoleEnum.管理员 | MemberRoleEnum.普通成员 | MemberRoleEnum.观察者);
    }
}

[Flags]
public enum MemberRoleEnum : int
{
    管理员 = 1 << 0,

    普通成员 = 1 << 3,

    [Description("观察者-只能看")] 观察者 = 1 << 4
}

public static class AuthConstants
{
    public static class Scheme
    {
        /// <summary>
        /// 外部登陆域
        /// </summary>
        public static string ExternalLoginScheme => "external_login_scheme";

        public static string JwtTokenScheme => "jwt_token_scheme";

        public static string NullHandlerScheme => "null_token_handler";
    }

    public static class Account
    {
        public static string DefaultUserName => "user";
    }
}