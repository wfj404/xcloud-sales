import { Button, Card, Modal } from 'antd';
import { useState } from 'react';

import { getSkuName } from '@/utils/biz';
import { SkuDto } from '@/utils/swagger';
import { GlobalContainer } from '@/utils/utils';
import { DragSortTable, ProColumns } from '@ant-design/pro-components';

import XGoodsSelector from './sku_selector';

export default ({
  sku_list,
  onChange,
  maxCount,
}: {
  sku_list: SkuDto[];
  onChange: (e: SkuDto[]) => void;
  maxCount?: number;
}) => {
  const [show, _show] = useState(false);

  const count = maxCount || 100000;
  const disabled = sku_list.length >= count;

  const columns: ProColumns<SkuDto>[] = [
    {
      title: 'No',
      dataIndex: 'index',
      valueType: 'indexBorder',
    },
    {
      title: '排序',
      dataIndex: 'sort',
      width: 60,
      className: 'drag-visible',
    },
    {
      title: '商品',
      render: (d, x) => getSkuName(x) || '--',
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      render: (d, x) => {
        return (
          <>
            <Button
              type="link"
              danger
              onClick={() => {
                if (!confirm('删除？')) {
                  return;
                }
                onChange([...sku_list.filter((m) => m.Id != x.Id)]);
              }}
            >
              删除
            </Button>
          </>
        );
      },
    },
  ];

  return (
    <>
      <Modal
        title="搜索商品"
        open={show}
        onCancel={() => _show(false)}
        onOk={() => _show(false)}
      >
        <XGoodsSelector
          onChange={(e) => {
            if (!e) {
              return;
            }
            if (disabled) {
              GlobalContainer.message?.error(`数量已达上限：${count}`);
              return;
            }
            if (!confirm(`添加${getSkuName(e) || '--'}？`)) {
              return;
            }
            if (sku_list.some((x) => x.Id == e.Id)) {
              GlobalContainer.message?.error('该商品已存在');
              return;
            }
            onChange([...sku_list, e]);
            _show(false);
          }}
        />
      </Modal>
      <Card
        size="small"
        title="选择商品"
        extra={
          <Button
            disabled={disabled}
            type="link"
            size="small"
            onClick={() => {
              _show(true);
            }}
          >
            添加商品
          </Button>
        }
      >
        <DragSortTable
          //rowKey={(x) => x.Id || '--'}
          rowKey={'Id'}
          columns={columns}
          dataSource={sku_list || []}
          toolBarRender={false}
          pagination={false}
          headerTitle={false}
          search={false}
          dragSortKey="sort"
          onDragSortEnd={(a, b, datalist) => {
            onChange([...datalist]);
          }}
        ></DragSortTable>
      </Card>
    </>
  );
};
