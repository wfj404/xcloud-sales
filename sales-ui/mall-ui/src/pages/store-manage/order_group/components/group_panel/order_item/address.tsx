import { Button, Col, Form, Input, Row, Space } from 'antd';
import { ReactNode } from 'react';

import { UserAddressDto } from '@/utils/swagger';
import { findMobile, isMobile, isStringEmpty } from '@/utils/utils';

export default ({
  address,
  onChange,
  nameLabel,
}: {
  address: UserAddressDto;
  onChange: (e: UserAddressDto) => void;
  nameLabel?: ReactNode;
}) => {
  if (!address || !onChange) {
    return <h1>error</h1>;
  }

  const is_mobile = isMobile(address.Tel || '');
  const mobile_finded_in_detail = findMobile(address.AddressDetail || '')?.at(
    0,
  );

  const renderMobileSuggestion = () => {
    if (is_mobile) {
      return null;
    }
    if (isStringEmpty(mobile_finded_in_detail)) {
      return null;
    }
    return (
      <>
        <Space direction="horizontal">
          <span>找到手机号：{mobile_finded_in_detail}</span>
          <Button
            size="small"
            type="link"
            onClick={() => {
              onChange({
                ...address,
                Tel: mobile_finded_in_detail,
              });
            }}
          >
            使用此号码
          </Button>
        </Space>
      </>
    );
  };

  return (
    <>
      <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
        <Row gutter={[10, 10]}>
          <Col span={6}>
            <Form.Item label={nameLabel || '收件人'}>
              <Input
                value={address.Name || ''}
                status={isStringEmpty(address.Name) ? 'error' : undefined}
                onChange={(e) => {
                  onChange({ ...address, Name: e.target.value });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="联系电话">
              <Input
                status={is_mobile ? undefined : 'error'}
                value={address.Tel || ''}
                onChange={(e) => {
                  onChange({ ...address, Tel: e.target.value });
                }}
              />
              {is_mobile || <b style={{ color: 'red' }}>请输入有效手机号</b>}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item label="收件地址">
              <Input.TextArea
                rows={2}
                status={
                  isStringEmpty(address.AddressDetail) ? 'error' : undefined
                }
                value={address.AddressDetail || ''}
                onChange={(e) => {
                  onChange({ ...address, AddressDetail: e.target.value });
                }}
              />
              {renderMobileSuggestion()}
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
};
