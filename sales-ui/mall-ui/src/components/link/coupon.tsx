import { LinkDto } from '@/utils/biz';
import { Space } from 'antd';
import React from 'react';

export default ({ link, onChange }: { link: LinkDto, onChange: (x: LinkDto) => void }) => {

  if (!link) {
    return null;
  }

  return <>
    <Space direction={'vertical'} style={{
      width: '100%',
    }}>
      <h2>todo</h2>
    </Space>
  </>;
};
