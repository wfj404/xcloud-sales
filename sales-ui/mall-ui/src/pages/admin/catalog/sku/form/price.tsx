import { Alert, Card, Col, Form, InputNumber, Row, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { GoodsGradePriceDto, SkuDto } from '@/utils/swagger';

export default ({
  data,
  _data,
}: {
  data: SkuDto;
  _data: (d: SkuDto) => void;
}) => {
  const app = useSnapshot(storeState) as StoreType;

  const all_grades = app.grades || [];

  const [loading, _loading] = useState(false);

  const originMappings = data.GradePrices || [];
  const mapping = all_grades.map<GoodsGradePriceDto>((x) => {
    let exist = originMappings.find((d) => d.GradeId == x.Id);
    if (exist) {
      return { ...exist };
    }
    const item: GoodsGradePriceDto = {
      GradeId: x.Id,
      SkuId: data.Id,
      PriceOffset: 0,
    };
    return item;
  });
  const _mapping = (x: GoodsGradePriceDto[]) => {
    _data({
      ...data,
      GradePrices: x,
    });
  };

  const columns: ColumnProps<GoodsGradePriceDto>[] = [
    {
      title: '会员等级',
      render: (x: GoodsGradePriceDto) => {
        return all_grades.find((d) => d.Id == x.GradeId)?.Name || '--';
      },
    },
    {
      title: '差价',
      render: (v, x: GoodsGradePriceDto, i: number) => {
        return (
          <InputNumber
            value={x.PriceOffset || 0}
            onChange={(e) => {
              let data = [...mapping];
              data[i].PriceOffset = e || 0;
              _mapping(data);
            }}
          />
        );
      },
    },
  ];

  useEffect(() => {
    app.queryGrades();
  }, []);

  const renderGradePriceTable = () => {
    if (all_grades.length <= 0) {
      return <Alert message={'未设置用户等级'} />;
    }

    return (
      <Table
        rowKey={(x) => x.Id || ''}
        columns={columns}
        dataSource={mapping}
        pagination={false}
        bordered={false}
      />
    );
  };

  return (
    <>
      <Card title="设置价格" loading={loading}>
        <Form
          labelCol={{ flex: '110px' }}
          labelAlign="right"
          wrapperCol={{ flex: 1 }}
        >
          <Row gutter={10}>
            <Col span={8}>
              <Form.Item label={'零售价'}>
                <InputNumber
                  min={0}
                  value={data.Price}
                  onChange={(e) => {
                    _data({
                      ...data,
                      Price: e || 0,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label="会员价">{renderGradePriceTable()}</Form.Item>
        </Form>
      </Card>
    </>
  );
};
