﻿using System.Net.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Configuration;
using XCloud.Core.Http.Dynamic.Definition;
using XCloud.Core.Json;

namespace XCloud.Core.Http.Dynamic.Proxy;

[ExposeServices(typeof(HttpMessageBuilder))]
public class HttpMessageBuilder : ITransientDependency
{
    private readonly IDynamicClientServiceDiscovery _dynamicClientServiceDiscovery;
    private readonly IJsonDataSerializer _jsonDataSerializer;
    private readonly AppConfig _appConfig;

    public HttpMessageBuilder(IDynamicClientServiceDiscovery dynamicClientServiceDiscovery,
        IJsonDataSerializer jsonDataSerializer, AppConfig appConfig)
    {
        _dynamicClientServiceDiscovery = dynamicClientServiceDiscovery;
        _jsonDataSerializer = jsonDataSerializer;
        _appConfig = appConfig;
    }

    string BuildRequestUrl(ApiCallDescriptor descriptor)
    {
        var baseUrl =
            _dynamicClientServiceDiscovery.GetServiceAddress(descriptor.ActionDefinition.ServiceDefinition
                .ClientConfiguration.ServiceName);

        if (string.IsNullOrWhiteSpace(baseUrl))
            throw new ArgumentNullException(nameof(baseUrl));

        var routePrefix = descriptor.ActionDefinition.ServiceDefinition.RouteOrEmpty();
        var path = descriptor.ActionDefinition.RouteOrEmpty();

        var routes = new string[] { routePrefix, path }.Where(x => x?.Length > 0).ToArray();
        routes = routes.SelectMany(x => x.Split('/', '\\')).ToArray();

        var requestPath = string.Join("/", routes);

        var args = descriptor.Args;

        var pathParameters =
            EnumerableExtension.ToDictionaryUpdateDuplicateKey(
                args.Where(x => x.Key.BindingSource == BindingSource.Path), x => x.Key, x => x.Value);
        requestPath = requestPath.ReplacePathArgs(
            EnumerableExtension.ToDictionaryUpdateDuplicateKey(pathParameters, x => x.Key.ParameterInfo.Name,
                x => x.Value));

        var queryParameters =
            EnumerableExtension.ToDictionaryUpdateDuplicateKey(
                args.Where(x => x.Key.BindingSource == BindingSource.Query), x => x.Key, x => x.Value);
        var queryString = EnumerableExtension.ToDictionaryUpdateDuplicateKey(queryParameters,
            x => x.Key.ParameterInfo.Name, x => x.Value?.ToString() ?? string.Empty).ToUrlQueryString();
        requestPath = $"{requestPath}?{queryString}";

        var requestUrl = $"{baseUrl}/{requestPath}";
        return requestUrl;
    }

    public HttpRequestMessage BuildMessage(ApiCallDescriptor descriptor)
    {
        var args = descriptor.Args;
        var requestUrl = BuildRequestUrl(descriptor);

        var httpMethod = descriptor.ActionDefinition.ActionHttpMethod();

        if ("GET".Equals(httpMethod, StringComparison.OrdinalIgnoreCase))
        {
            var message = new HttpRequestMessage(HttpMethod.Get, requestUrl);

            return message;
        }
        else if ("POST".Equals(httpMethod, StringComparison.OrdinalIgnoreCase))
        {
            var message = new HttpRequestMessage(HttpMethod.Post, requestUrl);

            var bodyArgs = args.Where(x => x.Key.BindingSource == BindingSource.Body).ToArray();
            if (bodyArgs.Any())
            {
                (message.Content == null).Should().BeTrue();

                (bodyArgs.Length == 1).Should().BeTrue();
                var entity = bodyArgs.FirstOrDefault().Value ?? throw new AbpException("post body 参数为空");

                var body = _jsonDataSerializer.SerializeToString(entity);
                message.Content = new StringContent(body, _appConfig.AppEncoding, "application/json");
            }

            var formArgs = args.Where(x => x.Key.BindingSource == BindingSource.Form).ToArray();
            if (formArgs.Any())
            {
                (message.Content == null).Should().BeTrue();

                var content = new MultipartFormDataContent();
                foreach (var p in args)
                {
                    content.Add(new StringContent(p.Value?.ToString()), p.Key.ParameterInfo.Name);
                }

                message.Content = content;
            }

            return message;
        }
        else
        {
            throw new NotImplementedException();
        }
    }
}