﻿using Newtonsoft.Json;
using Volo.Abp.DependencyInjection;

namespace XCloud.Core.Json.NewtonsoftJson;

public interface INewtonsoftJsonOptionAccessor
{
    JsonSerializerSettings SerializerSettings { get; }
}

[ExposeServices(typeof(INewtonsoftJsonOptionAccessor))]
public class DefaultNewtonsoftJsonOptionAccessor : INewtonsoftJsonOptionAccessor, ITransientDependency
{
    public DefaultNewtonsoftJsonOptionAccessor(IObjectAccessor<JsonSerializerSettings> serviceWrapper)
    {
        SerializerSettings = serviceWrapper.Value;
    }

    public JsonSerializerSettings SerializerSettings { get; }
}