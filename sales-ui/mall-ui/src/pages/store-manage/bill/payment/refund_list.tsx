import { Table, Tag, Typography } from 'antd';
import { ColumnType } from 'antd/lib/table';

import XTime from '@/components/common/time';
import XRenderTime from '@/components/common/time/render_time';
import { RefundBillDto } from '@/utils/swagger';
import { formatMoney, isArrayEmpty } from '@/utils/utils';

export default ({ datalist }: { datalist: RefundBillDto[] }) => {
  if (isArrayEmpty(datalist)) {
    return null;
  }

  const columns: ColumnType<RefundBillDto>[] = [
    {
      title: '退款单号',
      render: (d, x) => {
        return (
          <>
            <Typography.Paragraph copyable>{x.Id}</Typography.Paragraph>
            <div>{x.Description || '--'}</div>
          </>
        );
      },
    },
    {
      title: '退款金额',
      render: (d, x) => {
        return (
          <>
            <Tag color="blue">{formatMoney(x.Price || 0)}</Tag>
          </>
        );
      },
    },
    {
      title: '退款流水',
      render: (d, x) => {
        return (
          <>
            <Typography.Text copyable>
              {x.RefundTransactionId || '--'}
            </Typography.Text>
          </>
        );
      },
    },
    {
      title: '退款时间',
      render: (d, x) => {
        return <XRenderTime timeStr={x.RefundTime} />;
      },
    },
    {
      title: '时间',
      render: (d, x) => {
        return (
          <>
            <XTime model={x} />
          </>
        );
      },
    },
  ];

  return (
    <>
      <Table
        columns={columns}
        dataSource={datalist}
        pagination={false}
        expandable={{
          expandedRowRender: (e) => <div>{e.RefundNotifyData || '--'}</div>,
        }}
        title={() => <b>退款记录</b>}
      />
    </>
  );
};
