using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.WeChat.Domain;

public class WxUnion : PlatformBaseEntity
{
    public string Platform { get; set; }

    public string AppId { get; set; }

    public string OpenId { get; set; }

    public string UnionId { get; set; }
}