﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Authentication;

public class PermissionRecord : IEntityDto
{
    public string Name { get; set; }

    public string SystemName { get; set; }

    public string Group { get; set; }

    public static implicit operator string(PermissionRecord record) => record?.Name;
}

public static class SalesPermissions
{
    public static readonly PermissionRecord ManageStock = new()
    {
        Name = "ManageStock",
        SystemName = "ManageStock",
        Group = "Standard"
    };

    public static readonly PermissionRecord ManageReport = new()
    {
        Name = "ManageReport",
        SystemName = "ManageReport",
        Group = "Standard"
    };

    public static readonly PermissionRecord ManageFinance = new()
    {
        Name = "ManageFinance",
        SystemName = "ManageFinance",
        Group = "Standard"
    };

    public static readonly PermissionRecord ManageCatalog = new()
    {
        Name = "Admin area. Manage Catalog",
        SystemName = "ManageCatalog",
        Group = "Catalog"
    };

    public static readonly PermissionRecord ManageOrders = new()
    {
        Name = "Admin area. Manage Orders",
        SystemName = "ManageOrders",
        Group = "Orders"
    };

    public static readonly PermissionRecord ManageMarketing = new()
    {
        Name = "Admin area. Manage Coupon",
        SystemName = "ManageCoupons",
        Group = "Promo"
    };

    public static readonly PermissionRecord ManageSettings = new()
    {
        Name = "Admin area. Manage Settings",
        SystemName = "ManageSettings",
        Group = "Configuration"
    };
}

public class StoreUserAuthResult : ApiResponse<StoreUserDto>
{
    [CanBeNull] public StoreUserDto StoreUser => Data;

    [NotNull]
    public StoreUserDto GetRequiredStoreUser() => this.StoreUser ?? throw new BusinessException(nameof(StoreUserDto));
}

public class StoreManagerAuthResult : ApiResponse<StoreManagerDto>
{
    [CanBeNull] public StoreManagerDto StoreManager => Data;

    [NotNull]
    public StoreManagerDto GetRequiredStoreManager() =>
        this.StoreManager ?? throw new BusinessException(nameof(StoreManagerDto));
}