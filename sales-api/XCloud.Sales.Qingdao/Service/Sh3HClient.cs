﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Polly;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Utils;
using XCloud.Core.Cache;
using XCloud.Core.Extension;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Authentication;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Exceptions;
using XCloud.Sales.Qingdao.Utils;

namespace XCloud.Sales.Qingdao.Service;

[ExposeServices(typeof(QingdaoHttpCallExecutionPolicy))]
public class QingdaoHttpCallExecutionPolicy : ISingletonDependency
{
    public QingdaoHttpCallExecutionPolicy(CommonUtils commonUtils)
    {
        GetWapAdminPolicy = new Lazy<IAsyncPolicy>(commonUtils.BuildGenericHttpCallExecutionPolicy);
        GetMcsUserPolicy = new Lazy<IAsyncPolicy>(commonUtils.BuildGenericHttpCallExecutionPolicy);
    }

    [NotNull] [ItemNotNull] public Lazy<IAsyncPolicy> GetWapAdminPolicy { get; }

    [NotNull] [ItemNotNull] public Lazy<IAsyncPolicy> GetMcsUserPolicy { get; }
}

[ExposeServices(typeof(Sh3HClient))]
public class Sh3HClient : SalesAppService
{
    private readonly HttpClient _httpClient;
    private readonly QingdaoHttpCallExecutionPolicy _qingdaoHttpCallExecutionPolicy;
    private readonly ISalesRepository<Store> _repository;
    private readonly QingdaoUtils _qingdaoUtils;

    public Sh3HClient(IHttpClientFactory httpClientFactory,
        QingdaoHttpCallExecutionPolicy qingdaoHttpCallExecutionPolicy,
        ISalesRepository<Store> repository, QingdaoUtils qingdaoUtils)
    {
        _qingdaoHttpCallExecutionPolicy = qingdaoHttpCallExecutionPolicy;
        _repository = repository;
        _qingdaoUtils = qingdaoUtils;
        _httpClient = httpClientFactory.CreateClient(nameof(Sh3HClient));
    }

    public async Task<string[]> GetAdminPermissionsAsync(string token)
    {
        if (string.IsNullOrWhiteSpace(token))
            throw new ArgumentNullException(nameof(token));

        var trace = new Dictionary<string, string>();

        try
        {
            var config = Configuration.GetRequired3HConfig();
            var url = new[]
            {
                config.PlatformUrl,
                $"/api/wap/v2/auth/authorizations?appIdentity={config.SalesWapAppName}"
            }.ConcatUrl();
            trace[nameof(url)] = url;

            using var request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            request.Headers.Add("Token", token);

            using var res = await _httpClient.SendAsync(request);

            var json = await res.Content.ReadAsStringAsync();

            trace[nameof(json)] = json;

            var response = JsonDataSerializer.DeserializeFromString<WapResponse<WapPermission[]>>(json);

            var permissions = response.data?
                .Where(x => x.authorized)
                .Select(x => x.authCode)
                .WhereNotEmpty()
                .Distinct()
                .ToArray();

            return permissions;
        }
        catch (Exception e)
        {
            var exception = new WapException(message: nameof(GetPermissionsAsync), e)
                .WithData(nameof(trace), trace.ToUrlQueryString());

            Logger.LogWarning(message: nameof(GetPermissionsAsync), exception: exception);

            return null;
        }
    }

    public async Task<string[]> GetPermissionsAsync(string userExternalId)
    {
        if (!_qingdaoUtils.TryParseAdminId(userExternalId, out var id))
        {
            return Array.Empty<string>();
        }

        if (!WebHelper.RequiredHttpContext.IsWapToken())
        {
            Logger.LogWarning(message: $"{nameof(GetPermissionsAsync)}.wrong.token.type");
            return Array.Empty<string>();
        }

        var token = WebHelper.RequiredHttpContext.GetBearerToken();

        if (string.IsNullOrWhiteSpace(token))
        {
            Logger.LogWarning(message: $"{nameof(GetPermissionsAsync)}.token is required");
            return Array.Empty<string>();
        }

        var permissionList =
            await _qingdaoHttpCallExecutionPolicy.GetWapAdminPolicy.Value.ExecuteAsync(() =>
                this.GetAdminPermissionsAsync(token));

        return permissionList ?? Array.Empty<string>();
    }

    private string SelectTop1OrgSql => @"
SELECT TOP 1

ORGANIZATION_KEY,
cast(ORGANIZATION_KEY as nvarchar(100)) as organizationKey,
ORGANIZATION_ID as organizationId,
PARENT_ORGANIZATION_ID as parentOrganizationId,
ORGANIZATION_NAME as organizationName,
ORGANIZATION_CODE as organizationCode,
ORGANIZATION_TYPE as organizationType

FROM WAP_CONFIG.dbo.AUTH_ORGANIZATION 
";

    [NotNull]
    [ItemCanBeNull]
    private async Task<WapOrganization> GetOrganizationByIdAsync(int orgId)
    {
        if (orgId <= 0)
            throw new ArgumentNullException(nameof(orgId));

        var db = await _repository.GetDbContextAsync();

        var connection = db.Database.GetDbConnection();

        var sql = @$"
{SelectTop1OrgSql}
where ORGANIZATION_ID=@id
";

        var org = await connection.QueryFirstOrDefaultAsync<WapOrganization>(
            sql: sql,
            param: new { id = orgId },
            transaction: db.Database.CurrentTransaction?.GetDbTransaction());

        return org;
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<WapOrganization> GetOrganizationByCodeAsync(string orgCode)
    {
        if (string.IsNullOrWhiteSpace(orgCode))
            throw new ArgumentNullException(nameof(orgCode));

        var db = await _repository.GetDbContextAsync();

        var connection = db.Database.GetDbConnection();

        var sql = @$"
{SelectTop1OrgSql}
where ORGANIZATION_CODE=@code
";

        var org = await connection.QueryFirstOrDefaultAsync<WapOrganization>(
            sql: sql,
            param: new { code = orgCode },
            transaction: db.Database.CurrentTransaction?.GetDbTransaction());

        return org;
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<WapOrganization> ResolveStoreOrganizationByCodeWithCacheAsync(string code)
    {
        if (string.IsNullOrWhiteSpace(code))
            return null;

        var organization = await this.GetOrganizationByCodeAsync(code);
        if (organization == null)
            return null;

        var config = Configuration.GetRequired3HConfig();

        if (!config.StoreOrgCodeList.Any())
        {
            return null;
        }

        var handledList = await this.ResolveParentOrganizationWithCacheAsync(organization.organizationId);

        if (!handledList.Any())
        {
            return null;
        }

        handledList = handledList.Reverse().ToArray();

        foreach (var m in config.StoreOrgCodeList)
        {
            var storeOrg = handledList.FirstOrDefault(x => x.organizationCode == m);
            if (storeOrg != null)
            {
                return storeOrg;
            }
        }

        return null;
    }

    [NotNull]
    [ItemNotNull]
    public async Task<WapOrganization[]> ResolveParentOrganizationWithCacheAsync(int orgId)
    {
        var option = new CacheOption<WapOrganization[]>(
            key: $"{nameof(ResolveParentOrganizationWithCacheAsync)}.id={orgId}",
            expiration: TimeSpan.FromMinutes(3));

        var orgList = await this.CacheProvider.GetOrSetAsync(
            () => ResolveParentOrganizationAsync(orgId),
            option);

        return orgList ?? Array.Empty<WapOrganization>();
    }

    [NotNull]
    [ItemNotNull]
    private async Task<WapOrganization[]> ResolveParentOrganizationAsync(int orgId)
    {
        if (orgId <= 0)
            throw new ArgumentNullException(nameof(orgId));

        var handledList = new List<WapOrganization>();

        var currentId = orgId;

        while (true)
        {
            if (currentId <= 0)
                break;

            var currentOrg = await GetOrganizationByIdAsync(currentId);
            if (currentOrg == null)
                break;

            if (handledList.Any(x => x.organizationId == currentOrg.organizationId))
            {
                break;
            }

            handledList.Add(currentOrg);

            //next loop
            currentId = currentOrg.parentOrganizationId;
        }

        return handledList.ToArray().Reverse().ToArray();
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<WapAdminDto> GetWapAdminAsync(Sh3HConfig config, string token)
    {
        if (string.IsNullOrWhiteSpace(token))
            throw new ArgumentNullException(nameof(token));

        return await _qingdaoHttpCallExecutionPolicy.GetWapAdminPolicy.Value.ExecuteAsync(async () =>
        {
            var trace = new Dictionary<string, string>();
            try
            {
                var url = new[]
                {
                    config.PlatformUrl,
                    $"/api/wap/v2/users?token={HttpUtility.UrlEncode(token, Encoding.UTF8)}"
                }.ConcatUrl();
                trace[nameof(url)] = url;

                using var request = new HttpRequestMessage();
                request.RequestUri = new Uri(url);
                request.Method = HttpMethod.Get;
                //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                using var res = await _httpClient.SendAsync(request);

                //res.EnsureSuccessStatusCode();
                if (res.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return null;
                }

                var json = await res.Content.ReadAsStringAsync();

                trace[nameof(json)] = json;

                var response = JsonDataSerializer.DeserializeFromString<WapResponse<WapAdminDto>>(json);

                return response.data;
            }
            catch (Exception e)
            {
                var exception = new WapException(message: nameof(GetWapAdminAsync), e)
                    .WithData(nameof(trace), trace.ToUrlQueryString());

                Logger.LogWarning(message: nameof(GetWapAdminAsync), exception: exception);

                return null;
            }
        });
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<McsUserDto> GetMcsUserAsync(Sh3HConfig config, string token)
    {
        if (string.IsNullOrWhiteSpace(token))
            throw new ArgumentNullException(nameof(token));

        return await _qingdaoHttpCallExecutionPolicy.GetMcsUserPolicy.Value.ExecuteAsync(async () =>
        {
            var trace = new Dictionary<string, string>();

            try
            {
                var url = new[] { config.McsOpenApiUrl, "/open-api/system/users/my" }.ConcatUrl();
                trace[nameof(url)] = url;
                trace[nameof(token)] = token;
                trace[nameof(config.McsChannel)] = config.McsChannel;

                using var request = new HttpRequestMessage();
                request.RequestUri = new Uri(url);
                request.Method = HttpMethod.Get;
                request.Headers.Add("X-MCS-AUTH-TOKEN", token);
                request.Headers.Add("X-MCS-CHANNEL", config.McsChannel);
                //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                using var res = await _httpClient.SendAsync(request);

                //res.EnsureSuccessStatusCode();
                if (res.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return null;
                }

                var json = await res.Content.ReadAsStringAsync();

                trace[nameof(json)] = json;

                var response = JsonDataSerializer.DeserializeFromString<McsResponse<McsUserDto>>(json);

                return response.data;
            }
            catch (Exception e)
            {
                var exception = new McsException(message: nameof(GetMcsUserAsync), e)
                    .WithData(nameof(trace), trace.ToUrlQueryString());

                Logger.LogWarning(message: nameof(GetMcsUserAsync), exception: exception);

                return null;
            }
        });
    }
}