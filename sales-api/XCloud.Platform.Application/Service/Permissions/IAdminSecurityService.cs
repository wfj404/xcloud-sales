﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Domain.Security;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Admins;

namespace XCloud.Platform.Application.Service.Permissions;

public interface IAdminSecurityService : IXCloudAppService
{
    [NotNull]
    [ItemNotNull]
    Task<AdminGrantedPermissionResponse> GetGrantedPermissionsAsync(string adminId,
        CacheStrategy cacheStrategyOption);
}

public class AdminSecurityService : PlatformAppService, IAdminSecurityService
{
    private readonly IPlatformRepository<Role> _roleRepository;
    private readonly IAdminService _adminService;

    public AdminSecurityService(IPlatformRepository<Role> roleRepository,
        IAdminService adminService)
    {
        _roleRepository = roleRepository;
        _adminService = adminService;
    }

    private async Task<AdminGrantedPermissionResponse> GetGrantedPermissionsAsync(string adminId)
    {
        var db = await _roleRepository.GetDbContextAsync();

        var query = from rolePermission in db.Set<RolePermission>().AsNoTracking()
            join roleAdmin in db.Set<AdminRole>().AsNoTracking()
                on rolePermission.RoleId equals roleAdmin.RoleId
            select new { rolePermission, roleAdmin };

        query = query.Where(x => x.roleAdmin.AdminId == adminId);

        //permissions
        var permissionKeys = await query
            .OrderBy(x => x.rolePermission.PermissionKey)
            .Select(x => x.rolePermission.PermissionKey)
            .Distinct()
            .ToArrayAsync();

        var data = new AdminGrantedPermissionResponse
        {
            Permissions = permissionKeys,
        };

        //admin dto
        var admin = await _adminService.GetByIdAsync(adminId);
        data.SetData(admin);

        return data;
    }

    public async Task<AdminGrantedPermissionResponse> GetGrantedPermissionsAsync(string adminId,
        CacheStrategy cacheStrategyOption)
    {
        var key = $"{adminId}.permission.keys.response";

        var option = new CacheOption<AdminGrantedPermissionResponse>(key);

        var data = await CacheProvider.ExecuteWithPolicyAsync(
            () => GetGrantedPermissionsAsync(adminId),
            option, cacheStrategyOption);

        data ??= new AdminGrantedPermissionResponse();

        return data;
    }
}