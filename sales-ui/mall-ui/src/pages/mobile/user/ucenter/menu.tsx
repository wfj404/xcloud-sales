import { useRequest } from 'ahooks';
import { useEffect } from 'react';
import { history, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { isStringEmpty } from '@/utils/utils';
import { Settings } from '@mui/icons-material';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import BusinessIcon from '@mui/icons-material/Business';
import LocalActivityIcon from '@mui/icons-material/LocalActivity';
import MobileFriendlyIcon from '@mui/icons-material/MobileFriendly';
import {
  Box,
  ListItemIcon,
  ListItemText,
  MenuItem,
  MenuList,
  Paper,
  Typography,
} from '@mui/material';

interface IMenu {
  icon?: any;
  name?: string;
  path?: string;
  action?: () => void;
  right?: any;
}

const renderMenu = (items: IMenu[]) => {
  return (
    <Paper sx={{ maxWidth: '100%' }}>
      <MenuList>
        {items.map((x, i) => (
          <MenuItem
            key={i}
            onClick={() => {
              if (x.path) {
                history.push({
                  pathname: x.path,
                });
              } else {
                x.action && x.action();
              }
            }}
          >
            <ListItemIcon>{x.icon}</ListItemIcon>
            <ListItemText>{x.name}</ListItemText>
            <Typography variant="body2" color="text.secondary">
              {x.right}
            </Typography>
          </MenuItem>
        ))}
      </MenuList>
    </Paper>
  );
};

export default function () {
  const app = useSnapshot(storeState) as StoreType;
  const mobile = app.storeUser?.User?.UserMobile?.MobilePhone;

  const queryPendingOrderCountRequest = useRequest(
    apiClient.mall.orderPendingCount,
    { manual: true },
  );
  const pendingOrderCount = queryPendingOrderCountRequest.data?.data?.Data || 0;

  useEffect(() => {
    queryPendingOrderCountRequest.run();
  }, []);

  const actions: IMenu[] = [
    {
      icon: <AssignmentTurnedInIcon color="info" />,
      name: '订单',
      path: '/order',
      right: pendingOrderCount > 0 ? `${pendingOrderCount}个进行中` : ``,
    },
    {
      icon: <LocalActivityIcon color="warning" />,
      name: '优惠券',
      path: '/user/coupon',
    },
    {
      icon: <BusinessIcon />,
      name: '收货地址',
      right: '⌘X',
      path: '/user/address',
    },
    {
      icon: <MobileFriendlyIcon />,
      name: '绑定手机',
      right: isStringEmpty(mobile) ? '⌘C' : `${mobile}`,
      action: () => {},
    },
  ];

  const actions3: IMenu[] = [
    {
      icon: <Settings />,
      name: '设 置',
      path: '/ucenter/profile',
    },
  ];

  return (
    <>
      <Box sx={{ mb: 3, px: 1 }}>
        <Box sx={{ mb: 1 }}>{renderMenu(actions)}</Box>
        <Box sx={{ mb: 1 }}>{renderMenu(actions3)}</Box>
      </Box>
    </>
  );
}
