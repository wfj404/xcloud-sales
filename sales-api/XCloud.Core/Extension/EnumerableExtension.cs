﻿using System.Collections;
using Volo.Abp;
using XCloud.Core.Helper;

namespace XCloud.Core.Extension;

public static class EnumerableExtension
{
    public static IEnumerable<TSource> InBy<TSource, TKey>(this IEnumerable<TSource> first,
        IEnumerable<TSource> second,
        Func<TSource, TKey> keySelector,
        IEqualityComparer<TKey> comparer = null)
    {
        if (first == null || second == null || keySelector == null)
            throw new ArgumentNullException(nameof(NotInBy));

        var secondKeys = second.Select(keySelector).ToArray();

        Func<TKey, bool> containsBySecond;

        if (comparer != null)
            containsBySecond = x => secondKeys.Contains(x, comparer);
        else
            containsBySecond = x => secondKeys.Contains(x);

        foreach (var m in first)
        {
            var key = keySelector.Invoke(m);

            if (containsBySecond(key))
                yield return m;
        }
    }

    /// <summary>
    /// fix of except by
    /// </summary>
    public static IEnumerable<TSource> NotInBy<TSource, TKey>(this IEnumerable<TSource> first,
        IEnumerable<TSource> second,
        Func<TSource, TKey> keySelector,
        IEqualityComparer<TKey> comparer = null)
    {
        if (first == null || second == null || keySelector == null)
            throw new ArgumentNullException(nameof(NotInBy));

        var secondKeys = second.Select(keySelector).ToArray();

        Func<TKey, bool> containsBySecond;

        if (comparer != null)
            containsBySecond = x => secondKeys.Contains(x, comparer);
        else
            containsBySecond = x => secondKeys.Contains(x);

        foreach (var m in first)
        {
            var key = keySelector.Invoke(m);

            if (containsBySecond(key))
                continue;

            yield return m;
        }
    }

    public static bool IsEmtpy<T>(this IEnumerable<T> list) => !list.Any();

    public static IEnumerable<TResult> ItemOfType<TResult>(this IEnumerable source)
    {
        foreach (var m in source)
        {
            if (m is TResult d)
            {
                yield return d;
            }
        }
    }

    /// <summary>
    /// 不修改list，返回新list
    /// </summary>
    public static IEnumerable<T> AppendManyItems<T>(this IEnumerable<T> list, params T[] data)
    {
        foreach (var m in list)
        {
            yield return m;
        }

        foreach (var m in data)
        {
            yield return m;
        }
    }

    /// <summary>
    /// 修改list然后返回
    /// </summary>
    public static List<T> AddManyItems<T>(this List<T> list, params T[] data)
    {
        if (ValidateHelper.IsNotEmptyCollection(data))
        {
            list.AddRange(data);
        }

        return list;
    }

    public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> list) where T : class
    {
        var res = list.Where(x => x != null).ToArray();
        return res;
    }

    public static IEnumerable<string> WhereNotEmpty(this IEnumerable<string> list)
    {
        var res = list.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        return res;
    }

    public static bool AnyDuplicate<T>(this IEnumerable<T> list, IEqualityComparer<T> equalityComparer = default)
    {
        var grouped = list.GroupBy(x => x, comparer: equalityComparer).ToArray();

        foreach (var g in grouped)
        {
            if (g.Count() > 1)
                return true;
        }

        return false;
    }

    public static bool AnyDuplicateString(this IEnumerable<string> list) => list.AnyDuplicate();

    /// <summary>
    /// 在list中添加item，遇到重复就抛异常
    /// </summary>
    public static void AddOnceOrThrow(this IList<string> list, string flag, string errorMsg = null)
    {
        if (list.Contains(flag))
        {
            throw new UserFriendlyException(errorMsg ?? $"{flag}已经存在");
        }

        list.Add(flag);
    }

    /// <summary>
    /// 不会因为重复key报错，后面的key会覆盖前面的key
    /// </summary>
    public static Dictionary<K, V> ToDictionaryUpdateDuplicateKey<T, K, V>(this IEnumerable<T> list,
        Func<T, K> keySelector,
        Func<T, V> valueSelector)
    {
        var dict = new Dictionary<K, V>();

        foreach (var m in list)
        {
            var key = keySelector.Invoke(m);
            var value = valueSelector.Invoke(m);

            dict[key] = value;
        }

        return dict;
    }

    /// <summary>
    /// python reduce similar with csharp aggregate
    /// </summary>
    /// <param name="list"></param>
    /// <param name="func"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T ReduceOrDefault<T>(this IEnumerable<T> list, Func<T, T, T> func)
    {
        T cursor = default;

        using var enumerator = list.GetEnumerator();

        if (enumerator.MoveNext())
        {
            cursor = enumerator.Current;

            while (enumerator.MoveNext())
            {
                cursor = func.Invoke(cursor, enumerator.Current);
            }
        }

        return cursor;
    }

    public static IEnumerable<IEnumerable<T>> ToBatch<T>(this IEnumerable<T> source, int batchSize)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));

        if (batchSize <= 0)
            throw new ArgumentException("batch size必须大于0");

        IEnumerable<T> Batch(IEnumerator<T> enumerator, int size)
        {
            do
            {
                yield return enumerator.Current;
                //you can change the size,because int is not reference type
                //every batch you get new number of batch size
            } while (--size > 0 && enumerator.MoveNext());
        }

        using (var enumerator = source.GetEnumerator())
        {
            while (enumerator.MoveNext())
            {
                yield return Batch(enumerator, batchSize);
            }
        }
    }
}