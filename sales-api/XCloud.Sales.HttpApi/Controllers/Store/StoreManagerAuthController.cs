using XCloud.Application.Extension;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/store-manager-auth")]
public class StoreManagerAuthController : ShopBaseController
{
    private readonly IStoreManagerService _storeManagerService;
    private readonly IUserMobileService _userMobileService;

    public StoreManagerAuthController(IStoreManagerService storeManagerService,
        IUserMobileService userMobileService)
    {
        _storeManagerService = storeManagerService;
        _userMobileService = userMobileService;
    }

    [HttpPost("auth")]
    public async Task<StoreManagerAuthResult> AuthAsync()
    {
        var res = await this.StoreManagerAuthService.GetStoreManagerAsync();

        res.Data ??= new StoreManagerDto();

        if (!res.Data.IsEmptyId())
        {
            await this._storeManagerService.AttachUsersAsync(new[] { res.Data });
            await this._storeManagerService.AttachStoresAsync(new[] { res.Data });
        }

        res.Data.User ??= new UserDto();

        if (!res.Data.User.IsEmptyId())
        {
            res.Data.User.UserMobile = await _userMobileService.GetMobileByUserIdAsync(res.Data.User.Id);
        }

        return res;
    }

    [HttpPost("store-manager-list")]
    public async Task<ApiResponse<StoreManagerDto[]>> StoreManagerListAsync()
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        var managers = await _storeManagerService.ListActiveManagersByUserIdAsync(loginUser.Id);

        await _storeManagerService.AttachStoresAsync(managers);

        return new ApiResponse<StoreManagerDto[]>(managers);
    }
}