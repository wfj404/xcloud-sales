import { Badge, Button, Form, Popover, Space, Table, Tag, Typography } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XRenderTime from '@/components/common/time/render_time';
import { PagedResponse } from '@/utils/biz';
import { McsBizTypeOptions, McsInvoicingStatus } from '@/utils/biz_3h';
import { qingdaoApiClient } from '@/utils/client';
import { dateTimeFormat, myDayjs } from '@/utils/dayjs';
import { UserInvoiceDto } from '@/utils/swagger';
import { McsInvoiceRequestDto, QueryMcsInvoiceRequestInput } from '@/utils/swagger_qingdao';
import { formatMoney, handleResponse } from '@/utils/utils';
import { ProLayout } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';

import XItems from './items';
import XSearch from './search';

const test: McsInvoiceRequestDto = {
  TotalAmount: 536,
  BizType: 'mcs',
  Status: 'pending',
  InvoicingTime: myDayjs().format(dateTimeFormat),
  Picked: false,
  PickedTime: myDayjs().format(dateTimeFormat),
  CreationTime: myDayjs().format(dateTimeFormat),
  Items: [
    {
      Price: 20,
      Description: '维修管道',
      McsBizOrderId: '654321321',
    },
  ],
};
const test_error: McsInvoiceRequestDto = {
  TotalAmount: 536,
  BizType: 'mcs',
  Status: 'failed' as McsInvoicingStatus,
  InvoicingTime: myDayjs().format(dateTimeFormat),
  Picked: false,
  PickedTime: myDayjs().format(dateTimeFormat),
  CreationTime: myDayjs().format(dateTimeFormat),
  Items: [
    {
      Price: 20,
      Description: '维修管道',
      McsBizOrderId: '654321321',
    },
  ],
};

export default () => {
  const [loading, _loading] = useState(false);
  const [datalist, _datalist] = useState<PagedResponse<McsInvoiceRequestDto>>({
    Items: [test, test_error],
  });
  const [query, _query] = useState<QueryMcsInvoiceRequestInput>({});

  const queryData = (q: QueryMcsInvoiceRequestInput) => {
    _loading(true);
    qingdaoApiClient.mall3H
      .mcsInvoiceRequestAdminQueryPaging({ ...q })
      .then((res) => {
        handleResponse(res, () => {
          _datalist(res.data || {});
        });
      })
      .finally(() => _loading(false));
  };

  const markAsPicked = (row: McsInvoiceRequestDto) => {
    if (!confirm('确定标记为已领取？')) {
      return;
    }

    _loading(true);
    qingdaoApiClient.mall3H
      .mcsInvoiceRequestAdminMarkAsPicked({ Id: row.Id })
      .then((res) => {
        handleResponse(res, () => {
          queryData({ ...query });
        });
      })
      .finally(() => _loading(false));
  };

  const markAsSuccess = (row: McsInvoiceRequestDto) => {
    if (!confirm('确定标记为开票成功？')) {
      return;
    }

    _loading(true);
    qingdaoApiClient.mall3H
      .mcsInvoiceRequestAdminMarkAsSuccess({ Id: row.Id })
      .then((res) => {
        handleResponse(res, () => {
          queryData({ ...query });
        });
      })
      .finally(() => _loading(false));
  };

  const markAsFailed = (row: McsInvoiceRequestDto) => {
    if (!confirm('确定标记为开票失败？')) {
      return;
    }

    _loading(true);
    qingdaoApiClient.mall3H
      .mcsInvoiceRequestAdminMarkAsFailed({ Id: row.Id })
      .then((res) => {
        handleResponse(res, () => {
          queryData({ ...query });
        });
      })
      .finally(() => _loading(false));
  };

  useEffect(() => {
    queryData(query);
  }, []);

  const renderUserInvoice = (x: UserInvoiceDto) => {
    const renderRow = (
      label: string,
      value: string | null | undefined | any,
    ) => {
      return (
        <>
          <Form.Item label={label}>
            <Typography.Text copyable>{value || '--'}</Typography.Text>
          </Form.Item>
        </>
      );
    };
    return (
      <>
        <div style={{ width: 500 }}>
          <Form labelCol={{ span: 4 }}>
            {renderRow('税号', x.Code)}
            {renderRow('抬头', x.Head)}
            {renderRow('银行编码', x.BankCode)}
            {renderRow('银行名称', x.BankName)}
            {renderRow('发票类型', x.InvoiceType)}
            {renderRow('联系电话', x.PhoneNumber)}
          </Form>
        </div>
      </>
    );
  };

  const renderStatus = (x: McsInvoiceRequestDto) => {
    const status = x.Status as McsInvoicingStatus;

    if (status == 'pending') {
      return <Tag color={'processing'}>开票中</Tag>;
    }
    if (status == 'success') {
      return <Tag color={'success'}>开票成功</Tag>;
    }
    if (status == 'failed') {
      return <Tag color={'error'}>开票失败</Tag>;
    }

    return <span>error status</span>;
  };

  const columns: ColumnType<McsInvoiceRequestDto>[] = [
    {
      title: '金额',
      render: (d, x) => (
        <Tag color={'warning'}>{formatMoney(x.TotalAmount || 0)}</Tag>
      ),
      width: 100,
      fixed: 'left',
    },
    {
      title: '业务类型',
      render: (d, x) => (
        <Tag>
          {McsBizTypeOptions.find((d) => d.id == x.BizType)?.name || '--'}
        </Tag>
      ),
    },
    {
      title: '开票信息',
      render: (d, x) => (
        <Popover
          title={'开票信息'}
          content={renderUserInvoice(x.UserInvoice || {})}
          trigger={['hover']}
        >
          <Button type="link">查看开票信息</Button>
        </Popover>
      ),
    },
    {
      title: '开票状态',
      render: (d, x) => renderStatus(x),
    },
    {
      title: '开票时间',
      render: (d, x) => <XRenderTime timeStr={x.InvoicingTime} />,
    },
    {
      title: '是否领取',
      render: (d, x) => (
        <Badge
          status={x.Picked ? 'success' : 'default'}
          text={x.Picked ? '已领取' : '未领取'}
        />
      ),
    },
    {
      title: '领取时间',
      render: (d, x) => <XRenderTime timeStr={x.PickedTime} />,
    },
    {
      title: '申请时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (d, x) => {
        return (
          <>
            <Space direction={'vertical'}>
              {(x.Status as McsInvoicingStatus) == 'pending' && (
                <Button
                  type={'link'}
                  onClick={() => {
                    markAsSuccess(x);
                  }}
                >
                  开票成功
                </Button>
              )}

              {(x.Status as McsInvoicingStatus) == 'pending' && (
                <Button
                  type={'link'}
                  danger
                  onClick={() => {
                    markAsFailed(x);
                  }}
                >
                  开票失败
                </Button>
              )}

              {(x.Status as McsInvoicingStatus) == 'success' &&
                !(x.Picked || false) && (
                  <Button
                    type={'link'}
                    onClick={() => {
                      markAsPicked(x);
                    }}
                  >
                    标记为已领取
                  </Button>
                )}
            </Space>
          </>
        );
      },
    },
  ];

  return (
    <>
      <ProLayout pure>
        <PageContainer>
          <h1>发票申请受理</h1>
          <XSearch
            query={query}
            onChange={(e) => {
              const q: QueryMcsInvoiceRequestInput = {
                ...e,
                Page: 1,
              };
              queryData(q);
              _query(q);
            }}
          />
          <Table
            loading={loading}
            columns={columns}
            dataSource={datalist.Items || []}
            expandable={{
              expandedRowRender: (x) => <XItems items={x.Items || undefined} />,
            }}
            pagination={{
              current: datalist.PageIndex || undefined,
              total: datalist.TotalCount || undefined,
              pageSize: datalist.PageSize || undefined,
            }}
          />
        </PageContainer>
      </ProLayout>
    </>
  );
};
