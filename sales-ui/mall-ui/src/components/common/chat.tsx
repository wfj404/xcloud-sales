import { List } from 'antd-mobile';
import { ReactNode, useState } from 'react';

import XLoading from '@/components/common/loading/dots';
import XRenderTime from '@/components/common/time/render_time';
import { isStringEmpty } from '@/utils/utils';
import { SendOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Stack, TextField } from '@mui/material';

export interface ChatItem {
  left?: boolean;
  content?: ReactNode;
  time?: string;
  avatar?: ReactNode;
  nick_name?: ReactNode;
}

export default ({
  datalist,
  onSend,
  loading,
  loadingSend,
}: {
  datalist?: ChatItem[];
  onSend?: (e: string) => void;
  loading?: boolean;
  loadingSend?: boolean;
}) => {
  const [content, _content] = useState('');

  const renderChatItem = (x: ChatItem) => {
    return (
      <>
        <List.Item
          prefix={x.avatar}
          description={x.content}
          extra={<XRenderTime timeStr={x.time} />}
        >
          {x.nick_name || '--'}
        </List.Item>
      </>
    );
  };

  const renderDataList = () => {
    if (loading) {
      return <XLoading />;
    }
    return (
      <List>
        {datalist?.map((x, i) => {
          return <div key={i}>{renderChatItem(x)}</div>;
        })}
      </List>
    );
  };

  return (
    <>
      <div>
        {renderDataList()}
        <div style={{ marginTop: 10 }}>
          <form
            onSubmit={(e) => {
              e.preventDefault();
              onSend && onSend(content);
              _content('');
            }}
          >
            <Stack direction={'row'} spacing={1}>
              <div style={{ flexGrow: 1 }}>
                <TextField
                  sx={{
                    width: '100%',
                  }}
                  variant={'outlined'}
                  placeholder={'请输入...'}
                  multiline
                  rows={2}
                  value={content}
                  onChange={(e) => {
                    _content(e.target.value);
                  }}
                />
              </div>
              <LoadingButton
                loading={loading || loadingSend}
                type="submit"
                variant="contained"
                color="primary"
                disabled={isStringEmpty(content)}
                endIcon={<SendOutlined />}
              >
                发送
              </LoadingButton>
            </Stack>
          </form>
        </div>
      </div>
    </>
  );
};
