import { ReactNode } from 'react';

import { useDroppable } from '@dnd-kit/core';

export default function Droppable(props: { children: ReactNode }) {
  const { isOver, setNodeRef } = useDroppable({
    id: 'droppable',
  });
  const style = {
    color: isOver ? 'green' : undefined,
  };

  return (
    <div
      ref={setNodeRef}
      style={{
        ...style,
        padding: 30,
        margin: 30,
        border: '1px solid red',
      }}
    >
      drop here
      {props.children}
    </div>
  );
}
