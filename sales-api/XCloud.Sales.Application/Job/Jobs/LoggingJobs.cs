using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Logging;

namespace XCloud.Sales.Application.Job.Jobs;

[UnitOfWork]
public class LoggingJobs : SalesAppService
{
    private readonly IActivityLogService _activityLogService;

    public LoggingJobs(IActivityLogService activityLogService)
    {
        _activityLogService = activityLogService;
        //
    }

    public virtual async Task TriggerCleanExpiredLogsAsync()
    {
        await _activityLogService.ClearExpiredDataWithLockAsync(Clock.Now.AddMonths(-3));
    }
}