﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreManagerPermissionService : ISalesAppService
{
    Task<StoreManagerGrantedPermissionResponse> GetGrantedPermissionsAsync(string managerId,
        CacheStrategy cacheStrategy);
}

public class StoreManagerPermissionService : SalesAppService, IStoreManagerPermissionService
{
    private readonly ISalesRepository<StoreRole> _repository;
    private readonly IStoreManagerService _storeManagerService;

    public StoreManagerPermissionService(ISalesRepository<StoreRole> repository,
        IStoreManagerService storeManagerService)
    {
        _repository = repository;
        _storeManagerService = storeManagerService;
    }

    private async Task<StoreManagerGrantedPermissionResponse> GetGrantedPermissionsAsync(string managerId)
    {
        var response = new StoreManagerGrantedPermissionResponse();

        var manager = await this._storeManagerService.GetByIdAsync(managerId);

        if (manager == null)
        {
            return response.SetError(nameof(manager));
        }

        response.SetData(manager);

        var db = await this._repository.GetDbContextAsync();

        var query = from rolePermission in db.Set<StoreRolePermission>().AsNoTracking()
            join managerRole in db.Set<StoreManagerRole>().AsNoTracking()
                on rolePermission.RoleId equals managerRole.RoleId
            select new { managerRole.ManagerId, rolePermission.PermissionKey };

        query = query.Where(x => x.ManagerId == managerId);

        var keys = await query
            .Select(x => x.PermissionKey)
            .Distinct()
            .ToArrayAsync();

        response.Permissions = keys;

        return response;
    }

    public async Task<StoreManagerGrantedPermissionResponse> GetGrantedPermissionsAsync(string managerId,
        CacheStrategy cacheStrategy)
    {
        if (string.IsNullOrWhiteSpace(managerId))
            throw new ArgumentNullException(nameof(managerId));

        var key = $"store.manager.{managerId}.permissions";
        var option =
            new CacheOption<StoreManagerGrantedPermissionResponse>(key: key, expiration: TimeSpan.FromMinutes(3));

        var response = await this.CacheProvider.ExecuteWithPolicyAsync(
            () => this.GetGrantedPermissionsAsync(managerId),
            option, cacheStrategy);

        return response ?? new StoreManagerGrantedPermissionResponse();
    }
}