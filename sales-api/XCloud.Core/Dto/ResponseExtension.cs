﻿using Volo.Abp;
using Volo.Abp.Http;

namespace XCloud.Core.Dto;

public static class ResponseExtension
{
    public static bool HasError(this RemoteServiceErrorResponse response) => response.Error != null;

    public static bool HasNoError(this RemoteServiceErrorResponse response) => !response.HasError();

    public static void ThrowIfErrorOccured<ExceptionType>(this RemoteServiceErrorResponse response,
        Func<RemoteServiceErrorInfo, ExceptionType> ex)
        where ExceptionType : Exception
    {
        var error = response.Error;
        if (error != null)
        {
            throw ex.Invoke(error);
        }
    }

    public static void ThrowIfErrorOccured(this RemoteServiceErrorResponse response)
    {
        ThrowIfErrorOccured(response,
            error => new BusinessException(message: error.Message, code: error.Code).WithData("error", error));
    }

    public static ApiResponse<T> SetData<T>(this ApiResponse<T> response, T data)
    {
        response.Data = data;

        return response;
    }

    public static TResponse SetError<TResponse>(this TResponse response, string message,
        string code = default,
        string details = default)
        where TResponse : RemoteServiceErrorResponse
    {
        response.Error = new RemoteServiceErrorInfo(message: message, code: code, details: details);

        return response;
    }
}