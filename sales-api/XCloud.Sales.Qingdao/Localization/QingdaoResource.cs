﻿using Volo.Abp.Localization;

namespace XCloud.Sales.Qingdao.Localization;

[LocalizationResourceName(nameof(QingdaoResource))]
public class QingdaoResource
{
    //
}