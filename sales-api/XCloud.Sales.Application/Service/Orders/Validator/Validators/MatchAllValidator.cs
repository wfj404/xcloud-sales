﻿namespace XCloud.Sales.Application.Service.Orders.Validator.Validators;

[ExposeServices(typeof(IOrderValidator))]
public class MatchAllValidator : IOrderValidator, IScopedDependency
{
    public int Order => int.MaxValue;

    public string ValidatorType => OrderConditionValidatorTypes.All;

    public Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule)
    {
        return Task.FromResult(rule.ValidatorType == ValidatorType);
    }

    public async Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule)
    {
        await Task.CompletedTask;
        return rule.Description ?? string.Empty;
    }

    public Task<OrderConditionValidationResponse> IsConditionMatchedAsync(OrderConditionCheckDto input,
        OrderValidatorRule rule)
    {
        var response = new OrderConditionValidationResponse
        {
            Error = default
        };

        return Task.FromResult(response);
    }
}