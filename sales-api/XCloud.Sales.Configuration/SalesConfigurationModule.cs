﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Volo.Abp;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Logging;
using XCloud.AspNetMvc.Configuration;
using XCloud.AspNetMvc.Ocelot;
using XCloud.AspNetMvc.Spa;
using XCloud.Core.Configuration;
using XCloud.Core.Http.Dynamic;
using XCloud.Platform.HttpApi;
using XCloud.Platform.Messenger.Abstraction.WebSocketImpl;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Localization;
using XCloud.Sales.Configuration.Controllers;
using XCloud.Sales.HttpApi;
using XCloud.Sales.WeChat;

namespace XCloud.Sales.Configuration;

[DependsOn(
    //platform
    typeof(PlatformHttpApiModule),
    //sales
    typeof(SalesHttpApiModule),
    //optional modules--------------------------------------
    //typeof(XCloud.Sales.ElasticSearch.SalesElasticSearchModule),
    typeof(SalesWeChatModule)
)]
[MethodMetric]
public class SalesConfigurationModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        //
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        var builder = context.Services.GetRequiredXCloudBuilder();
        builder.AddSwagger(SalesConstants.ServiceName, SalesConstants.ServiceName);
        builder.AddDynamicHttpClient<IPostService>();

        context.ConfigOcelotGateway();

        context.AddHealthCheckConfig();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        this.Configure<MvcOptions>(option => { option.Filters.AddService<StoreAuditLoggingFilter>(); });

        context.Services.Configure<AbpLocalizationOptions>(options =>
        {
            options.Languages.Clear();
            options.Languages.Add(new LanguageInfo("en", "en", "English"));
            options.Languages.Add(new LanguageInfo("zh-Hans", "zh-Hans", "简体中文"));

            options.DefaultResourceType = typeof(SalesResource);
        });
    }

    public override void OnPreApplicationInitialization(ApplicationInitializationContext context)
    {
        var config = context.GetConfiguration();
        if (config.SkipAutoPipelineConfig())
        {
            return;
        }

        var app = context.GetApplicationBuilder();
        var env = context.GetEnvironment();

        //use spa page
        context.UseSpaStaticFile("/sales");

        //basic middleware
        app.UseStaticFiles();
        app.UseRouting();
        app.TryUseCors();

        //dev stuff
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseDevelopmentInformation();
        }

        //browser info
        app.UseCorrelationId();
        app.UseAbpRequestLocalization(option =>
        {
            option.FallBackToParentCultures = true;
            option.FallBackToParentUICultures = true;
            option.DefaultRequestCulture = new RequestCulture("zh-Hans");
        });

        //auth
        app.UseAuthentication();
        app.UseAuthorization();

        //logging
        app.UseMiddleware<AspNetCoreRequestLoggingMiddleware>();

        //uow
        app.UseUnitOfWork();

        //swagger
        context.TryEnableSwagger();
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var config = context.GetConfiguration();
        if (config.SkipAutoPipelineConfig())
        {
            return;
        }

        var app = context.GetApplicationBuilder();
        //web socket
        app.UseWebSockets();
        app.TryStartMessengerServer();
        app.UseWebSocketEndpoint($"/api/messenger/ws");
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var config = context.GetConfiguration();
        if (config.SkipAutoPipelineConfig())
        {
            return;
        }

        var app = context.GetApplicationBuilder();

        app.UseEndpoints(endpoints =>
        {
            //endpoints.MapRazorPages();
            endpoints.MapDefaultControllerRoute();
            endpoints.UseHealthCheckEndpointAndUi();
        });

        //reg ocelot pipeline
        //Task.Run(() => app.UseOcelot()).Wait();
    }
}