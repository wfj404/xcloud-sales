using Volo.Abp.Application.Dtos;
using XCloud.Application.Utils;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Qingdao.Service;

[TypeIdentityName("mcs-service-order")]
public class McsCustomerServiceOrder : ICustomerOrder
{
    //
}

public class WapResponse<T> : IEntityDto
{
    public string message { get; set; }

    public int code { get; set; }

    public int statusCode { get; set; }

    public T data { get; set; }
}

public class WapPermission : IEntityDto
{
    public string authName { get; set; }

    public string authCode { get; set; }

    public bool authorized { get; set; }
}

public class WapOrganization : IEntityDto
{
    public int organizationId { get; set; }

    public int parentOrganizationId { get; set; }

    public string organizationType { get; set; }

    public string organizationKey { get; set; }

    public string organizationCode { get; set; }

    public string organizationName { get; set; }
}

public class WapAdminDto : IEntityDto
{
    public int userId { get; set; }

    public string userName { get; set; }

    public bool active { get; set; }

    public WapOrganization organization { get; set; }
}

public class McsError : IEntityDto
{
    public string message { get; set; }

    public string details { get; set; }
}

public class McsUserDto : IEntityDto
{
    public string headImgUrl { get; set; }

    public string mcsUserId { get; set; }

    public string nickName { get; set; }
}

public class McsResponse<T> : IEntityDto
{
    public int code { get; set; }

    public string message { get; set; }

    public McsError error { get; set; }

    public T data { get; set; }
}