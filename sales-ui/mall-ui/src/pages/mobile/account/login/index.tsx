import { useTitle } from 'ahooks';
import { trimStart } from 'lodash-es';
import qs from 'query-string';
import React, { useEffect, useState } from 'react';
import { history, useLocation, useSnapshot } from 'umi';

import loginPic from '@/assets/static/illustrations/illustration_login.png';
import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { PasswordLoginDto } from '@/utils/swagger';
import { errorMsg, isStringEmpty, setAccessToken, successMsg } from '@/utils/utils';
import { LockOpenOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Box, Button, Container, Paper, Stack, TextField, Typography } from '@mui/material';

import XConnect from './connect';

export default function () {
  useTitle('登录账号~');

  const [data, _data] = useState<PasswordLoginDto>({});
  const [loginLoading, _loginLoading] = useState(false);

  const location = useLocation();

  const next: any = qs.parse(trimStart(location.search, '?'))?.next || '/';

  const app = useSnapshot(storeState) as StoreType;

  useEffect(() => {
    console.log('login page,location:', location, next);
    localStorage.setItem('login_next', next);
  }, []);

  const handleSubmit = () => {
    if (isStringEmpty(data.IdentityName) || isStringEmpty(data.Password)) {
      errorMsg('请输入完整的用户名和密码');
      return;
    }

    _loginLoading(true);
    apiClient.platform
      .authPasswordLogin({ ...data })
      .then((res) => {
        if (res.data.Error) {
          errorMsg(res.data.Error?.Message || '');
        } else {
          setAccessToken(res.data.Data?.AccessToken || '');

          successMsg('登录成功');

          setTimeout(() => {
            console.log(next);
            window.location.href = next;
          }, 1000);
        }
      })
      .finally(() => {
        _loginLoading(false);
      });
  };

  return (
    <>
      <Container maxWidth="sm" disableGutters>
        <Paper
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            p: 2,
          }}
          elevation={0}
        >
          <Stack spacing={2} direction="column" alignItems={'center'}>
            <img src={loginPic} width={200} alt={'login'} />
            <Typography component="h1" variant="h5">
              登录
            </Typography>
          </Stack>
          <Box sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              label="账号"
              name="email"
              value={data.IdentityName}
              onChange={(e) => _data({ ...data, IdentityName: e.target.value })}
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="密码"
              type="password"
              value={data.Password}
              onChange={(e) => _data({ ...data, Password: e.target.value })}
              autoComplete="current-password"
            />
            <LoadingButton
              loading={loginLoading}
              startIcon={<LockOpenOutlined />}
              fullWidth
              variant="contained"
              size="large"
              sx={{ mt: 5, mb: 2 }}
              onClick={() => handleSubmit()}
            >
              登录
            </LoadingButton>
            <Button
              variant="text"
              fullWidth
              onClick={() => {
                history.push({
                  pathname: '/',
                });
              }}
            >
              返回
            </Button>
            <XConnect />
          </Box>
          {isStringEmpty(app.mallSettings.LoginNotice) || (
            <Box sx={{ mt: 2 }}>
              <Typography variant="overline" color="primary">
                {app.mallSettings.LoginNotice}
              </Typography>
            </Box>
          )}
        </Paper>
      </Container>
    </>
  );
}
