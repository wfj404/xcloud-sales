using XCloud.Application.Mapper;
using XCloud.Core.Helper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.ShoppingCart;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.ShoppingCart;

public interface IShoppingCartService : ISalesAppService
{
    Task<ShoppingCartItemDto[]> AttachWarningsAsync(ShoppingCartItemDto[] data);

    Task<ShoppingCartItemDto[]> AttachGoodsAsync(ShoppingCartItemDto[] data);

    Task<SkuDto[]> AttachShoppingCartQuantityAsync(int userId, SkuDto[] data);

    Task DeleteBySkuIdsAsync(RemoveCartBySpecs dto);

    Task AddOrUpdateShoppingCartAsync(AddOrUpdateShoppingCartInput dto);

    Task UpdateShoppingCartQuantityAsync(int shoppingCartItemId, int newQuantity);

    Task DeleteShoppingCartAsync(int[] ids);

    Task<ShoppingCartItemDto[]> QueryUserShoppingCartAsync(int userId);
}

public class ShoppingCartService : SalesAppService, IShoppingCartService
{
    private readonly ISalesRepository<ShoppingCartItem> _repository;
    private readonly ISkuService _skuService;

    public ShoppingCartService(ISalesRepository<ShoppingCartItem> sciRepository, ISkuService skuService)
    {
        _repository = sciRepository;
        _skuService = skuService;
    }

    public async Task<ShoppingCartItemDto[]> AttachGoodsAsync(ShoppingCartItemDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.SkuId).WhereNotEmpty().Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var query = from cart in db.Set<ShoppingCartItem>().AsNoTracking()
            join spec in db.Set<Sku>().AsNoTracking()
                on cart.SkuId equals spec.Id
            select new { spec, cart };

        var list = await query.Where(x => ids.Contains(x.spec.Id)).ToArrayAsync();

        foreach (var m in data)
        {
            var item = list.FirstOrDefault(x => x.cart.Id == m.Id);
            if (item == null)
                continue;

            m.Sku = item.spec.MapTo<Sku, SkuDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<SkuDto[]> AttachShoppingCartQuantityAsync(int userId, SkuDto[] data)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<ShoppingCartItem>().AsNoTracking().Where(x => x.UserId == userId);

        var shoppingCarts = await query.Where(x => ids.Contains(x.SkuId)).ToArrayAsync();

        foreach (var m in data)
        {
            var q = shoppingCarts.Where(x => x.SkuId == m.Id);
            m.ShoppingCartQuantity = q.Sum(x => x.Quantity);
        }

        return data;
    }

    public async Task DeleteBySkuIdsAsync(RemoveCartBySpecs dto)
    {
        if (dto.UserId <= 0 || ValidateHelper.IsEmptyCollection(dto.SkuId))
            return;

        var db = await _repository.GetDbContextAsync();

        var set = db.Set<ShoppingCartItem>();

        var data = await set
            .Where(x => x.UserId == dto.UserId)
            .OrderByDescending(x => x.CreationTime)
            .ToArrayAsync();

        var toRemove = data.Where(x => dto.SkuId.Contains(x.SkuId)).ToArray();

        if (toRemove.Any())
        {
            set.RemoveRange(toRemove);

            var valueAddedSet = db.Set<ShoppingCartValueAddedItem>();

            var itemIds = toRemove.Ids();

            var range = valueAddedSet.Where(x => itemIds.Contains(x.ShoppingCartItemId));

            valueAddedSet.RemoveRange();
        }

        await db.TrySaveChangesAsync();
    }

    public async Task<ShoppingCartItemDto[]> AttachWarningsAsync(ShoppingCartItemDto[] data)
    {
        if (!data.Any())
            return data;

        await Task.CompletedTask;

        foreach (var m in data)
        {
            var warningList = new List<string>();

            if (m.Sku == null || m.Sku.IsDeleted || !m.Sku.IsActive)
            {
                warningList.Add("sku is not on sale");
            }
            else if (m.Sku.Goods == null || m.Sku.Goods.IsDeleted)
            {
                warningList.Add("goods is not available");
            }
            else
            {
                if (m.Sku.StockModel == null)
                {
                    warningList.Add("stock is not available");
                }
                else if (m.Sku.StockModel.StockQuantity < m.Quantity)
                {
                    warningList.Add("short of stock");
                }
            }

            m.Waring = warningList.ToArray();
        }

        return data;
    }

    public async Task<ShoppingCartItemDto[]> QueryUserShoppingCartAsync(int userId)
    {
        var db = await _repository.GetDbContextAsync();

        var data = await db.Set<ShoppingCartItem>().AsNoTracking()
            .Where(x => x.UserId == userId)
            .OrderBy(x => x.CreationTime)
            .ToArrayAsync();

        return data.MapArrayTo<ShoppingCartItem, ShoppingCartItemDto>(ObjectMapper);
    }

    public async Task DeleteShoppingCartAsync(int[] ids)
    {
        if (ValidateHelper.IsEmptyCollection(ids))
            return;

        var db = await _repository.GetDbContextAsync();

        var set = db.Set<ShoppingCartItem>();

        var data = await set
            .Where(x => ids.Contains(x.Id))
            .OrderByDescending(x => x.CreationTime)
            .ToArrayAsync();

        if (data.Any())
        {
            set.RemoveRange(data);
        }

        await db.TrySaveChangesAsync();
    }

    private async Task SaveValueAddedItemsAsync(int shoppingCartId, int[] valueAddedItemIds)
    {
        if (shoppingCartId <= 0)
            throw new ArgumentNullException(nameof(shoppingCartId));

        if (valueAddedItemIds == null)
            throw new ArgumentNullException(nameof(valueAddedItemIds));

        var entities = valueAddedItemIds.Select(x => new ShoppingCartValueAddedItem
        {
            ShoppingCartItemId = shoppingCartId,
            ValueAddedItemId = x
        }).ToArray();

        var db = await _repository.GetDbContextAsync();

        var set = db.Set<ShoppingCartValueAddedItem>();

        var originItems = await set.Where(x => x.ShoppingCartItemId == shoppingCartId).ToArrayAsync();

        string FingerPrint(ShoppingCartValueAddedItem x) => $"{x.ShoppingCartItemId}.{x.ValueAddedItemId}";

        var deleteList = originItems.NotInBy(entities, FingerPrint).ToArray();
        var insertList = entities.NotInBy(originItems, FingerPrint).ToArray();

        if (deleteList.Any())
        {
            set.RemoveRange(deleteList);
        }

        if (insertList.Any())
        {
            set.AddRange(insertList);
        }

        await db.TrySaveChangesAsync();
    }

    public async Task AddOrUpdateShoppingCartAsync(AddOrUpdateShoppingCartInput dto)
    {
        if (dto.Quantity <= 0)
            throw new ArgumentNullException(nameof(dto.Quantity));

        var sku = await this._skuService.GetByIdAsync(dto.SkuId);

        if (sku == null)
            throw new EntityNotFoundException("goods sku not found");

        var db = await _repository.GetDbContextAsync();

        var set = db.Set<ShoppingCartItem>();

        var cart = await set
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync(x => x.UserId == dto.UserId && x.SkuId == sku.Id);

        if (cart == null)
        {
            cart = new ShoppingCartItem
            {
                Id = default,
                UserId = dto.UserId,
                SkuId = sku.Id,
                Quantity = dto.Quantity,
                CreationTime = Clock.Now,
            };

            await this._repository.InsertAsync(cart);
        }
        else
        {
            cart.Quantity = dto.Quantity;
            cart.Quantity = Math.Max(cart.Quantity, 1);
            cart.LastModificationTime = Clock.Now;

            await this._repository.UpdateAsync(cart);
        }

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        if (dto.ValueAddedItemIds != null)
        {
            await SaveValueAddedItemsAsync(cart.Id, dto.ValueAddedItemIds);
        }
    }

    public virtual async Task UpdateShoppingCartQuantityAsync(
        int shoppingCartItemId,
        int newQuantity)
    {
        var item = await this._repository.GetRequiredByIdAsync(shoppingCartItemId);

        if (item == null)
            throw new EntityNotFoundException(nameof(item));

        var sku = await this._skuService.GetByIdAsync(item.SkuId);

        if (sku == null)
            throw new EntityNotFoundException(nameof(sku));

        item.Quantity = Math.Max(1, newQuantity);
        item.LastModificationTime = Clock.Now;

        await this._repository.UpdateAsync(item);
    }
}