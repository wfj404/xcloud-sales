import { useRequest } from 'ahooks';
import { Badge, Button, Space, Tag } from 'antd';
import { useEffect, useState } from 'react';
import { history } from 'umi';

import ImgPreview from '@/components/common/preview_group';
import XTime from '@/components/common/time';
import { isSh3H, sortGoodsPictures } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { GoodsDto, QueryGoodsPaging } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { ProColumns, ProTable } from '@ant-design/pro-components';

import XEdit from './form';
import XSearchForm from './search_form';
import XSkuList from './sku_list';

export default () => {
  const queryGoodsRequest = useRequest(
    apiClient.mallAdmin.goodsQueryGoodsPaging,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {});
      },
    },
  );

  const data = queryGoodsRequest.data?.data || {};
  const items = data.Items || [];
  const loading = queryGoodsRequest.loading;

  const [query, _query] = useState<QueryGoodsPaging>({
    Page: 1,
  });
  const [editId, _editId] = useState<string | undefined>(undefined);
  const [expandedKeys, _expandedKeys] = useState<string[]>([]);

  const columns: ProColumns<GoodsDto>[] = [
    {
      title: '名称',
      render: (d, x) => (
        <>
          <div>
            <a href={`/sales/goods/${x.Id}`} target="blank">
              {x.Name}
            </a>
          </div>
          <div>{x.Skus?.map((d) => d.Name).join(',')}</div>
        </>
      ),
    },
    {
      title: '基础信息',
      render: (d, x) => (
        <>
          <div>品牌:{x.Brand?.Name || '--'}</div>
          <div>分类:{x.Category?.Name || '--'}</div>
        </>
      ),
    },
    {
      title: '标记',
      render: (d, x) => {
        return (
          <Space direction="vertical">
            {x.StickyTop && <Tag color="red">置顶</Tag>}
          </Space>
        );
      },
    },
    {
      title: '状态',
      render: (d, x) => {
        return (
          <>
            {x.Published && <Badge status={'success'} text={'可用'} />}
            {x.Published || <Badge status={'error'} text={'不可用'} />}
          </>
        );
      },
    },
    {
      title: '图片',
      render: (d, x) => {
        const sortedPictures = sortGoodsPictures(x.GoodsPictures || []).map(
          (x) => x.StorageMeta || {},
        );
        return (
          <div
            style={{
              maxWidth: 300,
            }}
          >
            <ImgPreview count={3} data={sortedPictures} />
          </div>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 150,
      fixed: 'right',
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _editId(x.Id || undefined);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryGoodsRequest.run(query);
  }, []);

  const renderExpandButton = () => {
    if (items.length <= 0) {
      return null;
    }
    if (expandedKeys.length > 0) {
      return (
        <Button
          type="link"
          onClick={() => {
            _expandedKeys([]);
          }}
        >
          隐藏所有sku
        </Button>
      );
    } else {
      return (
        <Button
          type="link"
          onClick={() => {
            _expandedKeys(data.Items?.map((x) => x.Id || '') || []);
          }}
        >
          展开所有sku
        </Button>
      );
    }
  };

  return (
    <>
      <XEdit
        goodsId={editId}
        show={editId != undefined}
        hide={() => {
          _editId(undefined);
          queryGoodsRequest.run(query);
        }}
        ok={(x) => {
          console.log(x);
          _editId(undefined);
          queryGoodsRequest.run(query);
        }}
      />
      <XSearchForm
        query={query}
        onSearch={(q: QueryGoodsPaging) => {
          queryGoodsRequest.mutate();
          _query(q);
          queryGoodsRequest.run(q);
        }}
      />
      <ProTable
        toolBarRender={() => [
          <Button
            style={{
              visibility: isSh3H() ? 'hidden' : 'visible',
            }}
            type={'dashed'}
            onClick={() => {
              history.push({
                pathname: '/admin/catalog/sku-batch-update-price',
              });
            }}
          >
            批量修改价格
          </Button>,
          renderExpandButton(),
          <Button
            type="primary"
            onClick={() => {
              _editId('');
            }}
          >
            新增
          </Button>,
        ]}
        rowKey={(x) => x.Id || ''}
        loading={loading}
        columns={columns}
        options={false}
        search={false}
        dataSource={items}
        expandable={{
          expandedRowKeys: expandedKeys,
          onExpandedRowsChange(expandedKeys) {
            _expandedKeys(expandedKeys as any);
          },
          expandedRowRender: (x: GoodsDto) => {
            return (
              <div style={{ padding: 10, border: '5px dashed orange' }}>
                <XSkuList
                  goods={x}
                  items={x.Skus || []}
                  ok={() => {
                    queryGoodsRequest.run(query);
                  }}
                />
              </div>
            );
          },
        }}
        pagination={{
          showSizeChanger: false,
          pageSize: 20,
          current: query.Page,
          total: data.TotalCount,
          onChange: (e) => {
            queryGoodsRequest.mutate();
            const q: QueryGoodsPaging = {
              ...query,
              Page: e,
            };
            _query(q);
            queryGoodsRequest.run(q);
          },
        }}
      />
    </>
  );
};
