﻿using System.Text;
using Microsoft.Extensions.Logging;
using XCloud.Core.Json;

namespace XCloud.Sales.ThirdParty.Services.KuaiMai;

public static class KuaiMaiExtension
{
    public static void ThrowIfException<T>(this KuaiMaiResponse<T> response, ILogger logger,
        IJsonDataSerializer jsonDataSerializer)
    {
        if (!response.status)
        {
            var sb = new StringBuilder();

            sb.AppendLine(response.GetType().FullName);
            sb.AppendLine(jsonDataSerializer.SerializeToString(response));

            logger.LogWarning(message: sb.ToString());
        }
    }
}