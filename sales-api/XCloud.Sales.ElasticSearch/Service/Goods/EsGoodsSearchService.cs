using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Search;

namespace XCloud.Sales.ElasticSearch.Service.Goods;

[Dependency(ReplaceServices = true)]
public class EsGoodsSearchService : SalesAppService//, IGoodsSearchService
{
    public Task UpdateGoodsKeywordsAsync(string goodsId)
    {
        throw new System.NotImplementedException();
    }

    public Task<Tag[]> QueryRelatedTagsAsync(QueryGoodsPaging dto)
    {
        throw new System.NotImplementedException();
    }

    public Task<Brand[]> QueryRelatedBrandsAsync(QueryGoodsPaging dto)
    {
        throw new System.NotImplementedException();
    }

    public Task<Category[]> QueryRelatedCategoriesAsync(QueryGoodsPaging dto)
    {
        throw new System.NotImplementedException();
    }

    public Task<PagedResponse<GoodsDto>> SearchGoodsV2Async(QueryGoodsPaging dto)
    {
        throw new System.NotImplementedException();
    }

    public Task<PagedResponse<SkuDto>> QuerySkuPagingAsync(
        QuerySkuPaging dto)
    {
        throw new System.NotImplementedException();
    }
}