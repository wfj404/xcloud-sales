namespace XCloud.Sales.Application.Domain.Shipping;

public class DeliveryRecordItem : SalesBaseEntity<string>
{
    public string DeliveryId { get; set; }

    public string OrderItemId { get; set; }

    public int Quantity { get; set; }
}