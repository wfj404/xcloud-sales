﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;
using XCloud.EntityFrameworkCore.MySql;
using XCloud.Platform.Application.Repository;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Extended.Database.EntityFrameworkCore;
using XCloud.Sales.Application.Extended.Database.Repository;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Extended.Database;

public static class DatabaseConfigurationExtension
{
    public static void ConfigureExtendedMysql(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        context.Services.AddAbpDbContext<ShopMySqlDbContext>(option =>
        {
            //add default repository
            option.AddDefaultRepositories(true);
        });

        context.Services.Configure<AbpDbContextOptions>(contextOption =>
        {
            contextOption.Configure<ShopMySqlDbContext>(option =>
            {
                option.DbContextOptions.UseMySqlProvider(config, SalesConstants.DbConnectionNames.MySql);
                option.DbContextOptions.EnableDetailedErrors();
                option.DbContextOptions.EnableSensitiveDataLogging();
                option.DbContextOptions.EnableThreadSafetyChecks();
            });
        });

        //add repository
        context.Services.AddScoped(typeof(IPlatformRepository<>), typeof(PlatformMySqlRepository<>));
        context.Services.AddScoped(typeof(ISalesRepository<>), typeof(SalesMySqlRepository<>));
    }
}