﻿using System.Reflection;
using System.Threading.Tasks;
using Volo.Abp.Modularity;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Configuration;
using XCloud.Core.Dto;

namespace XCloud.Core.Development.Builtin;

[ExposeServices(typeof(IDevelopmentInformationProvider))]
public class ModuleDependencyDevInfoProvider : IDevelopmentInformationProvider, ITransientDependency
{
    private const int MaxDeep = 5;

    private readonly IXCloudBuilder _builder;

    public ModuleDependencyDevInfoProvider(IXCloudBuilder builder)
    {
        _builder = builder;
    }

    public string ProviderName => $"模块依赖关系,一共显示{MaxDeep}层";

    private IEnumerable<TreeNode> FindDependedAssemblies(Type entry, int maxDeep)
    {
        if (entry == null)
            throw new ArgumentNullException(nameof(entry));

        IEnumerable<TreeNode> Find(Type t, int parentDeep)
        {
            //检查有没有历遍过
            var currentDeep = parentDeep + 1;
            if (currentDeep <= maxDeep)
            {
                var node = new TreeNode
                {
                    Name = t.Assembly.GetName()?.Name,
                };

                var dependOn = t.GetCustomAttributes<DependsOnAttribute>().ToArray();
                if (dependOn.Any())
                {
                    var children = dependOn
                        .SelectMany(x => x.DependedTypes)
                        .SelectMany(x => Find(x, currentDeep))
                        .ToArray();

                    node.Children = children;
                }

                yield return node;
            }
        }

        return Find(entry, 0);
    }

    public async Task<object> GetInformationAsync()
    {
        await Task.CompletedTask;
        return FindDependedAssemblies(_builder.EntryModuleType, maxDeep: MaxDeep);
    }
}