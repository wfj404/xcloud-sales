﻿namespace XCloud.Sales.Application.Domain.Common;

public class OrderGroup : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime
{
    public string Name { get; set; }

    public string Description { get; set; }

    public string StoreId { get; set; }

    public string OrderDataJson { get; set; }

    public int RowVersion { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }
}