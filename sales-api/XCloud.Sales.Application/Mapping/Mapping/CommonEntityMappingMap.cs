using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Service.Mapping;

namespace XCloud.Sales.Application.Mapping.Mapping;

public class CommonEntityMappingMap : SalesEntityTypeConfiguration<CommonEntityMapping>
{
    public override void Configure(EntityTypeBuilder<CommonEntityMapping> builder)
    {
        builder.ToTable("sales_common_entity_mapping");

        builder.PreConfigIntPrimaryKey();
        builder.PreConfigEntityMappingEntity();

        base.Configure(builder);
    }
}