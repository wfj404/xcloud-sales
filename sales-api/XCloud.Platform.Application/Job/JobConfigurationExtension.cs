using System.Threading.Tasks;
using Hangfire;
using XCloud.Platform.Application.Job.Jobs;

namespace XCloud.Platform.Application.Job;

public static class JobConfigurationExtension
{
    internal static async Task StartPlatformJobsAsync(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var recurringJobManager = s.ServiceProvider.GetRequiredService<IRecurringJobManager>();

        recurringJobManager.AddOrUpdate<ExternalAccessTokenJob>("clean-expired-access-token",
            x => x.CleanExpiredAccessTokenAsync(),
            Cron.Hourly(1));

        recurringJobManager.AddOrUpdate<AutoResetUserPasswordJob>("auto-reset-default-user-password",
            x => x.ResetDefaultUserPasswordAsync(),
            Cron.Minutely());

        recurringJobManager.AddOrUpdate<EnsureSuperUserCreatedJob>("ensure-super-user-created",
            x => x.EnsureSuperUserCreatedAsync(), Cron.Hourly());

        var jobClient = s.ServiceProvider.GetRequiredService<IBackgroundJobClient>();
        jobClient.Schedule<AutoImportStorageJob>(x => x.RunImportAsync(),
            delay: TimeSpan.FromSeconds(30));

        await Task.CompletedTask;
    }
}