import { IHasDeletion } from '@/utils/biz';
import { ReactElement } from 'react';

export default ({ model, children, deletedNotice }: {
  model: IHasDeletion,
  deletedNotice?: ReactElement,
  children: ReactElement
}) => {

  if (model == null || model.IsDeleted) {
    return deletedNotice || <span>资源已经被删除</span>;
  }

  return <>{children}</>;
};
