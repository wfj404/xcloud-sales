namespace XCloud.Sales.Application.Domain.Catalog;

public class Spec : SalesBaseEntity, ISoftDelete
{
    public string Name { get; set; }

    public string GoodsId { get; set; }

    public bool IsDeleted { get; set; }
}