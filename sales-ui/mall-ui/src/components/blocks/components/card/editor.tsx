import { ColorPicker, Form, Input, InputNumber, Space } from 'antd';

import XMultipleGoodsSelector from '@/components/goods/multiple_goods_selector';
import XLinkEditor from '@/components/link';
import XUploader from '@/components/upload/single_picture_form';

import XBlockPictureEditor from '../../components/block_picture_setting';
import { BlockPictureProps } from '../../utils';
import { CardBlockProps } from './utils';

export default function ({
  data,
  onChange,
}: {
  data: CardBlockProps;
  onChange: (d: CardBlockProps) => void;
}) {
  if (!data || !onChange) {
    return null;
  }

  return (
    <>
      <Form labelCol={{ span: 3 }}>
        <Form.Item label={'色调'}>
          <ColorPicker
            allowClear
            showText
            defaultValue={undefined}
            value={data.color || undefined}
            onChange={(e) => {
              onChange({
                ...data,
                color: e.toRgbString(),
              });
            }}
            onClear={() => {
              onChange({
                ...data,
                color: undefined,
              });
            }}
          />
        </Form.Item>
        <Form.Item label={'边框'}>
          <Space direction="horizontal">
            <InputNumber
              addonBefore="边框厚度"
              value={data?.borderWidth}
              onChange={(e) => {
                onChange && onChange({ ...data, borderWidth: e || undefined });
              }}
            />
            <InputNumber
              addonBefore="圆角"
              value={data?.borderRadius}
              onChange={(e) => {
                onChange && onChange({ ...data, borderRadius: e || undefined });
              }}
            />
          </Space>
        </Form.Item>
        <Form.Item label={'标题'}>
          <Input
            value={data?.title}
            onChange={(e) => {
              onChange && onChange({ ...data, title: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'sub title'}>
          <Input
            value={data?.subtitle}
            onChange={(e) => {
              onChange && onChange({ ...data, subtitle: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'caption'}>
          <Input
            value={data?.caption}
            onChange={(e) => {
              onChange && onChange({ ...data, caption: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'标签'}>
          <Input
            value={data?.tag}
            onChange={(e) => {
              onChange && onChange({ ...data, tag: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'图片'}>
          <Space direction="horizontal">
            <div style={{ maxWidth: 200 }}>
              <XBlockPictureEditor
                data={data.picture}
                onChange={(e) => {
                  onChange({
                    ...data,
                    picture: e,
                  });
                }}
              />
            </div>
            <XLinkEditor
              link={data.picture?.link || {}}
              onChange={(e) => {
                const picture: BlockPictureProps = {
                  ...data.picture,
                  link: e,
                };
                onChange({
                  ...data,
                  picture: picture,
                });
              }}
            />
            <XUploader
              data={{}}
              ok={(e) => {
                const picture: BlockPictureProps = {
                  ...data.picture,
                  storage: e,
                };
                onChange({
                  ...data,
                  picture: picture,
                });
              }}
              remove={() => {
                const picture: BlockPictureProps = {
                  ...data.picture,
                  storage: undefined,
                };
                onChange({
                  ...data,
                  picture: picture,
                });
              }}
            />
          </Space>
        </Form.Item>
        <Form.Item label="关联商品">
          <XMultipleGoodsSelector
            maxCount={4}
            goods_list={data.goods_list || []}
            onChange={(e) => {
              onChange({
                ...data,
                goods_list: e || [],
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
}
