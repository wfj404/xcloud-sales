import { QueryWarehouseStockItemInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input } from 'antd';
import { useEffect, useState } from 'react';

const App = (props: {
  query: QueryWarehouseStockItemInput;
  onSearch: (d: QueryWarehouseStockItemInput) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QueryWarehouseStockItemInput>({});

  const triggerSearch = (e: QueryWarehouseStockItemInput) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form(query || {});
  }, []);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        name='basic'
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        onFinish={() => triggerSearch(form)}
        layout='inline'
      >
        <Form.Item label='序列号'>
          <Input value={form.SerialNo || ''}
                 onChange={e => _form(d => ({ ...d, SerialNo: e.target.value }))} />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default App;
