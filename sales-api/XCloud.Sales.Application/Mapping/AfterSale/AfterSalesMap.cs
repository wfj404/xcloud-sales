using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.AfterSale;

namespace XCloud.Sales.Application.Mapping.AfterSale;

public class AfterSalesMap : SalesEntityTypeConfiguration<AfterSales>
{
    public override void Configure(EntityTypeBuilder<AfterSales> builder)
    {
        builder.ToTable("sales_after_sales");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.OrderId).IsRequired().HasMaxLength(100);

        builder.Property(x => x.AfterSalesStatusId).RequiredId();

        builder.Property(rr => rr.ReasonForReturn).IsRequired().HasMaxLength(500);
        builder.Property(rr => rr.RequestedAction).IsRequired().HasMaxLength(500);
        builder.Property(rr => rr.UserComments).HasMaxLength(500);
        builder.Property(rr => rr.StaffNotes).HasMaxLength(500);
        builder.Property(rr => rr.Images).HasMaxLength(1000);

        builder.Property(x => x.HideForCustomer);

        base.Configure(builder);
    }
}