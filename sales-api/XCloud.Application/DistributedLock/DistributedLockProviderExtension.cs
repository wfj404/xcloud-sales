﻿using System;
using System.Threading.Tasks;

namespace XCloud.Application.DistributedLock;

public static class DistributedLockProviderExtension
{
    public static async Task<IAsyncDisposable> CreateRequiredLockAsync(this IDistributedLockProvider provider,
        string resource,
        TimeSpan? expiryTime)
    {
        if (string.IsNullOrWhiteSpace(resource))
            throw new ArgumentNullException(nameof(resource));
        
        var distributedLock = await provider.CreateLockAsync(resource, expiryTime);

        if (distributedLock.IsAcquired)
        {
            // yes we got lock
            return distributedLock;
        }

        await using (distributedLock)
        {
            // ensure disposed
        }

        throw new FailToGetDistributedLockException(message: $"resource:{resource}",
            key: resource,
            expiryTime: expiryTime);
    }
}