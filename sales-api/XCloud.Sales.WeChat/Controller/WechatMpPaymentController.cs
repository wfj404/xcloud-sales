﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Events;
using Volo.Abp;
using XCloud.Core.Dto;
using XCloud.Platform.WeChat;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.WeChat.Service.Payments;

namespace XCloud.Sales.WeChat.Controller;

[StoreAuditLog]
[Route("api/mall/wechat-mp-payment")]
public class WechatMpPaymentController : ShopBaseController
{
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderPaymentProcessingService _orderPaymentProcessingService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly WechatClientFactory _wechatClientFactory;
    private readonly IOrderService _orderService;
    private readonly WechatMpPaymentService _wechatMpPaymentService;
    private readonly WechatRefundService _wechatRefundService;

    public WechatMpPaymentController(
        IOrderPaymentBillService orderPaymentBillService,
        IOrderPaymentProcessingService orderPaymentProcessingService,
        WechatClientFactory wechatClientFactory,
        IOrderRefundBillService orderRefundBillService, IOrderService orderService,
        WechatMpPaymentService wechatMpPaymentService, WechatRefundService wechatRefundService)
    {
        _orderPaymentBillService = orderPaymentBillService;
        _orderPaymentProcessingService = orderPaymentProcessingService;
        _wechatClientFactory = wechatClientFactory;
        _orderRefundBillService = orderRefundBillService;
        _orderService = orderService;
        _wechatMpPaymentService = wechatMpPaymentService;
        _wechatRefundService = wechatRefundService;
    }

    [HttpPost("create-prepay-id")]
    public async Task<ApiResponse<WechatPaymentPrepayOrder>> CreateMpPrepayIdAsync([FromBody] IdDto dto)
    {
        var storeUser = await this.StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await this._orderService.EnsureUserOrderAsync(dto.Id, storeUser.Id);

        var bill = await this._orderPaymentBillService.CreateOrderPayBillAsync(new CreateOrderPayBillInput()
        {
            OrderId = order.Id,
            PriceOrNull = null
        });

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        var wechatResponse = await this._wechatMpPaymentService.CreatePaymentOrderAsync(bill.Id);

        var response = new WechatPaymentPrepayOrder()
        {
            PrepayId = wechatResponse.PrepayId
        };

        return new ApiResponse<WechatPaymentPrepayOrder>(response);
    }

    [HttpPost("create-bill-refund")]
    public async Task<ApiResponse<RefundBillDto>> CreateRefundAsync([FromBody] IdDto dto)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var bill = await this._orderPaymentBillService.GetRequiredPaidBillByIdAsync(dto.Id);

        await this._orderPaymentBillService.AttachOrderAsync(new[] { bill });

        bill.Order = bill.Order ?? throw new ArgumentException(message: "order not found");

        if (bill.Order.StoreId != manager.StoreId)
        {
            throw new ArgumentException(message: "order not belong to store");
        }

        if (bill.PaymentChannel != SalesConstants.PaymentChannel.WechatMp)
        {
            throw new ArgumentException(message: "payment method not wechat mp");
        }

        var refundBill = await this._orderRefundBillService.CreateRefundBillAsync(new CreateRefundBillDto()
        {
            Description = "wechat payment refund",
            BillId = bill.Id,
            PriceOrNull = null
        });

        var wechatResponse = await this._wechatRefundService.CreateRefundOrderAsync(refundBillId: refundBill.Id);

        return new ApiResponse<RefundBillDto>(refundBill);
    }

    [NonAction]
    private async Task MarkBillAsPaidAsync(TransactionResource callbackResource, string json)
    {
        var bill = await this._orderPaymentBillService.MarkAsPaidAsync(new MarkBillAsPayedInput()
        {
            Id = callbackResource.OutTradeNumber,
            TransactionId = callbackResource.TransactionId,
            NotifyData = json,
            PaymentMethod = SalesConstants.PaymentChannel.WechatMp
        });

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await this._orderPaymentProcessingService.TryUpdatePaymentStatusAsync(new IdDto(bill.OrderId));
    }

    [NonAction]
    private async Task MarkBillAsRefundedAsync(RefundResource callbackResource, string json)
    {
        await this._orderRefundBillService.MarkAsRefundedAsync(new MarkRefundBillAsRefundInput()
        {
            Id = callbackResource.OutRefundNumber,
            RefundId = callbackResource.RefundId,
            NotifyData = json
        });

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
    }

    [HttpPost("refresh-payment-status")]
    public async Task<ApiResponse<object>> RefreshPaymentStatusAsync([FromBody] IdDto dto)
    {
        await this._wechatMpPaymentService.TryUpdateBillStatusAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("refresh-refund-status")]
    public async Task<ApiResponse<object>> RefreshRefundStatusAsync([FromBody] IdDto dto)
    {
        await this._wechatRefundService.TryUpdateRefundBillStatusAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("m-{merchant_id}/payment-notify")]
    public async Task<IActionResult> PaymentNotifyAsync(
        [FromRoute(Name = "merchant_id")] string merchantId,
        [FromHeader(Name = "Wechatpay-Timestamp")]
        string timestamp,
        [FromHeader(Name = "Wechatpay-Nonce")] string nonce,
        [FromHeader(Name = "Wechatpay-Signature")]
        string signature,
        [FromHeader(Name = "Wechatpay-Serial")]
        string serialNumber)
    {
        var client = await this._wechatClientFactory.BuildTenPayClientAsync();

        if (merchantId != client.Credentials.MerchantId)
        {
            throw new ArgumentException(message: "wrong mch id");
        }

        using var reader = new StreamReader(Request.Body, Encoding.UTF8);
        var json = await reader.ReadToEndAsync();

        this.Logger.LogWarning(message: "接收到微信支付推送的数据：{0}", json);

        var validResult = await client.VerifyEventSignatureAsync(
            webhookTimestamp: timestamp,
            webhookNonce: nonce,
            webhookBody: json,
            webhookSignature: signature,
            webhookSerialNumber: serialNumber
        );

        if (!validResult)
        {
            // NOTICE:
            //   需提前注入 CertificateManager、并下载平台证书，才可以使用扩展方法执行验签操作。
            //   请参考本示例项目 TenpayCertificateRefreshingBackgroundService 后台任务中的相关实现。
            //   有关 CertificateManager 的完整介绍请参阅《开发文档 / 基础用法 / 如何验证回调通知事件签名？》。
            //   后续如何解密并反序列化，请参阅《开发文档 / 基础用法 / 如何解密回调通知事件中的敏感数据？》。

            this.Logger.LogError(message: "wechat-notify-valid-sign-error",
                exception: validResult.Error ?? new AbpException());

            throw new ArgumentException(message: "invalid wechat payment notification");
        }

        var callbackModel = client.DeserializeEvent(json);
        var eventType = callbackModel.EventType?.ToUpper();

        if (eventType == "TRANSACTION.SUCCESS")
        {
            var callbackResource = client.DecryptEventResource<TransactionResource>(callbackModel);
            // 后续处理略
            await this.MarkBillAsPaidAsync(callbackResource, json);

            return new JsonResult(new { code = "SUCCESS", message = "成功" });
        }
        else if (eventType == "REFUND.SUCCESS")
        {
            var callbackResource = client.DecryptEventResource<RefundResource>(callbackModel);

            await this.MarkBillAsRefundedAsync(callbackResource, json);

            return new JsonResult(new { code = "SUCCESS", message = "成功" });
        }
        else
        {
            throw new NotSupportedException();
        }
    }
}