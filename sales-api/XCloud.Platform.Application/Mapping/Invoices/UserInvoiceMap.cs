using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Invoices;

namespace XCloud.Platform.Application.Mapping.Invoices;

public class UserInvoiceMap : PlatformEntityTypeConfiguration<UserInvoice>
{
    public override void Configure(EntityTypeBuilder<UserInvoice> builder)
    {
        builder.ToTable("sys_user_invoice");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigAbpSoftDeletionTimeEntity();

        builder.Property(x => x.UserId).RequiredId();
        builder.Property(x => x.Head).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Code).HasMaxLength(100);
        builder.Property(x => x.InvoiceType);
        builder.Property(x => x.Address).HasMaxLength(500);
        builder.Property(x => x.PhoneNumber).HasMaxLength(100);
        builder.Property(x => x.BankName).HasMaxLength(100);
        builder.Property(x => x.BankCode).HasMaxLength(100);
        builder.Property(x => x.Content).HasMaxLength(1000);

        base.Configure(builder);
    }
}