import { useRequest } from 'ahooks';
import { Stepper, SwipeAction } from 'antd-mobile';

import XDefaultImage from '@/assets/empty.svg';
import XImage from '@/components/common/image';
import XPriceRow from '@/components/goods/sku/sku_price_row';
import {
  getSkuMainPictureUrl,
  getSkuMaxAmountInOnePurchase,
  getSkuMinAmountInOnePurchase,
  isSkuAndGoodsAvailable,
} from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { ShoppingCartItemDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty } from '@/utils/utils';
import { RadioButtonUncheckedOutlined } from '@mui/icons-material';
import TaskAltOutlinedIcon from '@mui/icons-material/TaskAltOutlined';
import { LoadingButton } from '@mui/lab';
import {
  Box,
  Checkbox,
  FormControlLabel,
  Paper,
  Stack,
  Typography,
} from '@mui/material';

export default function ({
  model,
  onSelect,
  checked,
  onUpdate,
}: {
  model: ShoppingCartItemDto;
  onSelect: (id: number, checked: boolean) => void;
  checked: boolean;
  onUpdate: () => void;
}) {
  const goodsImageUrl = getSkuMainPictureUrl(model.Sku || {}, {
    width: 200,
    height: 200,
  });

  const minQuantity = getSkuMinAmountInOnePurchase(model.Sku);
  const maxQuantity = Math.min(
    getSkuMaxAmountInOnePurchase(model.Sku),
    model.Sku?.StockModel?.StockQuantity || 0,
  );

  const deleteCartRequest = useRequest(apiClient.mall.shoppingCartRemoveItem, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        onUpdate();
      });
    },
  });

  const deleteCart = () => {
    if (!confirm('确定删除？')) {
      return;
    }
    deleteCartRequest.run({ Id: model.Id });
  };

  const updateQuantityRequest = useRequest(
    apiClient.mall.shoppingCartUpdateCartItem,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          onUpdate();
        });
      },
    },
  );

  const loading = updateQuantityRequest.loading || deleteCartRequest.loading;

  const renderWarnings = () => {
    if (isArrayEmpty(model.Waring)) {
      return null;
    }
    return (
      <>
        <Box sx={{ backgroundColor: 'rgb(250,250,250)', my: 1, p: 0.5 }}>
          {model.Waring?.map((x, index) => (
            <Typography
              gutterBottom
              variant={'caption'}
              component={'div'}
              key={index}
            >
              {x}
            </Typography>
          ))}
        </Box>
      </>
    );
  };

  const renderPictureRight = () => {
    return (
      <>
        <Box sx={{ mr: 1, mb: 1 }}>
          <XPriceRow model={model.Sku || {}} />
        </Box>
        <Stack
          direction={'row'}
          alignItems="center"
          justifyContent="right"
          spacing={1}
        >
          <Stepper
            value={model.Quantity}
            min={minQuantity}
            max={maxQuantity}
            disabled={minQuantity > maxQuantity || loading}
            onChange={(value) => {
              if (value <= minQuantity) {
                if (confirm('向左滑动即可删除！')) {
                  deleteCart();
                }
              } else {
                updateQuantityRequest.run({
                  Id: model.Id,
                  Quantity: value,
                });
              }
            }}
            style={{
              '--button-text-color': 'black',
            }}
          />
        </Stack>
      </>
    );
  };

  const renderHeader = () => {
    return (
      <>
        <Stack
          direction={'row'}
          alignItems="center"
          justifyContent={'space-between'}
        >
          <FormControlLabel
            control={
              <Checkbox
                checked={checked}
                onChange={(e) => {
                  onSelect && onSelect(model.Id || 0, e.target.checked);
                }}
                icon={<RadioButtonUncheckedOutlined />}
                checkedIcon={<TaskAltOutlinedIcon color="success" />}
                disabled={!isArrayEmpty(model.Waring)}
              />
            }
            label={`${model.Sku?.Goods?.Name || '--'}`}
          />
        </Stack>
      </>
    );
  };

  const renderItemContent = () => {
    return (
      <>
        {renderHeader()}
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <Box sx={{ width: '70px' }}>
            <XImage
              rate={1}
              src={goodsImageUrl || XDefaultImage}
              fit={'cover'}
              width={'100%'}
              placeholder={<img src={XDefaultImage} width="100%" alt="" />}
              style={{}}
              alt={''}
            />
          </Box>
          <Box flexGrow={1} sx={{ ml: 1, width: '100%' }}>
            {renderPictureRight()}
          </Box>
        </Box>
        {renderWarnings()}
      </>
    );
  };

  if (!isSkuAndGoodsAvailable(model.Sku || {})) {
    return (
      <>
        <div
          style={{
            padding: 10,
            marginBottom: 10,
            border: `1px dashed gray`,
            backgroundColor: 'rgb(250,250,250)',
          }}
        >
          <Stack
            direction={'row'}
            alignItems="center"
            justifyContent={'space-between'}
          >
            <div>
              <Typography variant={'body1'} component={'div'}>
                您此前收藏的商品已下架
              </Typography>
              <Typography
                variant={'caption'}
                color={'text.disabled'}
                component={'div'}
              >
                <span>
                  收藏于{formatRelativeTimeFromNow(model.CreationTime)}
                </span>
              </Typography>
            </div>
            <LoadingButton
              size={'small'}
              variant={'text'}
              color={'error'}
              loading={loading}
              onClick={() => {
                deleteCart();
              }}
            >
              删除此商品
            </LoadingButton>
          </Stack>
        </div>
      </>
    );
  }

  return (
    <>
      <Box sx={{ mb: 2, borderRadius: 1, overflow: 'hidden' }}>
        <SwipeAction
          rightActions={[
            {
              color: 'danger',
              text: deleteCartRequest.loading ? 'loading...' : '删除',
              key: 1,
              onClick: (e) => {
                deleteCart();
              },
            },
          ]}
        >
          <Paper
            sx={{
              p: 2,
            }}
            elevation={0}
          >
            {renderItemContent()}
          </Paper>
        </SwipeAction>
      </Box>
    </>
  );
}
