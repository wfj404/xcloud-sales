﻿using XCloud.Application.DistributedLock;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Users;

public enum PointsActionType
{
    Add = 1,
    Use = -1
}

public interface IStoreUserPointService : ISalesAppService
{
    Task AddPointsHistoryAsync(PointsHistoryDto dto);

    Task<PagedResponse<PointsHistoryDto>> QueryPagingAsync(QueryPointsPagingInput dto);
}

public class StoreUserPointService : SalesAppService, IStoreUserPointService
{
    private readonly ISalesRepository<PointsHistory> _pointsRepository;

    public StoreUserPointService(ISalesRepository<PointsHistory> pointsRepository)
    {
        _pointsRepository = pointsRepository;
    }

    public async Task<PagedResponse<PointsHistoryDto>> QueryPagingAsync(QueryPointsPagingInput dto)
    {
        var db = await _pointsRepository.GetDbContextAsync();

        var query = db.Set<PointsHistory>().AsNoTracking();

        query = query.Where(x => x.UserId == dto.UserId);

        if (!string.IsNullOrWhiteSpace(dto.OrderId))
            query = query.Where(x => x.OrderId == dto.OrderId);

        if (dto.ActionType != null)
            query = query.Where(x => x.ActionType == dto.ActionType.Value);

        var count = await query.CountOrDefaultAsync(dto);

        var list = await query.OrderByDescending(x => x.CreationTime).PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();
        var items = list.Select(x => ObjectMapper.Map<PointsHistory, PointsHistoryDto>(x)).ToArray();

        return new PagedResponse<PointsHistoryDto>(items, dto, count);
    }

    public async Task AddPointsHistoryAsync(PointsHistoryDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));
        if (dto.UserId <= 0)
            throw new ArgumentNullException(nameof(dto.UserId));

        dto.Points = Math.Abs(dto.Points);
        if (dto.Points == 0)
            throw new ArgumentNullException(nameof(dto.Points));

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(StoreUserPointService)}.{nameof(AddPointsHistoryAsync)}.{dto.UserId}",
            expiryTime: TimeSpan.FromSeconds(3));

        var db = await _pointsRepository.GetDbContextAsync();

        if (!string.IsNullOrWhiteSpace(dto.OrderId))
            if (await db.Set<PointsHistory>().AnyAsync(x => x.OrderId == dto.OrderId))
            {
                Logger.LogInformation("order points is sent");
                return;
            }

        var offset = 0;
        if (dto.ActionType == (int)PointsActionType.Use)
            offset = -dto.Points;
        else if (dto.ActionType == (int)PointsActionType.Add)
            offset = dto.Points;
        else
            throw new ArgumentException("points action type");

        var entity = ObjectMapper.Map<PointsHistoryDto, PointsHistory>(dto);

        var user = await db.Set<StoreUser>().FirstOrDefaultAsync(x => x.Id == entity.UserId);
        if (user == null)
            throw new EntityNotFoundException(nameof(AddPointsHistoryAsync));

        user.Points += offset;
        if (offset > 0)
            user.HistoryPoints += offset;
        user.LastModificationTime = Clock.Now;

        entity.PointsBalance = user.Points;
        entity.CreationTime = Clock.Now;

        db.Set<PointsHistory>().Add(entity);

        await db.SaveChangesAsync();
    }
}