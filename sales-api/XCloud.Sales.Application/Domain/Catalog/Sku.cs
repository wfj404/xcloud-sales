using XCloud.Application.Utils;
using XCloud.Sales.Application.Domain.Mapping;

namespace XCloud.Sales.Application.Domain.Catalog;

[TypeIdentityName("sku")]
public class Sku : SalesBaseEntity<string>, IHasDeletionTime, IHasCreationTime,
    IHasModificationTime, IMappingSupported
{
    public string GoodsId { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public string SkuCode { get; set; }

    public string Color { get; set; }

    public decimal Price { get; set; }

    public int MaxAmountInOnePurchase { get; set; }

    private int _minAmountInOnePurchase;

    public int MinAmountInOnePurchase
    {
        get => Math.Max(1, this._minAmountInOnePurchase);
        set => this._minAmountInOnePurchase = value;
    }

    public int Weight { get; set; }

    public string SpecCombinationJson { get; set; }

    public bool IsActive { get; set; }

    public DateTime? DeletionTime { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}