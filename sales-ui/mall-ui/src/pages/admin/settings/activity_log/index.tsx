import { Button, Card, message, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XUserAvatar from '@/components/manage/user/avatar';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { ActivityLogDto, QueryActivityLogPagingInput } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XDetail from './detail';
import XSearchForm from './search_form';
import utils from './utils';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<ActivityLogDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QueryActivityLogPagingInput>({
    Page: 1,
  });

  const [loadingId, _loadingId] = useState<number | undefined | null>(0);

  const queryList = (q: QueryActivityLogPagingInput) => {
    _loading(true);
    apiClient.mallAdmin
      .activityLogQueryActivityLogPaging(q)
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const deleteRow = (row: ActivityLogDto) => {
    if (!confirm('删除？')) {
      return;
    }
    _loadingId(row.Id);
    apiClient.mallAdmin
      .activityLogDeleteActivityLog({
        Id: row.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('删除成功');
          queryList(query);
        });
      })
      .finally(() => {
        _loadingId(undefined);
      });
  };

  const columns: ColumnProps<ActivityLogDto>[] = [
    {
      title: '日志内容',
      render: (d, x) => x.Comment || '--',
    },
    {
      title: '日志类型',
      render: (d, x) => {
        let desc = utils.ActivityLogTypesDescription.find(
          (d) => d.id == x.ActivityLogTypeId,
        );
        if (!desc) {
          return '--';
        }
        return <span>{desc.name || '--'}</span>;
      },
    },
    {
      title: '管理员',
      render: (d, x: ActivityLogDto) => {
        return <XUserAvatar model={x.Admin?.User} />;
      },
    },
    {
      title: '用户',
      render: (d, x: ActivityLogDto) => {
        return <XUserAvatar model={x.StoreUser?.User} />;
      },
    },
    {
      title: '地址',
      render: (d, x: ActivityLogDto) => x.GeoCity || x.GeoCountry || '--',
    },
    {
      title: '时间',
      render: (d, x: ActivityLogDto) => <XTime model={x} />,
    },
    {
      fixed: 'right',
      title: '操作',
      width: 200,
      render: (d, record: ActivityLogDto) => {
        return (
          <Button.Group>
            <Button
              loading={loadingId == record.Id}
              type="link"
              danger
              onClick={() => {
                deleteRow(record);
              }}
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card title="活动日志">
        <Table
          style={{ width: '100%' }}
          rowKey={(x) => x.Id || 0}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          expandable={{
            expandedRowRender: (e) => (
              <div style={{ padding: 10, border: '5px dashed orange' }}>
                <XDetail model={e} />
              </div>
            ),
          }}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              const q: QueryActivityLogPagingInput = {
                ...query,
                Page: e,
              };
              _query(q);
              queryList(q);
            },
          }}
        />
      </Card>
    </>
  );
};
