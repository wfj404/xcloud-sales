﻿using System;

namespace XCloud.Application.Apm;

/// <summary>
/// 标记开启apm
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class ApmAttribute : Attribute
{
    public ApmAttribute()
    {
        //
    }

    public bool Disabled { get; set; } = false;
}