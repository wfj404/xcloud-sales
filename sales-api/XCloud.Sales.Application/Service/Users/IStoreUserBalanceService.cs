﻿using XCloud.Application.DistributedLock;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Users;

public enum BalanceActionType
{
    Add = 1,
    Use = -1
}

public interface IStoreUserBalanceService : ISalesAppService
{
    Task<decimal> CountAllBalanceAsync();

    Task UpdateUserBalanceAsync(BalanceHistoryDto dto);

    Task<PagedResponse<BalanceHistoryDto>> QueryPagingAsync(QueryBalancePagingInput dto);
}

public class StoreUserBalanceService : SalesAppService, IStoreUserBalanceService
{
    private readonly ISalesRepository<BalanceHistory> _salesRepository;

    public StoreUserBalanceService(ISalesRepository<BalanceHistory> salesRepository)
    {
        _salesRepository = salesRepository;
    }

    public async Task<decimal> CountAllBalanceAsync()
    {
        var db = await _salesRepository.GetDbContextAsync();

        var sum = await db.Set<StoreUser>().AsNoTracking().SumAsync(x => x.Balance);

        return sum;
    }

    public async Task<PagedResponse<BalanceHistoryDto>> QueryPagingAsync(QueryBalancePagingInput dto)
    {
        var db = await _salesRepository.GetDbContextAsync();

        var query = db.Set<BalanceHistory>().AsNoTracking();

        query = query.Where(x => x.UserId == dto.UserId);

        if (dto.ActionType != null)
            query = query.Where(x => x.ActionType == dto.ActionType.Value);

        var count = await query.CountOrDefaultAsync(dto);

        var list = await query.OrderByDescending(x => x.CreationTime).PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();
        var items = list.Select(x => ObjectMapper.Map<BalanceHistory, BalanceHistoryDto>(x)).ToArray();

        return new PagedResponse<BalanceHistoryDto>(items, dto, count);
    }

    public async Task UpdateUserBalanceAsync(BalanceHistoryDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (dto.UserId <= 0)
            throw new ArgumentNullException(nameof(dto.UserId));

        if (dto.Balance <= decimal.Zero)
            throw new ArgumentException(nameof(dto.Balance));

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(StoreUserPointService)}.{nameof(UpdateUserBalanceAsync)}.{dto.UserId}",
            expiryTime: TimeSpan.FromSeconds(3));

        var db = await _salesRepository.GetDbContextAsync();

        var offset = default(decimal);
        if (dto.ActionType == (int)BalanceActionType.Use)
            offset = -dto.Balance;
        else if (dto.ActionType == (int)BalanceActionType.Add)
            offset = dto.Balance;
        else
            throw new ArgumentException("points action type");

        var user = await db.Set<StoreUser>().FirstOrDefaultAsync(x => x.Id == dto.UserId);
        if (user == null)
            throw new EntityNotFoundException(nameof(UpdateUserBalanceAsync));

        user.Balance += offset;
        if (user.Balance < decimal.Zero)
            throw new UserFriendlyException("balance is not enough to deduct");

        dto.LatestBalance = user.Balance;

        var entity = ObjectMapper.Map<BalanceHistoryDto, BalanceHistory>(dto);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        db.Set<BalanceHistory>().Add(entity);

        await db.SaveChangesAsync();
    }
}