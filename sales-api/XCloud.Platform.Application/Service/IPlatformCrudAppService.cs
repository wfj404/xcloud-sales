using System.Linq.Expressions;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Application.Service;
using XCloud.EntityFrameworkCore.Repository;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Application.Service;

public interface
    IPlatformCrudService<TEntity, TEntityDto, in TPagingRequest> :
        IXCloudCrudAppService<
            TEntity,
            TEntityDto, TPagingRequest, string>
    where TEntity : PlatformBaseEntity
    where TEntityDto : class, IEntityDto<string>
    where TPagingRequest : PagedRequest
{
    //
}

public abstract class
    PlatformCrudService<TEntity, TEntityDto, TPagingRequest> :
        XCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, string>,
        IPlatformCrudService<TEntity, TEntityDto, TPagingRequest>
    where TEntity : PlatformBaseEntity
    where TEntityDto : class, IEntityDto<string>
    where TPagingRequest : PagedRequest
{
    protected PlatformCrudService(IEfRepository<TEntity> repository) : base(repository)
    {
        //
    }

    protected override Expression<Func<TEntity, bool>> BuildPrimaryKeyEqualExpression(string id)
    {
        return x => x.Id == id;
    }

    protected override void TrySetPrimaryKey(TEntity entity)
    {
        entity.Id = GuidGenerator.CreateGuidString();
    }

    protected override bool IsEmptyKey(string id)
    {
        return string.IsNullOrWhiteSpace(id);
    }

    protected override async Task<IOrderedQueryable<TEntity>> GetPagingOrderedQueryableAsync(IQueryable<TEntity> query,
        TPagingRequest dto)
    {
        await Task.CompletedTask;
        return query.OrderByDescending(x => x.Id);
    }
}