﻿using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;
using XCloud.Core.Helper;

namespace XCloud.Core.Json.TextJson;

public class DateTimeJsonConverter : JsonConverter<DateTime>
{
    private readonly string _dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    private readonly DateTimeFormat _format;
    
    public DateTimeJsonConverter(string dateTimeFormat = null)
    {
        if (!string.IsNullOrWhiteSpace(dateTimeFormat))
        {
            _dateTimeFormat = dateTimeFormat;
        }
        _format = new DateTimeFormat(_dateTimeFormat);
    }

    public override bool CanConvert(Type typeToConvert) => typeToConvert == typeof(DateTime);

    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.String || reader.TokenType == JsonTokenType.Null)
        {
            var val = reader.GetString();

            if (!string.IsNullOrWhiteSpace(val) && DateTime.TryParse(val, _format.FormatProvider, _format.DateTimeStyles, out var res))
            {
                return res;
            }
            else
            {
                return default;
            }
        }
        else if (reader.TokenType == JsonTokenType.Number)
        {
            if (!reader.TryGetInt64(out var val))
                throw new NotSupportedException("can't get int64");
            
            var res = Com.Utc1970.AddMilliseconds(val);
            return res;
        }

        throw new NotSupportedException($"{typeToConvert.ToString()}不能转成日期");
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        var res = value.ToString(_dateTimeFormat, _format.FormatProvider);
        writer.WriteStringValue(res);
    }
}

public class NullableDateTimeJsonConverter : JsonConverter<DateTime?>
{
    private readonly string _dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    private readonly DateTimeFormat _format;
    
    public NullableDateTimeJsonConverter(string dateTimeFormat = null)
    {
        if (!string.IsNullOrWhiteSpace(dateTimeFormat))
        {
            _dateTimeFormat = dateTimeFormat;
        }
        _format = new DateTimeFormat(_dateTimeFormat);
    }

    public override bool CanConvert(Type typeToConvert) => typeToConvert == typeof(DateTime?);

    public override DateTime? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        if (reader.TokenType == JsonTokenType.String || reader.TokenType == JsonTokenType.Null)
        {
            var val = reader.GetString();

            if (!string.IsNullOrWhiteSpace(val) && DateTime.TryParse(val, _format.FormatProvider, _format.DateTimeStyles, out var res))
            {
                return res;
            }
            else
            {
                return default;
            }
        }
        else if (reader.TokenType == JsonTokenType.Number)
        {
            if (!reader.TryGetInt64(out var val))
                throw new NotSupportedException("can't read int64");
            
            var res = Com.Utc1970.AddMilliseconds(val);
            return res;
        }

        throw new NotSupportedException($"{typeToConvert.ToString()}不能转成日期");
    }

    public override void Write(Utf8JsonWriter writer, DateTime? value, JsonSerializerOptions options)
    {
        if (value == null)
        {
            writer.WriteNullValue();
        }
        else
        {
            var res = value.Value.ToString(_dateTimeFormat, _format.FormatProvider);
            writer.WriteStringValue(res);
        }
    }
}

public class TextJsonDatetimeConverter : JsonConverter<DateTime>
{
    private readonly string _dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var strValue = reader.GetString();
        if (!string.IsNullOrWhiteSpace(strValue) && DateTime.TryParse(strValue, out var d))
        {
            return d;
        }
        throw new NotSupportedException($"无法把'{strValue}'转成datetime格式");
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        var strValue = value.ToString(format: _dateTimeFormat);
        writer.WriteStringValue(strValue);
    }
}