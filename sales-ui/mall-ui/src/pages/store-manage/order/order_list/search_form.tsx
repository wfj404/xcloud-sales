import {
  Button,
  Card,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  Row,
  Segmented,
  Select,
  Space,
} from 'antd';
import { useEffect, useState } from 'react';

import {
  AfterSalesStatusList,
  OrderStatusList,
  PaymentStatusList,
  ShippingMethodList,
  ShippingStatusList,
} from '@/utils/biz';
import {
  dateTimeFormat,
  myDayjs,
  parseAsDayjs,
  timezoneOffset,
} from '@/utils/dayjs';
import { QueryOrderPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';

import { order_filter_finger_print, order_preset_filters } from './utils';

export default ({
  query,
  onSearch,
}: {
  query: QueryOrderPagingInput;
  onSearch: (d: QueryOrderPagingInput) => void;
}) => {
  const [selectedPreset, _selectedPreset] = useState<string>('');
  const [model, _model] = useState<QueryOrderPagingInput>({});

  const model_finger_print = order_filter_finger_print(model);
  const matched_preset_id = order_preset_filters.find(
    (x) => order_filter_finger_print(x.filter) == model_finger_print,
  )?.value;

  useEffect(() => {
    console.log('model finger print:', model_finger_print);
    console.log('matched preset id:', matched_preset_id);
    console.log(
      'all finger print:',
      order_preset_filters.map((x) => order_filter_finger_print(x.filter)),
    );
  }, [model_finger_print]);

  useEffect(() => {
    if (matched_preset_id && selectedPreset != matched_preset_id) {
      _selectedPreset(matched_preset_id);
    }
  }, [matched_preset_id]);

  const triggerSearch = (x: QueryOrderPagingInput) => {
    const e: QueryOrderPagingInput = { ...x, Page: 1 };

    if (!e.IsAfterSales) {
      e.IsAfterSales = undefined;
    }

    if (!e.IsAfterSalesPending) {
      e.IsAfterSalesPending = undefined;
    }

    if (!e.ToService) {
      e.ToService = undefined;
    }

    console.log('triggerSearch order', e);

    onSearch && onSearch(e);
  };

  useEffect(() => {
    _model({
      ...query,
    });
  }, [query]);

  return (
    <>
      <Segmented
        block
        style={{
          marginBottom: 10,
        }}
        size={'large'}
        value={selectedPreset}
        options={order_preset_filters.filter((x) => !x.hide)}
        onChange={(e) => {
          _selectedPreset(e);
          //
          const selected_filter: QueryOrderPagingInput =
            order_preset_filters.find((x) => x.value == e)?.filter || {};
          triggerSearch(selected_filter);
        }}
      />
      <Card bordered={false} style={{ marginBottom: 10 }}>
        <Form
          labelCol={{
            span: 8,
          }}
          autoComplete="off"
        >
          <Row gutter={10}>
            <Col span={6}>
              <Form.Item label="订单号">
                <Input
                  value={model.Sn || ''}
                  onChange={(e) => {
                    _model({
                      ...model,
                      Sn: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="订单状态">
                <Select
                  value={model.Status || []}
                  mode="multiple"
                  allowClear
                  options={OrderStatusList.map((x) => ({
                    label: x.name,
                    value: x.id,
                  }))}
                  onChange={(e) => {
                    _model({
                      ...model,
                      Status: e || [],
                    });
                  }}
                ></Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="支付状态">
                <Select
                  mode={'multiple'}
                  value={model.PaymentStatus || []}
                  allowClear
                  options={PaymentStatusList.map((x) => ({
                    label: x.name,
                    value: x.id,
                  }))}
                  onChange={(e) => {
                    _model({
                      ...model,
                      PaymentStatus: e || [],
                    });
                  }}
                ></Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="时间范围">
                <DatePicker.RangePicker
                  format={'YYYY-MM-DD'}
                  picker="date"
                  allowClear
                  value={[
                    parseAsDayjs(model.StartTime)?.add(timezoneOffset, 'hour'),
                    parseAsDayjs(model.EndTime)?.add(timezoneOffset, 'hour'),
                  ]}
                  onChange={(e) => {
                    _model({
                      ...model,
                      StartTime: e
                        ?.at(0)
                        ?.add(-timezoneOffset, 'hour')
                        ?.format(dateTimeFormat),
                      EndTime: e
                        ?.at(1)
                        ?.add(-timezoneOffset, 'hour')
                        ?.format(dateTimeFormat),
                    });
                  }}
                  presets={[
                    {
                      label: '过去一周',
                      value: [
                        myDayjs
                          .utc()
                          .add(timezoneOffset, 'hour')
                          .add(-1, 'week'),
                        myDayjs.utc().add(timezoneOffset, 'hour'),
                      ],
                    },
                  ]}
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="待服务" tooltip={'已支付或者货到付款'}>
                <Checkbox
                  checked={model.ToService || false}
                  onChange={(e) => {
                    _model({
                      ...model,
                      ToService: e.target.checked,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="交付方式">
                <Select
                  mode={'multiple'}
                  value={model.ShippingMethodId}
                  allowClear
                  options={ShippingMethodList.map((x) => ({
                    label: x.name,
                    value: x.id,
                  }))}
                  onChange={(e) => {
                    _model({
                      ...model,
                      ShippingMethodId: e,
                    });
                  }}
                ></Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label="交付状态">
                <Select
                  mode={'multiple'}
                  value={model.DeliveryStatus || []}
                  allowClear
                  options={ShippingStatusList.map((x) => ({
                    label: x.name,
                    value: x.id,
                  }))}
                  onChange={(e) => {
                    _model({
                      ...model,
                      DeliveryStatus: e || [],
                    });
                  }}
                ></Select>
              </Form.Item>
            </Col>
            <Col span={6}></Col>
            <Col span={6}>
              <Form.Item label="有售后">
                <Checkbox
                  checked={model.IsAfterSales || false}
                  onChange={(e) => {
                    _model({
                      ...model,
                      IsAfterSales: e.target.checked || undefined,
                      IsAfterSalesPending: e.target.checked
                        ? model.IsAfterSalesPending
                        : undefined,
                      AfterSalesStatus: e.target.checked
                        ? model.AfterSalesStatus
                        : undefined,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            {model.IsAfterSales && (
              <>
                <Col span={6}>
                  <Form.Item label="售后中">
                    <Checkbox
                      checked={model.IsAfterSalesPending || false}
                      onChange={(e) => {
                        _model({
                          ...model,
                          IsAfterSalesPending: e.target.checked || undefined,
                        });
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col span={6}>
                  <Form.Item label="售后状态">
                    <Select
                      mode={'multiple'}
                      value={model.AfterSalesStatus || []}
                      allowClear
                      options={AfterSalesStatusList.map((x) => ({
                        label: x.name,
                        value: x.id,
                      }))}
                      onChange={(e) => {
                        _model({
                          ...model,
                          AfterSalesStatus: e || [],
                        });
                      }}
                    ></Select>
                  </Form.Item>
                </Col>
              </>
            )}
            <Col span={6}>
              <Space>
                <Button
                  type="primary"
                  htmlType="submit"
                  icon={<SearchOutlined />}
                  onClick={() => triggerSearch(model)}
                >
                  搜索
                </Button>
              </Space>
            </Col>
            <Col span={6}></Col>
          </Row>
        </Form>
      </Card>
    </>
  );
};
