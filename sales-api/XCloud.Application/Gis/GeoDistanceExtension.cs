﻿using System;
using NetTopologySuite.Geometries;

namespace XCloud.Application.Gis;

public static class GeoDistanceExtension
{
    public static double TryGetDistanceInMeter(this GeoLocationDto location, GeoLocationDto otherLocation,
        out string error)
    {
        error = string.Empty;

        if (!location.IsValid)
        {
            error = $"invalid geo location :${nameof(location)}";
            return default;
        }

        if (!otherLocation.IsValid)
        {
            error = $"invalid geo location :${nameof(otherLocation)}";
            return default;
        }

        try
        {
            return GetDistanceInMeter(location, otherLocation);
        }
        catch (Exception e)
        {
            error = e.Message ?? nameof(TryGetDistanceInMeter);
            return default;
        }
    }

    private static double GetDistanceInMeter(this GeoLocationDto location, GeoLocationDto otherLocation)
    {
        var factory = new GeometryFactory(new PrecisionModel(), 4326);

        var from = factory.CreatePoint(new Coordinate(x: location.Lon, y: location.Lat)).ProjectTo(2855);

        var to = factory.CreatePoint(new Coordinate(x: otherLocation.Lon, y: otherLocation.Lat)).ProjectTo(2855);

        var distance = from.Distance(to);

        return distance;
    }

    public static double GreatCircleDistance(double lon1, double lat1, double lon2, double lat2)
    {
        double R = 6371e3; // m

        double sLat1 = Math.Sin(Radians(lat1));
        double sLat2 = Math.Sin(Radians(lat2));
        double cLat1 = Math.Cos(Radians(lat1));
        double cLat2 = Math.Cos(Radians(lat2));
        double cLon = Math.Cos(Radians(lon1) - Radians(lon2));

        double cosD = sLat1 * sLat2 + cLat1 * cLat2 * cLon;

        double d = Math.Acos(cosD);

        double dist = R * d;

        return dist;

        double Radians(double x)
        {
            return x * Math.PI / 180;
        }
    }
}