using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Sales.Application.Mapping;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Mapping.Warehouses;

public class WarehouseMap : SalesEntityTypeConfiguration<Warehouse>
{
    public override void Configure(EntityTypeBuilder<Warehouse> builder)
    {
        builder.ToTable("sales_warehouse");
        
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeleteEntity();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Address).HasMaxLength(500);
        builder.Property(x => x.Lng);
        builder.Property(x => x.Lat);

        builder.Property(x => x.CreationTime);
        builder.Property(x => x.IsDeleted);

        base.Configure(builder);
    }
}