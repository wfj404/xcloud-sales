﻿using System.Threading.Tasks;
using XCloud.Application.Service;

namespace XCloud.Platform.Application.Service.Users;

public interface IUserLoginGuardService : IXCloudAppService
{
    Task AddLoginFailureRecordsAsync(string userId);

    Task<LoginFailureRecordDto[]> ListUserRecentLoginFailureRecordsAsync(string userId);
}

public class UserLoginGuardService : PlatformAppService, IUserLoginGuardService
{
    private int MaxRecords => 100;
    private int ExpiredInSeconds => (int)TimeSpan.FromMinutes(3).TotalSeconds;

    private string BuildKey(string userId) => $"{nameof(UserLoginGuardService)}.{userId}";

    private LoginFailureRecordDto[] TrimData(LoginFailureRecordDto[] data)
    {
        var expired = this.Clock.Now.AddSeconds(-this.ExpiredInSeconds);

        return data
            .Where(x => x.CreationTime > expired)
            .OrderBy(x => x.CreationTime)
            .Take(this.MaxRecords)
            .ToArray();
    }

    public async Task AddLoginFailureRecordsAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var datalist = await ListUserRecentLoginFailureRecordsAsync(userId);

        datalist = datalist.Append(new LoginFailureRecordDto()
        {
            Id = userId,
            CreationTime = this.Clock.Now
        }).ToArray();

        await this.CacheProvider.SetAsync(BuildKey(userId), datalist, expire: TimeSpan.FromMinutes(100));
    }

    public async Task<LoginFailureRecordDto[]> ListUserRecentLoginFailureRecordsAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var datalist = await this.CacheProvider
            .GetOrDefaultAsync<LoginFailureRecordDto[]>(BuildKey(userId));

        datalist ??= Array.Empty<LoginFailureRecordDto>();

        return this.TrimData(datalist);
    }
}