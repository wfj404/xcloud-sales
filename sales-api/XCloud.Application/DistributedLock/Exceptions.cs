﻿using System;
using Volo.Abp;

namespace XCloud.Application.DistributedLock;

public class FailToGetDistributedLockException : BusinessException
{
    public FailToGetDistributedLockException()
    {
        //
    }

    public FailToGetDistributedLockException(string message, string key = null, TimeSpan? expiryTime = null) :
        base(message)
    {
        this.WithData(nameof(key), key);
        this.WithData(nameof(expiryTime), expiryTime);
    }
}