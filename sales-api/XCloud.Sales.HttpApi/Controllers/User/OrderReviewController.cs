﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/order-review")]
public class OrderReviewController : ShopBaseController
{
    private readonly IOrderReviewService _orderReviewService;
    private readonly IOrderReviewItemService _orderReviewItemService;
    private readonly IOrderService _orderService;
    private readonly IStoreUserService _storeUserService;

    public OrderReviewController(IOrderReviewService orderReviewService,
        IOrderService orderService,
        IStoreUserService storeUserService, IOrderReviewItemService orderReviewItemService)
    {
        _orderReviewService = orderReviewService;
        _orderService = orderService;
        _storeUserService = storeUserService;
        _orderReviewItemService = orderReviewItemService;
    }

    [HttpPost("goods-review")]
    public async Task<PagedResponse<OrderReviewItemDto>> QueryItemsPagingAsync(
        [FromBody] QueryOrderReviewItemPaging dto)
    {
        if (string.IsNullOrWhiteSpace(dto.GoodsId))
            throw new ArgumentNullException(nameof(dto.GoodsId));

        dto.IsApproved = true;
        dto.PageSize = 20;

        var response = await _orderReviewItemService.QueryItemsPagingAsync(dto);

        if (response.IsEmpty)
            return response;

        await _orderReviewItemService.AttachPicturesAsync(response.Items.ToArray());
        await _orderReviewItemService.AttachMainReviewAsync(response.Items.ToArray());
        var reviewList = response.Items.Select(x => x.OrderReview).WhereNotNull().ToArray();
        await this._orderReviewService.AttachStoreUserAsync(reviewList);
        var storeUsers = reviewList.Select(x => x.StoreUser).WhereNotNull().ToArray();
        await this._storeUserService.AttachPlatformUsersAsync(storeUsers);

        return response;
    }

    [HttpPost("insert")]
    public async Task<ApiResponse<OrderReview>> InsertAsync([FromBody] OrderReviewDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.OrderId, loginUser.Id);

        var review = await _orderReviewService.InsertAsync(dto);

        return new ApiResponse<OrderReview>(review);
    }

    [HttpPost("get-review-by-order-id")]
    public async Task<ApiResponse<OrderReviewDto>> QueryOrderReviewByOrderIdAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        var review = await _orderReviewService.GetReviewByOrderIdAsync(order.Id);

        if (review == null)
            return new ApiResponse<OrderReviewDto>();

        await _orderReviewService.AttachItemsAsync(new[] { review });
        await _orderReviewService.AttachPicturesAsync(new[] { review });
        await _orderReviewItemService.AttachPicturesAsync(review.Items);

        return new ApiResponse<OrderReviewDto>(review);
    }
}