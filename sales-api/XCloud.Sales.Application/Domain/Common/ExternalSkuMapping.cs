﻿namespace XCloud.Sales.Application.Domain.Common;

public class ExternalSkuMapping : SalesBaseEntity<string>
{
    public string CombineId { get; set; }

    public string SkuId { get; set; }
}