﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Exceptions;
using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.AfterSale;

public interface IAfterSaleService : ISalesStringCrudAppService<AfterSales, AfterSalesDto, QueryAfterSalePaging>
{
    Task<AfterSalesDto> EnsureStoreAfterSalesAsync(string afterSaleId, string storeId);

    Task<AfterSalesDto> EnsureUserAfterSalesAsync(string afterSaleId, int userId);

    Task<AfterSalesDto> QueryByOrderIdAsync(string orderId);

    Task UpdateStatusAsync(UpdateAfterSaleStatusDto dto);

    Task<AfterSalesDto[]> AttachOrderAsync(AfterSalesDto[] data);

    Task<AfterSalesDto[]> AttachItemsAsync(AfterSalesDto[] data);

    Task<AfterSalesItemDto[]> AttachOrderItemsAsync(AfterSalesItemDto[] data);

    Task<int> QueryPendingCountAsync(QueryAfterSalePendingCountInput dto);
}

public class AfterSaleService : SalesStringCrudAppService<AfterSales, AfterSalesDto, QueryAfterSalePaging>,
    IAfterSaleService
{
    private readonly ISalesRepository<AfterSales> _afterSalesRepository;
    private readonly AfterSaleUtils _afterSaleUtils;
    private readonly IOrderService _orderService;

    public AfterSaleService(
        AfterSaleUtils afterSaleUtils,
        ISalesRepository<AfterSales> afterSalesRepository, IOrderService orderService) : base(afterSalesRepository)
    {
        _afterSaleUtils = afterSaleUtils;
        _afterSalesRepository = afterSalesRepository;
        _orderService = orderService;
    }

    public override Task<AfterSales> InsertAsync(AfterSalesDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<AfterSales> UpdateAsync(AfterSalesDto dto)
    {
        throw new NotSupportedException();
    }

    public override async Task<PagedResponse<AfterSalesDto>> QueryPagingAsync(QueryAfterSalePaging dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _afterSalesRepository.GetDbContextAsync();

        var joinQuery = from afterSales in db.Set<AfterSales>().IgnoreQueryFilters().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on afterSales.OrderId equals order.Id
            select new { afterSales, order };

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            joinQuery = joinQuery.Where(x => x.order.StoreId == dto.StoreId);

        if (dto.UserId != null && dto.UserId.Value > 0)
            joinQuery = joinQuery.Where(x => x.order.UserId == dto.UserId.Value);

        var query = joinQuery.Select(x => x.afterSales);

        if (dto.IsDeleted != null)
            query = query.Where(x => x.HideForCustomer == dto.IsDeleted.Value);

        if (ValidateHelper.IsNotEmptyCollection(dto.Status))
            query = query.Where(x => dto.Status.Contains(x.AfterSalesStatusId));

        if (dto.IsAfterSalesPending ?? false)
        {
            var pendingStatus = _afterSaleUtils.PendingStatus();
            query = query.Where(x => pendingStatus.Contains(x.AfterSalesStatusId));
        }

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime <= dto.EndTime.Value);

        var count = await query.CountOrDefaultAsync(dto);

        query = query.OrderBy(x => x.HideForCustomer).ThenByDescending(x => x.CreationTime);

        var items = await query.PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var datalist = items.Select(x => ObjectMapper.Map<AfterSales, AfterSalesDto>(x)).ToArray();

        return new PagedResponse<AfterSalesDto>(datalist, dto, count);
    }

    public override Task DeleteByIdAsync(string id)
    {
        throw new NotSupportedException();
    }

    public async Task UpdateStatusAsync(UpdateAfterSaleStatusDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var db = await _afterSalesRepository.GetDbContextAsync();

        var entity = await db.Set<AfterSales>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        if (dto.IsDeleted != null)
            entity.HideForCustomer = dto.IsDeleted.Value;

        entity.LastModificationTime = Clock.Now;

        await _afterSalesRepository.UpdateAsync(entity);
    }

    public async Task<AfterSalesDto[]> AttachOrderAsync(AfterSalesDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _afterSalesRepository.GetDbContextAsync();

        var orderIds = data.Select(x => x.OrderId).Distinct().ToArray();
        if (orderIds.Any())
        {
            var orders = await db.Set<Order>().AsNoTracking().WhereIdIn(orderIds).ToArrayAsync();
            foreach (var m in data)
            {
                var order = orders.FirstOrDefault(x => x.Id == m.OrderId);
                if (order == null)
                    continue;

                m.Order = ObjectMapper.Map<Order, OrderDto>(order);
            }
        }

        return data;
    }

    public async Task<AfterSalesDto[]> AttachItemsAsync(AfterSalesDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _afterSalesRepository.GetDbContextAsync();

        var ids = data.Ids();
        var allitems = await db.Set<AfterSalesItem>().AsNoTracking().Where(x => ids.Contains(x.AfterSalesId))
            .ToArrayAsync();
        foreach (var m in data)
        {
            var items = allitems.Where(x => x.AfterSalesId == m.Id).ToArray();
            m.Items = items.Select(x => ObjectMapper.Map<AfterSalesItem, AfterSalesItemDto>(x)).ToArray();
        }

        return data;
    }

    public async Task<AfterSalesItemDto[]> AttachOrderItemsAsync(AfterSalesItemDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _afterSalesRepository.GetDbContextAsync();

        var ids = data.Select(x => x.OrderItemId).ToArray();
        var items = await db.Set<OrderItem>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToArrayAsync();
        foreach (var m in data)
        {
            var item = items.FirstOrDefault(x => x.Id == m.OrderItemId);
            if (item == null)
                continue;
            m.OrderItem = ObjectMapper.Map<OrderItem, OrderItemDto>(item);
        }

        return data;
    }

    public async Task<int> QueryPendingCountAsync(QueryAfterSalePendingCountInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _afterSalesRepository.GetDbContextAsync();

        var finishedStatus = _afterSaleUtils.DoneStatus();

        var query = from aftersales in db.Set<AfterSales>().Where(x => !finishedStatus.Contains(x.AfterSalesStatusId))
            join order in db.Set<Order>().AsNoTracking()
                on aftersales.OrderId equals order.Id
            select new { aftersales, order };

        if (dto.UserId != null && dto.UserId.Value > 0)
            query = query.Where(x => x.order.UserId == dto.UserId.Value);

        var count = await query.CountAsync();

        return count;
    }

    public async Task<AfterSalesDto> EnsureUserAfterSalesAsync(string afterSaleId, int userId)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        var db = await this.Repository.GetDbContextAsync();

        var query = from aftersales in db.Set<AfterSales>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on aftersales.OrderId equals order.Id
            select new { aftersales, order };

        var entity = await query.FirstOrDefaultAsync(x => x.aftersales.Id == afterSaleId);

        if (entity == null)
            throw new EntityNotFoundException();

        if (entity.order.UserId != userId)
            throw new PermissionRequiredException();

        return entity.aftersales.MapTo<AfterSales, AfterSalesDto>(this.ObjectMapper);
    }

    public async Task<AfterSalesDto> EnsureStoreAfterSalesAsync(string afterSaleId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var entity = await GetByIdAsync(afterSaleId);

        if (entity == null)
            throw new EntityNotFoundException();

        await _orderService.EnsureStoreOrderAsync(entity.OrderId, storeId);

        return entity;
    }

    public async Task<AfterSalesDto> QueryByOrderIdAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(QueryByOrderIdAsync));

        var db = await _afterSalesRepository.GetDbContextAsync();

        var query = db.Set<AfterSales>().AsNoTracking();

        var entity = await query.FirstOrDefaultAsync(x => x.OrderId == orderId);

        if (entity == null)
            return null;

        var dto = ObjectMapper.Map<AfterSales, AfterSalesDto>(entity);

        return dto;
    }
}