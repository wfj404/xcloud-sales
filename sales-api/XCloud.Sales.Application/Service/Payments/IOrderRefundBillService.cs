using System.Threading;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Payments;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Payments;

public interface IOrderRefundBillService : ISalesAppService
{
    Task<RefundBillDto> GetRequiredByIdAsync(string billId);

    Task<decimal> GetRestPriceToRefundAsync(string billId);

    Task<RefundBillDto[]> GetOrderRefundedBillsAsync(string orderId);

    Task<RefundBillDto[]> GetRefundedBillsAsync(string billId);

    Task<RefundBillDto> GetBillReadyForRefundAsync(string billId);

    Task<PaymentBillDto[]> AttachRefundedBillsAsync(PaymentBillDto[] data);

    Task<RefundBillDto> CreateRefundBillAsync(CreateRefundBillDto dto);

    Task MarkAsRefundedAsync(MarkRefundBillAsRefundInput dto);

    Task DeleteExpiredBillsAsync(CancellationToken cancellationToken);

    Task UpdateOrderRefundedAmountAsync(string orderId);

    [Obsolete]
    Task<RefundBillDto[]> AttachPaymentBillAsync(RefundBillDto[] data);

    [Obsolete]
    Task<PagedResponse<RefundBillDto>> QueryPagingAsync(QueryRefundBillPagingInput dto);
}

public class OrderRefundBillService : SalesAppService, IOrderRefundBillService
{
    private readonly ISalesRepository<RefundBill> _salesRepository;
    private readonly IOrderPaymentBillService _orderPaymentBillService;

    public OrderRefundBillService(ISalesRepository<RefundBill> salesRepository,
        IOrderPaymentBillService orderPaymentBillService)
    {
        _salesRepository = salesRepository;
        _orderPaymentBillService = orderPaymentBillService;
    }

    private int ExpiredInMinutes => 30;

    public async Task<RefundBillDto> GetRequiredByIdAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
            throw new ArgumentNullException(nameof(billId));

        var bill = await this._salesRepository.FirstOrDefaultAsync(x => x.Id == billId);

        if (bill == null)
        {
            throw new EntityNotFoundException(message: "refund bill not found");
        }

        return bill.MapTo<RefundBill, RefundBillDto>(this.ObjectMapper);
    }

    public async Task<RefundBillDto> GetBillReadyForRefundAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
            throw new ArgumentNullException(nameof(billId));

        var bill = await this._salesRepository.FirstOrDefaultAsync(x => x.Id == billId);

        if (bill == null)
        {
            return null;
        }

        if (bill.Refunded)
        {
            this.Logger.LogWarning(message: "bill is already refunded");
            return null;
        }

        if (bill.CreationTime < this.Clock.Now.AddMinutes(-this.ExpiredInMinutes))
        {
            this.Logger.LogWarning(message: "bill is expired");
            return null;
        }

        return bill.MapTo<RefundBill, RefundBillDto>(this.ObjectMapper);
    }

    public async Task<RefundBillDto[]> AttachPaymentBillAsync(RefundBillDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _salesRepository.GetDbContextAsync();

        var billIds = data.Select(x => x.PaymentBillId).Distinct().ToArray();
        var billList = await db.Set<PaymentBill>().AsNoTracking().WhereIdIn(billIds).ToArrayAsync();
        foreach (var m in data)
        {
            var bill = billList.FirstOrDefault(x => x.Id == m.PaymentBillId);
            if (bill == null)
                continue;

            m.PaymentBill = ObjectMapper.Map<PaymentBill, PaymentBillDto>(bill);
        }

        return data;
    }

    public async Task<PaymentBillDto[]> AttachRefundedBillsAsync(PaymentBillDto[] data)
    {
        var paidBills = data.Where(x => x.Paid).ToArray();

        if (paidBills.Any())
        {
            var ids = paidBills.Ids();

            var db = await _salesRepository.GetDbContextAsync();

            var refundBills = await db.Set<RefundBill>().AsNoTracking()
                .Where(x => x.Refunded)
                .Where(x => ids.Contains(x.PaymentBillId))
                .ToArrayAsync();

            foreach (var m in paidBills)
            {
                m.RefundBills = refundBills
                    .Where(x => x.PaymentBillId == m.Id)
                    .MapArrayTo<RefundBill, RefundBillDto>(this.ObjectMapper);
            }
        }

        return data;
    }

    public async Task<PagedResponse<RefundBillDto>> QueryPagingAsync(QueryRefundBillPagingInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _salesRepository.GetDbContextAsync();

        var query = db.Set<RefundBill>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.RefundId))
        {
            query = query.Where(x => x.RefundTransactionId == dto.RefundId);
        }

        var count = await query.CountOrDefaultAsync(dto);

        var data = await query
            .OrderBy(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = data.MapArrayTo<RefundBill, RefundBillDto>(ObjectMapper);

        return new PagedResponse<RefundBillDto>(items, dto, count);
    }

    public async Task<decimal> GetRestPriceToRefundAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
        {
            throw new ArgumentNullException(nameof(billId));
        }

        var bill = await _orderPaymentBillService.GetRequiredPaidBillByIdAsync(billId);

        var refundBills = await this.GetRefundedBillsAsync(bill.Id);

        var refundTotal = refundBills.Sum(x => x.Price);

        var remain = bill.Price - refundTotal;

        return remain;
    }

    public async Task<RefundBillDto[]> GetOrderRefundedBillsAsync(string orderId)
    {
        var bills = await this._orderPaymentBillService.GetPaidBillsAsync(orderId: orderId);

        await this.AttachRefundedBillsAsync(bills);

        var datalist = bills
            .Where(x => x.RefundBills != null)
            .SelectMany(x => x.RefundBills)
            .OrderBy(x => x.CreationTime).ToArray();

        return datalist;
    }

    public async Task<RefundBillDto[]> GetRefundedBillsAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
            throw new ArgumentNullException(nameof(billId));

        var db = await _salesRepository.GetDbContextAsync();

        var query = db.Set<RefundBill>().AsNoTracking()
            .Where(x => x.PaymentBillId == billId)
            .Where(x => x.Refunded);

        var data = await query.OrderBy(x => x.CreationTime).ToArrayAsync();

        var refundBills = data.MapArrayTo<RefundBill, RefundBillDto>(this.ObjectMapper);

        return refundBills;
    }

    public async Task<RefundBillDto> CreateRefundBillAsync(CreateRefundBillDto dto)
    {
        var bill = await this._orderPaymentBillService.GetRequiredPaidBillByIdAsync(dto.BillId);

        var restPrice = await GetRestPriceToRefundAsync(bill.Id);

        if (restPrice <= decimal.Zero)
        {
            throw new UserFriendlyException("there is no need to pay more money!");
        }

        dto.PriceOrNull ??= restPrice;

        if (dto.PriceOrNull.Value > restPrice)
        {
            throw new UserFriendlyException("bill price is bigger than the rest price");
        }

        var entity = new RefundBill
        {
            Description = dto.Description ?? string.Empty,
            PaymentBillId = bill.Id,
            Price = dto.PriceOrNull.Value,
            Id = GuidGenerator.CreateGuidString(),
            CreationTime = Clock.Now,
            Refunded = false,
            RefundTime = null,
            RefundTransactionId = string.Empty,
            RefundNotifyData = string.Empty
        };

        await this._salesRepository.InsertAsync(entity);

        return entity.MapTo<RefundBill, RefundBillDto>(ObjectMapper);
    }

    public async Task MarkAsRefundedAsync(MarkRefundBillAsRefundInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var entity = await _salesRepository.FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(MarkAsRefundedAsync));

        if (entity.Refunded)
            return;

        entity.Refunded = true;
        entity.RefundTime = dto.Time ?? Clock.Now;
        entity.RefundTransactionId = dto.RefundId;
        entity.RefundNotifyData = dto.NotifyData;

        await _salesRepository.UpdateAsync(entity);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        var bill = await this._orderPaymentBillService.GetPaidBillByIdAsync(entity.PaymentBillId);

        if (bill != null)
        {
            await this.UpdateOrderRefundedAmountAsync(bill.OrderId);
        }
    }

    public async Task UpdateOrderRefundedAmountAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var db = await this._salesRepository.GetDbContextAsync();

        var order = await db.Set<Order>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == orderId);

        if (order == null)
            return;

        var bills = await this.GetOrderRefundedBillsAsync(orderId: order.Id);
        order.RefundedAmount = bills.Sum(x => x.Price);
        order.LastModificationTime = this.Clock.Now;

        await db.TrySaveChangesAsync();
    }

    public async Task DeleteExpiredBillsAsync(CancellationToken cancellationToken)
    {
        while (!cancellationToken.IsCancellationRequested)
        {
            using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

            try
            {
                var expired = this.Clock.Now
                    .AddMinutes(-this.ExpiredInMinutes)
                    .AddDays(-300);

                var db = await _salesRepository.GetDbContextAsync();

                var query = db.Set<RefundBill>().AsTracking()
                    .Where(x => !x.Refunded)
                    .Where(x => x.CreationTime < expired);

                var datalist = await query.OrderBy(x => x.CreationTime).Take(500).ToArrayAsync();

                if (!datalist.Any())
                {
                    break;
                }

                await _salesRepository.DeleteManyAsync(datalist);

                await uow.SaveChangesAsync();

                await uow.CompleteAsync();
            }
            catch (Exception e)
            {
                this.Logger.LogError(message: e.Message, exception: e);
                await uow.RollbackAsync();
                break;
            }
        }
    }
}