import { BlockItem, BlockItemProps } from '../../utils';

//-------------------------------------------------------------
export interface HelloBlockProps extends BlockItemProps {
  helloText?: string;
}

export const hello_demo: BlockItem<HelloBlockProps> = {
  type: 'hello',
  style: {
    marginBottom: 10,
  },
  body: {},
};
