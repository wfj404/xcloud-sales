import { Badge, Button, Card, Divider, Space, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XUserAvatar from '@/components/manage/user/avatar';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { QueryUserPaging, UserDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XForm from './form';
import Mobile from './mobile';
import XPreview from './profile';
import PwdModifier from './pwd';
import XSearchForm from './search_form';
import XUpdateStatus from './status';

export default () => {
  const [query, _query] = useState<QueryUserPaging>({});
  const [data, _data] = useState<PagedResponse<UserDto>>({});
  const [loading, _loading] = useState(false);
  const [showForm, _showForm] = useState<boolean>(false);
  const [previewId, _previewId] = useState<string | undefined | null>(
    undefined,
  );

  const queryPaging = (q: QueryUserPaging) => {
    _loading(true);
    apiClient.sys
      .manageUserQueryUserPagination(q)
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryPaging(query);
  }, []);

  const columns: ColumnType<UserDto>[] = [
    {
      title: '账号主体',
      render: (record: UserDto) => {
        return (
          <div onClick={() => _previewId(record.Id)}>
            <XUserAvatar model={record} />
          </div>
        );
      },
    },
    {
      title: '电话',
      render: (record: UserDto) => {
        return (
          <Mobile
            model={record}
            ok={() => {
              queryPaging(query);
            }}
          />
        );
      },
    },
    {
      title: '活动状态',
      render: (d, x) => (
        <Badge
          status={x.IsActive ? 'success' : 'default'}
          text={x.IsActive ? '活动' : '冻结'}
        />
      ),
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (text, record) => {
        return (
          <Space
            size={'small'}
            direction={'horizontal'}
            split={<Divider type={'vertical'} />}
          >
            <PwdModifier
              model={record}
              ok={() => {
                //
              }}
            />
            <XUpdateStatus
              model={record}
              ok={() => {
                queryPaging(query);
              }}
            />
          </Space>
        );
      },
    },
  ];

  return (
    <>
      <XPreview
        userId={previewId}
        hide={() => {
          _previewId(undefined);
        }}
      />
      <XForm
        show={showForm}
        hide={() => {
          _showForm(false);
        }}
        ok={() => {
          _showForm(false);
          queryPaging(query);
        }}
      />
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
        }}
      />
      <Card
        title="平台用户"
        extra={
          <Button
            type="primary"
            onClick={() => {
              _showForm(true);
            }}
          >
            添加用户
          </Button>
        }
      >
        <Table
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            current: query.Page,
            total: data.TotalCount,
            pageSize: 20,
            onChange: (page, size) => {
              const q: QueryUserPaging = { ...query, Page: page };
              _query(q);
              queryPaging(q);
            },
          }}
        />
      </Card>
    </>
  );
};
