import { useInterval, useWebSocket } from 'ahooks';
import { ReadyState, Result } from 'ahooks/lib/useWebSocket';
import { take } from 'lodash-es';
import { useEffect, useMemo, useRef } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { MessageDto, PingMessageBody } from '@/utils/swagger';
import { getAccessToken, GlobalContainer } from '@/utils/utils';

export default () => {
  const store = useSnapshot(storeState) as StoreType;

  const messageHistory = useRef<MessageEvent[]>([]);
  const ws: Result = useWebSocket(
    'ws://localhost:6001/api/messenger/ws?device=web-pc',
    {
      manual: true,
      reconnectLimit: 3,
      reconnectInterval: 5000,
      onOpen(event, instance) {
        console.log('on-open', event);
        //auth
        sendPing();
      },
      onClose(event, instance) {
        console.log('on-close', event);
      },
      onError(event, instance) {
        console.log('on-error', event);
      },
      onMessage(message, instance) {
        console.log('on-message', message);
      },
    },
  );

  const ws_is_open = ws.readyState == ReadyState.Open;

  messageHistory.current = useMemo(() => {
    const datalist = [...messageHistory.current, ws.latestMessage]
      .filter((x) => x != undefined && x != null)
      .map((x) => x as MessageEvent);
    return take(datalist, 50);
  }, [ws.latestMessage]);

  useEffect(() => {
    //store.setMssageHistory(messageHistory.current || []);
    console.log('message-list-from-websocket:', messageHistory.current);
  }, [messageHistory.current]);

  useEffect(() => {
    GlobalContainer.sendWs = (e) => {
      ws.sendMessage(e);
    };

    return () => {
      GlobalContainer.sendWs = undefined;
    };
  }, [ws]);

  useEffect(() => {
    ws.connect();
    return () => ws.disconnect();
  }, []);

  const sendPing = () => {
    console.log('ping-ws', ws);
    const token = getAccessToken();

    const ping_body: PingMessageBody = {
      Token: token,
    };

    const msg: MessageDto = {
      MessageType: 'ping',
      Data: JSON.stringify(ping_body),
    };

    if (ws_is_open) {
      ws.sendMessage(JSON.stringify(msg));
    } else {
      console.log('ws is not open');
    }
  };

  useInterval(() => sendPing(), 1000 * 10);

  return (
    <>
      <p>state:{ws.readyState}</p>
      <button
        disabled={!ws_is_open}
        onClick={() => {
          sendPing();
        }}
      >
        ping
      </button>
      <button
        disabled={ws_is_open}
        onClick={() => {
          ws.connect();
        }}
      >
        connect
      </button>
      <button
        disabled={!ws_is_open}
        onClick={() => {
          ws.disconnect();
        }}
      >
        disconnect
      </button>
    </>
  );
};
