using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.AfterSale;

public interface IAfterSaleCommentService : ISalesAppService
{
    Task InsertAsync(AfterSalesCommentDto dto);

    Task<PagedResponse<AfterSalesCommentDto>> QueryPagingAsync(QueryAfterSalesCommentPaging dto);
}

public class AfterSaleCommentService : SalesAppService, IAfterSaleCommentService
{
    private readonly ISalesRepository<AfterSalesComment> _repository;
    private readonly IAfterSaleService _afterSaleService;
    private readonly AfterSaleUtils _afterSaleUtils;

    public AfterSaleCommentService(ISalesRepository<AfterSalesComment> repository, IAfterSaleService afterSaleService,
        AfterSaleUtils afterSaleUtils)
    {
        _repository = repository;
        _afterSaleService = afterSaleService;
        _afterSaleUtils = afterSaleUtils;
    }

    private async Task InsertCheckAsync(AfterSalesCommentDto dto)
    {
        var entity = await _afterSaleService.GetByIdAsync(dto.AfterSaleId);
        if (entity == null)
            throw new EntityNotFoundException(nameof(InsertCheckAsync));

        if (!_afterSaleUtils.PendingStatus().Contains(entity.AfterSalesStatusId))
            throw new UserFriendlyException("status error");

        var db = await _repository.GetDbContextAsync();

        var start = Clock.Now.Date;
        var end = start.AddDays(1);

        var query = db.Set<AfterSalesComment>()
            .Where(x => x.AfterSaleId == dto.AfterSaleId)
            .Where(x => !x.IsAdmin)
            .Where(x => x.CreationTime >= start && x.CreationTime < end);

        var count = await query.CountAsync();
        if (count > 5)
            throw new UserFriendlyException("try comment tomorrow");
    }

    public async Task InsertAsync(AfterSalesCommentDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.AfterSaleId))
            throw new ArgumentNullException(nameof(dto.AfterSaleId));

        await InsertCheckAsync(dto);

        var entity = ObjectMapper.Map<AfterSalesCommentDto, AfterSalesComment>(dto);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        await _repository.InsertAsync(entity);
    }

    public async Task<PagedResponse<AfterSalesCommentDto>> QueryPagingAsync(QueryAfterSalesCommentPaging dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.AfterSalesId))
            throw new ArgumentNullException(nameof(dto.AfterSalesId));

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<AfterSalesComment>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.AfterSalesId))
            query = query.Where(x => x.AfterSaleId == dto.AfterSalesId);

        var count = await query.CountOrDefaultAsync(dto);

        var list = await query
            .OrderByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = list.MapArrayTo<AfterSalesComment, AfterSalesCommentDto>(ObjectMapper);

        return new PagedResponse<AfterSalesCommentDto>(items, dto, count);
    }
}