﻿using Volo.Abp.DependencyInjection;
using XCloud.Sales.Application.Domain.Payments;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Payments;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Models;
using XCloud.Core.Extension;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.WeChat;

namespace XCloud.Sales.WeChat.Service.Payments;

[ExposeServices(typeof(WechatBillVerifyService))]
public class WechatBillVerifyService : SalesAppService
{
    private readonly ISettingService _settingService;
    private readonly WechatClientFactory _wechatClientFactory;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly ISalesRepository<PaymentBill> _salesRepository;

    public WechatBillVerifyService(ISettingService settingService,
        WechatClientFactory wechatClientFactory,
        IOrderPaymentBillService orderPaymentBillService,
        IOrderRefundBillService orderRefundBillService,
        ISalesRepository<PaymentBill> salesRepository)
    {
        _settingService = settingService;
        _wechatClientFactory = wechatClientFactory;
        _orderPaymentBillService = orderPaymentBillService;
        _orderRefundBillService = orderRefundBillService;
        _salesRepository = salesRepository;
    }

    private async Task<string> PrepareDirectoryAsync(DateTime beijingDate)
    {
        var env = this.LazyServiceProvider.LazyGetRequiredService<IWebHostEnvironment>();

        var billPath = Path.Combine(env.ContentRootPath, "payment-bills");

        billPath = Path.Combine(billPath, $"Y{beijingDate.Year}");

        //yyyy-MM-dd HH:mm:ss
        var dateString = beijingDate.ToString("MM-dd");

        var index = default(int) - 1;

        while (true)
        {
            ++index;

            var finalPath = Path.Combine(billPath, $"{dateString}_{index}");

            if (!Directory.Exists(finalPath))
            {
                await Task.CompletedTask;

                Directory.CreateDirectory(finalPath);

                return finalPath;
            }
        }
    }

    private async Task<string> DownloadBillsAsync(string savePath, DateTime beijingDate)
    {
        var client = await this._wechatClientFactory.BuildTenPayClientAsync();

        var billResponse = await client.ExecuteGetBillTradeBillAsync(new GetBillTradeBillRequest()
        {
            BillType = "ALL",
            BillDateString = beijingDate.ToString("yyyy-MM-dd")
        });

        billResponse.ThrowIfNotSuccess(this.JsonDataSerializer, "get-trade-bills");

        var downloadResponse = await client.ExecuteDownloadBillFileAsync(new DownloadBillFileRequest()
            { DownloadUrl = billResponse.DownloadUrl });

        downloadResponse.ThrowIfNotSuccess(this.JsonDataSerializer, "download-bills");

        var bytes = downloadResponse.GetRawBytes();

        var billsFilePath = Path.Combine(savePath, "all.xlsx");

        new FileInfo(billsFilePath).DeleteIfExist();

        await File.WriteAllBytesAsync(path: billsFilePath, bytes);

        return billsFilePath;
    }

    private string BeijingNow() => this.Clock.Now.AddHours(8).ToString("yyyy-MM-dd HH:mm:ss");

    private async Task ExecuteVerifyAsync(string savePath, DateTime beijingDate, WechatBillFileRecord[] datalist)
    {
        var startTime = beijingDate.AddHours(-8);
        var endTime = startTime.AddDays(1);

        var filePath = Path.Combine(savePath, "verify_result.txt");

        new FileInfo(filePath).DeleteIfExist();

        await using var writer = File.CreateText(filePath);

        await writer.WriteLineAsync($"对账结果");
        await writer.WriteLineAsync($"created at {this.BeijingNow()}");
        await writer.WriteLineAsync();

        var paymentRecords = datalist.Where(x => x.IsPayment).ToArray();
        var refundRecords = datalist.Where(x => x.IsRefund).ToArray();

        await writer.FlushAsync();

        throw new NotImplementedException();
    }

    public async Task VerifyPaymentBillsAsync(DateTime beijingDate)
    {
        var option = await this._settingService.GetWechatPaymentOptionAsync();

        if (option.Empty)
        {
            this.Logger.LogInformation(message: "skip:payment option is empty");
            return;
        }

        var savePath = await this.PrepareDirectoryAsync(beijingDate);

        var billsFilePath = await this.DownloadBillsAsync(savePath, beijingDate);

        var datalist = await WechatBillParserHelper.FromExcelAsync(billsFilePath);

        await this.ExecuteVerifyAsync(savePath, beijingDate, datalist);
    }
}