import { Alert, Button, Card, message, Modal, Space, Spin } from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { apiClient } from '@/utils/client';
import { GoodsDto, SkuDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XForm from './form';
import XPrice from './price';
import XSpecValueMapping from './spec_combination';
import XStoreMapping from './store_mapping';

export default ({
  goods,
  skuId,
  show,
  hide,
  ok,
}: {
  goods?: GoodsDto;
  skuId?: string;
  ok: (d: SkuDto) => void;
  hide: () => void;
  show: boolean;
}) => {
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<SkuDto>({});

  useEffect(() => {
    if (show) {
      //
    } else {
      _data({});
    }
  }, [show]);

  const save = (e: SkuDto) => {
    if (goods != null) {
      e = {
        ...e,
        Goods: goods,
        GoodsId: goods.Id,
      };
    }
    if (isStringEmpty(e.GoodsId)) {
      message.error('请选择标准商品');
      return;
    }

    e.Name = e.Name?.trim();
    if (isStringEmpty(e.Name)) {
      return;
    }

    _loading(true);
    apiClient.mallAdmin
      .skuSaveSkus({ ...e })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok && ok(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const queryDetail = () => {
    if (isStringEmpty(skuId)) {
      _data({
        Name: '默认',
        Price: 99999,
        IsActive: true,
      });
      return;
    }
    _loading(true);
    apiClient.mallAdmin
      .skuGetById({ Id: skuId })
      .then((res) => {
        _data(res.data.Data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryDetail();
  }, [skuId]);

  const renderWithCheck = (com: React.ReactElement) => {
    if (isStringEmpty(data.Id)) {
      return (
        <Card style={{ margin: 10 }}>
          <Alert message="请先保存商品再编辑此模块"></Alert>
        </Card>
      );
    }

    return <>{com}</>;
  };

  return (
    <>
      <Modal
        title="商品明细"
        width={'95%'}
        open={show}
        confirmLoading={loading}
        onCancel={() => {
          hide();
        }}
        onOk={() => {
          save(data);
        }}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                const res = await apiClient.mallAdmin.skuDelete({
                  Id: data.Id,
                });
                handleResponse(res, () => {
                  hide();
                  ok({ ...data, IsDeleted: true });
                });
              }}
              hide={isStringEmpty(data.Id)}
            />
            <Button
              type={'primary'}
              onClick={() => {
                save(data);
              }}
            >
              保存
            </Button>
          </Space>
        }
        forceRender
        destroyOnClose
      >
        <Spin spinning={loading}>
          <Space
            direction="vertical"
            style={{
              width: '100%',
            }}
          >
            <XForm
              goods={goods}
              data={data}
              _data={(d) => {
                _data(d || {});
              }}
            />
            {renderWithCheck(
              <XSpecValueMapping
                model={data}
                ok={(e) => {
                  _data({
                    ...data,
                    SpecItemList: e || [],
                  });
                }}
              />,
            )}
            <XPrice
              data={data}
              _data={(d) => {
                _data(d || {});
              }}
            />
            <XStoreMapping
              data={data}
              _data={(d) => {
                _data(d || {});
              }}
            />
          </Space>
        </Spin>
      </Modal>
    </>
  );
};
