import { SkuDto } from '@/utils/swagger';
import { Box, Tooltip, Typography } from '@mui/material';
import { useMemo } from 'react';
import { formatMoney } from '@/utils/utils';

export default function({ model }: { model: SkuDto }) {
  if (model == null) {
    return null;
  }

  if (model.PriceModel == null || model.PriceModel.Empty) {
    return (
      <Tooltip title={'价格登录可见'}>
        <Typography
          variant='caption'
          color={'text.disabled'}
          sx={{
            whiteSpace: 'nowrap',
            fontWeight: 'lighter',
          }}
        >
          登录见价格
        </Typography>
      </Tooltip>
    );
  }

  const originPrice = model.PriceModel?.BasePrice || 0;
  const finalPrice = model.PriceModel?.FinalPrice || 0;

  const tips = useMemo<string[]>(() => {
    const data: string[] = [];

    if (model.PriceModel == null) {
      return data;
    }

    if (model.PriceModel.DiscountModel != null) {
      let rate = model.PriceModel.DiscountModel.DiscountPercentage || 0;
      data.push(`折扣-${model.PriceModel.DiscountModel.Name}:${rate * 100}%`);
    }

    if (model.PriceModel.GradePriceModel != null) {
      let offset = model.PriceModel.GradePriceModel.PriceOffset || 0;
      if (offset != 0) {
        data.push(`${model.PriceModel.GradePriceModel.Grade?.Name || '会员价'}:${offset}`);
      }
    }

    return data;
  }, [model]);

  const renderPriceBox = () => {
    return (
      <Box
        sx={{
          display: 'inline-block',
        }}
      >
        {originPrice == finalPrice || (
          <Typography
            variant='caption'
            sx={{
              color: 'text.disabled',
              textDecoration: 'line-through',
              fontSize: '9px',
              whiteSpace: 'nowrap',
              fontWeight: 'lighter',
            }}
          >
            {`${formatMoney(originPrice)}`}
          </Typography>
        )}
        <Typography
          color='black'
          variant='button'
          sx={{
            whiteSpace: 'nowrap',
            //fontWeight: 'lighter',
          }}
        >
          {formatMoney(finalPrice)}
        </Typography>
      </Box>);
  };

  return (
    <>
      {tips.length <= 0 && renderPriceBox()}
      {tips.length <= 0 ||
        <Tooltip
          title={
            <p>
              {tips.map((x, i) => <p key={i}>{i + 1}:{x}</p>)}
            </p>
          }>
          {renderPriceBox()}
        </Tooltip>}
    </>
  );
}
