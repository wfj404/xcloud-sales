﻿using System.Threading.Tasks;
using XCloud.Platform.Application.Exceptions;

namespace XCloud.Platform.Application.Service.Permissions;

public static class PermissionExtension
{
    public static async Task CheckRequiredSuperAdminAsync(this IAdminSecurityService adminSecurityService,
        string adminId)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        var response =
            await adminSecurityService.GetGrantedPermissionsAsync(
                adminId,
                new CacheStrategy { Cache = true });

        if (!response.IsPermissionValid("SUPER-ADMIN"))
        {
            throw new PermissionRequiredException($"super admin is required");
        }
    }

    public static async Task CheckRequiredPermissionAsync(this IAdminSecurityService adminSecurityService,
        string adminId, string permissionKey)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        if (string.IsNullOrWhiteSpace(permissionKey))
            throw new ArgumentNullException(nameof(permissionKey));

        var response =
            await adminSecurityService.GetGrantedPermissionsAsync(
                adminId,
                new CacheStrategy { Cache = true });

        if (!response.IsPermissionValid(permissionKey))
        {
            throw new PermissionRequiredException($"permission:${permissionKey} is required");
        }
    }
}