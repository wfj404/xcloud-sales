﻿using XCloud.Sales.Application.Domain.Common;

namespace XCloud.Sales.Application.Service.Common;

public static class CommonExtension
{
    public static void EnsureStoreId(this OrderGroup group)
    {
        if (string.IsNullOrWhiteSpace(group.StoreId))
        {
            throw new ArgumentException(message: $"{nameof(group.StoreId)}");
        }
    }
}