import { Badge, Button, Card, Table, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XRenderTime from '@/components/common/time/render_time';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { DiscountDto, QueryCouponPagingInput } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { PlusOutlined } from '@ant-design/icons';

import XForm from './form';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<DiscountDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QueryCouponPagingInput>({
    Page: 1,
  });
  const [formData, _formData] = useState<DiscountDto | undefined>(undefined);

  const queryList = () => {
    _loading(true);
    apiClient.mallAdmin
      .discountQueryPaging({ ...query })
      .then((res) => {
        _data(res.data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnProps<DiscountDto>[] = [
    {
      title: '名称',
      render: (d, x: DiscountDto) => (
        <>
          <b>{x.Name || '--'}</b>
          {isStringEmpty(x.Description) || <p>{x.Description}</p>}
        </>
      ),
    },
    {
      title: '优惠力度',
      render: (d, x: DiscountDto) => x.DiscountPercentage || 0,
    },
    {
      title: '适用门店',
      render: (d, x: DiscountDto) => {
        if (!x.LimitedToStore) {
          return <Tag color={'green'}>全部门店</Tag>;
        }
        return <span>{`${x.Stores?.map((x) => x.Name || '').join(',')}`}</span>;
      },
    },
    {
      title: '适用商品',
      render: (d, x: DiscountDto) => {
        if (!x.LimitedToSku) {
          return <Tag color={'green'}>全局有效</Tag>;
        }

        if (!x.Skus) {
          return null;
        }

        return x.Skus?.map((x, i) => (
          <div
            key={i}
            style={{
              marginBottom: 5,
            }}
          >{`${i + 1}-${x.Goods?.Name}-${x.Name}`}</div>
        ));
      },
    },
    {
      title: '状态',
      render: (d, x: DiscountDto) =>
        x.IsActive ? (
          <Badge text={'可用'} status={'success'} />
        ) : (
          <Badge text={'不可用'} status={'error'} />
        ),
    },
    {
      title: '时间范围',
      render: (d, x: DiscountDto) => {
        return (
          <>
            {x.StartTime && (
              <div>
                开始时间：
                <XRenderTime timeStr={x.StartTime} />
              </div>
            )}
            {x.EndTime && (
              <div>
                结束时间：
                <XRenderTime timeStr={x.EndTime} />
              </div>
            )}
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, record: DiscountDto) => {
        if (record.IsDeleted) {
          return null;
        }
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, [query]);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title="折扣"
        extra={
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
