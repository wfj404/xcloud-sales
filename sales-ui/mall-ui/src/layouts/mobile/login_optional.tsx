import { useOutlet } from 'umi';
import ValidateStoreUser from '@/components/login/store_user';

export default () => {

  const children = useOutlet();

  return <>
    <ValidateStoreUser optional>
      {children}
    </ValidateStoreUser>
  </>;
};
