using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using XCloud.Application.DistributedLock;
using XCloud.Sales.Application.Configuration;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Database;

public static class DatabaseExtension
{
    public static async Task TryCreateSalesDatabase(this IApplicationBuilder app)
    {
        using var s = app.ApplicationServices.CreateScope();

        var config = s.ServiceProvider.GetRequiredService<IConfiguration>();

        if (config.GetSalesOptionOrDefault().AutoCreateDatabase)
        {
            await using var dlock = await s.ServiceProvider.GetRequiredService<IDistributedLockProvider>()
                .CreateLockAsync("try-create-sales-database-lock-key", TimeSpan.FromSeconds(30));

            if (dlock.IsAcquired)
            {
                var repository = s.ServiceProvider.GetRequiredService<ISalesRepository<StoreUser>>();
                await using var db = await repository.GetDbContextAsync();

                await db.Database.EnsureCreatedAsync();
            }
            else
            {
                var logger = s.ServiceProvider.GetRequiredService<ILogger<SalesApplicationModule>>();
                logger.LogWarning("failed to get dlock to create platform database");
            }
        }
    }
}