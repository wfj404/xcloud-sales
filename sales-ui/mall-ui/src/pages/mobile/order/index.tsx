import { useInfiniteScroll, useTitle } from 'ahooks';
import { Segmented } from 'antd';
import { useEffect, useState } from 'react';

import XDetailDialog from '@/components/common/modal_pure';
import ScrollLoading from '@/components/common/scroll/loading';
import XHeader from '@/components/mobile/simpleHeader';
import { OrderStatus, PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto, QueryOrderPagingInput } from '@/utils/swagger';
import { isArrayEmpty, isStringEmpty } from '@/utils/utils';
import { Badge, Box, Container } from '@mui/material';

import XDetail from './detail';
import XEmpty from './empty';
import XItem from './item';

type TabKeyType = 'all' | 'pending' | 'delivering' | 'complete' | 'aftersale';

interface TabItem {
  key: TabKeyType;
  name: string;
  count?: number;
  filter?: QueryOrderPagingInput;
}

export default function () {
  useTitle('订单');

  const [finalQuery, _finalQuery] = useState<QueryOrderPagingInput | undefined>(
    undefined,
  );

  const [pendingAftersaleCount, _pendingAftersaleCount] = useState<number>(0);

  const [selectedTab, _selectedTab] = useState<TabKeyType>('all');
  const [detailId, _detailId] = useState<string | null | undefined>(undefined);

  const { loading, loadingMore, mutate, noMore, data } = useInfiniteScroll<{
    list: OrderDto[];
    res?: PagedResponse<OrderDto>;
  }>(
    async (currentData) => {
      if (finalQuery == undefined) {
        return {
          list: [],
        };
      }

      const res = await apiClient.mall.orderQueryOrderList({
        ...finalQuery,
        Page: (currentData?.res?.PageIndex || 0) + 1,
      });

      return {
        list: res.data.Items || [],
        res: res.data,
      };
    },
    {
      target: window.document,
      isNoMore: (x) => x?.res?.IsEmpty || false,
      reloadDeps: [finalQuery],
    },
  );

  const items = data?.list || [];

  const queryPendingAftersaleCount = () => {
    apiClient.mall.afterSaleAfterSalePendingCount({}).then((res) => {
      _pendingAftersaleCount(res.data.Data || 0);
    });
  };

  const tabs: TabItem[] = [
    {
      key: 'pending',
      name: '进行中',
      filter: {
        Status: [OrderStatus.Pending, OrderStatus.Processing],
        IsAfterSales: false,
      },
    },
    {
      key: 'all',
      name: '历史订单',
      filter: {
        Status: null,
        IsAfterSales: false,
      },
    },
    {
      key: 'aftersale',
      name: '售后',
      count: pendingAftersaleCount,
      filter: {
        Status: [OrderStatus.AfterSales],
      },
    },
  ];

  useEffect(() => {
    queryPendingAftersaleCount();
  }, []);

  useEffect(() => {
    mutate({
      list: [],
    });
  }, [finalQuery]);

  useEffect(() => {
    const selectedTabData = tabs.find((x) => x.key == selectedTab);
    if (selectedTabData == undefined) {
      return;
    }

    const p: QueryOrderPagingInput = {
      ...finalQuery,
      ...selectedTabData.filter,
      Page: 1,
    };
    _finalQuery(p);
  }, [selectedTab]);

  const renderTab = () => {
    return (
      <Box sx={{ px: 1 }} style={{ width: '100%' }}>
        <Segmented
          block
          options={tabs.map((x) => ({
            label: (
              <Badge
                badgeContent={x.count}
                anchorOrigin={{
                  horizontal: 'right',
                  vertical: 'top',
                }}
                color="error"
                variant="dot"
                invisible={!(x.count && x.count > 0)}
              >
                <span>{x.name}</span>
              </Badge>
            ),
            value: x.key,
          }))}
          value={selectedTab}
          onChange={(e) => {
            _selectedTab(e as TabKeyType);
          }}
        />
      </Box>
    );
  };

  const renderItemList = () => {
    if (loading) {
      return null;
    }

    if (isArrayEmpty(items)) {
      return <XEmpty />;
    }

    return (
      <div style={{}}>
        {items.map((x, index) => (
          <Box
            sx={{ my: 2 }}
            onClick={() => {
              _detailId(x.Id);
            }}
            key={index}
          >
            <XItem model={x} />
          </Box>
        ))}
      </div>
    );
  };

  return (
    <>
      <XHeader>
        <Container disableGutters maxWidth="sm">
          <XDetailDialog
            open={!isStringEmpty(detailId)}
            onClose={() => {
              _detailId(undefined);
            }}
          >
            <XDetail
              orderId={detailId || ''}
              ok={() => {
                //trigger refresh list
                queryPendingAftersaleCount();
              }}
            />
          </XDetailDialog>

          {renderTab()}

          <div style={{ marginBottom: 8 }}>{renderItemList()}</div>

          <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
        </Container>
      </XHeader>
    </>
  );
}
