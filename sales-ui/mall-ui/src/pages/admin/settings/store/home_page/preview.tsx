import { Button, Modal, Typography } from 'antd';
import { useState } from 'react';

import { BlockItem, BlockItemProps } from '@/components/blocks/utils';

export default function ({ blocks }: { blocks: BlockItem<BlockItemProps>[] }) {
  const [jsonPreview, _jsonPreview] = useState(false);

  return (
    <>
      <Modal
        title={'配置参数'}
        width={'90%'}
        open={jsonPreview}
        onOk={() => _jsonPreview(false)}
        onCancel={() => _jsonPreview(false)}
      >
        {jsonPreview && (
          <Typography.Paragraph code copyable>
            {JSON.stringify(blocks || [])}
          </Typography.Paragraph>
        )}
      </Modal>
      <Button size={'small'} type={'link'} onClick={() => _jsonPreview(true)}>
        data-preview
      </Button>
    </>
  );
}
