import { useRequest } from 'ahooks';
import { Spin } from 'antd';
import { ReactNode, useEffect } from 'react';
import { useLocation, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isManagerLogin, isSh3H } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { hasAccessToken } from '@/utils/utils';

import ManagerDisabled from './disabled';
import SelectManagerToLogin from './select_manager';
import SH3HNotLogin from './sh3h/not_login';
import { StoreManagerAuthResult } from '@/utils/swagger';

export default function({ children }: { children: ReactNode }) {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const model = app.storeManagerAuthResult?.StoreManager || {};

  const query_login_request = useRequest(
    apiClient.mallManager.storeManagerAuthAuth,
    {
      manual: true,
      onSuccess(data, params) {
        const result: StoreManagerAuthResult = data.data || {};
        app.setStoreManagerAuthResult(result);
      },
    },
  );

  useEffect(() => {
    if (hasAccessToken()) {
      query_login_request.run();
    } else {
      app.setStoreManagerAuthResult(undefined);
      app.setManagerPermissions(undefined);
    }
  }, [location.pathname]);

  if (!isManagerLogin(model) && query_login_request.loading) {
    return (
      <Spin
        spinning={query_login_request.loading}
        size='large'
        fullscreen
        tip={<p>加载登录信息...</p>}
      />
    );
  }

  if (!isManagerLogin(model)) {
    if (isSh3H()) {
      return <SH3HNotLogin />;
    }

    return (<SelectManagerToLogin />);
  }

  if (!model.IsActive) {
    return (<ManagerDisabled />);
  }

  return <>{children}</>;
}
