﻿using Volo.Abp.Application.Dtos;
using XCloud.Platform.Application.Domain.Dept;

namespace XCloud.Platform.Application.Service.Dept;

public class DepartmentDto : Department, IEntityDto<string>
{
    public int? MemberCount { get; set; }
}