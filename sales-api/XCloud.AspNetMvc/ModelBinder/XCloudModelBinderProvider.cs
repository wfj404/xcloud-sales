﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Threading;

namespace XCloud.AspNetMvc.ModelBinder;

/// <summary>
/// https://docs.microsoft.com/zh-cn/aspnet/core/mvc/advanced/custom-model-binding?view=aspnetcore-3.1#implementing-a-modelbinderprovider
/// </summary>
public class XCloudModelBinderProvider : IModelBinderProvider
{
    public XCloudModelBinderProvider()
    {
        //
    }

    public IModelBinder GetBinder(ModelBinderProviderContext context)
    {
        if (context.Metadata.ModelType == typeof(CancellationToken) ||
            context.Metadata.ModelType == typeof(CancellationToken?))
        {
            return new CancellationTokenBinder();
        }

        return null;
    }
}