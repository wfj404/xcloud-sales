import { AfterSalesStatus, AfterSalesStatusList } from '@/utils/biz';
import { AfterSalesDto } from '@/utils/swagger';
import { Badge } from 'antd';

export default function(props: { model: AfterSalesDto }) {
  const { model } = props;

  const name =
    AfterSalesStatusList.find((x) => x.id == model.AfterSalesStatusId)?.name ||
    '--';

  if (model.AfterSalesStatusId == AfterSalesStatus.Complete) {
    return <Badge text={name} status={'success'} />;
  }

  return <Badge text={name} status={'default'} />;
}
