﻿using System.Threading.Tasks;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Users;

public enum AccessTokenTypeEnum : int
{
    Client = 1,
    User = 2
}

public interface IExternalAccessTokenService : IXCloudAppService
{
    Task CleanExpiredTokenAsync();

    Task InsertAccessTokenAsync(UserExternalAccessTokenDto dto);

    Task<ExternalAccessToken> GetValidClientAccessTokenAsync(GetValidClientAccessTokenDto dto);

    Task<ExternalAccessToken> GetValidUserAccessTokenAsync(GetValidUserAccessTokenDto dto);
}

public class ExternalAccessTokenService : PlatformAppService, IExternalAccessTokenService
{
    private readonly IPlatformRepository<ExternalAccessToken> _accessTokenRepository;

    public ExternalAccessTokenService(IPlatformRepository<ExternalAccessToken> accessTokenRepository)
    {
        _accessTokenRepository = accessTokenRepository;
    }

    public async Task InsertAccessTokenAsync(UserExternalAccessTokenDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var entity = ObjectMapper.Map<UserExternalAccessTokenDto, ExternalAccessToken>(dto);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        await _accessTokenRepository.InsertAsync(entity);
    }

    public async Task<ExternalAccessToken> GetValidUserAccessTokenAsync(GetValidUserAccessTokenDto dto)
    {
        var db = await _accessTokenRepository.GetDbContextAsync();

        var query = db.Set<ExternalAccessToken>().AsNoTracking();

        var tokenType = (int)AccessTokenTypeEnum.User;
        query = query.Where(x => x.AccessTokenType == tokenType);
        query = query.Where(x => x.Platform == dto.Platform && x.AppId == dto.AppId && x.UserId == dto.UserId);

        var now = Clock.Now;

        query = query.Where(x => x.ExpiredAt > now);

        var token = await query.OrderByDescending(x => x.CreationTime).FirstOrDefaultAsync();

        return token;
    }

    public async Task<ExternalAccessToken> GetValidClientAccessTokenAsync(GetValidClientAccessTokenDto dto)
    {
        var db = await _accessTokenRepository.GetDbContextAsync();

        var query = db.Set<ExternalAccessToken>().AsNoTracking();

        var tokenType = (int)AccessTokenTypeEnum.Client;
        query = query.Where(x => x.AccessTokenType == tokenType);
        query = query.Where(x => x.Platform == dto.Platform && x.AppId == dto.AppId);

        var now = Clock.Now;

        query = query.Where(x => x.ExpiredAt > now);

        var token = await query.OrderByDescending(x => x.CreationTime).FirstOrDefaultAsync();

        return token;
    }

    public async Task CleanExpiredTokenAsync()
    {
        while (true)
        {
            var uow = UnitOfWorkManager.Begin(requiresNew: true);
            try
            {
                var db = await _accessTokenRepository.GetDbContextAsync();

                var set = db.Set<ExternalAccessToken>();

                var now = Clock.Now;

                var list = await set.Where(x => x.ExpiredAt <= now).OrderBy(x => x.CreationTime).Take(100)
                    .ToArrayAsync();
                if (!list.Any())
                    break;

                set.RemoveRange(list);

                await db.SaveChangesAsync();
                await uow.CompleteAsync();
            }
            catch (Exception e)
            {
                await uow.RollbackAsync();
                Logger.LogError(message: e.Message, exception: e);
                break;
            }
        }
    }
}