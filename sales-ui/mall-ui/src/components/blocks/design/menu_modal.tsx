import { Col, Modal, Row } from 'antd';

import { AddComponentsMenuItems } from '@/components/blocks/utils';
import { Box } from '@mui/material';

import { BlockItem, BlockItemProps, menuToBlockItem } from '../utils';
import XMenuItem from '../editor/menu_item';

export default ({
                  show,
                  onBlockSelect,
                  onClose,
                }: {
  show: boolean;
  onBlockSelect: (x: BlockItem<BlockItemProps>) => void;
  onClose: () => void;
}) => {
  return (
    <>
      <Modal
        title={'添加组件'}
        open={show}
        onOk={() => onClose()}
        onCancel={() => onClose()}
      >
        <Row gutter={[10, 10]}>
          {AddComponentsMenuItems.map((x, i) => {
            return (
              <Col key={i} span={6}>
                <Box
                  onClick={() => {
                    const item = menuToBlockItem(x);
                    onBlockSelect && onBlockSelect(item);
                  }}
                >
                  <XMenuItem x={x} />
                </Box>
              </Col>
            );
          })}
        </Row>
      </Modal>
    </>
  );
};
