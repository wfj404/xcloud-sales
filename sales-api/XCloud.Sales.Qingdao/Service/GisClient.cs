﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Gis;
using XCloud.Application.Service;
using XCloud.Core.Extension;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Exceptions;

namespace XCloud.Sales.Qingdao.Service;

[ExposeServices(typeof(GisClient))]
public class GisClient : XCloudAppService
{
    private readonly HttpClient _httpClient;

    public GisClient(IHttpClientFactory httpClientFactory)
    {
        this._httpClient = httpClientFactory.CreateClient(nameof(GisClient));
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<PipelineTypeLocation> TransformCoordinatesToPipelineTypeAsync(GeoLocationDto location)
    {
        var trace = new Dictionary<string, string>();
        try
        {
            var config = this.Configuration.GetRequired3HConfig();
            var url = new[]
            {
                config.GisUrl,
                $"/CityInterface/rest/services/Zd_Civ_Interface.svc",
                $"/ToolOper/WgsGpsToGisXY?lon={location.Lon}&lat={location.Lat}"
            }.ConcatUrl();
            trace[nameof(url)] = url;

            using var request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;

            //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            using var res = await this._httpClient.SendAsync(request);

            //res.EnsureSuccessStatusCode();

            var json = await res.Content.ReadAsStringAsync();

            trace[nameof(json)] = json;

            var response = this.JsonDataSerializer.DeserializeFromString<GisResponse<PipelineTypeLocation>>(json);

            this.Logger.LogInformation(message: $"{trace.ToUrlQueryString()}");

            return response.data;
        }
        catch (Exception e)
        {
            var exception = new WapException(message: nameof(GetAuthCodeAsync), e)
                .WithData(nameof(trace), trace.ToUrlQueryString());

            this.Logger.LogWarning(message: nameof(GetAuthCodeAsync), exception: exception);

            return null;
        }
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<GisArea> GetGisAreaAsync(string authId, string bizType, PipelineTypeLocation location)
    {
        if (string.IsNullOrWhiteSpace(authId))
            throw new ArgumentNullException(nameof(authId));

        if (string.IsNullOrWhiteSpace(bizType))
            throw new ArgumentNullException(nameof(bizType));

        var trace = new Dictionary<string, string>();
        try
        {
            var encodedBizType = HttpUtility.UrlEncode(bizType);
            var config = this.Configuration.GetRequired3HConfig();
            var url = new[]
            {
                config.GisUrl,
                $"/CityInterface/rest/services/GeneralPorject.svc/QDProject",
                $"/GetAreaNameAndGroupNo?BusinessType={encodedBizType}&position={location.x},{location.y}"
            }.ConcatUrl();
            trace[nameof(url)] = url;

            using var request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            request.Headers.Add("Cookie", $"authorizationID={authId}");
            request.Headers.Add("Referrer", this.Configuration["app:current_ip"]);

            //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            using var res = await this._httpClient.SendAsync(request);

            //res.EnsureSuccessStatusCode();

            var json = await res.Content.ReadAsStringAsync();

            trace[nameof(json)] = json;

            var response = this.JsonDataSerializer.DeserializeFromString<GisResponse<GisArea>>(json);

            this.Logger.LogInformation(message: $"{trace.ToUrlQueryString()}");

            return response.data;
        }
        catch (Exception e)
        {
            var exception = new WapException(message: nameof(GetAuthCodeAsync), e)
                .WithData(nameof(trace), trace.ToUrlQueryString());

            this.Logger.LogWarning(message: nameof(GetAuthCodeAsync), exception: exception);

            return null;
        }
    }

    [NotNull]
    [ItemCanBeNull]
    public async Task<string> GetAuthCodeAsync()
    {
        var trace = new Dictionary<string, string>();
        try
        {
            var config = this.Configuration.GetRequired3HConfig();
            var url = new[]
            {
                config.GisUrl,
                $"/CityInterface/rest/services.svc",
                $"/GetAuthorityCodeForThirdParty?userName={config.GisUserName}"
            }.ConcatUrl();
            trace[nameof(url)] = url;

            using var request = new HttpRequestMessage();
            request.RequestUri = new Uri(url);
            request.Method = HttpMethod.Get;
            //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            using var res = await this._httpClient.SendAsync(request);

            //res.EnsureSuccessStatusCode();

            var json = await res.Content.ReadAsStringAsync();

            trace[nameof(json)] = json;

            var response = this.JsonDataSerializer.DeserializeFromString<GisAuthResponse>(json);

            this.Logger.LogInformation(message: $"{trace.ToUrlQueryString()}");

            return response.authorId;
        }
        catch (Exception e)
        {
            var exception = new WapException(message: nameof(GetAuthCodeAsync), e)
                .WithData(nameof(trace), trace.ToUrlQueryString());

            this.Logger.LogWarning(message: nameof(GetAuthCodeAsync), exception: exception);

            return null;
        }
    }
}