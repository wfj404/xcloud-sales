import { UniqueIdentifier } from '@dnd-kit/core';
import { CSSProperties, ReactNode } from 'react';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

export default ({
                  id,
                  children,
                }: {
  id: UniqueIdentifier;
  children?: ReactNode;
}) => {
  const {
    setNodeRef,
    setActivatorNodeRef,
    listeners,
    transform,
    transition,
    attributes,
    isDragging,
    isOver,
  } = useSortable({ id });

  const styles: CSSProperties = {
    transform: CSS.Transform.toString(transform),
    transition: transition,
    border: 'none',
  };

  if (isDragging) {
    styles.border = '3px dashed orange';
  } else if (isOver) {
    //styles.border = '3px dashed green';
  }

  return (
    <div ref={setNodeRef} {...attributes} {...listeners} style={styles}>
      <div
        style={{
          display: 'none',
        }}
      >
        {id}
      </div>
      {children == undefined ? id : children}
    </div>
  );
}
