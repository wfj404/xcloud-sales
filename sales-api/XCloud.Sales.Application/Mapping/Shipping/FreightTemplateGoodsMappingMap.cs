using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Mapping.Shipping;

public class FreightTemplateGoodsMappingMap : SalesEntityTypeConfiguration<FreightTemplateGoodsMapping>
{
    public override void Configure(EntityTypeBuilder<FreightTemplateGoodsMapping> builder)
    {
        builder.ToTable("sales_freight_template_goods_mapping");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.StoreId).RequiredId();
        builder.Property(x => x.TemplateId).RequiredId();
        builder.Property(x => x.GoodsId).RequiredId();

        base.Configure(builder);
    }
}