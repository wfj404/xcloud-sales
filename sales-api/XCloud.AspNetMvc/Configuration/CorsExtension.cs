﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Volo.Abp.Modularity;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.Configuration;

public static class CorsExtension
{
    public static string[] GetCorsOrigins(this IConfiguration config)
    {
        var originConfig = config["CorsHosts"];

        var origins = originConfig?.Split(',', ';', '\t', '\n').WhereNotEmpty().Distinct().ToArray();

        return origins ?? Array.Empty<string>();
    }

    public static void AddConfiguredCors(this WebSocketOptions options, IConfiguration configuration)
    {
        var cors = configuration.GetCorsOrigins();
        cors.ToList().ForEach(options.AllowedOrigins.Add);
    }

    public static void AddConfiguredCors(this WebSocketOptions options, ServiceConfigurationContext context)
    {
        var env = context.Services.GetHostingEnvironment();

        if (env.IsDevelopment())
        {
            options.AllowedOrigins.Add("*");
        }
        else
        {
            var configuration = context.Services.GetConfiguration();
            options.AddConfiguredCors(configuration);
        }
    }

    public static void UseRequiredCors(this IApplicationBuilder app, string[] origins)
    {
        if (!origins.Any())
            throw new ArgumentNullException(nameof(origins));

        using var s = app.ApplicationServices.CreateScope();
        var logger = s.ServiceProvider.GetRequiredService<ILoggerFactory>().CreateLogger(nameof(UseRequiredCors));
        origins.ToList().ForEach(x => logger.LogInformation($"use cors host:{x}"));

        app.UseCors(option =>
        {
            if (origins.Contains("*"))
            {
                option
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    //.AllowCredentials()
                    .AllowAnyOrigin();
            }
            else
            {
                option
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .WithOrigins(origins);
            }
        });
    }

    public static void TryUseCors(this IApplicationBuilder app)
    {
        using var s = app.ApplicationServices.CreateScope();
        var config = s.ServiceProvider.GetRequiredService<IConfiguration>();
        var origins = GetCorsOrigins(config);

        if (!origins.Any())
            return;

        UseRequiredCors(app, origins);
    }
}