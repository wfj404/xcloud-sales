﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.GenericOrders;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.GenericOrders;

public class GenericOrderDto : GenericOrder, IEntityDto<string>
{
    public OrderDto Order { get; set; }

    public StoreUserDto StoreUser { get; set; }
    
    public GenericOrderItemDto[] Items { get; set; }
}

[Serializable]
public class GenericOrderItemDto : IEntityDto
{
    public string GoodsName { get; set; }

    public StorageMetaDto[] Pictures { get; set; }

    public int? Quantity { get; set; }

    public decimal? Price { get; set; }
}

public class QueryGenericOrderInput : PagedRequest
{
    public int UserId { get; set; }

    public bool? ToService { get; set; }

    public bool? Serviced { get; set; }

    public bool? Reviewed { get; set; }

    public bool? Paid { get; set; }

    public bool? Finished { get; set; }

    public bool? Closed { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }
}