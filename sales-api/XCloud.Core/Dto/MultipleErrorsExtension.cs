﻿namespace XCloud.Core.Dto;

public static class MultipleErrorsExtension
{
    public static bool HasNoError<T, E>(this MultipleErrorsDto<T, E> response)
    {
        return !response.ErrorList.Any();
    }

    public static void SetData<T, E>(this MultipleErrorsDto<T, E> response, T data)
    {
        response.Data = data;
    }
}