﻿namespace XCloud.Platform.Application.Domain.Address;

public class UserAddress : PlatformBaseEntity, IHasCreationTime, IHasDeletionTime
{
    public string UserId { get; set; }

    public double? Lat { get; set; }
    
    public double? Lon { get; set; }

    public string Name { get; set; }

    public string NationCode { get; set; }
    
    public string Nation { get; set; }

    public string ProvinceCode { get; set; }
    
    public string Province { get; set; }

    public string CityCode { get; set; }
    
    public string City { get; set; }

    public string AreaCode { get; set; }
    
    public string Area { get; set; }

    public string AddressDescription { get; set; }

    public string AddressDetail { get; set; }

    public string PostalCode { get; set; }

    public string Tel { get; set; }

    public bool IsDefault { get; set; }

    public string ExtendedJson { get; set; }

    public DateTime? DeletionTime { get; set; }
    
    public bool IsDeleted { get; set; }

    public DateTime CreationTime { get; set; }
}