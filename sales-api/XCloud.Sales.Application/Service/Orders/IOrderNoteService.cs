using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Json;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderNoteService : ISalesIntCrudAppService<OrderNote, OrderNoteDto, PagedRequest>
{
    Task<OrderNoteDto[]> QueryOrderNotesAsync(QueryOrderNotesInput dto);
}

public class OrderNoteService : SalesIntCrudAppService<OrderNote, OrderNoteDto, PagedRequest>, IOrderNoteService
{
    public OrderNoteService(ISalesRepository<OrderNote> repository) : base(repository)
    {
        //
    }

    protected override async Task SetFieldsBeforeInsertAsync(OrderNote entity)
    {
        await Task.CompletedTask;

        entity.CreationTime = Clock.Now;
    }

    public override async Task<OrderNote> InsertAsync(OrderNoteDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.OrderId))
            throw new ArgumentNullException(nameof(dto.OrderId));

        dto.Id = default;

        if (dto.OrderNoteExtended != null)
        {
            dto.ExtendedJson = this.JsonDataSerializer.SerializeToString(dto.OrderNoteExtended);
        }

        var entity = await base.InsertAsync(dto);

        return entity;
    }

    public override Task<OrderNote> UpdateAsync(OrderNoteDto dto)
    {
        throw new NotSupportedException();
    }

    public virtual async Task<OrderNoteDto[]> QueryOrderNotesAsync(QueryOrderNotesInput dto)
    {
        if (string.IsNullOrWhiteSpace(dto.OrderId))
            throw new ArgumentNullException(nameof(dto.OrderId));

        var db = await Repository.GetDbContextAsync();

        var set = db.Set<OrderNote>().AsNoTracking();

        var query = set.Where(x => x.OrderId == dto.OrderId);

        if (dto.OnlyForUser)
        {
            query = query.Where(x => x.DisplayToUser);
        }

        var count = dto.MaxCount ?? 5000;

        var data = await query
            .OrderBy(x => x.CreationTime).ThenBy(x => x.Id)
            .Take(count)
            .ToArrayAsync();

        var orderNotes = data.MapArrayTo<OrderNote, OrderNoteDto>(ObjectMapper);

        foreach (var m in orderNotes)
        {
            m.OrderNoteExtended =
                this.JsonDataSerializer.DeserializeFromStringOrDefault<OrderNoteExtended>(m.ExtendedJson,
                    this.Logger);
        }

        return orderNotes;
    }
}