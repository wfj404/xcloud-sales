import { ReactNode } from 'react';

import XCategoryGoods from '@/components/blocks/components/category_goods/render';
import XGallery from '@/components/blocks/components/gallery/render';
import XHello from '@/components/blocks/components/hello/render';
import XHotGoods from '@/components/blocks/components/hot_goods/render';
import XMarkdown from '@/components/blocks/components/markdown/render';
import XSlider from '@/components/blocks/components/slider/render';

import XCard from './components/card/render';
import { CategoryGoodsBlockProps } from './components/category_goods/utils';
import XCoupon from './components/coupon/render';
import { GalleryBlockProps } from './components/gallery/utils';
import { MarkdownBlockProps } from './components/markdown/utils';
import XSearch from './components/search/render';
import XTitle from './components/title/render';
import XVideo from './components/video/render';
import { BlockItem, BlockItemProps } from './utils';

export default ({ block }: { block: BlockItem<BlockItemProps> }) => {
  if (!block) {
    return <p>block is null</p>;
  }

  const renderBlockItem = (): ReactNode => {
    const body = block.body || {};

    if (block.type == 'slider') {
      return <XSlider data={body} />;
    }

    if (block.type == 'hello') {
      return <XHello data={body} />;
    }

    if (block.type == 'gallery') {
      return <XGallery data={body as GalleryBlockProps} />;
    }

    if (block.type == 'markdown') {
      return <XMarkdown data={body as MarkdownBlockProps} />;
    }

    if (block.type == 'hot-goods') {
      return <XHotGoods data={body} />;
    }

    if (block.type == 'category-goods') {
      return <XCategoryGoods data={body as CategoryGoodsBlockProps} />;
    }

    if (block.type == 'title') {
      return <XTitle data={body} />;
    }

    if (block.type == 'video') {
      return <XVideo data={body} />;
    }

    if (block.type == 'search') {
      return <XSearch data={body} />;
    }

    if (block.type == 'coupon') {
      return <XCoupon data={body} />;
    }

    if (block.type == 'card') {
      return <XCard data={body} />;
    }

    if (block.type == 'coupon') {
      return (
        <>
          <h4>todo</h4>
        </>
      );
    }

    return (
      <div>
        <p>error block item</p>
        <div>{JSON.stringify(block)}</div>
      </div>
    );
  };

  return (
    <>
      <div
        style={{
          overflow: 'hidden',
          ...block.style,
        }}
      >
        {renderBlockItem()}
      </div>
    </>
  );
};
