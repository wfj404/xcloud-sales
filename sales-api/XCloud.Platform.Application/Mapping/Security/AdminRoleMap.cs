﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Security;

namespace XCloud.Platform.Application.Mapping.Security;

public class AdminRoleMap : PlatformEntityTypeConfiguration<AdminRole>
{
    public override void Configure(EntityTypeBuilder<AdminRole> builder)
    {
        builder.ToTable("sys_admin_role");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.RoleId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.AdminId).IsRequired().HasMaxLength(100);

        base.Configure(builder);
    }
}