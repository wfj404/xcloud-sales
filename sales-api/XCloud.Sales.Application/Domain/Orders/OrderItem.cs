namespace XCloud.Sales.Application.Domain.Orders;

public class OrderItem : SalesBaseEntity<string>
{
    public string OrderId { get; set; }

    public string SkuId { get; set; }

    public string GoodsName { get; set; }

    public string SkuName { get; set; }

    public string SkuDescription { get; set; }

    public int Weight { get; set; }

    public string StockDeductStrategy { get; set; }

    public decimal BasePrice { get; set; }

    public decimal GradePriceOffset { get; set; }

    public double DiscountRate { get; set; }

    public decimal ValueAddedPriceOffset { get; set; }

    public decimal UnitPrice { get; set; }

    public int Quantity { get; set; }

    public decimal TotalPrice { get; set; }

    public string Remark { get; set; }
}