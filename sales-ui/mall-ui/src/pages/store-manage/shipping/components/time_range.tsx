import { Space, Tag, Tooltip } from 'antd';
import { Box, Typography } from '@mui/material';
import { dateTimeFormat, hourMinuteFormat, parseAsDayjs, timezoneOffset, weekFormat } from '@/utils/dayjs';
import { ClockCircleOutlined } from '@ant-design/icons';

export default ({ start, end }: { start?: string | null, end?: string | null }) => {

  const renderTime = (time?: string | null) => {

    let djs = parseAsDayjs(time);
    if (!djs) {
      return <span>--</span>;
    }

    djs = djs.add(timezoneOffset, 'hour');

    return <>
      <Tooltip title={djs.format(dateTimeFormat)}>
        <Box sx={{
          backgroundColor: 'rgb(250,250,250)',
          padding: 0.5,
          '&:hover': {
            border: (theme) => `1px solid ${theme.palette.primary.main}`,
          },
        }}>
          <Typography color={'primary'} variant={'overline'}
                      component={'div'}>{djs.fromNow()}，{djs.format(weekFormat)}</Typography>
          <Tag icon={<ClockCircleOutlined />} color={'blue'}>{djs.format(hourMinuteFormat)}</Tag>
        </Box>
      </Tooltip>
    </>;
  };

  return <>
    <Space size={'small'} direction={'horizontal'} split={
      <Typography color={'text.disabled'} variant={'caption'}>到</Typography>
    }>
      {renderTime(start)}
      {renderTime(end)}
    </Space>
  </>;
};
