import XLoading from '@/components/common/loading/dots';

export default () => {
  return (
    <>
      <XLoading />
    </>
  );
};
