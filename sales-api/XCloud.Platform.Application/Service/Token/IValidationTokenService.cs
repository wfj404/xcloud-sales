﻿using System.Threading.Tasks;
using XCloud.Application.Service;

namespace XCloud.Platform.Application.Service.Token;

public interface IValidationTokenService : IXCloudAppService
{
    Task AddValidationCodeAsync(string group, string key, string code, TimeSpan? timeout = null);

    Task<ValidationCode> GetValidationCodeAsync(string group, string key);
}

public class ValidationTokenService : PlatformAppService, IValidationTokenService
{
    private readonly ICacheProvider _cacheProvider;

    public ValidationTokenService(ICacheProvider cacheProvider)
    {
        _cacheProvider = cacheProvider;
    }

    private string ValidationCodeKey(string group, string key) => $"validation_code:{group}:{key}";

    public async Task AddValidationCodeAsync(string group, string key, string code, TimeSpan? timeout = null)
    {
        if (string.IsNullOrWhiteSpace(group) || string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(code))
            throw new ArgumentNullException(nameof(AddValidationCodeAsync));

        var model = new ValidationCode
        {
            Code = code,
            CreateTime = DateTime.UtcNow
        };

        var finalKey = ValidationCodeKey(group, key);
        var finalTimeout = timeout ?? TimeSpan.FromMinutes(5);

        await _cacheProvider.SetAsync(finalKey, model, finalTimeout);
    }

    public async Task<ValidationCode> GetValidationCodeAsync(string group, string key)
    {
        if (string.IsNullOrWhiteSpace(group) || string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(AddValidationCodeAsync));

        var finalKey = ValidationCodeKey(group, key);

        var res = await _cacheProvider.GetAsync<ValidationCode>(finalKey);

        return res.Data;
    }
}