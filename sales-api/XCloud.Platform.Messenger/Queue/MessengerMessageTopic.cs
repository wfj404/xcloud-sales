namespace XCloud.Platform.Messenger.Queue;

public static class MessengerMessageTopic
{
    public static string BroadCastKey => "messenger-message-queue-broadcast-key";
}