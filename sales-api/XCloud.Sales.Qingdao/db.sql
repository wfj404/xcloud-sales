﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [mcs_invoice_request] (
        [Id] nvarchar(100) NOT NULL,
        [BizType] nvarchar(100) NOT NULL,
        [TotalAmount] decimal(18,2) NOT NULL,
        [Status] nvarchar(100) NOT NULL,
        [InvoicingTime] datetime2 NULL,
        [Email] nvarchar(100) NULL,
        [Picked] bit NOT NULL,
        [PickedTime] datetime2 NULL,
        [McsUserId] nvarchar(100) NOT NULL,
        [PickupLocationId] int NOT NULL,
        [PickupLocationName] nvarchar(200) NULL,
        [AttachmentMetaId] nvarchar(100) NULL,
        [CreationTime] datetime2 NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_mcs_invoice_request] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'mcs_invoice_request', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'mcs_invoice_request', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'mcs_invoice_request', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [mcs_invoice_request_item] (
        [Id] nvarchar(100) NOT NULL,
        [InvoiceRequestId] nvarchar(100) NOT NULL,
        [McsBizOrderId] nvarchar(100) NOT NULL,
        [Price] decimal(18,2) NOT NULL,
        [Description] nvarchar(500) NULL,
        CONSTRAINT [PK_mcs_invoice_request_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'mcs_invoice_request_item', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_activity_log] (
        [Id] int NOT NULL IDENTITY,
        [ActivityLogTypeId] int NOT NULL,
        [UserId] nvarchar(100) NULL,
        [StoreUserId] int NOT NULL,
        [AdministratorId] nvarchar(100) NULL,
        [ManagerId] nvarchar(100) NULL,
        [Comment] nvarchar(1000) NULL,
        [Value] nvarchar(100) NULL,
        [Data] nvarchar(max) NULL,
        [UrlReferrer] nvarchar(1000) NULL,
        [BrowserType] nvarchar(100) NULL,
        [Device] nvarchar(100) NULL,
        [UserAgent] nvarchar(1000) NULL,
        [IpAddress] nvarchar(100) NULL,
        [GeoCountry] nvarchar(200) NULL,
        [GeoCity] nvarchar(200) NULL,
        [Lng] float NULL,
        [Lat] float NULL,
        [RequestPath] nvarchar(1000) NULL,
        [SubjectType] nvarchar(100) NULL,
        [SubjectId] nvarchar(100) NULL,
        [SubjectIntId] int NOT NULL DEFAULT 0,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_activity_log] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_activity_log', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_after_sales] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [ReasonForReturn] nvarchar(500) NOT NULL,
        [RequestedAction] nvarchar(500) NOT NULL,
        [UserComments] nvarchar(500) NULL,
        [StaffNotes] nvarchar(500) NULL,
        [Images] nvarchar(1000) NULL,
        [AfterSalesStatusId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        [HideForCustomer] bit NOT NULL,
        CONSTRAINT [PK_sales_after_sales] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_after_sales', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_after_sales', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_after_sales', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_after_sales_comment] (
        [Id] nvarchar(100) NOT NULL,
        [AfterSaleId] nvarchar(100) NOT NULL,
        [Content] nvarchar(1000) NOT NULL,
        [PictureJson] nvarchar(max) NULL,
        [IsAdmin] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_after_sales_comment] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_after_sales_comment', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_after_sales_comment', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_after_sales_item] (
        [Id] nvarchar(100) NOT NULL,
        [AfterSalesId] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [OrderItemId] nvarchar(100) NOT NULL,
        [Quantity] int NOT NULL,
        CONSTRAINT [PK_sales_after_sales_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_after_sales_item', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_balance_history] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] int NOT NULL,
        [Balance] decimal(18,2) NOT NULL,
        [LatestBalance] decimal(18,2) NOT NULL,
        [ActionType] int NOT NULL,
        [Message] nvarchar(300) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_balance_history] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_balance_history', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_balance_history', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_brand] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(400) NOT NULL,
        [Description] nvarchar(500) NULL,
        [ShowOnPublicPage] bit NOT NULL,
        [MetaKeywords] nvarchar(400) NULL,
        [PictureMetaId] nvarchar(max) NULL,
        [Published] bit NOT NULL,
        [DeletionTime] datetime2 NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [DisplayOrder] int NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_brand] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_brand', 'COLUMN', N'Id';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_brand', 'COLUMN', N'DeletionTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_brand', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_brand', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_category] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [SeoName] nvarchar(100) NULL,
        [Description] nvarchar(1000) NULL,
        [RootId] nvarchar(100) NULL,
        [NodePath] nvarchar(1000) NULL,
        [ParentCategoryId] nvarchar(100) NULL,
        [PictureMetaId] nvarchar(100) NULL,
        [ShowOnHomePage] bit NOT NULL,
        [Published] bit NOT NULL,
        [DeletionTime] datetime2 NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [DisplayOrder] int NOT NULL,
        [Recommend] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_category] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_category', 'COLUMN', N'Id';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_category', 'COLUMN', N'DeletionTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_category', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_category', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_common_entity_mapping] (
        [Id] int NOT NULL IDENTITY,
        [EntityId] nvarchar(100) NOT NULL,
        [EntityName] nvarchar(500) NOT NULL,
        [TargetId] nvarchar(100) NOT NULL,
        [TargetName] nvarchar(500) NOT NULL,
        CONSTRAINT [PK_sales_common_entity_mapping] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_common_entity_not_mapping] (
        [Id] int NOT NULL IDENTITY,
        [EntityId] nvarchar(100) NOT NULL,
        [EntityName] nvarchar(500) NOT NULL,
        [TargetId] nvarchar(100) NOT NULL,
        [TargetName] nvarchar(500) NOT NULL,
        CONSTRAINT [PK_sales_common_entity_not_mapping] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_coupon] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [CouponType] int NOT NULL,
        [Price] decimal(18,2) NOT NULL,
        [StartTime] datetime2 NULL,
        [EndTime] datetime2 NULL,
        [OrderConditionRulesJson] nvarchar(max) NULL,
        [ExpiredInDays] int NULL,
        [Amount] int NOT NULL,
        [AccountIssuedLimitCount] int NULL,
        [LimitedToStore] bit NOT NULL DEFAULT CAST(0 AS bit),
        [CreationTime] datetime2 NOT NULL,
        [IsActive] bit NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_coupon] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_coupon', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_coupon', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_coupon', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_coupon_user_mapping] (
        [Id] int NOT NULL IDENTITY,
        [CouponId] nvarchar(max) NULL,
        [UserId] int NOT NULL,
        [IsUsed] bit NOT NULL,
        [UsedTime] datetime2 NULL,
        [ExpiredAt] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_coupon_user_mapping] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_coupon_user_mapping', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_delivery_record] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [PreferDeliveryStartTime] datetime2 NULL,
        [PreferDeliveryEndTime] datetime2 NULL,
        [DeliveryType] nvarchar(100) NOT NULL,
        [ExpressName] nvarchar(100) NULL,
        [TrackingNumber] nvarchar(100) NULL,
        [Delivering] bit NOT NULL,
        [DeliveringTime] datetime2 NULL,
        [Delivered] bit NOT NULL,
        [DeliveredTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_delivery_record] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_delivery_record', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_delivery_record', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_delivery_record', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_delivery_record_item] (
        [Id] nvarchar(100) NOT NULL,
        [DeliveryId] nvarchar(100) NOT NULL,
        [OrderItemId] nvarchar(100) NOT NULL,
        [Quantity] int NOT NULL,
        CONSTRAINT [PK_sales_delivery_record_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_delivery_record_item', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_delivery_record_track] (
        [Id] int NOT NULL IDENTITY,
        [DeliveryId] nvarchar(100) NOT NULL,
        [Lon] float NOT NULL,
        [Lat] float NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_delivery_record_track] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_delivery_record_track', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_discount] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [DiscountPercentage] float NOT NULL,
        [StartTime] datetime2 NULL,
        [EndTime] datetime2 NULL,
        [IsActive] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        [Sort] int NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [LimitedToStore] bit NOT NULL DEFAULT CAST(0 AS bit),
        [LimitedToSku] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_discount] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_discount', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_discount', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_discount', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_freight_template] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [ShipInHours] int NULL,
        [FreeShippingMinOrderTotalPrice] decimal(18,2) NOT NULL,
        [CalculateType] int NOT NULL,
        [RulesJson] nvarchar(max) NULL,
        [ExcludedAreaJson] nvarchar(max) NULL,
        [Sort] int NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_freight_template] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_freight_template', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_freight_template', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_freight_template_goods_mapping] (
        [Id] nvarchar(100) NOT NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [TemplateId] nvarchar(100) NOT NULL,
        [GoodsId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_freight_template_goods_mapping] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_freight_template_goods_mapping', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_freight_template_goods_mapping', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_generic_order] (
        [Id] nvarchar(100) NOT NULL,
        [OrderType] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [Paid] bit NOT NULL,
        [Serviced] bit NOT NULL,
        [ToService] bit NOT NULL,
        [Reviewed] bit NOT NULL,
        [Finished] bit NOT NULL,
        [FinishedTime] datetime2 NULL,
        [Closed] bit NOT NULL,
        [ClosedTime] datetime2 NULL,
        [ItemsJson] nvarchar(max) NULL,
        [ExtraDataJson] nvarchar(max) NULL,
        [StoreUserId] int NOT NULL,
        [LastModificationTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_generic_order] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_generic_order', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_generic_order', 'COLUMN', N'LastModificationTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_generic_order', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_gift_card] (
        [Id] nvarchar(100) NOT NULL,
        [Amount] decimal(18,2) NOT NULL,
        [EndTime] datetime2 NULL,
        [UserId] int NOT NULL,
        [Used] bit NOT NULL,
        [UsedTime] datetime2 NULL,
        [IsActive] bit NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_gift_card] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_gift_card', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_gift_card', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_gift_card', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_goods] (
        [Id] nvarchar(100) NOT NULL,
        [GoodsType] int NOT NULL DEFAULT 0,
        [Name] nvarchar(400) NOT NULL,
        [SeoName] nvarchar(500) NULL,
        [Description] nvarchar(1000) NULL,
        [Keywords] nvarchar(1000) NULL,
        [DetailInformation] nvarchar(max) NULL,
        [AdminComment] nvarchar(500) NULL,
        [CategoryId] nvarchar(100) NULL,
        [BrandId] nvarchar(100) NULL,
        [LimitedToRedirect] bit NOT NULL,
        [RedirectToUrlJson] nvarchar(max) NULL,
        [LimitedToContact] bit NOT NULL,
        [ContactTelephone] nvarchar(50) NULL,
        [ApprovedReviewsRates] float NOT NULL,
        [ApprovedTotalReviews] int NOT NULL,
        [StockDeductStrategy] nvarchar(100) NOT NULL,
        [StickyTop] bit NOT NULL,
        [DisplayAsInformationPage] bit NOT NULL,
        [PlaceOrderSupported] bit NOT NULL,
        [AfterSalesSupported] bit NOT NULL,
        [PayAfterDeliveredSupported] bit NOT NULL,
        [CommentSupported] bit NOT NULL,
        [Published] bit NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_goods] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_goods', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_goods', 'COLUMN', N'IsDeleted';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_goods', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_goods_grade_price] (
        [Id] nvarchar(450) NOT NULL,
        [SkuId] nvarchar(100) NOT NULL,
        [GradeId] nvarchar(100) NOT NULL,
        [PriceOffset] decimal(18,2) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_goods_grade_price] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_goods_picture] (
        [Id] int NOT NULL IDENTITY,
        [GoodsId] nvarchar(100) NOT NULL,
        [SkuId] nvarchar(100) NULL,
        [PictureMetaId] nvarchar(100) NULL,
        [FileName] nvarchar(500) NULL,
        [DisplayOrder] int NOT NULL,
        CONSTRAINT [PK_sales_goods_picture] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_goods_virtual_extension] (
        [Id] nvarchar(100) NOT NULL,
        [GoodsId] nvarchar(100) NOT NULL,
        [ExpiredAfterDays] int NULL,
        CONSTRAINT [PK_sales_goods_virtual_extension] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_goods_virtual_extension', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_grade] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(1000) NULL,
        [UserCount] int NOT NULL,
        [Sort] int NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_grade] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_grade', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_grade', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_notification] (
        [Id] nvarchar(100) NOT NULL,
        [Title] nvarchar(100) NULL,
        [Content] nvarchar(1000) NULL,
        [BusinessId] nvarchar(100) NULL,
        [App] nvarchar(100) NULL,
        [Read] bit NOT NULL,
        [ReadTime] datetime2 NULL,
        [UserId] int NOT NULL,
        [ActionType] nvarchar(100) NULL,
        [Data] nvarchar(max) NULL,
        [SenderId] nvarchar(100) NULL,
        [SenderType] nvarchar(50) NULL,
        [LastModificationTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_notification] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_notification', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_notification', 'COLUMN', N'LastModificationTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_notification', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order] (
        [Id] nvarchar(100) NOT NULL,
        [GoodsType] int NOT NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [OrderSn] nvarchar(100) NULL,
        [UserId] int NOT NULL,
        [GradeId] nvarchar(100) NULL,
        [OrderStatusId] nvarchar(100) NOT NULL,
        [ServiceStartTime] datetime2 NULL,
        [ServiceEndTime] datetime2 NULL,
        [CompleteTime] datetime2 NULL,
        [ShippingMethodId] nvarchar(100) NULL,
        [ShippingStatusId] nvarchar(100) NOT NULL,
        [UserAddressId] nvarchar(100) NULL,
        [IsAfterSales] bit NOT NULL,
        [PaymentMethodId] nvarchar(100) NULL,
        [PaymentStatusId] nvarchar(100) NOT NULL,
        [PaidTotalPrice] decimal(18,2) NOT NULL,
        [RefundedAmount] decimal(18,2) NOT NULL,
        [TotalPrice] decimal(18,2) NOT NULL,
        [ItemsTotalPrice] decimal(18,2) NOT NULL,
        [PackageFee] decimal(18,2) NOT NULL,
        [ShippingFee] decimal(18,2) NOT NULL,
        [SellerPriceOffset] decimal(18,2) NOT NULL,
        [CouponPrice] decimal(18,2) NOT NULL,
        [PromotionId] nvarchar(100) NULL,
        [PromotionPriceOffset] decimal(18,2) NOT NULL,
        [RewardPointsHistoryId] int NULL,
        [ExchangePointsAmount] decimal(18,2) NOT NULL,
        [UserInvoiceId] nvarchar(100) NULL,
        [AffiliateId] int NOT NULL,
        [Remark] nvarchar(1000) NULL,
        [OrderIp] nvarchar(100) NULL,
        [PlatformId] nvarchar(100) NULL,
        [HideForCustomer] bit NOT NULL,
        [LastModificationTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_order] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order', 'COLUMN', N'LastModificationTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_coupon_record] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [UserCouponId] int NOT NULL,
        CONSTRAINT [PK_sales_order_coupon_record] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_coupon_record', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_inventory_deduction_history] (
        [Id] int NOT NULL IDENTITY,
        [OrderItemId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_order_inventory_deduction_history] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_inventory_deduction_history', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_item] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [SkuId] nvarchar(100) NOT NULL,
        [GoodsName] nvarchar(500) NULL,
        [SkuName] nvarchar(500) NULL,
        [SkuDescription] nvarchar(500) NULL,
        [Weight] int NOT NULL,
        [StockDeductStrategy] nvarchar(100) NOT NULL,
        [BasePrice] decimal(18,2) NOT NULL,
        [GradePriceOffset] decimal(18,2) NOT NULL,
        [DiscountRate] float NOT NULL,
        [ValueAddedPriceOffset] decimal(18,2) NOT NULL,
        [UnitPrice] decimal(18,2) NOT NULL,
        [Quantity] int NOT NULL,
        [TotalPrice] decimal(18,2) NOT NULL,
        [Remark] nvarchar(100) NULL,
        CONSTRAINT [PK_sales_order_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_item', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_item_gift] (
        [Id] nvarchar(450) NOT NULL,
        [OrderItemId] nvarchar(100) NOT NULL,
        [SkuId] nvarchar(max) NULL,
        [Quantity] int NOT NULL,
        CONSTRAINT [PK_sales_order_item_gift] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_note] (
        [Id] int NOT NULL IDENTITY,
        [OrderId] nvarchar(100) NOT NULL,
        [Note] nvarchar(1000) NULL,
        [DisplayToUser] bit NOT NULL,
        [ExtendedJson] nvarchar(max) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_order_note] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_note', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_review] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [Approved] bit NOT NULL DEFAULT CAST(0 AS bit),
        [ReviewText] nvarchar(1000) NULL,
        [Rating] float NOT NULL,
        [PictureMetaIdsJson] nvarchar(max) NULL,
        [ExtraData] nvarchar(max) NULL,
        [IpAddress] nvarchar(200) NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_order_review] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_review', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_review', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_review', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_review_item] (
        [Id] nvarchar(100) NOT NULL,
        [ReviewId] nvarchar(100) NOT NULL,
        [OrderItemId] nvarchar(100) NOT NULL,
        [ReviewText] nvarchar(1000) NULL,
        [PictureMetaIdsJson] nvarchar(max) NULL,
        [Rating] float NOT NULL,
        CONSTRAINT [PK_sales_order_review_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_review_item', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_order_value_added_item] (
        [Id] nvarchar(100) NOT NULL,
        [OrderItemId] nvarchar(100) NOT NULL,
        [ValueAddedItemId] int NOT NULL,
        [PriceOffset] decimal(18,2) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        CONSTRAINT [PK_sales_order_value_added_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_order_value_added_item', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_page] (
        [Id] nvarchar(100) NOT NULL,
        [SeoName] nvarchar(500) NOT NULL,
        [CoverImageResourceData] nvarchar(max) NULL,
        [Title] nvarchar(100) NULL,
        [Description] nvarchar(1000) NULL,
        [BodyContent] nvarchar(max) NULL,
        [ReadCount] int NOT NULL,
        [IsPublished] bit NOT NULL,
        [PublishedTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_page] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_page', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_page', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_page', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_payment_bill] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [Price] decimal(18,2) NOT NULL,
        [PaymentChannel] nvarchar(100) NOT NULL,
        [Paid] bit NOT NULL,
        [PayTime] datetime2 NULL,
        [PaymentTransactionId] nvarchar(200) NULL,
        [NotifyData] nvarchar(max) NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_payment_bill] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_payment_bill', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_payment_bill', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_payment_bill', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_pickup_record] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [Prepared] bit NOT NULL,
        [PreparedTime] datetime2 NULL,
        [ReservationApproved] bit NULL,
        [ReservationApprovedTime] datetime2 NULL,
        [PreferPickupStartTime] datetime2 NULL,
        [PreferPickupEndTime] datetime2 NULL,
        [Picked] bit NOT NULL,
        [PickedTime] datetime2 NULL,
        [SecurityCode] nvarchar(100) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_pickup_record] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_pickup_record', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_pickup_record', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_points_history] (
        [Id] int NOT NULL IDENTITY,
        [UserId] int NOT NULL,
        [OrderId] nvarchar(100) NULL,
        [Points] int NOT NULL,
        [PointsBalance] int NOT NULL,
        [ActionType] int NOT NULL,
        [UsedAmount] decimal(18,2) NOT NULL,
        [Message] nvarchar(300) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_points_history] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_points_history', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_refund_bill] (
        [Id] nvarchar(100) NOT NULL,
        [OrderId] nvarchar(100) NOT NULL,
        [PaymentBillId] nvarchar(100) NOT NULL,
        [Price] decimal(18,2) NOT NULL,
        [Approved] bit NOT NULL DEFAULT CAST(0 AS bit),
        [ApprovedTime] datetime2 NULL,
        [Refunded] bit NOT NULL,
        [RefundTime] datetime2 NULL,
        [RefundTransactionId] nvarchar(200) NULL,
        [RefundNotifyData] nvarchar(max) NULL,
        [LastModificationTime] datetime2 NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [DeletionTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_refund_bill] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_refund_bill', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_refund_bill', 'COLUMN', N'LastModificationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_refund_bill', 'COLUMN', N'IsDeleted';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_refund_bill', 'COLUMN', N'DeletionTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_refund_bill', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_shopping_cart_item] (
        [Id] int NOT NULL IDENTITY,
        [UserId] int NOT NULL,
        [SkuId] nvarchar(100) NOT NULL,
        [Quantity] int NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_shopping_cart_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_shopping_cart_item', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_shopping_cart_item', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_shopping_cart_value_added_item] (
        [Id] int NOT NULL IDENTITY,
        [ShoppingCartItemId] int NOT NULL,
        [ValueAddedItemId] int NOT NULL,
        CONSTRAINT [PK_sales_shopping_cart_value_added_item] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_sku] (
        [Id] nvarchar(100) NOT NULL,
        [GoodsId] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [SkuCode] nvarchar(100) NULL,
        [Color] nvarchar(100) NULL,
        [Price] decimal(18,2) NOT NULL,
        [MaxAmountInOnePurchase] int NOT NULL,
        [MinAmountInOnePurchase] int NOT NULL,
        [Weight] int NOT NULL,
        [SpecCombinationJson] nvarchar(max) NULL,
        [IsActive] bit NOT NULL,
        [DeletionTime] datetime2 NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_sku] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_sku', 'COLUMN', N'Id';
    SET @description = N'规格配置参数';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_sku', 'COLUMN', N'SpecCombinationJson';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_sku', 'COLUMN', N'DeletionTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_sku', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_sku', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_sku', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_spec] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(100) NOT NULL,
        [GoodsId] nvarchar(100) NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_spec] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_spec', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_spec_value] (
        [Id] int NOT NULL IDENTITY,
        [SpecId] int NOT NULL,
        [Name] nvarchar(400) NOT NULL,
        [PriceOffset] decimal(18,2) NOT NULL,
        [AssociatedSkuId] int NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_spec_value] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_spec_value', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_stock] (
        [Id] nvarchar(100) NOT NULL,
        [No] nvarchar(100) NOT NULL,
        [SupplierId] nvarchar(100) NULL,
        [WarehouseId] nvarchar(100) NULL,
        [Remark] nvarchar(1000) NULL,
        [Approved] bit NOT NULL,
        [ApprovedByUserId] nvarchar(100) NULL,
        [ApprovedTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_stock] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_stock', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_stock', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_stock_item] (
        [Id] nvarchar(100) NOT NULL,
        [StockId] nvarchar(100) NOT NULL,
        [SkuId] nvarchar(100) NOT NULL,
        [Quantity] int NOT NULL,
        [Price] decimal(18,2) NOT NULL,
        [DeductQuantity] int NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_stock_item] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_stock_item', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_stock_item', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_stock_usage_history] (
        [Id] nvarchar(100) NOT NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [StockItemId] nvarchar(100) NOT NULL,
        [Quantity] int NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_stock_usage_history] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_stock_usage_history', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_stock_usage_history', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [AddressDetail] nvarchar(500) NULL,
        [Telephone] nvarchar(50) NULL,
        [Opening] bit NOT NULL,
        [InvoiceSupported] bit NOT NULL,
        [Lon] float NOT NULL,
        [Lat] float NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [ExternalId] nvarchar(100) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_store] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_goods_mapping] (
        [Id] nvarchar(100) NOT NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [SkuId] nvarchar(100) NOT NULL,
        [StockQuantity] int NOT NULL,
        [Price] decimal(18,2) NOT NULL,
        [OverridePrice] bit NOT NULL,
        [Active] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_store_goods_mapping] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_goods_mapping', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_goods_mapping', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_goods_mapping', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_manager] (
        [Id] nvarchar(100) NOT NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [Role] int NOT NULL,
        [IsActive] bit NOT NULL,
        [IsSuperManager] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_store_manager] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_manager', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_manager', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_manager_role] (
        [Id] nvarchar(100) NOT NULL,
        [RoleId] nvarchar(100) NOT NULL,
        [ManagerId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_store_manager_role] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_manager_role', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_manager_role', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_role] (
        [Id] nvarchar(100) NOT NULL,
        [StoreId] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(100) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_store_role] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_role', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_role', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_role_permission] (
        [Id] nvarchar(100) NOT NULL,
        [PermissionKey] nvarchar(100) NOT NULL,
        [RoleId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_store_role_permission] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_role_permission', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_role_permission', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_user] (
        [Id] int NOT NULL IDENTITY,
        [UserId] nvarchar(100) NOT NULL,
        [Balance] decimal(18,2) NOT NULL,
        [Points] int NOT NULL,
        [HistoryPoints] int NOT NULL,
        [IsActive] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastActivityTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sales_store_user] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_user', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_store_user_grade_mapping] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] int NOT NULL,
        [GradeId] nvarchar(max) NULL,
        [StartTime] datetime2 NULL,
        [EndTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sales_store_user_grade_mapping] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_user_grade_mapping', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_store_user_grade_mapping', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_supplier] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [ContactName] nvarchar(100) NULL,
        [Telephone] nvarchar(50) NULL,
        [Address] nvarchar(500) NULL,
        [CreationTime] datetime2 NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_supplier] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_supplier', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_supplier', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_supplier', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_tag] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(1000) NULL,
        [Alert] nvarchar(1000) NULL,
        [Link] nvarchar(1000) NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_tag] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_tag', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_tag', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_tag_goods_mapping] (
        [Id] int NOT NULL IDENTITY,
        [TagId] nvarchar(100) NOT NULL,
        [GoodsId] nvarchar(100) NOT NULL,
        CONSTRAINT [PK_sales_tag_goods_mapping] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_value_added_item] (
        [Id] int NOT NULL IDENTITY,
        [GroupId] nvarchar(100) NOT NULL,
        [PriceOffset] decimal(18,2) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        CONSTRAINT [PK_sales_value_added_item] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_value_added_item_group] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [GoodsId] nvarchar(100) NOT NULL,
        [IsRequired] bit NOT NULL,
        [MultipleChoice] bit NOT NULL,
        CONSTRAINT [PK_sales_value_added_item_group] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_value_added_item_group', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sales_warehouse] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Address] nvarchar(500) NULL,
        [Lat] float NULL,
        [Lng] float NULL,
        [CreationTime] datetime2 NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_sales_warehouse] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_warehouse', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_warehouse', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sales_warehouse', 'COLUMN', N'IsDeleted';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_admin] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [IsActive] bit NOT NULL,
        [IsSuperAdmin] bit NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_admin] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_admin', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_admin', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_admin_role] (
        [Id] nvarchar(100) NOT NULL,
        [RoleId] nvarchar(100) NOT NULL,
        [AdminId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_admin_role] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_admin_role', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_admin_role', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_blob_data] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(100) NOT NULL,
        [Content] varbinary(max) NULL,
        [Base64Text] nvarchar(max) NULL,
        [Format] int NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        [LastModificationTime] datetime2 NULL,
        CONSTRAINT [PK_sys_blob_data] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_blob_data', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_blob_data', 'COLUMN', N'CreationTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_blob_data', 'COLUMN', N'LastModificationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_external_access_token] (
        [Id] nvarchar(100) NOT NULL,
        [Platform] nvarchar(100) NOT NULL,
        [AppId] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NULL,
        [Scope] nvarchar(100) NULL,
        [AccessTokenType] int NOT NULL,
        [AccessToken] nvarchar(1000) NOT NULL,
        [RefreshToken] nvarchar(1000) NULL,
        [ExpiredAt] datetime2 NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_external_access_token] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_external_access_token', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_external_access_token', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_external_connect] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [Platform] nvarchar(100) NOT NULL,
        [AppId] nvarchar(100) NOT NULL,
        [OpenId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_external_connect] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_external_connect', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_external_connect', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_im_message] (
        [Id] nvarchar(100) NOT NULL,
        [Content] nvarchar(1000) NULL,
        [MessageType] nvarchar(100) NULL,
        [Data] nvarchar(max) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_im_message] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_im_message', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_im_message', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_im_online_status] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [DeviceId] nvarchar(100) NOT NULL,
        [ServerInstanceId] nvarchar(100) NOT NULL,
        [PingTime] datetime2 NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_im_online_status] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_im_online_status', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_im_online_status', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_im_server_instance] (
        [Id] nvarchar(100) NOT NULL,
        [InstanceId] nvarchar(100) NOT NULL,
        [ConnectionCount] int NOT NULL,
        [PingTime] datetime2 NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_im_server_instance] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_im_server_instance', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_im_server_instance', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_permission] (
        [Id] nvarchar(100) NOT NULL,
        [PermissionKey] nvarchar(100) NOT NULL,
        [Description] nvarchar(500) NULL,
        [Group] nvarchar(100) NULL,
        [AppKey] nvarchar(100) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_permission] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_permission', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_permission', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_resource_acl] (
        [Id] nvarchar(100) NOT NULL,
        [ResourceType] nvarchar(100) NOT NULL,
        [ResourceId] nvarchar(100) NOT NULL,
        [PermissionType] nvarchar(100) NOT NULL,
        [PermissionId] nvarchar(100) NOT NULL,
        [AccessControlType] int NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_resource_acl] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_resource_acl', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_resource_acl', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_role] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(200) NOT NULL,
        [Description] nvarchar(100) NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_role] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_role', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_role', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_role_permission] (
        [Id] nvarchar(100) NOT NULL,
        [PermissionKey] nvarchar(100) NOT NULL,
        [RoleId] nvarchar(100) NOT NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_role_permission] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_role_permission', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_role_permission', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_serial_no] (
        [Id] nvarchar(100) NOT NULL,
        [Category] nvarchar(100) NOT NULL,
        [NextId] int NOT NULL,
        [LastModificationTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_serial_no] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_serial_no', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_serial_no', 'COLUMN', N'LastModificationTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_serial_no', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_setting] (
        [Id] nvarchar(100) NOT NULL,
        [Name] nvarchar(300) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_sys_setting] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_setting', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_storage_meta] (
        [Id] nvarchar(100) NOT NULL,
        [FileExtension] nvarchar(30) NULL,
        [ContentType] nvarchar(100) NULL,
        [ResourceSize] bigint NOT NULL,
        [ResourceKey] nvarchar(500) NULL,
        [ResourceHash] nvarchar(100) NULL,
        [HashType] nvarchar(20) NULL,
        [ExtraData] nvarchar(max) NULL,
        [StorageProvider] nvarchar(100) NOT NULL,
        [ReferenceCount] int NOT NULL,
        [UploadTimes] int NOT NULL,
        [LastModificationTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_storage_meta] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_storage_meta', 'COLUMN', N'Id';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_storage_meta', 'COLUMN', N'LastModificationTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_storage_meta', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_user] (
        [Id] nvarchar(100) NOT NULL,
        [IdentityName] nvarchar(100) NOT NULL,
        [OriginIdentityName] nvarchar(100) NULL,
        [ExternalId] nvarchar(100) NULL,
        [NickName] nvarchar(100) NULL,
        [PassWord] nvarchar(100) NULL,
        [Avatar] nvarchar(2000) NULL,
        [LastPasswordUpdateTime] datetime2 NULL,
        [Gender] int NOT NULL,
        [IsActive] bit NOT NULL DEFAULT CAST(0 AS bit),
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [DeletionTime] datetime2 NULL,
        [LastModificationTime] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_user] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user', 'COLUMN', N'Id';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user', 'COLUMN', N'IsDeleted';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user', 'COLUMN', N'DeletionTime';
    SET @description = N'更新时间，同时也是乐观锁';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user', 'COLUMN', N'LastModificationTime';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_user_address] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NULL,
        [Lat] float NULL,
        [Lon] float NULL,
        [Name] nvarchar(100) NULL,
        [NationCode] nvarchar(100) NULL,
        [Nation] nvarchar(100) NULL,
        [ProvinceCode] nvarchar(100) NULL,
        [Province] nvarchar(100) NULL,
        [CityCode] nvarchar(100) NULL,
        [City] nvarchar(100) NULL,
        [AreaCode] nvarchar(100) NULL,
        [Area] nvarchar(100) NULL,
        [AddressDescription] nvarchar(1000) NULL,
        [AddressDetail] nvarchar(500) NULL,
        [PostalCode] nvarchar(100) NULL,
        [Tel] nvarchar(100) NULL,
        [IsDefault] bit NOT NULL,
        [ExtendedJson] nvarchar(max) NULL,
        [DeletionTime] datetime2 NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_user_address] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_address', 'COLUMN', N'Id';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_address', 'COLUMN', N'DeletionTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_address', 'COLUMN', N'IsDeleted';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_address', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_user_identity] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [Identity] nvarchar(50) NOT NULL,
        [Data] nvarchar(4000) NULL,
        [IdentityType] int NOT NULL,
        [MobilePhone] nvarchar(20) NULL DEFAULT N'',
        [MobileAreaCode] nvarchar(10) NULL DEFAULT N'',
        [MobileConfirmed] bit NULL,
        [MobileConfirmedTimeUtc] datetime2 NULL,
        [Email] nvarchar(100) NULL DEFAULT N'',
        [EmailConfirmed] bit NULL,
        [EmailConfirmedTimeUtc] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_user_identity] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_identity', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_identity', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_user_invoice] (
        [Id] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [Head] nvarchar(100) NOT NULL,
        [Code] nvarchar(100) NULL,
        [InvoiceType] int NOT NULL,
        [Address] nvarchar(500) NULL,
        [PhoneNumber] nvarchar(100) NULL,
        [BankName] nvarchar(100) NULL,
        [BankCode] nvarchar(100) NULL,
        [Content] nvarchar(1000) NULL,
        [CreationTime] datetime2 NOT NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        [DeletionTime] datetime2 NULL,
        CONSTRAINT [PK_sys_user_invoice] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_invoice', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_invoice', 'COLUMN', N'CreationTime';
    SET @description = N'是否删除';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_invoice', 'COLUMN', N'IsDeleted';
    SET @description = N'删除时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_invoice', 'COLUMN', N'DeletionTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_user_real_name] (
        [Id] nvarchar(100) NOT NULL,
        [IdCardIdentity] nvarchar(100) NOT NULL,
        [UserId] nvarchar(100) NOT NULL,
        [Data] nvarchar(4000) NULL,
        [IdCardType] int NOT NULL,
        [IdCard] nvarchar(100) NOT NULL,
        [IdCardRealName] nvarchar(20) NULL,
        [IdCardFrontUrl] nvarchar(1000) NULL,
        [IdCardBackUrl] nvarchar(1000) NULL,
        [IdCardHandUrl] nvarchar(1000) NULL,
        [StartTimeUtc] datetime2 NULL,
        [EndTimeUtc] datetime2 NULL,
        [IdCardStatus] int NOT NULL,
        [IdCardConfirmed] bit NULL,
        [IdCardConfirmedTimeUtc] datetime2 NULL,
        [CreationTime] datetime2 NOT NULL,
        CONSTRAINT [PK_sys_user_real_name] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_real_name', 'COLUMN', N'Id';
    SET @description = N'创建时间';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_user_real_name', 'COLUMN', N'CreationTime';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE TABLE [sys_wx_union] (
        [Id] nvarchar(100) NOT NULL,
        [Platform] nvarchar(100) NOT NULL,
        [AppId] nvarchar(100) NOT NULL,
        [OpenId] nvarchar(100) NOT NULL,
        [UnionId] nvarchar(100) NOT NULL,
        CONSTRAINT [PK_sys_wx_union] PRIMARY KEY ([Id])
    );
    DECLARE @defaultSchema AS sysname;
    SET @defaultSchema = SCHEMA_NAME();
    DECLARE @description AS sql_variant;
    SET @description = N'主键，最好是顺序生成的GUID';
    EXEC sp_addextendedproperty 'MS_Description', @description, 'SCHEMA', @defaultSchema, 'TABLE', N'sys_wx_union', 'COLUMN', N'Id';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_mcs_invoice_request_CreationTime] ON [mcs_invoice_request] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_mcs_invoice_request_IsDeleted] ON [mcs_invoice_request] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_activity_log_CreationTime] ON [sales_activity_log] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_after_sales_CreationTime] ON [sales_after_sales] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_after_sales_LastModificationTime] ON [sales_after_sales] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_after_sales_comment_CreationTime] ON [sales_after_sales_comment] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_balance_history_CreationTime] ON [sales_balance_history] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_brand_CreationTime] ON [sales_brand] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_brand_IsDeleted] ON [sales_brand] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_category_CreationTime] ON [sales_category] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_category_IsDeleted] ON [sales_category] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_coupon_CreationTime] ON [sales_coupon] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_coupon_IsDeleted] ON [sales_coupon] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_coupon_user_mapping_CreationTime] ON [sales_coupon_user_mapping] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_delivery_record_CreationTime] ON [sales_delivery_record] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_delivery_record_LastModificationTime] ON [sales_delivery_record] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_delivery_record_track_CreationTime] ON [sales_delivery_record_track] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_discount_CreationTime] ON [sales_discount] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_discount_IsDeleted] ON [sales_discount] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_freight_template_CreationTime] ON [sales_freight_template] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_freight_template_goods_mapping_CreationTime] ON [sales_freight_template_goods_mapping] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_generic_order_CreationTime] ON [sales_generic_order] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_generic_order_LastModificationTime] ON [sales_generic_order] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_gift_card_CreationTime] ON [sales_gift_card] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_gift_card_IsDeleted] ON [sales_gift_card] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_goods_IsDeleted] ON [sales_goods] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_goods_LastModificationTime] ON [sales_goods] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_grade_IsDeleted] ON [sales_grade] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_notification_CreationTime] ON [sales_notification] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_notification_LastModificationTime] ON [sales_notification] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_order_CreationTime] ON [sales_order] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_order_LastModificationTime] ON [sales_order] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_order_inventory_deduction_history_CreationTime] ON [sales_order_inventory_deduction_history] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_order_note_CreationTime] ON [sales_order_note] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_order_review_CreationTime] ON [sales_order_review] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_order_review_LastModificationTime] ON [sales_order_review] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_page_CreationTime] ON [sales_page] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_page_IsDeleted] ON [sales_page] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_payment_bill_CreationTime] ON [sales_payment_bill] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_payment_bill_IsDeleted] ON [sales_payment_bill] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_pickup_record_CreationTime] ON [sales_pickup_record] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_points_history_CreationTime] ON [sales_points_history] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_refund_bill_CreationTime] ON [sales_refund_bill] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_refund_bill_IsDeleted] ON [sales_refund_bill] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_refund_bill_LastModificationTime] ON [sales_refund_bill] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_shopping_cart_item_CreationTime] ON [sales_shopping_cart_item] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_shopping_cart_item_LastModificationTime] ON [sales_shopping_cart_item] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_sku_CreationTime] ON [sales_sku] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_sku_IsDeleted] ON [sales_sku] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_sku_LastModificationTime] ON [sales_sku] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_spec_IsDeleted] ON [sales_spec] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_spec_value_IsDeleted] ON [sales_spec_value] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_stock_CreationTime] ON [sales_stock] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_stock_item_LastModificationTime] ON [sales_stock_item] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_stock_usage_history_CreationTime] ON [sales_stock_usage_history] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_CreationTime] ON [sales_store] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_IsDeleted] ON [sales_store] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_goods_mapping_CreationTime] ON [sales_store_goods_mapping] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_goods_mapping_LastModificationTime] ON [sales_store_goods_mapping] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_manager_CreationTime] ON [sales_store_manager] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_manager_role_CreationTime] ON [sales_store_manager_role] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_role_CreationTime] ON [sales_store_role] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_role_permission_CreationTime] ON [sales_store_role_permission] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_user_LastModificationTime] ON [sales_store_user] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_store_user_grade_mapping_CreationTime] ON [sales_store_user_grade_mapping] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_supplier_CreationTime] ON [sales_supplier] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_supplier_IsDeleted] ON [sales_supplier] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_tag_IsDeleted] ON [sales_tag] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_warehouse_CreationTime] ON [sales_warehouse] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sales_warehouse_IsDeleted] ON [sales_warehouse] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_admin_CreationTime] ON [sys_admin] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_admin_role_CreationTime] ON [sys_admin_role] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_blob_data_CreationTime] ON [sys_blob_data] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_blob_data_LastModificationTime] ON [sys_blob_data] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE UNIQUE INDEX [IX_sys_blob_data_Name] ON [sys_blob_data] ([Name]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_external_access_token_CreationTime] ON [sys_external_access_token] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_external_connect_CreationTime] ON [sys_external_connect] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_im_message_CreationTime] ON [sys_im_message] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_im_online_status_CreationTime] ON [sys_im_online_status] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_im_server_instance_CreationTime] ON [sys_im_server_instance] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_permission_CreationTime] ON [sys_permission] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_resource_acl_CreationTime] ON [sys_resource_acl] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_role_CreationTime] ON [sys_role] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_role_permission_CreationTime] ON [sys_role_permission] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE UNIQUE INDEX [IX_sys_serial_no_Category] ON [sys_serial_no] ([Category]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_serial_no_CreationTime] ON [sys_serial_no] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_serial_no_LastModificationTime] ON [sys_serial_no] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_storage_meta_CreationTime] ON [sys_storage_meta] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_storage_meta_LastModificationTime] ON [sys_storage_meta] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_CreationTime] ON [sys_user] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE UNIQUE INDEX [IX_sys_user_IdentityName] ON [sys_user] ([IdentityName]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_IsDeleted] ON [sys_user] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_LastModificationTime] ON [sys_user] ([LastModificationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_address_CreationTime] ON [sys_user_address] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_address_IsDeleted] ON [sys_user_address] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_identity_CreationTime] ON [sys_user_identity] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE UNIQUE INDEX [IX_sys_user_identity_Identity] ON [sys_user_identity] ([Identity]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_identity_UserId] ON [sys_user_identity] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_invoice_CreationTime] ON [sys_user_invoice] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_invoice_IsDeleted] ON [sys_user_invoice] ([IsDeleted]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_real_name_CreationTime] ON [sys_user_real_name] ([CreationTime]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE UNIQUE INDEX [IX_sys_user_real_name_IdCardIdentity] ON [sys_user_real_name] ([IdCardIdentity]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    CREATE INDEX [IX_sys_user_real_name_UserId] ON [sys_user_real_name] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240110052429_invoice')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20240110052429_invoice', N'7.0.14');
END;
GO

COMMIT;
GO

