# order

## create order flow
- check form data
- check relative goods data
- check goods stock quantity
- load and check customer info
- start transaction
- load goods info into memory
- build order
- build order items
- try attach coupon
- try attach promotion
- calculate final price
- create order sn
- execute final check
- save to database
- commit transaction

## 订单自动处理的情况

- 货到付款【直接跳过一开始的支付环节】
- 0元支付【0元自动支付】