import { FloatingBubble } from 'antd-mobile';
import React, { useEffect } from 'react';
import { history, useLocation, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isStoreUserLogin } from '@/utils/biz';
import { StoreUserDto } from '@/utils/swagger';
import { pathEqual } from '@/utils/utils';
import { ShoppingBagOutlined } from '@mui/icons-material';
import { Badge } from '@mui/material';

const whitelist: string[] = ['/x-not-found'];

export default function () {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const enabled = whitelist.some((x) => pathEqual(location.pathname, x));

  const queryShoppingCartCount = async () => {
    if (!enabled) {
      return;
    }

    if (!isStoreUserLogin(app.storeUser as StoreUserDto)) {
      app.setShoppingCartCount(0);
      return;
    }

    await app.queryShoppingCartCount();
  };

  React.useEffect(() => {
    queryShoppingCartCount();
  }, [app.storeUser, location.pathname]);

  const count = app.shoppingCartCount || 0;

  if (count <= 0) {
    return null;
  }

  if (!enabled) {
    return null;
  }

  return (
    <>
      <FloatingBubble
        axis="xy"
        magnetic="x"
        style={{
          '--initial-position-bottom': `${24 + (app.bottomHeight || 0)}px`,
          '--initial-position-right': '24px',
          '--edge-distance': '24px',
          '--size': '58px',
          '--z-index': '9999',
          '--background': 'gray',
        }}
        onClick={() => {
          history.push({
            pathname: '/shoppingcart',
          });
        }}
      >
        <Badge
          variant="standard"
          color="error"
          invisible={count <= 0}
          badgeContent={count}
        >
          <ShoppingBagOutlined color={'inherit'} />
        </Badge>
      </FloatingBubble>
    </>
  );
}
