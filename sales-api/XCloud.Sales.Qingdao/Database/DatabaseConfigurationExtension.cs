﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;
using XCloud.Platform.Application.Repository;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Qingdao.Database.EntityFrameworkCore;
using XCloud.Sales.Qingdao.Database.Repository;

namespace XCloud.Sales.Qingdao.Database;

public static class DatabaseConfigurationExtension
{
    public static void ConfigureQingdaoSqlServer(this ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<ShopSqlServerDbContext>(option =>
        {
            //add default repository
            option.AddDefaultRepositories(true);
        });

        context.Services.Configure<AbpDbContextOptions>(contextOption =>
        {
            contextOption.Configure<ShopSqlServerDbContext>(option =>
            {
                //option.DbContextOptions.UseSqlServer(config.GetConnectionString(SalesDbConnectionNames.SalesCloud));
                option.UseSqlServer();
                option.DbContextOptions.EnableDetailedErrors();
                option.DbContextOptions.EnableSensitiveDataLogging();
                option.DbContextOptions.EnableThreadSafetyChecks();
            });
        });

        //add repository
        context.Services.AddScoped(typeof(IPlatformRepository<>), typeof(PlatformSqlServerRepository<>));
        context.Services.AddScoped(typeof(ISalesRepository<>), typeof(SalesSqlServerRepository<>));
    }
}