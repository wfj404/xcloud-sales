using Volo.Abp.Validation;
using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface ISpecService : ISalesAppService
{
    Task<KeyValuePair<Spec, SpecValue>[]> GetSpecAndValueByGoodsIdAsync(string goodsId);

    Task<SpecDto[]> QuerySpecByGoodsIdAsync(string goodsId);

    Task<Spec> InsertAsync(SpecDto dto);

    Task DeleteByIdAsync(int id);

    Task<Spec> UpdateAsync(SpecDto dto);
}

public class SpecService : SalesAppService, ISpecService
{
    private readonly ISalesRepository<Spec> _repository;

    public SpecService(ISalesRepository<Spec> repository)
    {
        _repository = repository;
    }

    public virtual async Task<KeyValuePair<Spec, SpecValue>[]> GetSpecAndValueByGoodsIdAsync(string goodsId)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            throw new ArgumentNullException(nameof(goodsId));

        var db = await _repository.GetDbContextAsync();

        var query = from spec in db.Set<Spec>().AsNoTracking().IgnoreQueryFilters()
            join value in db.Set<SpecValue>().AsNoTracking().IgnoreQueryFilters()
                on spec.Id equals value.SpecId
            select new
            {
                spec,
                value,
            };

        query = query.Where(x => x.spec.GoodsId == goodsId);

        var data = await query.ToArrayAsync();

        var res = data.Select(x => new KeyValuePair<Spec, SpecValue>(x.spec, x.value)).ToArray();

        return res;
    }

    public virtual async Task<SpecDto[]> QuerySpecByGoodsIdAsync(string goodsId)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            throw new ArgumentNullException(nameof(goodsId));

        var db = await _repository.GetDbContextAsync();

        var data = await db.Set<Spec>().AsNoTracking().Where(x => x.GoodsId == goodsId).ToArrayAsync();

        var res = data.Select(x => ObjectMapper.Map<Spec, SpecDto>(x)).ToArray();

        return res;
    }

    private async Task CheckExistAsync(string goodsId, string name, int? exceptId = default)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            throw new ArgumentNullException(nameof(goodsId));

        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<Spec>().AsNoTracking();

        query = query.Where(x => x.GoodsId == goodsId);
        query = query.Where(x => x.Name == name);

        if (exceptId != null && exceptId.Value > 0)
            query = query.Where(x => x.Id != exceptId.Value);

        if (await query.AnyAsync())
        {
            throw new AbpValidationException(message: "spec name already exist");
        }
    }

    public async Task<Spec> InsertAsync(SpecDto dto)
    {
        var entity = dto.MapTo<SpecDto, Spec>(this.ObjectMapper);

        if (string.IsNullOrWhiteSpace(entity.Name))
            throw new ArgumentNullException(nameof(entity.Name));

        await CheckExistAsync(entity.GoodsId, entity.Name);

        await this._repository.InsertAsync(entity);

        return entity;
    }

    public async Task<Spec> UpdateAsync(SpecDto dto)
    {
        if (dto.Id <= 0)
            throw new ArgumentNullException(nameof(dto.Id));

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        await CheckExistAsync(entity.GoodsId, dto.Name, exceptId: entity.Id);

        entity.Name = dto.Name;

        await this._repository.UpdateAsync(entity);

        return entity;
    }

    public async Task DeleteByIdAsync(int id)
    {
        var entity = await this._repository.GetRequiredByIdAsync(id);

        entity.IsDeleted = true;

        await this._repository.UpdateAsync(entity);
    }
}