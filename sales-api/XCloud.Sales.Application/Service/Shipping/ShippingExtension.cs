﻿using Volo.Abp.Guids;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Service.Shipping;

public static class ShippingExtension
{
    public static bool IsDelivery(this Order order) => order.ShippingMethodId == SalesConstants.ShippingMethodType.Delivery;

    public static bool IsPickup(this Order order) => order.ShippingMethodId == SalesConstants.ShippingMethodType.Pickup;

    public static void TrySetIds(this TimeRangeDto[] ranges, IGuidGenerator guidGenerator)
    {
        foreach (var m in ranges)
        {
            if (string.IsNullOrWhiteSpace(m.Id))
            {
                m.Id = guidGenerator.CreateGuidString();
            }
        }
    }

    public static bool IncludeTime(this IEnumerable<ExceptionTime> exceptionTimes, DateTime time)
    {
        return exceptionTimes.Any(x => x.StartTime <= time && x.EndTime >= time);
    }
}