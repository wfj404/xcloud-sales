﻿using System;
using Volo.Abp;

namespace XCloud.Platform.WeChat;

public class WechatException : BusinessException
{
    public WechatException()
    {
        //
    }

    public WechatException(string message, string detail = default, Exception e = default) :
        base(message: message, details: detail, innerException: e)
    {
        //
    }
}