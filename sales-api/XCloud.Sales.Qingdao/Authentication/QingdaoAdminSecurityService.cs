﻿using System;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Authentication;

[DisableConventionalRegistration]
public class QingdaoAdminSecurityService : SalesAppService, IAdminSecurityService
{
    private readonly IAdminService _adminService;
    private readonly Sh3HClient _sh3HClient;

    public QingdaoAdminSecurityService(IAdminService adminService, Sh3HClient sh3HClient)
    {
        _adminService = adminService;
        _sh3HClient = sh3HClient;
    }

    private async Task<AdminGrantedPermissionResponse> GetGrantedPermissionsAsync(string adminId)
    {
        var response = new AdminGrantedPermissionResponse();

        var admin = await this._adminService.GetByIdAsync(adminId);

        if (admin == null)
            return response;

        response.SetData(admin);

        await this._adminService.AttachUsersAsync(new[] { admin });

        if (admin.User != null)
        {
            //call api
            response.Permissions = await this._sh3HClient.GetPermissionsAsync(admin.User.ExternalId);
        }

        return response;
    }

    public async Task<AdminGrantedPermissionResponse> GetGrantedPermissionsAsync(string adminId,
        CacheStrategy cacheStrategyOption)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        var option = new CacheOption<AdminGrantedPermissionResponse>(
            key: $"3h.admin.permissions.by.{adminId}",
            expiration: TimeSpan.FromSeconds(60));

        var result = await this.CacheProvider.ExecuteWithPolicyAsync(
            () => this.GetGrantedPermissionsAsync(adminId),
            option, cacheStrategyOption);

        return result ?? new AdminGrantedPermissionResponse();
    }
}