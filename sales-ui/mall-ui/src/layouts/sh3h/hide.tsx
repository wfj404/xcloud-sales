import { isSh3H } from '@/utils/biz';
import { ReactNode } from 'react';

export default ({ children }: { children: ReactNode }) => {

  if (isSh3H()) {
    return <h1>404.not.found</h1>;
  }

  return <>{children}</>;
};
