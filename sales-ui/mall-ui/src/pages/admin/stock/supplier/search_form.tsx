import { QuerySupplierPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';
import { Button, Card, Form } from 'antd';
import { useEffect, useState } from 'react';

const App = (props: {
  query: QuerySupplierPagingInput;
  onSearch: (d: QuerySupplierPagingInput) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QuerySupplierPagingInput>({});

  const triggerSearch = (e: any) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form({ ...query });
  }, []);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        layout='inline'
      >
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type='primary' htmlType='submit' icon={<SearchOutlined />}
                  onClick={() => {
                    triggerSearch(form);
                  }}>
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default App;
