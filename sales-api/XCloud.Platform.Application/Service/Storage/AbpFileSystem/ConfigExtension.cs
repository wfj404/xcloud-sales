﻿using System.IO;
using Volo.Abp.Modularity;

namespace XCloud.Platform.Application.Service.Storage.AbpFileSystem;

public static class ConfigExtension
{
    public static string GetOrCreateSavePath(this ServiceConfigurationContext context)
    {
        var configuration = context.Services.GetConfiguration();

        var savePath = configuration.GetStorageOption().Disk.BasePath;

        if (string.IsNullOrWhiteSpace(savePath))
        {
            var env = context.Services.GetHostingEnvironment();
            savePath = Path.Combine(env.ContentRootPath, "upload");
        }

        new DirectoryInfo(savePath).CreateIfNotExist();

        return savePath;
    }
}