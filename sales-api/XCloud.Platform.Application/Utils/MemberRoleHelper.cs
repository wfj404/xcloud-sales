﻿using System.Collections.Generic;
using XCloud.Platform.Application.Core;

namespace XCloud.Platform.Application.Utils;

/// <summary>
/// 验证角色
/// </summary>
public static class MemberRoleHelper
{
    public static Dictionary<string, int> GetRoles() =>
        Enumerable.ToDictionary(typeof(MemberRoleEnum)
                .GetEnumFieldsValues(), x => x.Key, x => (int)x.Value);

    public static List<string> ParseRoleNames(int flag, Dictionary<string, int> allRoles = null)
    {
        allRoles = allRoles ?? GetRoles();

        return allRoles.Where(x => PermissionHelper.HasPermission(flag, x.Value)).Select(x => x.Key).ToList();
    }
}