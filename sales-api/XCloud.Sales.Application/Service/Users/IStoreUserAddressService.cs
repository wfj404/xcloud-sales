using XCloud.Application.Mapper;
using XCloud.Platform.Application.Domain.Address;
using XCloud.Platform.Application.Service.Address;

namespace XCloud.Sales.Application.Service.Users;

public interface IStoreUserAddressService : ISalesAppService
{
    Task<UserAddressDto[]> QueryByUserIdAsync(int userId);
}

public class StoreUserAddressService : SalesAppService, IStoreUserAddressService
{
    private readonly IStoreUserService _userService;
    private readonly IUserAddressService _userAddressService;

    public StoreUserAddressService(IStoreUserService userService, IUserAddressService userAddressService)
    {
        _userService = userService;
        _userAddressService = userAddressService;
    }

    public async Task<UserAddressDto[]> QueryByUserIdAsync(int userId)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        var user = await _userService.GetByIdAsync(userId);
        if (user == null)
            throw new EntityNotFoundException(nameof(user));

        if (string.IsNullOrWhiteSpace(user.UserId))
            throw new AbpException("user global id is empty");

        var addressList = await _userAddressService.QueryByUserIdAsync(user.UserId);

        return addressList.MapArrayTo<UserAddress, UserAddressDto>(ObjectMapper);
    }
}