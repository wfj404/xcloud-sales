import { Alert, Badge, Button, Calendar, Card, Col, Divider, Modal, Row } from 'antd';
import parser from 'cron-parser';
import { Dayjs } from 'dayjs';
import { useEffect, useMemo, useState } from 'react';

import { dateTimeFormat, myDayjs, parseAsDayjs } from '@/utils/dayjs';
import { CronRule, ExceptionTime, ServiceTimeDto } from '@/utils/swagger';

import XCron from './cron';
import XExceptionTime from './exception_time';
import XTimeRange from './time_range';

const exceptionTimeMatch = (
  exceptionTime: ExceptionTime,
  date: Dayjs,
): boolean => {
  const startTime = parseAsDayjs(exceptionTime.StartTime);
  const endTime = parseAsDayjs(exceptionTime.EndTime);

  let startTimeOk =
    startTime == null ||
    startTime.isBefore(date) ||
    startTime.isSame(date, 'date');

  let endTimeOk =
    endTime == null || endTime.isAfter(date) || endTime.isSame(date, 'date');

  return startTimeOk && endTimeOk;
};

const generate_next = (
  cron: string,
  start: Dayjs,
  end: Dayjs,
  count: number,
): Dayjs[] => {
  try {
    let expression = parser.parseExpression(cron, {
      currentDate: start.format(dateTimeFormat),
      iterator: true,
    });

    let response: Dayjs[] = [];

    let i = 0;
    while (expression.hasNext()) {
      ++i;
      if (i >= count) {
        break;
      }

      let next = expression.next();
      let time = myDayjs(next.value.toDate());

      if (time.isAfter(end)) {
        break;
      }

      response.push(time);
    }

    return response;
  } catch (e) {
    console.log(e);
    return [];
  }
};

interface CronNext {
  name?: string | null;
  dates: Dayjs[];
}

export default ({
  data,
  ok,
}: {
  data: ServiceTimeDto;
  ok: (d: ServiceTimeDto) => void;
}) => {
  const [show, _show] = useState(false);

  const preview_start = myDayjs.utc();
  const preview_end = preview_start.add(30, 'day');

  const generateCronNext = (cron: CronRule): CronNext => {
    return {
      name: cron.Name,
      dates: generate_next(cron.Cron || '', preview_start, preview_end, 1000),
    };
  };

  const next_dates =
    useMemo(
      () => data.CronRules?.map<CronNext>(generateCronNext),
      [data.CronRules, preview_start, preview_end],
    ) || [];

  const renderDateCell = (date: Dayjs) => {
    let filtered_date = next_dates.filter((x) =>
      x.dates.some((d) => d.isSame(date, 'date')),
    );
    return (
      <>
        {filtered_date.map((x) => {
          return (
            <div style={{}}>
              <Badge status={'success'} text={x.name || '--'} />
            </div>
          );
        })}
      </>
    );
  };

  const renderExceptionTime = (date: Dayjs) => {
    let filtered_exception_time =
      data.ExceptionTimes?.filter((x) => exceptionTimeMatch(x, date)) || [];
    if (filtered_exception_time.length <= 0) {
      return null;
    }
    return filtered_exception_time.map((x) => {
      return (
        <div style={{}}>
          <Badge text={x.Name} status={x.Include ? 'success' : 'error'} />
        </div>
      );
    });
  };

  useEffect(() => {
    console.log(
      next_dates.flatMap((x) => x.dates).map((x) => x.format(dateTimeFormat)),
    );
  }, [next_dates]);

  return (
    <>
      <Modal
        title="服务时间计划"
        open={show}
        onCancel={() => _show(false)}
        onOk={() => _show(false)}
        width={'80%'}
        destroyOnClose
        forceRender
      >
        <Row gutter={[10, 10]}>
          <Col span={12}>
            <XCron
              data={data.CronRules || []}
              ok={(x) => {
                ok({
                  ...data,
                  CronRules: x,
                });
              }}
            />
          </Col>
          <Col span={12}>
            <XTimeRange
              data={data.TimeRanges || []}
              ok={(x) => {
                ok({
                  ...data,
                  TimeRanges: x,
                });
              }}
            />
          </Col>
          <Divider type="horizontal" orientation="left">
            例外情况
          </Divider>
          <Col span={12}>
            <XExceptionTime
              data={data.ExceptionTimes || []}
              ok={(x) => {
                ok({
                  ...data,
                  ExceptionTimes: x,
                });
              }}
            />
          </Col>
        </Row>
      </Modal>
      <Card
        size="small"
        title={'服务时间计划表'}
        style={{
          marginTop: 10,
        }}
        extra={
          <Button
            type="link"
            onClick={() => {
              _show(true);
            }}
          >
            编辑
          </Button>
        }
      >
        <Alert
          style={{
            marginBottom: 10,
          }}
          type={'info'}
          message={`为了性能考虑，只展示了近期时间计划`}
        />
        <Calendar
          mode={'month'}
          onSelect={(e) => {
            console.log(e.format(dateTimeFormat));
          }}
          cellRender={(date, info) => {
            return (
              <>
                {renderDateCell(date)}
                {renderExceptionTime(date)}
              </>
            );
          }}
        />
      </Card>
    </>
  );
};
