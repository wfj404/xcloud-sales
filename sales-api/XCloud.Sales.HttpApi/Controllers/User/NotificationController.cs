﻿using XCloud.Application.Model;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Notifications;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("/api/mall/notification")]
public class NotificationController : ShopBaseController
{
    private readonly INotificationService _notificationService;

    public NotificationController(INotificationService notificationService)
    {
        _notificationService = notificationService;
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateNotificationStatusInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var entity = await _notificationService.GetByIdAsync(dto.Id);

        if (entity == null || entity.UserId != loginUser.Id)
            throw new EntityNotFoundException(nameof(DeleteNotification));

        await _notificationService.UpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<NotificationDto>> Paging([FromBody] QueryNotificationPagingInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = loginUser.Id;

        var res = await _notificationService.QueryPagingAsync(dto);

        return res;
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteNotification([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var entity = await _notificationService.GetByIdAsync(dto.Id);

        if (entity == null || entity.UserId != loginUser.Id)
            throw new EntityNotFoundException(nameof(DeleteNotification));

        await _notificationService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("delete-all")]
    public async Task<ApiResponse<object>> DeleteAllAsync()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        await _notificationService.DeleteAllByUserIdAsync(loginUser.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("unread-count")]
    public async Task<ApiResponse<int>> UnReadCountAsync()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var count = await _notificationService.UnreadCountAsync(loginUser.Id,
            new CacheStrategy { Cache = true });

        return new ApiResponse<int>(count);
    }
}