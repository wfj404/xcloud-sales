﻿using System.IO;
using System.Net.Http;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.Http;
using XCloud.Application.Images;
using XCloud.Application.Mapper;
using XCloud.AspNetMvc.Filters;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Core.Http;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/storage")]
public class StorageController : PlatformBaseController
{
    private readonly IStorageService _fileUploadService;
    private readonly IStorageMetaService _storageMetaService;
    private readonly IImageProcessingService _imageProcessingService;
    private readonly IThumborService _thumborService;
    private readonly StorageUtils _storageHelper;

    public StorageController(IStorageService fileUploadService,
        IStorageMetaService storageMetaService,
        IImageProcessingService imageProcessingService,
        IThumborService thumborService,
        StorageUtils storageHelper)
    {
        _storageMetaService = storageMetaService;
        _imageProcessingService = imageProcessingService;
        _thumborService = thumborService;
        _storageHelper = storageHelper;
        _fileUploadService = fileUploadService;
    }

    private const int MaxBytes = 1024 * 1024 * 2;

    [HttpPost("upload")]
    [RequestSizeLimit(MaxBytes)]
    public async Task<ApiResponse<StorageMetaDto[]>> UploadAsync([FromForm] IFormFileCollection formFileCollection)
    {
        if (ValidateHelper.IsEmptyCollection(formFileCollection))
        {
            return new ApiResponse<StorageMetaDto[]>().SetError("empty");
        }

        if (formFileCollection.Count > 3)
        {
            return new ApiResponse<StorageMetaDto[]>().SetError("3 files is the limitation");
        }

        foreach (var m in formFileCollection)
        {
            if (m.Length > MaxBytes)
            {
                return new ApiResponse<StorageMetaDto[]>().SetError($"{m.FileName} reach the limit size");
            }
        }

        var sb = new StringBuilder();
        sb.AppendLine(nameof(UploadAsync));

        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();
        sb.AppendLine("uploader:");
        sb.AppendLine(this.JsonDataSerializer.SerializeToString(loginUser));


        var resultList = new List<StorageMetaDto>();

        foreach (var file in formFileCollection)
        {
            await using var stream = file.OpenReadStream();
            var args = new FileUploadStreamArgs(stream)
            {
                FileName = file.FileName,
                ContentType = file.ContentType
            };

            var meta = await _fileUploadService.UploadStreamAsync(args);

            var dto = meta.MapTo<StorageMeta, StorageMetaDto>(ObjectMapper);

            resultList.Add(dto);

            sb.AppendLine(this.JsonDataSerializer.SerializeToString(dto));
        }

        this.Logger.LogInformation(message: sb.ToString());

        return new ApiResponse<StorageMetaDto[]>(resultList.ToArray());
    }

    [NonActionLogging]
    [HttpGet("file/origin/{key}")]
    public async Task OriginFileAsync([FromRoute] string key)
    {
        this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
        this.HttpContext.Response.ContentType = MimeTypes.Text.Plain;

        if (!string.IsNullOrWhiteSpace(key))
        {
            var contentType =
                await _storageMetaService.GetContentTypeOrDefaultAsync(key, defaultContentType: MimeTypes.Image.Png);

            var stream = await _fileUploadService.GetFileStreamOrNullAsync(key);

            if (stream == null)
            {
                await this.HttpContext.Response.WriteAsync("file not exist");
                return;
            }

            await using (stream)
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
                this.HttpContext.Response.ContentType = contentType;

                await stream.CopyToAsync(this.HttpContext.Response.Body);
            }
        }
    }

    [NonAction]
    private async Task<Stream> GetCompressedStreamOrNullAsync(string key,
        int height,
        int width)
    {
        var thumborEnabled = Configuration.IsThumborEnabled();
        if (thumborEnabled)
        {
            var gatewayAddress = await ServiceDiscoveryService.GetRequiredInternalGatewayAddressAsync();

            var thumborUrl = new[] { gatewayAddress, "/api/platform/storage/file/origin/", key }.ConcatUrl();

            var stream = await _thumborService.GetResizedStreamOrNullAsync(thumborUrl, height, width);

            return stream;
        }
        else
        {
            var bs = await _fileUploadService.GetFileBytesOrNullAsync(key);
            if (ValidateHelper.IsNotEmptyCollection(bs))
            {
                var compressedBytes = await _imageProcessingService.ResizeToPngAsync(bs, width, height);
                this.HttpContext.Response.ContentType = MimeTypes.Image.Png;
                return new MemoryStream(buffer: compressedBytes);
            }
        }

        return null;
    }

    [NonActionLogging]
    [HttpGet("file/{w}x{h}/{key}")]
    public async Task ResizedFileAsync([FromRoute] string key,
        [FromRoute] int? h,
        [FromRoute] int? w)
    {
        h ??= 0;
        w ??= 0;

        this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
        this.HttpContext.Response.ContentType = MimeTypes.Text.Plain;

        if (!string.IsNullOrWhiteSpace(key))
        {
            var contentType = await _storageMetaService.GetContentTypeOrDefaultAsync(key, MimeTypes.Image.Png);
            this.HttpContext.Response.ContentType = contentType;

            //resize and cache
            var stream = await TryGetFileCompressedStreamOrNullAsync();
            if (stream == null)
            {
                await this.HttpContext.Response.WriteAsync("file not found");
                return;
            }

            await using (stream)
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
                await stream.CopyToAsync(this.HttpContext.Response.Body);
            }
        }

        async Task<Stream> TryGetFileCompressedStreamOrNullAsync()
        {
            if ((h.Value > 0 || w.Value > 0) && _storageHelper.IsImage(key))
            {
                //resize and cache
                return await GetCompressedStreamOrNullAsync(key, h.Value, w.Value);
            }
            else
            {
                return await _fileUploadService.GetFileStreamOrNullAsync(key);
            }
        }
    }

    private const string ResizeUrlPath = "sales-url-resize/unsafe";

    [NonActionLogging]
    [HttpGet($"{ResizeUrlPath}/{{url}}/{{w}}x{{h}}/resize.png")]
    public async Task ResizedUrlAsync([FromRoute] string url,
        [FromRoute] int? h,
        [FromRoute] int? w)
    {
        this.HttpContext.Response.ContentType = MimeTypes.Image.Png;

        if (string.IsNullOrWhiteSpace(url))
        {
            this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            return;
        }

        url = HttpUtility.UrlDecode(url);

        if (url.Contains(ResizeUrlPath, StringComparison.OrdinalIgnoreCase))
        {
            throw new ArgumentException(message: $"{nameof(ResizedUrlAsync)}-{url}", paramName: nameof(url));
        }

        h ??= 0;
        w ??= 0;

        var client = this.HttpContext.RequestServices.GetRequiredService<IHttpClientFactory>()
            .CreateClient(nameof(ResizedUrlAsync));

        using var response = await client.GetAsync(url);

        response.EnsureSuccessStatusCode();

        var contentType = response.Headers.GetContentTypeOrDefault();

        if (!string.IsNullOrWhiteSpace(contentType))
        {
            this.HttpContext.Response.ContentType = contentType;
        }

        await using var stream = await response.Content.ReadAsStreamAsync();

        if ((h.Value > 0 || w.Value > 0) && _storageHelper.IsImage(url))
        {
            var bs = await stream.GetStreamBytesAsync(maxLength: 1024 * 1024 * 10);
            bs = await this._imageProcessingService.ResizeToPngAsync(bs, w.Value, h.Value);

            await this.HttpContext.Response.Body.WriteAsync(bs);
        }
        else
        {
            await stream.CopyToAsync(this.HttpContext.Response.Body);
        }
    }
}