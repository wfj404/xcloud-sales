﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/order-review")]
public class OrderReviewController : ShopBaseController
{
    private readonly IOrderReviewService _orderReviewService;
    private readonly IOrderReviewItemService _orderReviewItemService;
    private readonly IOrderService _orderService;
    private readonly ISkuService _skuService;
    private readonly IStoreUserService _storeUserService;
    private readonly IOrderItemService _orderItemService;

    public OrderReviewController(IOrderReviewService orderReviewService,
        IOrderService orderService, ISkuService skuService,
        IStoreUserService storeUserService,
        IOrderReviewItemService orderReviewItemService,
        IOrderItemService orderItemService)
    {
        _orderReviewService = orderReviewService;
        _orderService = orderService;
        _skuService = skuService;
        _storeUserService = storeUserService;
        _orderReviewItemService = orderReviewItemService;
        _orderItemService = orderItemService;
    }

    [HttpPost("approve-review")]
    public async Task<ApiResponse<object>> ApproveReviewAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderReviewService.EnsureStoreOrderReviewAsync(dto.Id, manager.StoreId);

        await _orderReviewService.ApproveReviewAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("by-order-id")]
    public async Task<ApiResponse<OrderReviewDto>> ByOrderIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var order = await this._orderService.EnsureStoreOrderAsync(dto, manager.StoreId);

        var review = await this._orderReviewService.GetReviewByOrderIdAsync(order.Id);

        if (review != null)
        {
            await _orderReviewService.AttachItemsAsync(new[] { review });
            await _orderReviewService.AttachPicturesAsync(new[] { review });
            await _orderReviewItemService.AttachPicturesAsync(review.Items);
        }

        return new ApiResponse<OrderReviewDto>(review);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<OrderReviewDto>> QueryOrderReviewPagingAsync(
        [FromBody] QueryOrderReviewPaging dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        dto.StoreId = manager.StoreId;
        dto.PageSize = 20;

        var response = await _orderReviewService.QueryPagingAsync(dto);

        if (response.IsEmpty)
            return response;

        await _orderReviewService.AttachPicturesAsync(response.Items.ToArray());
        await _orderReviewService.AttachItemsAsync(response.Items.ToArray());
        await this._orderReviewService.AttachStoreUserAsync(response.Items.ToArray());
        var storeUsers = response.Items.Select(x => x.StoreUser).WhereNotNull().ToArray();
        await this._storeUserService.AttachPlatformUsersAsync(storeUsers);

        var items = response.Items.SelectMany(x => x.Items).ToArray();
        await _orderReviewItemService.AttachPicturesAsync(items);
        await this._orderReviewItemService.AttachOrderItemAsync(items);

        var orderItems = items.Select(x => x.OrderItem).WhereNotNull().ToArray();
        await this._orderItemService.AttachOrderItemSkuAsync(orderItems);
        var skus = orderItems.Select(x => x.Sku).WhereNotNull().ToArray();
        await this._skuService.AttachGoodsAsync(skus);

        return response;
    }
}