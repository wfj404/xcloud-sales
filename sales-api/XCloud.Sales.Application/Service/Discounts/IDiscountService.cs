using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Discounts;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Discounts;

public interface IDiscountService : ISalesStringCrudAppService<Discount, DiscountDto, QueryDiscountPagingInput>,
    ISalesAppService
{
    Task SaveStoreMappingAsync(Discount discount, string[] storeIds);

    Task<DiscountDto[]> AttachStoresAsync(DiscountDto[] data);
}

public class DiscountService : SalesStringCrudAppService<Discount, DiscountDto, QueryDiscountPagingInput>,
    IDiscountService
{
    private readonly ISalesRepository<Discount> _repository;
    private readonly IStoreMappingService _storeMappingService;

    public DiscountService(ISalesRepository<Discount> repository,
        IStoreMappingService storeMappingService) : base(repository)
    {
        _repository = repository;
        _storeMappingService = storeMappingService;
    }

    public async Task SaveStoreMappingAsync(Discount discount, string[] storeIds)
    {
        await _storeMappingService.SaveStoreMappingAsync(discount, storeIds);
    }

    public async Task<DiscountDto[]> AttachStoresAsync(DiscountDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Where(x => x.LimitedToStore).Ids();

        if (ids.Any())
        {
            var db = await _repository.GetDbContextAsync();
            var typeName = this.CommonUtils.GetRequiredTypeIdentityName<Discount>();
            var storeTypeName = this.CommonUtils.GetRequiredTypeIdentityName<Store>();

            var mappingQuery = db.Set<CommonEntityMapping>().AsNoTracking();

            var query = from mapping in mappingQuery
                join store in db.Set<Store>().AsNoTracking()
                    on mapping.TargetId equals store.Id
                select new { mapping, store };

            query = query
                .Where(x => x.mapping.EntityName == typeName)
                .Where(x => x.mapping.TargetName == storeTypeName)
                .Where(x => ids.Contains(x.mapping.EntityId));

            var list = await query.ToArrayAsync();

            foreach (var m in data)
            {
                var stores = list.Where(x => x.mapping.EntityId == m.Id).Select(x => x.store).ToArray();

                m.Stores = stores.MapArrayTo<Store, StoreDto>(ObjectMapper);
            }
        }

        return data;
    }

    protected override void TrySetCreationTime(Discount entity)
    {
        entity.CreationTime = Clock.Now;
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Discount entity, DiscountDto dto)
    {
        await Task.CompletedTask;

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.DiscountPercentage = dto.DiscountPercentage;
        entity.LimitedToStore = dto.LimitedToStore;
        entity.StartTime = dto.StartTime;
        entity.EndTime = dto.EndTime;
        entity.LimitedToSku = dto.LimitedToSku;
        entity.IsActive = dto.IsActive;
        entity.Sort = dto.Sort;
    }

    protected override async Task SetFieldsBeforeInsertAsync(Discount entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
    }

    protected override async Task<IQueryable<Discount>> GetPagingFilteredQueryableAsync(DbContext db,
        QueryDiscountPagingInput dto)
    {
        await Task.CompletedTask;

        var query = db.Set<Discount>().AsNoTracking().IgnoreQueryFilters();

        if (dto.Time != null)
        {
            query = query.FilterByTimeRange(dto.Time.Value);
        }

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
        {
            query = _storeMappingService.ApplyStoreMappingFilter(db, query, new[] { dto.StoreId });
        }

        return query;
    }

    protected override async Task<IOrderedQueryable<Discount>> GetPagingOrderedQueryableAsync(
        IQueryable<Discount> query, QueryDiscountPagingInput dto)
    {
        await Task.CompletedTask;

        return query
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id);
    }

    public override Task DeleteByIdAsync(string id)
    {
        return this.Repository.SoftDeleteAsync(id, this.Clock.Now);
    }
}