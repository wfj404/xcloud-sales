import { Space, Tooltip } from 'antd';
import { ReactNode } from 'react';

import {
  dateFormat,
  dateTimeFormat,
  dateTimeFormatWithSecond,
  monthDayFormat,
  myDayjs,
  parseAsDayjs,
  timezoneOffset,
  weekFormat,
} from '@/utils/dayjs';
import { Box } from '@mui/material';

export default ({
  timeStr,
  fallback,
  onlyDate,
  prefix,
}: {
  timeStr?: string | null;
  fallback?: ReactNode | any;
  onlyDate?: boolean;
  prefix?: ReactNode;
}) => {
  const djs = parseAsDayjs(timeStr)?.add(timezoneOffset, 'hour');

  if (!djs) {
    return fallback || null;
  }

  const now = myDayjs().utc().add(timezoneOffset, 'hour');
  const same_year = djs.isSame(now, 'year'); // 是否是同一年
  const diff = djs.diff(now, 'day');

  const formatTime = same_year
    ? djs.format(monthDayFormat)
    : djs.format(dateFormat);

  const renderDetail = () => {
    return (
      <div>
        <Space direction="horizontal">
          <div>{djs.fromNow()}</div>
          <div>{djs.format(weekFormat)}</div>
        </Space>
        <div>{djs.format(dateTimeFormatWithSecond)}</div>
      </div>
    );
  };

  return (
    <Tooltip title={renderDetail()}>
      <Space direction="horizontal">
        {prefix}
        <Box
          sx={{
            cursor: 'pointer',
            display: 'inline-block',
            '&:hover': {
              backgroundColor: (t) => t.palette.warning.main,
            },
          }}
        >
          {onlyDate ? djs.format(dateFormat) : djs.format(dateTimeFormat)}
        </Box>
      </Space>
    </Tooltip>
  );
};
