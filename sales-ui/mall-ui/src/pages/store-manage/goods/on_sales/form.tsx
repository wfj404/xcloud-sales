import { useRequest } from 'ahooks';
import { Form, InputNumber, message, Modal, Spin, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StoreGoodsMappingDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: StoreGoodsMappingDto;
  ok: () => void;
}) => {
  const [model, _model] = useState<StoreGoodsMappingDto>({});

  const save_request = useRequest(apiClient.mallManager.skuUpdate, {
    manual: true,
    onSuccess(res, params) {
      handleResponse(res, () => {
        message.success('保存成功');
        ok();
      });
    },
  });

  const save = (row: StoreGoodsMappingDto) => {
    save_request.run(row);
  };

  useEffect(() => {
    _model({
      ...(data || {}),
    });
  }, [data]);

  return (
    <>
      <Modal
        title={'商品'}
        open={show}
        onCancel={() => hide()}
        onOk={() => save(model)}
      >
        <Spin spinning={save_request.loading}>
          <Form
            onFinish={(e) => save(e)}
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="库存">
              <InputNumber
                value={model.StockQuantity}
                onChange={(e) => {
                  _model({
                    ...model,
                    StockQuantity: e || 0,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="状态">
              <Switch
                checked={model.Active || false}
                onChange={(e) => {
                  _model({
                    ...model,
                    Active: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
