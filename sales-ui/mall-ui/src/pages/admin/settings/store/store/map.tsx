import 'leaflet/dist/leaflet.css';

import L from 'leaflet';
import { useEffect, useRef } from 'react';
import { MapContainer, Marker, TileLayer, Tooltip } from 'react-leaflet';

import {
  marker_icon,
  tile_server,
  wuxi_default_location,
} from '@/components/common/map/utils';
import { getStoreLatLng } from '@/utils/biz';
import { StoreDto } from '@/utils/swagger';
import { Box } from '@mui/material';

export default ({
  stores,
  onClick,
  selectedId,
}: {
  stores: StoreDto[];
  onClick: (e: StoreDto) => void;
  selectedId?: string;
}) => {
  const map = useRef<L.Map | null>(null);

  useEffect(() => {
    const locations_list = stores
      .map((x) => getStoreLatLng(x))
      .filter((x) => x != undefined)
      .map((x) => x || wuxi_default_location);
    map.current?.flyToBounds(locations_list.map((x) => [x.lat, x.lng]));
  }, [stores]);

  useEffect(() => {
    const store = stores.find((x) => x.Id == selectedId);
    if (store) {
      const latlng = getStoreLatLng(store);
      if (latlng) {
        map.current?.flyTo(latlng);
      }
    }
  }, [selectedId]);

  const renderMarkers = () => {
    return stores.map((x, i) => {
      const latlon = getStoreLatLng(x);
      if (!latlon) {
        return null;
      }

      return (
        <Marker
          key={i}
          draggable={false}
          position={latlon}
          icon={marker_icon}
          opacity={x.Id == selectedId ? 1 : 0.7}
          riseOnHover
          eventHandlers={{
            click: (e) => {
              onClick(x);
            },
          }}
        >
          <Tooltip
            eventHandlers={{
              overlayadd: (e) => {
                (e.target as L.Tooltip)?.openTooltip();
              },
            }}
          >
            {x.Name || '--'}
          </Tooltip>
        </Marker>
      );
    });
  };

  return (
    <>
      <div
        style={{
          height: '100%',
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
        }}
      >
        <Box flexGrow={1}>
          <MapContainer
            ref={map}
            fadeAnimation
            markerZoomAnimation
            zoomAnimation
            attributionControl={false}
            center={wuxi_default_location}
            zoom={13}
            style={{ height: '100%', width: '100%' }}
            whenReady={() => {
              setTimeout(() => {
                //map.current?.fireEvent('resize');
                //map.current?.invalidateSize(true);
                window.dispatchEvent(new Event('resize'));
              }, 3000);
            }}
          >
            <TileLayer url={tile_server} attribution={''} />
            {renderMarkers()}
          </MapContainer>
        </Box>
      </div>
    </>
  );
};
