using Microsoft.Extensions.Logging;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Coupons;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/coupon")]
public class CouponController : ShopBaseController
{
    private readonly ICouponService _couponService;
    private readonly IUserCouponService _userCouponService;
    private readonly CouponUtils _couponUtils;

    public CouponController(ICouponService couponService, IUserCouponService userCouponService, CouponUtils couponUtils)
    {
        _couponService = couponService;
        _userCouponService = userCouponService;
        _couponUtils = couponUtils;
    }

    [HttpPost("select-best-coupon")]
    public async Task<ApiResponse<CouponUserMappingDto[]>> BestCouponAsync([FromBody] OrderDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var storeUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var response = await _userCouponService.QueryUserCouponPagingAsync(new QueryUserCouponPagingInput()
        {
            IsDeleted = false,
            UserId = storeUser.Id,
            AvailableFor = Clock.Now,
            Used = false,
            SkipCalculateTotalCount = true,
            Page = 1,
            PageSize = 100
        });

        if (response.IsEmpty)
        {
            return new ApiResponse<CouponUserMappingDto[]>();
        }

        var couponList = response.Items.ToArray();

        await this._userCouponService.AttachCouponAsync(couponList);

        couponList = couponList.Where(x => x.Coupon != null).ToArray();

        foreach (var m in couponList)
        {
            try
            {
                m.Coupon.ValidationErrors =
                    await this._couponUtils.GetValidationErrorsAsync(m.Coupon, dto);
            }
            catch (Exception e)
            {
                this.Logger.LogError(message: e.Message, exception: e);
                m.Coupon.ValidationErrors = new string[] { "failed to validate coupon" };
            }
        }

        couponList = couponList
            .Where(x => ValidateHelper.IsEmptyCollection(x.Coupon.ValidationErrors))
            .OrderByDescending(x => x.Coupon.Price)
            .ToArray();

        return new ApiResponse<CouponUserMappingDto[]>(couponList);
    }

    [Obsolete]
    [HttpPost("paging")]
    public async Task<PagedResponse<CouponDto>> QueryCouponPagingAsync([FromBody] QueryCouponPagingInput dto)
    {
        dto.AvailableFor = Clock.Now;
        dto.SkipCalculateTotalCount = true;

        var response = await _couponService.QueryPagingAsync(dto);

        var storeUserOrNull = await StoreUserAuthService.GetStoreUserOrNullAsync();
        if (storeUserOrNull != null && response.IsNotEmpty)
        {
            await _userCouponService.CheckCanIssueAsync(response.Items.ToArray(), storeUserOrNull.Id);
        }

        return response;
    }

    [HttpPost("user-coupon-paging")]
    public async Task<PagedResponse<CouponUserMappingDto>> QueryUserCouponPagingAsync(
        [FromBody] QueryUserCouponPagingInput dto)
    {
        var storeUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.IsDeleted = false;
        dto.UserId = storeUser.Id;
        dto.AvailableFor = Clock.Now;
        dto.Used = false;
        dto.SkipCalculateTotalCount = true;

        var response = await _userCouponService.QueryUserCouponPagingAsync(dto);

        if (response.IsEmpty)
            return response;

        await _userCouponService.AttachCouponAsync(response.Items.ToArray());

        return response;
    }

    [HttpPost("issue-coupon")]
    public async Task<ApiResponse<object>> IssueCouponAsync([FromBody] IssueCouponInput dto)
    {
        var storeUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = storeUser.Id;

        await _userCouponService.CreateUserCouponAsync(new CouponUserMappingDto
        {
            UserId = dto.UserId,
            CouponId = dto.CouponId
        });

        return new ApiResponse<object>();
    }
}