﻿using Microsoft.Extensions.Configuration;

namespace XCloud.Core.Http.Dynamic;

public interface IDynamicClientServiceDiscovery
{
    string GetServiceAddress(string serviceName);
}

internal class ServiceDiscoveryViaConfiguration : IDynamicClientServiceDiscovery
{
    private readonly IConfiguration _configuration;

    public ServiceDiscoveryViaConfiguration(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public string GetServiceAddress(string serviceName)
    {
        if (string.IsNullOrWhiteSpace(serviceName))
            throw new ArgumentNullException(nameof(serviceName));

        var res = _configuration[$"RemoteServices:{serviceName}:BaseUrl"];
        return res;
    }
}