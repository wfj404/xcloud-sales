import XDefaultImage from '@/assets/empty.svg';
import XImage from '@/components/common/image';
import XLoading from '@/components/common/loading/skeleton';
import XPrice from '@/components/goods/sku/price';
import { getSkuName, sortGoodsPictures } from '@/utils/biz';
import { resolveUrl } from '@/utils/storage';
import {
  OrderItemDto,
  PlaceOrderDataHolder,
  PlaceOrderRequestDto,
  PlaceOrderRequestItemDto,
} from '@/utils/swagger';
import { formatMoney, isArrayEmpty } from '@/utils/utils';
import { Box, Divider, InputBase, Stack, Typography } from '@mui/material';

export default ({
  holder,
  model,
  onChange,
  loading,
}: {
  holder: PlaceOrderDataHolder;
  model: PlaceOrderRequestDto;
  onChange: (e: PlaceOrderRequestDto) => void;
  loading?: boolean;
}) => {
  const selectedItems = holder?.Order?.Items || [];

  if (loading && isArrayEmpty(selectedItems)) {
    return <XLoading />;
  }

  if (isArrayEmpty(selectedItems)) {
    return <h3>请选择商品</h3>;
  }

  const renderItem = (item: OrderItemDto) => {
    const sku = item.Sku;
    if (!sku) {
      return <p>商品信息缺失</p>;
    }

    const errors =
      holder.SkuErrors?.find((d) => d.Id == sku.Id)?.ErrorList || [];

    const discount = sku.PriceModel?.DiscountModel;

    const grade = sku.PriceModel?.GradePriceModel;

    const meta = sortGoodsPictures(sku.Goods?.GoodsPictures || [], sku.Id).at(
      0,
    );
    const goodsImageUrl = resolveUrl(meta?.StorageMeta, {
      width: 200,
    });

    const placeorderItem = model.Items?.find((x) => x.SkuId == sku.Id);

    return (
      <>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}
        >
          <Box sx={{ width: '50px' }}>
            <XImage
              rate={1}
              src={goodsImageUrl || XDefaultImage}
              fit={'cover'}
              width={'100%'}
              placeholder={<img src={XDefaultImage} width="100%" alt="" />}
              style={{}}
              alt={''}
            />
          </Box>
          <Box sx={{ ml: 1 }} flexGrow={1}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'flex-start',
                justifyContent: 'space-between',
              }}
            >
              <Box flexGrow={1} sx={{ mx: 1 }}>
                <Typography variant="overline" component={'div'}>
                  {getSkuName(sku)}
                </Typography>
                <InputBase
                  size="small"
                  placeholder="备注此商品"
                  fullWidth
                  value={placeorderItem?.Remark || ''}
                  onChange={(e) => {
                    let datalist: PlaceOrderRequestItemDto[] = [
                      ...(model.Items || []),
                    ];
                    datalist = datalist.filter((x) => x.SkuId != sku.Id);
                    datalist = [
                      ...datalist,
                      {
                        ...placeorderItem,
                        SkuId: sku.Id,
                        Remark: e.target.value,
                      },
                    ];

                    onChange({ ...model, Items: datalist });
                  }}
                  sx={{
                    borderRadius: 1,
                    fontSize: 12,
                    color: 'gray',
                  }}
                />
                {errors.map((x, i) => {
                  return (
                    <Typography
                      key={i}
                      variant="caption"
                      color="error"
                      component={'div'}
                    >
                      {x}
                    </Typography>
                  );
                })}
              </Box>
              <div style={{ display: 'inline-block' }}>
                <Box>
                  <XPrice model={sku} />
                </Box>
                {discount != undefined && (
                  <Typography
                    variant="caption"
                    color="text.disabled"
                    component={'div'}
                  >{`“${discount.Name}”优惠,${
                    (discount.DiscountPercentage || 0) * 10
                  }折`}</Typography>
                )}
                {grade != undefined && (grade.PriceOffset || 0) != 0 && (
                  <Typography
                    variant="caption"
                    color="text.disabled"
                    component={'div'}
                  >{`“${grade.Grade?.Name}”，差价${formatMoney(
                    grade.PriceOffset || 0,
                  )}`}</Typography>
                )}
                <Typography
                  variant="caption"
                  color="text.disabled"
                  component={'div'}
                >{`x${item.Quantity || 0}`}</Typography>
              </div>
            </div>
          </Box>
        </div>
      </>
    );
  };

  return (
    <>
      <Stack
        direction="column"
        spacing={1}
        divider={<Divider orientation="horizontal" flexItem />}
      >
        {selectedItems.map((x, i) => {
          return (
            <Box key={i} sx={{ my: 1 }}>
              {renderItem(x)}
            </Box>
          );
        })}
      </Stack>
    </>
  );
};
