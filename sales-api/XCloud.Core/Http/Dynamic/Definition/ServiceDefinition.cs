﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc.Routing;

namespace XCloud.Core.Http.Dynamic.Definition;

public class ServiceDefinition
{
    public Type ServiceInterface { get; }
    public DynamicHttpClientAttribute ClientConfiguration { get; }
    public IRouteTemplateProvider RouteTemplateProvider { get; }

    public IReadOnlyList<ActionDefinition> AllMethodDefinitions { get; }
    public IReadOnlyList<ActionDefinition> ActionDefinitions => AllMethodDefinitions.Where(x => x.IsAction()).ToArray();

    public ServiceDefinition(Type serviceContract)
    {
        if (serviceContract == null)
            throw new ArgumentNullException(nameof(serviceContract));
        
        serviceContract.IsInterface.Should().BeTrue();

        ServiceInterface = serviceContract;

        var attrs = serviceContract.GetCustomAttributes().ToArray();
        RouteTemplateProvider = attrs.OfType<IRouteTemplateProvider>().FirstOrDefault();
        ClientConfiguration = attrs.OfType<DynamicHttpClientAttribute>().FirstOrDefault();

        AllMethodDefinitions = serviceContract.GetMethods().Select(x => new ActionDefinition(this, x)).ToArray();
    }

    public bool IsValidService()
    {
        if (ClientConfiguration == null)
        {
            return false;
        }
        return true;
    }

    public string RouteOrEmpty() => RouteTemplateProvider?.Template ?? string.Empty;
}