using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(BrandViewService))]
public class BrandViewService : SalesAppService
{
    private readonly IBrandService _brandService;

    public BrandViewService(IBrandService brandService)
    {
        _brandService = brandService;
    }

    public async Task<BrandDto[]> QueryAllBrandsAsync(CacheStrategy cacheStrategy)
    {
        var key = $"mall.brands.all.view";
        var option = new CacheOption<BrandDto[]>(key, TimeSpan.FromMinutes(5));

        var response = await CacheProvider.ExecuteWithPolicyAsync(
            QueryAllBrandsAsync, option, cacheStrategy);

        if (response == null)
            response = Array.Empty<BrandDto>();

        return response;
    }

    private async Task<BrandDto[]> QueryAllBrandsAsync()
    {
        var dto = new QueryBrandPaging
        {
            Published = true,
            ShowOnHomePage = true,
            Page = 1,
            PageSize = 1000,
            SkipCalculateTotalCount = true
        };

        var response = await _brandService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _brandService.AttachPicturesAsync(response.Items.ToArray());
        }

        return response.Items.ToArray();
    }
}