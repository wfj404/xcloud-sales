using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/sku")]
public class SkuController : ShopBaseController
{
    private readonly ISkuService _skuService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly IGoodsPictureService _goodsPictureService;

    public SkuController(ISkuService skuService,
        IStoreGoodsMappingService storeGoodsMappingService,
        IGoodsPictureService goodsPictureService)
    {
        _skuService = skuService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _goodsPictureService = goodsPictureService;
    }

    [HttpPost("update-store-sku-mapping")]
    public async Task<ApiResponse<StoreGoodsMapping>> UpdateAsync([FromBody] StoreGoodsMappingDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var entity = await this._storeGoodsMappingService.UpdateAsync(dto);

        return new ApiResponse<StoreGoodsMapping>(entity);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<StoreGoodsMappingDto>> QueryPagingAsync(
        [FromBody] QueryStoreGoodsMappingPagingInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        dto.StoreId = manager.StoreId;

        var response = await _storeGoodsMappingService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _storeGoodsMappingService.AttachSkusAsync(response.Items.ToArray());
            var skus = response.Items.Select(x => x.Sku).WhereNotNull().ToArray();
            await _goodsPictureService.AttachImagesAsync(skus);

            await _skuService.AttachGoodsAsync(skus);
            var goods = skus.Select(x => x.Goods).WhereNotNull().ToArray();
            await _goodsPictureService.AttachImagesAsync(goods);
        }

        return response;
    }
}