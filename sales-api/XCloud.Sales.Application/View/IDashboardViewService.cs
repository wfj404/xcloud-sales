using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.AfterSale;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Coupons;
using XCloud.Sales.Application.Service.GiftCards;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Report;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(DashboardViewService))]
public class DashboardViewService : SalesAppService
{
    private readonly ICouponService _couponService;
    private readonly IGoodsService _goodsService;
    private readonly IOrderService _orderService;
    private readonly IAfterSaleService _afterSaleService;
    private readonly IUserReportService _userReportService;
    private readonly IStoreUserBalanceService _userBalanceService;
    private readonly IGiftCardService _prepaidCardService;
    private readonly IBrandService _brandService;
    private readonly ICategoryService _categoryService;
    private readonly ITagService _tagService;

    public DashboardViewService(ICouponService couponService,
        IGoodsService goodsService, IOrderService orderService,
        IAfterSaleService afterSaleService, IUserReportService userReportService,
        IStoreUserBalanceService userBalanceService,
        IGiftCardService prepaidCardService,
        IBrandService brandService, ICategoryService categoryService, ITagService tagService)
    {
        _brandService = brandService;
        _categoryService = categoryService;
        _tagService = tagService;
        _prepaidCardService = prepaidCardService;
        _userReportService = userReportService;
        _userBalanceService = userBalanceService;
        _afterSaleService = afterSaleService;
        _couponService = couponService;
        _goodsService = goodsService;
        _orderService = orderService;
    }

    private async Task<DashboardCountView> CounterAsync()
    {
        var dto = new DashboardCountView
        {
            Coupon = await _couponService.ActiveCouponCountAsync(),
            Goods = await _goodsService.CountAsync(),
            Orders = await _orderService.QueryPendingCountAsync(new QueryPendingCountInput()),
            AfterSale = await _afterSaleService.QueryPendingCountAsync(new QueryAfterSalePendingCountInput()),
            Balance = await _userBalanceService.CountAllBalanceAsync(),
            PrepaidCard = await _prepaidCardService.QueryUnusedAmountTotalAsync(),
            ActiveUser = await _userReportService.TodayActiveUserCountAsync(),
            Brand = await _brandService.CountAsync(),
            Category = await _categoryService.CountAsync(),
            Tag = await _tagService.CountAsync()
        };

        return dto;
    }

    public async Task<DashboardCountView> CounterAsync(CacheStrategy cacheStrategy)
    {
        if (cacheStrategy == null)
            throw new ArgumentNullException(nameof(cacheStrategy));

        var key = $"mall.manage.dashboard.counter.data";
        var option = new CacheOption<DashboardCountView>(key, TimeSpan.FromMinutes(10));

        var data = await CacheProvider.ExecuteWithPolicyAsync(
            CounterAsync,
            option,
            cacheStrategy);

        data = data ?? new DashboardCountView();

        return data;
    }
}