using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(OrderViewService))]
public class OrderViewService : SalesAppService
{
    private readonly IOrderService _orderService;
    private readonly IOrderItemService _orderItemService;
    private readonly IOrderValueAddedItemService _orderValueAddedItemService;
    private readonly IGoodsPictureService _goodsPictureService;
    private readonly ISkuService _skuService;
    private readonly IStoreUserService _storeUserService;

    public OrderViewService(IOrderService orderService,
        IOrderValueAddedItemService orderValueAddedItemService, ISkuService skuService,
        IGoodsPictureService goodsPictureService,
        IStoreUserService storeUserService, IOrderItemService orderItemService)
    {
        _orderService = orderService;
        _orderValueAddedItemService = orderValueAddedItemService;
        _skuService = skuService;
        _goodsPictureService = goodsPictureService;
        _storeUserService = storeUserService;
        _orderItemService = orderItemService;
    }

    public async Task<OrderDto[]> AttachOrderDetailDataAsync(OrderDto[] orders)
    {
        await _orderService.AttachStoreUsersAsync(orders);
        await _orderService.AttachOrderItemsAsync(orders);
        await _orderService.AttachAfterSalesAsync(orders);
        //ignore
        await _orderService.AttachStoresAsync(orders);
        await _orderService.AttachShippingAddressAsync(orders);

        var mallUsers = orders.Select(x => x.StoreUser).WhereNotNull().ToArray();

        await _storeUserService.AttachPlatformUsersAsync(mallUsers);

        var items = orders.SelectMany(x => x.Items).ToArray();

        //attach goods
        await _orderItemService.AttachOrderItemSkuAsync(items);
        var skus = items.Select(x => x.Sku).WhereNotNull().ToArray();
        await _skuService.AttachGoodsAsync(skus);
        var allGoods = skus.Select(x => x.Goods).WhereNotNull().ToArray();
        await _goodsPictureService.AttachImagesAsync(allGoods);

        //value added items
        await _orderValueAddedItemService.AttachOrderValueAddedItemsAsync(items);
        var orderValueAddedItems = items.SelectMany(x => x.OrderValueAddedItems).ToArray();
        await _orderValueAddedItemService.AttachValueAddedItemsAsync(orderValueAddedItems);

        return orders;
    }
}