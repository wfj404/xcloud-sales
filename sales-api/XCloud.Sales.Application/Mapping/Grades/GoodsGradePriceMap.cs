using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Grades;

namespace XCloud.Sales.Application.Mapping.Grades;

public class GoodsGradePriceMap : SalesEntityTypeConfiguration<GoodsGradePrice>
{
    public override void Configure(EntityTypeBuilder<GoodsGradePrice> builder)
    {
        builder.ToTable("sales_goods_grade_price");

        builder.Property(x => x.SkuId).RequiredId();
        builder.Property(x => x.GradeId).RequiredId();
        builder.Property(x => x.PriceOffset).HasPrecision(18, 2);

        base.Configure(builder);
    }
}