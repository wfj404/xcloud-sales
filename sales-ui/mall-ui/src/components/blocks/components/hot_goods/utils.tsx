import { BrandDto, CategoryDto, GoodsDto, TagDto } from '@/utils/swagger';

import { BlockItem, BlockItemProps, GoodsDisplayType } from '../../utils';

export type HotGoodsBlockType = 'filtered-goods' | 'selected-goods';

export interface HotGoodsBlockSearchParameter {
  keywords?: string;
  brand?: BrandDto;
  category?: CategoryDto;
  tag?: TagDto;
  count?: number;
}

export interface HotGoodsBlockProps extends BlockItemProps {
  type?: HotGoodsBlockType;
  search?: HotGoodsBlockSearchParameter;
  goods_list?: GoodsDto[];
  display_type?: GoodsDisplayType;
}

export const hot_goods_demo: BlockItem<HotGoodsBlockProps> = {
  type: 'hot-goods',
  style: {
    marginBottom: 10,
  },
  body: {
    type: 'filtered-goods',
    search: {},
    display_type: 'slider',
  },
};
