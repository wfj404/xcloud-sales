using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Authentication;

[DisableConventionalRegistration]
public class QingdaoStoreManagerAuthService : SalesAppService, IStoreManagerAuthService
{
    private readonly IRequestCacheProvider _cacheProvider;
    private readonly IUserAuthService _userAuthService;
    private readonly IQingdaoStoreService _qingdaoStoreService;
    private readonly IQingdaoStoreManagerAccountService _qingdaoStoreManagerAccountService;
    private readonly Sh3HClient _sh3HClient;

    public QingdaoStoreManagerAuthService(IRequestCacheProvider cacheProvider,
        IUserAuthService userAuthService,
        IQingdaoStoreService qingdaoStoreService,
        IQingdaoStoreManagerAccountService qingdaoStoreManagerAccountService,
        Sh3HClient sh3HClient)
    {
        _cacheProvider = cacheProvider;
        _userAuthService = userAuthService;
        _qingdaoStoreService = qingdaoStoreService;
        _qingdaoStoreManagerAccountService = qingdaoStoreManagerAccountService;
        _sh3HClient = sh3HClient;
    }

    public async Task<StoreManagerAuthResult> GetStoreManagerAsync(string storeId = null)
    {
        return await this._cacheProvider.GetOrSetAsync(async () =>
        {
            var response = new StoreManagerAuthResult();

            try
            {
                var userAuthResult = await this._userAuthService.GetAuthUserAsync();

                var user = userAuthResult.User;

                if (userAuthResult.HasError() || user == null)
                {
                    return response.SetError(message: $"user auth error",
                        details: this.JsonDataSerializer.SerializeToString(userAuthResult));
                }

                var wapAdminDto = userAuthResult.GetPropertyFromJson<WapAdminDto>(this.JsonDataSerializer);

                if (wapAdminDto == null)
                {
                    return response.SetError(nameof(wapAdminDto));
                }

                wapAdminDto.organization ??= new WapOrganization();

                if (string.IsNullOrWhiteSpace(wapAdminDto.organization.organizationCode))
                {
                    return response.SetError($"empty org code:{wapAdminDto.organization.organizationName}");
                }

                var storeOrg =
                    await this._sh3HClient.ResolveStoreOrganizationByCodeWithCacheAsync(wapAdminDto.organization
                        .organizationCode);

                if (storeOrg == null)
                {
                    return response.SetError("can't find store org");
                }

                var store = await this._qingdaoStoreService.PrepareStoreAsync(storeOrg);

                var storeManager =
                    await this._qingdaoStoreManagerAccountService.PrepareStoreManagerAsync(store, user, wapAdminDto);

                response.SetData(storeManager);

                return response;
            }
            catch (UserFriendlyException e)
            {
                return response.SetError(e.Message);
            }
        });
    }
}