﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SKIT.FlurlHttpClient.Wechat.Api;
using SKIT.FlurlHttpClient.Wechat.Api.Models;
using Volo.Abp.Application.Services;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.WeChat.Services.Mp;

public interface IWxMpService : IApplicationService
{
    string PlatformName { get; }

    Task TryUpdateUserProfileFromWechat(string userId, string accessToken, string openId);

    Task<SnsUserInfoResponse> GetUserWechatProfileAsync(string accessToken, string openId);

    Task<string> GetOrRefreshClientAccessTokenAsync();

    Task<SnsOAuth2AccessTokenResponse> GetUserAccessTokenAsync(string code);

    Task<CgibinTokenResponse> GetClientAccessTokenAsync();
}

public class WxMpService : XCloudAppService, IWxMpService
{
    private readonly IExternalAccessTokenService _externalAccessTokenService;
    private readonly IUserService _userService;
    private readonly IStorageService _storageService;
    private readonly HttpClient _httpClient;
    private readonly ISettingService _settingService;
    private readonly WechatClientFactory _wechatClientFactory;

    public WxMpService(IHttpClientFactory httpClientFactory,
        IStorageService storageService,
        IExternalAccessTokenService userExternalAccessTokenService,
        ISettingService settingService,
        IUserService userService, WechatClientFactory wechatClientFactory)
    {
        _storageService = storageService;
        _externalAccessTokenService = userExternalAccessTokenService;
        _settingService = settingService;
        _userService = userService;
        _wechatClientFactory = wechatClientFactory;
        _httpClient = httpClientFactory.CreateClient(nameof(WxMpService));
    }

    public string PlatformName => ThirdPartyPlatforms.WxMp;

    public async Task<string> GetOrRefreshClientAccessTokenAsync()
    {
        var config = await _settingService.GetWechatMpOptionAsync();
        config.EnsureWechatOptionNotEmpty();

        var token = await _externalAccessTokenService.GetValidClientAccessTokenAsync(
            new GetValidClientAccessTokenDto
            {
                AppId = config.AppId,
                Platform = PlatformName
            });
        var now = Clock.Now;

        if (token == null || token.ExpiredAt < now.AddMinutes(5))
        {
            var lockKey = $"{nameof(WxMpService)}.{nameof(GetOrRefreshClientAccessTokenAsync)}";

            await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
                resource: lockKey,
                expiryTime: TimeSpan.FromSeconds(5));

            var accessToken = new UserExternalAccessTokenDto
            {
                AppId = config.AppId,
                Platform = PlatformName,
                AccessTokenType = (int)AccessTokenTypeEnum.Client
            };
            var tokenResponse = await GetClientAccessTokenAsync();
            accessToken.AccessToken = tokenResponse.AccessToken;
            accessToken.RefreshToken = string.Empty;
            accessToken.ExpiredAt = now.AddSeconds(tokenResponse.ExpiresIn);
            accessToken.DataJson = this.JsonDataSerializer.SerializeToString(tokenResponse);

            await _externalAccessTokenService.InsertAccessTokenAsync(accessToken);

            return accessToken.AccessToken;
        }

        return token.AccessToken;
    }

    public async Task<CgibinTokenResponse> GetClientAccessTokenAsync()
    {
        var client = await this._wechatClientFactory.BuildWechatApiClientAsync();

        var response = await client.ExecuteCgibinTokenAsync(new CgibinTokenRequest { });

        response.ThrowIfNotSuccess(this.JsonDataSerializer, nameof(GetClientAccessTokenAsync));

        return response;
    }

    public async Task<SnsOAuth2AccessTokenResponse> GetUserAccessTokenAsync(string code)
    {
        if (string.IsNullOrWhiteSpace(code))
            throw new ArgumentNullException(nameof(code));

        var client = await this._wechatClientFactory.BuildWechatApiClientAsync();

        var response = await client.ExecuteSnsOAuth2AccessTokenAsync(new SnsOAuth2AccessTokenRequest
        {
            Code = code
        });

        response.ThrowIfNotSuccess(this.JsonDataSerializer, nameof(GetUserAccessTokenAsync));

        return response;
    }

    public async Task<SnsUserInfoResponse> GetUserWechatProfileAsync(string accessToken,
        string openId)
    {
        if (string.IsNullOrWhiteSpace(accessToken) || string.IsNullOrWhiteSpace(openId))
            throw new ArgumentNullException(nameof(GetUserWechatProfileAsync));

        var client = await this._wechatClientFactory.BuildWechatApiClientAsync();

        var response = await client.ExecuteSnsUserInfoAsync(new SnsUserInfoRequest
        {
            AccessToken = accessToken,
            OpenId = openId
        });

        response.ThrowIfNotSuccess(this.JsonDataSerializer, nameof(GetUserWechatProfileAsync));

        return response;
    }

    public async Task TryUpdateUserProfileFromWechat(string userId, string accessToken,
        string openId)
    {
        var response = await GetUserWechatProfileAsync(accessToken, openId);

        if (!string.IsNullOrWhiteSpace(response.Nickname))
        {
            try
            {
                var user = await _userService.GetByIdAsync(userId);
                if (user != null)
                {
                    user.NickName = response.Nickname;
                    await _userService.UpdateProfileAsync(user);
                }
            }
            catch (Exception e)
            {
                Logger.LogError(message: e.Message, exception: e);
            }
        }

        if (!string.IsNullOrWhiteSpace(response.HeadImageUrl) && (
                response.HeadImageUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase) ||
                response.HeadImageUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase)))
        {
            try
            {
                var meta = await _storageService.UploadFromUrlAsync(_httpClient,
                    new FileUploadFromUrl { Url = response.HeadImageUrl, FileName = "wechat-avatar.png" });

                var dto = meta.MapTo<StorageMeta, StorageMetaDto>(ObjectMapper);

                var json = JsonDataSerializer.SerializeToString(dto);

                var profile = await _userService.GetByIdAsync(userId);
                if (profile != null)
                {
                    profile.Avatar = json;
                    await _userService.UpdateProfileAsync(profile);
                }
            }
            catch (Exception e)
            {
                Logger.LogError(message: e.Message, exception: e);
            }
        }
    }
}