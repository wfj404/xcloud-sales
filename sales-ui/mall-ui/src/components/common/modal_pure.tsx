import { ReactNode } from 'react';

import XModal from './modal';

export default ({
  open,
  onClose,
  children,
}: {
  open: boolean;
  onClose: () => void;
  children: ReactNode;
}) => {
  return (
    <>
      <XModal
        open={open}
        onClose={() => {
          onClose && onClose();
        }}
        showCloseButton
        bodyRender={() => <>{children}</>}
      ></XModal>
    </>
  );
};
