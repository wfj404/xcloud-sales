import { useRequest } from 'ahooks';
import { Spin } from 'antd';
import React, { ReactNode } from 'react';
import { useLocation, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { hasAccessToken } from '@/utils/utils';

export default function({ children }: { children: ReactNode }) {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const query_permission_request = useRequest(
    apiClient.sys.manageAdminQueryAdminPermissions,
    {
      manual: true,
      onSuccess(data, params) {
        app.setAdminPermissions(data.data || {});
      },
    },
  );

  React.useEffect(() => {
    if (hasAccessToken()) {
      query_permission_request.run();
    } else {
      app.setAdminAuthResult(undefined);
      app.setAdminPermissions(undefined);
    }
  }, [location.pathname]);

  if (app.adminPermissions == undefined && query_permission_request.loading) {
    return (
      <Spin
        spinning={query_permission_request.loading}
        size='large'
        fullscreen
        tip={<p>加载权限信息...</p>}
      />
    );
  }

  return <>{children}</>;
}
