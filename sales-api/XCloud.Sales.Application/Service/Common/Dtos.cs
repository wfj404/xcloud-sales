﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Address;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Domain.Common;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Common;

public class OrderGroupOrderDto : OrderDto
{
    public bool SpecificSender { get; set; }

    public UserAddressDto Sender { get; set; }
}

[TypeIdentityName("order-group-data")]
public class OrderGroupDataDto : ISettings
{
    public UserAddressDto SharedSender { get; set; }

    private OrderGroupOrderDto[] _orders;

    [NotNull]
    public OrderGroupOrderDto[] Orders
    {
        get => this._orders ??= [];
        set => this._orders = value;
    }
}

public class OrderGroupDto : OrderGroup, IEntityDto<string>
{
    private OrderGroupDataDto _groupData;

    [NotNull]
    public OrderGroupDataDto GroupData
    {
        get => this._groupData ??= new OrderGroupDataDto();
        set => this._groupData = value;
    }
}

public class QueryOrderGroupPagingInput : PagedRequest
{
    public string StoreId { get; set; }
}

public class QueryExternalSkuInput : PagedRequest
{
    public string SystemName { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class ExternalSkuGroupingDto : IEntityDto
{
    public string CombineId { get; set; }

    public DateTime CreationTime { get; set; }

    public SkuDto Sku { get; set; }

    public ExternalSkuDto[] Items { get; set; }
}

public class ExternalSkuDto : ExternalSku, IEntityDto<string>
{
    public SkuDto Sku { get; set; }
}

public class ExternalSkuMappingDto : ExternalSkuMapping, IEntityDto<string>
{
    //
}