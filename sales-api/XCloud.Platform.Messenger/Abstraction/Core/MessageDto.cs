﻿using System;
using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Messenger.Abstraction.Core;

public class MessageDto : IEntityDto
{
    public string MessageType { get; set; }

    public string Data { get; set; }

    public DateTime CreationTime { get; set; }
}

public abstract class MessageBody : IEntityDto
{
    public string Message { get; set; }

    public DateTime SendTime { get; set; }
}

public class BroadCastMessageBody : MessageBody
{
    //
}

public class PingMessageBody : MessageBody
{
    public string Token { get; set; }
}

public class SendToUserMessageBody : MessageBody
{
    public string Sender { get; set; }
    public string Receiver { get; set; }
}