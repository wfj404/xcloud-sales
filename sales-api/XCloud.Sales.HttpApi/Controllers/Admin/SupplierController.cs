using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Warehouses.Service.Warehouses;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/supplier")]
public class SupplierController: ShopBaseController
{
    private readonly ISupplierService _supplierService;

    public SupplierController(ISupplierService supplierService)
    {
        _supplierService = supplierService;
    }
    
    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] SupplierDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _supplierService.InsertAsync(dto);
        }
        else
        {
            await _supplierService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateSupplierStatusInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        await _supplierService.UpdateStatusAsync(dto);
        
        return new ApiResponse<object>();
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<SupplierDto>> QueryPagingAsync([FromBody] QuerySupplierPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        var response = await _supplierService.QueryPagingAsync(dto);

        return response;
    }
    
    [HttpPost("all")]
    public async Task<ApiResponse<SupplierDto[]>> QueryAllAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        var response = await _supplierService.QueryAllAsync();

        return new ApiResponse<SupplierDto[]>(response);
    }
}