﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Users;

namespace XCloud.Platform.Application.Mapping.Users;

public class UserIdentityMap : PlatformEntityTypeConfiguration<UserIdentity>
{
    public override void Configure(EntityTypeBuilder<UserIdentity> builder)
    {
        builder.ToTable("sys_user_identity");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.UserId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Identity).IsRequired().HasMaxLength(50);

        builder.Property(x => x.Data).HasMaxLength(4000);

        builder.HasIndex(x => x.Identity).IsUnique();
        builder.HasIndex(x => x.UserId);

        //mobile
        builder.Property(x => x.MobilePhone).HasMaxLength(20).HasDefaultValue(string.Empty);
        builder.Property(x => x.MobileAreaCode).HasMaxLength(10).HasDefaultValue(string.Empty);
        builder.Property(x => x.MobileConfirmed);
        builder.Property(x => x.MobileConfirmedTimeUtc);
        
        //email
        builder.Property(x => x.Email).HasMaxLength(100).HasDefaultValue(string.Empty);
        builder.Property(x => x.EmailConfirmed);
        builder.Property(x => x.EmailConfirmedTimeUtc);
        
        base.Configure(builder);

    }
}