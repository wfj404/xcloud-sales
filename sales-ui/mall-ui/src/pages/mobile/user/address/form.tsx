import { useRequest } from 'ahooks';
import React, { useEffect, useState } from 'react';

import XCalculatedHeight from '@/components/common/calculated_height_box';
import DotsLoading from '@/components/common/loading/dots';
import LocationPicker from '@/components/common/map/location_picker';
import { wuxi_default_location } from '@/components/common/map/utils';
import XModal from '@/components/common/modal';
import XAreaSelector from '@/components/mobile/area_selector';
import { apiClient } from '@/utils/client';
import { UserAddress, UserAddressDto } from '@/utils/swagger';
import {
  errorMsg,
  handleResponse,
  isMobile,
  isStringEmpty,
} from '@/utils/utils';
import { LoadingButton } from '@mui/lab';
import { Box, Stack, TextField } from '@mui/material';

export default function ({
  open,
  onClose,
  data,
  onOk,
}: {
  open: boolean;
  onClose: () => void;
  data: UserAddress;
  onOk: () => void;
}) {
  const [formData, _formData] = useState<UserAddressDto>({
    Lat: wuxi_default_location.lat,
    Lon: wuxi_default_location.lng,
  });

  const setDefaultRequest = useRequest(
    apiClient.platform.userAddressSetDefault,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          onOk();
        });
      },
    },
  );

  const deleteAddressRequest = useRequest(
    apiClient.platform.userAddressDelete,
    {
      manual: true,
      onSuccess: (res) => {
        handleResponse(res, () => {
          onOk();
        });
      },
    },
  );

  const getLatLng = (): L.LatLngLiteral | undefined => {
    if (formData.Lat == undefined || formData.Lon == undefined) {
      return undefined;
    }

    return {
      lat: formData.Lat,
      lng: formData.Lon,
    };
  };

  const saveAddressRequest = useRequest(apiClient.platform.userAddressSave, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        onOk && onOk();
      });
    },
  });

  const saveAddress = () => {
    if (
      isStringEmpty(formData.Name) ||
      isStringEmpty(formData.Tel) ||
      isStringEmpty(formData.Province) ||
      isStringEmpty(formData.ProvinceCode) ||
      isStringEmpty(formData.City) ||
      isStringEmpty(formData.CityCode) ||
      isStringEmpty(formData.Area) ||
      isStringEmpty(formData.AreaCode) ||
      isStringEmpty(formData.AddressDetail)
    ) {
      errorMsg('请完善表单信息');
      return;
    }

    if (getLatLng() == undefined) {
      errorMsg('请在地图标注地点');
      return;
    }

    if (!isMobile(formData.Tel || '')) {
      errorMsg('手机号码格式不对');
      return;
    }

    saveAddressRequest.run({
      ...formData,
      IsDefault: true,
    });
  };

  useEffect(() => {
    _formData({
      ...data,
      Lat: data.Lat || wuxi_default_location.lat,
      Lon: data.Lon || wuxi_default_location.lng,
    });
  }, [data]);

  const renderActions = () => {
    return (
      <>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '100%',
          }}
        >
          <div className="left-action">
            {isStringEmpty(formData.Id) || (
              <>
                <Stack direction={'row'} spacing={1}>
                  {formData.IsDefault || (
                    <LoadingButton
                      loading={setDefaultRequest.loading}
                      onClick={() => {
                        setDefaultRequest.run({ Id: formData.Id });
                      }}
                    >
                      设为默认
                    </LoadingButton>
                  )}
                  <LoadingButton
                    loading={deleteAddressRequest.loading}
                    color="error"
                    onClick={() => {
                      if (confirm('确定删除该地址？')) {
                        deleteAddressRequest.run({ Id: formData.Id });
                      }
                    }}
                  >
                    删除
                  </LoadingButton>
                </Stack>
              </>
            )}
          </div>
          <div className="right-action">
            <Stack direction={'row'} spacing={1}>
              <LoadingButton
                onClick={() => {
                  onClose && onClose();
                }}
              >
                取消
              </LoadingButton>
              <LoadingButton
                variant="contained"
                color="primary"
                loading={saveAddressRequest.loading}
                onClick={() => {
                  saveAddress();
                }}
              >
                保存
              </LoadingButton>
            </Stack>
          </div>
        </div>
      </>
    );
  };

  const renderFormContent = () => {
    return (
      <>
        <Box sx={{ py: 2 }}>
          <Stack spacing={2}>
            <TextField
              label="姓名"
              fullWidth
              value={formData.Name}
              onChange={(e) => {
                _formData({ ...formData, Name: e.target.value });
              }}
              variant="outlined"
            />
            <TextField
              label="电话"
              fullWidth
              value={formData.Tel}
              onChange={(e) => {
                _formData({ ...formData, Tel: e.target.value });
              }}
              variant="outlined"
            />
            <XAreaSelector
              autoClose
              value={[
                {
                  label: formData.Province || undefined,
                  value: formData.ProvinceCode || undefined,
                },
                {
                  label: formData.City || undefined,
                  value: formData.CityCode || undefined,
                },
                {
                  label: formData.Area || undefined,
                  value: formData.AreaCode || undefined,
                },
              ]}
              onChange={(e) => {
                const province = e.at(0);
                const city = e.at(1);
                const area = e.at(2);
                _formData({
                  ...formData,
                  Province: province?.label,
                  ProvinceCode: province?.value,
                  City: city?.label,
                  CityCode: city?.value,
                  Area: area?.label,
                  AreaCode: area?.value,
                });
              }}
            />
            <TextField
              label="详细地址"
              placeholder="请精确到楼栋和门牌"
              fullWidth
              multiline
              minRows={2}
              value={formData.AddressDetail}
              onChange={(e) => {
                if (e.target.value.length > 100) {
                  errorMsg('长度过长');
                  return;
                }

                _formData({ ...formData, AddressDetail: e.target.value });
              }}
              variant="outlined"
            />
            <XCalculatedHeight
              rate={1}
              style={{
                border: '1px solid blue',
              }}
            >
              <LocationPicker
                marker_position={getLatLng()}
                marker_title={formData.Name || undefined}
                use_map_center
                onMarkerPositionChange={(e) => {
                  _formData({
                    ...formData,
                    Lat: e.lat,
                    Lon: e.lng,
                  });
                }}
              />
            </XCalculatedHeight>
          </Stack>
        </Box>
      </>
    );
  };

  return (
    <>
      <XModal
        title={'联系地址'}
        open={open}
        onClose={() => {
          onClose && onClose();
        }}
        actionRender={() => renderActions()}
      >
        {saveAddressRequest.loading && <DotsLoading />}
        {renderFormContent()}
      </XModal>
    </>
  );
}
