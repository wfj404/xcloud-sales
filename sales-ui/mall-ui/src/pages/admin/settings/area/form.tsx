import {
  Button,
  Form,
  Input,
  message,
  Modal,
  Segmented,
  Space,
  Spin,
  TreeSelect,
} from 'antd';
import { useEffect, useState } from 'react';

import XSinglePictureUploader from '@/components/upload/single_picture_form';
import { getFlatNodes, getTreeSelectOptionsV2 } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { AntDesignTreeNode, AreaDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: AreaDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [tree, _tree] = useState<AntDesignTreeNode[]>([]);
  const [expandKeys, _expandKeys] = useState<any[]>([]);

  const [model, _model] = useState<AreaDto>({});

  const queryTree = () => {
    _loading(true);

    apiClient.sys
      .areaLoadTree({})
      .then((res) => {
        handleResponse(res, () => {
          const data = res.data.Data || [];
          _tree(data);
          const keys = data.flatMap((x) => getFlatNodes(x)).map((x) => x.key);
          _expandKeys(keys);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const save = (row: AreaDto) => {
    row.Name = row.Name?.trim();

    if (isStringEmpty(row.Name)) {
      return;
    }

    _loading(true);

    apiClient.sys
      .areaSave({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...data,
    });
  }, [data]);

  useEffect(() => {
    if (show) {
      queryTree();
    } else {
      _model({});
    }
  }, [show]);

  return (
    <>
      <Modal
        title={'区域'}
        confirmLoading={loading}
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <Button
              type={'primary'}
              loading={loading}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label={'图片'}>
              <XSinglePictureUploader
                loadingSave={loading}
                data={model.StorageMeta || {}}
                ok={(res) => {
                  _model({
                    ...model,
                    MetaId: res.Id,
                    StorageMeta: res,
                  });
                }}
                remove={() => {
                  if (confirm('确定删除图片吗？')) {
                    _model({
                      ...model,
                      MetaId: undefined,
                      StorageMeta: undefined,
                    });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="父级">
              <TreeSelect
                //disabled={!isStringEmpty(model.Id)}
                placeholder="无父级分类则视为顶级分类"
                treeData={tree.map((x) => getTreeSelectOptionsV2(x))}
                treeExpandedKeys={expandKeys}
                onTreeExpand={(e) => _expandKeys(e)}
                allowClear
                value={model.ParentId || undefined}
                onChange={(e) => {
                  if (!isStringEmpty(model.Id) && e == model.Id) {
                    message.error('无法指定该节点');
                    return;
                  }

                  _model({
                    ...model,
                    ParentId: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="类型">
              <Segmented
                options={[
                  { label: '大洲', value: 'continent' },
                  { label: '国家', value: 'country' },
                  { label: '地区', value: 'area' },
                  { label: '其他', value: 'other' },
                ]}
                value={model.AreaType}
                onChange={(e) => {
                  _model({ ...model, AreaType: e });
                }}
              />
            </Form.Item>
            <Form.Item label="名称" required>
              <Input
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                required
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
              {isStringEmpty(model.Name) && (
                <p style={{ color: 'red' }}>必填</p>
              )}
            </Form.Item>
            <Form.Item label="描述">
              <Input.TextArea
                rows={2}
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                required
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
