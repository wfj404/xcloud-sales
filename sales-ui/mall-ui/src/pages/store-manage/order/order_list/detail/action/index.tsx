import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { Button, Divider, message, Space } from 'antd';
import { useState } from 'react';
import XCancel from './cancel';
import XPayment from './payment';
import XStatus from './status';
import { handleResponse } from '@/utils/utils';

export default ({ model, ok }: { model: OrderDto; ok: () => void }) => {
  const [loadingRefresh, _loadingRefresh] = useState(false);

  const triggerReload = () => ok && ok();

  return (
    <>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginBottom: 40,
          marginTop: 30,
        }}
      >
        <Space direction={'horizontal'} split={<Divider type={'vertical'} />}>
          <XCancel
            model={model}
            ok={() => {
              triggerReload();
            }}
          />
          <XPayment
            model={model}
            ok={() => {
              triggerReload();
            }}
          />
          <XStatus
            model={model}
            ok={() => {
              triggerReload();
            }}
          />
          <Button.Group>
            <Button onClick={() => triggerReload()}
                    type={'link'}>刷新页面</Button>
            <Button
              loading={loadingRefresh}
              type={'link'}
              danger
              onClick={() => {
                _loadingRefresh(true);
                apiClient.mallManager
                  .orderRefreshStatus({ Id: model.Id })
                  .then((res) => {
                    handleResponse(res, () => {
                      message.success('刷新成功');
                      triggerReload();
                    });
                  })
                  .finally(() => {
                    _loadingRefresh(false);
                  });
              }}
            >
              刷新状态
            </Button>
          </Button.Group>
        </Space>
      </div>
    </>
  );
};
