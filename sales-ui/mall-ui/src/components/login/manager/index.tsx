import React, { ReactNode } from 'react';

import XValidateLogin from './validate_login';
import XLoadPermission from './load_permission';

export default function ({ children }: { children: ReactNode }) {
  return (
    <>
      <XValidateLogin>
        <XLoadPermission>{children}</XLoadPermission>
      </XValidateLogin>
    </>
  );
}
