import XGoodsLabels from '@/components/goods/goods_labels';
import XSkus from '@/components/goods/sku/sku_list';
import { GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Typography } from '@mui/material';

import XGoodsPublishAlert from '../../goods_publish_alert';
import XPicture from '../../picture';
import Wrapper from '../wrapper';

export default function ({
  model,
  count,
  lazy,
}: {
  model: GoodsDto;
  count?: number;
  lazy?: boolean;
}) {
  if (isStringEmpty(model?.Id)) {
    return null;
  }

  if (!model.Published) {
    return <XGoodsPublishAlert model={model} />;
  }

  return (
    <>
      <Wrapper>
        <Box sx={{}}>
          <XPicture model={model} lazy={lazy} />
          <Box
            sx={{
              padding: {
                xs: 1,
                sm: 1,
                md: 2,
              },
            }}
          >
            <Typography variant="subtitle2" component="div" noWrap>
              {model.Name || '--'}
            </Typography>

            {isStringEmpty(model.AdminComment) || (
              <Typography
                variant="overline"
                color="primary"
                display="block"
                component={'div'}
              >
                {`${model.AdminComment}`}
              </Typography>
            )}
            <XSkus skus={model.Skus || []} count={count} />
            <XGoodsLabels model={model} />
          </Box>
        </Box>
      </Wrapper>
    </>
  );
}
