namespace XCloud.Sales.Application.Domain.Mapping;

public interface IStoreMappingSupported : IMappingSupported
{
    bool LimitedToStore { get; set; }
}

public class CommonEntityMapping : SalesBaseEntity, IMappingTable
{
    public string EntityId { get; set; }

    public string EntityName { get; set; }

    public string TargetId { get; set; }

    public string TargetName { get; set; }
}