using JetBrains.Annotations;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Extension;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.AfterSale;

public interface IAfterSaleProcessingService : ISalesAppService
{
    Task DangerouslyUpdateStatusAsync(DangerouslyUpdateAfterSalesStatusDto dto);

    Task ApproveAsync(ApproveAfterSaleDto dto);

    Task RejectAsync(RejectAfterSaleInput dto);

    Task CompleteAsync(CompleteAfterSaleDto dto);

    Task CancelAsync(CancelAfterSaleDto dto);

    Task<ApiResponse<AfterSalesDto>> CreateAsync(AfterSalesDto dto);
}

public class AfterSaleProcessingService : SalesAppService, IAfterSaleProcessingService
{
    private readonly ISalesRepository<AfterSales> _afterSalesRepository;
    private readonly ISettingService _settingService;

    public AfterSaleProcessingService(ISettingService settingService,
        ISalesRepository<AfterSales> afterSalesRepository)
    {
        _settingService = settingService;
        _afterSalesRepository = afterSalesRepository;
    }

    private void CheckCreateAfterSalesInput(AfterSalesDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.OrderId))
            throw new ArgumentException("order is required");

        if (ValidateHelper.IsEmptyCollection(dto.Items))
            throw new ArgumentException("items is required");

        if (string.IsNullOrWhiteSpace(dto.ReasonForReturn) || string.IsNullOrWhiteSpace(dto.RequestedAction))
            throw new UserFriendlyException("reason and request is required");
    }

    [NotNull]
    [ItemNotNull]
    private async Task<Order> GetRequiredOrderForAfterSalesAsync(DbContext db, string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var order = await db.Set<Order>().FirstOrDefaultAsync(x => x.Id == orderId);

        if (order == null)
            throw new EntityNotFoundException("order not found");

        if (order.IsAfterSales())
            throw new UserFriendlyException("order is already in aftersale");

        if (order.ShippingStatusId != SalesConstants.ShippingStatus.Shipped)
            throw new UserFriendlyException("you can't request aftersale now");

        return order;
    }

    private async Task CheckBeforeAfterSalesAsync(DbContext db, AfterSalesDto dto)
    {
        if (await db.Set<AfterSales>().AnyAsync(x => x.OrderId == dto.OrderId))
            throw new UserFriendlyException("a after sale is started before,pls don't resubmit");

        if (await db.Set<AfterSalesItem>().AnyAsync(x => x.OrderId == dto.OrderId))
            throw new UserFriendlyException("a after sale is started before,pls don't resubmit");
    }

    public async Task<ApiResponse<AfterSalesDto>> CreateAsync(AfterSalesDto dto)
    {
        CheckCreateAfterSalesInput(dto);

        var mallSettings = await _settingService.GetCachedMallSettingsAsync();
        if (mallSettings.AfterSalesSettings.AfterSaleDisabled)
            throw new UserFriendlyException("after sale is disabled by admin");

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(AfterSaleService)}.{nameof(CreateAsync)}.{dto.OrderId}",
            expiryTime: TimeSpan.FromSeconds(5));

        var db = await _afterSalesRepository.GetDbContextAsync();

        //check
        await CheckBeforeAfterSalesAsync(db, dto);

        //query relative order
        var order = await GetRequiredOrderForAfterSalesAsync(db, dto.OrderId);
        var orderItems = await db.Set<OrderItem>().Where(x => x.OrderId == order.Id).ToArrayAsync();

        //mapping
        var entity = ObjectMapper.Map<AfterSalesDto, AfterSales>(dto);
        var items = dto.Items.MapArrayTo<AfterSalesItemDto, AfterSalesItem>(ObjectMapper);

        //set entity fields
        entity.Id = GuidGenerator.CreateGuidString();
        entity.AfterSalesStatusId = SalesConstants.AfterSalesStatus.Processing;
        entity.CreationTime = Clock.Now;

        //set items fields
        foreach (var m in items)
        {
            var orderItem = orderItems.FirstOrDefault(x => x.Id == m.OrderItemId);
            if (orderItem == null)
                throw new AbpException("order item is not exist");

            if (orderItem.Quantity < m.Quantity)
                throw new AbpException("goods quantity error");

            m.Id = GuidGenerator.CreateGuidString();
            m.AfterSalesId = entity.Id;
            m.OrderId = order.Id;
        }

        //modify entity
        order.OrderStatusId = SalesConstants.OrderStatus.AfterSales;

        db.Set<AfterSales>().Add(entity);
        db.Set<AfterSalesItem>().AddRange(items);

        //save changes
        await db.SaveChangesAsync();

        var response = ObjectMapper.Map<AfterSales, AfterSalesDto>(entity);

        return new ApiResponse<AfterSalesDto>(response);
    }

    public async Task DangerouslyUpdateStatusAsync(DangerouslyUpdateAfterSalesStatusDto dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(ApproveAsync));

        var afterSales = await this._afterSalesRepository.GetRequiredByIdAsync(dto.Id);

        if (!string.IsNullOrWhiteSpace(dto.Status))
        {
            afterSales.AfterSalesStatusId = dto.Status;
        }

        afterSales.LastModificationTime = Clock.Now;

        await this._afterSalesRepository.UpdateAsync(afterSales);
    }

    public async Task ApproveAsync(ApproveAfterSaleDto dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(ApproveAsync));

        var afterSales = await this._afterSalesRepository.GetRequiredByIdAsync(dto.Id);

        if (afterSales.AfterSalesStatusId != SalesConstants.AfterSalesStatus.Processing)
            throw new UserFriendlyException("after sales is not in processing");

        afterSales.AfterSalesStatusId = SalesConstants.AfterSalesStatus.Approved;
        afterSales.LastModificationTime = Clock.Now;

        await this._afterSalesRepository.UpdateAsync(afterSales);
    }

    public async Task RejectAsync(RejectAfterSaleInput dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(RejectAsync));

        var afterSales = await this._afterSalesRepository.GetRequiredByIdAsync(dto.Id);

        if (afterSales.AfterSalesStatusId != SalesConstants.AfterSalesStatus.Processing)
            throw new UserFriendlyException("after sales is not in processing");

        afterSales.AfterSalesStatusId = SalesConstants.AfterSalesStatus.Rejected;
        afterSales.LastModificationTime = Clock.Now;

        await this._afterSalesRepository.UpdateAsync(afterSales);
    }

    public async Task CompleteAsync(CompleteAfterSaleDto dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(CompleteAsync));

        var afterSales = await this._afterSalesRepository.GetRequiredByIdAsync(dto.Id);

        if (afterSales.AfterSalesStatusId != SalesConstants.AfterSalesStatus.Approved)
            throw new UserFriendlyException("after sales is not approved");

        afterSales.AfterSalesStatusId = SalesConstants.AfterSalesStatus.Complete;
        afterSales.LastModificationTime = Clock.Now;

        await this._afterSalesRepository.UpdateAsync(afterSales);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
    }

    public async Task CancelAsync(CancelAfterSaleDto dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(CancelAsync));

        var afterSales = await _afterSalesRepository.GetRequiredByIdAsync(dto.Id);

        if (afterSales.AfterSalesStatusId == SalesConstants.AfterSalesStatus.Cancelled)
            throw new UserFriendlyException("after sales is already cancelled");

        afterSales.AfterSalesStatusId = SalesConstants.AfterSalesStatus.Cancelled;
        afterSales.LastModificationTime = Clock.Now;

        await this._afterSalesRepository.UpdateAsync(afterSales);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
    }
}