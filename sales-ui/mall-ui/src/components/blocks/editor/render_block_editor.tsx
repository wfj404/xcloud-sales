import XCard from '../components/card/editor';
import XCategoryGoods from '../components/category_goods/editor';
import XCoupon from '../components/coupon/editor';
import XGallery from '../components/gallery/editor';
import { GalleryBlockProps } from '../components/gallery/utils';
import XHello from '../components/hello/editor';
import XHotGoods from '../components/hot_goods/editor';
import XMarkdown from '../components/markdown/editor';
import XSearch from '../components/search/editor';
import XSlider from '../components/slider/editor';
import XTitle from '../components/title/editor';
import XVideo from '../components/video/editor';
import { BlockItem, BlockItemProps } from '../utils';

export default ({
  block,
  onBlockUpdated,
}: {
  block: BlockItem<BlockItemProps>;
  onBlockUpdated: (d: BlockItem<BlockItemProps>) => void;
}) => {
  if (block == undefined) {
    return <div>block is undefined</div>;
  }

  const renderForm = () => {
    if (!block) {
      return <p>empty block</p>;
    }

    if (block.type == 'markdown') {
      return (
        <XMarkdown
          data={block.body || {}}
          onChange={(x) =>
            onBlockUpdated({
              ...block,
              body: x,
            })
          }
        />
      );
    }

    if (block.type == 'slider') {
      return (
        <XSlider
          data={block.body || {}}
          onChange={(x) =>
            onBlockUpdated({
              ...block,
              body: x,
            })
          }
        />
      );
    }

    if (block.type == 'category-goods') {
      return (
        <XCategoryGoods
          data={block.body || {}}
          onChange={(x) =>
            onBlockUpdated({
              ...block,
              body: x,
            })
          }
        />
      );
    }

    if (block.type == 'gallery') {
      return (
        <XGallery
          data={(block.body as GalleryBlockProps) || {}}
          onChange={(x) =>
            onBlockUpdated({
              ...block,
              body: x,
            })
          }
        />
      );
    }

    if (block.type == 'hello') {
      return (
        <XHello
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    if (block.type == 'title') {
      return (
        <XTitle
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    if (block.type == 'search') {
      return (
        <XSearch
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    if (block.type == 'video') {
      return (
        <XVideo
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    if (block.type == 'hot-goods') {
      return (
        <XHotGoods
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    if (block.type == 'card') {
      return (
        <XCard
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    if (block.type == 'coupon') {
      return (
        <XCoupon
          data={block.body || {}}
          onChange={(x) => {
            onBlockUpdated({
              ...block,
              body: x,
            });
          }}
        />
      );
    }

    return (
      <div>
        <p>error block item</p>
        <div>{JSON.stringify(block)}</div>
      </div>
    );
  };

  return <>{renderForm()}</>;
};
