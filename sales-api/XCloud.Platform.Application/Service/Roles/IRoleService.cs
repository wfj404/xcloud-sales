using System.Threading.Tasks;
using XCloud.Application.Extension;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Domain.Security;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Permissions;

namespace XCloud.Platform.Application.Service.Roles;

public interface IRoleService : IPlatformCrudService<Role, RoleDto, QueryRolePaging>
{
    Task SetRolePermissionsAsync(string roleId, string[] permissionKeys);

    Task<RoleDto[]> AttachPermissionAsync(RoleDto[] data);
}

public class RoleService : PlatformCrudService<Role, RoleDto, QueryRolePaging>, IRoleService
{
    public RoleService(IPlatformRepository<Role> repository) : base(repository)
    {
        //
    }

    public async Task<RoleDto[]> AttachPermissionAsync(RoleDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await Repository.GetDbContextAsync();

        var assignQuery = db.Set<RolePermission>().AsNoTracking();

        var query = from assign in assignQuery
            join role in db.Set<Role>().AsNoTracking()
                on assign.RoleId equals role.Id
            select new { assign, role };

        var ids = data.Ids();

        var rolePermissions = await query.Where(x => ids.Contains(x.role.Id)).ToArrayAsync();

        foreach (var m in data)
        {
            m.PermissionKeys = rolePermissions
                .Where(x => x.role.Id == m.Id)
                .Select(x => x.assign.PermissionKey).ToArray();
        }

        return data;
    }

    public async Task SetRolePermissionsAsync(string roleId, string[] permissionKeys)
    {
        if (string.IsNullOrWhiteSpace(roleId))
            throw new ArgumentNullException(nameof(roleId));

        if (permissionKeys == null)
            throw new ArgumentNullException(nameof(permissionKeys));

        var assigns = permissionKeys.Select(x => new RolePermission
        {
            PermissionKey = x,
            RoleId = roleId,
        }).ToArray();

        var db = await Repository.GetDbContextAsync();

        var set = db.Set<RolePermission>();

        var originData = await set.Where(x => x.RoleId == roleId).ToArrayAsync();

        static string FingerPrint(RolePermission entity) =>
            $"{entity.PermissionKey}";

        var toDelete = originData.NotInBy(assigns, FingerPrint).ToArray();
        var toInsert = assigns.NotInBy(originData, FingerPrint).ToArray();

        if (toDelete.Any())
        {
            set.RemoveRange(toDelete);
        }

        if (toInsert.Any())
        {
            var now = Clock.Now;
            foreach (var m in toInsert)
            {
                m.Id = GuidGenerator.CreateGuidString();
                m.CreationTime = now;
                set.Add(m);
            }
        }

        await db.TrySaveChangesAsync();
    }

    private async Task<bool> CheckNameIsExistAsync(DbContext db,
        string name, string exceptIdOrEmpty = null)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        var query = db.Set<Role>().AsNoTracking();
        if (!string.IsNullOrWhiteSpace(exceptIdOrEmpty))
            query = query.Where(x => x.Id != exceptIdOrEmpty);

        var exist = await query.AnyAsync(x => x.Name == name);

        return exist;
    }

    protected override async Task SetFieldsBeforeInsertAsync(Role entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
    }

    public override async Task<Role> InsertAsync(RoleDto dto)
    {
        var db = await Repository.GetDbContextAsync();

        if (await CheckNameIsExistAsync(db, dto.Name))
            throw new UserFriendlyException("role name exist");

        return await base.InsertAsync(dto);
    }

    public override async Task<Role> UpdateAsync(RoleDto dto)
    {
        var db = await Repository.GetDbContextAsync();

        if (await CheckNameIsExistAsync(db, dto.Name, exceptIdOrEmpty: dto.Id))
            throw new UserFriendlyException("role name exist");

        return await base.UpdateAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Role entity, RoleDto dto)
    {
        await Task.CompletedTask;
        entity.Name = dto.Name;
        entity.Description = dto.Description;
    }
}