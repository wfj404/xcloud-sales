﻿using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Search;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Catalog;

public interface ISkuService : ISalesStringCrudAppService<Sku, SkuDto, QuerySkuPaging>
{
    Task<SkuDto[]> AttachGoodsAsync(SkuDto[] data);

    Task<SkuDto[]> ListForSelectionAsync(QuerySkuForSelectionInput dto);

    Task<SkuDto[]> ListByGoodsIdAsync(string goodsId);

    [ItemCanBeNull]
    Task<SkuDto> GetBySkuAsync(string skuCode);

    Task UpdatePriceAsync(string skuId, decimal price);

    Task<SkuDto[]> QueryByPageOrderByCreationTimeAsync(int page, int pageSize);
}

public class SkuService : SalesStringCrudAppService<Sku, SkuDto, QuerySkuPaging>, ISkuService
{
    private readonly IGoodsService _goodsService;
    private readonly GoodsUtils _goodsUtils;

    public SkuService(ISalesRepository<Sku> skuRepository,
        IGoodsService goodsService,
        GoodsUtils goodsUtils) : base(skuRepository)
    {
        _goodsService = goodsService;
        _goodsUtils = goodsUtils;
    }

    public override Task<PagedResponse<SkuDto>> QueryPagingAsync(QuerySkuPaging dto)
    {
        throw new NotSupportedException();
    }

    public async Task UpdatePriceAsync(string skuId, decimal price)
    {
        if (price < decimal.Zero)
            throw new ArgumentNullException(nameof(price));

        var entity = await this.Repository.GetRequiredByIdAsync(skuId);

        if (entity.Price == price)
            return;

        entity.Price = price;
        entity.LastModificationTime = this.Clock.Now;

        await this.Repository.UpdateAsync(entity);
    }

    public async Task<SkuDto[]> QueryByPageOrderByCreationTimeAsync(int page, int pageSize)
    {
        var db = await Repository.GetDbContextAsync();

        var query = db.Set<Sku>().AsNoTracking();

        query = query.OrderBy(x => x.CreationTime).ThenBy(x => x.Id);

        var pagingRequest = new PagedRequest()
        {
            Page = page,
            PageSize = pageSize,
            SkipCalculateTotalCount = true
        };

        var data = await query
            .PageBy(pagingRequest.ToAbpPagedRequest())
            .ToArrayAsync();

        return data.MapArrayTo<Sku, SkuDto>(ObjectMapper);
    }

    public override Task DeleteByIdAsync(string id)
    {
        return this.Repository.SoftDeleteAsync(id, this.Clock.Now);
    }

    public async Task<SkuDto[]> ListForSelectionAsync(QuerySkuForSelectionInput dto)
    {
        var db = await Repository.GetDbContextAsync();

        var query = from sku in db.Set<Sku>().AsNoTracking()
            join goods in db.Set<Goods>().AsNoTracking()
                on sku.GoodsId equals goods.Id
            select new { sku, goods };

        if (ValidateHelper.IsNotEmptyCollection(dto.ExcludedSkuIds))
            query = query.Where(x => !dto.ExcludedSkuIds.Contains(x.sku.Id));

        if (!string.IsNullOrWhiteSpace(dto.Keywords))
        {
            query = query.Where(x =>
                x.goods.Name.Contains(dto.Keywords) ||
                x.goods.Description.Contains(dto.Keywords) ||
                x.sku.Name.Contains(dto.Keywords));
        }

        query = query
            .OrderByDescending(x => x.goods.CreationTime)
            .ThenByDescending(x => x.goods.Id);

        var data = await query.Take(dto.Take).ToArrayAsync();

        return data.Select(x => x.sku).MapArrayTo<Sku, SkuDto>(ObjectMapper);
    }

    public async Task<SkuDto[]> AttachGoodsAsync(SkuDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await Repository.GetDbContextAsync();

        var goodsIds = data.Select(x => x.GoodsId).Distinct().ToArray();
        var goodsList = await db.Set<Goods>().AsNoTracking().IgnoreQueryFilters().WhereIdIn(goodsIds).ToArrayAsync();

        foreach (var m in data)
        {
            var g = goodsList.FirstOrDefault(x => x.Id == m.GoodsId);
            if (g == null)
                continue;
            m.Goods = ObjectMapper.Map<Goods, GoodsDto>(g);
        }

        return data;
    }

    public virtual async Task<SkuDto[]> ListByGoodsIdAsync(string goodsId)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            return Array.Empty<SkuDto>();

        var db = await Repository.GetDbContextAsync();

        var query = db.Set<Sku>().AsNoTracking().Where(x => x.GoodsId == goodsId);

        var datalist = await query.OrderByDescending(x => x.CreationTime).TakeUpTo5000().ToArrayAsync();

        return datalist.MapArrayTo<Sku, SkuDto>(this.ObjectMapper);
    }

    public async Task<SkuDto> GetBySkuAsync(string skuCode)
    {
        if (string.IsNullOrWhiteSpace(skuCode))
            throw new ArgumentNullException(nameof(skuCode));

        skuCode = this._goodsUtils.NormalizeSku(skuCode);

        var sku = await Repository.FirstOrDefaultAsync(x => x.SkuCode == skuCode);

        if (sku == null)
            return null;

        return sku.MapTo<Sku, SkuDto>(ObjectMapper);
    }

    protected override async Task SetFieldsBeforeInsertAsync(Sku entity)
    {
        entity.Id = GuidGenerator.CreateGuidString();

        entity.SkuCode = this._goodsUtils.NormalizeSku(entity.SkuCode);
        entity.IsDeleted = false;
        entity.DeletionTime = default;
        entity.CreationTime = Clock.Now;

        if (!string.IsNullOrWhiteSpace(entity.SkuCode))
        {
            if (await this.Repository.AnyAsync(x => x.SkuCode == entity.SkuCode))
                throw new UserFriendlyException("sku code is already taken");
        }
    }

    public override async Task<Sku> InsertAsync(SkuDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
        {
            throw new UserFriendlyException("sku name is empty");
        }

        if (string.IsNullOrWhiteSpace(dto.GoodsId))
            throw new ArgumentNullException(nameof(dto.GoodsId));

        var goods = await _goodsService.GetByIdAsync(dto.GoodsId);

        if (goods == null)
            throw new EntityNotFoundException(nameof(goods));

        return await base.InsertAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Sku entity, SkuDto dto)
    {
        entity.SkuCode = this._goodsUtils.NormalizeSku(dto.SkuCode);

        entity.Name = this._goodsUtils.NormalizeName(dto.Name);
        entity.Description = dto.Description;
        entity.Name = dto.Name;
        entity.Description = dto.Description;

        entity.Weight = dto.Weight;
        entity.Color = dto.Color;

        entity.Price = dto.Price;
        entity.MaxAmountInOnePurchase = dto.MaxAmountInOnePurchase;
        entity.MinAmountInOnePurchase = dto.MinAmountInOnePurchase;

        entity.IsActive = dto.IsActive;

        entity.LastModificationTime = Clock.Now;

        if (!string.IsNullOrWhiteSpace(entity.SkuCode))
        {
            if (await this.Repository.AnyAsync(x => x.Id != entity.Id && x.SkuCode == entity.SkuCode))
                throw new UserFriendlyException("sku code is already taken");
        }
    }

    public override Task<Sku> UpdateAsync(SkuDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
        {
            throw new UserFriendlyException("sku name is empty");
        }

        return base.UpdateAsync(dto);
    }
}