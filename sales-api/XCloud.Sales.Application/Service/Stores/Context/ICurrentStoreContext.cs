﻿using JetBrains.Annotations;
using XCloud.Sales.Application.Service.Stores.Context.Contributors;

namespace XCloud.Sales.Application.Service.Stores.Context;

public interface ICurrentStoreContext
{
    [NotNull]
    [ItemCanBeNull]
    Task<string> GetStoreIdOrEmptyAsync();
}

[ExposeServices(typeof(ICurrentStoreContext))]
public class CurrentStoreContext : SalesAppService, ICurrentStoreContext
{
    private readonly UserSelectedStoreSelectorContributor _userSelectedStoreSelectorContributor;
    private readonly GetOrCreateDefaultStoreSelectorContributor _getOrCreateDefaultStoreSelectorContributor;

    public CurrentStoreContext(
        UserSelectedStoreSelectorContributor userSelectedStoreSelectorContributor,
        GetOrCreateDefaultStoreSelectorContributor getOrCreateDefaultStoreSelectorContributor)
    {
        _userSelectedStoreSelectorContributor = userSelectedStoreSelectorContributor;
        _getOrCreateDefaultStoreSelectorContributor = getOrCreateDefaultStoreSelectorContributor;
    }

    public async Task<string> GetStoreIdOrEmptyAsync()
    {
        var providerList = new ICurrentStoreSelectorContributor[]
        {
            this._userSelectedStoreSelectorContributor,
            this._getOrCreateDefaultStoreSelectorContributor
        };

        var storeId = await providerList.GetComposedStoreIdOrEmptyAsync();

        return storeId;
    }
}