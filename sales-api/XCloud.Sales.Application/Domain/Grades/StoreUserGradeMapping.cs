﻿namespace XCloud.Sales.Application.Domain.Grades;

public class StoreUserGradeMapping : SalesBaseEntity<string>, IHasCreationTime
{
    public int UserId { get; set; }
    public string GradeId { get; set; }

    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }

    public DateTime CreationTime { get; set; }
}