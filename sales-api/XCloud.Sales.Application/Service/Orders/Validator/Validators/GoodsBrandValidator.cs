using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Orders.Validator.Validators;

public class GoodsBrandValidatorParameter : IRuleParameter
{
    public BrandDto[] SupportedBrands { get; set; }
}

[ExposeServices(typeof(IOrderValidator))]
public class GoodsBrandValidator : OrderValidatorBase<GoodsBrandValidatorParameter>, IOrderValidator,
    ITransientDependency
{
    public int Order => default;

    public string ValidatorType => OrderConditionValidatorTypes.GoodsBrand;

    public Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule)
    {
        return Task.FromResult(ValidatorType == rule.ValidatorType);
    }

    public async Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule)
    {
        await Task.CompletedTask;

        if (!string.IsNullOrWhiteSpace(rule.Description))
            return rule.Description;

        var param = ParseRuleParameterOrDefault(rule.RuleJson);

        if (param != null && ValidateHelper.IsNotEmptyCollection(param.SupportedBrands))
        {
            var names = param.SupportedBrands.Select(x => x.Name).Distinct().ToArray();
            return $"assign to {string.Join(',', names)}";
        }

        return string.Empty;
    }

    public async Task<OrderConditionValidationResponse> IsConditionMatchedAsync(OrderConditionCheckDto input,
        OrderValidatorRule rule)
    {
        await Task.CompletedTask;

        var param = ParseRequiredRuleParameter(rule.RuleJson);

        if (param == null || ValidateHelper.IsEmptyCollection(param.SupportedBrands))
            return new OrderConditionValidationResponse();

        var brandsInOrder = input.Order?.Items?.Select(x => x.Sku?.Goods?.BrandId).WhereNotEmpty().Distinct().ToArray();

        if (brandsInOrder == null || ValidateHelper.IsEmptyCollection(brandsInOrder))
            return new OrderConditionValidationResponse().SetError("brands in order is not matched");

        var supportedBrandIds = param.SupportedBrands.Ids();

        if (!brandsInOrder.All(x => supportedBrandIds.Contains(x)))
        {
            return new OrderConditionValidationResponse().SetError("goods brand not supported");
        }

        return new OrderConditionValidationResponse();
    }
}