import { useInfiniteScroll } from 'ahooks';
import { Avatar } from 'antd';
import { List } from 'antd-mobile';
import { groupBy, sortBy } from 'lodash-es';
import { useEffect, useState } from 'react';

import XEmpty from '@/components/common/empty';
import ScrollLoading from '@/components/common/scroll/loading';
import XHeader from '@/components/mobile/simpleHeader';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  dateFormat,
  formatRelativeTimeFromNow,
  parseAsDayjs,
  timezoneOffset,
} from '@/utils/dayjs';
import { NotificationDto, QueryNotificationPagingInput } from '@/utils/swagger';
import { handleResponse, isArrayEmpty } from '@/utils/utils';
import { MessageOutlined } from '@ant-design/icons';
import { LoadingButton } from '@mui/lab';
import { Box, Container } from '@mui/material';

interface NotificationGroup {
  key: string;
  items: NotificationDto[];
}

const getNotificationGroups = (
  items: NotificationDto[],
): NotificationGroup[] => {
  const groupedData = groupBy(
    items,
    (x) =>
      parseAsDayjs(x.CreationTime)
        ?.add(timezoneOffset, 'hour')
        ?.format(dateFormat) || '--',
  );

  const keys = sortBy(
    Object.keys(groupedData),
    (x) => parseAsDayjs(x)?.valueOf() || 0,
  );

  return keys.map<NotificationGroup>((x) => ({
    key: x,
    items: groupedData[x] || [],
  }));
};

export default function () {
  const [query, _query] = useState<QueryNotificationPagingInput>({
    Page: 1,
  });
  const [clearLoading, _clearLoading] = useState(false);
  const [loadingId, _loadingId] = useState<string | undefined | null>(
    undefined,
  );

  const { data, loading, loadingMore, noMore, mutate } = useInfiniteScroll<{
    list: NotificationDto[];
    res?: PagedResponse<NotificationDto>;
  }>(
    async (current) => {
      const res = await apiClient.mall.notificationPaging({
        ...query,
        Page: (current?.res?.PageIndex || 0) + 1,
      });

      return {
        list: res.data.Items || [],
        res: res.data,
      };
    },
    {
      target: window.document,
      isNoMore: (x) => x?.res?.IsEmpty || false,
      reloadDeps: [query],
    },
  );

  const markAsRead = (model: NotificationDto) => {
    if (model.Read) {
      return;
    }
    _loadingId(model.Id);
    apiClient.mall
      .notificationUpdateStatus({ Id: model.Id, Read: true })
      .then((res) => {
        handleResponse(res, () => {
          let d: NotificationDto[] = [
            ...items.filter((x) => x.Id != model.Id),
            {
              ...model,
              Read: true,
            },
          ];
          mutate({
            ...data,
            list: d,
          });
        });
      })
      .finally(() => {
        _loadingId(undefined);
      });
  };

  useEffect(() => {
    mutate({
      list: [],
    });
  }, [query]);

  const items = data?.list || [];

  const groups = getNotificationGroups(items);

  if (isArrayEmpty(groups)) {
    return <XEmpty />;
  }

  return (
    <>
      <XHeader>
        <Container disableGutters maxWidth="sm">
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
              mb: 1,
            }}
          >
            <LoadingButton
              loading={clearLoading}
              variant="text"
              onClick={() => {
                if (!confirm('清空所有消息？')) {
                  return;
                }
                _clearLoading(true);
                apiClient.mall
                  .notificationDeleteAll({})
                  .then((res) => {
                    handleResponse(res, () => {
                      let q: QueryNotificationPagingInput = {
                        ...query,
                        Page: 1,
                      };
                      _query(q);
                    });
                  })
                  .finally(() => {
                    _clearLoading(false);
                  });
              }}
              sx={{
                color: 'black',
              }}
            >
              清空
            </LoadingButton>
          </Box>
          {groups.map((g, groupIndex) => {
            return (
              <List
                header={g.key}
                key={groupIndex}
                style={{ marginBottom: 10 }}
              >
                {g.items.map((x, i) => {
                  return (
                    <List.Item
                      key={i}
                      prefix={
                        <Avatar
                          icon={<MessageOutlined />}
                          style={{
                            backgroundColor: 'green',
                          }}
                        />
                      }
                      description={x.Content}
                      extra={formatRelativeTimeFromNow(x.CreationTime)}
                      disabled={loadingId == x.Id}
                      onClick={() => {
                        markAsRead(x);
                      }}
                    >
                      {x.Read && <span>{x.Title}</span>}
                      {x.Read || <b>{x.Title}</b>}
                    </List.Item>
                  );
                })}
              </List>
            );
          })}

          <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
        </Container>
      </XHeader>
    </>
  );
}
