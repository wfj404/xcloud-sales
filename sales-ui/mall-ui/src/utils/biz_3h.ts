import { Status } from './biz';

export type McsInvoicingStatus = 'pending' | 'success' | 'failed';

export interface McsInvoicingStatusOptionType {
  label: string;
  value: McsInvoicingStatus;
}

export const McsInvoicingStatusOptions: McsInvoicingStatusOptionType[] = [
  {
    label: '开票中',
    value: 'pending',
  },
  {
    label: '开票成功',
    value: 'success',
  },
  {
    label: '开票失败',
    value: 'failed',
  },
];

export const McsBizTypeOptions: Status<any>[] = [
  {
    name: 'mcs业务单',
    id: '3',
  },
];
