﻿using JetBrains.Annotations;

namespace XCloud.Sales.Application.Service.Shipping.Delivery.ShortDistance;

public static class ShortDistanceExtension
{
    public static decimal CalculatePrice([NotNull] this TieredPrice price, int amount)
    {
        if (price == null)
            throw new ArgumentNullException(nameof(price));

        if (amount <= 0)
            return decimal.Zero;

        var finalPrice = decimal.Zero;

        finalPrice += price.StartingPrice;

        var addedAmount = amount - price.StartingAmount;

        if (addedAmount > 0)
        {
            var count = addedAmount / price.AddedUnitAmount;

            if (addedAmount % price.AddedUnitAmount > 0)
                ++count;

            finalPrice += count * price.AddedUnitPrice;
        }

        return finalPrice;
    }
}