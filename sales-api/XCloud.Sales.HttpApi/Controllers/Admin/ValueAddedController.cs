using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/value-added")]
public class ValueAddedController : ShopBaseController
{
    private readonly IValueAddedItemService _valueAddedItemService;
    private readonly IValueAddedItemGroupService _valueAddedItemGroupService;

    public ValueAddedController(IValueAddedItemService valueAddedItemService,
        IValueAddedItemGroupService valueAddedItemGroupService)
    {
        _valueAddedItemService = valueAddedItemService;
        _valueAddedItemGroupService = valueAddedItemGroupService;
    }

    [HttpPost("query-group-with-items-by-goods-id")]
    public async Task<ApiResponse<ValueAddedItemGroupDto[]>> QueryGroupWithItemsByGoodsIdAsync(
        [FromBody] IdDto<string> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var groups = await _valueAddedItemGroupService.QueryByGoodsIdAsync(dto.Id);

        await _valueAddedItemGroupService.AttachItemsAsync(groups);

        return new ApiResponse<ValueAddedItemGroupDto[]>(groups);
    }

    [HttpPost("save-group")]
    public async Task<ApiResponse<object>> SaveGroupAsync([FromBody] ValueAddedItemGroupDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _valueAddedItemGroupService.InsertAsync(dto);
        }
        else
        {
            await _valueAddedItemGroupService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete-group")]
    public async Task<ApiResponse<object>> DeleteGroupAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _valueAddedItemGroupService.DeleteAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("save-group-item")]
    public async Task<ApiResponse<object>> SaveGroupItemAsync([FromBody] ValueAddedItemDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        if (dto.Id <= 0)
            await _valueAddedItemService.InsertAsync(dto);
        else
            await _valueAddedItemService.UpdateAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("delete-group-item")]
    public async Task<ApiResponse<object>> DeleteGroupItemAsync([FromBody] IdDto<int> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _valueAddedItemService.DeleteAsync(dto.Id);

        return new ApiResponse<object>();
    }
}