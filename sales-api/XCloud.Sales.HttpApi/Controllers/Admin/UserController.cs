﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/user")]
public class UserController : ShopBaseController
{
    private readonly IStoreUserService _userService;
    private readonly IGradeService _gradeService;
    private readonly IStoreUserAccountService _storeUserAccountService;

    public UserController(IStoreUserService userService, IGradeService gradeService,
        IStoreUserAccountService storeUserAccountService)
    {
        _userService = userService;
        _gradeService = gradeService;
        _storeUserAccountService = storeUserAccountService;
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateUserStatusInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var storeUser = await this._userService.GetByIdAsync(dto.Id);

        if (storeUser == null)
            throw new EntityNotFoundException(nameof(storeUser));

        await _storeUserAccountService.UpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<StoreUserDto>> QueryPaging([FromBody] QueryUserPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var response = await _userService.QueryStoreUserPagingAsync(dto);

        await _userService.AttachPlatformUsersAsync(response.Items.ToArray());

        await _gradeService.AttachGradeAsync(response.Items.ToArray());

        return response;
    }
}