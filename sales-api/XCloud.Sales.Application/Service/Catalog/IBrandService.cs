using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface IBrandService : ISalesStringCrudAppService<Brand, BrandDto, QueryBrandPaging>
{
    Task<BrandDto[]> AttachPicturesAsync(BrandDto[] data);
}

public class BrandService : SalesStringCrudAppService<Brand, BrandDto, QueryBrandPaging>, IBrandService
{
    private readonly ISalesRepository<Brand> _brandRepository;
    private readonly IStorageMetaService _storageMetaService;

    public BrandService(
        ISalesRepository<Brand> brandRepository, IStorageMetaService storageMetaService) : base(brandRepository)
    {
        _brandRepository = brandRepository;
        _storageMetaService = storageMetaService;
    }

    private string NormalizeName(string name)
    {
        return name?.Trim();
    }

    private string NormalizeKeywords(string keywords)
    {
        if (string.IsNullOrWhiteSpace(keywords))
            return string.Empty;

        keywords = keywords.Trim().ToLower().RemoveWhitespace();
        return keywords;
    }

    public async Task<BrandDto[]> AttachPicturesAsync(BrandDto[] data)
    {
        if (!data.Any())
            return data;

        var pictureIds = data
            .Where(x => !string.IsNullOrWhiteSpace(x.PictureMetaId))
            .Select(x => x.PictureMetaId).Distinct().ToArray();

        if (pictureIds.Any())
        {
            var pictures = await this._storageMetaService.ByIdsAsync(pictureIds);
            foreach (var m in data)
            {
                var pic = pictures.FirstOrDefault(x => x.Id == m.PictureMetaId);
                if (pic == null)
                    continue;
                m.Picture = pic;
                m.Picture.Simplify();
            }
        }

        return data;
    }

    protected override async Task<IQueryable<Brand>> GetPagingFilteredQueryableAsync(DbContext db, QueryBrandPaging dto)
    {
        await Task.CompletedTask;

        var query = db.Set<Brand>().AsNoTracking();

        var q = await BuildQuery(dto);

        var brandIdsQuery = q.Select(x => x.Id).Distinct();

        var brandQuery = query.Where(x => brandIdsQuery.Contains(x.Id));

        return brandQuery;
    }

    private async Task<IQueryable<Brand>> BuildQuery(QueryBrandPaging dto)
    {
        var db = await _brandRepository.GetDbContextAsync();

        var query = db.Set<Brand>().AsNoTracking();

        if (dto.ShowOnHomePage)
            query = query.Where(x => x.ShowOnPublicPage);

        if (!string.IsNullOrWhiteSpace(dto.Name))
            query = query.Where(x => x.Name.Contains(dto.Name) || x.MetaKeywords.Contains(dto.Name));

        if (dto.Published)
        {
            var joinQuery = from brand in query
                join g in db.Set<Goods>().AsNoTracking()
                    on brand.Id equals g.BrandId into goodsGrouping
                from goods in goodsGrouping.DefaultIfEmpty()
                select new
                {
                    brand,
                    goods,
                };
            joinQuery = joinQuery.Where(x => x.goods.Published);
            query = joinQuery.Select(x => x.brand);
        }

        return query;
    }

    protected override async Task<IOrderedQueryable<Brand>> GetPagingOrderedQueryableAsync(IQueryable<Brand> query,
        QueryBrandPaging dto)
    {
        await Task.CompletedTask;
        return query
            .OrderBy(x => x.DisplayOrder)
            .ThenByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id);
    }

    public override async Task DeleteByIdAsync(string id)
    {
        await this.Repository.SoftDeleteAsync(id, this.Clock.Now);
    }

    public override async Task<Brand> InsertAsync(BrandDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException("brand name is required");

        return await base.InsertAsync(dto);
    }

    public override Task<Brand> UpdateAsync(BrandDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException("brand name is required");

        return base.UpdateAsync(dto);
    }

    protected override async Task SetFieldsBeforeInsertAsync(Brand entity)
    {
        entity.Id = GuidGenerator.CreateGuidString();
        entity.Name = NormalizeName(entity.Name);
        entity.Description = entity.Description?.Trim();
        entity.MetaKeywords = NormalizeKeywords(entity.MetaKeywords);
        entity.CreationTime = Clock.Now;

        if (await _brandRepository.AnyAsync(x => x.Name == entity.Name))
            throw new UserFriendlyException("brand is already exist");
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Brand brand, BrandDto dto)
    {
        await Task.CompletedTask;

        brand.Name = NormalizeName(dto.Name);
        brand.MetaKeywords = NormalizeKeywords(dto.MetaKeywords);
        brand.Description = dto.Description;
        brand.PictureMetaId = dto.PictureMetaId;
        brand.ShowOnPublicPage = dto.ShowOnPublicPage;
        brand.Published = dto.Published;

        if (await _brandRepository.AnyAsync(x => x.Id != brand.Id && x.Name == brand.Name))
            throw new UserFriendlyException("brand is already exist");
    }
}