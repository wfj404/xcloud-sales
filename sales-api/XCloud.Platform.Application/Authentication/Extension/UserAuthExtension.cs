﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Authorization;
using XCloud.Application.Extension;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Authentication.Extension;

public static class UserAuthExtension
{
    [NotNull]
    [ItemNotNull]
    public static async Task<UserDto> GetRequiredAuthedUserAsync(this IUserAuthService userAuthService)
    {
        var res = await userAuthService.GetAuthUserAsync();

        res.ThrowIfErrorOccured(x => new AbpAuthorizationException(x.Message ?? nameof(GetRequiredAuthedUserAsync)));

        var user = res.User;

        if (user == null || user.IsEmptyId() || user.IsDeleted)
            throw new AbpAuthorizationException(message: nameof(user));

        if (!user.IsActive)
            throw new AbpAuthorizationException(message: nameof(user.IsActive));

        return user;
    }
}