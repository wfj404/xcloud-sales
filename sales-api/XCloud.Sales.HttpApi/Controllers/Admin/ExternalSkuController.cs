﻿using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using Volo.Abp;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Common;
using XCloud.Sales.WeChat.Service.Payments;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/external-sku")]
public class ExternalSkuController : ShopBaseController
{
    private readonly IExternalSkuService _externalSkuService;
    private readonly ISkuService _skuService;

    public ExternalSkuController(IExternalSkuService externalSkuService, ISkuService skuService)
    {
        _externalSkuService = externalSkuService;
        _skuService = skuService;
    }

    private readonly DataFormatter _dataFormatter = new DataFormatter();

    [NonAction]
    private string[] GetCellValues(ICell[] cells) =>
        cells.Select(x => this._dataFormatter.FormatCellValue(x) ?? string.Empty).ToArray();

    [HttpPost("upload-and-parse-excel")]
    public async Task<ApiResponse<ExternalSkuDto[]>> UploadAndParseExcelAsync([FromForm] IFormFileCollection collection)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var file = collection.FirstOrDefault() ?? throw new UserFriendlyException("file not found");

        await using var stream = file.OpenReadStream();

        var table = await ExcelHelper.FromExcelAsync(stream);

        var sheet = table.Values.FirstOrDefault() ??
                    throw new UserFriendlyException("no sheet found");

        var rowsData = sheet.Select(GetCellValues).ToList();

        if (rowsData.Count < 1)
            throw new UserFriendlyException("wrong format");

        var headers = rowsData[0];
        var datalist = new List<ExternalSkuDto>();

        for (var i = 1; i <= rowsData.Count - 1; ++i)
        {
            var row = rowsData[i];

            if (!decimal.TryParse(row.GetCellValue(headers, "Price", required: true)!, out var price))
            {
                throw new UserFriendlyException("wrong price format");
            }

            var item = new ExternalSkuDto()
            {
                SystemName = default,
                ExternalSkuId = row.GetCellValue(headers, "ID"),
                GoodsName = default,
                SkuName = row.GetCellValue(headers, "SkuName"),
                Price = price
            };

            datalist.Add(item);
        }

        return new ApiResponse<ExternalSkuDto[]>(datalist.ToArray());
    }

    [HttpPost("insert-many")]
    public async Task<ApiResponse<object>> InsertManyAsync([FromBody] ExternalSkuDto[] dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._externalSkuService.InsertManyAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<ExternalSkuGroupingDto>> QueryPagingAsync([FromBody] QueryExternalSkuInput dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        dto.ThrowIfException(1000);

        var response = await this._externalSkuService.QueryPagingAsync(dto);

        await this._externalSkuService.AttachSkusAsync(response.Items.ToArray());

        var skus = response.Items.Select(x => x.Sku).WhereNotNull().ToArray();

        await this._skuService.AttachGoodsAsync(skus);

        return response;
    }

    [HttpPost("save-mapping")]
    public async Task<ApiResponse<object>> SaveMappingAsync([FromBody] ExternalSkuMappingDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._externalSkuService.SaveExternalSkuMappingAsync(combineId: dto.CombineId, skuIdOrEmpty: dto.SkuId);

        return new ApiResponse<object>();
    }
}