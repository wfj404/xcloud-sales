import { Button, Card, Col, DatePicker, Form, Input, Row, Select, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { McsBizTypeOptions, McsInvoicingStatusOptions } from '@/utils/biz_3h';
import { dateTimeFormat, parseAsDayjs, timezoneOffset } from '@/utils/dayjs';
import { QueryMcsInvoiceRequestInput } from '@/utils/swagger_qingdao';

export default ({
  query,
  onChange,
}: {
  query: QueryMcsInvoiceRequestInput;
  onChange: (x: QueryMcsInvoiceRequestInput) => void;
}) => {
  const [q, _q] = useState<QueryMcsInvoiceRequestInput>({});

  useEffect(() => {
    _q(query);
  }, [query]);

  return (
    <>
      <Card bordered={false} style={{ marginBottom: 10 }}>
        <Form labelCol={{ span: 5 }}>
          <Row gutter={[10, 10]}>
            <Col span={8}>
              <Form.Item label="业务类型">
                <Select
                  allowClear
                  value={q.BizType}
                  options={McsBizTypeOptions.map((x) => ({
                    label: x.name,
                    value: x.id,
                  }))}
                  onClear={() => {
                    _q({
                      ...q,
                      BizType: undefined,
                    });
                  }}
                  onChange={(e) => {
                    _q({
                      ...q,
                      BizType: e,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="业务单号">
                <Input
                  value={q.BizOrderId || ''}
                  onChange={(e) => {
                    _q({
                      ...q,
                      BizOrderId: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>

            <Col span={8}>
              <Form.Item label="时间范围">
                <DatePicker.RangePicker
                  allowClear
                  value={[
                    parseAsDayjs(q.StartTime)?.add(timezoneOffset, 'hour') ||
                      null,
                    parseAsDayjs(q.EndTime)?.add(timezoneOffset, 'hour') ||
                      null,
                  ]}
                  onChange={(e) => {
                    _q({
                      ...q,
                      StartTime: e
                        ?.at(0)
                        ?.add(-timezoneOffset, 'hour')
                        ?.format(dateTimeFormat),
                      EndTime: e
                        ?.at(1)
                        ?.add(-timezoneOffset, 'hour')
                        ?.format(dateTimeFormat),
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="开票状态">
                <Select
                  allowClear
                  value={q.Status}
                  options={McsInvoicingStatusOptions}
                  onClear={() => {
                    _q({
                      ...q,
                      Status: undefined,
                    });
                  }}
                  onChange={(e) => {
                    _q({
                      ...q,
                      Status: e,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="是否领取">
                <Switch
                  checked={q.Picked || false}
                  onChange={(e) => {
                    _q({
                      ...q,
                      Picked: e,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item>
                <Button
                  type="primary"
                  onClick={() => {
                    onChange(q);
                  }}
                >
                  搜索
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
    </>
  );
};
