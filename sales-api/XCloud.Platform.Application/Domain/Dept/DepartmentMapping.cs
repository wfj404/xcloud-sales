﻿namespace XCloud.Platform.Application.Domain.Dept;

public class DepartmentMapping : PlatformBaseEntity, IHasCreationTime
{
    public string DepartmentId { get; set; }

    public string AdminId { get; set; }

    public DateTime CreationTime { get; set; }
}