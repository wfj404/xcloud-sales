namespace XCloud.Sales.Application.Utils;

[ExposeServices(typeof(AfterSaleUtils))]
public class AfterSaleUtils : ITransientDependency
{
    public AfterSaleUtils()
    {
        //
    }

    public string[] DoneStatus()
    {
        var status = new[]
        {
            SalesConstants.AfterSalesStatus.Cancelled,
            SalesConstants.AfterSalesStatus.Complete
        };

        return status;
    }

    public string[] PendingStatus()
    {
        var status = new[]
        {
            SalesConstants.AfterSalesStatus.Complete,
            SalesConstants.AfterSalesStatus.Cancelled,
            SalesConstants.AfterSalesStatus.Approved,
            SalesConstants.AfterSalesStatus.Rejected,
            SalesConstants.AfterSalesStatus.Processing
        };

        return status.Select(x => x).Except(DoneStatus()).ToArray();
    }
}