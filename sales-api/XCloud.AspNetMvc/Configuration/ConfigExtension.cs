using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Steeltoe.Extensions.Configuration.Placeholder;

namespace XCloud.AspNetMvc.Configuration;

public static class ConfigExtension
{
    public static IConfigurationBuilder CopyBuilder(this IConfigurationBuilder builder)
    {
        //nacos
        var configBuilder = new ConfigurationBuilder();
        foreach (var originSource in builder.Sources)
        {
            configBuilder.Add(originSource);
        }

        return configBuilder;
    }

    private static void TryAddJsonFile(this IConfigurationBuilder builder, string jsonPath)
    {
        if (!File.Exists(jsonPath))
            return;

        builder.AddJsonFile(jsonPath, false);
    }

    public static void ConfigureConfigBuilder(this WebApplicationBuilder builder,
        Action<IConfigurationBuilder, IList<IConfigurationSource>> configBuilder = null)
    {
        var originSources = builder.Configuration.Sources.ToList();

        builder.Configuration.Sources.Clear();

        builder.Configuration.TryAddJsonFile(Path.Combine(builder.Environment.ContentRootPath, "base.json"));
        builder.Configuration.TryAddJsonFile(Path.Combine(builder.Environment.ContentRootPath, "logging.json"));

        configBuilder?.Invoke(builder.Configuration, originSources);

        //finally add placeholder resolver
        builder.Configuration.AddPlaceholderResolver();
    }
}