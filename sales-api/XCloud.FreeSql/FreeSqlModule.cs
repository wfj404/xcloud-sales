﻿using FreeSql;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using XCloud.Core;

namespace XCloud.FreeSql;

[DependsOn(typeof(XCloudCoreModule))]
public class FreeSqlModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddScoped<UnitOfWorkManager>();
        context.Services.AddScoped(typeof(IFreeSqlIntKeyRepository<>), typeof(FreeSqlIntKeyRepository<>));
        context.Services.AddScoped(typeof(IFreeSqlStringKeyRepository<>), typeof(FreeSqlStringKeyRepository<>));
    }
}