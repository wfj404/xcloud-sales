﻿using System.Threading.Tasks;
using SKIT.FlurlHttpClient.Wechat.Api;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Settings;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.WeChat.Services.Payments;

namespace XCloud.Platform.WeChat;

[ExposeServices(typeof(WechatClientFactory))]
public class WechatClientFactory : ISingletonDependency
{
    private readonly ISettingService _settingService;
    private readonly WechatPaymentCertificateManageService _wechatPaymentCertificateManageService;

    public WechatClientFactory(ISettingService settingService,
        WechatPaymentCertificateManageService wechatPaymentCertificateManageService)
    {
        _settingService = settingService;
        _wechatPaymentCertificateManageService = wechatPaymentCertificateManageService;
    }

    public async Task<WechatApiClient> BuildWechatApiClientAsync()
    {
        var option = await this._settingService.GetWechatMpOptionAsync();

        option.EnsureWechatOptionNotEmpty();

        var wechatOption = new WechatApiClientOptions
        {
            AppId = option.AppId,
            AppSecret = option.AppSecret
        };

        var client = new WechatApiClient(wechatOption);

        return client;
    }

    public async Task<WechatTenpayClient> BuildTenPayClientAsync()
    {
        var option = await this._settingService.GetWechatPaymentOptionAsync();

        option.EnsurePaymentOptionNotEmpty();

        var clientOption = new WechatTenpayClientOptions
        {
            MerchantId = option.MchId,
            MerchantV3Secret = option.ApiV3Key,
            MerchantCertificateSerialNumber = option.MerchantCertificateSerialNumber,
            MerchantCertificatePrivateKey = option.MerchantCertificatePrivateKey,
            PlatformCertificateManager = new InMemoryCertificateManager(),
            AutoDecryptResponseSensitiveProperty = true,
            AutoEncryptRequestSensitiveProperty = true
        };

        var client = new WechatTenpayClient(clientOption);

        var certificateList =
            await this._wechatPaymentCertificateManageService.GetCertificateEntriesAsync(option.MchId);
        
        client.PlatformCertificateManager.SetCertificates(certificateList);

        return client;
    }
}