﻿using System;
using Elastic.Apm.Api;
using Elastic.Apm.NetCoreAll;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.DependencyInjection;

namespace XCloud.Application.Apm.ElasticApm;

public static class ElasticApmExtension
{
    [NotNull]
    public static IConfigurationSection GetElasticApmSectionOrEmpty(this IConfiguration configuration)
    {
        return configuration.GetSection("ElasticApm");
    }

    [CanBeNull]
    private static ITransaction FilterTransaction(ITransaction x)
    {
        x.SetLabel($"{nameof(FilterTransaction)}", "true");

        if (x.Context.Request != null)
        {
            var methodsToIgnore = new[] { "GET", "OPTIONS" };

            foreach (var m in methodsToIgnore)
            {
                if (x.Context.Request?.Method?.Equals(m, StringComparison.OrdinalIgnoreCase) ?? false)
                {
                    return null;
                }
            }
        }

        return x;
    }

    public static void TryUseElasticApm(this IApplicationBuilder builder)
    {
        var config = builder.ApplicationServices.GetRequiredService<IConfiguration>();

        var section = GetElasticApmSectionOrEmpty(configuration: config);

        if (section.Exists())
        {
            builder.UseAllElasticApm(configuration: config);

            if (!Elastic.Apm.Agent.AddFilter(FilterTransaction))
            {
                throw new BusinessException(message: nameof(FilterTransaction));
            }
        }
        else
        {
            var logger = builder.ApplicationServices.GetRequiredService<ILoggerFactory>()
                .CreateLogger(nameof(TryUseElasticApm));

            logger.LogInformation($"skip:{nameof(TryUseElasticApm)}");
        }
    }

    internal static void TryAddElasticApmInterceptor(this IOnServiceRegistredContext context)
    {
        if (context.ShouldTrace())
        {
            /*
             Notice that OnRegistred callback might be called multiple times for the same service class
            if it exposes more than one service/interface.
            So, it's safe to use Interceptors.TryAdd method instead of Interceptors.Add method.
            See the documentation of dynamic proxying / interceptors.
             */

            context.Interceptors.TryAdd<ElasticApmInterceptor>();
        }
    }
}