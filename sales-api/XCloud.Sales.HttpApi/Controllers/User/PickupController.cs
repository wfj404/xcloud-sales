﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.Shipping.Pickup;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/pickup")]
public class PickupController : ShopBaseController
{
    private readonly IStoreSettingService _storeSettingService;
    private readonly ServiceTimeService _serviceTimeService;
    private readonly IPickupService _pickupService;
    private readonly IOrderService _orderService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;

    public PickupController(IStoreSettingService storeSettingService,
        ServiceTimeService serviceTimeService,
        IPickupService pickupService,
        IOrderService orderService,
        IOrderAutoProcessService orderAutoProcessService)
    {
        _storeSettingService = storeSettingService;
        _serviceTimeService = serviceTimeService;
        _pickupService = pickupService;
        _orderService = orderService;
        _orderAutoProcessService = orderAutoProcessService;
    }

    [HttpPost("by-order-id")]
    public async Task<ApiResponse<PickupRecordDto[]>> GetByOrderIdAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        var datalist = await _pickupService.GetByOrderIdAsync(order.Id);

        return new ApiResponse<PickupRecordDto[]>(datalist);
    }

    [HttpPost("mark-as-picked")]
    public async Task<ApiResponse<object>> MarkAsPickedAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var pickup = await _pickupService.EnsureUserPickupAsync(dto.Id, loginUser.Id);

        await _pickupService.SetPickedAsync(pickup.Id);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _pickupService.TryUpdateOrderShipStatusAsync(pickup.OrderId);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _orderAutoProcessService.AutoProcessOrderAsync(pickup.OrderId);

        return new ApiResponse<object>();
    }

    [HttpPost("get-service-time")]
    public async Task<ApiResponse<AvailableServiceTimeRangeDto[]>> GetServiceTimeAsync()
    {
        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var settings = await _storeSettingService.GetSettingsAsync<StorePickupSettings>(storeId);

        var data = _serviceTimeService.ParseServiceTime(settings.ServiceTime, 7, timezoneOffset: 8).ToArray();

        return new ApiResponse<AvailableServiceTimeRangeDto[]>(data);
    }
}