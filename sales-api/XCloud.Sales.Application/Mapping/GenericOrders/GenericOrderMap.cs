﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.GenericOrders;

namespace XCloud.Sales.Application.Mapping.GenericOrders;

public class GenericOrderMap : SalesEntityTypeConfiguration<GenericOrder>
{
    public override void Configure(EntityTypeBuilder<GenericOrder> builder)
    {
        builder.ToTable("sales_generic_order");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigModificationTimeEntity();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.OrderType).RequiredId();
        builder.Property(x => x.OrderId).RequiredId();
        builder.Property(x => x.StoreUserId);

        builder.Property(x => x.Paid);
        builder.Property(x => x.Serviced);
        builder.Property(x => x.ToService);
        builder.Property(x => x.Reviewed);
        builder.Property(x => x.Finished);
        builder.Property(x => x.FinishedTime);
        builder.Property(x => x.Closed);
        builder.Property(x => x.ClosedTime);

        builder.Property(x => x.ItemsJson);
        builder.Property(x => x.ExtraDataJson);

        base.Configure(builder);
    }
}