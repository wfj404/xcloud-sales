﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.ThirdParty.Services.YiLianYun;

[UnitOfWork]
[ExposeServices(typeof(YiLianYunJobs))]
public class YiLianYunJobs : SalesAppService
{
    private readonly IStoreService _storeService;
    private readonly IStoreSettingService _storeSettingService;
    private readonly YiLianYunService _yiLianYunService;

    public YiLianYunJobs(IStoreService storeService,
        IStoreSettingService storeSettingService,
        YiLianYunService yiLianYunService)
    {
        _storeService = storeService;
        _storeSettingService = storeSettingService;
        _yiLianYunService = yiLianYunService;
    }

    public async Task RefreshAccessTokenAsync()
    {
        var storeList = await this._storeService.ListAllStoresAsync(openingOnly: false, maxCount: 1000);

        foreach (var store in storeList)
        {
            try
            {
                var settings = await this._storeSettingService.GetSettingsAsync<YiLianYunConfigDto>(store.Id);

                if (!settings.Available)
                    continue;

                foreach (var device in settings.Devices)
                {
                    await this._yiLianYunService.GetOrCreateAccessTokenAsync(settings, device.MachineCode);
                }
            }
            catch (Exception e)
            {
                this.Logger.LogWarning(message: e.Message, exception: e);
            }
        }
    }
}