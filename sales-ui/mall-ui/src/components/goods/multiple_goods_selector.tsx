import { Button, Card, Modal } from 'antd';
import { useState } from 'react';

import XGoodsSelector from '@/components/goods/goods_selector';
import { GoodsDto } from '@/utils/swagger';
import { GlobalContainer } from '@/utils/utils';
import { DragSortTable, ProColumns } from '@ant-design/pro-components';

export default ({
  goods_list,
  onChange,
  maxCount,
}: {
  goods_list: GoodsDto[];
  onChange: (e: GoodsDto[]) => void;
  maxCount?: number;
}) => {
  const [show, _show] = useState(false);

  const count = maxCount || 100000;
  const disabled = goods_list.length >= count;

  const columns: ProColumns<GoodsDto>[] = [
    {
      title: 'No',
      dataIndex: 'index',
      valueType: 'indexBorder',
    },
    {
      title: '排序',
      dataIndex: 'sort',
      width: 60,
      className: 'drag-visible',
    },
    {
      title: '商品',
      render: (d, x) => x.Name || '--',
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      render: (d, x) => {
        return (
          <>
            <Button
              type="link"
              danger
              onClick={() => {
                if (!confirm('删除？')) {
                  return;
                }
                onChange([...goods_list.filter((m) => m.Id != x.Id)]);
              }}
            >
              删除
            </Button>
          </>
        );
      },
    },
  ];

  return (
    <>
      <Modal
        title="搜索商品"
        open={show}
        onCancel={() => _show(false)}
        onOk={() => _show(false)}
      >
        <XGoodsSelector
          onChange={(e) => {
            if (!e) {
              return;
            }
            if (disabled) {
              GlobalContainer.message?.error(`数量已达上限：${count}`);
              return;
            }
            if (!confirm(`添加${e.Name || '--'}？`)) {
              return;
            }
            if (goods_list.some((x) => x.Id == e.Id)) {
              GlobalContainer.message?.error('该商品已存在');
              return;
            }
            onChange([...goods_list, e]);
            _show(false);
          }}
        />
      </Modal>
      <Card
        size="small"
        title="选择商品"
        extra={
          <Button
            disabled={disabled}
            type="link"
            size="small"
            onClick={() => {
              _show(true);
            }}
          >
            添加商品
          </Button>
        }
      >
        <DragSortTable
          //rowKey={(x) => x.Id || '--'}
          rowKey={'Id'}
          columns={columns}
          dataSource={goods_list || []}
          toolBarRender={false}
          pagination={false}
          headerTitle={false}
          search={false}
          dragSortKey="sort"
          onDragSortEnd={(a, b, datalist) => {
            onChange([...datalist]);
          }}
        ></DragSortTable>
      </Card>
    </>
  );
};
