import { ReactElement } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { managerHasPermission } from '@/utils/biz';

export default ({
  children,
  permissionKey,
}: {
  permissionKey: string;
  children: ReactElement | any;
}) => {
  const app = useSnapshot(storeState) as StoreType;

  if (app.managerPermissions == undefined) {
    return <h1>权限未加载</h1>;
  }

  if (!managerHasPermission(app.managerPermissions, permissionKey)) {
    return <h1>无权访问</h1>;
  }

  return children;
};
