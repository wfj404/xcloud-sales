﻿using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace XCloud.Core.Json;

public static class SerializerExtension
{
    public static bool TrySerializeToString<T>(this IJsonDataSerializer jsonDataSerializer, T data, out string json)
    {
        json = default;
        try
        {
            json = jsonDataSerializer.SerializeToString(data);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static T SerializeThenDeserialize<T>(this IJsonDataSerializer jsonDataSerializer, T data)
    {
        var json = jsonDataSerializer.SerializeToString(data);

        var dataCopy = jsonDataSerializer.DeserializeFromString<T>(json);

        return dataCopy;
    }

    [NotNull]
    public static T[] DeserializeArrayFromStringOrEmpty<T>(this IJsonDataSerializer jsonDataSerializer,
        string json,
        ILogger logger)
    {
        if (!string.IsNullOrWhiteSpace(json))
        {
            var trimJson = json.Trim();

            if (trimJson.StartsWith('[') && trimJson.EndsWith(']'))
            {
                var data = jsonDataSerializer.DeserializeFromStringOrDefault<T[]>(trimJson, logger);
                if (data != null)
                {
                    return data;
                }
            }
        }

        return [];
    }

    [CanBeNull]
    public static T DeserializeFromStringOrDefault<T>(this IJsonDataSerializer jsonDataSerializer,
        string json,
        ILogger logger)
    {
        if (!string.IsNullOrWhiteSpace(json))
        {
            try
            {
                var obj = jsonDataSerializer.DeserializeFromString<T>(json);

                return obj;
            }
            catch (Exception e)
            {
                logger.LogWarning(message: nameof(DeserializeFromStringOrDefault), exception: e);
            }
        }

        return default;
    }

    public static string GetDateformatOrDefault(this IConfiguration config)
    {
        var format = config["app:config:date_format"];
        if (string.IsNullOrWhiteSpace(format))
        {
            format = "yyyy-MM-dd HH:mm:ss";
        }

        return format;
    }

    private static IEnumerable<string> FindJsonNode(JToken token)
    {
        if (token == null)
        {
            yield break;
        }

        if (token.Type == JTokenType.Property)
        {
            yield return token.Path;
        }

        //find next node
        var children = token.Children();
        foreach (var child in children)
        {
            foreach (var path in FindJsonNode(child))
            {
                yield return path;
            }
        }
    }

    /// <summary>
    /// 比较两个json结构是否相同
    /// </summary>
    public static bool HasSameStructure(this IJsonDataSerializer jsonDataSerializer, string json1, string json2)
    {
        var path1 = FindJsonNode(JToken.Parse(json1)).OrderBy(x => x).ToArray();
        var path2 = FindJsonNode(JToken.Parse(json2)).OrderBy(x => x).ToArray();

        return path1.SequenceEqual(path2);
    }
}