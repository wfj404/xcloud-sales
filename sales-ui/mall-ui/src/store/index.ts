import { proxy } from 'umi';

import { apiClient } from '@/utils/client';
import { config } from '@/utils/config';
import {
    AdminAuthResult, AdminDto, AdminGrantedPermissionResponse, GradeDto, MallSettingsDto, StoreDto,
    StoreManagerAuthResult, StoreManagerDto, StoreManagerGrantedPermissionResponse,
    StoreUserAuthResult, StoreUserDto, UserAuthResult
} from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export interface StoreType {
  messageHistory: MessageEvent[];
  setMssageHistory: (e: MessageEvent[]) => void;
  grades: GradeDto[];
  stores: StoreDto[];
  queryGrades: () => Promise<void>;
  queryStores: () => Promise<void>;
  notificationCount: number;
  setNotificationCount: (x: number) => void;
  queryNotificationCount: () => Promise<void>;
  headerHeight: number;
  setHeaderHeight: (x: number) => void;
  bottomHeight: number;
  setBottomHeight: (x: number) => void;
  mallSettings: MallSettingsDto;
  setMallSettings: (x: MallSettingsDto) => void;
  shoppingCartCount: number;
  setShoppingCartCount: (x: number) => void;
  queryShoppingCartCount: () => Promise<void>;
  currentStore?: StoreDto;
  setCurrentStore: (x?: StoreDto) => void;
  queryCurrentStore: () => Promise<void>;
  mostNearbyStore?: StoreDto;
  setMostNearbyStore: (x?: StoreDto) => void;
  queryMostNearbyStore: () => Promise<void>;
  getAppName: (official?: boolean) => string | undefined | null;
  userAuthResult?: UserAuthResult;
  setUserAuthResult: (x?: UserAuthResult) => void;
  adminAuthResult?: AdminAuthResult;
  setAdminAuthResult: (x?: AdminAuthResult) => void;
  adminPermissions?: AdminGrantedPermissionResponse;
  setAdminPermissions: (x?: AdminGrantedPermissionResponse) => void;
  storeUserAuthResult?: StoreUserAuthResult;
  setStoreUserAuthResult: (x?: StoreUserAuthResult) => void;
  storeManagerAuthResult?: StoreManagerAuthResult;
  setStoreManagerAuthResult: (x?: StoreManagerAuthResult) => void;
  managerPermissions?: StoreManagerGrantedPermissionResponse;
  setManagerPermissions: (x?: StoreManagerGrantedPermissionResponse) => void;
  //computed
  admin?: AdminDto;
  storeUser?: StoreUserDto;
  storeManager?: StoreManagerDto;
}

const getAppName = (official?: boolean) => {
  const mallSettings = storeState.mallSettings || {};
  let data = [mallSettings.StoreNickName, mallSettings.StoreEnglishName];

  if (official) {
    data = [
      mallSettings.StoreOfficialName,
      mallSettings.StoreOfficialEnglishName,
      ...data,
    ];
  } else {
    data = [
      ...data,
      mallSettings.StoreOfficialName,
      mallSettings.StoreOfficialEnglishName,
    ];
  }

  const name = data.filter((x) => !isStringEmpty(x)).at(0);

  return name || config.app.name || config.app.englishName;
};

const data_container: StoreType = {
  messageHistory: [],
  setMssageHistory: (e) => (storeState.messageHistory = e || []),
  grades: [],
  stores: [],
  queryGrades: async () => {
    const res = await apiClient.mallAdmin.gradeListGrades();
    storeState.grades = res.data.Data || [];
  },
  queryStores: async () => {
    const res = await apiClient.mallAdmin.storeQueryStores();
    storeState.stores = res.data.Data || [];
  },
  notificationCount: 0,
  setNotificationCount: (x: number) => (storeState.notificationCount = x),
  queryNotificationCount: async () => {
    const res = await apiClient.mall.notificationUnReadCount();
    storeState.notificationCount = res.data.Data || 0;
  },
  headerHeight: 0,
  setHeaderHeight: (x: number) => (storeState.headerHeight = x),
  bottomHeight: 0,
  setBottomHeight: (x: number) => (storeState.bottomHeight = x),
  mallSettings: {},
  setMallSettings: (x: MallSettingsDto) => (storeState.mallSettings = x),
  shoppingCartCount: 0,
  setShoppingCartCount: (x: number) => (storeState.shoppingCartCount = x),
  queryShoppingCartCount: async () => {
    const res = await apiClient.mall.shoppingCartCount();
    storeState.shoppingCartCount = res.data.Data || 0;
  },
  currentStore: undefined,
  setCurrentStore: (x?: StoreDto) => (storeState.currentStore = x),
  queryCurrentStore: async () => {
    const res = await apiClient.mall.storeGetCurrentStore();
    storeState.currentStore = res.data.Data || {};
  },
  mostNearbyStore: undefined,
  setMostNearbyStore: (x?: StoreDto) => (storeState.mostNearbyStore = x),
  queryMostNearbyStore: async () => {
    const res = await apiClient.mall.storeMostNearbyStore();
    storeState.mostNearbyStore = res.data.Data || {};
  },
  getAppName: getAppName,
  userAuthResult: undefined,
  setUserAuthResult: (x?: UserAuthResult) => (storeState.userAuthResult = x),
  adminAuthResult: undefined,
  setAdminAuthResult: (x?: AdminAuthResult) => (storeState.adminAuthResult = x),
  adminPermissions: undefined,
  setAdminPermissions: (x?: AdminGrantedPermissionResponse) =>
    (storeState.adminPermissions = x),
  storeUserAuthResult: undefined,
  setStoreUserAuthResult: (x?: StoreUserAuthResult) =>
    (storeState.storeUserAuthResult = x),
  storeManagerAuthResult: undefined,
  setStoreManagerAuthResult: (x?: StoreManagerAuthResult) =>
    (storeState.storeManagerAuthResult = x),
  managerPermissions: undefined,
  setManagerPermissions: (x?: StoreManagerGrantedPermissionResponse) =>
    (storeState.managerPermissions = x),
  //computed
  get admin() {
    return this.adminAuthResult?.Admin;
  },
  get storeUser() {
    return this.storeUserAuthResult?.StoreUser;
  },
  get storeManager() {
    return this.storeManagerAuthResult?.StoreManager;
  },
};

export const storeState: StoreType = proxy<StoreType>(data_container);
