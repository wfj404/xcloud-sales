﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;
using XCloud.Application.Model;

namespace XCloud.Application.Extension;

public static class EntityExtension
{
    public static bool IsTimeRangeValid<T>(this T x, DateTime time)
        where T : IMayHaveTimeRange
    {
        return (x.StartTime == null || x.StartTime.Value <= time) &&
               (x.EndTime == null || x.EndTime.Value >= time);
    }

    public static IQueryable<T> FilterByTimeRange<T>(this IQueryable<T> queryable, DateTime time)
        where T : IMayHaveTimeRange
    {
        return queryable
            .Where(x => x.StartTime == null || x.StartTime.Value <= time)
            .Where(x => x.EndTime == null || x.EndTime.Value >= time);
    }

    public static bool IsEmptyId(this IEntityDto<int> entity) => entity.Id <= 0;

    public static bool IsEmptyId(this IEntityDto<string> entity) => string.IsNullOrWhiteSpace(entity.Id);

    public static T[] Ids<T>(this IEnumerable<IEntity<T>> list) =>
        list.Select(x => x.Id).Distinct().ToArray();

    public static void SetEntityFields<T>(this T model, object data, Func<PropertyInfo, bool> ignorePropertyFunc = null)
        where T : class, IEntity
    {
        if (model == null)
            throw new ArgumentNullException(nameof(model));

        if (data == null)
            throw new ArgumentNullException(nameof(data));

        ignorePropertyFunc ??= x => false;

        var props = model.GetType().GetProperties();

        foreach (var m in data.GetType().GetProperties())
        {
            if (ignorePropertyFunc.Invoke(m))
                continue;

            var val = m.GetValue(data);

            var prop = props.FirstOrDefault(x => x.Name == m.Name);

            if (prop == null)
                throw new AbpException("set data,field not found");

            if (!prop.CanWrite)
                throw new AbpException("set data,field can't be written");

            prop.SetValue(model, val);
        }
    }

    public static void SetPropertyValue<T, F>(this T model, string field, F value)
    {
        if (string.IsNullOrWhiteSpace(field))
            throw new ArgumentNullException(nameof(field));

        var prop = model.GetType().GetProperties().FirstOrDefault(x => x.Name == field);

        if (prop == null)
            throw new ArgumentException($"prop:{field} not found");

        if (!prop.CanWrite)
            throw new NotSupportedException($"{prop.Name} can't write");

        prop.SetValue(model, value);
    }

    public static void SetMemberValue<T, F>(this T model, Expression<Func<T, F>> field, F value)
    {
        if (field == null)
            throw new ArgumentNullException(nameof(field));

        if (field.Body.NodeType == ExpressionType.MemberAccess && field.Body is MemberExpression member)
        {
            if (member.Member is PropertyInfo prop)
            {
                SetPropertyValue(model, prop.Name, value);
                return;
            }
        }

        throw new NotSupportedException($"{nameof(SetMemberValue)}");
    }
}