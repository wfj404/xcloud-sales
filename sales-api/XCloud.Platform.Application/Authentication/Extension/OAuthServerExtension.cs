using Microsoft.Extensions.Configuration;

namespace XCloud.Platform.Application.Authentication.Extension;

public static class OAuthServerExtension
{
    public static OAuthServerOption GetOAuthServerOption(this IConfiguration configuration)
    {
        var section = configuration.GetRequiredSection("OAuthServer");

        var option = new OAuthServerOption();

        section.Bind(option);

        option.Validate();

        return option;
    }
}