import { Button, Switch, Table } from 'antd';
import { ColumnType } from 'antd/es/table';

import { DeliveryRule } from '@/utils/swagger';

import XAreaSelectorMultiple from '../../components/area_selector_multiple';
import XTieredPrice from '../../components/tiered_price';

export default ({
  delivery_rules,
  ok,
}: {
  delivery_rules: DeliveryRule[];
  ok: (d: DeliveryRule[]) => void;
}) => {
  const columns: ColumnType<DeliveryRule>[] = [
    {
      title: '地区',
      render: (d, x, i) => {
        if (x.IsDefault) {
          return <span>所有地区</span>;
        }

        return (
          <>
            <XAreaSelectorMultiple
              area_list={x.Areas || []}
              ok={(e) => {
                let rules = [...delivery_rules];
                rules[i].Areas = e || [];
                ok(rules);
              }}
            />
          </>
        );
      },
    },
    {
      title: '阶梯定价',
      render: (d, x, i) => {
        return (
          <>
            <XTieredPrice
              unit="kg"
              data={x.TieredPrice || {}}
              onChange={(e) => {
                let rules = [...delivery_rules];
                rules[i].TieredPrice = e || {};
                ok(rules);
              }}
            />
          </>
        );
      },
    },
    {
      title: '默认',
      render: (d, x, i) => {
        return (
          <>
            <Switch
              checked={x.IsDefault}
              onChange={(e) => {
                let rules = [...delivery_rules];
                rules.forEach((m) => (m.IsDefault = false));
                rules[i].IsDefault = e;
                ok(rules);
              }}
            />
          </>
        );
      },
    },
    {
      title: '--',
      render: (d, x, i) => {
        return (
          <>
            <Button.Group>
              <Button danger type={'primary'} onClick={() => {}}>
                删除
              </Button>
            </Button.Group>
          </>
        );
      },
    },
  ];

  return (
    <>
      <Table
        columns={columns}
        dataSource={delivery_rules || []}
        pagination={false}
      />
      <Button
        style={{
          marginTop: 10,
          marginBottom: 10,
        }}
        block
        type={'dashed'}
        onClick={() => {
          ok([
            ...delivery_rules,
            {
              Areas: [],
              TieredPrice: {
                StartingAmount: 0,
                StartingPrice: 0,
                AddedUnitPrice: 0,
                AddedUnitAmount: 0,
              },
              IsDefault: false,
            },
          ]);
        }}
      >
        添加地区邮费设置
      </Button>
    </>
  );
};
