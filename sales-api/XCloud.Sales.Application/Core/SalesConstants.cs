namespace XCloud.Sales.Application.Core;

public static class SalesConstants
{
    public static string ServiceName => "mall";

    public static class DbConnectionNames
    {
        public const string SqlServer = "XSalesSqlServer";
        public const string MySql = "XSalesMySql";
    }
    
    /// <summary>
    /// 库存扣件策略
    /// </summary>
    public static class OrderStockDeductStrategy
    {
        public static string AfterPlaceOrder => "after-place-order";
        public static string AfterPayment => "after-payment";
    }

    public static class PaymentMethod
    {
        public static string Online => "online";
        public static string Offline => "offline";
    }

    public static class PaymentChannel
    {
        public static string None => string.Empty;
        public static string Manual => "manual";
        public static string Balance => "balance";
        public static string WechatMp => "wechat-mp";
        public static string WechatOpen => "wechat-open";
    }

    public static class PaymentStatus
    {
        public static string Pending => "pending";
        public static string Paid => "paid";
    }

    public static class OrderStatus
    {
        public static string Pending => "pending";
        public static string Processing => "processing";
        public static string AfterSales => "aftersales";
        public static string Finished => "finished";
        public static string Closed => "closed";
    }

    public static class DeliveryType
    {
        public static string Express => "express";
        public static string ShortDistanceDelivery => "short-distance-delivery";
    }

    public static class ShippingStatus
    {
        public static string NotShipping => "not-yet";
        public static string Shipping => "shipping";
        public static string Shipped => "shipped";
    }

    public static class ShippingMethodType
    {
        public static string Delivery => "delivery";
        public static string Pickup => "pickup";
    }

    public static class AfterSalesStatus
    {
        public static string Processing => "processing";
        public static string Approved => "approved";
        public static string Rejected => "rejected";
        public static string Complete => "finished";
        public static string Cancelled => "closed";
    }
}