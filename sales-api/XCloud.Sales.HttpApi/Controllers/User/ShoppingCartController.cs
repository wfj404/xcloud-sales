﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Sales.Application.Service.ShoppingCart;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context;

namespace XCloud.Sales.HttpApi.Controllers.User;

/// <summary>
/// 购物车/收藏接口
/// </summary>
[Route("api/mall/shoppingcart")]
public class ShoppingCartController : ShopBaseController
{
    private readonly IShoppingCartService _shoppingCartService;
    private readonly IPriceCalculationService _priceCalculationService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly IGoodsPictureService _goodsPictureService;
    private readonly ISkuService _skuService;

    /// <summary>
    /// 构造器
    /// </summary>
    public ShoppingCartController(IShoppingCartService shoppingCartService,
        IStoreGoodsMappingService storeGoodsMappingService,
        IGoodsPictureService goodsPictureService, ISkuService skuService,
        IPriceCalculationService priceCalculationService)
    {
        _shoppingCartService = shoppingCartService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _goodsPictureService = goodsPictureService;
        _skuService = skuService;
        _priceCalculationService = priceCalculationService;
    }

    [HttpPost("count")]
    public async Task<ApiResponse<int>> CountAsync()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var data = await _shoppingCartService.QueryUserShoppingCartAsync(loginUser.Id);

        return new ApiResponse<int>(data.Length);
    }

    [HttpPost("list")]
    public async Task<ApiResponse<ShoppingCartItemDto[]>> ListShoppingCarts()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var cart = await _shoppingCartService.QueryUserShoppingCartAsync(loginUser.Id);

        if (cart.Any())
        {
            //goods and specs
            await _shoppingCartService.AttachGoodsAsync(cart);

            var skus = cart.Select(x => x.Sku).WhereNotNull().ToArray();
            await this._skuService.AttachGoodsAsync(skus);

            var goods = skus.Select(x => x.Goods).WhereNotNull().ToArray();

            //images
            await _goodsPictureService.AttachImagesAsync(goods);

            //quantity
            await _storeGoodsMappingService.AttachStoreQuantityAsync(skus, storeId);

            //price
            await _priceCalculationService.AttachPriceModelAsync(skus, storeId, loginUser.Id);

            //warnings
            await _shoppingCartService.AttachWarningsAsync(cart);
        }

        return new ApiResponse<ShoppingCartItemDto[]>(cart);
    }

    [HttpPost("add-v1")]
    public async Task<ApiResponse<object>> GoodsToCartV1([FromBody] AddOrUpdateShoppingCartInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = loginUser.Id;

        await _shoppingCartService.AddOrUpdateShoppingCartAsync(dto);

        await SalesEventBusService.NotifyInsertActivityLog(new ActivityLog
        {
            ActivityLogTypeId = ActivityLogType.AddShoppingCart,
            StoreUserId = loginUser.Id,
            SubjectId = dto.SkuId,
            SubjectType = ActivityLogSubjectType.Sku,
            Comment = "添加收藏"
        });

        return new ApiResponse<object>();
    }

    [HttpPost("update")]
    public async Task<ApiResponse<object>> UpdateCartItem([FromBody] UpdateShoppingCartQuantityInput item)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        if (item.Quantity < 1)
            return new ApiResponse<object>().SetError("数量不能小于零");

        //get shopping cart item
        var cart = await _shoppingCartService.QueryUserShoppingCartAsync(loginUser.Id);
        var sci = cart.FirstOrDefault(x => x.Id == item.Id);

        if (sci == null)
            return new ApiResponse<object>().SetError("shopping cart item is deleted");

        await _shoppingCartService.UpdateShoppingCartQuantityAsync(sci.Id, item.Quantity);

        return new ApiResponse<object>();
    }

    /// <summary>
    /// 删除购物车子项
    /// </summary>
    [HttpPost("delete")]
    public async Task<ApiResponse<object>> RemoveItem([FromBody] IdDto<int> dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var userCarts = await _shoppingCartService.QueryUserShoppingCartAsync(loginUser.Id);

        var cart = userCarts.FirstOrDefault(x => x.Id == dto.Id);
        if (cart == null)
            throw new EntityNotFoundException(nameof(cart));

        await _shoppingCartService.DeleteShoppingCartAsync(new[] { dto.Id });

        await SalesEventBusService.NotifyInsertActivityLog(new ActivityLog
        {
            ActivityLogTypeId = ActivityLogType.DeleteShoppingCart,
            StoreUserId = loginUser.Id,
            SubjectId = cart.SkuId,
            SubjectType = ActivityLogSubjectType.Sku,
            Comment = "删除购物车"
        });

        return new ApiResponse<object>();
    }
}