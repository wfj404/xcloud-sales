import { useTitle } from 'ahooks';
import { useEffect } from 'react';

import { Box, Container } from '@mui/material';

import XBalance from './balance';
import XHeader from './header';
import XMenu from './menu';

export default function () {
  useTitle('用户');

  useEffect(() => {
    let originColor = document.body.style.backgroundColor;
    document.body.style.backgroundColor = '#f5f7f9';
    return () => {
      document.body.style.backgroundColor = originColor;
    };
  }, []);

  return (
    <>
      <Container maxWidth="sm" disableGutters>
        <Box sx={{ mb: 1 }} className="bg">
          <XHeader />
          <XBalance />
          <XMenu />
        </Box>
      </Container>
    </>
  );
}
