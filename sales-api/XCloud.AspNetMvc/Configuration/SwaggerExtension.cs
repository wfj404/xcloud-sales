﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using XCloud.Application.Core;
using XCloud.Core.Configuration;
using XCloud.Core.Extension;
using XCloud.Core.Helper;

namespace XCloud.AspNetMvc.Configuration;

public static class SwaggerExtension
{
    [Obsolete("不能很好的处理array，尽量使用list替代")]
    public static string GetSwaggerSchemeName(this Type t)
    {
        if (t.IsArray)
        {
            var name = t.Name;
            var ends = new[] { '[', ',', ']' };
            while (true)
            {
                var find = false;
                foreach (var m in ends)
                {
                    if (name.EndsWith(m))
                    {
                        find = true;
                        name = name.TrimEnd(m);
                    }
                }

                if (!find)
                    break;
            }

            return $"{name}_Array_{t.GetArrayRank()}";
        }

        if (t.IsGenericType)
        {
            var sb = new StringBuilder();
            sb.Append(t.Name.Replace("`", "_"));

            var args = t.GetGenericArguments();

            foreach (var m in args)
            {
                sb.Append($"_{GetSwaggerSchemeName(m)}");
            }

            return sb.ToString();
        }

        return t.Name;
    }

    [MethodMetric]
    public static void AddSwagger(this IXCloudBuilder builder, string serviceName,
        string title,
        string description = null)
    {
        var config = builder.Services.GetConfiguration();
        if (!SwaggerEnabled(config))
            return;

        var apiAssembly = builder.EntryModuleType.Assembly;
        var env = builder.Services.GetHostingEnvironment();
        var path = Path.Combine(env.ContentRootPath, $"{apiAssembly.GetName().Name}.xml");

        builder.Services.AddSwaggerGen(option =>
        {
            option.SwaggerDoc(name: serviceName, info: new OpenApiInfo
            {
                Title = title,
                Description = description ?? string.Empty
            });
            option.CustomSchemaIds(x => x.GetSwaggerSchemeName());

            option.AddServer(new OpenApiServer
            {
                Url = string.Empty,
                Description = "default server"
            });
            option.CustomOperationIds(apiDesc =>
            {
                var controllerAction = apiDesc.ActionDescriptor as ControllerActionDescriptor;

                if (controllerAction == null)
                    throw new ArgumentException(nameof(controllerAction));

                return controllerAction.ControllerName + "-" + controllerAction.ActionName;
            });

            if (File.Exists(path))
            {
                option.IncludeXmlComments(path);
            }

            option.AddSwaggerSecurityRequirement(builder);
        });
        //https://github.com/domaindrivendev/Swashbuckle.AspNetCore#systemtextjson-stj-vs-newtonsoft
        //builder.Services.AddSwaggerGenNewtonsoftSupport();
    }

    /// <summary>
    /// 默认开启，可以通过配置关闭
    /// </summary>
    /// <param name="config"></param>
    /// <returns></returns>
    private static bool SwaggerEnabled(this IConfiguration config)
    {
        var res = (config["app:config:swagger"] ?? "true").ToBool();
        return res;
    }

    private static void AddSwaggerSecurityRequirement(this SwaggerGenOptions option, IXCloudBuilder builder)
    {
        var securityRequirement = new OpenApiSecurityRequirement();

        var bearerAuth = new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "请输入OAuth接口返回的Token，前置Bearer。示例：Bearer {Token}",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey
        };
        option.AddSecurityDefinition("Bearer", bearerAuth);
        securityRequirement.Add(new OpenApiSecurityScheme
        {
            Reference = new OpenApiReference
            {
                Type = ReferenceType.SecurityScheme,
                Id = "Bearer"
            }
        }, Array.Empty<string>());

        option.AddSecurityRequirement(securityRequirement);
    }

    public static void UseSwaggerDefaultDefinitionJson(this IApplicationBuilder app)
    {
        //在网关层不要用这种route模板暴露json，否则会被swagger中间件拦截json请求。
        //这时应该另外定义模板，然后把api开头的url交给网关转发到下层服务
        app.UseSwagger(option => option.RouteTemplate = "/swagger/{documentName}/swagger.json");
    }

    /// <summary>
    /// 暴露接口定义json
    /// </summary>
    /// <param name="app"></param>
    /// <returns></returns>
    public static void UseSwaggerApiDefinitionJson(this IApplicationBuilder app)
    {
        app.UseSwagger(option => option.RouteTemplate = "/api/{documentName}/swagger.json");
    }

    public static void UseSwaggerUiPage(this IApplicationBuilder app, Dictionary<string, string> endpoints)
    {
        if (ValidateHelper.IsEmptyCollection(endpoints))
            throw new ArgumentNullException(nameof(endpoints));

        var config = app.ApplicationServices.GetRequiredService<IConfiguration>();

        if (!SwaggerEnabled(config))
            return;

        app.UseSwaggerUI(option =>
        {
            foreach (var endpoint in endpoints)
            {
                option.SwaggerEndpoint(name: endpoint.Key, url: endpoint.Value);
            }
        });
    }
}