import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { config } from '@/utils/config';
import { isStringEmpty } from '@/utils/utils';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { Typography } from '@mui/material';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  const settings = app.mallSettings || {};

  if (isStringEmpty(settings.FooterSlogan)) {
    const englishName =
      [settings.StoreEnglishName, settings.StoreOfficialEnglishName]
        .filter((x) => !isStringEmpty(x))
        .at(0) || config.app.englishName;

    return (
      <Typography
        variant="body2"
        color="text.secondary"
        align="center"
        sx={{ mt: 2, mb: 1, fontSize: 10 }}
      >
        {'Provided by'} <strong>{englishName}</strong> {'with love.'}
        {'{'}
        <FavoriteIcon sx={{ color: 'red' }} fontSize="small" />
        {'}'}
      </Typography>
    );
  }

  return (
    <>
      <Typography
        variant="body2"
        color="text.secondary"
        align="center"
        sx={{ mt: 2, mb: 1, fontSize: 10 }}
      >
        {settings.FooterSlogan}
      </Typography>
    </>
  );
};
