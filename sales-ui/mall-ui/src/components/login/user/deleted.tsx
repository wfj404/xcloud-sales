import { ErrorBlock } from 'antd-mobile';

export default () => {
  return (
    <>
      <ErrorBlock fullPage title={'无法访问'} description={'账号已经被删除'} status={'disconnected'} />
    </>
  );
};
