using Volo.Abp.Localization;

namespace XCloud.Application.Localization;

[LocalizationResourceName(nameof(ApplicationSharedResource))]
public class ApplicationSharedResource
{
    //
}