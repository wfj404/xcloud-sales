﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Pickup;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/order")]
public class OrderController : ShopBaseController
{
    private readonly IOrderService _orderService;
    private readonly IOrderProcessingService _orderProcessingService;
    private readonly OrderViewService _orderViewService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;
    private readonly IOrderPaymentProcessingService _orderPaymentProcessingService;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly IDeliveryService _deliveryService;
    private readonly IPickupService _pickupService;

    public OrderController(IOrderService orderService,
        IOrderProcessingService orderProcessingService,
        OrderViewService orderViewService,
        IOrderAutoProcessService orderAutoProcessService,
        IOrderPaymentProcessingService orderPaymentProcessingService,
        IDeliveryService deliveryService,
        IPickupService pickupService,
        IOrderRefundBillService orderRefundBillService,
        IOrderPaymentBillService orderPaymentBillService)
    {
        _orderService = orderService;
        _orderProcessingService = orderProcessingService;
        _orderViewService = orderViewService;
        _orderAutoProcessService = orderAutoProcessService;
        _orderPaymentProcessingService = orderPaymentProcessingService;
        _deliveryService = deliveryService;
        _pickupService = pickupService;
        _orderRefundBillService = orderRefundBillService;
        _orderPaymentBillService = orderPaymentBillService;
    }

    [HttpPost("refresh-status")]
    public async Task<ApiResponse<object>> RefreshStatusAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var order = await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        await this._orderPaymentBillService.UpdateOrderPaidAmountAsync(order.Id);

        await this._orderRefundBillService.UpdateOrderRefundedAmountAsync(order.Id);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _orderPaymentProcessingService.TryUpdatePaymentStatusAsync(order.Id);

        if (order.IsDelivery())
        {
            await _deliveryService.TryUpdateOrderDeliveryStatusAsync(order.Id);
        }
        else if (order.IsPickup())
        {
            await _pickupService.TryUpdateOrderShipStatusAsync(order.Id);
        }

        await _orderAutoProcessService.AutoProcessOrderAsync(order.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("dangerously-update-status")]
    public async Task<ApiResponse<object>> DangerouslyUpdateStatusAsync([FromBody] UpdateOrderStatusInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        await _orderProcessingService.DangerouslyUpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<OrderDto>> QueryOrderPagingAsync([FromBody] QueryOrderPagingInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        dto.StoreId = manager.StoreId;
        dto.PageSize = 20;
        dto.HideForCustomer = null;

        var res = await _orderService.QueryOrderPagingAsync(dto);

        await _orderViewService.AttachOrderDetailDataAsync(res.Items.ToArray());

        return res;
    }

    /// <summary>
    /// 获取订单详情
    /// </summary>
    [HttpPost("detail")]
    public async Task<ApiResponse<OrderDto>> DetailAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var order = await _orderService.GetByIdAsync(dto.Id);

        if (order == null)
            throw new EntityNotFoundException();

        await _orderViewService.AttachOrderDetailDataAsync(new[] { order });

        return new ApiResponse<OrderDto>(order);
    }

    [HttpPost("cancel")]
    public async Task<ApiResponse<object>> CancelOrder([FromBody] CloseOrderInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.OrderId, manager.StoreId);

        await _orderProcessingService.CloseAsync(dto);

        return new ApiResponse<object>();
    }
}