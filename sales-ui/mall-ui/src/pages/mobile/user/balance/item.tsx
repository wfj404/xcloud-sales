import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { BalanceHistoryDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Chip, Divider, Typography } from '@mui/material';

export default (props: { model: BalanceHistoryDto }) => {
  const { model } = props;

  if (!model || model.Balance == 0) {
    return null;
  }

  const renderBalance = () => {
    let balance = model.Balance;
    let action = model.ActionType;
    if (action == 1) {
      return (
        <Chip
          color='error'
          label={`+${balance}`}
          variant='outlined'
          size='small'
        ></Chip>
      );
    } else if (action == -1) {
      return (
        <Chip
          color='success'
          label={`-${balance}`}
          variant='outlined'
          size='small'
        ></Chip>
      );
    } else {
      return <Chip label={balance} variant='outlined' size='small'></Chip>;
    }
  };

  return (
    <>
      <Box
        sx={{
          p: 2,
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box>{renderBalance()}</Box>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'end',
            }}
          >
            <Typography variant='caption' color={'text.disabled'} component={'div'}>
              {formatRelativeTimeFromNow(model.CreationTime)}
            </Typography>
            {isStringEmpty(model.Message) || (
              <Box sx={{ py: 1 }}>
                <Typography
                  variant='overline'
                  color='primary'
                  component={'div'}
                >
                  {model.Message}
                </Typography>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
      <Divider />
    </>
  );
};
