import {
  Button,
  Card,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Space,
  Switch,
} from 'antd';
import { useEffect, useState } from 'react';

import { QueryStoreGoodsMappingPagingInput } from '@/utils/swagger';

export default ({
  query,
  onSearch,
}: {
  query: QueryStoreGoodsMappingPagingInput;
  onSearch: (d: QueryStoreGoodsMappingPagingInput) => void;
}) => {
  const [form, _form] = useState<QueryStoreGoodsMappingPagingInput>({});

  useEffect(() => {
    _form({
      ...form,
      ...query,
    });
  }, [query]);

  return (
    <>
      <Card
        bordered={false}
        style={{
          marginBottom: 10,
        }}
      >
        <Form
          onFinish={() => {
            onSearch({
              ...form,
              Page: 1,
            });
          }}
        >
          <Row gutter={10}>
            <Col span={6}>
              <Form.Item label={'Sku'}>
                <Input
                  value={form.Sku || ''}
                  onChange={(e) => {
                    _form({
                      ...form,
                      Sku: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label={'关键词'}>
                <Input
                  value={form.Keywords || ''}
                  onChange={(e) => {
                    _form({
                      ...form,
                      Keywords: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label={'可用状态'}>
                <Switch
                  checked={form.Active || false}
                  onChange={(e) => {
                    _form({
                      ...form,
                      Active: e,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label={'最低价'}>
                <Space direction="horizontal" split={<b>~</b>}>
                  <InputNumber
                    value={form.MinPrice}
                    onChange={(e) => {
                      _form({
                        ...form,
                        MinPrice: e,
                      });
                    }}
                  />
                  <InputNumber
                    value={form.MaxPrice}
                    onChange={(e) => {
                      _form({
                        ...form,
                        MaxPrice: e,
                      });
                    }}
                  />
                </Space>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label={'最低库存'}>
                <Space direction="horizontal" split={<b>~</b>}>
                  <InputNumber
                    value={form.MinStockQuantity}
                    onChange={(e) => {
                      _form({
                        ...form,
                        MinStockQuantity: e,
                      });
                    }}
                  />
                  <InputNumber
                    value={form.MaxStockQuantity}
                    onChange={(e) => {
                      _form({
                        ...form,
                        MaxStockQuantity: e,
                      });
                    }}
                  />
                </Space>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item>
                <Button htmlType={'submit'} type={'primary'}>
                  搜索
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
    </>
  );
};
