import { Tabs } from 'antd';
import { useState } from 'react';
import { useParams } from 'umi';

import {
  CrownOutlined,
  ShoppingOutlined,
  UserOutlined,
} from '@ant-design/icons';

import XGrade from './grade';
import XStoreUser from './store_user';
import XUser from './user';

export default () => {
  const param = useParams<{ id?: string }>();

  const [selected, _selected] = useState<string | null | undefined>(param.id);

  return (
    <>
      <Tabs
        destroyInactiveTabPane
        activeKey={selected || 'user'}
        onChange={(e) => _selected(e)}
        items={[
          {
            key: 'user',
            label: '普通用户',
            children: <XUser />,
            icon: <UserOutlined />,
          },
          {
            key: 'store-user',
            label: '商城用户',
            children: <XStoreUser />,
            icon: <ShoppingOutlined />,
          },
          {
            key: 'grade',
            label: '用户等级',
            children: <XGrade />,
            icon: <CrownOutlined />,
          },
        ]}
      />
    </>
  );
};
