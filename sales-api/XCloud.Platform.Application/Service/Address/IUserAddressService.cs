﻿using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Core.Helper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Domain.Address;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Address;

public interface IUserAddressService : IXCloudAppService
{
    Task<UserAddressDto[]> AttachErrorsAsync(UserAddressDto[] data);

    Task<UserAddressDto[]> QueryByIdsAsync(string[] ids);

    Task<UserAddress> InsertAsync(UserAddressDto userAddress);

    Task UpdateAsync(UserAddressDto userAddress);

    Task SetAsDefaultAsync(string userId, string addressId);

    Task<UserAddressDto[]> QueryByUserIdAsync(string userId);

    Task SoftDeleteByIdAsync(string id);

    Task<UserAddressDto> QueryByIdAsync(string id);
}

public class UserAddressService : PlatformAppService, IUserAddressService
{
    private readonly IPlatformRepository<UserAddress> _userAddressRepository;

    public UserAddressService(IPlatformRepository<UserAddress> userAddressRepository)
    {
        _userAddressRepository = userAddressRepository;
    }

    private int MaxCount => 100;

    public async Task<UserAddressDto[]> AttachErrorsAsync(UserAddressDto[] data)
    {
        if (data.Any())
        {
            await Task.CompletedTask;

            foreach (var m in data)
            {
                if (string.IsNullOrWhiteSpace(m.ProvinceCode))
                {
                    m.ErrorList.Add("province is required");
                }

                if (string.IsNullOrWhiteSpace(m.CityCode))
                {
                    m.ErrorList.Add("city is required");
                }

                if (string.IsNullOrWhiteSpace(m.AreaCode))
                {
                    m.ErrorList.Add("area is required");
                }

                if (string.IsNullOrWhiteSpace(m.AddressDetail))
                {
                    m.ErrorList.Add("detail address is required");
                }

                if (string.IsNullOrWhiteSpace(m.Tel))
                {
                    m.ErrorList.Add("mobile phone number is required");
                }

                if (!m.HasGeoLocation())
                {
                    m.ErrorList.Add("geo location is required");
                }
            }
        }

        return data;
    }

    public async Task<UserAddressDto[]> QueryByIdsAsync(string[] ids)
    {
        if (ValidateHelper.IsEmptyCollection(ids))
            return Array.Empty<UserAddressDto>();

        var data = await _userAddressRepository.QueryManyAsync(x => ids.Contains(x.Id));

        return data.MapArrayTo<UserAddress, UserAddressDto>(ObjectMapper);
    }

    public async Task<UserAddress> InsertAsync(UserAddressDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.UserId))
            throw new ArgumentNullException(nameof(dto.UserId));

        if (await this._userAddressRepository.CountAsync(x => x.UserId == dto.UserId) >= this.MaxCount)
        {
            throw new UserFriendlyException($"最多可以添加{this.MaxCount}个地址");
        }

        var userAddress = dto.MapTo<UserAddressDto, UserAddress>(ObjectMapper);

        userAddress.Id = GuidGenerator.CreateGuidString();
        userAddress.CreationTime = Clock.Now;

        await _userAddressRepository.InsertAsync(userAddress);

        return userAddress;
    }

    public async Task UpdateAsync(UserAddressDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var address = await QueryByIdAsync(dto.Id);

        if (address != null)
        {
            //hide previous address
            await SoftDeleteByIdAsync(address.Id);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        //insert new one
        var userAddress = dto.MapTo<UserAddressDto, UserAddress>(ObjectMapper);
        userAddress.Id = default;
        userAddress = await InsertAsync(userAddress.MapTo<UserAddress, UserAddressDto>(ObjectMapper));

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        //set default if previous address is the default 
        if (userAddress.IsDefault)
        {
            await SetAsDefaultAsync(userAddress.UserId, userAddress.Id);
        }
    }

    public async Task SetAsDefaultAsync(string userId, string addressId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        if (string.IsNullOrWhiteSpace(addressId))
            throw new ArgumentNullException(nameof(addressId));

        var db = await _userAddressRepository.GetDbContextAsync();

        var set = db.Set<UserAddress>();

        //remove previous default address
        var commonList = await set.Where(x => x.UserId == userId && x.Id != addressId && x.IsDefault).ToListAsync();
        commonList.ForEach(x => x.IsDefault = false);

        //set new default address
        var theOne = await set.Where(x => x.UserId == userId && x.Id == addressId).FirstOrDefaultAsync();
        if (theOne != null)
        {
            theOne.IsDefault = true;
        }

        await db.TrySaveChangesAsync();
    }

    public async Task<UserAddressDto[]> QueryByUserIdAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var db = await this._userAddressRepository.GetDbContextAsync();

        var allAddress = await db.Set<UserAddress>().AsNoTracking()
            .Where(x => x.UserId == userId)
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .Take(this.MaxCount)
            .ToArrayAsync();

        return allAddress.MapArrayTo<UserAddress, UserAddressDto>(ObjectMapper);
    }

    public async Task SoftDeleteByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var address = await _userAddressRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (address == null)
            return;

        address.IsDeleted = true;
        address.DeletionTime = Clock.Now;

        await _userAddressRepository.UpdateAsync(address);
    }

    public async Task<UserAddressDto> QueryByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var address = await _userAddressRepository.FirstOrDefaultAsync(x => x.Id == id);

        return address?.MapTo<UserAddress, UserAddressDto>(ObjectMapper);
    }
}