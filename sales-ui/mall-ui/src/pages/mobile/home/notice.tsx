import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isStringEmpty } from '@/utils/utils';
import { Alert, Box } from '@mui/material';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  if (isStringEmpty(app.mallSettings.HomePageNotice)) {
    return null;
  }

  return (
    <>
      <Box
        sx={{
          px: 1,
          mb: 3,
        }}
      >
        <Alert>{app.mallSettings.HomePageNotice}</Alert>
      </Box>
    </>
  );
};
