﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Utils;

namespace XCloud.Sales.Qingdao.Service;

public interface IQingdaoUserAccountService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<UserDto> PrepareUserAsync(WapAdminDto admin);

    [NotNull]
    [ItemNotNull]
    Task<UserDto> PrepareUserAsync(McsUserDto mcsUser);
}

public class QingdaoUserAccountService : SalesAppService, IQingdaoUserAccountService
{
    private readonly IUserAccountService _userAccountService;
    private readonly IUserService _userService;
    private readonly QingdaoUtils _qingdaoUtils;

    public QingdaoUserAccountService(
        IUserAccountService userAccountService,
        IUserService userService,
        QingdaoUtils qingdaoUtils)
    {
        _userAccountService = userAccountService;
        _userService = userService;
        _qingdaoUtils = qingdaoUtils;
    }

    public async Task<UserDto> PrepareUserAsync(WapAdminDto admin)
    {
        var user = await _userAccountService.GetOrCreateByExternalIdAsync(_qingdaoUtils.BuildAdminExternalId(admin));

        if (user.NickName != admin.userName && !string.IsNullOrWhiteSpace(admin.userName))
        {
            user.NickName = admin.userName;
            
            await this._userService.UpdateProfileAsync(user);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        if (user.IsActive != admin.active)
        {
            await this._userAccountService.UpdateStatusAsync(new UpdateUserStatusDto()
            {
                Id = user.Id,
                IsActive = admin.active
            });

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return user;
    }

    public async Task<UserDto> PrepareUserAsync(McsUserDto mcsUser)
    {
        var user = await _userAccountService.GetOrCreateByExternalIdAsync(
            _qingdaoUtils.BuildMcsUserExternalId(mcsUser));

        var change = false;
        if (user.NickName != mcsUser.nickName && !string.IsNullOrWhiteSpace(mcsUser.nickName))
        {
            user.NickName = mcsUser.nickName;
            change = true;
        }

        if (user.Avatar != mcsUser.headImgUrl && !string.IsNullOrWhiteSpace(mcsUser.headImgUrl))
        {
            user.Avatar = mcsUser.headImgUrl;
            change = true;
        }

        if (change)
        {
            await this._userService.UpdateProfileAsync(user);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return user;
    }
}