﻿using System.IO;
using System.Threading.Tasks;
using Volo.Abp;

namespace XCloud.Core.Extension;

public static class IoExtension
{
    /// <summary>
    /// 创建目录
    /// </summary>
    /// <param name="dir"></param>
    public static void CreateIfNotExist(this DirectoryInfo dir)
    {
        if (!dir.Exists)
        {
            dir.Create();
        }
    }

    public static void DeleteIfExist(this FileInfo f)
    {
        if (f.Exists)
        {
            f.Delete();
        }
    }

    public static async Task<byte[]> GetStreamBytesAsync(this Stream stream, int? maxLength = default)
    {
        using var ms = new MemoryStream();

        var buffer = new byte[1024 * 2];
        var len = default(int);

        while (true)
        {
            var readLen = await stream.ReadAsync(buffer, 0, buffer.Length);
            if (readLen <= 0)
                break;
            len += readLen;

            if (maxLength != null && len > maxLength.Value)
                throw new BusinessException(message: $"{nameof(GetStreamBytesAsync)}-data too large");

            await ms.WriteAsync(buffer, 0, readLen);
        }

        return ms.ToArray();
    }
}