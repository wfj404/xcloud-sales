import { Button, Dropdown, MenuProps } from 'antd';
import { pullAt } from 'lodash-es';
import { ReactElement, useState } from 'react';

import XAddComponentModal from '@/components/blocks/design/menu_modal';
import { BlockItem, BlockItemProps } from '@/components/blocks/utils';
import { insertAt, swapArrayPosition } from '@/utils/utils';
import {
  DeleteOutlined,
  DownOutlined,
  MenuOutlined,
  UpOutlined,
  VerticalAlignBottomOutlined,
  VerticalAlignTopOutlined,
} from '@ant-design/icons';
import { Box } from '@mui/material';

export default ({
                  data,
                  i,
                  children,
                  blocks,
                  onBlocksChange,
                  onMaskClick,
                  selected,
                  onInsert,
                }: {
  data: BlockItem<BlockItemProps>;
  i: number;
  blocks: BlockItem<BlockItemProps>[];
  onBlocksChange: (x: BlockItem<BlockItemProps>[]) => void;
  onMaskClick: () => void;
  selected?: boolean;
  children: ReactElement;
  onInsert?: (x: number) => void;
}) => {
  const [mouseEnter, _mouseEnter] = useState(false);
  const [insertIndex, _insertIndex] = useState(0);
  const [showInsert, _showInsert] = useState(false);

  const focus = selected || mouseEnter;

  const renderMask = () => {
    return (
      <div
        onClick={() => onMaskClick()}
        className={'blur-sm'}
        style={{
          backgroundColor: 'rgba(250,250,250,0.4)',
          zIndex: 10,
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }}
      ></div>
    );
  };

  const items: MenuProps['items'] = [
    {
      key: 'up',
      label: '上移',
      icon: <UpOutlined />,
      onClick: () => {
        const arr = [...blocks];
        swapArrayPosition(arr, i - 1, i);
        onBlocksChange(arr);
      },
    },
    {
      key: 'down',
      label: '下移',
      icon: <DownOutlined />,
      onClick: () => {
        const arr = [...blocks];
        swapArrayPosition(arr, i, i + 1);
        onBlocksChange(arr);
      },
    },
    {
      key: 'insert-above',
      label: '添加到上面',
      icon: <VerticalAlignTopOutlined />,
      onClick: () => {
        _insertIndex(Math.max(0, i));
        _showInsert(true);
      },
    },
    {
      key: 'insert-below',
      label: '添加到下面',
      icon: <VerticalAlignBottomOutlined />,
      onClick: () => {
        _insertIndex(Math.max(0, i + 1));
        _showInsert(true);
      },
    },
    {
      type: 'divider',
    },
    {
      key: 'delete',
      label: '删除',
      icon: <DeleteOutlined />,
      danger: true,
      onClick: () => {
        if (!confirm('确定删除该组件？')) {
          return;
        }
        const datalist = [...blocks];
        pullAt(datalist, i);
        console.log(data, datalist);
        onBlocksChange(datalist);
      },
    },
  ];

  const renderToolbar = () => {
    return (
      <Box
        sx={{
          zIndex: 1000,
          position: 'absolute',
          top: 5,
          right: 5,
          display: 'inline-block',
          //backgroundColor: 'rgb(250,250,250)',
        }}
      >
        <Dropdown menu={{ items }} arrow trigger={['hover']}>
          <Button
            icon={<MenuOutlined />}
            type={'primary'}
            shape={'circle'}
          ></Button>
        </Dropdown>
      </Box>
    );
  };

  return (
    <>
      <XAddComponentModal
        show={showInsert}
        onClose={() => _showInsert(false)}
        onBlockSelect={(x) => {
          _showInsert(false);
          let datalist = [...blocks];
          datalist = insertAt(datalist, x, insertIndex);
          onBlocksChange(datalist);
          setTimeout(() => {
            onInsert && onInsert(insertIndex);
          }, 1000);
        }}
      />
      <Box
        sx={{
          position: 'relative',
          display: 'block',
          border: focus ? '4px dashed orange' : 'none',
        }}
        onMouseEnter={(x) => {
          _mouseEnter(true);
        }}
        onMouseLeave={(x) => {
          _mouseEnter(false);
        }}
      >
        {focus && (
          <>
            {renderMask()}
            {renderToolbar()}
          </>
        )}
        <div
          style={{
            background: 'none',
            zIndex: 1,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
        ></div>
        <Box
          sx={{
            minHeight: 20,
          }}
        >
          {children}
        </Box>
      </Box>
    </>
  );
};
