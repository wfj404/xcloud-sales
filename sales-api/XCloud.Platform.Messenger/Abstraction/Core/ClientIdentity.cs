﻿using System;
using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Messenger.Abstraction.Core;

public class ClientIdentity : IEntityDto
{
    public string SubjectId { get; set; }
    public string DeviceType { get; set; }
    public string ConnectionId { get; set; }

    public DateTime PingTime { get; set; }

    public bool IsAuthed => !string.IsNullOrWhiteSpace(this.SubjectId);
}