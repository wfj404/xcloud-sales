import { ErrorBlock } from 'antd-mobile';
import { CSSProperties, ReactNode, useState } from 'react';

import { UserAddressDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { AddOutlined, RefreshOutlined } from '@mui/icons-material';
import { Box, Button, IconButton, Stack } from '@mui/material';

import XForm from './form';
import XItem from './item';

export default function ({
  addressList,
  onClick,
  onRefresh,
  header,
  itemStyle,
}: {
  addressList: UserAddressDto[];
  onRefresh?: () => void;
  onClick?: (e: UserAddressDto) => void;
  header?: ReactNode;
  itemStyle?: CSSProperties;
}) {
  const [formData, _formData] = useState<UserAddressDto | undefined>(undefined);

  const renderList = () => {
    if (isArrayEmpty(addressList)) {
      return <ErrorBlock status="empty" />;
    }
    return addressList?.map((x, index) => (
      <div
        key={index}
        style={{ marginBottom: 10 }}
        onClick={() => {
          onClick && onClick(x);
        }}
      >
        <XItem
          item={x}
          style={itemStyle}
          onClick={() => {
            _formData(x);
          }}
        />
      </div>
    ));
  };

  return (
    <>
      <XForm
        data={formData || {}}
        open={formData != undefined}
        onClose={() => {
          _formData(undefined);
        }}
        onOk={() => {
          _formData(undefined);
          onRefresh && onRefresh();
        }}
      />
      {header}
      <Box sx={{ mb: 2 }}>{renderList()}</Box>
      <Box
        sx={{
          my: 2,
        }}
      >
        <Stack
          direction={'row'}
          alignItems={'center'}
          justifyContent={'flex-start'}
          spacing={1}
        >
          <IconButton
            onClick={() => {
              onRefresh && onRefresh();
            }}
          >
            <RefreshOutlined />
          </IconButton>
          <Button
            disabled={addressList.length >= 50}
            startIcon={<AddOutlined />}
            variant="outlined"
            fullWidth
            onClick={() => {
              _formData({});
            }}
          >
            新增地址
          </Button>
        </Stack>
      </Box>
    </>
  );
}
