import { isString } from 'lodash-es';

import { config } from '@/utils/config';
import { StorageMetaDto } from '@/utils/swagger';
import {
  concatUrl,
  isBase64,
  isImageUrl,
  isStringEmpty,
  isUrl,
  parseJsonOrEmpty,
} from '@/utils/utils';

export interface ResolveUrlOption {
  width?: number;
  height?: number;
}

const resolveObjectUrl = function (
  data: StorageMetaDto,
  option?: ResolveUrlOption,
): string | null {
  if (!data) {
    return null;
  }

  if (isStringEmpty(data.ResourceKey) || isStringEmpty(data.StorageProvider)) {
    return null;
  }

  /*
   * 目前后端只提供一种storage provider，因此这里的判断其实没有必要。
   * 这里提供一种方式，用来在多provider的情况下扩展
   * */

  if (data.StorageProvider == 'abp-fs-blob-provider-1') {
    //resolve abp fs url
  }

  if (data.StorageProvider == 'qcloud') {
    //resolve qcloud url
  }

  //这里是默认resolver
  let resizeOption = `${option?.width || 0}x${option?.height || 0}`;
  let path = `/api/platform/storage/file/${resizeOption}/`;
  return concatUrl([config.apiGateway, path, data.ResourceKey || '']);
};

const resizeExternalUrl = (
  externalUrl?: string | null,
  option?: ResolveUrlOption,
): string | null => {
  if (!externalUrl) {
    return externalUrl || null;
  }

  if (!isImageUrl(externalUrl)) {
    return externalUrl;
  }

  const width = option?.width || 0;
  const height = option?.height || 0;

  if (width <= 0 && height <= 0) {
    return externalUrl;
  }

  const currentDomain = config.domain || '';
  const resizeEndpointPart = 'sales-url-resize/unsafe';

  const black_list = [currentDomain, resizeEndpointPart];

  if (black_list.some((x) => externalUrl.indexOf(x.toLowerCase()) >= 0)) {
    console.log('不符合裁剪要求：', externalUrl);
    return externalUrl;
  }

  let resizeOption = `${width}x${height}`;

  return concatUrl([
    config.apiGateway,
    '/api/platform/storage',
    resizeEndpointPart,
    encodeURIComponent(externalUrl),
    resizeOption,
    'resize.png',
  ]);
};

const resolveStringUrl = (
  data?: string | null,
  option?: ResolveUrlOption,
): string | null => {
  if (!data || isStringEmpty(data)) {
    return null;
  }

  data = data.trim();

  if (
    data.toLowerCase().startsWith('https://') ||
    (data.toLowerCase().startsWith('http://') && isUrl(data))
  ) {
    //return origin url
    //return data;
    return resizeExternalUrl(data, option);
  } else if (data.startsWith('{') && data.endsWith('}')) {
    //parse json as meta object
    return resolveObjectUrl(parseJsonOrEmpty(data) || {}, option);
  } else if (data.startsWith('data:') || isBase64(data)) {
    return data;
  } else {
    //data is relative url path
    return concatUrl([config.apiGateway, data]);
  }
};

export const resolveUrl = function (
  data?: string | null | StorageMetaDto,
  option?: ResolveUrlOption,
): string | null {
  if (!data) {
    return null;
  }

  if (isString(data)) {
    return resolveStringUrl(data, option);
  } else {
    return resolveObjectUrl(data, option);
  }
};

export const resolveAvatar = (
  avatar?: string | null | StorageMetaDto,
  option?: ResolveUrlOption,
): string => {
  let url = resolveUrl(avatar, option);
  if (url) {
    return url;
  }

  return concatUrl([config.apiGateway, '/api/platform/user/random-avatar']);
};
