﻿using XCloud.Application.Model;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Roles;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/manage-admin")]
public class ManageAdminController : PlatformBaseController
{
    private readonly IAdminService _adminService;
    private readonly IAdminAccountService _adminAccountService;
    private readonly IAdminRoleService _adminRoleService;
    private readonly IAdminSecurityService _adminSecurityService;
    private readonly IRoleService _roleService;

    public ManageAdminController(
        IAdminService userService,
        IAdminRoleService adminRoleService,
        IAdminSecurityService adminSecurityService,
        IRoleService roleService,
        IAdminAccountService adminAccountService)
    {
        _adminService = userService;
        _adminRoleService = adminRoleService;
        _adminSecurityService = adminSecurityService;
        _roleService = roleService;
        _adminAccountService = adminAccountService;
    }

    [HttpPost("permissions")]
    public async Task<AdminGrantedPermissionResponse> QueryAdminPermissionsAsync()
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var response = await _adminSecurityService.GetGrantedPermissionsAsync(
            loginAdmin.Id,
            new CacheStrategy { Cache = true });

        return response;
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<AdminDto>> QueryAdminPagingAsync([FromBody] QueryAdminPaging dto)
    {
        dto.PageSize = 20;

        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._adminSecurityService.CheckRequiredSuperAdminAsync(loginAdmin.Id);

        var data = await _adminService.QueryAdminPagingAsync(dto);

        if (!data.IsNotEmpty)
            return data;

        await _adminService.AttachUsersAsync(data.Items.ToArray());

        await _adminRoleService.AttachRolesAsync(data.Items.ToArray());

        var roles = data.Items.SelectMany(x => x.Roles).ToArray();

        await _roleService.AttachPermissionAsync(roles);

        return data;
    }

    [HttpPost("set-as-admin")]
    public async Task<ApiResponse<XCloud.Platform.Application.Domain.Admins.Admin>> SetAsAdminAsync(
        [FromBody] SetAsAdminDto dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._adminSecurityService.CheckRequiredSuperAdminAsync(loginAdmin.Id);

        var admin = await _adminAccountService.GetOrCreateByUserIdAsync(dto.UserId);

        return new ApiResponse<XCloud.Platform.Application.Domain.Admins.Admin>(admin);
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateAdminStatusDto dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._adminSecurityService.CheckRequiredSuperAdminAsync(loginAdmin.Id);

        await _adminAccountService.UpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }
}