﻿using System;
using FluentMigrator.Runner;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Volo.Abp.Modularity;
using XCloud.Core;
using XCloud.Core.Extension;
using XCloud.Sales.Application.Core;

namespace XCloud.Sales.Application.Extended.DatabaseMigrator;

public static class FluentMigratorExtension
{
    public static void AddDatabaseMigrator(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        var connectionString =
            config.GetRequiredConnectionString(SalesConstants.DbConnectionNames.MySql);

        context.Services.AddFluentMigratorCore().ConfigureRunner(x =>
        {
            x.AddMySql8();
            x.WithGlobalCommandTimeout(TimeSpan.FromSeconds(30));
            x.WithGlobalConnectionString(connectionString);
            x.ScanIn(typeof(FluentMigratorExtension).Assembly);
        });

        //add migration logging
        context.Services.AddLogging(builder => builder.AddFluentMigratorConsole());
    }

    public static void ExecuteDatabaseMigrator(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var logger = s.ServiceProvider.GetRequiredService<ILoggerFactory>()
            .CreateLogger(nameof(ExecuteDatabaseMigrator));

        var env = s.ServiceProvider.GetRequiredService<IWebHostEnvironment>();

        if (env.IsProduction())
        {
            var config = s.ServiceProvider.GetRequiredService<IConfiguration>();

            var skipInProduction = config["app:config:skip_migration_in_production"]?.ToBool() ?? false;

            if (skipInProduction)
            {
                logger.LogInformation("skip database migration in production-deployment");
                return;
            }
        }

        var runner = s.ServiceProvider.GetRequiredService<IMigrationRunner>();

        logger.LogWarning(message: "starting execute database migration");
        runner.MigrateUp();
        logger.LogWarning(message: "finish execute database migration");
    }
}