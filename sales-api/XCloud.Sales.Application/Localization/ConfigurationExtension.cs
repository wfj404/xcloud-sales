using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;
using XCloud.Platform.Application.Localization;

namespace XCloud.Sales.Application.Localization;

public static class ConfigurationExtension
{
    public static void ConfigSalesLocalization(this ServiceConfigurationContext context)
    {
        context.Services.Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<SalesApplicationModule>(
                baseNamespace: typeof(SalesApplicationModule).Namespace);
        });

        context.Services.Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<SalesResource>(defaultCultureName: "zh-Hans")
                .AddVirtualJson("/Localization/Resources/Sales")
                .AddBaseTypes(typeof(PlatformResource), typeof(AbpValidationResource));
        });
    }
}