﻿using System;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;

namespace XCloud.Sales.ThirdParty.Services.YiLianYun;

public static class YiLianYunExtension
{
    internal static void StartYiLianYunJobs(this IServiceProvider serviceProvider)
    {
        var jobManager = serviceProvider.GetRequiredService<IRecurringJobManager>();

        jobManager.AddOrUpdate<YiLianYunJobs>("refresh-yi-lian-yun-access-token",
            x => x.RefreshAccessTokenAsync(),
            Cron.Daily);
    }

    public static void ConfigYiLianYun(this IServiceCollection serviceCollection)
    {
        //
    }
}