using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Mapping;
using XCloud.Platform.Messenger.Domain.Messenger;

namespace XCloud.Platform.Messenger.Mapping.Messenger;

public class OnlineStatusMap : PlatformEntityTypeConfiguration<OnlineStatus>
{
    public override void Configure(EntityTypeBuilder<OnlineStatus> builder)
    {
        base.Configure(builder);

        builder.ToTable("sys_im_online_status");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.UserId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.DeviceId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.ServerInstanceId).IsRequired().HasMaxLength(100);

        builder.Property(x => x.PingTime);
    }
}