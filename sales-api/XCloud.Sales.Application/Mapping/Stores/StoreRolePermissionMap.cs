﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Mapping.Stores;

public class StoreRolePermissionMap : SalesEntityTypeConfiguration<StoreRolePermission>
{
    public override void Configure(EntityTypeBuilder<StoreRolePermission> builder)
    {
        builder.ToTable("sales_store_role_permission");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.RoleId).RequiredId();
        builder.Property(x => x.PermissionKey).RequiredId();

        base.Configure(builder);
    }
}