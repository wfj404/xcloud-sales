import { useLocalStorageState } from 'ahooks';
import { ErrorBlock, IndexBar, List } from 'antd-mobile';
import { groupBy } from 'lodash-es';
import { useEffect, useMemo, useState } from 'react';
import { useSnapshot } from 'umi';

import XBacktotop from '@/components/common/back_to_top';
import XImage from '@/components/common/image';
import DotsLoading from '@/components/common/loading/dots';
import XHeader from '@/components/mobile/simpleHeader';
import { storeState, StoreType } from '@/store';
import { redirectToGoodsSearch } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { resolveUrl } from '@/utils/storage';
import { BrandDto } from '@/utils/swagger';
import {
  getPinyinFirstLetter,
  isAlphabet,
  isArrayEmpty,
  parseJsonOrEmpty,
} from '@/utils/utils';
import { Container } from '@mui/material';

interface BrandGroup {
  title: string;
  brands: BrandDto[];
}

export default function () {
  const app = useSnapshot(storeState) as StoreType;

  const [cachedData, _cachedData] = useLocalStorageState<BrandDto[]>(
    'brand.list.data',
    {
      serializer: (x) => JSON.stringify(x || []),
      deserializer: (x) => parseJsonOrEmpty(x),
      defaultValue: undefined,
    },
  );
  const data = cachedData || [];

  const [loading, _loading] = useState(false);
  const finalLoading = cachedData == undefined && loading;

  const queryList = () => {
    _loading(true);
    apiClient.mall
      .brandAll({})
      .then((res) => {
        _cachedData(res.data.Data || []);
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList();
  }, []);

  const compareGroupKey = (a: string, b: string): number => {
    const aIsAlphabet = isAlphabet(a);
    const bIsAlphabet = isAlphabet(b);

    if (aIsAlphabet && !bIsAlphabet) {
      return -1;
    }

    if (!aIsAlphabet && bIsAlphabet) {
      return 1;
    }

    return a.localeCompare(b);
  };

  const groups = useMemo<BrandGroup[]>(() => {
    const grouping = groupBy(
      data,
      (x) => getPinyinFirstLetter(x.Name)?.toUpperCase() || '#',
    );

    const keys = Object.keys(grouping).sort((a, b) => compareGroupKey(a, b));

    return keys.map<BrandGroup>((x) => ({
      title: x,
      brands:
        grouping[x]?.sort(
          (a, b) => a?.Name?.localeCompare(b?.Name || '') || 0,
        ) || [],
    }));
  }, [data]);

  const renderPicture = (x: BrandDto) => {
    const picUrl = resolveUrl(x.Picture, {
      width: 50,
      height: 50,
    });
    if (!picUrl) {
      return null;
    }
    return (
      <div style={{ padding: 5 }}>
        <XImage
          rate={1}
          style={{ borderRadius: 4 }}
          src={picUrl}
          width={40}
          height={40}
          lazy
          fit="cover"
          alt={x.Name || ''}
        />
      </div>
    );
  };

  const renderBrandList = (items: BrandDto[], arrow?: boolean) => {
    if (isArrayEmpty(items)) {
      return null;
    }
    return (
      <List>
        {items.map((x, i) => (
          <List.Item
            key={`item-${i}`}
            extra={renderPicture(x)}
            arrow={arrow}
            onClick={() => {
              redirectToGoodsSearch({ BrandId: x.Id });
            }}
          >
            {x.Name}
          </List.Item>
        ))}
      </List>
    );
  };

  const renderList = () => {
    if (finalLoading) {
      return <DotsLoading />;
    }
    if (isArrayEmpty(groups)) {
      return <ErrorBlock status="empty" />;
    }
    return (
      <div style={{}}>
        {groups.map((group, index) => {
          return (
            <div key={`group-${index}`} style={{}}>
              <div
                style={{
                  backgroundColor: '#f5f5f5',
                  color: '#999999',
                  padding: 12,
                  paddingTop: 8,
                  paddingBottom: 8,
                  fontSize: '12px',
                }}
              >{`${group.title}`}</div>
              {renderBrandList(group.brands, true)}
            </div>
          );
        })}
      </div>
    );
  };

  const renderListV2 = () => {
    if (finalLoading) {
      return <DotsLoading />;
    }
    if (isArrayEmpty(groups)) {
      return <ErrorBlock status="empty" />;
    }
    return (
      <IndexBar
        style={{
          height: '100%',
        }}
      >
        {groups.map((group, index) => {
          return (
            <IndexBar.Panel
              key={`group-${index}`}
              index={`${group.title}`}
              title={`${group.title}`}
            >
              {renderBrandList(group.brands, false)}
            </IndexBar.Panel>
          );
        })}
      </IndexBar>
    );
  };

  return (
    <>
      <XHeader>
        <XBacktotop />
        <Container disableGutters maxWidth="sm">
          {renderList()}
        </Container>
      </XHeader>
    </>
  );
}
