using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Warehouses.Service.Warehouses;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/warehouse")]
public class WarehouseController: ShopBaseController
{
    private readonly IWarehouseService _warehouseService;

    public WarehouseController(IWarehouseService warehouseService)
    {
        _warehouseService = warehouseService;
    }
    
    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] WarehouseDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _warehouseService.InsertAsync(dto);
        }
        else
        {
            await _warehouseService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateWarehouseStatusInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        await _warehouseService.UpdateStatusAsync(dto);
        
        return new ApiResponse<object>();
    }

    [HttpPost("all")]
    public async Task<ApiResponse<WarehouseDto[]>> QueryAllAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        var response = await _warehouseService.QueryAllAsync();

        return new ApiResponse<WarehouseDto[]>(response);
    }
}