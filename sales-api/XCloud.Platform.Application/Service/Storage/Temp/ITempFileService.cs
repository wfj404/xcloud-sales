﻿using System.IO;
using System.Security;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Core.Configuration;

namespace XCloud.Platform.Application.Service.Storage.Temp;

public interface ITempFileService : IXCloudAppService
{
    Task<ITempFile> CreateTempFileAsync(Stream stream);
}

[ExposeServices(typeof(ITempFileService))]
public class TempFileService : PlatformAppService, ITempFileService
{
    private readonly IServiceProvider _serviceProvider;

    public TempFileService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task<ITempFile> CreateTempFileAsync(Stream stream)
    {
        var tempDir = await EnsureTempDirAsync();

        var tempFileName = Path.Combine(tempDir, $"{GuidGenerator.CreateGuidString()}.tmp");

        if (File.Exists(tempFileName))
        {
            throw new BusinessException(message: "temp file is already exist,pls try again").WithData(
                nameof(tempFileName), tempFileName);
        }

        await using var fileStream = new FileStream(tempFileName, FileMode.OpenOrCreate);

        await stream.CopyToAsync(fileStream);
        await fileStream.FlushAsync();

        return new LocalDiskTempFile(tempFileName);
    }

    private async Task<string> GetRequiredTempDirAsync()
    {
        try
        {
            return Path.GetTempPath();
        }
        catch (SecurityException e)
        {
            await Task.CompletedTask;

            this.Logger.LogWarning(message: e.Message, exception: e);

            var webHostEnvironment = this._serviceProvider.GetRequiredService<IWebHostEnvironment>();
            return Path.Combine(webHostEnvironment.ContentRootPath, "temp");
        }
    }

    private async Task<string> EnsureTempDirAsync()
    {
        var tempDir = await this.GetRequiredTempDirAsync();

        var appName = this._serviceProvider.GetAppName();

        if (!string.IsNullOrWhiteSpace(appName))
        {
            tempDir = Path.Combine(tempDir, appName);
        }

        //new DirectoryInfo(tempDir).CreateIfNotExist();

        //yyyy/MM/dd hh:mm:ss
        var time = Clock.Now.ToString("yyyyMMddhh");

        tempDir = Path.Combine(tempDir, $"temp-{time}");

        new DirectoryInfo(tempDir).CreateIfNotExist();

        return tempDir;
    }
}