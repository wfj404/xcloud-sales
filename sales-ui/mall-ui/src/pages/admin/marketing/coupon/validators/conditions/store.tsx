import { Button, Form, Modal, Select } from 'antd';
import { useEffect, useState } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { OrderStoreConditionParameter } from '@/utils/swagger';

export default ({
  rule,
  ok,
}: {
  rule: OrderStoreConditionParameter;
  ok: (d: OrderStoreConditionParameter) => void;
}) => {
  const [settings, _settings] = useState<OrderStoreConditionParameter>({});
  const [show, _show] = useState(false);

  const app = useSnapshot(storeState) as StoreType;

  useEffect(() => {
    _settings({
      ...rule,
    });
  }, [rule]);

  useEffect(() => {
    if (show) {
      app.queryStores();
    }
  }, [show]);

  return (
    <>
      <Button
        block
        onClick={() => {
          _show(true);
        }}
      >{`限定门店(${
        rule.SupportedStores?.map((x) => x.Name).join(',') || '--'
      })`}</Button>

      <Modal
        title={'限定门店可用'}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        onOk={() => {
          ok({
            ...settings,
          });
          _show(false);
        }}
      >
        <Form>
          <Form.Item label={'门店'}>
            <Select
              mode={'multiple'}
              maxTagCount={100}
              onChange={(e) => {
                _settings({
                  ...settings,
                  SupportedStores: app.stores.filter(
                    (x) => e.indexOf(x.Id) >= 0,
                  ),
                });
              }}
              options={app.stores.map((x) => ({
                label: x.Name,
                value: x.Id || '',
              }))}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
