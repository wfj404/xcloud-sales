import { QueryBrandPaging } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input } from 'antd';
import { useEffect, useState } from 'react';

const App = (props: {
  query: QueryBrandPaging;
  onSearch: (d: QueryBrandPaging) => void;
}) => {
  const { query, onSearch } = props;

  const [model, _model] = useState<QueryBrandPaging>({});

  const triggerSearch = (e: QueryBrandPaging) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _model({
      ...(query || {}),
    });
  }, [query]);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        autoComplete="off"
        layout="inline"
      >
        <Form.Item label="关键词">
          <Input
            value={model.Name || ''}
            onChange={(e) => {
              _model({
                ...model,
                Name: e.target.value,
              });
            }}
          />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            icon={<SearchOutlined />}
            onClick={() => triggerSearch(model)}
          >
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default App;
