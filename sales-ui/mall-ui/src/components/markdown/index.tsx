import ReactMarkdown from 'react-markdown';
import { isStringEmpty } from '@/utils/utils';

export default function(props: { md: string }) {
  const { md } = props;

  if (isStringEmpty(md)) {
    return null;
  }
  return (
    <>
      <ReactMarkdown remarkPlugins={[]}>{md}</ReactMarkdown>
    </>
  );
}
