﻿using Microsoft.Extensions.DependencyInjection;

namespace XCloud.Platform.Messenger.Configuration;

public class MessengerBuilder
{
    public IServiceCollection Services { get; }

    public MessengerBuilder(IServiceCollection services)
    {
        Services = services;
    }
}