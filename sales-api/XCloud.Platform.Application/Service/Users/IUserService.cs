using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Users;

public interface IUserService : IPlatformCrudService<User, UserDto, QueryUserPaging>
{
    [ItemNotNull]
    Task<UserDto> EnsureUserAsync(string userId);

    Task UpdateProfileAsync(UserDto dto);
}

public class UserService : PlatformCrudService<User, UserDto, QueryUserPaging>, IUserService
{
    public UserService(IPlatformRepository<User> repository) : base(repository)
    {
        //
    }

    public async Task<UserDto> EnsureUserAsync(string userId)
    {
        var user = await this.GetByIdAsync(userId);

        if (user == null)
            throw new EntityNotFoundException(nameof(EnsureUserAsync));

        return user;
    }

    public virtual async Task UpdateProfileAsync(UserDto dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(UpdateProfileAsync));

        var user = await Repository.FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (user == null)
            throw new EntityNotFoundException($"用户不存在:{dto.Id}");

        user.SetEntityFields(new
        {
            dto.Avatar,
            dto.Gender,
            dto.NickName
        });

        user.LastModificationTime = Clock.Now;

        await Repository.UpdateAsync(user);
    }

    public override Task<User> InsertAsync(UserDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<User> UpdateAsync(UserDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task DeleteByIdAsync(string id)
    {
        throw new NotSupportedException();
    }

    protected override async Task<IQueryable<User>> GetPagingFilteredQueryableAsync(DbContext db, QueryUserPaging dto)
    {
        await Task.CompletedTask;

        var query = db.Set<User>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.Keyword))
        {
            var set = db.Set<UserIdentity>().AsNoTracking();

            var scope = new[]
            {
                (int)UserIdentityTypeEnum.Email,
                (int)UserIdentityTypeEnum.MobilePhone
            };

            var identityQuery = set.Where(x => scope.Contains(x.IdentityType))
                .Where(x => x.Identity.StartsWith(dto.Keyword));

            query = query.Where(x =>
                x.IdentityName == dto.Keyword || x.NickName == dto.Keyword ||
                identityQuery.Any(d => d.UserId == x.Id));
        }

        if (dto.IsActive != null)
            query = query.Where(x => x.IsActive == dto.IsActive.Value);

        return query;
    }

    protected override async Task<IOrderedQueryable<User>> GetPagingOrderedQueryableAsync(IQueryable<User> query,
        QueryUserPaging dto)
    {
        await Task.CompletedTask;

        return query
            .OrderByDescending(x => x.IsActive)
            .ThenByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id);
    }
}