﻿using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Uow;
using XCloud.Application.Utils;
using XCloud.AspNetMvc.Core;
using XCloud.Core.Json;

namespace XCloud.AspNetMvc.Controller;

public abstract class XCloudBaseController : AbpController
{
    [NotNull]
    protected IUnitOfWork RequiredCurrentUnitOfWork =>
        this.CurrentUnitOfWork ?? throw new BusinessException(nameof(RequiredCurrentUnitOfWork));

    protected IJsonDataSerializer JsonDataSerializer =>
        this.LazyServiceProvider.LazyGetRequiredService<IJsonDataSerializer>();

    protected IWebHelper WebHelper => LazyServiceProvider.LazyGetRequiredService<IWebHelper>();

    protected IConfiguration Configuration =>
        LazyServiceProvider.LazyGetRequiredService<IConfiguration>();

    protected CommonUtils CommonUtils => LazyServiceProvider.LazyGetRequiredService<CommonUtils>();
}