﻿global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Threading.Tasks;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.Logging;
global using Volo.Abp;
global using Volo.Abp.Auditing;
global using Volo.Abp.DependencyInjection;
global using Volo.Abp.Domain.Entities;
global using Volo.Abp.Domain.Repositories;
global using Volo.Abp.Uow;
global using XCloud.Core.Cache;
global using XCloud.Core.Extension;
global using XCloud.Sales.Application.Core;
global using XCloud.Application.Extension;
global using XCloud.Application.Mapping;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Data;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Platform.Application;
using XCloud.Platform.Messenger;
using XCloud.Sales.Application.Database;
using XCloud.Sales.Application.Configuration;
using XCloud.Sales.Application.Job;
using XCloud.Sales.Application.Localization;
using XCloud.Sales.Application.Mapper;

namespace XCloud.Sales.Application;

[DependsOn(
    typeof(PlatformApplicationModule),
    typeof(PlatformMessengerModule)
)]
[MethodMetric]
public class SalesApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.ConfigSalesLocalization();

        context.AddAutoMapperProfile<SalesAutoMapperConfiguration>();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //request data holder
        context.Services.RemoveAll<IRequestCacheProvider>();
        context.Services.AddScoped<IRequestCacheProvider, HttpContextItemRequestCache>();
    }

    public override void OnPreApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        Task.Run(() => app.TryCreateSalesDatabase()).Wait();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        using var s = app.ApplicationServices.CreateScope();

        var config = s.ServiceProvider.GetRequiredService<IConfiguration>();

        if (config.GetSalesOptionOrDefault().AutoStartJob)
        {
            Task.Run(() => app.ApplicationServices.StartSalesJobsAsync()).Wait();
            Task.Run(() => app.ApplicationServices.StartDataSeederJob()).Wait();
        }
    }
}