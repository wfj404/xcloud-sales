﻿using System;
using System.Threading.Tasks;
using DotNetCore.CAP;
using JetBrains.Annotations;
using Volo.Abp.Domain.Entities.Events.Distributed;

namespace XCloud.Application.Queue;

public static class DistributedEventBusExtension
{
    [NotNull]
    public static T EnsureNotNull<T>(this EntityCreatedEto<T> entityCreatedEto)
    {
        if (entityCreatedEto.Entity == null)
            throw new ArgumentNullException(nameof(entityCreatedEto));

        return entityCreatedEto.Entity;
    }

    [NotNull]
    public static T EnsureNotNull<T>(this EntityUpdatedEto<T> entityCreatedEto)
    {
        if (entityCreatedEto.Entity == null)
            throw new ArgumentNullException(nameof(entityCreatedEto));

        return entityCreatedEto.Entity;
    }

    public static async Task PublishEntityUpdatedAsync<T>(this ICapPublisher distributedEventBus, string topic, T data)
        where T : class
    {
        if (string.IsNullOrWhiteSpace(topic))
            throw new ArgumentNullException(nameof(topic));

        if (data == null)
            throw new ArgumentNullException(nameof(data));

        await distributedEventBus.PublishAsync(topic, new EntityUpdatedEto<T>(data));
    }

    public static async Task PublishEntityCreatedAsync<T>(this ICapPublisher distributedEventBus, string topic, T data)
        where T : class
    {
        if (string.IsNullOrWhiteSpace(topic))
            throw new ArgumentNullException(nameof(topic));

        if (data == null)
            throw new ArgumentNullException(nameof(data));

        await distributedEventBus.PublishAsync(topic, new EntityCreatedEto<T>(data));
    }
}