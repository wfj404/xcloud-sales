﻿using System.Reflection;
using Microsoft.Extensions.Configuration;
using Volo.Abp.Data;
using Volo.Abp.Modularity;
using Volo.Abp.ObjectExtending;

namespace XCloud.Core.Configuration;

public interface IXCloudBuilder : IHasExtraProperties
{
    /// <summary>
    /// 入口module类型
    /// </summary>
    Type EntryModuleType { get; }

    /// <summary>
    /// 所有程序集
    /// </summary>
    IReadOnlyCollection<Assembly> AllModuleAssemblies { get; }

    IConfiguration Configuration { get; }

    IServiceCollection Services { get; }
}

public sealed class XCloudBuilder<EntryModule> : ExtensibleObject, IXCloudBuilder
{
    public IConfiguration Configuration { get; }
    public IServiceCollection Services { get; }

    public IReadOnlyCollection<Assembly> AllModuleAssemblies { get; }

    public Type EntryModuleType { get; }

    public XCloudBuilder(IServiceCollection collection)
    {
        Services = collection ?? throw new ArgumentNullException(nameof(collection));
        Configuration = Services.GetConfiguration();

        //入口程序集
        EntryModuleType = typeof(EntryModule);

        AllModuleAssemblies = FindDependedAssembliesDeepFirst(EntryModuleType);
    }

    private Assembly[] FindDependedAssembliesDeepFirst(Type entry)
    {
        if (entry == null)
            throw new ArgumentNullException(nameof(entry));

        var moduleList = new List<Type>();

        var assemblies = new List<Assembly>();

        void FindAssembly(Type t)
        {
            //检查有没有历遍过
            if (moduleList.Contains(t))
                return;
            moduleList.Add(t);

            //深度优先
            var dependOn = t.GetCustomAttributes<DependsOnAttribute>().ToArray();
            if (dependOn.Any())
            {
                dependOn.SelectMany(x => x.DependedTypes).ToList().ForEach(FindAssembly);
            }

            //保证被依赖的排在前面
            if (!assemblies.Contains(t.Assembly))
            {
                assemblies.Add(t.Assembly);
            }
        }

        FindAssembly(entry);

        var res = assemblies.WhereNotNull().ToArray();
        return res;
    }
}