﻿using Volo.Abp;

namespace XCloud.Core.Cache;

public class CacheException : BusinessException
{
    public CacheException(string msg, Exception e = null) : base(message: msg, innerException: e)
    {
        //
    }
}

public class CacheSourceException : BusinessException
{
    public CacheSourceException(Exception e) : base(message: e.Message, innerException: e)
    {
        //
    }
}