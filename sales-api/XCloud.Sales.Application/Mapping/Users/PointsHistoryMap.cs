using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Users;

namespace XCloud.Sales.Application.Mapping.Users;

public class PointsHistoryMap : SalesEntityTypeConfiguration<PointsHistory>
{
    public override void Configure(EntityTypeBuilder<PointsHistory> builder)
    {
        builder.ToTable("sales_points_history");
        
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.UserId);
        builder.Property(x => x.OrderId).HasMaxLength(100);
        builder.Property(x => x.Points);
        builder.Property(x => x.PointsBalance);
        builder.Property(x => x.ActionType);
        builder.Property(x => x.Message).HasMaxLength(300);
        builder.Property(rph => rph.UsedAmount).HasPrecision(18, 2);

        base.Configure(builder);
    }
}