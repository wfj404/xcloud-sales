﻿using System.Reflection;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Volo.Abp;
using XCloud.Core.Dto;

namespace XCloud.Core.Http.Dynamic.Definition;

public class ActionDefinition
{
    public ServiceDefinition ServiceDefinition { get; }
    public MethodInfo ActionMethod { get; }

    public IRouteTemplateProvider RouteTemplateProvider { get; }
    public IActionHttpMethodProvider ActionHttpMethodProvider { get; }

    public IReadOnlyList<ParameterDefinition> ParameterDefinitions { get; }

    public ActionDefinition(ServiceDefinition serviceContractDefinition, MethodInfo m)
    {
        if (serviceContractDefinition == null)
            throw new ArgumentNullException(nameof(serviceContractDefinition));

        if (m == null)
            throw new ArgumentNullException(nameof(m));

        ServiceDefinition = serviceContractDefinition;
        ActionMethod = m;
        var attrs = m.GetCustomAttributes().ToArray();
        RouteTemplateProvider = attrs.OfType<IRouteTemplateProvider>().FirstOrDefault();
        ActionHttpMethodProvider = attrs.OfType<IActionHttpMethodProvider>().FirstOrDefault();

        var parameters = m.GetParameters();
        ParameterDefinitions = parameters.Select(x => new ParameterDefinition(x)).ToArray();
    }

    public string RouteOrEmpty() => RouteTemplateProvider?.Template ?? string.Empty;

    public string ActionHttpMethod() => ActionHttpMethodProvider?.HttpMethods?.FirstOrDefault();

    [NotNull]
    static MethodInfo __get_dispose_method__()
    {
        var m = typeof(IDisposable).GetMethods().FirstOrDefault(x => x.Name == nameof(IDisposable.Dispose));

        return m ?? throw new AbpException(nameof(__get_dispose_method__));
    }

    static readonly MethodInfo DisposeMethod = __get_dispose_method__();

    public bool IsDisposeMethod() => ActionMethod == DisposeMethod;

    public bool IsAsyncMethod() => ActionMethod.ReturnType.IsTask();

    public Type GetTaskRealReturnType() => ActionMethod.ReturnType.UnWrapTaskOrNull();

    public IDictionary<ParameterDefinition, object> GetMethodParameters(object[] args)
    {
        if (args == null)
            throw new ArgumentNullException(nameof(args));

        (args.Length == ParameterDefinitions.Count).Should().BeTrue();

        var res = EnumerableExtension.ToDictionaryUpdateDuplicateKey(
            ParameterDefinitions.OrderBy(x => x.ParameterInfo.Position), x => x, x => args[x.ParameterInfo.Position]);

        return res;
    }

    public bool IsAction()
    {
        if (ParameterDefinitions.Any(x => !x.IsValidated()))
        {
            return false;
        }

        var m = ActionMethod;
        if (m.GetCustomAttributesList<NonActionAttribute>().Any())
        {
            return false;
        }

        if (RouteTemplateProvider == null || ActionHttpMethodProvider == null)
        {
            return false;
        }

        if (m.ReturnType == typeof(void) || m.ReturnType == typeof(Task))
        {
            return false;
        }

        var returnType = GetTaskRealReturnType();
        if (returnType != null)
        {
            var normalResponse = returnType == typeof(string) || IsResponseEntity(returnType);
            if (!normalResponse)
            {
                return false;
            }
        }

        if (!m.IsPublic || m.IsGenericMethod)
        {
            return false;
        }

        var baseDefinition = m.GetBaseDefinition();
        if (baseDefinition != null)
        {
            if (baseDefinition.DeclaringType == typeof(object))
            {
                return false;
            }
        }

        return true;
    }

    private bool IsResponseEntity(Type t)
    {
        var res = t.IsGenericType && t.IsEqualToGenericType(typeof(ApiResponse<>));
        return res;
    }
}