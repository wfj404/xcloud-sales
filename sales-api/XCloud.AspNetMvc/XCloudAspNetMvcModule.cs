﻿using System.Text.Encodings.Web;
using System.Text.Unicode;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.AntiForgery;
using Volo.Abp.Modularity;
using Volo.Abp.Uow;
using XCloud.Application;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.Configuration;
using XCloud.AspNetMvc.Json;
using XCloud.AspNetMvc.Mapper;
using XCloud.AspNetMvc.Nacos;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Configuration;

namespace XCloud.AspNetMvc;

[DependsOn(
    typeof(AbpAspNetCoreMvcModule),
    typeof(AbpUnitOfWorkModule),
    typeof(XCloudApplicationModule)
)]
[MethodMetric]
public class XCloudAspNetMvcModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        context.AddAutoMapperProfile<AspNetMvcAutoMapperConfiguration>();
        //添加mvc组件
        context.ConfigureMvc();
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //解决中文被编码
        context.Services.AddSingleton(HtmlEncoder.Create(UnicodeRanges.All));

        //http context上下文
        context.Services
            .RemoveAll<IHttpContextAccessor>()
            .AddHttpContextAccessor();

        context.ConfigureMvcOptions();
        context.ConfigJsonProvider();
        context.ConfigDynamicApi();
        context.ConfigModelValidation();

        //nacos
        var config = context.Services.GetConfiguration();
        context.Services.TryAddNacosServiceDiscovery(config, NacosDefaults.DefaultSectionName);
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //uow
        Configure<AbpUnitOfWorkDefaultOptions>(option =>
        {
            option.TransactionBehavior = UnitOfWorkTransactionBehavior.Enabled;
        });

        //anti forgery
        Configure<AbpAntiForgeryOptions>(option => option.AutoValidate = false);

        //request data holder
        context.Services.RemoveAll<IRequestCacheProvider>();
        context.Services.AddScoped<IRequestCacheProvider, HttpContextItemRequestCache>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();
        using var s = app.ApplicationServices.CreateScope();
        var builder = s.ServiceProvider.GetRequiredService<IXCloudBuilder>();

        builder.ExtraProperties.Clear();
    }
}