﻿using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/store")]
public class StoreController : ShopBaseController
{
    private readonly IStoreService _storeService;

    public StoreController(IStoreService storeService)
    {
        _storeService = storeService;
    }

    [HttpPost("get-store")]
    public async Task<ApiResponse<StoreDto>> GetStoreAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var store = await this._storeService.GetRequiredByIdAsync(manager.StoreId);

        return new ApiResponse<StoreDto>(store);
    }

    [HttpPost("save")]
    public async Task<ApiResponse<object>> UpdateStoreAsync([FromBody] StoreDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredSuperManagerAsync(manager.Id);

        await this._storeService.UpdateAsync(dto);

        return new ApiResponse<object>();
    }
}