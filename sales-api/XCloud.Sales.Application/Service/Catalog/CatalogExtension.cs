using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Service.Catalog;

public static class CatalogExtension
{
    public static bool IsStatusOk(this Goods goods) => goods.Published && !goods.IsDeleted;

    public static bool IsStatusOk(this Sku sku) => sku.IsActive && !sku.IsDeleted;

    public static string FingerPrint(this SpecItemDto dto) => dto;

    public static string FingerPrint(this SpecItemDto[] combination)
    {
        if (!combination.Any())
        {
            return string.Empty;
        }

        var items = combination
            .OrderBy(x => x.SpecId)
            .ThenBy(x => x.SpecValueId)
            .Select(x => x.FingerPrint())
            .ToArray();

        var fingerPrint = string.Join('&', items);

        return fingerPrint;
    }

    public static GoodsPictureDto[] SortGoodsPictures(this GoodsPictureDto[] data, string skuId = default)
    {
        int SortSku(GoodsPictureDto x)
        {
            if (string.IsNullOrWhiteSpace(skuId))
                return default;

            return x.SkuId == skuId ? 1 : 0;
        }

        return data
            .OrderByDescending(SortSku)
            .ThenBy(x => x.DisplayOrder)
            .ToArray();
    }

    public static void HidePrice(this SkuDto sku)
    {
        sku.Price = default;
        sku.PriceModel = new PriceModel() { Empty = true };
    }

    public static void HidePrice(this IEnumerable<SkuDto> list)
    {
        foreach (var m in list)
        {
            m.HidePrice();
        }
    }

    public static void HideDetail(this Goods goods)
    {
        goods.DetailInformation = default;
    }

    public static void HideDetail(this IEnumerable<Goods> goodsList)
    {
        foreach (var goods in goodsList)
        {
            goods.HideDetail();
        }
    }

    public static async Task<string[]> QueryCategoryAndAllChildrenIdsWithCacheAsync(
        this ICategoryService categoryService,
        string catId,
        ICacheProvider cacheProvider)
    {
        var cachePolicyOption = new CacheStrategy { Cache = true };

        var key = $"{nameof(CatalogExtension)}.{nameof(QueryCategoryAndAllChildrenIdsWithCacheAsync)}.{catId}";

        var option = new CacheOption<CategoryDto[]>(key: key, expiration: TimeSpan.FromMinutes(5));

        var categories = await
            cacheProvider.ExecuteWithPolicyAsync(
                () => categoryService.GetAllChildrenAsync(catId),
                option, cachePolicyOption);

        categories ??= Array.Empty<CategoryDto>();

        var ids = categories.Ids().Append(catId).Distinct().ToArray();

        return ids;
    }

    private static IOrderedEnumerable<AntDesignTreeNode<CategoryDto>> SortCategory(
        this AntDesignTreeNode<CategoryDto>[] data)
    {
        return data
            .OrderByDescending(x => x.raw_data?.DisplayOrder ?? default(int))
            .ThenByDescending(x => x.raw_data?.Id ?? string.Empty);
    }

    public static AntDesignTreeNode<CategoryDto>[] SortAllLevelsV2(this AntDesignTreeNode<CategoryDto>[] data)
    {
        data = data.SortCategory().ToArray();

        foreach (var m in data)
        {
            m.WalkTreeNodes(
                idSelector: x => x.key,
                childrenSelector: (node, id) => node.children ?? [],
                callback: (node, children) => { node.children = node.children?.SortCategory().ToArray(); });
        }

        return data;
    }

    [Obsolete]
    public static AntDesignTreeNode<CategoryDto>[] SortAllLevels(this AntDesignTreeNode<CategoryDto>[] data)
    {
        var ids = new List<string>();

        void HandleNode(AntDesignTreeNode<CategoryDto> node)
        {
            if (node.raw_data == null)
            {
                return;
            }

            if (ids.Contains(node.raw_data.Id))
            {
                return;
            }

            ids.Add(node.raw_data.Id);

            if (ValidateHelper.IsEmptyCollection(node.children))
            {
                return;
            }

            node.children.ToList().ForEach(HandleNode);
            node.children = node.children.SortCategory().ToArray();
        }

        data.ToList().ForEach(HandleNode);
        data = data.SortCategory().ToArray();

        return data;
    }
}