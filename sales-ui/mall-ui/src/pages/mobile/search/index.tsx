import { useInfiniteScroll, useTitle } from 'ahooks';
import { ErrorBlock } from 'antd-mobile';
import { useEffect, useState } from 'react';
import { history, useLocation } from 'umi';

import XBackToTop from '@/components/common/back_to_top';
import ScrollLoading from '@/components/common/scroll/loading';
import XItem from '@/components/goods/card/box';
import XDetailPreview from '@/components/goods/detail/detailPreview';
import XHeader from '@/components/mobile/simpleHeader';
import {
  PagedResponse,
  parseQueryString,
  updateQueryString,
} from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { GoodsDto, QueryGoodsPaging } from '@/utils/swagger';
import { Masonry } from '@mui/lab';
import { Box } from '@mui/material';

import XSearchBox from './search_box';
import XTopicView from './topic';
import { isEmptyGoodsQueryFilter } from './utils';

export default function () {
  useTitle('搜索');

  const location = useLocation();

  const [detailId, _detailId] = useState<string | undefined>(undefined);

  const [query, _query] = useState<QueryGoodsPaging>(
    parseQueryString(location.search) as QueryGoodsPaging,
  );

  const { loading, loadingMore, noMore, data, mutate, reload } =
    useInfiniteScroll<{
      list: GoodsDto[];
      res?: PagedResponse<GoodsDto>;
    }>(
      async (currentData) => {
        const res = await apiClient.mall.searchSearchGoodsList({
          ...query,
          Page: (currentData?.res?.PageIndex || 0) + 1,
        });

        return {
          list: res.data.Items || [],
          res: res.data,
        };
      },
      {
        manual: true,
        target: window.document,
        isNoMore: (x) => x?.res?.IsEmpty || false,
        //reloadDeps: [query],
      },
    );

  useEffect(() => {
    mutate({
      list: [],
    });
    if (!isEmptyGoodsQueryFilter(query)) {
      reload();
    }
    updateQueryString<QueryGoodsPaging>({
      ...query,
      Page: undefined,
      PageSize: undefined,
      IsPublished: undefined,
      SkipCalculateTotalCount: undefined,
    });
  }, [query]);

  useEffect(() => {
    const dispose = history.listen((loc) => {
      _query(parseQueryString(loc.location.search));
    });

    return () => {
      dispose();
    };
  }, []);

  const renderColumns = () => {
    if (isEmptyGoodsQueryFilter(query)) {
      return (
        <XTopicView
          query={query}
          onChange={(e) => {
            _query({
              ...e,
            });
          }}
        />
      );
    }

    const itemList = data?.list || [];

    if (itemList.length <= 0) {
      return <ErrorBlock status="empty" />;
    }

    return (
      <>
        <Box sx={{ px: 1 }}>
          <Masonry columns={{ xs: 2, sm: 4 }} spacing={{ xs: 1, sm: 2, md: 3 }}>
            {itemList.map((item, index) => (
              <div
                key={index}
                onClick={() => {
                  item.Id && _detailId(item.Id);
                }}
              >
                <XItem model={item} lazy={false} />
              </div>
            ))}
          </Masonry>
        </Box>
        <div style={{ marginTop: 10 }}>
          <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
        </div>
      </>
    );
  };

  return (
    <>
      <XHeader>
        <XBackToTop />
        <XDetailPreview
          detailId={detailId}
          onClose={() => {
            _detailId('');
          }}
        />
        <Box sx={{ mb: 2, px: 1 }}>
          <XSearchBox
            finalQuery={query}
            onSearch={(e) => {
              _query(e);
            }}
          />
        </Box>
        <Box sx={{ mb: 1 }}>{renderColumns()}</Box>
      </XHeader>
    </>
  );
}
