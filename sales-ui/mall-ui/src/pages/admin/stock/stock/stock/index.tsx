import { Button, message, Tag } from 'antd';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { QueryWarehouseStockPagingInput, StockDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { CheckOutlined, DeleteFilled } from '@ant-design/icons';
import { ProColumns, ProTable } from '@ant-design/pro-components';

import XItems from './items';
import XSearchForm from './search_form';
import XForm from './xform';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<StockDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QueryWarehouseStockPagingInput>({
    Page: 1,
  });

  const [formData, _formData] = useState<StockDto | undefined>(undefined);
  const [loadingId, _loadingId] = useState<string | undefined>(undefined);

  const queryList = (q: QueryWarehouseStockPagingInput) => {
    _loading(true);
    apiClient.mallAdmin
      .stockStockPaging(q)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data || {});
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const approveStock = (row: StockDto) => {
    if (row.Approved) {
      return;
    }
    if (!confirm('确定批准采购单？')) {
      return;
    }
    _loadingId(row.Id || '');
    apiClient.mallAdmin
      .stockApproveStock({ Id: row.Id })
      .then((res) => {
        handleResponse(res, () => {
          message.success('已经批准采购');
          queryList(query);
        });
      })
      .finally(() => {
        _loadingId('');
      });
  };

  const deleteBrand = (row: StockDto) => {
    if (!confirm('删除入库单？')) {
      return;
    }
    _loadingId(row.Id || '');
    apiClient.mallAdmin
      .stockDeleteUnApprovedStock({
        Id: row.Id,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          queryList(query);
        }
      })
      .finally(() => {
        _loadingId('');
      });
  };

  const columns: ProColumns<StockDto>[] = [
    {
      title: '批号',
      render: (d, x: StockDto) => <b>{`${x.No || '--'}`}</b>,
    },
    {
      title: '备注信息',
      render: (d, x) => x.Remark || '--',
    },
    {
      title: '仓库',
      render: (d, x) => {
        return <span>{x.Warehouse?.Name || '--'}</span>;
      },
    },
    {
      title: '供货商',
      render: (d, x) => {
        return <span>{x.Supplier?.Name || '--'}</span>;
      },
    },
    {
      title: '状态',
      render: (d, x: StockDto) => {
        return x.Approved ? (
          <Tag color="green">已批准</Tag>
        ) : (
          <Tag color="yellow">待审核</Tag>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, record: StockDto) => {
        return (
          <Button.Group>
            {record.Approved || (
              <Button
                icon={<CheckOutlined />}
                loading={loadingId == record.Id}
                type="primary"
                onClick={() => {
                  approveStock(record);
                }}
              >
                审核
              </Button>
            )}
            {record.Approved || (
              <Button
                icon={<DeleteFilled />}
                loading={loadingId == record.Id}
                danger
                onClick={() => {
                  deleteBrand(record);
                }}
              >
                删除
              </Button>
            )}
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList(query);
        }}
      />
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <ProTable
        options={false}
        search={false}
        headerTitle={'采购单'}
        toolBarRender={() => [
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>,
        ]}
        rowKey={(x) => x.Id || ''}
        loading={loading}
        columns={columns}
        dataSource={data.Items || []}
        expandable={{
          expandedRowRender: (e: StockDto) => (
            <div style={{ padding: 10, border: '5px dashed orange' }}>
              <XItems data={e.Items || []} />
            </div>
          ),
        }}
        pagination={{
          showSizeChanger: false,
          pageSize: 20,
          current: query.Page,
          total: data.TotalCount,
          onChange: (e) => {
            const q: QueryWarehouseStockPagingInput = {
              ...query,
              Page: e,
            };
            _query(q);
            queryList(q);
          },
        }}
      />
    </>
  );
};
