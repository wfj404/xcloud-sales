﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Common;

namespace XCloud.Sales.Application.Mapping.Common;

public class ExternalSkuMap : SalesEntityTypeConfiguration<ExternalSku>
{
    public override void Configure(EntityTypeBuilder<ExternalSku> builder)
    {
        builder.ToTable("sales_external_sku");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.SystemName).RequiredId();
        builder.Property(x => x.ExternalSkuId).RequiredId();
        builder.Property(x => x.CombineId).IsRequired().HasMaxLength(200);
        builder.Property(x => x.GoodsName).HasMaxLength(100);
        builder.Property(x => x.SkuName).HasMaxLength(100);
        builder.Property(x => x.Price).PricePrecision();
        builder.Property(x => x.DataJson);
        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}