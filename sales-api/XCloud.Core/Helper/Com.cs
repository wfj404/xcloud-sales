﻿using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace XCloud.Core.Helper;

/// <summary>
/// 公共方法类
/// </summary>
public static class Com
{
    public static Size CalculateMaxSize(Size originSize, int? maxHeight, int? maxWidth)
    {
        if (originSize.Height <= 0 || originSize.Width <= 0)
            throw new ArgumentException(message: nameof(CalculateMaxSize));

        maxHeight ??= 0;
        maxWidth ??= 0;

        var rateList = new List<double>();

        //height resize rate
        if (maxHeight.Value > 0)
        {
            var height = (double)Math.Min(originSize.Height, maxHeight.Value);
            rateList.Add(height / originSize.Height);
        }

        //width resize rate
        if (maxWidth.Value > 0)
        {
            var width = (double)Math.Min(originSize.Width, maxWidth.Value);
            rateList.Add(width / originSize.Width);
        }

        if (rateList.Any())
        {
            var rate = rateList.Min();
            return new Size(width: (int)(originSize.Width * rate), height: (int)(originSize.Height * rate));
        }

        return originSize;
    }

    public static DateTime Utc1970 => new(
        year: 1970, month: 1, day: 1,
        hour: 0, minute: 0, second: 0,
        kind: DateTimeKind.Utc);

    public static long GetTimestampInSecond()
    {
        return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
    }

    public static async Task WalkDirectoryAsync(string path, Func<FileInfo, Task> func)
    {
        if (string.IsNullOrWhiteSpace(path))
            throw new ArgumentNullException(nameof(path));

        if (func == null)
            throw new ArgumentNullException(nameof(func));

        var stack = new Stack<DirectoryInfo>();

        var root = new DirectoryInfo(path);
        stack.Push(root);

        while (stack.Count > 0)
        {
            var curNode = stack.Pop();

            if (curNode == null || !curNode.Exists)
            {
                continue;
            }

            foreach (var f in curNode.GetFiles())
            {
                await func.Invoke(f);
            }

            foreach (var dir in curNode.GetDirectories())
            {
                stack.Push(dir);
            }
        }
    }

    /// <summary>
    /// 监听目录文件变化
    /// </summary>
    public static FileSystemWatcher WatcherFileSystem(string path, Action<object, FileSystemEventArgs> change,
        string filter = "*.*")
    {
        if (string.IsNullOrWhiteSpace(path))
            throw new ArgumentNullException(nameof(path));

        if (string.IsNullOrWhiteSpace(filter))
            throw new ArgumentNullException(nameof(filter));

        var watcher = new FileSystemWatcher();

        try
        {
            watcher.Path = path;
            watcher.Filter = filter;

            watcher.NotifyFilter =
                NotifyFilters.Attributes | NotifyFilters.CreationTime | NotifyFilters.DirectoryName |
                NotifyFilters.FileName | NotifyFilters.LastAccess | NotifyFilters.LastWrite |
                NotifyFilters.Security | NotifyFilters.Size;

            watcher.EnableRaisingEvents = true;
            watcher.IncludeSubdirectories = true;

            watcher.Changed += change.Invoke;
            watcher.Created += change.Invoke;
            watcher.Deleted += change.Invoke;
            watcher.Renamed += change.Invoke;

            return watcher;
        }
        catch
        {
            using (watcher)
            {
                //
            }

            throw;
        }
    }

    public static IEnumerable<int> Range(int start, int end, int? step = default)
    {
        step ??= 1;

        if (step.Value <= 0)
            throw new ArgumentException($"{nameof(step)}必须大于0");

        for (var i = start; i < end; i += step.Value)
        {
            yield return i;
        }
    }
}