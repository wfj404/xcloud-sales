import { CSSProperties, ReactNode } from 'react';

import { UniqueIdentifier, useDraggable } from '@dnd-kit/core';
import { CSS } from '@dnd-kit/utilities';

export default ({
  id,
  children,
}: {
  id: UniqueIdentifier;
  children?: ReactNode;
}) => {
  const { setNodeRef, attributes, listeners, transform, isDragging } =
    useDraggable({ id });
  const styles: CSSProperties = {
    transform: CSS.Translate.toString(transform),
    //border: isDragging ? '2px dashed orange' : 'none',
  };
  return (
    <>
      <div
        ref={setNodeRef}
        style={{ ...styles }}
        {...attributes}
        {...listeners}
      >
        {children == undefined ? id : children}
      </div>
    </>
  );
};
