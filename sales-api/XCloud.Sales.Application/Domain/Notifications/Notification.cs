﻿namespace XCloud.Sales.Application.Domain.Notifications;

public class Notification : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime
{
    public string Title { get; set; }

    public string Content { get; set; }

    public string BusinessId { get; set; }

    public string App { get; set; }

    public bool Read { get; set; }

    public DateTime? ReadTime { get; set; }

    public int UserId { get; set; }

    public string ActionType { get; set; }

    public string Data { get; set; }

    public string SenderId { get; set; }

    public string SenderType { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }
}