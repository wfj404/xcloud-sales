import { useOutlet } from 'umi';

import { config } from '@/utils/config';
import { isStringEmpty } from '@/utils/utils';

export default () => {
  const children = useOutlet();

  if (!isStringEmpty(config.external_3h_login_url)) {
    return <h1>已经接入外部登录，此地址已经停用</h1>;
  }

  return <>{children}</>;
};
