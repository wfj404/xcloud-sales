using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Grades;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/grade-price")]
public class GradePriceController : ShopBaseController
{
    private readonly IGradeGoodsPriceService _gradeGoodsPriceService;
    
    public GradePriceController(IGradeGoodsPriceService gradeGoodsPriceService)
    {
        _gradeGoodsPriceService = gradeGoodsPriceService;
    }

    [HttpPost("list-sku-grade-price")]
    public async Task<ApiResponse<GoodsGradePriceDto[]>> ListSkuGradePriceAsync([FromBody] IdDto<string> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var data = await _gradeGoodsPriceService.QueryBySpecIdAsync(dto.Id);

        await _gradeGoodsPriceService.AttachGradesAsync(data);

        return new ApiResponse<GoodsGradePriceDto[]>(data);
    }
}