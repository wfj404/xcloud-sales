﻿using XCloud.Application.Model;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/manage-setting")]
public class ManageSettingController : PlatformBaseController
{
    private readonly ISettingManageService _settingManageService;

    public ManageSettingController(ISettingManageService settingManageService)
    {
        _settingManageService = settingManageService;
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<SettingDto>> QueryPagingAsync([FromBody] QuerySettingsPagingInput dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var data = await _settingManageService.QueryPagingAsync(dto);

        return data;
    }
}