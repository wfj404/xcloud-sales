using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderNoteMap : SalesEntityTypeConfiguration<OrderNote>
{
    public override void Configure(EntityTypeBuilder<OrderNote> builder)
    {
        builder.ToTable("sales_order_note");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.OrderId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Note).HasMaxLength(1000);

        builder.Property(x => x.DisplayToUser);

        builder.Property(x => x.ExtendedJson);

        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}