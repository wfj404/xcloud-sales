﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Common;

namespace XCloud.Sales.Application.Mapping.Common;

public class ExternalSkuMappingMap : SalesEntityTypeConfiguration<ExternalSkuMapping>
{
    public override void Configure(EntityTypeBuilder<ExternalSkuMapping> builder)
    {
        builder.ToTable("sales_external_sku_mapping");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.CombineId).IsRequired().HasMaxLength(200);
        builder.Property(x => x.SkuId).RequiredId();

        base.Configure(builder);
    }
}