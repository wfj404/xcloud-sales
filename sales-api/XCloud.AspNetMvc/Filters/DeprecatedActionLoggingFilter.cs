﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;

namespace XCloud.AspNetMvc.Filters;

[ExposeServices(typeof(DeprecatedActionLoggingFilter), typeof(IAsyncActionFilter))]
public class DeprecatedActionLoggingFilter : IAsyncActionFilter, ITransientDependency
{
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var env = context.HttpContext.RequestServices.GetRequiredService<IWebHostEnvironment>();

        if (env.IsStaging() || env.IsDevelopment())
        {
            var logger =
                context.HttpContext.RequestServices.GetRequiredService<ILogger<DeprecatedActionLoggingFilter>>();
            try
            {
                var method = context.ActionDescriptor?.GetMethodInfo();
                var attr = method.GetCustomAttributes<ObsoleteAttribute>().FirstOrDefault();
                if (attr != null)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine(nameof(DeprecatedActionLoggingFilter));
                    sb.AppendLine(attr.Message ?? "null-attr-message");
                    sb.AppendLine(context.HttpContext.Request.Path);
                    sb.AppendLine(method.DeclaringType?.FullName ?? "null-type-name");
                    sb.AppendLine(method.Name);

                    logger.LogWarning(message: sb.ToString());
                }
            }
            catch (Exception e)
            {
                logger.LogError(message: e.Message, exception: e);
            }
        }

        await next.Invoke();
    }
}