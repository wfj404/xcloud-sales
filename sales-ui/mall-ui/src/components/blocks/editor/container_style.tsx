import { ColorPicker, Form, Input, InputNumber, Space } from 'antd';
import { CSSProperties } from 'react';

import { stringOrUndefined } from '@/utils/utils';

export default ({
  sx,
  onChange,
}: {
  sx: CSSProperties;
  onChange: (x: CSSProperties) => void;
}) => {
  const setStyles = (e: CSSProperties) => {
    onChange({
      ...sx,
      ...e,
      padding: undefined,
      margin: undefined,
    });
  };

  return (
    <>
      <Form
        labelCol={{
          span: 2,
        }}
      >
        <Form.Item label={'外边距'}>
          <Space direction={'horizontal'}>
            <InputNumber
              addonBefore={'left'}
              value={sx.marginLeft || 0}
              onChange={(e) => {
                setStyles({
                  marginLeft: e || undefined,
                });
              }}
            />
            <InputNumber
              addonBefore={'top'}
              value={sx.marginTop || 0}
              onChange={(e) => {
                setStyles({
                  marginTop: e || undefined,
                });
              }}
            />
            <InputNumber
              addonBefore={'right'}
              value={sx.marginRight || 0}
              onChange={(e) => {
                setStyles({
                  marginRight: e || undefined,
                });
              }}
            />
            <InputNumber
              addonBefore={'bottom'}
              value={sx.marginBottom || 0}
              onChange={(e) => {
                setStyles({
                  marginBottom: e || undefined,
                });
              }}
            />
          </Space>
        </Form.Item>

        <Form.Item label={'内边距'}>
          <Space direction={'horizontal'}>
            <InputNumber
              addonBefore={'left'}
              value={sx.paddingLeft || 0}
              onChange={(e) => {
                setStyles({
                  paddingLeft: e || undefined,
                });
              }}
            />
            <InputNumber
              addonBefore={'top'}
              value={sx.paddingTop || 0}
              onChange={(e) => {
                setStyles({
                  paddingTop: e || undefined,
                });
              }}
            />
            <InputNumber
              addonBefore={'right'}
              value={sx.paddingRight || 0}
              onChange={(e) => {
                setStyles({
                  paddingRight: e || undefined,
                });
              }}
            />
            <InputNumber
              addonBefore={'bottom'}
              value={sx.paddingBottom || 0}
              onChange={(e) => {
                setStyles({
                  paddingBottom: e || undefined,
                });
              }}
            />
          </Space>
        </Form.Item>

        <Form.Item label={'边框'}>
          <Input
            value={sx.border}
            onChange={(e) => {
              setStyles({
                border: stringOrUndefined(e.target.value),
              });
            }}
          />
        </Form.Item>

        <Form.Item label={'圆角'}>
          <InputNumber
            min={0}
            value={sx.borderRadius || 0}
            onChange={(e) => {
              setStyles({
                borderRadius: e || undefined,
              });
            }}
          />
        </Form.Item>

        <Form.Item label={'背景颜色'}>
          <ColorPicker
            allowClear
            showText
            defaultValue={undefined}
            value={sx.backgroundColor || undefined}
            onChange={(e) => {
              setStyles({
                backgroundColor: e.toRgbString(),
              });
            }}
            onClear={() => {
              setStyles({
                backgroundColor: undefined,
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
};
