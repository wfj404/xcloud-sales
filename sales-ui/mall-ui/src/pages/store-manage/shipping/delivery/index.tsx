import { apiClient } from '@/utils/client';
import { PagedResponse } from '@/utils/biz';
import { DeliveryRecordDto, QueryDeliveryRecordPagingInput } from '@/utils/swagger';
import { Card, Space, Switch } from 'antd';
import { useEffect, useState } from 'react';
import XDeliveryList from './list';
import { handleResponse } from '@/utils/utils';

export default () => {
  const [loading, _loading] = useState(true);
  const [query, _query] = useState<QueryDeliveryRecordPagingInput>({
    OrderToService: true,
  });
  const [data, _data] = useState<PagedResponse<DeliveryRecordDto>>({});

  const queryList = (q: QueryDeliveryRecordPagingInput) => {
    _loading(true);
    apiClient.mallManager
      .deliveryQueryPaging({ ...q })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <Card
        loading={loading}
        title='配送'
        extra={
          <Space direction={'horizontal'}>
            <Switch checked={query.OrderToService || false} onChange={e => {
              const q: QueryDeliveryRecordPagingInput = {
                ...query,
                Page: 1,
                OrderToService: e,
              };
              _query(q);
              queryList(q);
            }} />
            <span>只展示待服务订单相关配送</span>
          </Space>
        }
      >
        <XDeliveryList
          data={data.Items || []}
          pagination={{
            total: data.TotalCount,
            pageSize: data.PageSize,
            current: query.Page,
            onChange: (e) => {
              const q: QueryDeliveryRecordPagingInput = { ...query, Page: e };
              _query(q);
              queryList(q);
            },
          }}
          refresh={() => {
            queryList({ ...query });
          }}
        />
      </Card>
    </>
  );
};
