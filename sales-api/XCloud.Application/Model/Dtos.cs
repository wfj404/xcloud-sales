﻿using System;
using Volo.Abp.Application.Dtos;

namespace XCloud.Application.Model;

public class AuthTokenDto : IEntityDto
{
    public AuthTokenDto()
    {
        //
    }

    public virtual string AccessToken { get; set; }

    public virtual string RefreshToken { get; set; }

    public virtual DateTime? ExpiredTime { get; set; }
}

public class AntDesignTreeNode : AntDesignTreeNode<object>
{
    //
}

public class AntDesignTreeNode<T> : IEntityDto
{
    public AntDesignTreeNode()
    {
        //
    }

    public AntDesignTreeNode(T data) : this()
    {
        this.raw_data = data;
    }

    public string key { get; set; }

    public string title { get; set; }

    public T raw_data { get; set; }

    public int? node_level { get; set; }

    public AntDesignTreeNode<T>[] children { get; set; }
}

public class IdentityNameDto : IEntityDto
{
    public IdentityNameDto()
    {
        //
    }

    public IdentityNameDto(string name)
    {
        IdentityName = name;
    }

    public string IdentityName { get; set; }

    public static implicit operator IdentityNameDto(string name) => new IdentityNameDto(name);

    public static implicit operator string(IdentityNameDto dto) => dto?.IdentityName;
}

public class NameDto : IEntityDto
{
    public string Name { get; set; }
}