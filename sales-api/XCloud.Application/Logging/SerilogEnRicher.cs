﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Serilog.Core;
using Serilog.Events;
using XCloud.Core;

namespace XCloud.Application.Logging;

internal class SerilogEnRicher : ILogEventEnricher
{
    public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
    {
        logEvent.Exception?.Demystify();

        EnrichAssembly(logEvent, propertyFactory);
        EnrichEnvironment(logEvent, propertyFactory);
        EnrichExtraProperties(logEvent, propertyFactory);
    }

    private void EnrichExtraProperties(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
    {
        var provider = EngineContext.Current.ServiceProvider;

        if (provider == null)
        {
            return;
        }

        using var s = provider.CreateScope();

        var providers = s.ServiceProvider.GetServices<ILoggingExtraPropertiesProvider>().ToArray();

        foreach (var p in providers)
        {
            var properties = p.GetExtraProperties();

            foreach (var prop in properties)
            {
                if (string.IsNullOrWhiteSpace(prop.Key) || string.IsNullOrWhiteSpace(prop.Value))
                    continue;

                logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty(name: prop.Key, value: prop.Value));
            }
        }
    }

    private void EnrichAssembly(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
    {
        var assembly = (Assembly.GetEntryAssembly() ?? Assembly.GetCallingAssembly())!;

        var assemblyName = assembly.GetName();
        logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("AssemblyName", assemblyName.FullName));

        var version = assemblyName.Version;
        if (version != null)
        {
            string versionStr = $"{version.Major}.{version.Minor}.{version.Build}";
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("AssemblyVersion", versionStr));
        }
    }

    private void EnrichEnvironment(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
    {
        var os = Environment.OSVersion!;

        var platformName = Enum.GetName<PlatformID>(os.Platform) ?? "unknown";
        logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("platform_name", platformName));
        logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("platform_version", os.VersionString));
    }
}