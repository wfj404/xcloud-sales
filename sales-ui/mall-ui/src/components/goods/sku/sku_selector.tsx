import { SelectProps } from 'antd';
import { CSSProperties } from 'react';

import XSearchSelector, {
  SearchSelectorItem,
} from '@/components/common/search_selector';
import { getSkuName } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { SkuDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export default ({
  selectedSku,
  onChange,
  style,
  props,
}: {
  selectedSku?: SkuDto;
  onChange?: (d?: SkuDto) => void;
  style?: CSSProperties;
  props?: SelectProps;
}) => {
  const queryImpl = async (kwd: string): Promise<SearchSelectorItem[]> => {
    if (isStringEmpty(kwd)) {
      return [];
    }

    const res = await apiClient.mallAdmin.skuQuerySkuForSelection({
      Keywords: kwd,
      Take: 20,
    });

    const options = res?.data?.Data?.map((x) => ({
      label: getSkuName(x) || '--',
      value: x.Id || '--',
    }));

    return options || [];
  };

  return (
    <>
      <XSearchSelector
        style={style}
        props={props}
        onSearch={async (x) => await queryImpl(x)}
        placeholder="搜索商品..."
        value={selectedSku?.Id || undefined}
        initItem={
          selectedSku == null
            ? undefined
            : {
                label: getSkuName(selectedSku) || '',
                value: selectedSku.Id || '',
              }
        }
        onChange={(x) => {
          onChange &&
            onChange(
              x == undefined ? undefined : { Id: x.value, Name: x.label },
            );
        }}
      />
    </>
  );
};
