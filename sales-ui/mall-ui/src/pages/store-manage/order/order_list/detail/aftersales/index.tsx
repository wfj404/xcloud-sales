import { Col, Row, Spin, Tag } from 'antd';
import { useEffect, useState } from 'react';

import { OrderStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { AfterSalesDto, OrderDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XComment from './comment';
import XGoods from './goods';
import XSummary from './summary';

export default ({ order }: { order: OrderDto }) => {
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<AfterSalesDto>({});

  const queryOrder = () => {
    if (
      isStringEmpty(order.Id) ||
      order.OrderStatusId != OrderStatus.AfterSales
    ) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .afterSaleGetByOrderId({ Id: order.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    order && queryOrder();
  }, [order]);

  if (order.OrderStatusId != OrderStatus.AfterSales) {
    return null;
  }

  return (
    <>
      <div
        style={{
          marginBottom: 10,
        }}
      >
        <div style={{ marginBottom: 10 }}>
          <Tag color="red">售后中</Tag>
        </div>
        <div
          style={{
            padding: 10,
            backgroundColor: 'rgb(250,250,250)',
            borderRadius: 1,
            border: '3px dashed black',
          }}
        >
          <Spin spinning={loading}>
            <Row gutter={10}>
              <Col span={24}>
                <XSummary model={data} />
                <XGoods model={data} />
                <XComment model={data} />
              </Col>
              <Col span={8}></Col>
            </Row>
          </Spin>
        </div>
      </div>
    </>
  );
};
