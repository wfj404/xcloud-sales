import { Space } from 'antd-mobile';

import { PlaceOrderRequestDto } from '@/utils/swagger';
import { Box, TextField } from '@mui/material';

export default ({
  model,
  onChange,
}: {
  model: PlaceOrderRequestDto;
  onChange: (e: PlaceOrderRequestDto) => void;
}) => {
  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Box sx={{ py: 1 }}>
          <TextField
            multiline
            label="备注"
            rows={1}
            fullWidth
            value={model.Remark}
            onChange={(e) => {
              onChange({
                ...model,
                Remark: e.target.value,
              });
            }}
          />
        </Box>
      </Space>
    </>
  );
};
