using System;
using Volo.Abp.Auditing;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Messenger.Domain.Messenger;

public class OnlineStatus : PlatformBaseEntity, IHasCreationTime
{
    public string UserId { get; set; }
    public string DeviceId { get; set; }
    public string ServerInstanceId { get; set; }
    public DateTime PingTime { get; set; }
    public DateTime CreationTime { get; set; }
}