import { OrderStatus } from '@/utils/biz';
import { OrderDto } from '@/utils/swagger';
import { Box, Stack } from '@mui/material';

import XAftersale from './aftersales';
import XCancelButton from './cancel';
import XConfirm from './confirm';
import XPayment from './payment';

export default function ({ model, ok }: { model: OrderDto; ok: () => void }) {
  const triggerReload = () => ok && ok();

  if (model == null) {
    return null;
  }

  if (model.OrderStatusId == OrderStatus.Closed) {
    return null;
  }

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          px: 3,
        }}
      >
        <Stack direction={'row'} spacing={2}>
          <XCancelButton
            model={model}
            ok={() => {
              triggerReload();
            }}
          />

          <XConfirm
            model={model}
            ok={() => {
              triggerReload();
            }}
          />
          <XAftersale
            model={model}
            ok={() => {
              triggerReload();
            }}
          />
          <XPayment
            model={model}
            ok={() => {
              triggerReload();
            }}
          />
        </Stack>
      </Box>
    </>
  );
}
