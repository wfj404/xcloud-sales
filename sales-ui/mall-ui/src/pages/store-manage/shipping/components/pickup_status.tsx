import { PickupRecordDto } from '@/utils/swagger';
import { Badge } from 'antd';

export default ({ data }: { data: PickupRecordDto }) => {
  const x = data;

  if (data.ReservationApproved == undefined) {
    return <Badge status={'default'} text={'待审批'} />;
  }

  if (!x.ReservationApproved) {
    return <Badge status={'error'} text={'审批未通过'} />;
  }

  if (!x.Prepared) {
    return <Badge status={'default'} text={'准备中'} />;
  }

  if (!x.Picked) {
    return <Badge status={'default'} text={'待取货'} />;
  } else {
    return <Badge status={'success'} text={`已取货`} />;
  }
};
