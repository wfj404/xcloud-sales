﻿using System;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.AspNetMvc.Exceptions;

[ExposeServices(typeof(IErrorInfoConverter), typeof(AspNetCoreErrorInfoConverter))]
public class AspNetCoreErrorInfoConverter : IErrorInfoConverter, ISingletonDependency
{
    public int Order => default;

    private string GetArgumentExceptionDetail(ArgumentException argumentException)
    {
        var sb = new StringBuilder();

        sb.AppendLine($"param-name:{argumentException.ParamName}");
        sb.AppendLine($"message:{argumentException.Message}");

        return sb.ToString();
    }

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        if (e is BusinessException businessException)
        {
            return new ErrorInfo
            {
                Message = businessException.Message,
                Details = businessException.Details,
                Data = businessException.Data,
            };
        }
        else if (e is EntityNotFoundException entityNotFoundException)
        {
            return new ErrorInfo
            {
                Message = "data is not exist",
                Details = entityNotFoundException.Message
            };
        }
        else if (e is ArgumentNullException argumentNullException)
        {
            return new ErrorInfo
            {
                Message = $"参数错误:{nameof(ArgumentNullException)}-{GetArgumentExceptionDetail(argumentNullException)}",
                Code = "-100",
                HttpStatusCode = HttpStatusCode.BadRequest
            };
        }
        else if (e is ArgumentException argumentException)
        {
            return new ErrorInfo
            {
                Message = $"参数错误:{nameof(ArgumentException)}-{GetArgumentExceptionDetail(argumentException)}",
                Code = "-100",
                HttpStatusCode = HttpStatusCode.BadRequest
            };
        }
        else if (e is NotImplementedException || e is NotSupportedException)
        {
            return new ErrorInfo
            {
                Message = "该功能未实现或者暂不支持",
                Details = e.Message,
                HttpStatusCode = HttpStatusCode.NotImplemented
            };
        }

        return null;
    }
}