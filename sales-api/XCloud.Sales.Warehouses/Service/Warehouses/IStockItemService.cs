﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Service.Warehouses;

public interface IStockItemService : ISalesAppService
{
    Task<StockItemDto[]> AttachStockAsync(StockItemDto[] data);

    Task<StockItemDto[]> AttachSkusAsync(StockItemDto[] data);

    Task<PagedResponse<StockItemDto>> QueryItemPagingAsync(QueryWarehouseStockItemInput dto);
}

public class StockItemService : SalesAppService, IStockItemService
{
    private readonly ISalesRepository<Stock> _salesRepository;

    public StockItemService(ISalesRepository<Stock> salesRepository)
    {
        _salesRepository = salesRepository;
    }

    private IQueryable<Stock> BuildStockQuery(DbContext db, QueryWarehouseStockPagingInput dto)
    {
        var query = db.Set<Stock>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.SerialNo))
            query = query.Where(x => x.No == dto.SerialNo);

        if (!string.IsNullOrWhiteSpace(dto.SupplierId))
            query = query.Where(x => x.SupplierId == dto.SupplierId);

        if (!string.IsNullOrWhiteSpace(dto.WarehouseId))
            query = query.Where(x => x.WarehouseId == dto.WarehouseId);

        if (dto.Approved != null)
            query = query.Where(x => x.Approved == dto.Approved.Value);

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime <= dto.EndTime.Value);

        return query;
    }

    private IQueryable<StockItem> BuildStockItemQuery(DbContext db, QueryWarehouseStockItemInput dto)
    {
        var query = db.Set<StockItem>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.SkuId))
            query = query.Where(x => x.SkuId == dto.SkuId);

        if (dto.RunningOut != null)
        {
            if (dto.RunningOut.Value)
            {
                query = query.Where(x => x.DeductQuantity >= x.Quantity);
            }
            else
            {
                query = query.Where(x => x.DeductQuantity < x.Quantity);
            }
        }

        return query;
    }

    public async Task<PagedResponse<StockItemDto>> QueryItemPagingAsync(QueryWarehouseStockItemInput dto)
    {
        var db = await _salesRepository.GetDbContextAsync();

        var stockQuery = BuildStockQuery(db, dto);
        var itemQuery = BuildStockItemQuery(db, dto);

        var query = from item in itemQuery
            join s in stockQuery
                on item.StockId equals s.Id into stockGroup
            from stock in stockGroup.DefaultIfEmpty()
            select new { item, stock };

        var count = await query.CountOrDefaultAsync(dto);

        StockItemDto BuildResponse(StockItem item, Stock stockOrNull)
        {
            var itemDto = ObjectMapper.Map<StockItem, StockItemDto>(item);

            if (stockOrNull != null)
                itemDto.Stock = ObjectMapper.Map<Stock, StockDto>(stockOrNull);

            return itemDto;
        }

        var stocks = await query.OrderByDescending(x => x.stock.CreationTime).PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var stockItems = stocks.Select(x => BuildResponse(x.item, x.stock)).ToArray();

        return new PagedResponse<StockItemDto>(stockItems, dto, count);
    }

    public async Task<StockItemDto[]> AttachSkusAsync(StockItemDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _salesRepository.GetDbContextAsync();

        var skuIds = data.Select(x => x.SkuId).ToArray();

        var query = from sku in db.Set<Sku>().AsNoTracking()
            join g in db.Set<Goods>().AsNoTracking()
                on sku.GoodsId equals g.Id into goodsGrouping
            from goods in goodsGrouping.DefaultIfEmpty()
            select new { sku, goods };

        var goodsList = await query.Where(x => skuIds.Contains(x.sku.Id)).ToArrayAsync();

        foreach (var m in data)
        {
            var item = goodsList.FirstOrDefault(x => x.sku.Id == m.SkuId);
            if (item == null)
                continue;

            m.Sku = ObjectMapper.Map<Sku, SkuDto>(item.sku);
            m.Goods = item.goods?.MapTo<Goods, GoodsDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<StockItemDto[]> AttachStockAsync(StockItemDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _salesRepository.GetDbContextAsync();

        var ids = data.Select(x => x.StockId).Distinct().ToArray();

        var allStocks = await db.Set<Stock>().AsNoTracking()
            .Where(x => ids.Contains(x.Id))
            .ToArrayAsync();
        foreach (var m in data)
        {
            var stock = allStocks.FirstOrDefault(x => x.Id == m.StockId);
            if (stock == null)
                continue;
            m.Stock = ObjectMapper.Map<Stock, StockDto>(stock);
        }

        return data;
    }
}