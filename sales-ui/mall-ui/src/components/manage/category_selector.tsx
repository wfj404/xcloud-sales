import { useRequest } from 'ahooks';
import { TreeSelect } from 'antd';
import React, { useEffect } from 'react';

import { getFlatNodes, getTreeSelectOptionsV2 } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { CategoryDto } from '@/utils/swagger';
import { getPinyinShortStyle } from '@/utils/utils';

export default ({
  value,
  onChange,
}: {
  value?: string | null;
  onChange?: (e: CategoryDto | undefined) => void;
}) => {
  const queryCategory = useRequest(apiClient.mallAdmin.categoryQueryAntdTree, {
    manual: true,
  });

  const categoryTree = queryCategory.data?.data?.Data || [];

  const flatNodes = categoryTree.flatMap((x) => getFlatNodes(x));

  const allKeys = [...new Set(flatNodes.map((x) => x.key || ''))];

  const options = categoryTree.map((x) =>
    getTreeSelectOptionsV2(
      x,
      (d) => `${d.title}-${getPinyinShortStyle(d.title || '') || 'N/A'}`,
    ),
  );

  useEffect(() => {
    queryCategory.run({});
  }, []);

  return (
    <>
      <TreeSelect
        loading={queryCategory.loading}
        value={value}
        onChange={(e) => {
          onChange && onChange(flatNodes.find((x) => x.key == e)?.raw_data);
        }}
        placeholder="输入拼音缩写可以搜索..."
        treeData={options}
        treeExpandedKeys={allKeys}
        treeDefaultExpandAll
        allowClear
        showSearch
        treeNodeFilterProp="label"
        filterTreeNode={undefined}
      />
    </>
  );
};
