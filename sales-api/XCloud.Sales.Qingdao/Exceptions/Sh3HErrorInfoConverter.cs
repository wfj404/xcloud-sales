﻿using System;
using Microsoft.AspNetCore.Http;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.Sales.Qingdao.Exceptions;

[ExposeServices(typeof(IErrorInfoConverter))]
public class Sh3HErrorInfoConverter : IErrorInfoConverter, ITransientDependency
{
    public int Order => int.MaxValue;

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        if (e is McsException mcsException)
        {
            return new ErrorInfo()
            {
                Message = mcsException.Message ?? nameof(McsException),
                Code = "mcs"
            };
        }

        if (e is WapException wapException)
        {
            return new ErrorInfo()
            {
                Message = wapException.Message ?? nameof(WapException),
                Code = "wap"
            };
        }

        return null;
    }
}