import { Avatar, Button, ConfigProvider } from 'antd';
import { useEffect, useMemo } from 'react';
import { history, Link, useLocation, useOutlet, useSnapshot } from 'umi';

import ManagerLoginRequired from '@/components/login/manager';
import UserLoginRequired from '@/components/login/user';
import XAppList from '@/layouts/app_list';
import { storeState, StoreType } from '@/store';
import {
  getFilteredMenuDataOrigin,
  getUserName,
  isSh3H,
  managerHasPermission,
} from '@/utils/biz';
import { config } from '@/utils/config';
import { resolveAvatar, resolveUrl } from '@/utils/storage';
import {
  StoreManagerGrantedPermissionResponse,
  UserDto,
} from '@/utils/swagger';
import { isInIframe, isStringEmpty, simpleString } from '@/utils/utils';
import { PayCircleOutlined, ToolOutlined } from '@ant-design/icons';
import { PageContainer, ProLayout } from '@ant-design/pro-components';

import sh3h_app_list from '../sh3h/app_list';
import Sh3hStoreSelector from '../sh3h/store-manager/current_store';
import sh3h_menu from '../sh3h/store-manager/menu';
import XStoreSelector from './current_store';
import default_menu from './menu';

export default () => {
  const children = useOutlet();
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const menus = isSh3H() ? sh3h_menu : default_menu;

  const filteredMenuData = getFilteredMenuDataOrigin(menus, (x) =>
    managerHasPermission(
      (app.managerPermissions as StoreManagerGrantedPermissionResponse) || {},
      x,
    ),
  );

  useEffect(() => {
    console.log('store-manager-menu-data', filteredMenuData);
  }, [filteredMenuData]);

  const settings = app.mallSettings || {};
  const logo_url =
    useMemo(() => {
      const urls = [
        settings.FullLogoStorageData,
        settings.SimpleLogoStorageData,
      ].map((x) => resolveUrl(x, { height: 100 }));
      return urls.filter((x) => !isStringEmpty(x)).at(0);
    }, [settings]) || config.app.logo.normal;

  const nick_name = simpleString(
    getUserName(app.storeManager?.User as UserDto) || '--',
    8,
  );
  const avatar_url = useMemo(
    () => resolveAvatar(app.storeManager?.User?.Avatar),
    [app.storeManager?.User?.Avatar],
  );

  return (
    <ConfigProvider
      theme={{
        token: {
          //borderRadius: 0,
        },
      }}
    >
      <UserLoginRequired>
        <ManagerLoginRequired>
          <ProLayout
            pure={isInIframe()}
            theme="light"
            layout="mix"
            //fixedHeader
            splitMenus
            title={isSh3H() ? '二级单位' : '门店端'}
            logo={
              <img
                style={{}}
                alt=""
                src={logo_url}
                onClick={() => {
                  history.push({
                    pathname: '/store-manage',
                  });
                }}
              />
            }
            appList={isSh3H() ? sh3h_app_list : XAppList}
            menu={{
              request: async () => filteredMenuData,
              params: filteredMenuData,
            }}
            menuItemRender={(x, dom) => {
              return <Link to={x.path || '/'}>{dom}</Link>;
            }}
            location={location}
            waterMarkProps={{
              content: `门店端：${nick_name}`,
            }}
            avatarProps={{
              icon: <Avatar src={avatar_url}>{nick_name}</Avatar>,
              size: 'small',
              title: nick_name,
              onClick: (e) => {
                console.log(e);
                history.push({
                  pathname: '/store-manage/ucenter',
                });
              },
            }}
            actionsRender={() => [
              isSh3H() || (
                <Button
                  size={'small'}
                  type={'dashed'}
                  icon={<ToolOutlined />}
                  onClick={() => {
                    history.push({
                      pathname: '/store-manage/order-group',
                    });
                  }}
                >
                  下单助手
                </Button>
              ),
              isSh3H() || (
                <Button
                  size={'small'}
                  type={'dashed'}
                  icon={<PayCircleOutlined />}
                  onClick={() => {
                    history.push({
                      pathname: '/store-manage/cashier',
                    });
                  }}
                >
                  收银台
                </Button>
              ),
              isSh3H() || <XStoreSelector />,
              isSh3H() && <Sh3hStoreSelector />,
            ]}
          >
            <PageContainer>{children}</PageContainer>
          </ProLayout>
        </ManagerLoginRequired>
      </UserLoginRequired>
    </ConfigProvider>
  );
};
