﻿using System;
using System.Linq;
using Elasticsearch.Net;
using Nest;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Helper;

namespace XCloud.Elasticsearch.Core;

public class ElasticSearchClient : IObjectAccessor<IElasticClient>
{
    public ElasticSearchClient(ElasticSearchOption option)
    {
        //
    }

    public ElasticSearchClient(string[] hosts)
    {
        if (ValidateHelper.IsEmptyCollection(hosts))
            throw new ArgumentNullException(nameof(hosts));

        var urls = hosts.Select(x => new Uri(x)).ToArray();

        var pool = new SniffingConnectionPool(uris: urls);

        var settings = new ConnectionSettings(connectionPool: pool);
        //settings.BasicAuthentication("", "");
        settings.DisableDirectStreaming(true);
        settings.OnRequestCompleted(x =>
        {
            //
        });

        this.Value = new ElasticClient(connectionSettings: settings);
    }

    public IElasticClient Value { get; }
}