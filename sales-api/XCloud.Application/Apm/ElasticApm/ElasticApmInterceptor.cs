﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Elastic.Apm.Api;
using JetBrains.Annotations;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.DependencyInjection;
using Volo.Abp.DynamicProxy;
using XCloud.Core.Extension;

namespace XCloud.Application.Apm.ElasticApm;

[ExposeServices(typeof(ElasticApmInterceptor), typeof(IAbpInterceptor))]
public class ElasticApmInterceptor : IAbpInterceptor, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IMemoryCache _memoryCache;

    public ElasticApmInterceptor(IServiceProvider serviceProvider, IMemoryCache memoryCache)
    {
        _serviceProvider = serviceProvider;
        _memoryCache = memoryCache;
    }

    private bool ApmEnabledFor(IAbpMethodInvocation invocation)
    {
        var m = invocation.Method;

        return _memoryCache.GetOrCreate(m, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);

            var config = m.GetCustomAttributesList<ApmAttribute>().FirstOrDefault();

            if (config != null)
            {
                return !config.Disabled;
            }

            return m.IsPublic && (m.ReturnType.IsTask() || m.ReturnType.IsValueTask());
        });
    }

    private bool ApmEnabled()
    {
        var key = $"{nameof(ElasticApmInterceptor)}.{nameof(ApmEnabled)}";

        var option = _memoryCache.GetOrCreate(key, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1);

            return this._serviceProvider.GetRequiredService<IConfiguration>().GetApmOptionOrDefault();
        });

        option ??= new ApmOption();

        return option.Enabled && option.LogServiceEnabled;
    }

    [CanBeNull]
    private ITransaction GetCurrentTransactionOrNull()
    {
        if (global::Elastic.Apm.Agent.IsConfigured)
        {
            return global::Elastic.Apm.Agent.Tracer.CurrentTransaction;
        }

        return null;
    }

    public async Task InterceptAsync(IAbpMethodInvocation invocation)
    {
        var transaction = GetCurrentTransactionOrNull();

        if (transaction != null && ApmEnabled() && ApmEnabledFor(invocation))
        {
            var targetMethod = invocation.Method;

            var span = transaction.StartSpan(
                name: $"{targetMethod.DeclaringType?.Name}.{targetMethod.Name}",
                type: ApiConstants.TypeApp);

            try
            {
#if DEBUG
                //var logger = this._serviceProvider.GetRequiredService<ILogger<ElasticApmInterceptor>>();
                //logger.LogInformation($"apm:create transaction span:{span.Name}");
#endif
                var paramsArray = targetMethod.GetParameters()
                    .Select(x => $"{x.ParameterType.Name} {x.Name}")
                    .ToArray();
                var paramsData = string.Join(",", paramsArray);
                span.SetLabel(nameof(paramsData), paramsData);

                await invocation.ProceedAsync();

                //log return data
                //todo
            }
            catch (Exception e)
            {
                span.CaptureException(e);
                throw;
            }
            finally
            {
                span.End();
            }
        }
        else
        {
            //如果没有开启apm
            await invocation.ProceedAsync();
        }
    }
}