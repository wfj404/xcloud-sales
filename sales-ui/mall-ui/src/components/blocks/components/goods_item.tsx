import { useState } from 'react';

import XEmpty from '@/components/common/empty';
import XItem from '@/components/goods/card/item';
import XDetailPreview from '@/components/goods/detail/detailPreview';
import { GoodsDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

export default ({ goods }: { goods: GoodsDto[] }) => {
  const [detailId, _detailId] = useState<string | undefined>(undefined);

  if (isArrayEmpty(goods)) {
    return <XEmpty />;
  }

  return (
    <>
      <XDetailPreview
        detailId={detailId}
        onClose={() => {
          _detailId('');
        }}
      />
      <Box
        sx={{
          px: 1,
        }}
      >
        {goods?.map((x, index) => (
          <Box
            key={index}
            onClick={() => {
              x.Id && _detailId(x.Id);
            }}
            sx={{
              my: 1,
            }}
          >
            <XItem model={x} count={3} />
          </Box>
        ))}
      </Box>
    </>
  );
};
