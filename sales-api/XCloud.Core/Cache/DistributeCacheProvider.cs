﻿using Microsoft.Extensions.Caching.Distributed;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using XCloud.Core.Dto;
using XCloud.Core.Json;

namespace XCloud.Core.Cache;

public class DistributeCacheProvider : ICacheProvider
{
    private readonly IDistributedCache _distributedCache;
    private readonly IJsonDataSerializer _dataSerializer;
    private readonly CacheProviderOption _cacheProviderOption;

    public DistributeCacheProvider(
        IConfiguration configuration,
        IDistributedCache distributedCache,
        IJsonDataSerializer dataSerializer,
        ILogger<DistributeCacheProvider> logger)
    {
        _distributedCache = distributedCache;
        _dataSerializer = dataSerializer;
        Logger = logger;
        _cacheProviderOption = configuration.GetRequiredCacheProviderOption();
    }

    public ILogger Logger { get; }

    [NotNull]
    private string BuildKey(string key)
    {
        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        if (!string.IsNullOrWhiteSpace(_cacheProviderOption.KeyPrefix))
        {
            return $"{_cacheProviderOption.KeyPrefix}.{key.TrimStart('.')}";
        }

        return key;
    }

    private DistributedCacheEntryOptions BuildOptions(TimeSpan? expire)
    {
        if (expire == null)
        {
            if (this._cacheProviderOption.DefaultExpiredSeconds > 0)
            {
                expire = TimeSpan.FromSeconds(this._cacheProviderOption.DefaultExpiredSeconds);
            }
        }

        expire ??= TimeSpan.FromMinutes(3);

        var option = new DistributedCacheEntryOptions
        {
            AbsoluteExpirationRelativeToNow = expire.Value
        };

        return option;
    }

    public async Task<CacheResult<T>> GetAsync<T>(string key)
    {
        key = BuildKey(key);

        var json = await _distributedCache.GetStringAsync(key);

        if (string.IsNullOrWhiteSpace(json))
        {
            return new CacheResult<T>().SetError("empty-data");
        }

        var res = _dataSerializer.DeserializeCacheResultFromString<T>(json);

        return res;
    }

    public async Task SetAsync<T>(string key, T data, TimeSpan? expire)
    {
        key = BuildKey(key);

        var json = _dataSerializer.SerializeCacheResultToString(data);

        await _distributedCache.SetStringAsync(key, json, BuildOptions(expire));
    }

    public async Task RemoveAsync(string key)
    {
        key = BuildKey(key);

        await _distributedCache.RemoveAsync(key);
    }
}