using System;
using System.Threading.Tasks;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Service.Warehouses;

public interface IStockUsageHistoryService : ISalesStringCrudAppService<StockUsageHistory, StockUsageHistoryDto,
    QueryStockUsageHistoryPagingInput>
{
    //
}

public class StockUsageHistoryService : SalesStringCrudAppService<StockUsageHistory, StockUsageHistoryDto,
        QueryStockUsageHistoryPagingInput>,
    IStockUsageHistoryService
{
    public StockUsageHistoryService(ISalesRepository<StockUsageHistory> repository) : base(repository)
    {
        //
    }

    public override Task DeleteByIdAsync(string id)
    {
        throw new NotImplementedException();
    }

    public override Task<StockUsageHistory> UpdateAsync(StockUsageHistoryDto dto)
    {
        throw new NotImplementedException();
    }

    public override Task<StockUsageHistory> InsertAsync(StockUsageHistoryDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.StoreId))
            throw new ArgumentNullException(nameof(dto.StoreId));

        if (string.IsNullOrWhiteSpace(dto.StockItemId))
            throw new ArgumentNullException(nameof(dto.StockItemId));

        if (dto.Quantity <= 0)
            throw new ArgumentNullException(nameof(dto.Quantity));
        
        return base.InsertAsync(dto);
    }

    protected override async Task SetFieldsBeforeInsertAsync(StockUsageHistory entity)
    {
        await base.SetFieldsBeforeInsertAsync(entity);
    }
}