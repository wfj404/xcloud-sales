﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Sales.Application.Dto;

public class ShoppingCartSettings : IEntityDto
{
    //
}

public class DeliverySettings : IEntityDto
{
    public bool ShippingFeeFree { get; set; }

    public int MultipleDeliveryTemplateCalculateType { get; set; }
}

public class GoodsSettings : IEntityDto
{
    public bool DisplayPriceForGuest { get; set; } = false;
}

public class OrderSettings : IEntityDto
{
    public bool PlaceOrderDisabled { get; set; } = false;

    public int? ReviewEntryExpiredInDays { get; set; }
}

public class AfterSalesSettings : IEntityDto
{
    public bool AfterSaleDisabled { get; set; } = false;
}

public class PaymentSettings : IEntityDto
{
    //
}

[TypeIdentityName("home-page-blocks")]
public class HomePageBlocksDto : ISettings
{
    public string Blocks { get; set; }
}

[TypeIdentityName("sales-settings")]
public class MallSettingsDto : ISettings
{
    public string StoreOfficialName { get; set; }
    public string StoreOfficialEnglishName { get; set; }
    public string StoreNickName { get; set; }
    public string StoreEnglishName { get; set; }
    public string FooterSlogan { get; set; }
    public string FilingNumber { get; set; }
    public string FullLogoStorageData { get; set; }
    public string SimpleLogoStorageData { get; set; }

    public string HomePageNotice { get; set; }
    public string HomeSliderImages { get; set; }

    public string PlaceOrderNotice { get; set; }
    public string LoginNotice { get; set; }
    public string RegisterNotice { get; set; }
    public string GoodsDetailNotice { get; set; }

    private PaymentSettings _paymentSettings;

    [NotNull]
    public PaymentSettings PaymentSettings
    {
        get => _paymentSettings ??= new PaymentSettings();
        set => _paymentSettings = value;
    }

    private GoodsSettings _goodsSettings;

    [NotNull]
    public GoodsSettings GoodsSettings
    {
        get => _goodsSettings ??= new GoodsSettings();
        set => _goodsSettings = value;
    }

    private OrderSettings _orderSettings;

    [NotNull]
    public OrderSettings OrderSettings
    {
        get => _orderSettings ??= new OrderSettings();
        set => _orderSettings = value;
    }

    private AfterSalesSettings _afterSalesSettings;

    [NotNull]
    public AfterSalesSettings AfterSalesSettings
    {
        get => _afterSalesSettings ??= new AfterSalesSettings();
        set => _afterSalesSettings = value;
    }

    private ShoppingCartSettings _shoppingCartSettings;

    [NotNull]
    public ShoppingCartSettings ShoppingCartSettings
    {
        get => _shoppingCartSettings ??= new ShoppingCartSettings();
        set => _shoppingCartSettings = value;
    }

    private DeliverySettings _deliverySettings;

    [NotNull]
    public DeliverySettings DeliverySettings
    {
        get => _deliverySettings ??= new DeliverySettings();
        set => _deliverySettings = value;
    }
}