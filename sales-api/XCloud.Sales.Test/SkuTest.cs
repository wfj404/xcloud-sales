﻿using System.Linq;
using XCloud.Sales.Application.Extension;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Test;

[TestClass]
public class SkuTest
{
    [TestMethod]
    public void GenerateCombination()
    {
        var mappings = new[] {
            new SpecItemDto { SpecId=1,SpecValueId=1 },
            new SpecItemDto { SpecId=1,SpecValueId=2 },
            new SpecItemDto { SpecId=1,SpecValueId=3 },
            new SpecItemDto { SpecId=1,SpecValueId=4 },
            new SpecItemDto { SpecId=1,SpecValueId=5 },

            new SpecItemDto { SpecId=2,SpecValueId=6 },
            new SpecItemDto { SpecId=2,SpecValueId=7 },

            new SpecItemDto { SpecId=3,SpecValueId=8 },

            new SpecItemDto { SpecId=4,SpecValueId=9 },
            new SpecItemDto { SpecId=4,SpecValueId=10 },
        };

        string GetString(SpecItemDto[] data) =>
            string.Join('\n', data.Select(x => $"{x.SpecId}={x.SpecValueId}"));

        var combines = mappings.GenerateCombination().Select(GetString).ToArray();

    }
}