import { Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';

import { McsInvoiceRequestItemDto } from '@/utils/swagger_qingdao';
import { formatMoney, isArrayEmpty } from '@/utils/utils';

export default ({ items }: { items?: McsInvoiceRequestItemDto[] }) => {
  if (!items || isArrayEmpty(items)) {
    return <h1>empty items</h1>;
  }

  const columns: ColumnType<McsInvoiceRequestItemDto>[] = [
    {
      title: '金额',
      render: (d, x) => (
        <Tag color={'warning'}>{formatMoney(x.Price || 0)}</Tag>
      ),
    },
    {
      title: '账单号',
      render: (d, x) => <Tag>{x.McsBizOrderId}</Tag>,
    },
    {
      title: '业务描述',
      render: (d, x) => <Tag>{x.Description}</Tag>,
    },
  ];

  return (
    <>
      <div style={{}}>
        <Table
          size={'small'}
          bordered
          columns={columns}
          dataSource={items || []}
          pagination={false}
        />
      </div>
    </>
  );
};
