﻿using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Options;
using Volo.Abp.Json.SystemTextJson;
using XCloud.Core.Configuration;

namespace XCloud.Core.Json.TextJson;

public class TextJsonDataSerializer : IJsonDataSerializer
{
    private readonly JsonSerializerOptions _jsonSerializerSettings;

    public TextJsonDataSerializer(
        AppConfig appConfig,
        IOptions<AbpSystemTextJsonSerializerOptions> options)
    {
        JsonEncoding = appConfig.AppEncoding;
        _jsonSerializerSettings = options.Value?.JsonSerializerOptions;
        if (this._jsonSerializerSettings == null)
            throw new ArgumentNullException(nameof(this._jsonSerializerSettings));
    }

    private Encoding JsonEncoding { get; }

    public T DeserializeFromBytes<T>(byte[] serializedObject)
    {
        try
        {
            var obj = JsonSerializer.Deserialize<T>(serializedObject, _jsonSerializerSettings);

            return obj;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromBytes), e);
        }
    }

    public object DeserializeFromBytes(byte[] serializedObject, Type target)
    {
        try
        {
            var obj = JsonSerializer.Deserialize(serializedObject, target, _jsonSerializerSettings);

            return obj;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromBytes), e);
        }
    }

    public T DeserializeFromString<T>(string serializedObject)
    {
        try
        {
            var bs = JsonEncoding.GetBytes(serializedObject);

            return DeserializeFromBytes<T>(bs);
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromString), e);
        }
    }

    public object DeserializeFromString(string serializedObject, Type target)
    {
        try
        {
            var bs = JsonEncoding.GetBytes(serializedObject);

            return DeserializeFromBytes(bs, target);
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromString), e);
        }
    }

    public byte[] SerializeToBytes(object item)
    {
        if (item == null)
            throw new ArgumentNullException(nameof(item));
        try
        {
            var bs = JsonSerializer.SerializeToUtf8Bytes(item, item.GetType(), _jsonSerializerSettings);

            return bs;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(SerializeToBytes), e);
        }
    }

    public string SerializeToString(object item)
    {
        try
        {
            var bs = SerializeToBytes(item);

            var jsonString = JsonEncoding.GetString(bs);

            return jsonString;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(SerializeToString), e);
        }
    }
}