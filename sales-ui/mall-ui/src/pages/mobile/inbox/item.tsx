import personImg from '@/assets/static/images/avatars/avatar_default.jpg';
import XLoading from '@/components/common/loading/dots';
import { apiClient } from '@/utils/client';
import { NotificationDto } from '@/utils/swagger';
import { Avatar, Box, CardActionArea, Typography } from '@mui/material';
import { Badge } from 'antd-mobile';
import { useState } from 'react';
import { handleResponse } from '@/utils/utils';

export default function({
                          model,
                          ok,
                        }: {
  model: NotificationDto;
  ok: () => void;
}) {
  const [loading, _loading] = useState(false);

  const markAsRead = () => {
    if (model.Read) {
      return;
    }
    _loading(true);
    apiClient.mall
      .notificationUpdateStatus({ Id: model.Id, Read: true })
      .then((res) => {
        handleResponse(res, () => {
          ok && ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  return (
    <>
      {loading && <XLoading />}
      <CardActionArea
        sx={{
          py: 1,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
        onClick={() => {
          markAsRead();
        }}
      >
        <Avatar alt='Profile Picture' src={personImg} />
        <Box
          sx={{
            ml: 1,
            flexGrow: 1,
          }}
        >
          <Typography variant='subtitle2'>{model.Title || '--'}</Typography>
          <Typography
            variant='caption'
            color={'text.disabled'}
          >
            {model.Content || '--'}
          </Typography>
        </Box>
        <span>
          <Badge
            color={model.Read ? 'gray' : 'red'}
            content={Badge.dot}
          ></Badge>
        </span>
      </CardActionArea>
    </>
  );
}
