import XDetailDialog from '@/components/common/modal_pure';
import { isStringEmpty } from '@/utils/utils';

import XDetail from './index';

export default function ({
  detailId,
  onClose,
}: {
  detailId?: string | null;
  onClose: () => void;
}) {
  const open = !isStringEmpty(detailId);

  return (
    <>
      <XDetailDialog open={open} onClose={onClose}>
        {detailId && detailId.length > 0 && <XDetail goodsId={detailId} />}
      </XDetailDialog>
    </>
  );
}
