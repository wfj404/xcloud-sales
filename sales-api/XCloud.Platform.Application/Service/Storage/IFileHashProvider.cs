﻿using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Configuration;

namespace XCloud.Platform.Application.Service.Storage;

public interface IFileHashProvider
{
    string HashType { get; }

    Task<string> CalculateHashStringAsync(Stream stream);
}

[ExposeServices(typeof(IFileHashProvider))]
public class Md5FileHashProvider : IFileHashProvider, ISingletonDependency
{
    private readonly AppConfig _appConfig;

    public Md5FileHashProvider(AppConfig appConfig)
    {
        _appConfig = appConfig;
    }

    public string HashType => "md5";

    public async Task<string> CalculateHashStringAsync(Stream stream)
    {
        if (stream == null)
            throw new ArgumentNullException(nameof(stream));

        stream.EnsureOriginPosition();

        using var md5Algorithm = MD5.Create();
        var bytes = await md5Algorithm.ComputeHashAsync(stream);
        var hash = BitConverter.ToString(bytes);

        hash = hash.Replace("-", string.Empty).ToUpper();

        return hash;
    }
}