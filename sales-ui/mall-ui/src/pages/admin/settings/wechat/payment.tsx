import { Form, Input } from 'antd';

import { WechatPaymentOption } from '@/utils/swagger';

export default ({
  data,
  onChange,
}: {
  data: WechatPaymentOption;
  onChange: (e: WechatPaymentOption) => void;
}) => {
  return (
    <>
      <Form labelCol={{ span: 4 }}>
        <Form.Item label="MchId" tooltip="商户号">
          <Input
            value={data.MchId || ''}
            onChange={(e) => {
              onChange({ ...data, MchId: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label="ApiV3Key">
          <Input
            value={data.ApiV3Key || ''}
            onChange={(e) => {
              onChange({ ...data, ApiV3Key: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label="商户证书 SerialNumber">
          <Input
            value={data.MerchantCertificateSerialNumber || ''}
            onChange={(e) => {
              onChange({
                ...data,
                MerchantCertificateSerialNumber: e.target.value,
              });
            }}
          />
        </Form.Item>
        <Form.Item label="商户证书 PrivateKey">
          <Input.TextArea
            rows={10}
            value={data.MerchantCertificatePrivateKey || ''}
            onChange={(e) => {
              onChange({
                ...data,
                MerchantCertificatePrivateKey: e.target.value,
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
};
