namespace XCloud.Sales.Application.Domain.Mapping;

public interface IMappingTable : IEntity<int>
{
    string EntityId { get; set; }

    string EntityName { get; set; }

    string TargetId { get; set; }
    
    string TargetName { get; set; }
}

public interface IMappingSupported : IEntity<string>
{
    //
}