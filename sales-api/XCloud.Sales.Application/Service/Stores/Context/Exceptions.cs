﻿namespace XCloud.Sales.Application.Service.Stores.Context;

public class StoreIsRequiredException : SalesException
{
    public StoreIsRequiredException()
    {
        //
    }

    public StoreIsRequiredException(string message) : base(message: message)
    {
        //
    }
}