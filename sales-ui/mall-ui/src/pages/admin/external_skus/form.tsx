import { useRequest } from 'ahooks';
import { Button, Input, message, Modal, Space, Table, Upload } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { ApiResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { httpClient } from '@/utils/http';
import { ExternalSkuDto } from '@/utils/swagger';
import { formatMoney, handleResponse, isStringEmpty } from '@/utils/utils';
import { ImportOutlined, UploadOutlined } from '@ant-design/icons';

export default ({
                  show,
                  close,
                  ok,
                }: {
  show: boolean;
  close: () => void;
  ok: () => void;
}) => {
  const [items, _items] = useState<ExternalSkuDto[]>([]);
  const [systemName, _systemName] = useState<string>('');

  const upload_impl = (f: File) => {
    const formdata = new FormData();
    formdata.append('collection', f, f.name);
    return httpClient.post(
      '/api/mall-admin/external-sku/upload-and-parse-excel',
      formdata,
    );
  };

  const upload_file = useRequest(upload_impl, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        const datalist =
          (data.data as ApiResponse<ExternalSkuDto[]>)?.Data || [];
        if (datalist.length <= 0) {
          message.warning('表格中没有数据');
        }
        _items(datalist);
      });
    },
  });

  const import_request = useRequest(apiClient.mallAdmin.externalSkuInsertMany, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        message.success('保存成功');
        ok();
      });
    },
  });

  useEffect(() => {
    if (!show) {
      _items([]);
    }
  }, [show]);

  const columns: ColumnType<ExternalSkuDto>[] = [
    {
      title: '唯一标识',
      render: (d, x) => `${x.ExternalSkuId}@${systemName || '--'}`,
    },
    {
      title: '规格名称',
      render: (d, x) => x.SkuName || '--',
    },
    {
      title: '价格',
      render: (d, x) => formatMoney(x.Price || 0),
    },
  ];

  return (
    <>
      <Modal
        open={show}
        onCancel={() => {
          close();
        }}
        onOk={() => {
          close();
        }}
        width={'95%'}
      >
        <Space direction='vertical' style={{ width: '100%' }}>
          <Space direction='horizontal'>
            <Upload
              fileList={[]}
              showUploadList={false}
              multiple={false}
              accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
              customRequest={(e) => {
                const file = e.file;
                file && upload_file.run(file as File);
              }}
            >
              <Button
                size='large'
                type='primary'
                icon={<UploadOutlined />}
                loading={upload_file.loading}
              >
                上传表格
              </Button>
            </Upload>
            <Input
              size='large'
              value={systemName}
              onChange={(e) => _systemName(e.target.value)}
              placeholder='数据分组'
            />
            <Button
              size='large'
              disabled={items.length <= 0 || isStringEmpty(systemName)}
              onClick={() => {
                const datalist = items.map<ExternalSkuDto>((x) => ({
                  ...x,
                  SystemName: systemName,
                }));

                import_request.run(datalist);
              }}
              icon={<ImportOutlined />}
              loading={import_request.loading}
            >
              导入数据
            </Button>
            <Button
              size='large'
              type={'dashed'}
              disabled={items.length <= 0}
              onClick={() => _items([])}
            >
              清空数据
            </Button>
          </Space>
          <Table columns={columns} dataSource={items} pagination={false} />
        </Space>
      </Modal>
    </>
  );
};
