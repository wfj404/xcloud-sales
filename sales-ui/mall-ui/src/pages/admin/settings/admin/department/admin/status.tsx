import { apiClient } from '@/utils/client';
import { AdminDto, UpdateAdminStatusDto } from '@/utils/swagger';
import { Form, message, Modal, Switch } from 'antd';
import { useEffect, useState } from 'react';
import { handleResponse } from '@/utils/utils';

export default ({
                  show, hide, ok, model,
                }:
                  {
                    show: boolean;
                    hide: () => void;
                    ok: () => void;
                    model: AdminDto
                  }) => {

  const [loading, _loading] = useState(false);
  const [form, _form] = useState<UpdateAdminStatusDto>({});

  const save = (e: UpdateAdminStatusDto) => {
    _loading(true);
    apiClient.sys
      .manageAdminUpdateStatus({
        ...e,
        Id: model.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok && ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _form({
      IsActive: model.IsActive,
      IsSuperAdmin: model.IsSuperAdmin,
    });
  }, [model]);

  return (
    <>
      <Modal
        title='修改状态'
        confirmLoading={loading}
        open={show}
        onCancel={() => {
          hide();
        }}
        onOk={() => {
          save(form);
        }}
      >
        <Form>
          <Form.Item
            label='超级管理员'
          >
            <Switch checked={form.IsSuperAdmin || false} onChange={e => {
              _form({
                ...form,
                IsSuperAdmin: e,
              });
            }} />
          </Form.Item>
          <Form.Item label='是否激活'>
            <Switch checked={form.IsActive || false} onChange={e => {
              _form({
                ...form,
                IsActive: e,
              });
            }} />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
