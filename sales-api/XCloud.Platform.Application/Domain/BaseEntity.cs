﻿using Volo.Abp.Domain.Entities;
using XCloud.Core.Dto;

namespace XCloud.Platform.Application.Domain;

public interface IPlatformBaseEntity : IEntity<string>, IDbTableFinder
{
    new string Id { get; set; }
}

public abstract class PlatformBaseEntity : IPlatformBaseEntity
{
    protected PlatformBaseEntity()
    {
        //
    }

    public virtual string Id { get; set; }

    public virtual object[] GetKeys()
    {
        return new object[] { Id };
    }
}

public interface IHasIdentityNameFields : IEntity
{
    string IdentityName { get; set; }

    string OriginIdentityName { get; set; }
}

public interface IHasExternalBinding : IEntity
{
    string ExternalId { get; set; }
}