using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Volo.Abp.DependencyInjection;
using XCloud.AspNetMvc.Extension;

namespace XCloud.AspNetMvc.Core;

/// <summary>
/// Represents a common helper
/// </summary>
public interface IWebHelper
{
    [CanBeNull]
    HttpContext HttpContext { get; }

    [NotNull]
    HttpContext RequiredHttpContext { get; }

    [CanBeNull]
    string GetCurrentIpAddressOrNull();
}

[ExposeServices(typeof(IWebHelper))]
public class WebHelper : IWebHelper, ITransientDependency
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public WebHelper(
        IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    public HttpContext HttpContext => this._httpContextAccessor.HttpContext;

    public HttpContext RequiredHttpContext =>
        this.HttpContext ??
        throw new NotSupportedException($"http context is not available-{nameof(RequiredHttpContext)}");

    /// <summary>
    /// Get IP address from HTTP context
    /// </summary>
    /// <returns>String of IP address</returns>
    public virtual string GetCurrentIpAddressOrNull()
    {
        return this.HttpContext?.GetCurrentIpAddress();
    }
}