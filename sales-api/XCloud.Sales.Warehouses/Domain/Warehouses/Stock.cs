﻿using System;
using Volo.Abp.Auditing;
using XCloud.Sales.Application.Domain;

namespace XCloud.Sales.Warehouses.Domain.Warehouses;

public class Stock : SalesBaseEntity<string>, IHasCreationTime
{
    public string No { get; set; }

    public string SupplierId { get; set; }

    public string WarehouseId { get; set; }

    public string Remark { get; set; }

    public bool Approved { get; set; }
    
    public string ApprovedByUserId { get; set; }
    
    public DateTime? ApprovedTime { get; set; }

    public DateTime CreationTime { get; set; }
}