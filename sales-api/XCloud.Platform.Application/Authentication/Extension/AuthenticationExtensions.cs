﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Net.Http.Headers;
using XCloud.Core.Dto;
using XCloud.Core.Helper;

namespace XCloud.Platform.Application.Authentication.Extension;

public static class PlatformClaimTypes
{
    public static string CreationTime => "x_creation_time";
}

public static class AuthenticationExtensions
{
    public static bool IsLoginRequired(this HttpContext context)
    {
        var endpoint = context.Features.Get<IEndpointFeature>()?.Endpoint;
        if (endpoint == null)
        {
            //如果不是action，默认加载登录信息
            return true;
        }

        var allowAnonymous = endpoint.Metadata.GetMetadata<AllowAnonymousAttribute>();
        if (allowAnonymous != null)
        {
            return false;
        }

        return true;
    }

    public static async Task<ApiResponse<ClaimsPrincipal>> IsAuthenticatedAsync(this HttpContext context, string scheme)
    {
        if (string.IsNullOrWhiteSpace(scheme))
            throw new ArgumentNullException(nameof(scheme));

        var result = await context.AuthenticateAsync(scheme: scheme);

        if (result.Succeeded && result.Principal.IsAuthenticated(out var claims))
        {
            return new ApiResponse<ClaimsPrincipal>().SetData(result.Principal);
        }
        else
        {
            return new ApiResponse<ClaimsPrincipal>().SetError("error");
        }
    }

    public static bool IsAuthenticated(this ClaimsPrincipal principal, out Claim[] claims)
    {
        if (principal == null)
        {
            claims = [];
            return false;
        }

        claims = principal?.Claims?.ToArray() ?? [];

        var authed = principal?.Identity?.IsAuthenticated ?? false;

        return authed && claims.Any();
    }

    public static string GetSubjectId(this IEnumerable<Claim> claims)
    {
        var res = claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid)?.Value;
        return res;
    }

    public static ClaimsIdentity SetSubjectId(this ClaimsIdentity identity, string subjectId)
    {
        identity.SetOrReplaceClaim(ClaimTypes.Sid, subjectId);
        return identity;
    }

    public static ClaimsIdentity SetCreationTime(this ClaimsIdentity identity, DateTime time)
    {
        var timestamp = (int)(time - Com.Utc1970).TotalSeconds;
        identity.SetOrReplaceClaim(PlatformClaimTypes.CreationTime, timestamp.ToString());
        return identity;
    }

    public static DateTime? GetCreationTime(this IEnumerable<Claim> claims)
    {
        var data = claims.FirstOrDefault(x => x.Type == PlatformClaimTypes.CreationTime)?.Value;
        if (!string.IsNullOrWhiteSpace(data) && int.TryParse(data, out var timestamp))
        {
            var res = Com.Utc1970.AddSeconds(timestamp);
            return res;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// 先删除，再添加
    /// </summary>
    public static void SetOrReplaceClaim(this ClaimsIdentity model, string claimType, string data)
    {
        var toRemove = model.FindAll(x => x.Type == claimType).ToArray();

        foreach (var m in toRemove)
        {
            model.RemoveClaim(m);
        }

        model.AddClaim(new Claim(claimType, data));
    }

    /// <summary>
    /// 获取bearer token
    /// </summary>
    public static string GetBearerToken(this HttpContext context)
    {
        var hasTokenHeader = context.Request.Headers.TryGetValue(HeaderNames.Authorization, out var val);

        if (hasTokenHeader)
        {
            var token = ((string)val)?.Split(' ').Skip(1).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(token))
            {
                return token;
            }
        }

        return null;
    }
}