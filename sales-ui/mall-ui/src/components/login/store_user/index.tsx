import { useRequest } from 'ahooks';
import React, { ReactNode, useEffect } from 'react';
import { useLocation, useSnapshot } from 'umi';

import DotsLoading from '@/components/common/loading/dots';
import { storeState, StoreType } from '@/store';
import { isStoreUserLogin } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { hasAccessToken } from '@/utils/utils';

import StoreUserNotLogin from './not_login';
import { StoreUserAuthResult } from '@/utils/swagger';

export default function({
                          children,
                          optional,
                        }: {
  children: ReactNode;
  optional?: boolean;
}) {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const model = app.storeUserAuthResult?.StoreUser || {};

  const query_login_request = useRequest(
    apiClient.mall.userAuthQueryUserProfile,
    {
      manual: true,
      onSuccess(data, params) {
        const result: StoreUserAuthResult = data.data || {};
        app.setStoreUserAuthResult(result);
      },
    },
  );

  useEffect(() => {
    if (hasAccessToken()) {
      query_login_request.run();
    } else {
      app.setStoreUserAuthResult(undefined);
    }
  }, [location.pathname]);

  if (query_login_request.loading) {
    return <DotsLoading />;
  }

  if (!optional) {
    if (!isStoreUserLogin(model)) {
      return (
        <>
          <StoreUserNotLogin />
        </>
      );
    }

    if (!model.IsActive) {
      return (
        <>
          <h1>商城账号已经被禁用</h1>
        </>
      );
    }
  }

  return <>{children}</>;
}
