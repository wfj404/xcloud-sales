import { useRequest } from 'ahooks';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { dateTimeFormat, myDayjs } from '@/utils/dayjs';
import { QueryTopSkuListInput, TopSkuList } from '@/utils/swagger';
import { formatMoney } from '@/utils/utils';

import XIndexNo from '../components/indexNo';

const columns: ColumnProps<TopSkuList>[] = [
  {
    render: (x, record, index) => <XIndexNo index={index} />,
  },
  {
    title: 'SKU',
    render: (d, x) => x.Name,
  },
  {
    title: '金额',
    render: (d, x) => formatMoney(x.TotalPrice || 0),
  },
  {
    title: '数量',
    render: (d, x) => x.TotalQuantity,
  },
];

export default () => {
  const [finalQuery, _finalQuery] = useState<QueryTopSkuListInput>({});

  const request = useRequest(apiClient.mallAdmin.reportTopSkus, {
    manual: true,
  });

  const data = request.data?.data?.Data || [];

  useEffect(() => {
    request.run({ ...finalQuery });
  }, []);

  useEffect(() => {
    let now = myDayjs.utc();
    let monthAgo = now.add(-1, 'month');
    _finalQuery({
      StartTime: monthAgo.format(dateTimeFormat),
      EndTime: now.format(dateTimeFormat),
    });
  }, []);

  return (
    <>
      <Card
        title="TOP SKU"
        extra={<Button type="link">更多</Button>}
        loading={request.loading}
        style={{ marginBottom: 10 }}
      >
        <Table columns={columns} dataSource={data} pagination={false} />
      </Card>
    </>
  );
};
