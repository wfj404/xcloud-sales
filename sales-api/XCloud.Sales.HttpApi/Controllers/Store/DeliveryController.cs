﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Notifications;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/delivery")]
public class DeliveryController : ShopBaseController
{
    private readonly IOrderService _orderService;
    private readonly IOrderItemService _orderItemService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;
    private readonly IDeliveryService _deliveryService;
    private readonly INotificationService _notificationService;
    private readonly ISkuService _skuService;

    public DeliveryController(IOrderService orderService,
        IDeliveryService deliveryService,
        INotificationService notificationService,
        ISkuService skuService,
        IOrderAutoProcessService orderAutoProcessService, IOrderItemService orderItemService)
    {
        _deliveryService = deliveryService;
        _notificationService = notificationService;
        _skuService = skuService;
        _orderAutoProcessService = orderAutoProcessService;
        _orderItemService = orderItemService;
        _orderService = orderService;
    }

    [NonAction]
    private async Task AttachDetailAsync(DeliveryRecordDto[] data)
    {
        await _deliveryService.AttachItemsAsync(data);
        await _deliveryService.AttachOrdersAsync(data);

        var orders = data.Select(x => x.Order).WhereNotNull().ToArray();

        await _orderService.AttachShippingAddressAsync(orders);
        await _orderService.AttachOrderItemsAsync(orders);

        var items = orders.SelectMany(x => x.Items).WhereNotNull().ToArray();

        await _orderItemService.AttachOrderItemSkuAsync(items);
        var skus = items.Select(x => x.Sku).WhereNotNull().ToArray();
        await _skuService.AttachGoodsAsync(skus);
    }

    [HttpPost("by-id")]
    public async Task<ApiResponse<DeliveryRecordDto>> QueryByIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        var delivery = await _deliveryService.EnsureStoreDeliveryAsync(dto.Id, manager.StoreId);

        await AttachDetailAsync(new[] { delivery });

        return new ApiResponse<DeliveryRecordDto>(delivery);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<DeliveryRecordDto>> QueryPagingAsync(
        [FromBody] QueryDeliveryRecordPagingInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        dto.StoreId = manager.StoreId;

        var response = await _deliveryService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await AttachDetailAsync(response.Items.ToArray());
        }

        return response;
    }

    [HttpPost("get-delivery-item-by-order-id")]
    public async Task<ApiResponse<DeliveryRecordItem[]>> GetOrderDeliveryItemsAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        var order = await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var data = await _deliveryService.GetOrderDeliveryItemsAsync(order.Id);

        return new ApiResponse<DeliveryRecordItem[]>(data);
    }

    [HttpPost("by-order-id")]
    public async Task<ApiResponse<DeliveryRecordDto[]>> ByOrderId([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var response = await _deliveryService.QueryByOrderIdAsync(dto.Id);

        await AttachDetailAsync(response);

        return new ApiResponse<DeliveryRecordDto[]>(response);
    }

    [HttpPost("mark-as-delivering")]
    public async Task<ApiResponse<object>> MarkAsDeliveringAsync([FromBody] DeliveryRecordDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        var delivery = await _deliveryService.EnsureStoreDeliveryAsync(dto.Id, manager.StoreId);

        await _deliveryService.MarkAsDeliveringAsync(dto);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _deliveryService.TryUpdateOrderDeliveryStatusAsync(delivery.OrderId);

        return new ApiResponse<object>();
    }

    [HttpPost("mark-as-delivered")]
    public async Task<ApiResponse<object>> MarkAsDeliveredAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        var delivery = await _deliveryService.EnsureStoreDeliveryAsync(dto.Id, manager.StoreId);

        await _deliveryService.MarkAsDeliveredAsync(dto);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _deliveryService.TryUpdateOrderDeliveryStatusAsync(delivery.OrderId);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _orderAutoProcessService.AutoProcessOrderAsync(delivery.OrderId);

        return new ApiResponse<object>();
    }

    [HttpPost("create-delivery")]
    public async Task<ApiResponse<DeliveryRecord>> CreateDeliveryAsync([FromBody] DeliveryRecordDto recordDto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        var order = await _orderService.EnsureStoreOrderAsync(recordDto.OrderId, manager.StoreId);

        var delivery = await _deliveryService.CreateDeliveryAsync(recordDto);

        await _notificationService.InsertAsync(new NotificationDto
        {
            UserId = order.UserId,
            Title = "order start shipping",
            Content = "",
            Data = string.Empty,
        });

        return new ApiResponse<DeliveryRecord>(delivery);
    }
}