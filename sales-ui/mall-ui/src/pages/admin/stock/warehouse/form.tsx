import { apiClient } from '@/utils/client';
import { WarehouseDto } from '@/utils/swagger';
import { Form, Input, InputNumber, message, Modal } from 'antd';
import { useEffect, useState } from 'react';

export default (props: {
  show: boolean;
  hide: () => void;
  data: WarehouseDto;
  ok: () => void;
}) => {
  const { show, hide, data, ok } = props;
  const [loading, _loading] = useState(false);

  const [form, _form] = useState<WarehouseDto>({});

  const save = (row: WarehouseDto) => {
    _loading(true);

    apiClient.mallAdmin
      .warehouseSave(row)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _form({ ...data });
  }, [data]);

  return (
    <>
      <Modal
        open={show}
        confirmLoading={loading}
        onCancel={() => hide()}
        onOk={() => save(form)}
      >
        <Form
          labelCol={{ flex: '110px' }}
          labelAlign='right'
          wrapperCol={{ flex: 1 }}
        >
          <Form.Item
            label='名称'
          >
            <Input value={form.Name || ''}
                   onChange={e => {
                     _form(x => ({ ...x, Name: e.target.value }));
                   }}
            />
          </Form.Item>
          <Form.Item label='地址'>
            <Input
              value={form.Address || ''}
              onChange={e => {
                _form(x => ({ ...x, Address: e.target.value }));
              }}
            />
          </Form.Item>

          <Form.Item label='lat'>
            <InputNumber
              value={form.Lat}
              onChange={e => {
                _form(x => ({ ...x, Lat: e }));
              }}
            />
          </Form.Item>

          <Form.Item label='lng'>
            <InputNumber
              value={form.Lng}
              onChange={e => {
                _form(x => ({ ...x, Lng: e }));
              }}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
