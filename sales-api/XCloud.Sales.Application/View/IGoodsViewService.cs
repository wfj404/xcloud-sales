﻿using XCloud.Core.Json;
using XCloud.Platform.Application.Service.Common;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Extension;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(GoodsViewService))]
public class GoodsViewService : SalesAppService
{
    private readonly IGoodsService _goodsService;
    private readonly IGoodsPictureService _goodsPictureService;
    private readonly ISettingService _settingService;
    private readonly IPriceCalculationService _priceCalculationService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly IAreaService _areaService;

    public GoodsViewService(IGoodsService goodsService, IGoodsPictureService goodsPictureService,
        ISettingService settingService, IPriceCalculationService priceCalculationService,
        IStoreGoodsMappingService storeGoodsMappingService,
        IAreaService areaService)
    {
        _goodsService = goodsService;
        _goodsPictureService = goodsPictureService;
        _settingService = settingService;
        _priceCalculationService = priceCalculationService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _areaService = areaService;
    }

    public async Task<GoodsDto[]> AttachPreviewInfoForListAsync(GoodsDto[] data, string storeId,
        StoreUserDto loginUserOrNull = default)
    {
        if (data.Any())
        {
            data.HideDetail();

            await _goodsPictureService.AttachImagesAsync(data);

            await _storeGoodsMappingService.AttachAvailableSkusAsync(data, storeId);
            var skus = data.SelectMany(x => x.Skus).ToArray();

            await this._storeGoodsMappingService.AttachStoreQuantityAsync(skus, storeId);

            var settings = await _settingService.GetCachedMallSettingsAsync();

            if (loginUserOrNull != null || settings.GoodsSettings.DisplayPriceForGuest)
            {
                await _priceCalculationService.AttachPriceModelAsync(skus, storeId, loginUserOrNull?.Id);
            }
            else
            {
                skus.HidePrice();
            }
        }

        return data;
    }

    private async Task<GoodsDto> QueryGoodsDetailByIdAsync(string goodsId)
    {
        var goods = await _goodsService.GetByIdAsync(goodsId);
        if (goods == null || !goods.Published)
            return null;

        await _goodsPictureService.AttachImagesAsync(new[] { goods });
        await _goodsService.AttachTagsAsync(new[] { goods });
        await _goodsService.AttachBrandsAsync(new[] { goods });
        await _goodsService.AttachCategoriesAsync(new[] { goods });
        await _goodsService.AttachAreasAsync(new[] { goods });

        if (goods.Area != null)
        {
            goods.Area.PathList = await this._areaService.QueryNodePathAsync(goods.Area);
            await this._areaService.AttachPictureMetaAsync(goods.Area.PathList);
        }

        if (goods.LimitedToRedirect)
        {
            goods.RedirectToUrl =
                this.JsonDataSerializer.DeserializeFromStringOrDefault<GoodsRedirectToUrlDto>(
                    goods.RedirectToUrlJson, this.Logger);
            goods.RedirectToUrl ??= new GoodsRedirectToUrlDto();
        }

        return goods;
    }

    public async Task<GoodsDto> QueryGoodsDetailByIdAsync(string goodsId, CacheStrategy cacheStrategyOption)
    {
        var key = $"mall.goods.detail.id={goodsId}";
        var option = new CacheOption<GoodsDto>(key, TimeSpan.FromMinutes(30));

        var goods = await CacheProvider.ExecuteWithPolicyAsync(() => QueryGoodsDetailByIdAsync(goodsId),
            option,
            cacheStrategyOption);

        return goods;
    }
}