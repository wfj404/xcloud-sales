import { useRequest } from 'ahooks';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XUserAvatar from '@/components/manage/user/avatar';
import { apiClient } from '@/utils/client';
import { dateTimeFormat, myDayjs } from '@/utils/dayjs';
import { QueryTopSellerInput, TopSellersList } from '@/utils/swagger';
import { formatMoney, isArrayEmpty } from '@/utils/utils';

import XIndexNo from '../components/indexNo';

export default () => {
  const [finalQuery, _finalQuery] = useState<QueryTopSellerInput>({});

  const request = useRequest(apiClient.mallAdmin.reportTopSellers, {
    manual: true,
  });

  const data = request.data?.data?.Data || [];

  useEffect(() => {
    request.run({ ...finalQuery });
  }, []);

  useEffect(() => {
    let now = myDayjs.utc();
    let monthAgo = now.add(-1, 'month');
    _finalQuery({
      StartTime: monthAgo.format(dateTimeFormat),
      EndTime: now.format(dateTimeFormat),
    });
  }, []);

  const usersRequest = useRequest(apiClient.sys.manageUserQueryUserByIds, {
    manual: true,
  });

  const users = usersRequest.data?.data?.Data || [];

  useEffect(() => {
    if (isArrayEmpty(data)) {
      return;
    }
    const ids = data.map((x) => x.GlobalUserId || '');
    usersRequest.run(ids);
  }, [data]);

  const columns: ColumnProps<TopSellersList>[] = [
    {
      render: (x, record, index) => <XIndexNo index={index} />,
    },
    {
      title: '销售',
      render: (x: TopSellersList) => {
        let selectedUser = users.find((d) => d.Id == x.GlobalUserId);
        if (selectedUser) {
          return <XUserAvatar model={selectedUser} />;
        }
        return '--';
      },
    },
    {
      title: '金额',
      render: (d, x) => formatMoney(x.TotalPrice || 0),
    },
    {
      title: '数量',
      render: (d, x) => x.TotalQuantity,
    },
  ];

  return (
    <>
      <Card
        title="TOP卖家"
        extra={<Button type="link">更多</Button>}
        loading={request.loading}
        style={{ marginBottom: 10 }}
      >
        <Table columns={columns} dataSource={data} pagination={false} />
      </Card>
    </>
  );
};
