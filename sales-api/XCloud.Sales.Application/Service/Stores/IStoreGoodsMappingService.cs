using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreGoodsMappingService : ISalesAppService
{
    Task<StoreGoodsMapping> UpdateAsync(StoreGoodsMappingDto dto);

    Task<GoodsDto[]> AttachAvailableSkusAsync(GoodsDto[] data, string storeId);

    [NotNull]
    [ItemNotNull]
    Task<StoreGoodsMapping> GetOrCreateAsync(string skuId, string storeId);

    Task<StoreGoodsMappingDto[]> AttachSkusAsync(StoreGoodsMappingDto[] data);

    Task<SkuDto[]> AttachActiveStoreMappingsWithStoreAsync(SkuDto[] data);

    Task<SkuDto[]> AttachStoreQuantityAsync(SkuDto[] data, string storeId);

    Task<CheckSkuForSaleError[]> CheckSkuForSaleAsync(string storeId, CheckSkuForSaleInput[] items);

    Task<StoreGoodsMappingDto[]> AttachStoresAsync(StoreGoodsMappingDto[] data);

    Task SaveGoodsStoreMappingAsync(SaveGoodsStoreMappingDto dto);

    Task<PagedResponse<StoreGoodsMappingDto>> QueryPagingAsync(
        QueryStoreGoodsMappingPagingInput dto);
}

public class StoreGoodsMappingService : SalesAppService, IStoreGoodsMappingService
{
    private readonly ISalesRepository<StoreGoodsMapping> _repository;
    private readonly ISkuService _skuService;

    public StoreGoodsMappingService(
        ISalesRepository<StoreGoodsMapping> repository,
        ISkuService skuService)
    {
        _repository = repository;
        _skuService = skuService;
    }

    public async Task<StoreGoodsMapping> UpdateAsync(StoreGoodsMappingDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        entity.Price = dto.Price;
        entity.OverridePrice = dto.OverridePrice;

        entity.StockQuantity = dto.StockQuantity;
        entity.Active = dto.Active;

        entity.LastModificationTime = this.Clock.Now;

        await this._repository.UpdateAsync(entity);

        return entity;
    }

    public async Task<GoodsDto[]> AttachAvailableSkusAsync(GoodsDto[] data, string storeId)
    {
        if (!data.Any())
            return data;

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var goodsIds = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var query = from mapping in db.Set<StoreGoodsMapping>().AsNoTracking()
                    join sku in db.Set<Sku>().AsNoTracking()
                        on mapping.SkuId equals sku.Id
                    select new { mapping, sku };

        query = query.Where(x => x.mapping.Active);

        var datalist = await query
            .Where(x => goodsIds.Contains(x.sku.GoodsId) && x.mapping.StoreId == storeId)
            .OrderByDescending(x => x.sku.CreationTime)
            .TakeUpTo5000()
            .ToArrayAsync();

        foreach (var m in data)
        {
            m.Skus = datalist
                .Where(x => x.sku.GoodsId == m.Id)
                .Select(x => x.sku)
                .MapArrayTo<Sku, SkuDto>(this.ObjectMapper)
                .ToArray();
        }

        return data;
    }

    public async Task<StoreGoodsMapping> GetOrCreateAsync(string skuId, string storeId)
    {
        var db = await _repository.GetDbContextAsync();

        var entity = await db.Set<StoreGoodsMapping>().AsNoTracking()
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync(x => x.SkuId == skuId && x.StoreId == storeId);

        if (entity == null)
        {
            entity = new StoreGoodsMapping
            {
                Id = GuidGenerator.CreateGuidString(),
                StoreId = storeId,
                SkuId = skuId,
                StockQuantity = default,
                Price = default,
                OverridePrice = false,
                Active = false,
                LastModificationTime = default,
                CreationTime = Clock.Now
            };

            await this._repository.InsertAsync(entity);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return entity;
    }

    public async Task<PagedResponse<StoreGoodsMappingDto>> QueryPagingAsync(QueryStoreGoodsMappingPagingInput dto)
    {
        var db = await _repository.GetDbContextAsync();

        var query = from mapping in db.Set<StoreGoodsMapping>().AsNoTracking()
                    join sku in db.Set<Sku>().AsNoTracking()
                        on mapping.SkuId equals sku.Id
                    join goods in db.Set<Goods>().AsNoTracking()
                        on sku.GoodsId equals goods.Id
                    select new { mapping, sku, goods };

        if (dto.Active != null)
            query = query.Where(x => x.mapping.Active == dto.Active.Value);

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            query = query.Where(x => x.mapping.StoreId == dto.StoreId);

        if (dto.MinPrice != null)
        {
            query = query.Where(x =>
                (x.mapping.OverridePrice && x.mapping.Price >= dto.MinPrice) || x.sku.Price >= dto.MinPrice);
        }

        if (dto.MaxPrice != null)
        {
            query = query.Where(x =>
                (x.mapping.OverridePrice && x.mapping.Price <= dto.MaxPrice) || x.sku.Price <= dto.MaxPrice);
        }

        if (dto.MinStockQuantity != null)
        {
            query = query.Where(x => x.mapping.StockQuantity >= dto.MinStockQuantity.Value);
        }

        if (dto.MaxStockQuantity != null)
        {
            query = query.Where(x => x.mapping.StockQuantity <= dto.MaxStockQuantity.Value);
        }

        if (!string.IsNullOrWhiteSpace(dto.Sku))
        {
            query = query.Where(x => x.sku.SkuCode == dto.Sku);
        }

        if (!string.IsNullOrWhiteSpace(dto.Keywords))
        {
            query = query.Where(x => x.goods.Name.Contains(dto.Keywords) || x.sku.Name.Contains(dto.Keywords));
        }

        var count = await query.CountOrDefaultAsync(dto);

        var data = await query
            .OrderByDescending(x => x.sku.CreationTime)
            .ThenByDescending(x => x.mapping.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .Select(x => x.mapping)
            .ToArrayAsync();

        var datalist = data.MapArrayTo<StoreGoodsMapping, StoreGoodsMappingDto>(ObjectMapper);

        return new PagedResponse<StoreGoodsMappingDto>(datalist, dto, count);
    }

    public async Task<StoreGoodsMappingDto[]> AttachSkusAsync(StoreGoodsMappingDto[] data)
    {
        if (!data.Any())
            return data;

        var skuIds = data.Select(x => x.SkuId).Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var skus = await db.Set<Sku>().WhereIdIn(skuIds).ToArrayAsync();

        foreach (var m in data)
        {
            var s = skus.FirstOrDefault(x => x.Id == m.SkuId);
            if (s == null)
                continue;

            m.Sku = s.MapTo<Sku, SkuDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<StoreGoodsMappingDto[]> AttachStoresAsync(StoreGoodsMappingDto[] data)
    {
        if (!data.Any())
            return data;

        var storeIds = data.Select(x => x.StoreId).Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var stores = await db.Set<Store>().WhereIdIn(storeIds).ToArrayAsync();

        foreach (var m in data)
        {
            var store = stores.FirstOrDefault(x => x.Id == m.StoreId);
            if (store == null)
                continue;

            m.Store = store.MapTo<Store, StoreDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<SkuDto[]> AttachActiveStoreMappingsWithStoreAsync(SkuDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var query = from mapping in db.Set<StoreGoodsMapping>().AsNoTracking()
                    join store in db.Set<Store>().AsNoTracking()
                        on mapping.StoreId equals store.Id
                    select new { mapping, store };

        query = query.Where(x => x.mapping.Active);

        query = query.Where(x => ids.Contains(x.mapping.SkuId));

        var mappings = await query.ToArrayAsync();

        foreach (var m in data)
        {
            m.StoreGoodsMapping = mappings
                .Where(x => x.mapping.SkuId == m.Id)
                .Select(x => MapDto(x.mapping, x.store))
                .ToArray();
        }

        return data;

        StoreGoodsMappingDto MapDto(StoreGoodsMapping mapping, Store store)
        {
            var dto = mapping.MapTo<StoreGoodsMapping, StoreGoodsMappingDto>(ObjectMapper);

            dto.Store = store?.MapTo<Store, StoreDto>(ObjectMapper);

            return dto;
        }
    }

    public async Task<CheckSkuForSaleError[]> CheckSkuForSaleAsync(string storeId, CheckSkuForSaleInput[] items)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        if (ValidateHelper.IsEmptyCollection(items))
            throw new ArgumentNullException(nameof(items));

        var errorList = new List<CheckSkuForSaleError>();

        var ids = items.Select(x => x.Id).Distinct().ToArray();
        var skus = await this._skuService.GetByIdsAsync(ids);

        await this._skuService.AttachGoodsAsync(skus);
        await this.AttachStoreQuantityAsync(skus, storeId);

        foreach (var m in items)
        {
            var sku = skus.FirstOrDefault(x => x.Id == m.Id);

            var error = new CheckSkuForSaleError(m.Id) { Sku = sku };

            if (sku == null || !sku.IsActive)
            {
                error.Error = "sku is not available";
                errorList.Add(error);
                continue;
            }

            var goods = sku.Goods;

            if (goods == null || !goods.Published)
            {
                error.Error = "goods is not available";
                errorList.Add(error);
                continue;
            }

            var quantity = sku.StockModel?.StockQuantity ?? default(int);

            if (m.Quantity > quantity)
            {
                error.Error = $"{goods.Name}-{sku.Name} short of quantity : {m.Quantity}";
                errorList.Add(error);
                continue;
            }
        }

        return errorList.ToArray();
    }

    public async Task<SkuDto[]> AttachStoreQuantityAsync(SkuDto[] data, string storeId)
    {
        if (!data.Any())
            return data;

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<StoreGoodsMapping>().AsNoTracking();

        query = query.Where(x => x.Active);
        query = query.Where(x => x.StoreId == storeId);
        query = query.Where(x => ids.Contains(x.SkuId));

        var mappings = await query.OrderByDescending(x => x.CreationTime).ToArrayAsync();

        foreach (var m in data)
        {
            m.StockModel = default;

            var mapping = mappings.FirstOrDefault(x => x.SkuId == m.Id);

            if (mapping == null)
                continue;

            m.StockModel = new StockModel
            {
                StoreId = mapping.StoreId,
                StockQuantity = mapping.StockQuantity,
                Provider = "store",
                Description = "store quantity"
            };
        }

        return data;
    }

    public async Task SaveGoodsStoreMappingAsync(SaveGoodsStoreMappingDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.SkuId))
            throw new ArgumentException(nameof(dto.SkuId));

        if (dto.Mappings == null)
            throw new ArgumentNullException(nameof(dto.Mappings));

        foreach (var m in dto.Mappings)
        {
            m.SkuId = dto.SkuId;
        }

        var db = await _repository.GetDbContextAsync();

        var sku = await _skuService.GetByIdAsync(dto.SkuId);

        if (sku == null)
            throw new EntityNotFoundException(nameof(sku));

        var mappings = dto.Mappings.MapArrayTo<StoreGoodsMappingDto, StoreGoodsMapping>(ObjectMapper);

        var set = db.Set<StoreGoodsMapping>();

        var origin = await set.Where(x => x.SkuId == sku.Id).ToArrayAsync();

        string FingerPrint(StoreGoodsMapping x) => $"{x.StoreId}";

        var toRemove = origin.NotInBy(mappings, FingerPrint).ToArray();
        var toInsert = mappings.NotInBy(origin, FingerPrint).ToArray();
        var toUpdate = origin.InBy(mappings, FingerPrint).ToArray();

        var now = Clock.Now;

        if (toRemove.Any())
        {
            set.RemoveRange(toRemove);
        }

        if (toInsert.Any())
        {
            foreach (var m in toInsert)
            {
                m.Id = GuidGenerator.CreateGuidString();
                m.CreationTime = now;
            }

            set.AddRange(toInsert);
        }

        foreach (var m in toUpdate)
        {
            var entity = mappings.FirstOrDefault(x => x.StoreId == m.StoreId);
            if (entity == null)
                continue;

            m.StockQuantity = entity.StockQuantity;
            m.Price = entity.Price;
            m.OverridePrice = entity.OverridePrice;
            m.Active = entity.Active;
            m.LastModificationTime = now;
        }

        await db.TrySaveChangesAsync();
    }
}