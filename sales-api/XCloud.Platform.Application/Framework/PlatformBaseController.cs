﻿using XCloud.Application.ServiceDiscovery;
using XCloud.AspNetMvc.Controller;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Queue;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Platform.Application.Framework;

public abstract class PlatformBaseController : XCloudBaseController
{
    protected ISettingService SettingService => LazyServiceProvider.LazyGetRequiredService<ISettingService>();

    protected IServiceDiscoveryService ServiceDiscoveryService =>
        LazyServiceProvider.LazyGetRequiredService<IServiceDiscoveryService>();

    protected PlatformEventBusService PlatformEventBusService =>
        this.LazyServiceProvider.LazyGetRequiredService<PlatformEventBusService>();

    protected IAdminSecurityService AdminSecurityService =>
        LazyServiceProvider.LazyGetRequiredService<IAdminSecurityService>();

    protected IUserAuthService UserAuthService =>
        LazyServiceProvider.LazyGetRequiredService<IUserAuthService>();

    protected IAdminAuthService AdminAuthService =>
        LazyServiceProvider.LazyGetRequiredService<IAdminAuthService>();
}