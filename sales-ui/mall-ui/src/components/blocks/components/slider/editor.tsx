import { Form, InputNumber, Segmented, Space, Switch } from 'antd';

import XPictureListEditor from '../block_picture_list_editor';
import { SliderBlockProps } from './utils';

export default ({
  data,
  onChange,
}: {
  data: SliderBlockProps;
  onChange: (x: SliderBlockProps) => void;
}) => {
  return (
    <>
      <Form labelCol={{ span: 3 }}>
        <Form.Item label="自动">
          <Space direction="horizontal">
            <Switch
              checked={data.auto_play}
              onChange={(e) => {
                onChange({
                  ...data,
                  auto_play: e,
                });
              }}
            />
            {data.auto_play && (
              <InputNumber
                addonBefore="间隔"
                addonAfter="s"
                value={data.auto_play_interval}
                onChange={(e) => {
                  onChange({
                    ...data,
                    auto_play_interval: e || undefined,
                  });
                }}
              />
            )}
          </Space>
        </Form.Item>
        <Form.Item label="循环播放">
          <Switch
            checked={data.loop}
            onChange={(e) => {
              onChange({
                ...data,
                loop: e,
              });
            }}
          />
        </Form.Item>
        <Form.Item label="隐藏指示器">
          <Switch
            checked={data.hide_indicator}
            onChange={(e) => {
              onChange({
                ...data,
                hide_indicator: e,
              });
            }}
          />
        </Form.Item>
        <Form.Item label="滑动方向">
          <Segmented
            value={data.vertical ? 1 : 0}
            options={[
              {
                label: '水平',
                value: 0,
              },
              {
                label: '垂直',
                value: 1,
              },
            ]}
            onChange={(e) => {
              onChange({
                ...data,
                vertical: e == 1,
              });
            }}
          />
        </Form.Item>
      </Form>
      <XPictureListEditor
        data={data?.sliders || []}
        onChange={(x) => {
          onChange({
            ...data,
            sliders: x,
          });
        }}
      />
    </>
  );
};
