import { Alert, Button, message, Space, Switch, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { getSkuName } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { SkuDto } from '@/utils/swagger';
import { getPinyinNormalStyle, isStringEmpty } from '@/utils/utils';

import FormBox from './item';

interface SkuItem extends SkuDto {
  name_for_sorting: string;
  pinyin: string;
}

export default () => {
  const [loading, _loading] = useState(false);
  const [originData, _originData] = useState<SkuDto[]>([]);
  const [datalist, _datalist] = useState<SkuItem[]>([]);
  const [msg, _msg] = useState<string>('');
  const [sortEnabled, _sortEnabled] = useState(true);

  const maxCount = 3000;

  const queryList = async () => {
    if (loading) {
      message.error('加载中');
      return;
    }
    _originData((x) => []);
    _loading(true);
    let resultList: SkuDto[] = [];
    try {
      let page = 0;
      while (true) {
        if (resultList.length > maxCount) {
          break;
        }
        page = page + 1;
        const res = await apiClient.mallAdmin.skuQueryByPageOrderByCreationTime(
          { Page: page, PageSize: 100, SkipCalculateTotalCount: true },
        );
        const items = res.data.Data || [];
        if (items.length <= 0) {
          break;
        }
        resultList = [...resultList, ...items];
        _msg(
          (x) =>
            `已经加载第${page}页数据，共${resultList.length}条，加载结束后呈现`,
        );

        await new Promise<void>((resolve, reject) =>
          setTimeout(() => resolve(), 300),
        );
      }
      _msg((x) => `加载完成`);
    } finally {
      _originData((x) => resultList);
      _loading(false);
      _msg((x) => '');
    }
  };

  useEffect(() => {
    _datalist((x) => convertDatalist(originData));
  }, [originData, sortEnabled]);

  const convertData = (d: SkuDto): SkuItem => {
    const title = getSkuName(d);
    const py = getPinyinNormalStyle(d.Goods?.Name || '');
    return {
      ...d,
      pinyin: py,
      name_for_sorting: title || '',
    };
  };

  const convertDatalist = (skus: SkuDto[]): SkuItem[] => {
    let data = skus.map((x) => convertData(x));

    if (sortEnabled) {
      data = data.sort(
        (a, b) =>
          a.name_for_sorting?.localeCompare(b.name_for_sorting || '') || 0,
      );
    }

    return data;
  };

  const columns: ColumnType<SkuItem>[] = [
    {
      title: '搜索拼音',
      render: (d, x) => x.pinyin || '--',
    },
    {
      title: '价格修改器',
      render: (d, x, i) => {
        return (
          <FormBox
            item={x}
            ok={(d) => {
              if (!d) {
                return;
              }
              const temp = [...datalist];
              temp[i] = { ...x, ...d };
              _datalist((x) => temp);
            }}
          />
        );
      },
    },
  ];

  const render_sort_button = () => {
    return (
      <Space direction={'horizontal'}>
        <span>排序</span>
        <Switch
          title={'排序'}
          checked={sortEnabled}
          onChange={(e) => {
            _sortEnabled(e);
          }}
        />
      </Space>
    );
  };

  const render_load_button = () => {
    return (
      <Space direction={'horizontal'}>
        <Button
          block
          loading={loading}
          type={'primary'}
          onClick={() => {
            queryList();
          }}
        >
          加载SKU
        </Button>

        <Button
          type={'dashed'}
          danger
          onClick={() => {
            _datalist((x) => []);
          }}
        >
          清空
        </Button>
      </Space>
    );
  };

  return (
    <>
      <Space
        direction={'vertical'}
        style={{
          marginBottom: 10,
        }}
      >
        <Alert
          style={{ marginBottom: 10 }}
          type={'warning'}
          message={'该页面会加载大量数据，可能造成性能低下'}
          description={`最多加载${maxCount}个sku`}
        ></Alert>
        {isStringEmpty(msg) || <Alert type={'info'} message={msg} />}
        {datalist.length > 0 && (
          <Alert type={'info'} message={`一共${datalist.length}条数据`} />
        )}

        {render_load_button()}

        {render_sort_button()}
      </Space>
      <Table
        style={{
          marginBottom: 10,
        }}
        virtual
        size="small"
        columns={columns}
        dataSource={datalist}
        pagination={false}
      />
    </>
  );
};
