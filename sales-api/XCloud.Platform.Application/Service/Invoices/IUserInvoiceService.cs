using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Platform.Application.Domain.Invoices;
using XCloud.Platform.Application.Exceptions;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Invoices;

public interface IUserInvoiceService : IPlatformCrudService<UserInvoice, UserInvoiceDto, QueryUserInvoicePagingInput>
{
    Task<UserInvoiceDto> EnsureUserInvoiceAsync(string invoiceId, string userId);

    Task<UserInvoice[]> QueryByUserIdAsync(string userId);

    Task UpdateStatusAsync(UpdateUserInvoiceStatusInput dto);
}

public class UserInvoiceService : PlatformCrudService<UserInvoice, UserInvoiceDto, QueryUserInvoicePagingInput>,
    IUserInvoiceService
{
    public UserInvoiceService(IPlatformRepository<UserInvoice> repository) : base(repository)
    {
        //
    }

    public async Task<UserInvoiceDto> EnsureUserInvoiceAsync(string invoiceId, string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var entity = await GetByIdAsync(invoiceId);

        if (entity == null)
            throw new EntityNotFoundException(nameof(EnsureUserInvoiceAsync));

        if (entity.UserId != userId)
            throw new PermissionRequiredException();

        return entity.MapTo<UserInvoice, UserInvoiceDto>(ObjectMapper);
    }

    public override async Task DeleteByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await Repository.FirstOrDefaultAsync(x => x.Id == id);

        if (entity == null)
        {
            return;
        }

        entity.IsDeleted = true;
        entity.DeletionTime = this.Clock.Now;

        await Repository.UpdateAsync(entity);
    }

    public override async Task<UserInvoice> InsertAsync(UserInvoiceDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Head))
            throw new ArgumentNullException(nameof(dto.Head));

        return await base.InsertAsync(dto);
    }

    public override async Task<UserInvoice> UpdateAsync(UserInvoiceDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var invoice = await this.GetByIdAsync(dto.Id);

        if (invoice != null)
        {
            await this.DeleteByIdAsync(invoice.Id);
            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        dto.Id = default;
        dto.IsDeleted = false;
        dto.DeletionTime = default;

        return await this.InsertAsync(dto);
    }

    public async Task UpdateStatusAsync(UpdateUserInvoiceStatusInput dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        if (dto.IsDeleted != null && dto.IsDeleted.Value)
        {
            await this.DeleteByIdAsync(dto.Id);
        }
    }

    protected override async Task SetFieldsBeforeInsertAsync(UserInvoice entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
        entity.IsDeleted = false;
    }

    protected override async Task SetFieldsBeforeUpdateAsync(UserInvoice entity, UserInvoiceDto dto)
    {
        await Task.CompletedTask;

        entity.Head = dto.Head;
        entity.Code = dto.Code;
        entity.InvoiceType = dto.InvoiceType;
        entity.Address = dto.Address;
        entity.PhoneNumber = dto.PhoneNumber;
        entity.BankName = dto.BankName;
        entity.BankCode = dto.BankCode;
    }

    public async Task<UserInvoice[]> QueryByUserIdAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var datalist = await Repository.QueryManyAsync(x => x.UserId == userId);

        return datalist;
    }

    protected override async Task<IQueryable<UserInvoice>> GetPagingFilteredQueryableAsync(DbContext db,
        QueryUserInvoicePagingInput dto)
    {
        await Task.CompletedTask;

        var query = db.Set<UserInvoice>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.UserId))
        {
            query = query.Where(x => x.UserId == dto.UserId);
        }

        return query;
    }

    protected override async Task<IOrderedQueryable<UserInvoice>> GetPagingOrderedQueryableAsync(
        IQueryable<UserInvoice> query, QueryUserInvoicePagingInput dto)
    {
        await Task.CompletedTask;

        return query.OrderByDescending(x => x.CreationTime).ThenByDescending(x => x.Id);
    }
}