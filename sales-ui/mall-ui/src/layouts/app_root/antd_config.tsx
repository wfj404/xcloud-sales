import { App, ConfigProvider, message } from 'antd';
import antdZHCN from 'antd/es/locale/zh_CN';
import { ReactElement, useEffect } from 'react';

import { GlobalContainer } from '@/utils/utils';

export default function ({ children }: { children: ReactElement }) {
  const [api, contextHolder] = message.useMessage();

  useEffect(() => {
    GlobalContainer.message = api;
    return () => {
      GlobalContainer.message = undefined;
    };
  }, [api]);

  return (
    <>
      <ConfigProvider
        locale={antdZHCN}
        componentSize="middle"
        theme={{
          //algorithm: [theme.compactAlgorithm],
          token: {
            //borderRadius: 0,
          },
          components: {
            Message: {
              zIndexPopup: 9999999,
            },
            Tooltip: {
              zIndexBase: 9999999,
              zIndexPopupBase: 9999999,
            },
          },
        }}
      >
        <App message={{ maxCount: 10 }} notification={{ placement: 'top' }}>
          {contextHolder}
          {children}
        </App>
      </ConfigProvider>
    </>
  );
}
