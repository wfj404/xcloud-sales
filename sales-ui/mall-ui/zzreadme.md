# mall-ui 前端项目

## Getting Started

> `此项目基于node 16.18.1开发测试`

⚠️ 因为组件是 dynamic load，所以前后台都放在一个项目中。
首次加载速度尚可接受。

### install

```bash
$ npm install --registry=http://registry.npmmirror.com
```

### dev

```bash
$ export api_gateway=https://api.your-domain.com:8888 && npm run dev
```

### 指定环境变量

```shell
cross-env api_gateway=http://10.0.1.50:7888 MOCK=none umi dev
```

### build

```bash
# build mobile-app and management-app
$ export api_gateway=https://api.your-domain.com:8888 && npm run build
# build mobile-app only [smaller size,without management app]
$ export api_gateway=https://api.your-domain.com:8888 && npm run build-app-only
```

## 通过 swagger 生成客户端代码：

```shell
> npm run generate-client
```

https://www.npmjs.com/package/swagger-typescript-api

```shell
npx swagger-typescript-api -p http://localhost:6001/api/mall/swagger/swagger.json -o ./ -n swagger.ts --axios
swagger-typescript-api -p http://localhost:6001/api/mall/swagger.json -o ./src/utils/ -n swagger.ts --axios --sort-types --route-types --module-name-index 1
```

## swagger codegen

https://swagger.io/tools/swagger-codegen/
https://github.com/swagger-api/swagger-codegen
