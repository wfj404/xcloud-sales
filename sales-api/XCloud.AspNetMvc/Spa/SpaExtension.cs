﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Volo.Abp;
using Volo.Abp.Http;

namespace XCloud.AspNetMvc.Spa;

public static class SpaExtension
{
    public static void UseSpaStaticFile(this ApplicationInitializationContext context, string requestPath,
        string rootDir = default)
    {
        if (string.IsNullOrWhiteSpace(requestPath))
            throw new ArgumentNullException(nameof(requestPath));

        var app = context.GetApplicationBuilder();

        if (string.IsNullOrWhiteSpace(rootDir))
        {
            var env = context.GetEnvironment();
            rootDir = Path.Combine(env.WebRootPath, "app");
        }

        app.UseStaticFiles(new StaticFileOptions()
        {
            RequestPath = requestPath,
            FileProvider = new FallToIndexFileProvider(rootDir),
            ServeUnknownFileTypes = true,
            DefaultContentType = MimeTypes.Text.Html
        });
    }
}