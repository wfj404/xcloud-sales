using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction;

public interface IConnection : IDisposable
{
    bool IsActive { get; }

    CancellationToken CancellationToken { get; }

    void RequestToClose();

    [NotNull] IMessengerServer Server { get; }

    [NotNull] ClientIdentity ClientIdentity { get; set; }

    Task<IAsyncDisposable> JoinServerAsync();

    Task SendMessageToClientAsync(MessageDto data);

    Task StartBlockedMessageLoopAsync();
}