namespace XCloud.Sales.Application.Domain.AfterSale;

public class AfterSalesComment : SalesBaseEntity<string>, IHasCreationTime
{
    public string AfterSaleId { get; set; }
    public string Content { get; set; }
    public string PictureJson { get; set; }
    public bool IsAdmin { get; set; }
    public DateTime CreationTime { get; set; }
}