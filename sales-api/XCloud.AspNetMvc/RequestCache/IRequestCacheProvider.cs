﻿using System;
using Volo.Abp.DependencyInjection;
using XCloud.AspNetMvc.Core;

namespace XCloud.AspNetMvc.RequestCache;

public interface IRequestCacheProvider
{
    T GetObject<T>(string key);

    void SetObject<T>(string key, T data);

    bool HasKey(string key);

    void RemoveKey(string key);
}

[ExposeServices(typeof(IRequestCacheProvider))]
public class HttpContextItemRequestCache : IRequestCacheProvider, IScopedDependency
{
    private readonly IWebHelper _webHelper;

    public HttpContextItemRequestCache(IWebHelper webHelper)
    {
        _webHelper = webHelper;
    }

    public T GetObject<T>(string key)
    {
        if (this._webHelper.RequiredHttpContext.Items[key] is T d)
        {
            return d;
        }

        return default;
    }

    public bool HasKey(string key)
    {
        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        return this._webHelper.RequiredHttpContext.Items.ContainsKey(key);
    }

    public void RemoveKey(string key)
    {
        this._webHelper.RequiredHttpContext.Items.Remove(key);
    }

    public void SetObject<T>(string key, T data)
    {
        this._webHelper.RequiredHttpContext.Items[key] = data;
    }
}