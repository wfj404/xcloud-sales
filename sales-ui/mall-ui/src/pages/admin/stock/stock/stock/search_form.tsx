import { QueryWarehouseStockPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input } from 'antd';
import { useEffect, useState } from 'react';

const App = (props: {
  query: QueryWarehouseStockPagingInput;
  onSearch: (d: QueryWarehouseStockPagingInput) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QueryWarehouseStockPagingInput>({});

  const triggerSearch = (e: QueryWarehouseStockPagingInput) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form({ ...query });
  }, []);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        layout={'inline'}
      >
        <Form.Item label='批次号'>
          <Input value={form.SerialNo || ''}
                 onChange={e => {
                   _form(x => ({
                     ...x,
                     SerialNo: e.target.value,
                   }));
                 }} />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type='primary' htmlType='submit' icon={<SearchOutlined />} onClick={() => {
            triggerSearch(form);
          }}>
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default App;
