import { ReactNode, useState } from 'react';

import XBlockEditor from '@/components/blocks/editor';
import { BlockItem, BlockItemProps } from '@/components/blocks/utils';

import XDragSort from './drag_sort_design';

export default function({
                          blocks,
                          onBlocksChange,
                          renderSaveButtons,
                          loading,
                        }: {
  blocks: BlockItem<BlockItemProps>[];
  onBlocksChange: (e: BlockItem<BlockItemProps>[]) => void;
  renderSaveButtons?: () => ReactNode;
  loading?: boolean;
}) {
  const [editIndex, _editIndex] = useState<number | undefined>(undefined);

  const getEditorData = (): BlockItem<BlockItemProps> | undefined => {
    if (editIndex != undefined) {
      return blocks.at(editIndex);
    }
    return undefined;
  };

  const editorData = getEditorData();

  return (
    <>
      <XBlockEditor
        title={'编辑组件'}
        data={editorData}
        onClose={() => _editIndex(undefined)}
        onChangeCommitted={(e) => {
          console.log(e);
          if (editIndex == undefined) {
            return;
          }

          const datalist = [...blocks];
          datalist[editIndex] = { ...e };
          onBlocksChange(datalist);
        }}
      />
      <XDragSort
        loading={loading}
        blocks={blocks}
        onChange={(e) => {
          onBlocksChange(e);
        }}
        onMaskClick={(i) => {
          _editIndex(i);
        }}
        selectedIndex={editIndex}
        renderExtra={() => renderSaveButtons && renderSaveButtons()}
      />
    </>
  );
}
