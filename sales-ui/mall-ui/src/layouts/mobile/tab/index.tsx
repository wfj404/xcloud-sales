import { useSize } from 'ahooks';
import { useEffect, useRef } from 'react';
import { history, useLocation, useOutlet, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isStoreUserLogin } from '@/utils/biz';
import { firstNotEmpty, simpleString } from '@/utils/utils';
import {
  AllInclusiveOutlined,
  FolderSpecialOutlined,
  PermIdentityOutlined,
  ShoppingBagOutlined,
} from '@mui/icons-material';
import {
  Badge,
  BottomNavigation,
  BottomNavigationAction,
  Box,
  Paper,
  styled,
} from '@mui/material';

import XShoppingCartButton from './shoppingcart';

const XBottomNavigationAction = styled(BottomNavigationAction)((theme) => ({
  '&.Mui-selected': {
    //color: '#282b31',
  },
}));

export default function () {
  const children = useOutlet();

  const location = useLocation();

  const app = useSnapshot(storeState) as StoreType;

  const ref = useRef<HTMLDivElement>(null);
  const rect = useSize(ref);

  useEffect(() => {
    if (isStoreUserLogin(app.storeUser)) {
      app.queryNotificationCount();
    }
  }, [location.pathname]);

  useEffect(() => {
    app.setBottomHeight(rect?.height || 0);
  }, [rect]);

  const home_name = simpleString(
    firstNotEmpty([app.mallSettings?.StoreNickName, '首页']) || '',
    3,
  );

  return (
    <>
      <XShoppingCartButton />
      <Box sx={{ pb: `${app.bottomHeight + 15}px` }}>{children}</Box>
      <Paper
        ref={ref}
        sx={{
          position: 'fixed',
          bottom: 0,
          left: 0,
          right: 0,
          zIndex: 1000,
          background: 'none',
          backgroundColor: 'rgba(255,255,255,0.9)',
          backdropFilter: 'blur(6px)',
          WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
        }}
        className="backdrop-blur"
        elevation={0}
      >
        <BottomNavigation
          showLabels
          value={location.pathname}
          onChange={(event, newValue) => {
            //setValue(newValue);
            history.push(newValue);
          }}
          sx={{
            background: 'none',
          }}
        >
          <XBottomNavigationAction
            value={'/'}
            label={home_name}
            icon={<AllInclusiveOutlined fontSize="small" />}
          />
          <XBottomNavigationAction
            value={'/category'}
            label="商品"
            icon={<FolderSpecialOutlined fontSize="small" />}
          />
          <XBottomNavigationAction
            value={'/shoppingcart'}
            label="购物袋"
            icon={
              <Badge
                variant="dot"
                color="error"
                invisible={app.shoppingCartCount <= 0}
                badgeContent={app.shoppingCartCount}
              >
                <ShoppingBagOutlined fontSize="small" />
              </Badge>
            }
          />
          <XBottomNavigationAction
            value={'/ucenter'}
            label="我的"
            icon={
              <Badge
                variant="dot"
                color="error"
                invisible={app.notificationCount <= 0}
                badgeContent={app.notificationCount}
              >
                <PermIdentityOutlined fontSize="small" />
              </Badge>
            }
          />
        </BottomNavigation>
      </Paper>
    </>
  );
}
