import { useRequest } from 'ahooks';
import { Button, Card, message, Space, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import PreviewGroup from '@/components/common/preview_group';
import XTime from '@/components/common/time';
import { AntDesignTreeNodeV2, getFlatNodes } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { AreaDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty } from '@/utils/utils';

import XForm from './form';

export default () => {
  const [expandKeys, _expandKeys] = useState<string[]>([]);

  const [formData, _formData] = useState<AreaDto | undefined>(undefined);

  const query_area_request = useRequest(apiClient.sys.areaLoadTree, {
    manual: true,
    onSuccess(data, params) {
      const treeData = data.data.Data || [];
      const keys = treeData
        .flatMap((x) => getFlatNodes(x))
        .map((x) => x.key || '');
      _expandKeys(keys);
    },
  });
  const data = query_area_request.data?.data?.Data || [];

  const columns: ColumnType<AntDesignTreeNodeV2<AreaDto>>[] = [
    {
      title: '名称',
      render: (d, x) => x.title,
    },
    {
      title: '描述',
      render: (d, x) => x.raw_data?.Description || '--',
    },
    {
      title: '类型',
      render: (d, x) => x.raw_data?.AreaType || '--',
    },
    {
      title: '图片',
      render: (d, x) => {
        const record = x.raw_data || {};

        if (record.StorageMeta == null) {
          return null;
        }

        return (
          <>
            <PreviewGroup data={[record.StorageMeta]} />
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x.raw_data} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        const record = x.raw_data || {};
        return (
          <>
            <Space direction="horizontal">
              <Button
                type="link"
                onClick={() => {
                  _formData(record);
                }}
              >
                编辑
              </Button>
              {isArrayEmpty(x.children) && (
                <XDeleteButton
                  action={async () => {
                    const res = await apiClient.sys.areaDeleteById({
                      Id: record.Id,
                    });
                    handleResponse(res, () => {
                      message.success('删除成功');
                      query_area_request.run({});
                    });
                  }}
                />
              )}
            </Space>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    query_area_request.run({});
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          query_area_request.run({});
        }}
      />
      <Card
        title="地区"
        loading={query_area_request.loading}
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.key || ''}
          expandable={{
            expandedRowKeys: expandKeys,
            defaultExpandAllRows: true,
            showExpandColumn: true,
            onExpandedRowsChange: (expandedKeys) => {
              _expandKeys(expandedKeys as any);
            },
          }}
          loading={query_area_request.loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Card>
    </>
  );
};
