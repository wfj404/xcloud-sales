﻿using System;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Service.Warehouses;

public class QueryStockUsageHistoryPagingInput : PagedRequest
{
    //
}

public class StockUsageHistoryDto : StockUsageHistory, IEntityDto<string>
{
    //
}

public class SupplierDto : Supplier, IEntityDto
{
    //
}

public class WarehouseDto : Warehouse, IEntityDto
{
    //
}

public class UpdateWarehouseStatusInput : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? IsDeleted { get; set; }
}

public class UpdateSupplierStatusInput : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? IsDeleted { get; set; }
}

public class QueryWarehousePagingInput : PagedRequest
{
    //
}

public class QuerySupplierPagingInput : PagedRequest
{
    //
}

public class DeductStockInput : IEntityDto
{
    public string OrderItemId { get; set; }
    public string SkuId { get; set; }
    public int Quantity { get; set; }
}

public class QueryWarehouseStockPagingInput : PagedRequest
{
    public string SerialNo { get; set; }
    public string SupplierId { get; set; }
    public string WarehouseId { get; set; }
    public bool? Approved { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class QueryWarehouseStockItemInput : QueryWarehouseStockPagingInput
{
    public string SkuId { get; set; }
    public bool? RunningOut { get; set; }
}

public class StockDto : Stock, IEntityDto
{
    public SupplierDto Supplier { get; set; }

    public WarehouseDto Warehouse { get; set; }

    public StockItemDto[] Items { get; set; }
}

public class StockItemDto : StockItem, IEntityDto
{
    public StockDto Stock { get; set; }
    public GoodsDto Goods { get; set; }
    public SkuDto Sku { get; set; }
}