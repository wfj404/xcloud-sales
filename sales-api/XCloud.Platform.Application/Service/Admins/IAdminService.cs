﻿using System.Threading.Tasks;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Service.Admins;

public interface IAdminService : IPlatformCrudService<Admin, AdminDto, QueryAdminPaging>
{
    Task<UserDto[]> AttachAdminIdentityAsync(UserDto[] data);

    Task<AdminDto[]> AttachUsersAsync(AdminDto[] data);

    Task<PagedResponse<AdminDto>> QueryAdminPagingAsync(QueryAdminPaging filter);
}

public class AdminService : PlatformCrudService<Admin, AdminDto, QueryAdminPaging>, IAdminService
{
    private readonly IPlatformRepository<Admin> _adminRepository;

    public AdminService(IPlatformRepository<Admin> adminRepository) : base(adminRepository)
    {
        _adminRepository = adminRepository;
    }

    public override Task<Admin> InsertAsync(AdminDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<Admin> UpdateAsync(AdminDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task DeleteByIdAsync(string id)
    {
        throw new NotSupportedException();
    }

    public async Task<UserDto[]> AttachAdminIdentityAsync(UserDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _adminRepository.GetDbContextAsync();

        var adminList = await db.Set<Admin>().AsNoTracking().Where(x => ids.Contains(x.UserId))
            .ToArrayAsync();

        foreach (var m in data)
        {
            var admin = adminList.OrderBy(x => x.CreationTime).FirstOrDefault(x => x.UserId == m.Id);
            if (admin == null)
                continue;

            m.AdminIdentity = admin.MapTo<Admin, AdminDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<AdminDto[]> AttachUsersAsync(AdminDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.UserId).Distinct().ToArray();

        var db = await _adminRepository.GetDbContextAsync();

        var userList = await db.Set<User>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            var user = userList.OrderBy(x => x.CreationTime).FirstOrDefault(x => x.Id == m.UserId);
            if (user == null)
                continue;

            m.User = user.MapTo<User, UserDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<PagedResponse<AdminDto>> QueryAdminPagingAsync(QueryAdminPaging dto)
    {
        var db = await _adminRepository.GetDbContextAsync();

        var query = from admin in db.Set<Admin>().IgnoreQueryFilters().AsNoTracking()
            join u in db.Set<User>().IgnoreQueryFilters().AsNoTracking()
                on admin.UserId equals u.Id into userGrouping
            from user in userGrouping.DefaultIfEmpty()
            select new { admin, user };

        if (dto.IsDeleted != null)
        {
            query = query.Where(x => x.user.IsDeleted == dto.IsDeleted.Value);
        }

        if (dto.IsActive != null)
        {
            query = query.Where(x => x.admin.IsActive == dto.IsActive.Value);
        }

        if (!string.IsNullOrWhiteSpace(dto.Keyword))
        {
            query = query.Where(x =>
                x.user.IdentityName == dto.Keyword || x.user.NickName.Contains(dto.Keyword));
        }

        var count = await query.CountOrDefaultAsync(dto);

        var items = await query
            .OrderByDescending(x => x.admin.IsActive)
            .ThenByDescending(x => x.admin.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var list = items.Select(x => x.admin).MapArrayTo<Admin, AdminDto>(ObjectMapper);

        return new PagedResponse<AdminDto>(list, dto, count);
    }
}