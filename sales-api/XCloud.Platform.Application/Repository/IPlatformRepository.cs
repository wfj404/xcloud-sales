﻿using XCloud.EntityFrameworkCore.Repository;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Application.Repository;

public interface IPlatformRepository<T> : IEfRepository<T> where T : class, IPlatformBaseEntity
{
    //
}