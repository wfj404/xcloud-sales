using Volo.Abp;

namespace XCloud.Platform.Messenger.Abstraction.Core;

public class MessengerException : BusinessException
{
    public MessengerException(string message) : base(message: message)
    {
        //
    }
}