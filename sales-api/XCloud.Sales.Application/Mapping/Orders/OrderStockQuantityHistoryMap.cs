using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderStockQuantityHistoryMap : SalesEntityTypeConfiguration<OrderStockQuantityHistory>
{
    public override void Configure(EntityTypeBuilder<OrderStockQuantityHistory> builder)
    {
        builder.ToTable("sales_order_inventory_deduction_history");

        builder.PreConfigIntPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.OrderItemId).RequiredId();

        base.Configure(builder);
    }
}