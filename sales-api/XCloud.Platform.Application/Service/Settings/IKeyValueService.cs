﻿using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Core.Json;
using XCloud.Platform.Application.Domain.Settings;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Settings;

public interface IKeyValueService : IApplicationService
{
    IJsonDataSerializer XJsonDataSerializer { get; }

    ILogger XLogger { get; }

    Task<bool> SettingsExistAsync(string name);

    Task SaveSettingsAsync(string name, string value);
    Task RemoveByNameAsync(string name);

    Task<string> GetSettingValueByNameAsync(string name);
}

public class KeyValueService : XCloudAppService, IKeyValueService
{
    private readonly IPlatformRepository<Setting> _settingRepository;

    public KeyValueService(IPlatformRepository<Setting> settingRepository)
    {
        _settingRepository = settingRepository;
    }

    public IJsonDataSerializer XJsonDataSerializer => JsonDataSerializer;

    public ILogger XLogger => Logger;

    private string NormalizeName(string name) => name.Trim().RemoveWhitespace();

    public async Task<string> GetSettingValueByNameAsync(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        name = NormalizeName(name);

        var entity = await _settingRepository.FirstOrDefaultAsync(x => x.Name == name);

        if (entity == null)
            return null;

        return entity.Value;
    }

    public async Task<bool> SettingsExistAsync(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        name = NormalizeName(name);

        var exist = await _settingRepository.AnyAsync(x => x.Name == name);
        return exist;
    }

    public async Task SaveSettingsAsync(string name, string value)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        name = NormalizeName(name);

        await _settingRepository.DeleteAsync(x => x.Name == name);

        var entity = new Setting
        {
            Name = name,
            Value = value,
            Id = GuidGenerator.CreateGuidString(),
        };

        await _settingRepository.InsertAsync(entity);
    }

    public async Task RemoveByNameAsync(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        name = NormalizeName(name);

        await _settingRepository.DeleteAsync(x => x.Name == name);
    }
}