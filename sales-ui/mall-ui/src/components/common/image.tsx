import { useDebounceFn, useSize } from 'ahooks';
import { Image, ImageProps } from 'antd-mobile';
import { useRef } from 'react';

import XImageLoadError from '@/assets/img-load-error.png';

export interface XImageProps extends ImageProps {
  rate?: number;
}

export default (props: XImageProps) => {
  const { height, fallback, placeholder } = props;

  const imgContainerRef = useRef<HTMLDivElement>(null);
  const imgContainerSize = useSize(imgContainerRef);

  const calculateHeightImpl = (): number | string | undefined => {
    const { rate } = props;
    const realWidth = imgContainerSize?.width;

    if (rate != undefined && realWidth != undefined) {
      return realWidth * rate;
    }

    return undefined;
  };

  const calculateHeight = useDebounceFn(calculateHeightImpl, { wait: 80 }).run;

  const xprops: XImageProps = {
    ...props,
    height: height || calculateHeightImpl() || undefined,
    fallback: fallback || <img src={XImageLoadError} alt="图片无法加载" />,
    placeholder: placeholder,
  };

  return (
    <>
      <div
        ref={imgContainerRef}
        style={{
          display: 'block',
          width: '100%',
        }}
      >
        <Image {...xprops} />
      </div>
    </>
  );
};
