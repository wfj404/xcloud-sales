﻿namespace XCloud.Sales.Application.Domain.Coupons;

/// <summary>
/// 优惠价用户映射
/// </summary>
public class CouponUserMapping : SalesBaseEntity, IHasCreationTime
{
    public string CouponId { get; set; }
    
    public int UserId { get; set; }

    public bool IsUsed { get; set; }

    public DateTime? UsedTime { get; set; }

    public DateTime? ExpiredAt { get; set; }

    public DateTime CreationTime { get; set; }
}