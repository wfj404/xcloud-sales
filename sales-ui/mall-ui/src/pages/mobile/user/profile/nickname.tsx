import React, { useState } from 'react';

import { apiClient } from '@/utils/client';
import { UserDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { LoadingButton } from '@mui/lab';
import { Box, TextField, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import XItem from './item';

export default function ({ model, ok }: { model: UserDto; ok: () => void }) {
  const [open, setOpen] = useState(false);

  const [nickname, _nickname] = useState<string>('');
  const [loadingSave, _loadingSave] = useState(false);

  const handleClickOpen = () => {
    _nickname(model.NickName || '');
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const save = () => {
    _loadingSave(true);
    apiClient.platform
      .userUpdateProfile({
        ...model,
        NickName: nickname,
      })
      .then((res) => {
        handleResponse(res, () => {
          handleClose();
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  return (
    <>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>修改昵称</DialogTitle>
        <DialogContent>
          <DialogContentText>
            请输入昵称。要求10个字符内，不包含特殊字符和空格。
          </DialogContentText>
          <TextField
            autoFocus
            label="新昵称"
            fullWidth
            value={nickname}
            onChange={(e) => _nickname(e.target.value)}
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>取消</Button>
          <LoadingButton
            onClick={() => {
              save();
            }}
            loading={loadingSave}
          >
            确认修改
          </LoadingButton>
        </DialogActions>
      </Dialog>
      <Box
        sx={{}}
        onClick={() => {
          handleClickOpen();
        }}
      >
        <XItem
          title={'昵称'}
          right={
            <Typography
              variant="overline"
              sx={{
                color: 'text.secondary',
              }}
            >
              {model.NickName || '--'}
            </Typography>
          }
        />
      </Box>
    </>
  );
}
