﻿using System;
using System.Threading.Tasks;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.RequestCache;

public static class RequestCacheExtension
{
    private static string GenerateObjectCacheKey<T>() => typeof(T).FullName.RemoveWhitespace();

    public static T GetObject<T>(this IRequestCacheProvider holder)
    {
        return holder.GetObject<T>(GenerateObjectCacheKey<T>());
    }

    public static Task<T> GetOrSetAsync<T>(this IRequestCacheProvider holder, Func<Task<T>> func)
    {
        var key = GenerateObjectCacheKey<T>();

        return GetOrSetAsync(holder, key, func);
    }

    public static async Task<T> GetOrSetAsync<T>(this IRequestCacheProvider holder, string key, Func<Task<T>> func)
    {
        if (holder == null)
            throw new ArgumentNullException(nameof(holder));

        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        if (func == null)
            throw new ArgumentNullException(nameof(func));

        try
        {
            if (holder.HasKey(key))
            {
                var res = holder.GetObject<T>(key);
                return res;
            }

            var data = await func.Invoke();

            holder.SetObject(key, data);

            return data;
        }
        catch
        {
            holder.RemoveKey(key);
            throw;
        }
    }
}