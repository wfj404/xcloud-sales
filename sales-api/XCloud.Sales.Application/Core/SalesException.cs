﻿namespace XCloud.Sales.Application.Core;

/// <summary>
/// Represents errors that occur during application execution
/// </summary>
[Serializable]
public class SalesException : BusinessException
{
    public SalesException(string message = default, Exception e = default)
        : base(message: message, innerException: e)
    {
        //
    }
}