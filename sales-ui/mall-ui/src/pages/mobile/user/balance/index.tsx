import { useInfiniteScroll } from 'ahooks';
import React, { useEffect, useState } from 'react';

import ScrollLoading from '@/components/common/scroll/loading';
import XHeader from '@/components/mobile/simpleHeader';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { BalanceHistoryDto, QueryBalancePagingInput } from '@/utils/swagger';
import { Box, Button } from '@mui/material';

import XBalanceAndPointsMenu from '../ucenter/balance';
import XItem from './item';

export default function () {
  const [finalQuery, _finalQuery] = React.useState<QueryBalancePagingInput>({});

  const { loading, loadingMore, mutate, noMore, data } = useInfiniteScroll<{
    list: BalanceHistoryDto[];
    res?: PagedResponse<BalanceHistoryDto>;
  }>(
    async (currentData) => {
      const res = await apiClient.mall.userQueryBalancePaging({
        ...finalQuery,
        Page: (currentData?.res?.PageIndex || 0) + 1,
      });

      return {
        list: res.data.Items || [],
        res: res.data,
      };
    },
    {
      target: window.document,
      isNoMore: (x) => x?.res?.IsEmpty || false,
      reloadDeps: [finalQuery],
    },
  );

  const items = data?.list || [];

  React.useEffect(() => {
    mutate({
      list: [],
    });
  }, [finalQuery]);

  return (
    <>
      <XHeader>
        <Box sx={{ my: 2 }}>
          <XBalanceAndPointsMenu />
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}
        >
          <Button onClick={() => {}}>预充值</Button>
        </Box>
        <Box sx={{ backgroundColor: 'white' }}>
          {items.map((x, index) => (
            <Box key={index}>
              <XItem model={x} />
            </Box>
          ))}
        </Box>
        <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
      </XHeader>
    </>
  );
}
