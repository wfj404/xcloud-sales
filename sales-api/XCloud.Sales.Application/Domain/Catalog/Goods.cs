using XCloud.Application.Utils;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Domain.Catalog;

[TypeIdentityName("goods")]
public class Goods : SalesBaseEntity<string>, ISoftDelete, IHasModificationTime
{
    public int GoodsType { get; set; } = GoodsTypeEnum.Goods;

    public string Name { get; set; }

    public string SeoName { get; set; }

    public string Description { get; set; }

    public string Keywords { get; set; }

    public string DetailInformation { get; set; }

    public string AdminComment { get; set; }

    public string CategoryId { get; set; }

    public string BrandId { get; set; }

    public string AreaId { get; set; }

    public bool LimitedToRedirect { get; set; }

    public string RedirectToUrlJson { get; set; }

    public bool LimitedToContact { get; set; }

    public string ContactTelephone { get; set; }

    public double ApprovedReviewsRates { get; set; }

    public int ApprovedTotalReviews { get; set; }

    public string StockDeductStrategy { get; set; } = SalesConstants.OrderStockDeductStrategy.AfterPlaceOrder;

    public bool StickyTop { get; set; }

    public bool DisplayAsInformationPage { get; set; }

    public bool PlaceOrderSupported { get; set; }

    public bool AfterSalesSupported { get; set; }

    public bool PayAfterDeliveredSupported { get; set; }

    public bool CommentSupported { get; set; }

    public bool Published { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}