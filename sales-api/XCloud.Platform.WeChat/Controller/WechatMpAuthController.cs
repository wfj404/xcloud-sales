using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.Application.Service.Users;
using XCloud.Platform.WeChat.Services;
using XCloud.Platform.WeChat.Services.Mp;

namespace XCloud.Platform.WeChat.Controller;

[Route("/api/platform/wechat-mp-auth")]
public class WechatMpAuthController : PlatformBaseController
{
    private readonly IWxMpService _wxMpService;
    private readonly IExternalConnectService _externalConnectService;
    private readonly IUserAccountService _userAccountService;
    private readonly IWxUnionService _wxUnionService;
    private readonly ITokenService _tokenService;
    private readonly ISettingService _settingService;

    public WechatMpAuthController(IUserAccountService userAccountService,
        IExternalConnectService userExternalAccountService,
        IWxMpService wxMpService,
        IWxUnionService wxUnionService,
        ITokenService tokenService,
        ISettingService settingService)
    {
        _userAccountService = userAccountService;
        _externalConnectService = userExternalAccountService;
        _wxMpService = wxMpService;
        _wxUnionService = wxUnionService;
        _tokenService = tokenService;
        _settingService = settingService;
    }

    [HttpPost("get-wechat-mp-option")]
    public async Task<ApiResponse<WechatMpOption>> GetWechatMpOptionAsync()
    {
        var settings = await this._settingService.GetWechatMpOptionAsync();

        var option = new WechatMpOption() { AppId = settings.AppId, AppSecret = default };

        return new ApiResponse<WechatMpOption>(option);
    }

    [NonAction]
    [NotNull]
    [ItemNotNull]
    private async Task<ExternalConnect> GetOrCreateAccountAsync(OAuthCodeDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Code))
            throw new ArgumentNullException(nameof(dto.Code));

        var wxToken = await _wxMpService.GetUserAccessTokenAsync(dto.Code);
        if (string.IsNullOrWhiteSpace(wxToken.OpenId))
            throw new BusinessException("empty openid from wechat mp");

        var config = await SettingService.GetWechatMpOptionAsync();

        config.EnsureWechatOptionNotEmpty();

        var entity = await _externalConnectService.FindByOpenIdAsync(
            ThirdPartyPlatforms.WxMp, config.AppId, wxToken.OpenId);

        if (entity == null)
        {
            var randomUserName = GuidGenerator.CreateGuidString();
            var userEntity =
                await _userAccountService.CreateUserAccountV1Async(new IdentityNameDto(randomUserName));

            entity = new ExternalConnect
            {
                Platform = ThirdPartyPlatforms.WxMp,
                AppId = config.AppId,
                UserId = userEntity.Id,
                OpenId = wxToken.OpenId
            };

            await _externalConnectService.SaveAsync(entity);
            dto.UseWechatProfile = true;
        }

        //create connection if needed
        if (!string.IsNullOrWhiteSpace(wxToken.UnionId))
        {
            //save openid-union id mapping
            await _wxUnionService.SaveOpenIdUnionIdMappingAsync(entity.Platform, entity.AppId,
                entity.OpenId, wxToken.UnionId);
        }

        if (dto.UseWechatProfile ?? false)
        {
            //try sync wechat user profile
            await _wxMpService.TryUpdateUserProfileFromWechat(entity.UserId, wxToken.AccessToken,
                wxToken.OpenId);
        }

        return entity;
    }

    [HttpPost("oauth-code-login")]
    public async Task<ApiResponse<AuthTokenDto>> WxMpOAuthCodeLoginAsync([FromBody] OAuthCodeDto dto)
    {
        var connection = await GetOrCreateAccountAsync(dto);

        var userId = connection.UserId;

        var validationResponse =
            await this._userAccountService.GetUserAuthValidationDataAsync(userId,
                new CacheStrategy() { Refresh = true });

        if (validationResponse.ErrorList.Any())
        {
            throw new UserFriendlyException(message: validationResponse.ErrorList.FirstOrDefault()?.Message ??
                                                     nameof(WxMpOAuthCodeLoginAsync));
        }

        var user = validationResponse.Data!;

        var identity = new ClaimsIdentity().SetSubjectId(user.Id)
            .SetCreationTime(Clock.Now);

        var token = await _tokenService.CreateTokenAsync(identity.Claims.ToArray());

        return new ApiResponse<AuthTokenDto>(new AuthTokenDto
        {
            AccessToken = token
        });
    }
}