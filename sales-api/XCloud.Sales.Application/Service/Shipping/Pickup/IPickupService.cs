﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Exceptions;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Shipping.Pickup;

public interface IPickupService : ISalesStringCrudAppService<PickupRecord, PickupRecordDto, QueryPickupPagingInput>
{
    Task TryUpdateOrderShipStatusAsync(string orderId);

    Task<PickupRecordDto> EnsureStorePickupAsync(string pickupId, string storeId);

    Task<PickupRecordDto> EnsureUserPickupAsync(string pickupId, int userId);

    Task<PickupRecordDto[]> AttachOrdersAsync(PickupRecordDto[] data);

    Task<PickupRecord> CreatePickupRecordAsync(PickupRecordDto dto);

    Task<PickupRecordDto[]> GetByOrderIdAsync(string orderId);

    Task SetPickupApprovedAsync(string id, bool approved);

    Task SetReadyForPickupAsync(string id);

    Task SetPickedAsync(string id);
}

public class PickupService : SalesStringCrudAppService<PickupRecord, PickupRecordDto, QueryPickupPagingInput>,
    IPickupService
{
    private readonly ISalesRepository<PickupRecord> _repository;
    private readonly IOrderService _orderService;
    private readonly IOrderShippingProcessService _orderShippingProcessService;

    public PickupService(ISalesRepository<PickupRecord> repository,
        IOrderService orderService,
        IOrderShippingProcessService orderShippingProcessService) : base(repository)
    {
        _repository = repository;
        _orderService = orderService;
        _orderShippingProcessService = orderShippingProcessService;
    }

    public override async Task<PagedResponse<PickupRecordDto>> QueryPagingAsync(QueryPickupPagingInput dto)
    {
        var db = await _repository.GetDbContextAsync();

        var orderQuery = db.Set<Order>().AsNoTracking();

        if (dto.OrderToService ?? false)
        {
            orderQuery = orderQuery.WhereToService();
        }

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            orderQuery = orderQuery.Where(x => x.StoreId == dto.StoreId);

        var query = from pickup in db.Set<PickupRecord>().AsNoTracking().IgnoreQueryFilters()
            select pickup;

        query = query.Where(x => orderQuery.Select(d => d.Id).Contains(x.OrderId));

        if (dto.Approved != null)
        {
            query = query.Where(x => x.ReservationApproved == dto.Approved.Value);
        }

        var count = await query.CountOrDefaultAsync(dto);

        query = query.OrderByDescending(x => x.CreationTime);

        var data = await query.PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();

        var items = data.MapArrayTo<PickupRecord, PickupRecordDto>(ObjectMapper);

        return new PagedResponse<PickupRecordDto>(items, dto, count);
    }

    public async Task<PickupRecordDto> EnsureStorePickupAsync(string pickupId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(pickupId))
            throw new ArgumentNullException(nameof(pickupId));

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var db = await _repository.GetDbContextAsync();

        var query = from pickup in db.Set<PickupRecord>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on pickup.OrderId equals order.Id
            select new { pickup, order };

        var data = await query.FirstOrDefaultAsync(x => x.pickup.Id == pickupId);

        if (data == null)
            throw new EntityNotFoundException(nameof(EnsureStorePickupAsync));

        if (data.order.StoreId != storeId)
            throw new PermissionRequiredException(nameof(EnsureStorePickupAsync));

        return data.pickup.MapTo<PickupRecord, PickupRecordDto>(ObjectMapper);
    }

    public async Task<PickupRecordDto> EnsureUserPickupAsync(string pickupId, int userId)
    {
        if (string.IsNullOrWhiteSpace(pickupId))
            throw new ArgumentNullException(nameof(pickupId));

        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        var db = await _repository.GetDbContextAsync();

        var query = from pickup in db.Set<PickupRecord>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on pickup.OrderId equals order.Id
            select new { pickup, order };

        var data = await query.FirstOrDefaultAsync(x => x.pickup.Id == pickupId);

        if (data == null)
            throw new EntityNotFoundException(nameof(EnsureUserPickupAsync));

        if (data.order.UserId != userId)
            throw new PermissionRequiredException(nameof(EnsureUserPickupAsync));

        return data.pickup.MapTo<PickupRecord, PickupRecordDto>(ObjectMapper);
    }

    public override Task DeleteByIdAsync(string id)
    {
        throw new NotSupportedException();
    }

    public async Task<PickupRecordDto[]> AttachOrdersAsync(PickupRecordDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.OrderId).WhereNotEmpty().Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var datalist = await db.Set<Order>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            m.Order = datalist.FirstOrDefault(x => x.Id == m.OrderId)?
                .MapTo<Order, OrderDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<PickupRecordDto[]> GetByOrderIdAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var db = await _repository.GetDbContextAsync();

        var entity = await db.Set<PickupRecord>().AsNoTracking().IgnoreQueryFilters()
            .Where(x => x.OrderId == orderId)
            .OrderBy(x => x.CreationTime)
            .ToArrayAsync();

        return entity.MapArrayTo<PickupRecord, PickupRecordDto>(ObjectMapper);
    }

    public async Task<PickupRecord> CreatePickupRecordAsync(PickupRecordDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.OrderId))
            throw new ArgumentNullException(nameof(dto.OrderId));

        if (await _repository.AnyAsync(x => x.OrderId == dto.OrderId))
            throw new BusinessException("pick up record already exist");

        var entity = dto.MapTo<PickupRecordDto, PickupRecord>(ObjectMapper);

        entity.Prepared = false;
        entity.PreparedTime = default;
        entity.Picked = false;
        entity.PickedTime = default;
        entity.ReservationApproved = null;
        entity.ReservationApprovedTime = default;
        entity.SecurityCode = default;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        await _repository.InsertAsync(entity);

        return entity;
    }

    public override Task<PickupRecord> InsertAsync(PickupRecordDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<PickupRecord> UpdateAsync(PickupRecordDto dto)
    {
        throw new NotSupportedException();
    }

    private readonly Random _random = new Random((int)DateTime.UtcNow.Ticks);

    public async Task SetReadyForPickupAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await _repository.GetRequiredByIdAsync(id);

        if (entity.Prepared)
            return;

        var order = await this._orderService.EnsureOrderIsToServiceAsync(entity.OrderId);

        entity.Prepared = true;
        entity.PreparedTime = Clock.Now;
        var pools = Com.Range(0, 10).Select(x => x.ToString()).ToArray();
        entity.SecurityCode = string.Join("", _random.Sample(pools, 4));

        await _repository.UpdateAsync(entity);
    }

    public async Task TryUpdateOrderShipStatusAsync(string orderId)
    {
        var order = await this._orderService.GetByIdAsync(orderId);

        if (order == null)
            return;

        var pickRecords = await GetByOrderIdAsync(order.Id);

        if (pickRecords.Any())
        {
            if (pickRecords.All(x => x.Picked))
            {
                await this._orderShippingProcessService.MarkAsShippedAsync(order.Id);
            }
            else if (pickRecords.Any(x => x.Picked))
            {
                await this._orderShippingProcessService.MarkAsShippingAsync(order.Id);
            }
        }
    }

    public async Task SetPickedAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await _repository.GetRequiredByIdAsync(id);

        if (entity.Picked)
            return;

        var order = await this._orderService.EnsureOrderIsToServiceAsync(entity.OrderId);

        if (!order.IsPaid())
            throw new UserFriendlyException("款项未结清");

        entity.Picked = true;
        entity.PickedTime = Clock.Now;

        await _repository.UpdateAsync(entity);
    }

    public async Task SetPickupApprovedAsync(string id, bool approved)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await _repository.GetRequiredByIdAsync(id);

        var order = await this._orderService.EnsureOrderIsToServiceAsync(entity.OrderId);

        if (entity.ReservationApproved == approved)
            return;

        entity.ReservationApproved = approved;
        entity.ReservationApprovedTime = Clock.Now;

        await _repository.UpdateAsync(entity);
    }
}