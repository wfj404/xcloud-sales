﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Admins;

namespace XCloud.Platform.Application.Authentication;

public interface IAdminAuthService : IXCloudAppService
{
    [NotNull]
    [ItemNotNull]
    Task<AdminAuthResult> GetAuthAdminAsync();
}

public class AdminAuthService : PlatformAppService, IAdminAuthService
{
    private readonly IAdminAccountService _adminAccountService;
    private readonly IRequestCacheProvider _cacheProvider;
    private readonly IUserAuthService _userAuthService;

    public AdminAuthService(IAdminAccountService adminAccountService,
        IRequestCacheProvider cacheProvider,
        IUserAuthService userAuthService)
    {
        _cacheProvider = cacheProvider;
        _userAuthService = userAuthService;
        _adminAccountService = adminAccountService;
    }

    private async Task<AdminAuthResult> GetAuthAdminImplAsync()
    {
        var res = new AdminAuthResult();

        var userAuthResult = await this._userAuthService.GetAuthUserAsync();

        if (userAuthResult.HasError())
        {
            res.SetPropertyAsJson(userAuthResult, this.JsonDataSerializer);
            return res.SetError("user auth failed");
        }

        var user = userAuthResult.GetRequiredUser();

        var admin = await _adminAccountService.GetAdminAuthValidationDataAsync(
            user.Id,
            new CacheStrategy { Cache = true });

        if (admin == null)
        {
            return res.SetError("admin identity is not exist");
        }

        admin.User = user;
        res.SetData(admin);

        return res;
    }

    public async Task<AdminAuthResult> GetAuthAdminAsync()
    {
        return await this._cacheProvider.GetOrSetAsync(async () => await this.GetAuthAdminImplAsync());
    }
}