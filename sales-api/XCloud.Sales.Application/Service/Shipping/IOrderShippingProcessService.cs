﻿using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Shipping;

public interface IOrderShippingProcessService : ISalesAppService
{
    Task MarkAsShippingAsync(string orderId);

    Task MarkAsShippedAsync(string orderId);
}

public class OrderShippingProcessService : SalesAppService, IOrderShippingProcessService
{
    private readonly OrderUtils _orderUtils;
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly IOrderNoteService _orderNoteService;

    public OrderShippingProcessService(OrderUtils orderUtils,
        ISalesRepository<Order> orderRepository,
        IOrderNoteService orderNoteService)
    {
        _orderUtils = orderUtils;
        _orderRepository = orderRepository;
        _orderNoteService = orderNoteService;
    }

    public async Task MarkAsShippingAsync(string orderId)
    {
        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, orderId);

        if (order.IsShipping())
        {
            this.Logger.LogInformation($"skip:{nameof(MarkAsShippingAsync)}");
            return;
        }

        order.ShippingStatusId = SalesConstants.ShippingStatus.Shipping;
        order.LastModificationTime = Clock.Now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);

        await _orderNoteService.InsertAsync(new OrderNoteDto()
        {
            OrderId = order.Id,
            Note = $"{this.L["order.shipping.note"]}",
            DisplayToUser = true,
            CreationTime = Clock.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Shipping
            }
        });
    }

    public async Task MarkAsShippedAsync(string orderId)
    {
        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, orderId);

        if (order.IsShipped())
        {
            this.Logger.LogInformation($"skip:{nameof(MarkAsShippedAsync)}");
            return;
        }

        order.ShippingStatusId = SalesConstants.ShippingStatus.Shipped;
        order.LastModificationTime = Clock.Now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);

        await _orderNoteService.InsertAsync(new OrderNoteDto()
        {
            OrderId = order.Id,
            Note = $"{this.L["order.shipped.note"]}",
            DisplayToUser = true,
            CreationTime = Clock.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Shipped
            }
        });
    }
}