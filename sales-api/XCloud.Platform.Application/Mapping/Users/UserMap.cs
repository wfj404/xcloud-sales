﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Users;

namespace XCloud.Platform.Application.Mapping.Users;

public class UserMap : PlatformEntityTypeConfiguration<User>
{
    public override void Configure(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("sys_user");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();
        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigAbpSoftDeletionTimeEntity();
        builder.PreConfigIdentityNameEntity();
        builder.PreConfigExternalBinding();

        builder.Property(x => x.NickName).HasMaxLength(100);
        builder.Property(x => x.PassWord).HasMaxLength(100);
        builder.Property(x => x.Avatar).HasMaxLength(2000);

        builder.Property(x => x.LastPasswordUpdateTime);
        builder.Property(x => x.IsActive).HasDefaultValue(false);
        builder.Property(x => x.Gender);

        base.Configure(builder);
    }
}