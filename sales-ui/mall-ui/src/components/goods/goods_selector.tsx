import { SelectProps } from 'antd';
import { CSSProperties } from 'react';

import XSearchSelector, {
  SearchSelectorItem,
} from '@/components/common/search_selector';
import { apiClient } from '@/utils/client';
import { GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export default ({
  selectedGoods,
  onChange,
  style,
  props,
}: {
  selectedGoods?: GoodsDto;
  onChange?: (d?: GoodsDto) => void;
  style?: CSSProperties;
  props?: SelectProps;
}) => {
  const queryImpl = async (kwd: string): Promise<SearchSelectorItem[]> => {
    if (isStringEmpty(kwd)) {
      return [];
    }

    const res = await apiClient.mallAdmin.goodsQueryGoodsForSelection({
      Keyword: kwd,
      PageSize: 20,
    });

    const options = res?.data?.Data?.map((x) => ({
      label: x.Name || '--',
      value: x.Id || '',
    }));

    return options || [];
  };

  return (
    <>
      <XSearchSelector
        style={style}
        props={props}
        onSearch={async (x) => await queryImpl(x)}
        placeholder="搜索商品..."
        value={selectedGoods?.Id || undefined}
        initItem={
          selectedGoods == null
            ? undefined
            : {
                label: selectedGoods.Name || '',
                value: selectedGoods.Id || '',
              }
        }
        onChange={(x) => {
          onChange &&
            onChange(
              x == undefined ? undefined : { Id: x.value, Name: x.label },
            );
        }}
      />
    </>
  );
};
