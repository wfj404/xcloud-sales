﻿using Volo.Abp.Application.Dtos;

namespace XCloud.Application.Gis;

public class GeoLocationDto : IEntityDto
{
    public static double InValidValue => double.MaxValue;

    public static GeoLocationDto InValidLocation => new GeoLocationDto()
    {
        Lat = InValidValue,
        Lon = InValidValue
    };

    /// <summary>
    /// 纬度
    /// </summary>
    public double Lat { get; set; } = GeoLocationDto.InValidValue;

    /// <summary>
    /// 经度
    /// </summary>
    public double Lon { get; set; } = GeoLocationDto.InValidValue;

    public bool IsValid => Nest.GeoLocation.IsValidLatitude(this.Lat) && Nest.GeoLocation.IsValidLongitude(this.Lon);
}