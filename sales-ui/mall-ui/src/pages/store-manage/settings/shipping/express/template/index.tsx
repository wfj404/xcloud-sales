import { useRequest } from 'ahooks';
import { Button } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { FreightTemplateDto } from '@/utils/swagger';
import { ProColumns, ProTable } from '@ant-design/pro-components';

import XTemplateForm from './form';

export default () => {
  const queryTemplate = useRequest(
    apiClient.mallManager.freightTemplateQueryAll,
    {
      manual: true,
    },
  );
  const templateData = queryTemplate.data?.data?.Data || [];

  const [formData, _formData] = useState<FreightTemplateDto | undefined>(
    undefined,
  );

  const columns: ProColumns<FreightTemplateDto>[] = [
    {
      dataIndex: 'index',
      valueType: 'indexBorder',
      width: 100,
    },
    {
      title: '名称',
      render: (d, x) => x.Name || '--',
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      title: '编辑',
      width: 200,
      render: (d, x) => {
        return (
          <Button
            type={'link'}
            onClick={() => {
              _formData({ ...x });
            }}
          >
            编辑
          </Button>
        );
      },
    },
  ];

  useEffect(() => {
    queryTemplate.run({});
  }, []);

  return (
    <>
      <XTemplateForm
        show={formData != undefined}
        data={formData || {}}
        ok={() => {
          queryTemplate.run({});
          _formData(undefined);
        }}
        hide={() => {
          _formData(undefined);
        }}
      />
      <ProTable
        style={{}}
        loading={queryTemplate.loading}
        columns={columns}
        dataSource={templateData}
        pagination={false}
        search={false}
        toolbar={{
          title: '运费模板',
          settings: [],
        }}
        toolBarRender={() => [
          <Button
            type={'link'}
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>,
        ]}
      />
    </>
  );
};
