﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Warehouses.Service.Warehouses;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/stock")]
public class StockController : ShopBaseController
{
    private readonly IStockService _stockService;
    private readonly IStockItemService _stockItemService;

    public StockController(IStockService stockService, IStockItemService stockItemService)
    {
        _stockService = stockService;
        _stockItemService = stockItemService;
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<StockDto>> StockPagingAsync([FromBody] QueryWarehouseStockPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        var response = await _stockService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _stockService.AttachWarehouseAsync(response.Items.ToArray());
            await _stockService.AttachSuppliersAsync(response.Items.ToArray());
            
            await _stockService.AttachItemsAsync(response.Items.ToArray());
            var items = response.Items.SelectMany(x => x.Items).WhereNotNull().ToArray();
            await _stockItemService.AttachSkusAsync(items);
        }

        return response;
    }

    [HttpPost("approve")]
    public async Task<ApiResponse<object>> ApproveStockAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        await _stockService.ApproveStockAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("delete-unapproved-stock")]
    public async Task<ApiResponse<object>> DeleteUnApprovedStockAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        await _stockService.DeleteUnApprovedStockAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("insert-stock")]
    public async Task<ApiResponse<object>> InsertStockAsync([FromBody] StockDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        await _stockService.InsertWarehouseStockAsync(dto);

        return new ApiResponse<object>();
    }
}