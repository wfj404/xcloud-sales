using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Service.Notifications;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.Shipping.Pickup;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/pickup")]
public class PickupController : ShopBaseController
{
    private readonly IOrderAutoProcessService _orderAutoProcessService;
    private readonly IStoreSettingService _storeSettingService;
    private readonly IPickupService _pickupService;
    private readonly IOrderService _orderService;
    private readonly INotificationService _notificationService;

    public PickupController(
        IStoreSettingService storeSettingService,
        IPickupService pickupService,
        IOrderService orderService,
        IOrderAutoProcessService orderAutoProcessService, INotificationService notificationService)
    {
        _storeSettingService = storeSettingService;
        _pickupService = pickupService;
        _orderService = orderService;
        _orderAutoProcessService = orderAutoProcessService;
        _notificationService = notificationService;
    }

    [HttpPost("create-pickup")]
    public async Task<ApiResponse<PickupRecord>> CreatePickupAsync([FromBody] PickupRecordDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var order = await _orderService.EnsureStoreOrderAsync(dto.OrderId, manager.StoreId);

        var pickup = await _pickupService.CreatePickupRecordAsync(dto);

        await _notificationService.InsertAsync(new NotificationDto
        {
            UserId = order.UserId,
            Title = "order is ready to pickup",
            Content = "",
            Data = string.Empty,
        });

        return new ApiResponse<PickupRecord>(pickup);
    }

    [HttpPost("by-id")]
    public async Task<ApiResponse<PickupRecordDto>> QueryByIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var pickup = await _pickupService.EnsureStorePickupAsync(dto.Id, manager.StoreId);

        await _pickupService.AttachOrdersAsync(new[] { pickup });

        if (pickup.Order != null)
        {
            await _orderService.AttachOrderItemsAsync(new[] { pickup.Order });
        }

        return new ApiResponse<PickupRecordDto>(pickup);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<PickupRecordDto>> QueryPagingAsync([FromBody] QueryPickupPagingInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        dto.StoreId = manager.StoreId;

        var response = await _pickupService.QueryPagingAsync(dto);

        await _pickupService.AttachOrdersAsync(response.Items.ToArray());

        return response;
    }

    [HttpPost("approve-or-not")]
    public async Task<ApiResponse<object>> ApproveOrNotAsync([FromBody] KeyValuePair<string, bool> dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var pickup = await _pickupService.EnsureStorePickupAsync(dto.Key, manager.StoreId);

        await _pickupService.SetPickupApprovedAsync(pickup.Id, dto.Value);

        return new ApiResponse<object>();
    }

    [HttpPost("set-prepared")]
    public async Task<ApiResponse<object>> SetPreparedAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var pickup = await _pickupService.EnsureStorePickupAsync(dto.Id, manager.StoreId);

        await _pickupService.SetReadyForPickupAsync(pickup.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("set-picked")]
    public async Task<ApiResponse<object>> SetPickedAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var pickup = await _pickupService.EnsureStorePickupAsync(dto.Id, manager.StoreId);

        await _pickupService.SetPickedAsync(pickup.Id);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _pickupService.TryUpdateOrderShipStatusAsync(pickup.OrderId);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _orderAutoProcessService.AutoProcessOrderAsync(pickup.OrderId);

        return new ApiResponse<object>();
    }

    [HttpPost("get-settings")]
    public async Task<ApiResponse<StorePickupSettings>> GetSettingsAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var settings = await _storeSettingService.GetSettingsAsync<StorePickupSettings>(manager.StoreId);

        return new ApiResponse<StorePickupSettings>(settings);
    }

    [HttpPost("save-settings")]
    public async Task<ApiResponse<object>> SaveSettingsAsync([FromBody] StorePickupSettings settings)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        settings.ServiceTime?.TimeRanges.TrySetIds(this.GuidGenerator);

        await _storeSettingService.SaveSettingsAsync(manager.StoreId, settings);

        return new ApiResponse<object>();
    }

    [HttpPost("get-by-order-id")]
    public async Task<ApiResponse<PickupRecordDto[]>> GetByOrderIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var data = await _pickupService.GetByOrderIdAsync(dto.Id);

        return new ApiResponse<PickupRecordDto[]>(data);
    }
}