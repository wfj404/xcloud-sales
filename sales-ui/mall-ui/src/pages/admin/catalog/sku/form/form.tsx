import {
  Alert,
  Card,
  Col,
  ColorPicker,
  Form,
  Input,
  InputNumber,
  Row,
  Switch,
  Tag,
  Tooltip,
} from 'antd';

import XGoodsSelector from '@/components/goods/goods_selector';
import { GoodsDto, SkuDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export default ({
  goods,
  data,
  _data,
}: {
  goods?: GoodsDto;
  data: SkuDto;
  _data: (d: SkuDto) => void;
}) => {
  const renderGoods = () => {
    if (goods) {
      return <Tag color={'blue'}>{goods.Name || '--'}</Tag>;
    }

    if (!isStringEmpty(data.Id)) {
      return (
        <Tooltip title={'edit mode'}>
          <Tag color={'blue'}>
            {data.Goods?.Name || 'error : spu not found'}
          </Tag>
        </Tooltip>
      );
    }

    if (data.Goods != null) {
      return (
        <Tag
          color={'blue'}
          closable
          onClose={() => {
            _data({
              ...data,
              GoodsId: '',
              Goods: undefined,
            });
          }}
        >
          {data.Goods.Name || '--'}
        </Tag>
      );
    }
    return (
      <XGoodsSelector
        onChange={(e) => {
          _data({
            ...data,
            GoodsId: e?.Id,
            Goods: e,
          });
        }}
      />
    );
  };

  return (
    <>
      <Card title="基础信息">
        <Form
          labelCol={{ flex: '110px' }}
          labelAlign="right"
          wrapperCol={{ flex: 1 }}
        >
          <Row gutter={10}>
            <Col span={8}>
              <Form.Item
                label={'基础商品信息'}
                tooltip={'known as spu，保存后不可以修改'}
              >
                {renderGoods()}
              </Form.Item>
            </Col>
            <Col span={8}></Col>
            <Col span={8}></Col>
          </Row>
          <Row gutter={10}>
            <Col span={8}>
              <Form.Item label={'规格名称'} required>
                <Input
                  required
                  count={{
                    show: true,
                    max: 20,
                  }}
                  maxLength={20}
                  value={data.Name || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      Name: e.target.value,
                    });
                  }}
                />
                {isStringEmpty(data.Name) && (
                  <p style={{ color: 'red' }}>不可留空</p>
                )}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'sku编码'} tooltip={'通常是商品条码'}>
                <Input
                  maxLength={100}
                  value={data.SkuCode || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      SkuCode: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'颜色'}>
                <ColorPicker
                  allowClear
                  value={data.Color as string | undefined}
                  onChange={(e) => {
                    _data({
                      ...data,
                      Color: e.toHex(),
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={16}>
              <Form.Item label={'描述信息'}>
                <Input.TextArea
                  value={data.Description || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      Description: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={10}>
            <Col span={8}>
              <Form.Item label={'重量'} tooltip={'kg'}>
                <InputNumber
                  min={0}
                  value={data.Weight || 0}
                  onChange={(e) => {
                    _data({
                      ...data,
                      Weight: e || 0,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={10}>
            <Col span={8}>
              <Form.Item label="是否可用">
                <Switch
                  checked={data.IsActive || false}
                  onChange={(e) => {
                    _data({
                      ...data,
                      IsActive: e,
                    });
                  }}
                />
                {data.IsActive || <Alert message={'当前sku不可以销售'} />}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'最少购买数量'}>
                <InputNumber
                  min={0}
                  value={data.MinAmountInOnePurchase || 0}
                  onChange={(e) => {
                    _data({
                      ...data,
                      MinAmountInOnePurchase: e || 0,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label={'最大购买数量'}>
                <InputNumber
                  min={0}
                  value={data.MaxAmountInOnePurchase || 0}
                  onChange={(e) => {
                    _data({
                      ...data,
                      MaxAmountInOnePurchase: e || 0,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
    </>
  );
};
