using System;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Timing;
using XCloud.Platform.Messenger.Service;

namespace XCloud.Platform.Messenger.Abstraction.Tasks.Impl;

[ExposeServices(typeof(IMessengerTask))]
public class ServerInstanceHeartBeatsTask : IMessengerTask, IScopedDependency
{
    private readonly IServerInstanceService _serverInstanceService;
    private readonly IMessengerServer _messengerServer;
    private readonly IClock _clock;

    public ServerInstanceHeartBeatsTask(IServerInstanceService serverInstanceService,
        IMessengerServer messengerServer,
        IClock clock)
    {
        _serverInstanceService = serverInstanceService;
        _messengerServer = messengerServer;
        _clock = clock;
    }

    public TimeSpan Delay => TimeSpan.FromSeconds(30);

    public async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        await _serverInstanceService.PingAsync(new ServerInstanceDto
        {
            InstanceId = _messengerServer.ServerInstanceId,
            ConnectionCount = _messengerServer.ConnectionManager.AsReadOnlyList().Count,
            PingTime = _clock.Now
        });
    }
}