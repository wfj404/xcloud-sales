import { Button, Form, Input, message, Modal, Space, Spin, Switch } from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import XSinglePictureUploader from '@/components/upload/single_picture_form';
import { apiClient } from '@/utils/client';
import { BrandDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: BrandDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [model, _model] = useState<BrandDto>({});

  const save = (row: BrandDto) => {
    row.Name = row.Name?.trim();
    if (isStringEmpty(row.Name)) {
      return;
    }

    _loading(true);

    apiClient.mallAdmin
      .brandSaveBrand({ ...row })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...data,
    });
  }, [data]);

  useEffect(() => {
    if (!show) {
      _model({});
    }
  }, [show]);

  return (
    <>
      <Modal
        title={'品牌'}
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                let res = await apiClient.mallAdmin.brandDelete({
                  Id: model.Id,
                });
                handleResponse(res, () => {
                  hide();
                  ok();
                });
              }}
              hide={isStringEmpty(model.Id)}
            />
            <Button
              type={'primary'}
              loading={loading}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label={'图片'}>
              <XSinglePictureUploader
                loadingSave={loading}
                data={model.Picture || {}}
                ok={(res) => {
                  _model({
                    ...model,
                    PictureMetaId: res.Id,
                    Picture: res,
                  });
                }}
                remove={() => {
                  if (confirm('确定删除图片吗？')) {
                    _model({
                      ...model,
                      PictureMetaId: undefined,
                      Picture: undefined,
                    });
                  }
                }}
              />
            </Form.Item>
            <Form.Item label="名称" required>
              <Input
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
              {isStringEmpty(model.Name) && (
                <p style={{ color: 'red' }}>必填</p>
              )}
            </Form.Item>
            <Form.Item label="描述" rules={[{ max: 50 }]}>
              <Input.TextArea
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="关键词" rules={[{ max: 200 }]}>
              <Input.TextArea
                value={model.MetaKeywords || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    MetaKeywords: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="首页显示">
              <Switch
                checked={model.ShowOnPublicPage}
                onChange={(e) => {
                  _model({
                    ...model,
                    ShowOnPublicPage: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="是否发布">
              <Switch
                checked={model.Published}
                onChange={(e) => {
                  _model({
                    ...model,
                    Published: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
