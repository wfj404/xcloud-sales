import { useRequest } from 'ahooks';
import React, { useEffect, useState } from 'react';

import { PaymentStatus } from '@/utils/biz';
/*
* {
      key: 'wechat',
      name: '微信支付',
      btn: '微信支付',
      disabled: true,
      action: () => {
        paywithwechat();
      },
    },
* */
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import {
  formatMoney,
  handleResponse,
  isStringEmpty,
  successMsg,
} from '@/utils/utils';
import { PaymentOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Button, FormControlLabel } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FormControl from '@mui/material/FormControl';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';

interface PaymentOption {
  key: 'balance' | 'wechat' | 'alipay';
  name: string;
  btn: string;
  disabled: boolean;
  action: () => void;
}

export default function ({ model, ok }: { model: OrderDto; ok: () => void }) {
  const [show, _show] = useState<boolean>(false);

  const get_money_to_pay_request = useRequest(
    apiClient.mall.orderGetMoneyToPay,
    { manual: true },
  );

  const money_to_pay = get_money_to_pay_request.data?.data?.Data || 0;

  const get_balance_request = useRequest(
    apiClient.mall.userUserBalanceAndPoints,
    { manual: true },
  );

  const balance = get_balance_request.data?.data?.Data?.Balance || 0;

  const balanceEnough = balance >= money_to_pay;

  const get_wechat_mp_settings_request = useRequest(
    apiClient.platform.wechatMpAuthGetWechatMpOption,
    { manual: true },
  );
  const wechat_mp_appid =
    get_wechat_mp_settings_request.data?.data?.Data?.AppId;

  useEffect(() => {
    if (show) {
      get_balance_request.run({});
      get_money_to_pay_request.run({ Id: model.Id });
      get_wechat_mp_settings_request.run({});
    }
  }, [show]);

  const pay_with_balance_request = useRequest(
    apiClient.mall.balancePayWithBalance,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          _show(false);
          successMsg('支付成功');
          ok && ok();
        });
      },
    },
  );

  const any_pay_loading = pay_with_balance_request.loading;

  const [selectedPayment, _selectedPayment] = useState<
    string | null | undefined
  >(undefined);

  const paymentOption: PaymentOption[] = [
    {
      key: 'wechat',
      name: '微信支付',
      btn: '微信支付',
      disabled: isStringEmpty(wechat_mp_appid) || true,
      action: () => {
        alert('todo');
      },
    },
    {
      key: 'alipay',
      name: '支付宝支付',
      btn: '支付宝支付',
      disabled: true,
      action: () => {
        alert('todo');
      },
    },
    {
      key: 'balance',
      name: get_balance_request.loading
        ? `loading...`
        : balanceEnough
          ? `余额支付（${formatMoney(balance)})`
          : `余额支付（${formatMoney(balance)}，余额不足）`,
      btn: '余额支付',
      disabled: !balanceEnough,
      action: () => {
        pay_with_balance_request.run({ Id: model.Id });
      },
    },
  ];

  const selectedOption = paymentOption.find((x) => x.key == selectedPayment);

  useEffect(() => {
    _selectedPayment(paymentOption.filter((x) => !x.disabled)?.at(0)?.key);
  }, [money_to_pay]);

  if (model.PaymentStatusId == PaymentStatus.Paid) {
    return <h3>订单已支付</h3>;
  }

  if (money_to_pay <= 0) {
    return <h3>订单无需再支付</h3>;
  }

  return (
    <>
      <Button
        startIcon={<PaymentOutlined />}
        size={'large'}
        variant={'outlined'}
        onClick={() => {
          _show(true);
        }}
      >
        支付
      </Button>
      <Dialog
        open={show}
        onClose={() => {
          _show(false);
        }}
        fullWidth
      >
        <DialogTitle>收银台</DialogTitle>
        <DialogContent>
          <p>订单仍需支付{formatMoney(money_to_pay)}</p>
          <DialogContentText>请选择支付方式</DialogContentText>
          <FormControl>
            <RadioGroup value={selectedPayment}>
              {paymentOption.map((x, index) => (
                <FormControlLabel
                  key={index}
                  value={x.key}
                  disabled={x.disabled}
                  control={<Radio />}
                  label={x.name}
                />
              ))}
            </RadioGroup>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              _show(false);
            }}
          >
            取消
          </Button>
          <LoadingButton
            disabled={!selectedOption}
            onClick={() => {
              selectedOption && selectedOption.action();
            }}
            loading={any_pay_loading}
          >
            {selectedOption?.btn || '支付'}
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}
