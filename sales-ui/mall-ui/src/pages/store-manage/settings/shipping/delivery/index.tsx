import { useRequest } from 'ahooks';
import { Button, Divider, Form, InputNumber, message, Segmented, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StoreShortDistanceDeliverySettings } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XServiceTime from '../components/service_time';
import XTieredPrice from '../components/tiered_price';

export default () => {
  const querySettings = useRequest(
    apiClient.mallManager.deliverySettingsGetShortDistanceDeliverySettings,
    {
      manual: true,
      onSuccess: (e) => {
        _settings(e.data?.Data || {});
      },
    },
  );
  const [settings, _settings] = useState<StoreShortDistanceDeliverySettings>(
    {},
  );
  const [loadingSaveSettings, _loadingSaveSettings] = useState(false);

  const saveSettings = () => {
    _loadingSaveSettings(true);
    apiClient.mallManager
      .deliverySettingsSaveShortDistanceDeliverySettings({
        ...settings,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          querySettings.run({});
        });
      })
      .finally(() => {
        _loadingSaveSettings(false);
      });
  };

  useEffect(() => {
    querySettings.run({});
  }, []);

  return (
    <>
      <div>
        <div
          style={{
            marginBottom: 20,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <h2>同城配送</h2>
          <Button
            type={'primary'}
            loading={loadingSaveSettings}
            onClick={() => {
              saveSettings();
            }}
          >
            保存设置
          </Button>
        </div>
        <div
          style={{
            marginBottom: 10,
          }}
        >
          <Form
            labelCol={{ span: 3 }}
            style={{
              marginBottom: 10,
            }}
          >
            <Form.Item
              label={'支持同城配送'}
              tooltip={'用户可在购买商品时选择该配送方式'}
            >
              <Switch
                loading={loadingSaveSettings || querySettings.loading}
                checked={settings.Enabled}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    Enabled: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label={'支持货到付款'} tooltip={'需要上门收款'}>
              <Switch
                checked={settings.PayAfterShippedSupported}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    PayAfterShippedSupported: e,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label={'订单最低消费'}>
              <InputNumber
                min={0}
                value={settings.MinimalOrderPrice}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    MinimalOrderPrice: e,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label={'最远配送距离'}>
              <InputNumber
                min={0}
                value={settings.MaxDistance}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    MaxDistance: e,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label={'计费方式'}>
              <Segmented
                value={settings.CalculatePriceType || undefined}
                onChange={(e) => {
                  _settings({
                    ...settings,
                    CalculatePriceType: e?.toString(),
                  });
                }}
                options={[
                  {
                    label: '免费',
                    value: 'free',
                  },
                  {
                    label: '阶梯计费',
                    value: 'tiered',
                  },
                ]}
              />
            </Form.Item>
            {settings.CalculatePriceType == 'tiered' && (
              <Form.Item label={'阶梯计费'}>
                <XTieredPrice
                  unit="km"
                  data={settings.TieredPrice || {}}
                  onChange={(e) => {
                    _settings({
                      ...settings,
                      TieredPrice: e,
                    });
                  }}
                />
              </Form.Item>
            )}
          </Form>
          <Divider type="horizontal" />
          <XServiceTime
            data={settings.ServiceTime || {}}
            ok={(x) => {
              _settings({
                ...settings,
                ServiceTime: { ...x },
              });
            }}
          />
        </div>
      </div>
    </>
  );
};
