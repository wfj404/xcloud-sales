import {
  Button,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Space,
  Spin,
  Tag,
  Tooltip,
} from 'antd';
import { useState } from 'react';

import XSkuSelector from '@/components/goods/sku/sku_selector';
import { getSkuName } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { SkuDto, SpecDto, SpecValueDto } from '@/utils/swagger';
import { formatMoney, isNumberEmpty, isStringEmpty } from '@/utils/utils';
import { DeleteOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';

export default ({ model, ok }: { model: SpecDto; ok: () => void }) => {
  const data = model?.Values || [];

  const [loadingSave, _loadingSave] = useState(false);
  const [form, _form] = useState<SpecValueDto | undefined>(undefined);
  const f: SpecValueDto = form || {};

  const save = (row: SpecValueDto) => {
    _loadingSave(true);
    apiClient.mallAdmin
      .specValueEdit({
        ...row,
        SpecId: model.Id,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          _form(undefined);
          ok && ok();
        }
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const deleteSpecValue = (record: SpecValueDto) => {
    if (!confirm('确定删除规格')) {
      return;
    }
    _loadingSave(true);
    apiClient.mallAdmin
      .specValueDelete({ Id: record.Id })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          _form(undefined);
          ok && ok();
        }
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const renderPriceOffset = (x: SpecValueDto) => {
    const price = x.PriceOffset || 0;
    if (price > 0) {
      return <span>{`+${formatMoney(price)}`}</span>;
    } else if (price < 0) {
      return <span>{`-${formatMoney(price)}`}</span>;
    } else {
      return null;
    }
  };

  const get_error_associate_sku = (): SkuDto | undefined => {
    if (form?.AssociatedSkuId) {
      return {
        Id: form.AssociatedSkuId,
        Name: `错误的规格id: ${form.AssociatedSkuId}`,
      };
    }

    return undefined;
  };

  const error_associate_sku = get_error_associate_sku();

  return (
    <>
      <Modal
        title={'规格参数'}
        open={form != undefined}
        onCancel={() => _form(undefined)}
        footer={
          <Space size={2}>
            {isNumberEmpty(f.Id) || (
              <Button
                type="primary"
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  deleteSpecValue(f);
                }}
              >
                删除
              </Button>
            )}
            <Button
              type="primary"
              icon={<SaveOutlined />}
              onClick={() => {
                save(f);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loadingSave}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="规格名称">
              <Tag color="processing">{model.Name || '--'}</Tag>
            </Form.Item>
            <Form.Item
              label="名称"
              rules={[
                {
                  required: true,
                },
                {
                  max: 20,
                },
              ]}
            >
              <Input
                value={f.Name || ''}
                onChange={(e) => {
                  _form({
                    ...form,
                    Name: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="差价">
              <InputNumber
                defaultValue={f.PriceOffset || undefined}
                onChange={(e) => {
                  _form({
                    ...form,
                    PriceOffset: e || undefined,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="关联物料" tooltip="检查并扣减物料库存">
              <Space direction="vertical" style={{ width: '100%' }}>
                <XSkuSelector
                  props={{
                    status:
                      form?.AssociatedSku == undefined &&
                      error_associate_sku != undefined
                        ? 'error'
                        : undefined,
                  }}
                  selectedSku={form?.AssociatedSku || error_associate_sku}
                  onChange={(e) => {
                    _form({
                      ...form,
                      AssociatedSku: e,
                      AssociatedSkuId: e?.Id,
                    });
                  }}
                />
                {isStringEmpty(form?.AssociatedSkuId) || (
                  <InputNumber
                    addonBefore={<b>物料数量</b>}
                    status={
                      (form?.AssociatedSkuCount || 0) < 1 ? 'error' : undefined
                    }
                    value={form?.AssociatedSkuCount || 0}
                    onChange={(e) => {
                      _form({
                        ...form,
                        AssociatedSkuCount: e || 0,
                      });
                    }}
                    min={1}
                  />
                )}
              </Space>
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
      <Space wrap direction="horizontal" size={2}>
        {data.map((x, index) => {
          const sku_name = getSkuName(x.AssociatedSku);
          return (
            <Tag
              key={index}
              color={'blue'}
              style={{
                cursor: 'pointer',
              }}
              onClick={() => {
                _form(x);
              }}
              closable={false}
            >
              <Tooltip
                title={
                  sku_name
                    ? `关联物料：${sku_name} * ${x.AssociatedSkuCount || 0}`
                    : undefined
                }
              >
                <Space direction="horizontal">
                  <span>{x.Name}</span>
                  {renderPriceOffset(x)}
                </Space>
              </Tooltip>
            </Tag>
          );
        })}
        <Tooltip title="添加规格明细">
          <Button
            icon={<PlusOutlined />}
            onClick={() => {
              _form({
                PriceOffset: 0,
                AssociatedSkuCount: 1,
              });
            }}
          ></Button>
        </Tooltip>
      </Space>
    </>
  );
};
