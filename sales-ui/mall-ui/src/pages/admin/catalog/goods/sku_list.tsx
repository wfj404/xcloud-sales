import { Badge, Button, Popover, QRCode, Tag } from 'antd';
import { useState } from 'react';

import ImgPreview from '@/components/common/preview_group';
import XTime from '@/components/common/time';
import { sortGoodsPictures } from '@/utils/biz';
import { GoodsDto, SkuDto } from '@/utils/swagger';
import { formatMoney, isArrayEmpty, isStringEmpty } from '@/utils/utils';
import { PlusOutlined } from '@ant-design/icons';
import { ProColumns, ProTable } from '@ant-design/pro-components';

import XForm from '../sku/form';

export default ({
  goods,
  items,
  ok,
}: {
  goods: GoodsDto;
  items: SkuDto[];
  ok: () => void;
}) => {
  const [skuId, _skuId] = useState<string | undefined | null>(undefined);

  const columns: ProColumns<SkuDto>[] = [
    {
      title: '名称',
      width: 150,
      fixed: 'left',
      render: (d, x) => (
        <>
          <div>
            <b>{x.Name}</b>
          </div>
          {isStringEmpty(x.SkuCode) || (
            <Popover
              content={
                <QRCode
                  value={x.SkuCode || ''}
                  style={{
                    height: 50,
                  }}
                />
              }
              title="SKU"
            >
              <div>
                <Tag>{`编号：${x.SkuCode}`}</Tag>
              </div>
            </Popover>
          )}
          {isArrayEmpty(x.SpecCombinationErrors) || (
            <div style={{ color: 'red', fontWeight: 'lighter' }}>
              规格参数存在错误
            </div>
          )}
        </>
      ),
    },
    {
      title: '价格',
      render: (d, x) => {
        return (
          <div>
            <Tag color={'warning'}>
              <b>{formatMoney(x.Price || 0)}</b>
            </Tag>
          </div>
        );
      },
    },
    {
      title: '状态',
      render: (d, x) => {
        return (
          <>
            {x.IsActive && <Badge status={'success'} text={'可用'} />}
            {x.IsActive || <Badge status={'error'} text={'不可用'} />}
          </>
        );
      },
    },
    {
      title: '在售门店',
      render: (d, x) => {
        const names = x.StoreGoodsMapping?.map(
          (x) => x.Store?.Name || null,
        ).filter((x) => !isStringEmpty(x));
        if (!names || isArrayEmpty(names)) {
          return <span>--</span>;
        }

        return (
          <>
            {names.map((x, i) => (
              <div key={i}>{`${i + 1}. ${x}`}</div>
            ))}
          </>
        );
      },
    },
    {
      title: '图片',
      render: (d, x) => {
        const sortedPictures = sortGoodsPictures(
          x.GoodsPictures?.filter((d) => d.SkuId == x.Id) || [],
        ).map((x) => x.StorageMeta || {});

        return (
          <div
            style={{
              maxWidth: 300,
            }}
          >
            <ImgPreview count={3} data={sortedPictures} />
          </div>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '详情',
      render: (d, x) => {
        if (x.IsDeleted) {
          return null;
        }
        return (
          <>
            <Button
              type="link"
              onClick={() => {
                _skuId(x.Id || undefined);
              }}
            >
              编辑
            </Button>
          </>
        );
      },
    },
  ];

  return (
    <>
      <XForm
        goods={goods}
        show={skuId != undefined}
        skuId={skuId || ''}
        hide={() => {
          _skuId(undefined);
          ok();
        }}
        ok={() => {
          _skuId(undefined);
          ok();
        }}
      />
      <ProTable
        headerTitle={'规则明细'}
        toolBarRender={() => [
          <Button
            type={'link'}
            icon={<PlusOutlined />}
            onClick={() => {
              _skuId('');
            }}
          >
            添加sku
          </Button>,
        ]}
        bordered
        search={false}
        options={false}
        rowKey={(x) => x.Id || ''}
        columns={columns}
        dataSource={items}
        pagination={false}
      />
    </>
  );
};
