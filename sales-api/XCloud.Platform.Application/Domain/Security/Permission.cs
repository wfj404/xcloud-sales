namespace XCloud.Platform.Application.Domain.Security;

public class Permission : PlatformBaseEntity, IHasCreationTime
{
    public string PermissionKey { get; set; }
    public string Description { get; set; }
    public string Group { get; set; }
    public string AppKey { get; set; }
    public DateTime CreationTime { get; set; }
}