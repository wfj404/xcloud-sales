﻿using JetBrains.Annotations;
using Newtonsoft.Json;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Service.Admins;

namespace XCloud.Platform.Application.Service.Users;

public class SendSmsDto : IEntityDto
{
    public string Mobile { get; set; }
    public string CaptchaCode { get; set; }
}

public class SmsLoginDto : IEntityDto
{
    public string Mobile { get; set; }
    public string SmsCode { get; set; }
}

public class PasswordLoginDto : IEntityDto
{
    public string IdentityName { get; set; }

    public string Password { get; set; }
}

public class UserAuthResult : ApiResponse<UserDto>
{
    public bool? TokenIsExpired { get; set; }

    [CanBeNull] public UserDto User => Data;

    [NotNull]
    public UserDto GetRequiredUser() => this.User ?? throw new BusinessException(nameof(UserDto));
}

public static class ThirdPartyPlatforms
{
    public static string WxMp = "wx-mp";

    public static string WxOpen = "wx-open";
}

public class UserExternalAccessTokenDto : ExternalAccessToken, IEntityDto
{
    //
}

public class UserExternalConnectDto : ExternalConnect, IEntityDto
{
    //
}

public class GetValidUserAccessTokenDto : IEntityDto
{
    public string UserId { get; set; }
    public string AppId { get; set; }
    public string Platform { get; set; }
}

public class GetValidClientAccessTokenDto : IEntityDto
{
    public string AppId { get; set; }
    public string Platform { get; set; }
}

public class UserPhoneBindSmsMessage : IEntityDto
{
    public string Phone { get; set; }
    public string Code { get; set; }
}

public class UserMobileDto : IEntityDto<string>
{
    public UserMobileDto()
    {
        //
    }

    public UserMobileDto(UserIdentity userIdentity) : this()
    {
        Id = userIdentity.UserId;
        MobilePhone = userIdentity.MobilePhone;
        Confirmed = userIdentity.MobileConfirmed;
    }

    public string Id { get; set; }
    public string MobilePhone { get; set; }
    public bool? Confirmed { get; set; }
}

public class UserEmailDto : IEntityDto<string>
{
    public UserEmailDto()
    {
        //
    }

    public UserEmailDto(UserIdentity userIdentity) : this()
    {
        Id = userIdentity.UserId;
        Email = userIdentity.Email;
        Confirmed = userIdentity.EmailConfirmed;
    }

    public string Id { get; set; }

    public string Email { get; set; }

    public bool? Confirmed { get; set; }
}

public class SetIdCardDto : IEntityDto
{
    public string UserId { get; set; }
    public string IdCard { get; set; }
    public string RealName { get; set; }
}

public class SetUserMobileDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string Mobile { get; set; }
}

public class SetUserPasswordDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string Password { get; set; }
}

public class UpdateUserStatusDto : IdDto
{
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
}

public class LoginFailureRecordDto : IEntityDto<string>, IHasCreationTime
{
    public string Id { get; set; }

    public DateTime CreationTime { get; set; }
}

public class OAuthCodeDto : IEntityDto
{
    public string Code { get; set; }
    public bool? UseWechatProfile { get; set; }
}

public class SearchUserAccountDto : IEntityDto
{
    public string AccountIdentity { get; set; }
}

public class QueryUserPaging : PagedRequest
{
    public string Keyword { get; set; }

    public bool? IsActive { get; set; }
}

public class UserRealNameDto : UserRealName, IEntityDto<string>
{
    //
}

public class UserDto : User, IEntityDto<string>
{
    [Obsolete("always null")]
    [CanBeNull]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public override string PassWord
    {
        get => null;
        set { }
    }

    public AdminDto AdminIdentity { get; set; }

    public UserRealNameDto UserRealName { get; set; }

    public UserEmailDto[] UserEmails { get; set; }

    public UserMobileDto UserMobile { get; set; }
}