import { useRequest } from 'ahooks';
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  InputNumber,
  message,
  Row,
  Switch,
} from 'antd';
import { useEffect, useState } from 'react';

import XLogoUrl from '@/assets/logo-sm.png';
import XUploader from '@/components/upload/single_picture_form';
import { apiClient } from '@/utils/client';
import { MallSettingsDto } from '@/utils/swagger';
import { handleResponse, parseJsonOrEmpty } from '@/utils/utils';

export default () => {
  const [data, _data] = useState<MallSettingsDto>({});

  const query_setting_request = useRequest(
    apiClient.mallAdmin.settingGetMallSettings,
    {
      manual: true,
      onSuccess(res, params) {
        _data(res.data?.Data || {});
      },
    },
  );

  const save_settings_request = useRequest(
    apiClient.mallAdmin.settingSaveMallSettings,
    {
      manual: true,
      onSuccess(res, params) {
        handleResponse(res, () => {
          message.success('保存成功');
          query_setting_request.run();
        });
      },
    },
  );

  const saveSettings = (e: MallSettingsDto) => {
    save_settings_request.run(e);
  };

  useEffect(() => {
    query_setting_request.run();
  }, []);

  return (
    <>
      <Card
        title="商城设置"
        loading={query_setting_request.loading}
        style={{
          marginBottom: 10,
        }}
        extra={
          <Button
            loading={save_settings_request.loading}
            type="primary"
            onClick={() => {
              saveSettings(data);
            }}
          >
            保存设置
          </Button>
        }
      >
        <Form
          labelCol={{ flex: '210px' }}
          labelAlign="right"
          wrapperCol={{ flex: 1 }}
        >
          <Row gutter={[10, 10]} style={{ marginBottom: 10 }}>
            <Col span={12}>
              <Form.Item label={'全尺寸 LOGO'}>
                <XUploader
                  width={100}
                  defaultImage={XLogoUrl}
                  data={parseJsonOrEmpty(data.FullLogoStorageData) || {}}
                  ok={(e) => {
                    _data({
                      ...data,
                      FullLogoStorageData: JSON.stringify(e),
                    });
                  }}
                  remove={() => {
                    _data({
                      ...data,
                      FullLogoStorageData: undefined,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label={'小尺寸 LOGO'}>
                <XUploader
                  width={100}
                  defaultImage={XLogoUrl}
                  data={parseJsonOrEmpty(data.SimpleLogoStorageData) || {}}
                  ok={(e) => {
                    _data({
                      ...data,
                      SimpleLogoStorageData: JSON.stringify(e),
                    });
                  }}
                  remove={() => {
                    _data({
                      ...data,
                      SimpleLogoStorageData: null,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={10} style={{ marginBottom: 10 }}>
            <Col span={12}>
              <Form.Item label={'官方中文名字'}>
                <Input
                  value={data.StoreOfficialName || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      StoreOfficialName: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label={'官方英文名字'}>
                <Input
                  value={data.StoreOfficialEnglishName || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      StoreOfficialEnglishName: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={10} style={{ marginBottom: 10 }}>
            <Col span={12}>
              <Form.Item label={'中文昵称'}>
                <Input
                  value={data.StoreNickName || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      StoreNickName: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label={'英文昵称'}>
                <Input
                  value={data.StoreEnglishName || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      StoreEnglishName: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={10} style={{ marginBottom: 10 }}>
            <Col span={12}>
              <Form.Item label={'Footer Slogan'}>
                <Input
                  value={data.FooterSlogan || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      FooterSlogan: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label={'备案号'} tooltip="工信部备案号">
                <Input
                  value={data.FilingNumber || ''}
                  onChange={(e) => {
                    _data({
                      ...data,
                      FilingNumber: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label="首页提示">
            <Input
              value={data.HomePageNotice || ''}
              onChange={(e) => {
                _data({
                  ...data,
                  HomePageNotice: e.target.value || '',
                });
              }}
            />
          </Form.Item>
          <Form.Item label="首页轮播图" tooltip="图片地址，以逗号分隔">
            <Input.TextArea
              value={data.HomeSliderImages || ''}
              onChange={(e) => {
                _data({
                  ...data,
                  HomeSliderImages: e.target.value || '',
                });
              }}
            />
          </Form.Item>

          <Form.Item label="下单页提示">
            <Input
              value={data.PlaceOrderNotice || ''}
              onChange={(e) => {
                _data({
                  ...data,
                  PlaceOrderNotice: e.target.value || '',
                });
              }}
            />
          </Form.Item>
          <Form.Item label="登录页提示">
            <Input
              value={data.LoginNotice || ''}
              onChange={(e) => {
                _data({
                  ...data,
                  LoginNotice: e.target.value || '',
                });
              }}
            />
          </Form.Item>
          <Form.Item label="注册页提示">
            <Input
              value={data.RegisterNotice || ''}
              onChange={(e) => {
                _data({ ...data, RegisterNotice: e.target.value || '' });
              }}
            />
          </Form.Item>
          <Form.Item label="商品详情页提示">
            <Input
              value={data.GoodsDetailNotice || ''}
              onChange={(e) => {
                _data({
                  ...data,
                  GoodsDetailNotice: e.target.value || '',
                });
              }}
            />
          </Form.Item>
          <Form.Item label="价格不登陆可见">
            <Switch
              checked={data.GoodsSettings?.DisplayPriceForGuest || false}
              onChange={(e) => {
                let settings = data.GoodsSettings || {};
                settings = {
                  ...settings,
                  DisplayPriceForGuest: e,
                };
                _data({
                  ...data,
                  GoodsSettings: settings,
                });
              }}
            />
          </Form.Item>
          <Form.Item label="订单评价入口过期时间">
            <InputNumber
              value={data.OrderSettings?.ReviewEntryExpiredInDays}
              onChange={(e) => {
                let settings = data.OrderSettings || {};
                settings = {
                  ...settings,
                  ReviewEntryExpiredInDays: e,
                };
                _data({
                  ...data,
                  OrderSettings: settings,
                });
              }}
              addonAfter={<b>天</b>}
            />
          </Form.Item>
          <Form.Item label="禁止下单">
            <Switch
              checked={data.OrderSettings?.PlaceOrderDisabled || false}
              onChange={(e) => {
                let settings = data.OrderSettings || {};
                settings = {
                  ...settings,
                  PlaceOrderDisabled: e,
                };
                _data({
                  ...data,
                  OrderSettings: settings,
                });
              }}
            />
          </Form.Item>
          <Form.Item label="禁止售后">
            <Switch
              checked={data.AfterSalesSettings?.AfterSaleDisabled || false}
              onChange={(e) => {
                let settings = data.AfterSalesSettings || {};
                settings = {
                  ...settings,
                  AfterSaleDisabled: e,
                };
                _data({
                  ...data,
                  AfterSalesSettings: settings,
                });
              }}
            />
          </Form.Item>
        </Form>
      </Card>
    </>
  );
};
