﻿using System.Threading.Tasks;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Repository;
using JetBrains.Annotations;
using Volo.Abp.Domain.Entities;
using XCloud.Application.DistributedLock;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.EntityFrameworkCore.Extensions;

namespace XCloud.Platform.Application.Service.Admins;

public interface IAdminAccountService : IXCloudAppService
{
    Task UpdateStatusAsync(UpdateAdminStatusDto dto);

    [NotNull]
    Task<AdminDto> GetOrCreateByUserIdAsync(string userId);

    Task<AdminDto> GetAdminAuthValidationDataAsync(string userId, CacheStrategy cacheStrategyOption);
}

public class AdminAccountService : PlatformAppService, IAdminAccountService
{
    private readonly IPlatformRepository<Admin> _repository;

    public AdminAccountService(IPlatformRepository<Admin> repository)
    {
        _repository = repository;
    }

    public async Task<AdminDto> GetOrCreateByUserIdAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        await using var _ =
            await DistributedLockProvider.CreateRequiredLockAsync(
                resource: $"admin.{nameof(GetOrCreateByUserIdAsync)}.{userId}",
                expiryTime: TimeSpan.FromSeconds(5));

        var db = await _repository.GetDbContextAsync();

        var admin = await db.Set<Admin>().AsNoTracking().IgnoreQueryFilters()
            .Where(x => x.UserId == userId)
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync();

        if (admin == null)
        {
            admin = new Admin
            {
                Id = GuidGenerator.CreateGuidString(),
                CreationTime = Clock.Now,
                IsActive = true,
                IsSuperAdmin = false,
                UserId = userId
            };

            db.Set<Admin>().Add(admin);

            await db.TrySaveChangesAsync();

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return admin.MapTo<Admin, AdminDto>(ObjectMapper);
    }

    public async Task UpdateStatusAsync(UpdateAdminStatusDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var db = await _repository.GetDbContextAsync();

        var entity = await db.Set<Admin>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (entity == null)
            throw new EntityNotFoundException(nameof(UpdateStatusAsync));

        if (dto.IsActive != null)
            entity.IsActive = dto.IsActive.Value;

        if (dto.IsSuperAdmin != null)
            entity.IsSuperAdmin = dto.IsSuperAdmin.Value;

        await _repository.UpdateAsync(entity, autoSave: true);

        await this.GetAdminAuthValidationDataAsync(entity.UserId, new CacheStrategy() { Refresh = true });
    }

    public async Task<AdminDto> GetAdminAuthValidationDataAsync(string userId,
        CacheStrategy cacheStrategyOption)
    {
        var option = new CacheOption<AdminDto>($"user.{userId}.admin.data", TimeSpan.FromMinutes(1));

        var data = await CacheProvider.ExecuteWithPolicyAsync(
            async () =>
            {
                var db = await _repository.GetDbContextAsync();

                var query = db.Set<Admin>().IgnoreQueryFilters().AsNoTracking();

                query = query.Where(x => x.UserId == userId);

                var authData = await query.OrderBy(x => x.CreationTime).FirstOrDefaultAsync();

                if (authData == null)
                    return null;

                var dto = ObjectMapper.Map<Admin, AdminDto>(authData);
                return dto;
            },
            option,
            cacheStrategyOption);

        return data;
    }
}