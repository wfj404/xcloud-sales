﻿using XCloud.Application.Model;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Warehouses.Service.Warehouses;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/stock-item")]
public class StockItemController : ShopBaseController
{
    private readonly IStockService _stockService;
    private readonly IStockItemService _stockItemService;

    public StockItemController(IStockService stockService, IStockItemService stockItemService)
    {
        _stockService = stockService;
        _stockItemService = stockItemService;
    }

    [HttpPost("item-paging")]
    public async Task<PagedResponse<StockItemDto>> StockItemPagingAsync([FromBody] QueryWarehouseStockItemInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageStock);

        var response = await _stockItemService.QueryItemPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _stockItemService.AttachSkusAsync(response.Items.ToArray());
            await _stockItemService.AttachStockAsync(response.Items.ToArray());

            var stocks = response.Items
                .Select(x => x.Stock)
                .WhereNotNull()
                .ToArray();

            await _stockService.AttachWarehouseAsync(stocks);
            await _stockService.AttachSuppliersAsync(stocks);
        }

        return response;
    }
}