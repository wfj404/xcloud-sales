﻿using System;
using Volo.Abp.Domain.Entities;

namespace XCloud.Application.Model;

public interface IHasParent : IEntity<string>
{
    string ParentId { get; set; }
}

public interface IMayHaveTimeRange : IEntity
{
    DateTime? StartTime { get; set; }

    DateTime? EndTime { get; set; }
}