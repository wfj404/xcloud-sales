import { Form, Input } from 'antd';

import { HelloBlockProps } from './utils';

export default function({ data, onChange }: {
  data: HelloBlockProps,
  onChange: (d: HelloBlockProps) => void
}) {

  return <>
    <Form>
      <Form.Item label={'hello world'}>
        <Input value={data?.helloText} onChange={e => {
          onChange && onChange({ ...data, helloText: e.target.value });
        }} />
      </Form.Item>
    </Form>
  </>;
}
