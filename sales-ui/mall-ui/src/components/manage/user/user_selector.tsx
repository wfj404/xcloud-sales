import { useRequest } from 'ahooks';
import { Input, message, Modal } from 'antd';
import { CSSProperties } from 'react';

import { apiClient } from '@/utils/client';
import { UserDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

import XUserCard from './user_card';

export default ({
  onFind,
  style,
  loading,
}: {
  onFind: (e: UserDto) => void;
  style?: CSSProperties;
  loading?: boolean;
}) => {
  const searchUser = useRequest(apiClient.sys.manageUserSearchUserAccount, {
    manual: true,
    onSuccess: (e) => {
      const user = e.data?.Data;
      if (!user) {
        message.error('找不到账号');
        return;
      }
    },
  });

  const data = searchUser.data?.data?.Data;

  return (
    <>
      <Modal
        title="确定添加"
        open={data != undefined}
        onCancel={() => searchUser.mutate(undefined)}
        onOk={() => {
          data && onFind(data);
          searchUser.mutate(undefined);
        }}
      >
        <XUserCard model={data} />
      </Modal>
      <div
        style={{
          marginBottom: 10,
          marginTop: 10,
          ...style,
        }}
      >
        <Input.Search
          style={{
            width: '100%',
          }}
          loading={searchUser.loading || loading}
          onSearch={(e) => {
            if (isStringEmpty(e)) {
              message.error('输入账号');
              return;
            }
            searchUser.run({
              AccountIdentity: e,
            });
          }}
          placeholder={'搜索账号（手机号，邮箱）以添加...'}
          size="large"
        />
      </div>
    </>
  );
};
