import { useRequest } from 'ahooks';
import { Badge, Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import PreviewGroup from '@/components/common/preview_group';
import XTime from '@/components/common/time';
import { apiClient } from '@/utils/client';
import { BrandDto, QueryBrandPaging } from '@/utils/swagger';

import XForm from './form';
import XSearchForm from './search_form';

export default () => {
  const queryRequest = useRequest(apiClient.mallAdmin.brandQueryPaging, {
    manual: true,
  });

  const loading = queryRequest.loading;
  const data = queryRequest.data?.data || {};

  const [query, _query] = useState<QueryBrandPaging>({
    Page: 1,
  });

  const [formData, _formData] = useState<BrandDto | undefined>(undefined);

  const queryList = () => {
    queryRequest.run({ ...query });
  };

  const columns: ColumnProps<BrandDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name,
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      title: '关键词',
      render: (d, x) => x.MetaKeywords || '--',
    },
    {
      title: '图片',
      render: (d, x) => {
        if (x.Picture == null) {
          return null;
        }

        return (
          <>
            <PreviewGroup data={[x.Picture]} />
          </>
        );
      },
    },
    {
      title: '首页展示',
      render: (d, x) => {
        return <Badge status={x.ShowOnPublicPage ? 'success' : 'default'} />;
      },
    },
    {
      title: '状态',
      render: (d, x) => {
        return (
          <>
            {x.Published && <Badge status={'success'} text={'可用'} />}
            {x.Published || <Badge status={'error'} text={'不可用'} />}
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(x);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, [query]);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <XSearchForm
        query={query}
        onSearch={(q: any) => {
          _query(q);
        }}
      />
      <Card
        title="品牌"
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
