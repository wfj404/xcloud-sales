import { useRequest } from 'ahooks';
import { sumBy } from 'lodash-es';
import { useEffect } from 'react';

import { PaymentChannelList } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { OrderDto } from '@/utils/swagger';
import { formatMoney, isArrayEmpty, isStringEmpty } from '@/utils/utils';
import { Box, TableHead, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';

export default function ({ model }: { model: OrderDto }) {
  const query_bill_request = useRequest(apiClient.mall.orderListOrderBill, {
    manual: true,
  });
  const bills = query_bill_request.data?.data?.Data || [];

  const queryBill = () => {
    if (isStringEmpty(model.Id)) {
      return;
    }
    query_bill_request.run({ Id: model.Id });
  };

  useEffect(() => {
    queryBill();
  }, [model]);

  if (query_bill_request.loading) {
    return <span>loading...</span>;
  }

  if (isArrayEmpty(bills)) {
    return <></>;
  }

  return (
    <>
      <Box sx={{ p: 2, my: 1 }}>
        <Typography variant="h6" gutterBottom>
          账单信息
        </Typography>
        <TableContainer component={Paper}>
          <Table sx={{}} size="small">
            <TableHead>
              <TableRow>
                <TableCell>支付渠道</TableCell>
                <TableCell align="right">金额</TableCell>
                <TableCell align="right">支付时间</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {bills.map((x, index) => {
                const channel = PaymentChannelList.find(
                  (d) => d.id == x.PaymentChannel,
                );
                const refunded_price = sumBy(
                  x.RefundBills || [],
                  (x) => x.Price || 0,
                );

                return (
                  <TableRow
                    key={index}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {channel?.name || '未知渠道'}
                    </TableCell>
                    <TableCell align="right">
                      <span>{formatMoney(x.Price || 0)}</span>
                      {refunded_price > 0 && (
                        <div style={{ color: 'red' }}>
                          已退款{formatMoney(refunded_price)}
                        </div>
                      )}
                    </TableCell>
                    <TableCell align="right">
                      {formatRelativeTimeFromNow(x.PayTime)}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </>
  );
}
