//能匹配越多信息的路由越靠前，避免覆盖低优先级的路由
export interface UmiRoute {
  title?: string;
  path?: string;
  component?: string;
  routes?: UmiRoute[];
  redirect?: string;
  wrappers?: string[];
}

const routes: Array<UmiRoute> = [
  //----------------------------------------------qingdao
  {
    path: '/qingdao',
    routes: [
      {
        title: '开票申请受理',
        path: '/qingdao/mcs/invoice-request',
        component: './3h/mcs_invoice_request',
      },
    ],
  },
  {
    path: '/*',
    component: './404',
  },
  {
    title: 'index',
    path: '/',
    component: `@/layouts/app_root`,
    routes: [
      //----------------------------------------------admin part
      {
        title: 'admin',
        path: '/admin',
        component: `@/layouts/admin`,
        routes: [
          {
            path: '/admin',
            //redirect: '/admin/dashboard',
            redirect: '/admin/catalog/goods',
          },
          {
            path: '/admin/ucenter',
            component: './admin/ucenter',
          },
          {
            path: '/admin/dashboard',
            component: '@/pages/admin/dashboard',
            routes: [
              {
                path: '/admin/dashboard',
                redirect: '/admin/dashboard/home',
              },
              {
                title: '统计报表',
                path: '/admin/dashboard/home',
                component: './admin/dashboard/home',
                //wrappers: ['@/pages/admin/dashboard/permission_check'],
              },
              {
                title: '销售报表',
                path: '/admin/dashboard/sales',
                component: './admin/dashboard/sales',
              },
              {
                title: '搜索关键词',
                path: '/admin/dashboard/keywords',
                component: './admin/dashboard/keywords',
              },
              {
                title: '用户活动',
                path: '/admin/dashboard/activity',
                component: './admin/dashboard/activity',
              },
              {
                title: '活跃城市',
                path: '/admin/dashboard/activity-geo',
                component: './admin/dashboard/activity/geo',
              },
            ],
          },
          {
            title: '类目管理',
            path: '/admin/catalog',
            component: '@/pages/admin/catalog',
            routes: [
              {
                path: '/admin/catalog',
                redirect: '/admin/catalog/goods',
              },
              {
                title: '商品管理',
                path: '/admin/catalog/goods',
                component: './admin/catalog/goods',
              },
              {
                path: '/admin/catalog/external_skus',
                component: './admin/external_skus',
              },
              {
                title: '商品规格明细',
                path: '/admin/catalog/sku-batch-update-price',
                component: './admin/catalog/sku/batch_update_price',
              },
              {
                title: '品牌管理',
                path: '/admin/catalog/brand',
                component: './admin/catalog/brand',
              },
              {
                title: '分类管理',
                path: '/admin/catalog/category',
                component: './admin/catalog/category',
              },
              {
                title: '标签管理',
                path: '/admin/catalog/tag',
                component: './admin/catalog/tag',
              },
            ],
          },
          {
            title: '仓库管理',
            path: '/admin/stock',
            component: '@/pages/admin/stock',
            routes: [
              {
                title: '采购单',
                path: '/admin/stock/stock',
                component: './admin/stock/stock/stock',
              },
              {
                title: '商品库存',
                path: '/admin/stock/stock-items',
                component: './admin/stock/stock/stock-item',
              },
              {
                title: '仓库管理',
                path: '/admin/stock/warehouse',
                component: './admin/stock/warehouse',
              },
              {
                title: '供应商管理',
                path: '/admin/stock/supplier',
                component: './admin/stock/supplier',
              },
            ],
          },
          {
            title: '营销方案',
            path: '/admin/marketing',
            component: '@/pages/admin/marketing',
            routes: [
              {
                title: '优惠券',
                path: '/admin/marketing/coupon',
                component: './admin/marketing/coupon',
              },
              {
                title: '折扣',
                path: '/admin/marketing/discount',
                component: './admin/marketing/discount',
              },
              {
                title: '预售充值卡',
                path: '/admin/marketing/gift-card',
                component: './admin/marketing/gift_card',
              },
            ],
          },
          {
            title: '系统设置',
            path: '/admin/settings',
            component: '@/pages/admin/settings',
            routes: [
              {
                path: '/admin/settings',
                redirect: '/admin/settings/store',
              },
              {
                title: '商城会员',
                path: '/admin/settings/store_user',
                component: './admin/settings/store_user',
              },
              {
                title: '门店管理',
                path: '/admin/settings/store',
                component: './admin/settings/store',
              },
              {
                title: '管理员',
                path: '/admin/settings/admin',
                component: './admin/settings/admin',
              },
              {
                title: '环境变量',
                path: '/admin/settings/env',
                component: './admin/settings/env',
              },
              {
                title: '地区',
                path: '/admin/settings/area',
                component: './admin/settings/area',
              },
              {
                title: '活动日志',
                path: '/admin/settings/log',
                component: './admin/settings/activity_log',
              },
              {
                title: '微信设置',
                path: '/admin/settings/wechat',
                component: './admin/settings/wechat',
              },
            ],
          },
        ],
      },
      //---------------------------------------------store management part
      {
        title: 'store-manage',
        path: '/store-manage',
        component: `@/layouts/store-manage`,
        routes: [
          {
            path: '/store-manage',
            redirect: '/store-manage/order',
          },
          {
            path: '/store-manage/cashier',
            component: './store-manage/cashier',
          },
          {
            path: '/store-manage/order-group',
            component: './store-manage/order_group/list',
          },
          {
            path: '/store-manage/order-group-detail',
            component: './store-manage/order_group/detail',
          },
          {
            path: '/store-manage/ucenter',
            component: './store-manage/ucenter',
          },
          {
            path: '/store-manage/order',
            component: './store-manage/order',
            routes: [
              {
                path: '/store-manage/order',
                redirect: '/store-manage/order/list',
              },
              {
                path: '/store-manage/order/list',
                component: './store-manage/order/order_list',
              },
              {
                path: '/store-manage/order/review',
                component: './store-manage/order/order_review',
              },
            ],
          },
          {
            path: '/store-manage/shipping',
            component: '@/pages/store-manage/shipping',
            routes: [
              {
                path: '/store-manage/shipping',
                redirect: '/store-manage/shipping/delivery',
              },
              {
                path: '/store-manage/shipping/delivery',
                component: './store-manage/shipping/delivery',
              },
              {
                path: '/store-manage/shipping/pickup',
                component: './store-manage/shipping/pickup',
              },
            ],
          },
          {
            path: '/store-manage/sku',
            component: '@/pages/store-manage/goods',
            routes: [
              {
                path: '/store-manage/sku',
                component: './store-manage/goods/on_sales',
              },
            ],
          },
          {
            path: '/store-manage/bill',
            component: '@/pages/store-manage/bill',
            routes: [
              {
                path: '/store-manage/bill',
                redirect: '/store-manage/bill/payment',
              },
              {
                path: '/store-manage/bill/payment',
                component: './store-manage/bill/payment',
              },
            ],
          },
          {
            path: '/store-manage/settings',
            component: '@/pages/store-manage/settings',
            routes: [
              {
                path: '/store-manage/settings',
                redirect: '/store-manage/settings/shipping',
              },
              {
                path: '/store-manage/settings/shipping',
                component: './store-manage/settings/shipping',
              },
              {
                path: '/store-manage/settings/manager',
                component: './store-manage/settings/manager',
              },
              {
                path: '/store-manage/settings/role',
                component: './store-manage/settings/role',
              },
              {
                path: '/store-manage/settings/common',
                component: './store-manage/settings/common',
              },
            ],
          },
        ],
      },
      //-------------------------------------------mobile-part
      {
        path: '/',
        component: `@/layouts/mobile`,
        routes: [
          {
            path: '/',
            component: '@/layouts/mobile/tab',
            routes: [
              {
                title: '商城首页',
                path: '/',
                component: './mobile/home',
                wrappers: ['@/layouts/mobile/login_optional'],
              },
              {
                title: 'HOME',
                path: '/home',
                component: './mobile/home',
                wrappers: ['@/layouts/mobile/login_optional'],
              },
              {
                title: '搜索',
                path: '/search',
                component: './mobile/search',
                wrappers: ['@/layouts/mobile/login_optional'],
              },
              {
                title: '个人中心',
                path: '/ucenter',
                component: './mobile/user/ucenter',
                wrappers: ['@/layouts/mobile/login_required'],
              },
              {
                title: '订单',
                path: '/order',
                component: './mobile/order',
                wrappers: ['@/layouts/mobile/login_required'],
              },
              {
                title: '商品类目',
                path: '/category',
                component: './mobile/category',
                wrappers: ['@/layouts/mobile/login_optional'],
              },
              {
                title: '品牌',
                path: '/brand',
                component: './mobile/brand',
              },
              {
                title: '购物车',
                path: '/shoppingcart',
                component: './mobile/cart',
                wrappers: ['@/layouts/mobile/login_required'],
              },
              {
                title: '通知',
                path: '/inbox',
                component: './mobile/inbox',
                wrappers: ['@/layouts/mobile/login_required'],
              },
              {
                title: '商品详情',
                path: '/goods/:id',
                component: './mobile/goods/detail',
                wrappers: ['@/layouts/mobile/login_optional'],
              },
            ],
          },
          {
            path: '/account',
            component: './mobile/account',
            routes: [
              {
                title: '登录',
                path: '/account/login',
                component: './mobile/account/login',
              },
              {
                title: '微信回调',
                path: '/account/wx-callback',
                component: './mobile/account/login/wxcallback',
              },
              {
                title: '注册',
                path: '/account/register',
                component: './mobile/account/register',
              },
            ],
          },
          {
            title: '充值卡',
            path: '/ucenter/gift-card/:id',
            component: './mobile/user/gift-card',
            wrappers: ['@/layouts/mobile/login_required'],
          },
          {
            title: '账户余额',
            path: '/ucenter/balance',
            component: './mobile/user/balance',
            wrappers: ['@/layouts/mobile/login_required'],
          },
          {
            title: '积分',
            path: '/ucenter/points',
            component: './mobile/user/points',
            wrappers: ['@/layouts/mobile/login_required'],
          },
          {
            title: '个人资料',
            path: '/ucenter/profile',
            component: './mobile/user/profile',
            wrappers: ['@/layouts/mobile/login_required'],
          },
          {
            title: '邮寄地址',
            path: '/user/address',
            component: './mobile/user/address',
            wrappers: ['@/layouts/mobile/login_required'],
          },
          {
            title: '我的优惠券',
            path: '/user/coupon',
            component: './mobile/user/coupon',
            wrappers: ['@/layouts/mobile/login_required'],
          },
        ],
      },
    ],
  },
];

export default routes;
