import { useRequest } from 'ahooks';
import { Form, message, Modal, Spin, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StoreManagerDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: StoreManagerDto;
  ok: () => void;
}) => {
  const [model, _model] = useState<StoreManagerDto>({});

  const save_request = useRequest(
    apiClient.mallManager.storeManagerUpdateManager,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          message.success('保存成功');
          ok();
        });
      },
    },
  );

  useEffect(() => {
    _model({
      ...(data || {}),
    });
  }, [data]);

  return (
    <>
      <Modal
        title={'门店成员'}
        open={show}
        onCancel={() => hide()}
        onOk={() => save_request.run(model)}
      >
        <Spin spinning={save_request.loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="是否激活">
              <Switch
                checked={model.IsActive}
                onChange={(e) => {
                  _model({
                    ...model,
                    IsActive: e,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="超级管理员">
              <Switch
                checked={model.IsSuperManager}
                onChange={(e) => {
                  _model({
                    ...model,
                    IsSuperManager: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
