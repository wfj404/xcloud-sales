using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Discounts;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Mapping;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Discounts;

public interface IDiscountMappingService : ISalesAppService
{
    Task<DiscountDto[]> AttachSkusAsync(DiscountDto[] data);

    Task<SkuDto[]> AttachDiscountModelAsync(SkuDto[] data, string storeIdOrEmpty);

    Task SaveSkuMappingAsync(Discount discount, string[] skuIds);
}

public class DiscountMappingService : SalesAppService, IDiscountMappingService
{
    private readonly ISalesRepository<Discount> _repository;
    private readonly IStoreMappingService _storeMappingService;

    public DiscountMappingService(
        IStoreMappingService storeMappingService,
        ISalesRepository<Discount> repository)
    {
        _storeMappingService = storeMappingService;
        _repository = repository;
    }

    public async Task SaveSkuMappingAsync(Discount discount, string[] skuIds)
    {
        var db = await _repository.GetDbContextAsync();

        await db.SaveMappingEntitiesAsync<Discount, Sku, CommonEntityMapping>(discount, skuIds, this.CommonUtils);
    }

    public async Task<DiscountDto[]> AttachSkusAsync(DiscountDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _repository.GetDbContextAsync();

        var ids = data.Ids();

        var query = from mapping in db.Set<CommonEntityMapping>().AsNoTracking()
                .WhereMappingFromTo<CommonEntityMapping, Discount, Sku>(this.CommonUtils)
            join discount in db.Set<Discount>().AsNoTracking()
                on mapping.EntityId equals discount.Id
            join sku in db.Set<Sku>().AsNoTracking()
                on mapping.TargetId equals sku.Id
            select new { discount, sku };

        query = query
            .Where(x => x.discount.LimitedToSku && ids.Contains(x.discount.Id));

        var dataList = await query.OrderByDescending(x => x.discount.CreationTime).ToArrayAsync();

        foreach (var m in data)
        {
            if (!m.LimitedToSku)
                continue;

            var skuList = dataList.Where(x => x.discount.Id == m.Id).Select(x => x.sku).ToArray();
            m.Skus = skuList.MapArrayTo<Sku, SkuDto>(ObjectMapper);
        }

        return data;
    }

    private async Task<IQueryable<Discount>> GetFilteredDiscountQuery(DbContext db, string storeIdOrEmpty)
    {
        await Task.CompletedTask;

        var now = Clock.Now;

        var discountQuery = db.Set<Discount>().AsNoTracking();

        discountQuery = discountQuery.Where(x => x.IsActive);

        discountQuery = discountQuery.FilterByTimeRange(now);

        if (!string.IsNullOrWhiteSpace(storeIdOrEmpty))
        {
            discountQuery = _storeMappingService.ApplyStoreMappingFilter(db, discountQuery, new[] { storeIdOrEmpty });
        }

        return discountQuery;
    }

    public async Task<SkuDto[]> AttachDiscountModelAsync(SkuDto[] data, string storeIdOrEmpty)
    {
        if (!data.Any())
            return data;

        var db = await _repository.GetDbContextAsync();

        var ids = data.Ids();

        var discountQuery = await GetFilteredDiscountQuery(db, storeIdOrEmpty);

        var mappingQuery = db.Set<CommonEntityMapping>().AsNoTracking()
            .WhereMappingFromTo<CommonEntityMapping, Discount, Sku>(this.CommonUtils);

        var joinQuery = from discount in discountQuery
            join mapping in mappingQuery
                on discount.Id equals mapping.EntityId into mappingGrouping
            from mappingOrNull in mappingGrouping.DefaultIfEmpty()
            select new { discount, mappingOrNull };

        joinQuery = joinQuery.Where(x => !x.discount.LimitedToSku || ids.Contains(x.mappingOrNull.TargetId));

        var dataList = await joinQuery.ToArrayAsync();

        foreach (var m in data)
        {
            var discountList = dataList
                .Where(x => !x.discount.LimitedToSku || x.mappingOrNull?.TargetId == m.Id)
                .Select(x => x.discount).ToArray();

            discountList = discountList.DistinctBy(x => x.Id).ToArray();

            m.Discounts = discountList.MapArrayTo<Discount, DiscountDto>(ObjectMapper);
            m.PriceModel.DiscountModel = m.Discounts.SelectDiscount();
        }

        return data;
    }
}