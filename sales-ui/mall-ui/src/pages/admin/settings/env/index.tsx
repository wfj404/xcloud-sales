import { Card, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { QuerySettingsPagingInput, SettingDto } from '@/utils/swagger';

export default () => {
  const [query, _query] = useState<QuerySettingsPagingInput>({
    Page: 1,
  });
  const [data, _data] = useState<PagedResponse<SettingDto>>({});
  const [loading, _loading] = useState(false);

  const queryData = (q: QuerySettingsPagingInput) => {
    _loading(true);

    apiClient.sys
      .manageSettingQueryPaging({
        ...q,
      })
      .then((res) => {
        _data(res.data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryData(query);
  }, []);

  const columns: ColumnType<SettingDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name,
    },
  ];

  return (
    <>
      <Card
        title="环境变量"
        loading={loading}
        style={{
          marginBottom: 10,
        }}
      >
        <Table
          rowKey={(x) => x.Id || ''}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            total: data.TotalCount,
            current: query.Page,
            pageSize: 50,
            onChange: (page) => {
              const q: QuerySettingsPagingInput = {
                ...query,
                Page: page,
              };
              _query(q);
              queryData(q);
            },
          }}
          expandable={{
            expandedRowRender: (x) => (
              <div style={{ padding: 10, border: '5px dashed orange' }}>
                <pre>
                  <code>{x.Value || '--'}</code>
                </pre>
              </div>
            ),
          }}
        ></Table>
      </Card>
    </>
  );
};
