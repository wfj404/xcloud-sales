﻿using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using XCloud.Application.Redis;
using XCloud.Core.Extension;
using XCloud.Sales.Application.Database;

namespace XCloud.Sales.Configuration;

public static class HealthCheckExtension
{
    public static bool HealthCheckEnabled(this IConfiguration configuration)
    {
        return configuration["health_check:enabled"]?.ToBool() ?? false;
    }

    private static string HealthCheckEndpoint => "/internal/health-info";

    public static void AddHealthCheckConfig(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        if (!config.HealthCheckEnabled())
        {
            return;
        }

        var healthCheckBuilder = context.Services.AddHealthChecks();
        healthCheckBuilder.AddCheck<RedisHealthCheck>(nameof(RedisHealthCheck));
        healthCheckBuilder.AddCheck<SalesDatabaseHealthCheck>(nameof(SalesDatabaseHealthCheck));

        context.Services
            .AddHealthChecksUI(setup =>
            {
                var endpoint = new string[] { "http://localhost:6001", HealthCheckEndpoint }.ConcatUrl();
                setup.AddHealthCheckEndpoint("default", endpoint);
                setup.SetEvaluationTimeInSeconds(10);
                setup.MaximumHistoryEntriesPerEndpoint(1000);
            }).AddInMemoryStorage();
    }

    public static void UseHealthCheckEndpointAndUi(this IEndpointRouteBuilder builder)
    {
        var config = builder.ServiceProvider.GetRequiredService<IConfiguration>();

        if (!config.HealthCheckEnabled())
        {
            return;
        }

        builder.MapHealthChecks(HealthCheckEndpoint, new HealthCheckOptions()
        {
            Predicate = _ => true,
            ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
        });
        builder.MapHealthChecksUI(setup =>
        {
            setup.UseRelativeApiPath = false;
            setup.UseRelativeResourcesPath = false;
            setup.UseRelativeWebhookPath = false;
            setup.UIPath = $"/internal/{setup.UIPath?.TrimStart('/')}";
            setup.ApiPath = $"/internal/{setup.ApiPath?.TrimStart('/')}";
            setup.WebhookPath = $"/internal/{setup.WebhookPath?.TrimStart('/')}";
            setup.ResourcesPath = $"/internal/{setup.ResourcesPath?.TrimStart('/')}";
        });
    }
}