﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.ThirdParty.Services.YiLianYun;

namespace XCloud.Sales.ThirdParty.Controllers.StoreManager;

[StoreAuditLog]
[Route("api/mall-manager/yilianyun")]
public class YiLianYunController : ShopBaseController
{
    private readonly IStoreSettingService _storeSettingService;

    public YiLianYunController(IStoreSettingService storeSettingService)
    {
        _storeSettingService = storeSettingService;
    }

    [HttpPost("get-settings")]
    public async Task<ApiResponse<YiLianYunConfigDto>> GetSettingsAsync()
    {
        var storeManager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var settings = await this._storeSettingService.GetSettingsAsync<YiLianYunConfigDto>(storeManager.StoreId);

        return new ApiResponse<YiLianYunConfigDto>(settings);
    }

    [HttpPost("save-settings")]
    public async Task<ApiResponse<object>> SaveSettingsAsync([FromBody] YiLianYunConfigDto dto)
    {
        var storeManager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeSettingService.SaveSettingsAsync(storeManager.StoreId, dto);

        return new ApiResponse<object>();
    }
}