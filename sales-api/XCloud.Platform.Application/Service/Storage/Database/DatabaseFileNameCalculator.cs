﻿using Volo.Abp.BlobStoring;
using Volo.Abp.DependencyInjection;

namespace XCloud.Platform.Application.Service.Storage.Database;

public interface IDatabaseFileNameCalculator
{
    string CalculateDatabaseBlobFileName(BlobProviderArgs args);
}

[ExposeServices(typeof(IDatabaseFileNameCalculator))]
public class DatabaseFileNameCalculator : IDatabaseFileNameCalculator, ITransientDependency
{
    private readonly IFileHashProvider _fileHashProvider;

    public DatabaseFileNameCalculator(IFileHashProvider fileHashProvider)
    {
        _fileHashProvider = fileHashProvider;
    }

    public string CalculateDatabaseBlobFileName(BlobProviderArgs args)
    {
        var arr = new[] { this._fileHashProvider.HashType, args.BlobName }.WhereNotEmpty().ToArray();

        var name = arr.ConcatUrl();

        return name;
    }
}