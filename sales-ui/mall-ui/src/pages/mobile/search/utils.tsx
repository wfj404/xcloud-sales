

import { QueryGoodsPaging } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export const isEmptyGoodsQueryFilter = (x: QueryGoodsPaging): boolean => {
  return (
    isStringEmpty(x.Keywords) &&
    isStringEmpty(x.TagId) &&
    isStringEmpty(x.BrandId) &&
    isStringEmpty(x.CategoryId)
  );
};
