namespace XCloud.Platform.Application.Core;

public static class StorageProviders
{
    public static string OriginUrl => "origin-url";
    public static string AbpFsBlob => "file";
    public static string DatabaseBlob => "database";
}