﻿using DotNetCore.CAP.Internal;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using StackExchange.Redis;
using Volo.Abp.Modularity;
using XCloud.Application.Redis;
using XCloud.Core.Configuration;

namespace XCloud.Application.Queue;

public static class DotnetCoreCapExtension
{
    internal static void AddCapMessageProvider(this ServiceConfigurationContext context)
    {
        var appName = context.Services.GetAppName();

        var client = context.Services.GetSingletonInstance<RedisClient>();

        context.Services.AddCap(option =>
        {
            //transport
            //option.UseRedis(connection: client.ConnectionString);
            option.UseRedis(redisOption =>
            {
                redisOption.Configuration = ConfigurationOptions.Parse(configuration: client.ConnectionString);
                redisOption.Configuration.DefaultDatabase = RedisDatabasePurpose.MessageQueue;
            });

            //store
            option.UseInMemoryStorage();

            //isolate
            option.GroupNamePrefix = appName;
            //option.TopicNamePrefix = appName;

            //option.UseDashboard();
            option.UseDashboard(x => { x.PathMatch = "/internal/cap"; });
        });

        //replace selector
        context.Services.RemoveAll<IConsumerServiceSelector>();
        context.Services.AddSingleton<IConsumerServiceSelector, MyCapConsumerServiceSelector>();
    }

    internal static void UseMessageBusWorker(this IApplicationBuilder builder)
    {
        //builder.UseCapDashboard();
    }
}