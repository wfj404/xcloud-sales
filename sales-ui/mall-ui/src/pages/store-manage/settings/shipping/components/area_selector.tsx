import { Cascader } from 'antd';
import { CSSProperties } from 'react';

import { convertToAntdData } from '@/utils/biz';
import { DeliveryArea } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { useCascaderAreaData } from '@vant/area-data';

export default ({
  data,
  onChange,
  style,
  placeholder,
}: {
  data: DeliveryArea;
  onChange: (e: DeliveryArea) => void;
  style?: CSSProperties;
  placeholder?: string;
}) => {
  if (!data) {
    return null;
  }

  const vantAreaData = useCascaderAreaData();
  const antdArea = vantAreaData.map((x) => convertToAntdData(x));

  const getAreaSelectValue = (m: DeliveryArea): string[] | undefined => {
    let value = [m.ProvinceId || '', m.CityId || '', m.AreaId || ''].filter(
      (mm) => !isStringEmpty(mm),
    );

    if (value.length <= 0) {
      return undefined;
    }

    return value;
  };

  return (
    <>
      <Cascader
        style={{
          minWidth: 200,
          ...style,
        }}
        options={antdArea}
        value={getAreaSelectValue(data)}
        placeholder={placeholder || '请选择区域'}
        changeOnSelect
        allowClear
        onChange={(e) => {
          let area: DeliveryArea = {
            ProvinceId: e?.at(0)?.valueOf()?.toString(),
            CityId: e?.at(1)?.valueOf()?.toString(),
            AreaId: e?.at(2)?.valueOf()?.toString(),
          };
          onChange(area);
        }}
      />
    </>
  );
};
