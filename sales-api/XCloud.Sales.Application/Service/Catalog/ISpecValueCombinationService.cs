﻿using JetBrains.Annotations;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface ISpecValueCombinationService : ISalesAppService
{
    string SerializeToString([NotNull] SpecItemDto[] combination);

    bool TryDeserializeFromString(string json, [CanBeNull] out SpecItemDto[] combination);

    bool AreSpecCombinationEqual([NotNull] SpecItemDto[] combination1, [NotNull] SpecItemDto[] combination2);

    public void CalculateSpecCombinationErrors(
        [NotNull] KeyValuePair<Spec, SpecValue>[] specAndValues,
        [NotNull] SpecItemDto[] combination,
        out string[] errors);

    Task<SpecItemDto[]> AttachSpecAndValuesAsync(SpecItemDto[] data);

    Task<SkuDto[]> AttachSpecCombinationErrorsAsync(SkuDto[] data);

    SkuDto[] AttachSpecItemList(SkuDto[] data);

    Task SetSpecCombinationAsync(string skuId, SpecItemDto[] combination);
}

public class SpecValueCombinationService : SalesAppService, ISpecValueCombinationService
{
    private readonly ISalesRepository<Sku> _repository;
    private readonly ISpecService _specService;
    private readonly ISkuService _skuService;

    public SpecValueCombinationService(ISalesRepository<Sku> skuRepository, ISpecService specService,
        ISkuService skuService)
    {
        _repository = skuRepository;
        _specService = specService;
        _skuService = skuService;
    }

    public string SerializeToString([NotNull] SpecItemDto[] combination)
    {
        if (combination == null)
            throw new ArgumentNullException(nameof(combination));

        var datalist = combination.Select(x => new SpecItemDto(x)).ToArray();

        //remove duplicate items
        datalist = datalist.DistinctBy(x => x.FingerPrint()).ToArray();

        var json = JsonDataSerializer.SerializeToString(datalist);

        return json;
    }

    public bool TryDeserializeFromString(string json, [CanBeNull] out SpecItemDto[] combination)
    {
        combination = default;

        try
        {
            json = json?.Trim();

            if (!string.IsNullOrWhiteSpace(json) && json.StartsWith('[') && json.EndsWith(']'))
            {
                combination = this.JsonDataSerializer.DeserializeFromString<SpecItemDto[]>(json);
                return true;
            }
        }
        catch (Exception e)
        {
            this.Logger.LogWarning(message: e.Message, exception: e);
        }

        return false;
    }

    public bool AreSpecCombinationEqual([NotNull] SpecItemDto[] combination1, [NotNull] SpecItemDto[] combination2)
    {
        if (combination1 == null)
            throw new ArgumentNullException(nameof(combination1));

        if (combination2 == null)
            throw new ArgumentNullException(nameof(combination2));

        if (combination1.Length != combination2.Length)
        {
            return false;
        }

        return combination1.FingerPrint() == combination2.FingerPrint();
    }

    public SkuDto[] AttachSpecItemList(SkuDto[] data)
    {
        if (data == null)
            throw new ArgumentNullException(nameof(data));

        foreach (var m in data)
        {
            this.TryDeserializeFromString(m.SpecCombinationJson, out var items);
            m.SpecItemList = items ?? [];
        }

        return data;
    }

    public async Task<SkuDto[]> AttachSpecCombinationErrorsAsync(SkuDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _repository.GetDbContextAsync();

        var goodsIds = data.Select(x => x.GoodsId).Distinct().ToArray();

        var query = from value in db.Set<SpecValue>().AsNoTracking()
            join spec in db.Set<Spec>().AsNoTracking()
                on value.SpecId equals spec.Id
            select new { value, spec };

        var specAndValues = await query.Where(x => goodsIds.Contains(x.spec.GoodsId)).ToArrayAsync();

        var goodsSpecsGroupedByGoods = data
            .GroupBy(x => new { x.GoodsId })
            .Select(x => new { x.Key.GoodsId, Items = x.ToArray() }).ToArray();

        foreach (var g in goodsSpecsGroupedByGoods)
        {
            var scopedSpecAndValues = specAndValues
                .Where(x => x.spec.GoodsId == g.GoodsId)
                .Select(x => new KeyValuePair<Spec, SpecValue>(x.spec, x.value))
                .ToArray();

            foreach (var sku in g.Items)
            {
                if (sku.SpecItemList == null)
                {
                    this.TryDeserializeFromString(sku.SpecCombinationJson, out var items);
                    sku.SpecItemList = items ?? [];
                }

                CalculateSpecCombinationErrors(scopedSpecAndValues, sku.SpecItemList,
                    out var errors);

                sku.SpecCombinationErrors = errors;
            }
        }

        return data;
    }

    public async Task<SpecItemDto[]> AttachSpecAndValuesAsync(SpecItemDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _repository.GetDbContextAsync();

        var specValueIds = data.Select(x => x.SpecValueId).Distinct().ToArray();

        var query = from value in db.Set<SpecValue>().AsNoTracking()
            join spec in db.Set<Spec>().AsNoTracking()
                on value.SpecId equals spec.Id
            select new { value, spec };

        var specAndValues = await query.Where(x => specValueIds.Contains(x.value.Id)).ToArrayAsync();

        foreach (var m in data)
        {
            var specValuePair = specAndValues.FirstOrDefault(x =>
                x.value.Id == m.SpecValueId && x.spec.Id == m.SpecId);

            if (specValuePair == null)
                continue;

            m.Spec = ObjectMapper.Map<Spec, SpecDto>(specValuePair.spec);
            m.SpecValue = ObjectMapper.Map<SpecValue, SpecValueDto>(specValuePair.value);
        }

        return data;
    }

    public void CalculateSpecCombinationErrors(
        KeyValuePair<Spec, SpecValue>[] specAndValues,
        SpecItemDto[] combination,
        out string[] errors)
    {
        if (specAndValues == null)
            throw new ArgumentNullException(nameof(specAndValues));

        if (combination == null)
            throw new ArgumentNullException(nameof(combination));

        var errorList = new List<string>();

        //所有配置项都是存在的
        var allFingerPrints = specAndValues
            .Select(x => new SpecItemDto(x.Key.Id, x.Value.Id))
            .Select(x => x.FingerPrint())
            .ToArray();

        if (combination.Any())
        {
            var existInAllSkus = combination.All(x => allFingerPrints.Contains(x.FingerPrint()));
            if (!existInAllSkus)
            {
                errorList.Add("invalid parameter exist");
            }
        }

        //每个规格分组有且只有一个配置项
        var grouping = specAndValues
            .GroupBy(x => x.Key.Id)
            .Select(x => new
            {
                SpecId = x.Key,
                Items = x.ToArray()
            })
            .ToArray();

        foreach (var group in grouping)
        {
            var spec = group.Items.First().Key;

            var options = group.Items
                .Select(x => new SpecItemDto(x.Key.Id, x.Value.Id))
                .ToArray();

            var values = combination.InBy(options, x => x.FingerPrint()).ToArray();

            if (!values.Any())
            {
                errorList.Add($"config for {spec.Name} is required");
                continue;
            }

            if (values.Length > 1)
            {
                errorList.Add($"config for {spec.Name} is configured for multiple times");
                continue;
            }
        }

        errors = errorList.Distinct().WhereNotEmpty().ToArray();
    }

    public async Task SetSpecCombinationAsync(string skuId, SpecItemDto[] combination)
    {
        if (string.IsNullOrWhiteSpace(skuId))
            throw new ArgumentNullException(nameof(skuId));

        if (combination == null)
            throw new ArgumentNullException(nameof(combination));

        var entity = await this._repository.GetRequiredByIdAsync(skuId);

        //validate when specs combination is not empty
        if (combination.Any())
        {
            var specAndValues = await _specService.GetSpecAndValueByGoodsIdAsync(entity.GoodsId);

            CalculateSpecCombinationErrors(specAndValues, combination, out var errors);

            if (errors.Any())
                throw new UserFriendlyException(errors.First());

            var skus = await this._skuService.ListByGoodsIdAsync(entity.GoodsId);

            this.AttachSpecItemList(skus);

            var otherSkus = skus.Where(x => x.Id != entity.Id).ToArray();

            if (otherSkus.Any(x => this.AreSpecCombinationEqual(x.SpecItemList, combination)))
            {
                throw new UserFriendlyException("duplicate spec combination");
            }
        }

        entity.SpecCombinationJson = this.SerializeToString(combination);
        entity.LastModificationTime = Clock.Now;

        await this._repository.UpdateAsync(entity);
    }
}