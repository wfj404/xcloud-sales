﻿using XCloud.Application.Model;

namespace XCloud.Platform.Application.Domain.Dept;

public class Department : PlatformBaseEntity, IHasParent, IHasCreationTime
{
    public string Name { get; set; }

    public string Description { get; set; }

    public string DeptType { get; set; }

    public DateTime CreationTime { get; set; }

    public string ParentId { get; set; }
}