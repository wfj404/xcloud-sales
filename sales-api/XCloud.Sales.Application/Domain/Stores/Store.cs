﻿using XCloud.Application.Utils;
using XCloud.Platform.Application.Domain;
using XCloud.Sales.Application.Domain.Mapping;

namespace XCloud.Sales.Application.Domain.Stores;

[TypeIdentityName("store")]
public class Store : SalesBaseEntity<string>, IHasCreationTime, ISoftDelete, IMappingSupported, IHasExternalBinding
{
    public string Name { get; set; }

    public string Description { get; set; }

    public string AddressDetail { get; set; }

    public string Telephone { get; set; }

    public bool Opening { get; set; }

    public bool InvoiceSupported { get; set; }

    public double Lon { get; set; }

    public double Lat { get; set; }

    public bool IsDeleted { get; set; }

    public string ExternalId { get; set; }

    public DateTime CreationTime { get; set; }
}