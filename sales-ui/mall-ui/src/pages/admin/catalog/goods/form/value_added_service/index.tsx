import {
  Button,
  Card,
  Form,
  Input,
  message,
  Modal,
  Spin,
  Switch,
  Table,
} from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { GoodsDto, ValueAddedItemGroupDto } from '@/utils/swagger';

import ValueList from './value';

export default (props: { model: GoodsDto; ok?: () => void }) => {
  const { model } = props;

  const [loading, _loading] = useState(true);
  const [data, _data] = useState<ValueAddedItemGroupDto[]>([]);

  const [formData, _formData] = useState<ValueAddedItemGroupDto | undefined>(
    undefined,
  );
  const f = formData || {};
  const [loadingSave, _loadingSave] = useState(false);

  const queryList = () => {
    if (!model.Id) {
      return;
    }
    _loading(true);
    apiClient.mallAdmin
      .valueAddedQueryGroupWithItemsByGoodsId({ Id: model.Id })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data.Data || []);
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const deleteGroup = (record: ValueAddedItemGroupDto) => {
    if (!confirm('确定删除？')) {
      return;
    }
    _loading(true);
    apiClient.mallAdmin
      .valueAddedDeleteGroup({ Id: record.Id })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          queryList();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList();
  }, [model]);

  const save = (row: ValueAddedItemGroupDto) => {
    _loadingSave(true);

    row.GoodsId = model.Id;

    apiClient.mallAdmin
      .valueAddedSaveGroup({
        ...row,
        GoodsId: model.Id,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          _formData(undefined);
          queryList();
        }
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const columns: ColumnType<ValueAddedItemGroupDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name || '--',
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      title: '明细',
      render: (d, record) => {
        return (
          <ValueList
            model={record}
            ok={() => {
              queryList();
            }}
          />
        );
      },
    },
    {
      title: '必选项',
      render: (d, x) => {
        return <Switch checked={x.IsRequired} />;
      },
    },
    {
      title: '多选项',
      render: (d, x) => {
        return <Switch checked={x.MultipleChoice} />;
      },
    },
    {
      title: '操作',
      render: (text, record) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
            <Button
              type="link"
              danger
              onClick={() => {
                deleteGroup(record);
              }}
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  return (
    <>
      <Modal
        title={'分组'}
        open={formData != undefined}
        onCancel={() => _formData(undefined)}
        onOk={() => save(f)}
      >
        <Spin spinning={loadingSave}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item
              label="名称"
              rules={[
                {
                  required: true,
                },
                {
                  max: 20,
                },
              ]}
            >
              <Input
                value={f.Name || ''}
                onChange={(e) => {
                  _formData({
                    ...formData,
                    Name: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="描述">
              <Input.TextArea
                rows={2}
                value={f.Description || ''}
                onChange={(e) => {
                  _formData({
                    ...formData,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="必选项">
              <Switch
                checked={f.IsRequired}
                onChange={(e) => {
                  _formData({
                    ...formData,
                    IsRequired: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="多选">
              <Switch
                checked={f.MultipleChoice}
                onChange={(e) => {
                  _formData({
                    ...formData,
                    MultipleChoice: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
      <Card
        title="增值服务"
        style={{
          marginBottom: 10,
        }}
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Card>
    </>
  );
};
