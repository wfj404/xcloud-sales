import { apiClient } from '@/utils/client';
import { RefreshViewCacheInput } from '@/utils/swagger';
import { ReloadOutlined } from '@ant-design/icons';
import { Alert, Button, Checkbox, Form, message, Modal, Tooltip } from 'antd';
import { useState } from 'react';
import { handleResponse } from '@/utils/utils';

export default () => {
  const [loading, _loading] = useState(false);
  const [formdata, _formdata] = useState<RefreshViewCacheInput | undefined>(undefined);

  const data = formdata || {};
  const show = formdata != undefined;
  const _show = (b: boolean) => {
    if (b) {
      _formdata({
        Home: true,
        RootCategory: true,
        Dashboard: true,
        Search: true,
      });
    } else {
      _formdata(undefined);
    }
  };

  const triggerRefresh = (e: RefreshViewCacheInput) => {
    _loading(true);
    apiClient.mallAdmin
      .settingRefreshViewCache({ ...e })
      .then((res) => {
        handleResponse(res, () => {
          message.success('缓存刷新成功！');
          _show(false);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  return (
    <>
      <Modal
        open={show}
        title='刷新缓存'
        confirmLoading={loading}
        okText='刷新缓存'
        onOk={() => {
          triggerRefresh(data);
        }}
        onCancel={() => {
          _show(false);
        }}
      >
        <Alert
          style={{ marginBottom: 10 }}
          message='刚更新了数据，但是用户端却没有显示，可以尝试刷新缓存'
          type='warning'
        ></Alert>
        <Form>
          <Form.Item label='首页'>
            <Checkbox checked={data.Home} onChange={e => {
              _formdata({
                ...formdata,
                Home: e.target.checked,
              });
            }}>首页</Checkbox>
          </Form.Item>
          <Form.Item label='分类'>
            <Checkbox checked={data.RootCategory} onChange={e => {
              _formdata({
                ...formdata,
                RootCategory: e.target.checked,
              });
            }}>分类</Checkbox>
          </Form.Item>
          <Form.Item label='报表统计'>
            <Checkbox checked={data.Dashboard} onChange={e => {
              _formdata({
                ...formdata,
                Dashboard: e.target.checked,
              });
            }}>报表统计</Checkbox>
          </Form.Item>
          <Form.Item label='搜索'>
            <Checkbox checked={data.Search} onChange={e => {
              _formdata({
                ...formdata,
                Search: e.target.checked,
              });
            }}>搜索页面</Checkbox>
          </Form.Item>
        </Form>
      </Modal>
      <Tooltip title='刷新缓存'>
        <Button
          size={'small'}
          icon={<ReloadOutlined />}
          type='dashed'
          onClick={() => {
            _show(true);
          }}
        ></Button>
      </Tooltip>
    </>
  );
};
