﻿using Microsoft.Extensions.Logging;

namespace XCloud.Application.Logging;

public static class StaticLoggerFactoryHolder
{
    public static ILoggerFactory LoggerFactory { get; set; }
}