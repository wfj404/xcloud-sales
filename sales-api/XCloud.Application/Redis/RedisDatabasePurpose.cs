﻿namespace XCloud.Application.Redis;

public static class RedisDatabasePurpose
{
    public static int Caching => 1;
    public static int DataProtection => 2;
    public static int Biz => 3;
    public static int DistributedLock => 4;
    public static int PubSub => 5;
    public static int JobScheduler => 6;
    public static int MessageQueue => 9;
}