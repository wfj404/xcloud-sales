using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using XCloud.Application.ServiceDiscovery;
using XCloud.Platform.Application.Domain.Users;

namespace XCloud.Platform.Application.Extension;

public static class PlatformApplicationExtension
{
    public static T HideSensitiveInformation<T>(this T user) where T : User, IEntity
    {
        user.PassWord = default;
        user.LastModificationTime = default;
        user.LastPasswordUpdateTime = default;

        return user;
    }
    
    public static async Task<string> GetRequiredPublicGatewayAddressAsync(
        this IServiceDiscoveryService serviceDiscoveryService)
    {
        var response = await serviceDiscoveryService.ResolveRequiredServiceAsync("PublicGateway");
        return response;
    }

    public static async Task<string> GetRequiredInternalGatewayAddressAsync(
        this IServiceDiscoveryService serviceDiscoveryService)
    {
        var response = await serviceDiscoveryService.ResolveRequiredServiceAsync("InternalGateway");
        return response;
    }
}