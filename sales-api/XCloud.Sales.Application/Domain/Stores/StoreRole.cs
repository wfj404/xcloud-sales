﻿
namespace XCloud.Sales.Application.Domain.Stores;

public class StoreRole : SalesBaseEntity<string>, IHasCreationTime
{
    public string StoreId { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public DateTime CreationTime { get; set; }
}