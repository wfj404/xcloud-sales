﻿using System.Text;

namespace XCloud.Core.Security;

public static class SecurityUtils
{
    public static string BsToStr(byte[] bs)
    {
        var arr = bs.Select(x => x.ToString("x2")).ToList();

        var sb = new StringBuilder();
        arr.ForEach(x => sb.Append(x));

        var data = sb.ToString();
        return data;
    }
}