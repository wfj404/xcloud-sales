﻿using XCloud.Core.Dto;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Dto;
using XCloud.Sales.Application.Extension;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/setting")]
public class SettingController : ShopBaseController
{
    private readonly ISettingService _settingService;

    public SettingController(ISettingService settingService)
    {
        _settingService = settingService;
    }

    [HttpPost("mall-settings")]
    public async Task<ApiResponse<MallSettingsDto>> GetMallSettingsAsync()
    {
        var settings = await _settingService.GetCachedMallSettingsAsync();

        return new ApiResponse<MallSettingsDto>(settings);
    }
}