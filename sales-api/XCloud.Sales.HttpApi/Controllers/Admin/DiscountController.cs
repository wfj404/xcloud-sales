using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Domain.Discounts;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Discounts;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/discount")]
public class DiscountController : ShopBaseController
{
    private readonly IDiscountService _discountService;
    private readonly IDiscountMappingService _discountMappingService;
    private readonly IStoreMappingService _storeMappingService;
    private readonly ISkuService _skuService;

    public DiscountController(IDiscountService discountService,
        IDiscountMappingService discountMappingService,
        IStoreMappingService storeMappingService,
        ISkuService skuService)
    {
        _discountService = discountService;
        _discountMappingService = discountMappingService;
        _storeMappingService = storeMappingService;
        _skuService = skuService;
    }

    [HttpPost("save-discount-store-mapping")]
    public async Task<ApiResponse<object>> SaveStoreMappingAsync([FromBody] SaveStoreMappingInput<Discount> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _storeMappingService.SaveStoreMappingAsync(dto.Entity, dto.StoreIds);

        return new ApiResponse<object>();
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<DiscountDto>> QueryPagingAsync([FromBody] QueryDiscountPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var response = await _discountService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _discountService.AttachStoresAsync(response.Items.ToArray());
            await _discountMappingService.AttachSkusAsync(response.Items.ToArray());

            var skus = response.Items.SelectMany(x => x.Skus).ToArray();
            await _skuService.AttachGoodsAsync(skus);
        }

        return response;
    }

    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] DiscountDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var entity = default(Discount);

        if (!string.IsNullOrWhiteSpace(dto.Id))
        {
            entity = await _discountService.UpdateAsync(dto);
        }
        else
        {
            entity = await _discountService.InsertAsync(dto);
        }

        if (dto.Stores != null)
            await _storeMappingService.SaveStoreMappingAsync(entity, dto.Stores.Ids());

        if (dto.Skus != null)
            await _discountMappingService.SaveSkuMappingAsync(entity, dto.Skus.Ids());

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _discountService.DeleteByIdAsync(dto);

        return new ApiResponse<object>();
    }
}