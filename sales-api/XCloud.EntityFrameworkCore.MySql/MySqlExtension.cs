﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using XCloud.Core;
using XCloud.Core.Exceptions;

namespace XCloud.EntityFrameworkCore.MySql;

public static class MySqlExtension
{
    public static void ApplyUtf8Mb4ForAll(this ModelBuilder builder)
    {
        builder.HasCharSet(CharSet.Utf8Mb4, DelegationModes.ApplyToAll);
    }

    private static void UseMySqlProvider(this DbContextOptionsBuilder option,
        string connectionString,
        bool includeErrors = true)
    {
        if (string.IsNullOrWhiteSpace(connectionString))
            throw new ConfigException($"缺少mysql链接字符串配置");

        var version = ServerVersion.AutoDetect(connectionString: connectionString);

        option.UseMySql(connectionString: connectionString,
            serverVersion: version,
            mySqlOptionsAction: builder =>
            {
                //command timeout
                builder.CommandTimeout((int)TimeSpan.FromSeconds(10).TotalSeconds);
            });

        option.EnableDetailedErrors(detailedErrorsEnabled: includeErrors);
        option.EnableSensitiveDataLogging(sensitiveDataLoggingEnabled: includeErrors);
    }

    public static void UseMySqlProvider(this DbContextOptionsBuilder option,
        IConfiguration configuration,
        string connectionName,
        bool includeErrors = true)
    {
        if (string.IsNullOrWhiteSpace(connectionName))
            throw new ArgumentNullException(nameof(connectionName));

        var connectionString = configuration.GetRequiredConnectionString(connectionName);

        UseMySqlProvider(option, connectionString: connectionString, includeErrors);
    }
}