﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.WeChat.Services.Mp;

namespace XCloud.Platform.WeChat.Job;

public class WechatMpJobs : PlatformAppService
{
    private readonly IWxMpService _wxMpService;
    private readonly ISettingService _settingService;

    public WechatMpJobs(IWxMpService wxMpService, ISettingService settingService)
    {
        _wxMpService = wxMpService;
        _settingService = settingService;
    }

    public async Task RefreshClientAccessTokenAsync()
    {
        try
        {
            var option = await this._settingService.GetWechatMpOptionAsync();

            if (option.Empty)
                return;

            await _wxMpService.GetOrRefreshClientAccessTokenAsync();
            this.Logger.LogInformation("refresh wx-mp client access token");
        }
        catch (Exception e)
        {
            this.Logger.LogError(message: e.Message, exception: e);
        }
    }
}