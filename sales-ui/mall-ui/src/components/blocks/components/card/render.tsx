import { Image, Space } from 'antd-mobile';
import { take } from 'lodash-es';
import { useEffect, useState } from 'react';

import GoodsDetail from '@/components/goods/detail/detailPreview';
import { sortGoodsPictures } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { resolveUrl } from '@/utils/storage';
import { GoodsDto, SkuDto } from '@/utils/swagger';
import {
  formatMoney,
  isArrayEmpty,
  isStringEmpty,
  simpleString,
} from '@/utils/utils';
import { Box, Typography } from '@mui/material';

import XCardPicture from '../../components/block_picture';
import { CardBlockProps } from './utils';

export default ({ data }: { data: CardBlockProps }) => {
  const [goods_list, _goods_list] = useState<GoodsDto[]>([]);
  const [detailId, _detailId] = useState<string | undefined>(undefined);
  const [loading, _loading] = useState(false);

  const queryGoods = (ids: string[]) => {
    if (!ids || isArrayEmpty(ids)) {
      return;
    }

    _loading(true);
    apiClient.mall
      .goodsByIds(ids)
      .then((res) => {
        _goods_list(res.data.Data || []);
      })
      .finally(() => _loading(false));
  };

  useEffect(() => {
    const ids = data?.goods_list?.map((x) => x.Id || '') || [];
    queryGoods(ids.filter((x) => !isStringEmpty(x)));
  }, [data]);

  if (!data) {
    return null;
  }

  const renderSkuList = (skus: SkuDto[]) => {
    return (
      <>
        {skus.map((d, i) => {
          return (
            <div
              key={i}
              style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Typography variant="body1" color="white">
                {d.Name || '--'}
              </Typography>
              <Typography
                variant="caption"
                sx={{
                  color: (t) => t.palette.warning.main,
                }}
              >
                {d.PriceModel?.FinalPrice != undefined
                  ? formatMoney(d.PriceModel?.FinalPrice)
                  : '--'}
              </Typography>
            </div>
          );
        })}
      </>
    );
  };

  const renderGoodsRow = (x: GoodsDto) => {
    const url = resolveUrl(
      sortGoodsPictures(x.GoodsPictures || []).at(0)?.StorageMeta,
      { width: 100, height: 100 },
    );

    if (!url || isStringEmpty(url)) {
      return null;
    }

    const all_skus = x.Skus || [];
    const skus = take(all_skus, 1);
    const remain = all_skus.length - skus.length;

    return (
      <>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'start',
            justifyContent: 'start',
            cursor: 'pointer',
            borderRadius: 1,
            p: 1,
            '&:hover': {
              backgroundColor: 'rgba(0,0,0,0.3)',
            },
          }}
          onClick={() => {
            _detailId(x.Id || undefined);
          }}
        >
          <Image
            alt=""
            src={url}
            style={{ width: 70, height: 70, borderRadius: 8 }}
          />
          <Box
            sx={{
              flexGrow: 1,
              px: 1,
            }}
          >
            <Typography variant="subtitle1" component={'div'} color="white">
              {x.Name}
            </Typography>
            {renderSkuList(skus)}
            {remain > 0 && (
              <Typography
                variant="overline"
                color="text.disabled"
              >{`还有${remain}个款式`}</Typography>
            )}
          </Box>
        </Box>
      </>
    );
  };

  const renderGoodsList = () => {
    if (loading) {
      return null;
    }

    if (isArrayEmpty(goods_list)) {
      return null;
    }

    return (
      <>
        <Box
          sx={{
            p: 2,
            overflow: 'hidden',
            backgroundColor: 'rgba(50,50,50,0.5)',
          }}
        >
          <Space direction="vertical" style={{ width: '100%' }}>
            {goods_list.map((x, i) => {
              return <Box key={i}>{renderGoodsRow(x)}</Box>;
            })}
          </Space>
        </Box>
      </>
    );
  };

  const renderCard = () => {
    return (
      <>
        <div
          style={{
            position: 'relative',
          }}
        >
          <XCardPicture data={data.picture} />
          <div
            style={{
              position: 'absolute',
              left: 0,
              top: 0,
              zIndex: 10,
              padding: '1px 8px',
              backgroundColor: data.color || 'black',
              borderBottomRightRadius: data.borderRadius,
              color: 'white',
            }}
          >
            {data.tag}
          </div>
          <Box
            sx={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              zIndex: 10,
              p: 2,
              //backgroundColor: `rgb(8,8,8)`,
              background: `linear-gradient(0deg, rgba(8,8,8,0.8) 0%, rgba(125,125,125,0.4) 75%, rgba(255,255,255,0) 100%)`,
            }}
          >
            {isStringEmpty(data.subtitle) || (
              <Typography variant="subtitle1" color="white">
                {data.subtitle}
              </Typography>
            )}
            {isStringEmpty(data.title) || (
              <Typography variant="h3" color="white">
                {simpleString(data.title || '', 10)}
              </Typography>
            )}
            {isStringEmpty(data.caption) || (
              <Typography variant="caption" color="text.disabled">
                {data.caption}
              </Typography>
            )}
          </Box>
        </div>
      </>
    );
  };

  return (
    <>
      <GoodsDetail
        detailId={detailId}
        onClose={() => {
          _detailId(undefined);
        }}
      />
      <div
        style={{
          backgroundColor: data.color || undefined,
          borderColor: data.color,
          borderStyle: 'solid',
          borderWidth: data.borderWidth,
          borderRadius: data.borderRadius,
          overflow: 'hidden',
        }}
      >
        {renderCard()}
        {renderGoodsList()}
      </div>
    </>
  );
};
