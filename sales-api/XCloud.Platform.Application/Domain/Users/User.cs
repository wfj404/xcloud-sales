﻿namespace XCloud.Platform.Application.Domain.Users;

public class User : PlatformBaseEntity, IHasIdentityNameFields, IHasCreationTime, IHasDeletionTime,
    IHasModificationTime, IHasExternalBinding
{
    public User()
    {
        //
    }

    public string IdentityName { get; set; }

    public string OriginIdentityName { get; set; }

    public string ExternalId { get; set; }

    public string NickName { get; set; }

    public virtual string PassWord { get; set; }

    public string Avatar { get; set; }

    public DateTime? LastPasswordUpdateTime { get; set; }

    public int Gender { get; set; }

    public bool IsActive { get; set; }

    public bool IsDeleted { get; set; }

    public DateTime? DeletionTime { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }

    public static implicit operator string(User user) => user?.Id;
}