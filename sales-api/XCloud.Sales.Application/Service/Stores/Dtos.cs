﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Stores;

public class StoreRoleDto : StoreRole, IEntityDto<string>
{
    public string[] PermissionKeys { get; set; }
}

public class StoreManagerGrantedPermissionResponse : GrantedPermissionResponse<StoreManagerDto>
{
    public override bool HasAllPermission => this.Data?.IsSuperManager ?? false;
}

public class SaveStoreMappingInput<T> where T : class, IStoreMappingSupported
{
    public T Entity { get; set; }

    public string[] StoreIds { get; set; }
}

public class StoreDto : Store, IEntityDto<string>
{
    public StorageMetaDto Picture { get; set; }

    public double? Distance { get; set; }

    public bool? InServiceArea { get; set; }
}

public class StoreGoodsMappingDto : StoreGoodsMapping, IEntityDto<string>
{
    public StoreDto Store { get; set; }

    public SkuDto Sku { get; set; }
}

public class QueryStoreGoodsMappingPagingInput : PagedRequest
{
    public string StoreId { get; set; }

    public decimal? MinPrice { get; set; }

    public decimal? MaxPrice { get; set; }

    public int? MinStockQuantity { get; set; }

    public int? MaxStockQuantity { get; set; }

    public string Sku { get; set; }

    public string Keywords { get; set; }

    public bool? Active { get; set; }
}

public class QueryStorePagingInput : PagedRequest
{
    //
}

public class CheckSkuForSaleError : IEntityDto<string>
{
    public CheckSkuForSaleError(string id) => this.Id = id;

    public string Id { get; set; }
    public SkuDto Sku { get; set; }
    public string Error { get; set; }
}

public class CheckSkuForSaleInput : IEntityDto<string>
{
    public string Id { get; set; }
    public int Quantity { get; set; }
}

public class SaveGoodsStoreMappingDto : IEntityDto
{
    public string SkuId { get; set; }

    public StoreGoodsMappingDto[] Mappings { get; set; }
}

public class UpdateStoreManagerStatusInput : IEntityDto
{
    public string ManagerId { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsSuperManager { get; set; }
}

public class QueryStoreManagerPaging : PagedRequest
{
    public string StoreId { get; set; }

    public string UserId { get; set; }
}

public static class StoreManagerRoles
{
    public static int StoreManager => 1;
}

public class StoreManagerDto : StoreManager, IEntityDto<string>
{
    public StoreRoleDto[] StoreRoles { get; set; }

    public StoreDto Store { get; set; }

    public UserDto User { get; set; }
}