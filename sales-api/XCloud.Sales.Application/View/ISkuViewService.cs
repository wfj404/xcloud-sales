using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(SkuViewService))]
public class SkuViewService : SalesAppService
{
    private readonly ISkuService _skuService;
    private readonly ISpecService _specService;
    private readonly ISpecValueService _specValueService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly ISpecValueCombinationService _specValueCombinationService;

    public SkuViewService(
        ISkuService skuService,
        ISpecValueCombinationService specValueCombinationService,
        ISpecValueService specValueService,
        ISpecService specService,
        IStoreGoodsMappingService storeGoodsMappingService)
    {
        _skuService = skuService;
        _specValueCombinationService = specValueCombinationService;
        _specValueService = specValueService;
        _specService = specService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _skuService = skuService;
    }

    public async Task<SpecMenuGroupResponse> GetSpecMenuGroupAsync(QuerySkuBySelectedItemsDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.GoodsId))
            throw new ArgumentNullException(nameof(dto.GoodsId));

        if (string.IsNullOrWhiteSpace(dto.StoreId))
            throw new ArgumentNullException(nameof(dto.StoreId));

        dto.Specs ??= [];

        if (dto.Specs.GroupBy(x => x.SpecId).Any(x => x.Count() > 1))
        {
            throw new BusinessException("selection error");
        }

        var skus = await this.GetActiveSkusByGoodsIdAsync(dto.GoodsId);

        await this._storeGoodsMappingService.AttachStoreQuantityAsync(skus, dto.StoreId);

        skus = skus.Where(x => x.StockModel != null).ToArray();

        this._specValueCombinationService.AttachSpecItemList(skus);

        var specList = await this._specService.QuerySpecByGoodsIdAsync(dto.GoodsId);

        await this._specValueService.AttachValuesAsync(specList);

        foreach (var spec in specList)
        {
            foreach (var value in spec.Values)
            {
                var item = new SpecItemDto(spec.Id, value.Id);
                var fingerPrint = item.FingerPrint();

                value.Selected = dto.Specs.Any(x => x.FingerPrint() == fingerPrint);

                if (value.Selected.Value)
                {
                    value.Available = true;
                }
                else
                {
                    var candidateFilter = dto.Specs
                        .Select(x => new SpecItemDto(x))
                        .Where(x => x.SpecId != spec.Id)
                        .Append(new SpecItemDto(spec.Id, value.Id))
                        .ToArray();

                    value.Available = skus.Any(x => IsSkuMatched(x, candidateFilter) && x.StockModel.StockQuantity > 0);
                }
            }
        }

        var response = new SpecMenuGroupResponse()
        {
            Specs = specList,
            FilteredSkuList = skus.Where(x => IsSkuMatched(x, dto.Specs)).ToArray(),
        };

        return response;
    }

    private bool IsSkuMatched(SkuDto sku, SpecItemDto[] selection)
    {
        if (!selection.Any())
            return true;

        if (!sku.SpecItemList.Any())
            return false;

        var skuFingerPrints = sku.SpecItemList.Select(x => x.FingerPrint()).ToArray();

        var containAll = selection.All(x => skuFingerPrints.Contains(x.FingerPrint()));

        return containAll;
    }

    public async Task<SkuDto[]> GetActiveSkusByGoodsIdAsync(string goodsId)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            throw new ArgumentNullException(nameof(goodsId));

        var datalist = await _skuService.ListByGoodsIdAsync(goodsId);

        datalist = datalist.Where(x => x.IsStatusOk()).ToArray();

        return datalist;
    }
}