using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Domain.Invoices;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Invoices;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/user-invoice")]
public class UserInvoiceController : PlatformBaseController
{
    private readonly IUserInvoiceService _userInvoiceService;

    public UserInvoiceController(IUserInvoiceService userInvoiceService)
    {
        _userInvoiceService = userInvoiceService;
    }

    [HttpPost("list")]
    public async Task<ApiResponse<UserInvoice[]>> ListAsync()
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        var datalist = await _userInvoiceService.QueryByUserIdAsync(loginUser.Id);

        return new ApiResponse<UserInvoice[]>(datalist);
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateUserInvoiceStatusInput dto)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        await _userInvoiceService.EnsureUserInvoiceAsync(dto.Id, loginUser.Id);

        await _userInvoiceService.UpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] UserInvoiceDto dto)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            dto.UserId = loginUser.Id;

            await _userInvoiceService.InsertAsync(dto);
        }
        else
        {
            await _userInvoiceService.EnsureUserInvoiceAsync(dto.Id, loginUser.Id);

            dto.UserId = loginUser.Id;
            await _userInvoiceService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }
}