﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.Shipping.Delivery.Express;
using XCloud.Sales.Application.Service.Shipping.Delivery.ShortDistance;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/delivery-settings")]
public class DeliverySettingsController : ShopBaseController
{
    private readonly IStoreSettingService _storeSettingService;

    public DeliverySettingsController(IStoreSettingService storeSettingService)
    {
        _storeSettingService = storeSettingService;
    }


    [HttpPost("get-short-distance-delivery-settings")]
    public async Task<ApiResponse<StoreShortDistanceDeliverySettings>> GetShortDistanceDeliverySettingsAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var settings = await _storeSettingService.GetSettingsAsync<StoreShortDistanceDeliverySettings>(manager.StoreId);

        return new ApiResponse<StoreShortDistanceDeliverySettings>(settings);
    }

    [HttpPost("save-short-distance-delivery-settings")]
    public async Task<ApiResponse<object>> SaveShortDistanceDeliverySettingsAsync(
        [FromBody] StoreShortDistanceDeliverySettings settings)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        settings.ServiceTime?.TimeRanges.TrySetIds(this.GuidGenerator);

        await _storeSettingService.SaveSettingsAsync(manager.StoreId, settings);

        return new ApiResponse<object>();
    }

    [HttpPost("get-express-settings")]
    public async Task<ApiResponse<StoreExpressSettings>> GetExpressSettingsAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var settings = await _storeSettingService.GetSettingsAsync<StoreExpressSettings>(manager.StoreId);

        return new ApiResponse<StoreExpressSettings>(settings);
    }

    [HttpPost("save-express-settings")]
    public async Task<ApiResponse<object>> SaveExpressSettingsAsync([FromBody] StoreExpressSettings settings)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _storeSettingService.SaveSettingsAsync(manager.StoreId, settings);

        return new ApiResponse<object>();
    }
}