import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Row,
  Space,
  Spin,
  Switch,
} from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { apiClient } from '@/utils/client';
import { dateTimeFormat, parseAsDayjs, timezoneOffset } from '@/utils/dayjs';
import { CouponDto, OrderValidatorRule } from '@/utils/swagger';
import { handleResponse, isStringEmpty, parseJsonOrEmpty } from '@/utils/utils';

import XRules from './validators';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: CouponDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);

  const [model, _model] = useState<CouponDto>({});

  const empty = isStringEmpty(model.Id);

  const save = (row: CouponDto) => {
    _loading(true);

    apiClient.mallAdmin
      .couponSaveCoupon({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model(data || {});
  }, [data]);

  return (
    <>
      <Modal
        title={'编辑优惠券'}
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                let res = await apiClient.mallAdmin.couponDelete({
                  Id: data.Id,
                });
                handleResponse(res, () => {
                  hide();
                  ok();
                });
              }}
              hide={isStringEmpty(data.Id)}
            />
            <Button
              type={'primary'}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
        width={'90%'}
      >
        <Spin spinning={loading}>
          <Form
            disabled={!empty}
            onFinish={() => save(model)}
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Row gutter={10}>
              <Col span={12}>
                <Form.Item label="名称" tooltip="例如满100减10">
                  <Input
                    maxLength={30}
                    required
                    value={model.Name || ''}
                    onChange={(e) => {
                      _model({
                        ...model,
                        Name: e.target.value,
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="描述" name="Description">
                  <Input.TextArea
                    maxLength={100}
                    value={model.Description || ''}
                    onChange={(e) => {
                      _model({
                        ...model,
                        Description: e.target.value,
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="金额" tooltip="优惠券金额">
                  <InputNumber
                    required
                    min={0}
                    value={model.Price || 0}
                    onChange={(e) => {
                      _model({
                        ...model,
                        Price: e || 0,
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="数量" tooltip="发行数量">
                  <InputNumber
                    min={0}
                    required
                    value={model.Amount}
                    onChange={(e) => {
                      _model({
                        ...model,
                        Amount: e || 0,
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="时间范围">
                  <DatePicker.RangePicker
                    allowClear
                    value={[
                      parseAsDayjs(model.StartTime)?.add(
                        timezoneOffset,
                        'hour',
                      ) || null,
                      parseAsDayjs(model.EndTime)?.add(
                        timezoneOffset,
                        'hour',
                      ) || null,
                    ]}
                    onChange={(e) => {
                      _model({
                        ...model,
                        StartTime: e
                          ?.at(0)
                          ?.add(-timezoneOffset, 'hours')
                          .format(dateTimeFormat),
                        EndTime: e
                          ?.at(1)
                          ?.add(-timezoneOffset, 'hours')
                          .format(dateTimeFormat),
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="领取次数" tooltip="每个账号可以领取多少次">
                  <InputNumber
                    value={model.AccountIssuedLimitCount || 0}
                    onChange={(e) => {
                      _model({
                        ...model,
                        AccountIssuedLimitCount: e || 0,
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="有效期" tooltip="领取后多少天有效">
                  <InputNumber
                    value={model.ExpiredInDays || 0}
                    onChange={(e) => {
                      _model({
                        ...model,
                        ExpiredInDays: e || 0,
                      });
                    }}
                  />
                </Form.Item>
                <Form.Item label="状态可用">
                  <Switch
                    checked={model.IsActive}
                    onChange={(e) => {
                      _model({
                        ...model,
                        IsActive: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="使用条件" name={'OrderConditionRulesJson'}>
                  <XRules
                    disabled={!empty}
                    conditionList={
                      (parseJsonOrEmpty(model.OrderConditionRulesJson) ||
                        []) as OrderValidatorRule[]
                    }
                    ok={(e) => {
                      console.log('rules-changed', e);

                      _model({
                        ...model,
                        OrderConditionRulesJson: JSON.stringify(e || []),
                      });
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
