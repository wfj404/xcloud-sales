﻿using System.Threading;
using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Payments;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Payments;

public interface IOrderPaymentBillService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<PaymentBillDto> GetRequiredByIdAsync(string billId);

    Task UpdateOrderPaidAmountAsync(string orderId);

    Task<PaymentBillDto> GetBillReadyForPaymentAsync(string billId);

    Task<PaymentBillDto> GetPaidBillByIdAsync(string billId);

    Task<PagedResponse<PaymentBillDto>> QueryPagingAsync(QueryOrderBillPagingInput dto);

    Task<PaymentBillDto> CreateOrderPayBillAsync(CreateOrderPayBillInput dto);

    [NotNull]
    [ItemNotNull]
    Task<PaymentBillDto> MarkAsPaidAsync(MarkBillAsPayedInput dto);

    Task<PaymentBillDto[]> GetPaidBillsAsync(string orderId);

    Task<PaymentBillDto[]> AttachOrderAsync(PaymentBillDto[] datalist);

    Task DeleteExpiredBillsAsync(CancellationToken cancellationToken);

    Task<OrderDto[]> AttachPaidPaymentBillsAsync(OrderDto[] data);

    Task<decimal> GetOrderRestMoneyToPayAsync(string orderId);
}

public class OrderPaymentBillService : SalesAppService, IOrderPaymentBillService
{
    private readonly ISalesRepository<PaymentBill> _billRepository;
    private readonly IOrderService _orderService;

    private int ExpiredInMinutes => 30;

    public OrderPaymentBillService(ISalesRepository<PaymentBill> billRepository,
        IOrderService orderService)
    {
        _billRepository = billRepository;
        _orderService = orderService;
    }

    public async Task<PaymentBillDto> GetRequiredByIdAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
            throw new ArgumentNullException(nameof(billId));

        var bill = await this._billRepository.FirstOrDefaultAsync(x => x.Id == billId);

        if (bill == null)
            throw new EntityNotFoundException("bill not found");

        return bill.MapTo<PaymentBill, PaymentBillDto>(this.ObjectMapper);
    }

    public async Task<PaymentBillDto> GetBillReadyForPaymentAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
            throw new ArgumentNullException(nameof(billId));

        var bill = await this._billRepository.FirstOrDefaultAsync(x => x.Id == billId);

        if (bill == null)
        {
            return null;
        }

        if (bill.Paid)
        {
            this.Logger.LogWarning(message: "bill is already paid");
            return null;
        }

        if (bill.CreationTime < this.Clock.Now.AddMinutes(-this.ExpiredInMinutes))
        {
            this.Logger.LogWarning(message: "bill is expired");
            return null;
        }

        return bill.MapTo<PaymentBill, PaymentBillDto>(this.ObjectMapper);
    }

    public async Task<PaymentBillDto> GetPaidBillByIdAsync(string billId)
    {
        if (string.IsNullOrWhiteSpace(billId))
            throw new ArgumentNullException(nameof(billId));

        var bill = await this._billRepository.FirstOrDefaultAsync(x => x.Id == billId);

        if (bill == null)
        {
            return null;
        }

        if (!bill.Paid)
        {
            this.Logger.LogWarning(message: "bill is unpaid");
            return null;
        }

        return bill.MapTo<PaymentBill, PaymentBillDto>(this.ObjectMapper);
    }

    public async Task<PaymentBillDto[]> AttachOrderAsync(PaymentBillDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _billRepository.GetDbContextAsync();

        var orderIds = data
            .Where(x => !string.IsNullOrWhiteSpace(x.OrderId))
            .Select(x => x.OrderId)
            .Distinct().ToArray();

        if (orderIds.Any())
        {
            var orders = await db.Set<Order>().AsNoTracking().WhereIdIn(orderIds).ToArrayAsync();
            foreach (var m in data)
            {
                var order = orders.FirstOrDefault(x => x.Id == m.OrderId);
                if (order == null)
                    continue;
                m.Order = ObjectMapper.Map<Order, OrderDto>(order);
            }
        }

        return data;
    }

    public async Task<PagedResponse<PaymentBillDto>> QueryPagingAsync(QueryOrderBillPagingInput dto)
    {
        var db = await _billRepository.GetDbContextAsync();

        var billQuery = db.Set<PaymentBill>().AsNoTracking();

        billQuery = billQuery.WhereCreationTimeBetween(dto.StartTime, dto.EndTime);

        if (!string.IsNullOrWhiteSpace(dto.OutTradeNo))
            billQuery = billQuery.Where(x => x.Id == dto.OutTradeNo);

        if (!string.IsNullOrWhiteSpace(dto.PaymentTransactionId))
            billQuery = billQuery.Where(x => x.PaymentTransactionId == dto.PaymentTransactionId);

        if (dto.Paid ?? false)
            billQuery = billQuery.Where(x => x.Paid);

        if (!string.IsNullOrWhiteSpace(dto.PaymentMethod))
            billQuery = billQuery.Where(x => x.PaymentChannel == dto.PaymentMethod);

        var refundBillQuery = db.Set<RefundBill>().AsNoTracking();
        var originRefundBillQuery = refundBillQuery;

        if (dto.Refunded ?? false)
            refundBillQuery = refundBillQuery.Where(x => x.Refunded);

        if (!string.IsNullOrWhiteSpace(dto.OutRefundNo))
            refundBillQuery = refundBillQuery.Where(x => x.Id == dto.OutRefundNo);

        if (!string.IsNullOrWhiteSpace(dto.RefundId))
            refundBillQuery = refundBillQuery.Where(x => x.RefundTransactionId == dto.RefundId);

        if (!object.ReferenceEquals(refundBillQuery, originRefundBillQuery))
        {
            var billIds = refundBillQuery.Select(x => x.PaymentBillId);
            billQuery = billQuery.Where(x => billIds.Contains(x.Id));
        }

        var orderQuery = db.Set<Order>().AsNoTracking().IgnoreQueryFilters();
        var originOrderQuery = orderQuery;

        if (!string.IsNullOrWhiteSpace(dto.OrderId))
            orderQuery = orderQuery.Where(x => x.Id == dto.OrderId);

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            orderQuery = orderQuery.Where(x => x.StoreId == dto.StoreId);

        if (!string.IsNullOrWhiteSpace(dto.OrderNo))
            orderQuery = orderQuery.Where(x => x.OrderSn == dto.OrderNo);

        if (!object.ReferenceEquals(orderQuery, originOrderQuery))
        {
            var orderIds = orderQuery.Select(x => x.Id);
            billQuery = billQuery.Where(x => orderIds.Contains(x.OrderId));
        }

        var count = await billQuery.CountOrDefaultAsync(dto);

        var list = await billQuery
            .OrderByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = list.MapArrayTo<PaymentBill, PaymentBillDto>(this.ObjectMapper);

        return new PagedResponse<PaymentBillDto>(items, dto, count);
    }

    public async Task<PaymentBillDto> MarkAsPaidAsync(MarkBillAsPayedInput dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(MarkAsPaidAsync));

        var db = await _billRepository.GetDbContextAsync();

        var entity = await _billRepository.GetRequiredByIdAsync(dto.Id);

        if (!entity.Paid)
        {
            entity.Paid = true;
            entity.PaymentChannel = dto.PaymentMethod;
            entity.PayTime = dto.Time ?? Clock.Now;
            entity.PaymentTransactionId = dto.TransactionId;
            entity.NotifyData = dto.NotifyData;

            await db.TrySaveChangesAsync();

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

            await this.UpdateOrderPaidAmountAsync(entity.OrderId);
        }

        return entity.MapTo<PaymentBill, PaymentBillDto>(this.ObjectMapper);
    }

    public async Task<PaymentBillDto> CreateOrderPayBillAsync(CreateOrderPayBillInput dto)
    {
        var order = await _orderService.GetByIdAsync(dto.OrderId);
        if (order == null)
            throw new EntityNotFoundException();

        var restPrice = await GetOrderRestMoneyToPayAsync(order.Id);

        if (restPrice <= decimal.Zero)
        {
            throw new UserFriendlyException("there is no need to pay more money!");
        }

        dto.PriceOrNull ??= restPrice;

        if (dto.PriceOrNull.Value > restPrice)
        {
            throw new UserFriendlyException("bill price is bigger than the rest price");
        }

        var entity = new PaymentBill
        {
            Description = dto.Description ?? string.Empty,
            OrderId = order.Id,
            Price = dto.PriceOrNull.Value,
            Id = GuidGenerator.CreateGuidString(),
            CreationTime = Clock.Now,
            Paid = false,
            PaymentChannel = SalesConstants.PaymentChannel.None,
            PayTime = null,
            PaymentTransactionId = string.Empty,
            NotifyData = string.Empty
        };

        await _billRepository.InsertAsync(entity);

        return entity.MapTo<PaymentBill, PaymentBillDto>(ObjectMapper);
    }

    public async Task<decimal> GetOrderRestMoneyToPayAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var order = await _orderService.GetByIdAsync(orderId);

        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        var bills = await GetPaidBillsAsync(orderId);

        var offset = order.TotalPrice - bills.Sum(x => x.Price);

        return offset;
    }

    public async Task<OrderDto[]> AttachPaidPaymentBillsAsync(OrderDto[] data)
    {
        if (!data.Any())
        {
            return data;
        }

        var ids = data.Ids();

        var db = await _billRepository.GetDbContextAsync();
        var query = db.Set<PaymentBill>().AsNoTracking()
            .Where(x => ids.Contains(x.OrderId))
            .Where(x => x.Paid);

        var datalist = await query.OrderByDescending(x => x.CreationTime).ToArrayAsync();

        foreach (var m in data)
        {
            m.PaymentBills = datalist
                .Where(x => x.OrderId == m.Id)
                .MapArrayTo<PaymentBill, PaymentBillDto>(this.ObjectMapper);
        }

        return data;
    }

    public async Task<PaymentBillDto[]> GetPaidBillsAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(GetPaidBillsAsync));

        var db = await _billRepository.GetDbContextAsync();

        var query = db.Set<PaymentBill>().AsNoTracking()
            .Where(x => x.OrderId == orderId)
            .Where(x => x.Paid);

        var data = await query.OrderBy(x => x.CreationTime).ToArrayAsync();

        var bills = data.Select(x => ObjectMapper.Map<PaymentBill, PaymentBillDto>(x)).ToArray();

        return bills;
    }

    public async Task UpdateOrderPaidAmountAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var db = await this._billRepository.GetDbContextAsync();

        var order = await db.Set<Order>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == orderId);

        if (order == null)
            return;

        var bills = await this.GetPaidBillsAsync(orderId: order.Id);
        order.PaidTotalPrice = bills.Sum(x => x.Price);
        order.LastModificationTime = this.Clock.Now;

        await db.TrySaveChangesAsync();
    }

    public async Task DeleteExpiredBillsAsync(CancellationToken cancellationToken)
    {
        while (!cancellationToken.IsCancellationRequested)
        {
            using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

            try
            {
                var expired = this.Clock.Now
                    .AddMinutes(-this.ExpiredInMinutes)
                    .AddDays(-300);

                var db = await _billRepository.GetDbContextAsync();

                var query = db.Set<PaymentBill>().AsTracking()
                    .Where(x => !x.Paid)
                    .Where(x => x.CreationTime < expired);

                var datalist = await query.OrderBy(x => x.CreationTime).Take(500).ToArrayAsync();

                if (!datalist.Any())
                {
                    break;
                }

                await _billRepository.DeleteManyAsync(datalist);

                await uow.SaveChangesAsync();

                await uow.CompleteAsync();
            }
            catch (Exception e)
            {
                this.Logger.LogError(message: e.Message, exception: e);
                await uow.RollbackAsync();
                break;
            }
        }
    }
}