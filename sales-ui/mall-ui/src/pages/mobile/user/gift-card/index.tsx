import { useTitle } from 'ahooks';
import React, { useEffect, useState } from 'react';
import { history, useLocation, useParams, useSnapshot } from 'umi';

import DotsLoading from '@/components/common/loading/dots';
import XUserAvatar from '@/components/manage/user/avatar';
import XHeader from '@/components/mobile/simpleHeader';
import { storeState, StoreType } from '@/store';
import { parseQueryString } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  dateTimeFormat,
  myDayjs,
  parseAsDayjs,
  timezoneOffset,
} from '@/utils/dayjs';
import { GiftCardDto } from '@/utils/swagger';
import {
  formatMoney,
  GlobalContainer,
  handleResponse,
  isStringEmpty,
} from '@/utils/utils';
import { CurrencyExchangeOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Container,
  Divider,
  Stack,
  Typography,
} from '@mui/material';

interface GiftCardParamType extends Record<string, string> {
  id: string;
}

export default function () {
  useTitle('兑换充值卡');

  const app = useSnapshot(storeState) as StoreType;

  const [loading, _loading] = useState(false);
  const [loadingUse, _loadingUse] = React.useState(false);
  const [data, _data] = React.useState<GiftCardDto>({});

  const cardUseable = !(data.Expired || data.Used);

  const location = useLocation();
  const queryParam = parseQueryString(location.search) || {};

  const params = useParams<GiftCardParamType>();

  const id = (queryParam['id'] as string) || params.id || '';

  const getGiftCard = (id: string) => {
    if (isStringEmpty(id)) {
      return;
    }

    _loading(true);
    apiClient.mall
      .giftCardQueryById({ Id: id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const useCard = () => {
    if (isStringEmpty(id)) {
      return;
    }

    _loadingUse(true);
    apiClient.mall
      .giftCardUseCard({ CardId: id })
      .then((res) => {
        handleResponse(res, () => {
          GlobalContainer.message?.success('充值成功');
          getGiftCard(id);
        });
      })
      .finally(() => {
        _loadingUse(false);
      });
  };

  useEffect(() => {
    getGiftCard(id);
  }, []);

  const expired_date = parseAsDayjs(data.EndTime)?.add(timezoneOffset, 'hour');
  const expired_time = expired_date
    ? expired_date.format(dateTimeFormat)
    : '永久有效';
  const expired = expired_date && expired_date.utc().isBefore(myDayjs().utc());

  const is_inactive = !data.Used && !data.IsActive;

  const renderExchangeButton = () => {
    if (is_inactive) {
      return null;
    }
    return (
      <LoadingButton
        disabled={!cardUseable}
        variant="contained"
        color="error"
        loading={loadingUse}
        fullWidth
        onClick={() => {
          if (confirm('确定使用此卡？')) {
            useCard();
          }
        }}
      >
        {cardUseable ? '使用此卡充值' : '此卡已经失效，或已被使用'}
      </LoadingButton>
    );
  };

  const renderInactiveMessage = () => {
    if (is_inactive) {
      return (
        <Typography
          variant="overline"
          color="error"
          gutterBottom
          component={'div'}
        >
          卡片被冻结，无法兑换。请联系管理员！
        </Typography>
      );
    }
    return null;
  };

  return (
    <>
      <XHeader>
        <Container maxWidth="sm">
          {loading && <DotsLoading />}
          <Card sx={{ minWidth: 275, margin: 1 }}>
            <CardHeader
              title="预充值卡"
              subheader="此卡是非记名充值卡，请勿泄露给其他人！否则将造成您的财产损失。"
              avatar={<CurrencyExchangeOutlined />}
            ></CardHeader>
            <CardContent>
              <Stack
                direction={'column'}
                spacing={1}
                sx={{ width: '100%', my: 1 }}
                divider={<Divider orientation="horizontal" />}
              >
                <Typography gutterBottom variant="h6" component="div">
                  卡面金额：
                  <Chip
                    variant="filled"
                    color="warning"
                    label={formatMoney(data.Amount || 0)}
                    size="small"
                  ></Chip>
                </Typography>
                <Typography variant="body2" gutterBottom component={'div'}>
                  <span>{`过期时间：${expired_time}`}</span>
                  {expired && (
                    <Chip
                      variant="filled"
                      color="error"
                      label={'已过期'}
                      size="small"
                      sx={{ ml: 1 }}
                    ></Chip>
                  )}
                </Typography>
                <Typography variant="body2" gutterBottom component={'div'}>
                  <span>充值账户：</span>
                  <XUserAvatar model={app.storeUser?.User} />
                </Typography>
              </Stack>
            </CardContent>
            <CardActions>
              <Stack direction={'column'} spacing={1} sx={{ width: '100%' }}>
                {renderExchangeButton()}
                {renderInactiveMessage()}
                <LoadingButton
                  size="small"
                  fullWidth
                  onClick={() => {
                    history.push({
                      pathname: '/',
                    });
                  }}
                >
                  返回首页
                </LoadingButton>
              </Stack>
            </CardActions>
          </Card>
        </Container>
      </XHeader>
    </>
  );
}
