import XAftersaleStatus from '@/components/status/aftersale';
import XStatus from '@/components/status/order';
import { OrderStatus } from '@/utils/biz';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { OrderDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Alert, Box, Card, CardContent, Divider, Typography } from '@mui/material';

import FooterItem from './components/footer_item';
import XOrderItemList from './components/orderItemList';

export default function (props: { model: OrderDto }) {
  const { model } = props;

  const tryRenderAfterSaleTips = () => {
    if (model.OrderStatusId != OrderStatus.AfterSales) {
      return null;
    }
    if (model.AfterSales == null) {
      return <Alert severity="warning">售后数据未关联</Alert>;
    }
    return (
      <>
        <Box
          sx={{
            mb: 1,
            p: 1,
            borderRadius: 1,
            border: '1px dashed gray',
            borderColor: (theme) => theme.palette.primary.main,
            backgroundColor: 'rgb(250,250,250)',
          }}
        >
          <Typography variant="overline" color="text.disabled">
            售后状态：
          </Typography>
          <XAftersaleStatus model={model.AfterSales} />
        </Box>
      </>
    );
  };

  return (
    <Card
      sx={{
        '&:hover': {
          border: (theme) => `1px solid ${theme.palette.primary.main}`,
        },
      }}
    >
      <CardContent sx={{ p: 2 }}>
        <Box sx={{ mb: 1, display: 'flex', justifyContent: 'space-between' }}>
          <Box>
            <Typography variant="h6" sx={{ display: 'inline' }}>
              {model.Store?.Name || '--'}
            </Typography>
            <Typography variant="overline" sx={{ display: 'inline', ml: 1 }}>
              #{model.OrderSn}
            </Typography>
          </Box>
          <Box sx={{}}>
            <XStatus model={model} />
          </Box>
        </Box>
        {tryRenderAfterSaleTips()}
        {isArrayEmpty(model.Items) || (
          <Box sx={{}}>
            <XOrderItemList model={model} />
          </Box>
        )}
        <Divider />
        <FooterItem
          left={formatRelativeTimeFromNow(model.CreationTime) || '--'}
          right={`总计：${model.TotalPrice}元`}
          primary
        />
      </CardContent>
    </Card>
  );
}
