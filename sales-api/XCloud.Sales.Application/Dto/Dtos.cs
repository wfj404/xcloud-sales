using Volo.Abp.Application.Dtos;
using XCloud.Application.Gis;
using XCloud.Application.Model;
using XCloud.Platform.Messenger.Abstraction.Core;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Orders.Validator;
using XCloud.Sales.Application.Service.Orders.Validator.Validators;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Delivery.Express;

namespace XCloud.Sales.Application.Dto;

/// <summary>
/// generate swagger dto scheme
/// </summary>
public class RefDto : IEntityDto
{
    public OrderValidatorRule OrderCondition { get; set; }
    public OrderConditionCheckDto OrderConditionCheckDto { get; set; }
    public OrderConditionValidationResponse OrderConditionValidationResponse { get; set; }
    public OrderPriceConditionParameter OrderPriceConditionParameter { get; set; }
    public GoodsBrandValidatorParameter GoodsBrandValidatorParameter { get; set; }
    public GoodsCategoriesValidatorParameter GoodsCategoriesValidatorParameter { get; set; }
    public OrderTypeValidatorParameter OrderTypeValidatorParameter { get; set; }
    public OrderStoreConditionParameter OrderStoreConditionParameter { get; set; }

    public DeliveryRecordTrackDto DeliveryRecordTrackDto { get; set; }
    public FreightTemplateDto FreightTemplateDto { get; set; }
    public DeliveryStatusEvent DeliveryStatusEvent { get; set; }
    public DeliveryArea DeliveryArea { get; set; }
    public DeliveryRule DeliveryRule { get; set; }
    public GeoLocationDto GeoLocationDto { get; set; }

    public GoodsPictureDto GoodsPicture { get; set; }

    public AntDesignTreeNode AntDesignTreeNode { get; set; }

    public MessageDto Message { get; set; }
    public SendToUserMessageBody SendToUserMessageBody { get; set; }
    public BroadCastMessageBody BroadCastMessageBody { get; set; }
    public PingMessageBody PingMessageBody { get; set; }
}