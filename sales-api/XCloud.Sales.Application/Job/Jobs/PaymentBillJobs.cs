﻿using System.Threading;
using XCloud.Application.DistributedLock;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Payments;

namespace XCloud.Sales.Application.Job.Jobs;

[UnitOfWork]
public class PaymentBillJobs : SalesAppService
{
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;

    public PaymentBillJobs(IOrderPaymentBillService orderPaymentBillService,
        IOrderRefundBillService orderRefundBillService)
    {
        _orderPaymentBillService = orderPaymentBillService;
        _orderRefundBillService = orderRefundBillService;
    }

    public async Task DeleteUnpaidExpiredBillsAsync()
    {
        await using var _ = await
            this.DistributedLockProvider.CreateRequiredLockAsync(
                resource: $"{nameof(PaymentBillJobs)}.{nameof(DeleteUnpaidExpiredBillsAsync)}",
                expiryTime: TimeSpan.FromSeconds(20));

        using var cancellationTokenSource = new CancellationTokenSource();

        cancellationTokenSource.CancelAfter(TimeSpan.FromMinutes(10));

        await this._orderPaymentBillService.DeleteExpiredBillsAsync(cancellationTokenSource.Token);
        await this._orderRefundBillService.DeleteExpiredBillsAsync(cancellationTokenSource.Token);
    }
}