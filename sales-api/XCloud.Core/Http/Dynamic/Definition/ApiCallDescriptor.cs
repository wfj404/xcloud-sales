﻿using System.Threading;
// ReSharper disable All

namespace XCloud.Core.Http.Dynamic.Definition;

public class ApiCallDescriptor
{
    /// <summary>
    /// 方法定义
    /// </summary>
    public ActionDefinition ActionDefinition { get; }

    /// <summary>
    /// 请求参数
    /// </summary>
    public IReadOnlyDictionary<ParameterDefinition, object> Args { get; }

    public CancellationToken CancellationToken { get; }

    public ApiCallDescriptor(ActionDefinition actionDefinition, IDictionary<ParameterDefinition, object> args,
        CancellationToken cancellationToken)
    {
        if (actionDefinition == null)
            throw new ArgumentNullException(nameof(actionDefinition));

        if (args == null)
            throw new ArgumentNullException(nameof(args));

        ActionDefinition = actionDefinition;
        Args = EnumerableExtension.ToDictionaryUpdateDuplicateKey(args, x => x.Key, x => x.Value);
        CancellationToken = cancellationToken;
    }
}