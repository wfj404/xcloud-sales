import { Col, Form, Input, Row } from 'antd';

import { OrderDto } from '@/utils/swagger';

export default ({
  order,
  onChange,
}: {
  order: OrderDto;
  onChange: (e: OrderDto) => void;
}) => {
  if (!order || !onChange) {
    return <h1>error</h1>;
  }

  return (
    <>
      <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
        <Row gutter={[10, 10]}>
          <Col span={24}>
            <Form.Item label="订单备注">
              <Input.TextArea
                rows={2}
                placeholder="请输入订单备注"
                value={order.Remark || ''}
                onChange={(e) => {
                  onChange({ ...order, Remark: e.target.value });
                }}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
};
