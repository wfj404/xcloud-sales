import { Alert, Card, Divider, Rate, Space } from 'antd';
import React, { useEffect } from 'react';

import PreviewGroup from '@/components/common/preview_group';
import { getSkuName } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto, OrderReviewDto, OrderReviewItemDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default (props: { model: OrderDto }) => {
  const { model } = props;

  const [data, _data] = React.useState<OrderReviewDto>({});
  const [loading, _loading] = React.useState<boolean>(false);

  const queryList = () => {
    if (isStringEmpty(model.Id)) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .orderReviewByOrderId({ Id: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList();
  }, [model]);

  const renderItem = (item: OrderReviewItemDto) => {
    const sku = model.Items?.find(x => x.Id == item.OrderItemId)?.Sku;

    return <>
      <div style={{
        padding: 10,
      }}>
        <div>{getSkuName(sku || {}) || 'unknown sku name'}</div>
        <div style={{ padding: 10 }}>{item.ReviewText || '--'}</div>
        <Rate value={item.Rating || 0} allowHalf disabled />
        <PreviewGroup data={item.StorageMeta || []} count={10} />
      </div>
    </>;
  };

  if (isStringEmpty(data?.Id)) {
    return <Alert message={'暂无评价'} />;
  }

  return (
    <>
      <Card title='订单评价' style={{ marginBottom: 10 }} loading={loading}>
        <Space direction={'vertical'}>
          <div style={{
            padding: 10,
            borderRadius: 5,
            border: '1px solid gray',
          }}>
            {data.ReviewText || '--'}
          </div>
          <Rate value={data.Rating || 0} allowHalf disabled />
          <PreviewGroup data={data.StorageMeta || []} count={10} />
          {data.Items?.map((x, i) => <div style={{}} key={i}>
            <Divider type={'horizontal'} />
            {renderItem(x)}
          </div>)}
        </Space>
      </Card>
    </>
  );
};
