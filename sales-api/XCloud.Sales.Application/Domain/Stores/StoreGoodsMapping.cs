﻿namespace XCloud.Sales.Application.Domain.Stores;

public class StoreGoodsMapping : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime
{
    public string StoreId { get; set; }

    public string SkuId { get; set; }

    public int StockQuantity { get; set; }
    
    public decimal Price { get; set; }

    public bool OverridePrice { get; set; }

    public bool Active { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}