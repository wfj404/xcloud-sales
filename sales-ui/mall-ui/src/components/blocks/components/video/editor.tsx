import { Button, Form, Input, Space, Switch } from 'antd';

import XPictureUploader from '@/components/upload/single_picture_form';
import { resolveUrl } from '@/utils/storage';

import { VideoBlockProps } from './utils';

export default function ({
  data,
  onChange,
}: {
  data: VideoBlockProps;
  onChange: (d: VideoBlockProps) => void;
}) {
  if (!data || !onChange) {
    return null;
  }

  const poster_url = resolveUrl(data.poster);

  return (
    <>
      <Form labelCol={{ span: 3 }}>
        <Form.Item label={'视频地址'}>
          <Input
            value={data?.video_url}
            onChange={(e) => {
              onChange && onChange({ ...data, video_url: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label={'视频封面'}>
          <Space direction={'horizontal'}>
            <XPictureUploader
              data={data.poster || {}}
              ok={(e) => {
                onChange({
                  ...data,
                  poster: e,
                });
              }}
              remove={() => {
                onChange({
                  ...data,
                  poster: undefined,
                });
              }}
            />
          </Space>
        </Form.Item>
        <Form.Item label={'上传'}>
          <h2>todo</h2>
          <Button type={'link'}>{resolveUrl(data.storage) || '--'}</Button>
        </Form.Item>
        <Form.Item label="自动">
          <Space direction="horizontal">
            <Switch
              checked={data.auto_play}
              onChange={(e) => {
                onChange({
                  ...data,
                  auto_play: e,
                });
              }}
            />
          </Space>
        </Form.Item>
        <Form.Item label="循环播放">
          <Switch
            checked={data.loop}
            onChange={(e) => {
              onChange({
                ...data,
                loop: e,
              });
            }}
          />
        </Form.Item>
        <Form.Item label="显示进度控制器">
          <Switch
            checked={data.controls}
            onChange={(e) => {
              onChange({
                ...data,
                controls: e,
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
}
