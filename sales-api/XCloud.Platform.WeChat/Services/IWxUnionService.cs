using System.Threading.Tasks;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.WeChat.Domain;

namespace XCloud.Platform.WeChat.Services;

public interface IWxUnionService : IXCloudAppService
{
    Task SaveOpenIdUnionIdMappingAsync(string platform, string appId, string openId, string unionIdOrEmpty);
}

public class WxUnionService : XCloudAppService, IWxUnionService
{
    private readonly IPlatformRepository<WxUnion> _repository;

    public WxUnionService(IPlatformRepository<WxUnion> repository)
    {
        _repository = repository;
    }

    public async Task SaveOpenIdUnionIdMappingAsync(string platform, string appId, string openId, string unionIdOrEmpty)
    {
        await _repository.DeleteAsync(x => x.Platform == platform && x.AppId == appId && x.OpenId == openId);

        if (!string.IsNullOrWhiteSpace(unionIdOrEmpty))
        {
            var entity = new WxUnion
            {
                Platform = platform,
                AppId = appId,
                OpenId = openId,
                UnionId = unionIdOrEmpty,
                Id = GuidGenerator.CreateGuidString(),
            };
            await _repository.InsertAsync(entity);
        }
    }
}