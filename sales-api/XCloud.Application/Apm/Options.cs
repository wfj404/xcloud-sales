﻿using Volo.Abp.Application.Dtos;

namespace XCloud.Application.Apm;

public class ApmOption : IEntityDto
{
    public bool Enabled { get; set; } = false;

    public bool LogServiceEnabled { get; set; } = false;
}