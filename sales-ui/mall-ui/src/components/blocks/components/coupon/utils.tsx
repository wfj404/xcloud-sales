import { BlockItem, BlockItemProps } from '../../utils';

export interface CouponBlockProps extends BlockItemProps {
  hide_if_empty?: boolean;
  title?: string;
  description?: string;
  count?: number;
}

export const coupon_demo: BlockItem<CouponBlockProps> = {
  type: 'coupon',
  style: {
    marginBottom: 10,
  },
  body: {
    hide_if_empty: true,
    title: '领券中心',
    description: undefined,
    count: 5,
  },
};
