import React, { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { QueryGoodsPaging, SearchOptionsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Chip, Stack } from '@mui/material';

export default function ({
  query,
  onSearch,
}: {
  query: QueryGoodsPaging;
  onSearch: (d: QueryGoodsPaging) => void;
}) {
  const [options, _options] = useState<SearchOptionsDto>({});
  const [loading, _loading] = useState<boolean>(false);

  const querySearchOptions = (param: QueryGoodsPaging) => {
    if (param.TagId || param.BrandId || param.CategoryId) {
    } else {
      console.log('没有搜索条件');
      return;
    }
    _loading(true);
    apiClient.mall
      .searchSearchOptions(param)
      .then((res) => {
        _options(res.data.Data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    console.log('query', query);
    querySearchOptions(query);
  }, [query]);

  const renderSelectedBrand = () => {
    if (isStringEmpty(query.BrandId)) {
      return null;
    }

    return (
      <Chip
        size="small"
        label={options.Brand?.Name || `品牌`}
        onDelete={() => {
          onSearch &&
            onSearch({
              ...query,
              BrandId: undefined,
            });
        }}
      />
    );
  };

  const renderSelectedCategory = () => {
    if (isStringEmpty(query.CategoryId)) {
      return null;
    }

    return (
      <Chip
        size="small"
        label={options.Category?.Name || `类目`}
        onDelete={() => {
          onSearch &&
            onSearch({
              ...query,
              CategoryId: undefined,
            });
        }}
      />
    );
  };

  const renderSelectedTag = () => {
    if (isStringEmpty(query.TagId)) {
      return null;
    }

    return (
      <Chip
        size="small"
        label={options.Tag?.Name || `标签`}
        onDelete={() => {
          onSearch &&
            onSearch({
              ...query,
              TagId: undefined,
            });
        }}
      />
    );
  };

  if (loading) {
    return null;
  }

  if (options.Tag || options.Brand || options.Category) {
  } else {
    return null;
  }

  return (
    <>
      <Stack spacing={1} direction="row">
        {renderSelectedBrand()}
        {renderSelectedCategory()}
        {renderSelectedTag()}
      </Stack>
    </>
  );
}
