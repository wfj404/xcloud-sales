import { useRequest } from 'ahooks';
import { Button, Card, Divider, message, Space } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { WechatSettings } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { ReloadOutlined } from '@ant-design/icons';

import XMpForm from './mp';
import XOpenForm from './open';
import XPaymentForm from './payment';

export default () => {
  const [form, _form] = useState<WechatSettings>({});

  const get_settings_request = useRequest(
    apiClient.sys.wechatSettingsGetWechatSettings,
    {
      manual: true,
      onSuccess(data, params) {
        _form(data?.data?.Data || {});
      },
    },
  );

  const save_settings_request = useRequest(
    apiClient.sys.wechatSettingsSaveWechatSettings,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          message.success('保存成功');
          get_settings_request.run({});
        });
      },
    },
  );

  useEffect(() => {
    get_settings_request.run({});
  }, []);

  return (
    <>
      <Card
        title="微信设置"
        loading={get_settings_request.loading}
        extra={
          <Space direction="horizontal">
            <Button
              type="dashed"
              icon={<ReloadOutlined />}
              onClick={() => {
                get_settings_request.run({});
              }}
            ></Button>
            <Button
              type="primary"
              loading={save_settings_request.loading}
              onClick={() => {
                save_settings_request.run(form);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Space direction="vertical" style={{ width: '100%' }}>
          <div>
            <Divider orientation="left">公众号</Divider>
            <XMpForm
              data={form.MpOption || {}}
              onChange={(e) => {
                _form({ ...form, MpOption: e });
              }}
            />
          </div>
          <div>
            <Divider orientation="left">小程序</Divider>
            <XOpenForm
              data={form.OpenOption || {}}
              onChange={(e) => {
                _form({ ...form, OpenOption: e });
              }}
            />
          </div>
          <div>
            <Divider orientation="left">支付设置</Divider>
            <XPaymentForm
              data={form.PaymentOption || {}}
              onChange={(e) => {
                _form({ ...form, PaymentOption: e });
              }}
            />
          </div>
        </Space>
      </Card>
    </>
  );
};
