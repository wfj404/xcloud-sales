using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace XCloud.Application.Localization;

public static class LocalizationExtension
{
    public static void ConfigApplicationSharedLocalization(this ServiceConfigurationContext context)
    {
        context.Services.Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<XCloudApplicationModule>(
                baseNamespace: typeof(XCloudApplicationModule).Namespace);
        });

        context.Services.Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<ApplicationSharedResource>(defaultCultureName: "zh-Hans")
                .AddVirtualJson("/Localization/Resources/Shared")
                .AddBaseTypes(typeof(AbpValidationResource));
        });
    }
}