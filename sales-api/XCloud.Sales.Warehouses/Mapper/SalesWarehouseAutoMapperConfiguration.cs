﻿using AutoMapper;
using XCloud.Sales.Warehouses.Domain.Warehouses;
using XCloud.Sales.Warehouses.Service.Warehouses;

namespace XCloud.Sales.Warehouses.Mapper;

public class SalesWarehouseAutoMapperConfiguration : Profile
{
    public SalesWarehouseAutoMapperConfiguration()
    {
        //warehouse
        CreateMap<Warehouse, WarehouseDto>().ReverseMap();
        CreateMap<Stock, StockDto>().ReverseMap();
        CreateMap<StockItem, StockItemDto>().ReverseMap();
        CreateMap<Supplier, SupplierDto>().ReverseMap();
    }
}