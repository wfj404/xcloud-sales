import { AppListProps } from '@ant-design/pro-components';
import admin from '@/assets/app/admin.svg';
import worker from '@/assets/app/worker.svg';
import user from '@/assets/app/user.svg';

const data: AppListProps = [
  {
    title: '管理端',
    desc: '终极管理员使用',
    url: '/sales/admin/',
    target: '_blank',
    icon: admin,
  },
  {
    title: '门店端',
    desc: '门店管理员使用',
    url: '/sales/store-manage/',
    target: '_blank',
    icon: worker,
  },
  {
    title: '用户端',
    desc: '普通用户使用',
    url: '/sales/',
    target: '_blank',
    icon: user,
  },
];

export default data;
