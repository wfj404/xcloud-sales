using Volo.Abp.Application.Dtos;
using XCloud.Sales.Application.Domain.Grades;

namespace XCloud.Sales.Application.Service.Grades;

public class GradeDto : Grade, IEntityDto<string>
{
    //
}

public class StoreUserGradeMappingDto : StoreUserGradeMapping, IEntityDto
{
    //
}

public class UpdateGradeStatusInput : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? IsDeleted { get; set; }
}

public class GoodsGradePriceDto : GoodsGradePrice, IEntityDto
{
    public Grade Grade { get; set; }
}