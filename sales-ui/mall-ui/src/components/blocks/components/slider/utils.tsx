import { BlockItem, BlockItemProps, BlockPictureProps } from '../../utils';

export interface SliderBlockProps extends BlockItemProps {
  sliders?: BlockPictureProps[];
  auto_play?: boolean;
  auto_play_interval?: number;
  loop?: boolean;
  vertical?: boolean;
  hide_indicator?: boolean;
}

export const slider_demo: BlockItem<SliderBlockProps> = {
  type: 'slider',
  hide: true,
  style: {
    marginBottom: 10,
  },
  body: {
    auto_play: false,
    vertical: false,
    sliders: [
      {
        storage:
          'https://lf1-cdn-tos.bytescm.com/obj/static/ies/bytedance_official/_next/static/images/cod-cb0d883e8d2c1c45bafb93eb7cd64468.png',
        link: {},
      },
      {
        storage:
          'https://lf1-cdn-tos.bytescm.com/obj/static/ies/bytedance_official/_next/static/images/2-468bb0555d827d48bce4e178f085bf90.png',
        link: {},
      },
    ],
  },
};
