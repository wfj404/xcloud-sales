﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Volo.Abp.Modularity;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.Extension;

public static class MvcExtension
{
    public static void AddMvcPart<T>(this ServiceConfigurationContext context) where T : AbpModule
    {
        context.Services.Configure<IMvcBuilder>(builder =>
        {
            var currentAssembly = typeof(T).Assembly;
            builder.PartManager.ApplicationParts.AddIfNotContains(currentAssembly);
            builder.PartManager.ApplicationParts.AddRazorAssemblyIfNotContains(
                new CompiledRazorAssemblyPart(currentAssembly));
        });
    }

    public static void AddRazorAssemblyIfNotContains(this IList<ApplicationPart> applicationParts,
        CompiledRazorAssemblyPart compiledRazorAssemblyPart)
    {
        if (compiledRazorAssemblyPart == null)
            throw new ArgumentNullException(nameof(compiledRazorAssemblyPart));

        if (applicationParts.Any(x =>
                x is CompiledRazorAssemblyPart part &&
                part.Assembly == compiledRazorAssemblyPart.Assembly))
            return;

        applicationParts.Add(compiledRazorAssemblyPart);
    }

    public static string GetCurrentIpAddress(this HttpContext context)
    {
        var result = string.Empty;
        
        //The X-Forwarded-For (XFF) HTTP header field is a de facto standard
        //for identifying the originating IP address of a client
        //connecting to a web server through an HTTP proxy or load balancer.

        //it's used for identifying the originating IP address of a client connecting to a web server
        //through an HTTP proxy or load balancer. 
        var xff = (string)context.Request.Headers.Keys
            .Where(x => "X-FORWARDED-FOR".Equals(x, StringComparison.InvariantCultureIgnoreCase))
            .Select(k => context.Request.Headers[k])
            .FirstOrDefault();

        //if you want to exclude private IP addresses, then see http://stackoverflow.com/questions/2577496/how-can-i-get-the-clients-ip-address-in-asp-net-mvc
        if (!string.IsNullOrWhiteSpace(xff))
        {
            var lastIp = xff.Split(new[] { ',' }).FirstOrDefault();
            result = lastIp;
        }

        //some validation
        if (result == "::1") result = "127.0.0.1";

        //remove port
        if (!string.IsNullOrWhiteSpace(result))
        {
            var index = result.IndexOf(":", StringComparison.InvariantCultureIgnoreCase);
            if (index > 0) result = result.Substring(0, index);
        }

        //if this header not exists try get connection remote IP address
        if (string.IsNullOrEmpty(result) && context.Connection.RemoteIpAddress != null)
            result = context.Connection.RemoteIpAddress.ToString();

        //some of the validation
        if (result != null && result.Equals(IPAddress.IPv6Loopback.ToString(),
                StringComparison.InvariantCultureIgnoreCase))
            result = IPAddress.Loopback.ToString();

        //"TryParse" doesn't support IPv4 with port number
        if (IPAddress.TryParse(result ?? string.Empty, out var ip))
            //IP address is valid 
            result = ip.ToString();
        else if (!string.IsNullOrEmpty(result))
            //remove port
            result = result.Split(':').FirstOrDefault();

        return result;
    }

    public static Dictionary<string, string> ToDictionary(this IEnumerable<KeyValuePair<string, StringValues>> data)
    {
        var res = data.ToDictionaryUpdateDuplicateKey(
            keySelector: x => x.Key,
            valueSelector: x => (string)x.Value);

        return res;
    }
}