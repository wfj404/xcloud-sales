import { Tabs } from 'antd';
import { useState } from 'react';
import { useParams } from 'umi';

import XDepartment from './department';
import XRole from './role';

export default () => {
  const param = useParams<{ id?: string }>();

  const [selected, _selected] = useState<string | null | undefined>(param.id);

  return (
    <>
      <Tabs
        destroyInactiveTabPane
        activeKey={selected || 'org'}
        onChange={(e) => _selected(e)}
        items={[
          {
            key: 'org',
            label: '组织架构',
            children: <XDepartment />,
          },
          {
            key: 'role',
            label: '角色',
            children: <XRole />,
          },
        ]}
      />
    </>
  );
};
