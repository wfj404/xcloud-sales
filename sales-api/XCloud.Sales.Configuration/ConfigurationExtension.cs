﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Volo.Abp;
using XCloud.AspNetMvc.Configuration;
using XCloud.Core.Extension;
using XCloud.Sales.Application.Core;

namespace XCloud.Sales.Configuration;

public static class ConfigurationExtension
{
    public static bool SkipAutoPipelineConfig(this IConfiguration configuration)
    {
        return configuration["app:config:skip_auto_pipeline_config"]?.ToBool() ?? false;
    }

    public static void TryEnableSwagger(this ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        var alwaysFalse = DateTime.UtcNow.Year < 0;
        if (alwaysFalse)
        {
            app.UseSwaggerDefaultDefinitionJson();
        }
        else
        {
            app.UseSwaggerApiDefinitionJson();
        }

        app.UseSwaggerUiPage(new Dictionary<string, string>
        {
            [SalesConstants.ServiceName] = $"/api/{SalesConstants.ServiceName}/swagger.json",
            [$"{SalesConstants.ServiceName}@GatewayIntegrated"] = $"/swagger/{SalesConstants.ServiceName}/swagger.json"
        });
    }
}