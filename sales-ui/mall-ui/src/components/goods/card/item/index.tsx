import XGoodsLabels from '@/components/goods/goods_labels';
import XPicture from '@/components/goods/picture';
import XSkuList from '@/components/goods/sku/sku_list';
import { GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Typography } from '@mui/material';
import Box from '@mui/material/Box';

import XGoodsPublishAlert from '../../goods_publish_alert';
import Wrapper from '../wrapper';

export default function ({
  model,
  count,
  lazy,
}: {
  model: GoodsDto;
  count?: number;
  lazy?: boolean;
}) {
  if (isStringEmpty(model.Id)) {
    return null;
  }
  if (!model.Published) {
    return <XGoodsPublishAlert model={model} />;
  }
  return (
    <>
      <Wrapper>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}
        >
          <Box
            sx={{
              width: {
                xs: '120px',
                sm: '170px',
                md: '230px',
              },
            }}
          >
            <XPicture model={model} lazy={lazy} />
          </Box>
          <Box
            sx={{
              p: 1,
              width: '100%',
            }}
          >
            <Typography
              variant="subtitle2"
              component={'div'}
              gutterBottom
              noWrap
            >
              {model.Name || '--'}
            </Typography>
            <XSkuList skus={model.Skus || []} count={count} />
            {isStringEmpty(model.AdminComment) || (
              <Typography variant="caption" component={'div'} color="primary">
                {model.AdminComment || '--'}
              </Typography>
            )}
            <XGoodsLabels model={model} />
          </Box>
        </Box>
      </Wrapper>
    </>
  );
}
