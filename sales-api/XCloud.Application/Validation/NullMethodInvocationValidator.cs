﻿using System.Threading.Tasks;
using Volo.Abp.Validation;

namespace XCloud.Application.Validation;

public class NullMethodInvocationValidator : IMethodInvocationValidator
{
    public Task ValidateAsync(MethodInvocationValidationContext context)
    {
        return Task.CompletedTask;
    }
}