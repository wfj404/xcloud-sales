import { useRequest } from 'ahooks';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { dateTimeFormat, myDayjs } from '@/utils/dayjs';
import { QueryTopCategoryListInput, TopCategoryList } from '@/utils/swagger';
import { formatMoney } from '@/utils/utils';

import XIndexNo from '../components/indexNo';

const columns: ColumnProps<TopCategoryList>[] = [
  {
    render: (x, record, index) => <XIndexNo index={index} />,
  },
  {
    title: '分类',
    render: (d, x) => `${x.RootCategoryName}-${x.CategoryName}`,
  },
  {
    title: '金额',
    render: (d, x) => formatMoney(x.TotalPrice || 0),
  },
  {
    title: '数量',
    render: (d, x) => x.TotalQuantity,
  },
];

export default () => {
  const request = useRequest(apiClient.mallAdmin.reportTopCategories, {
    manual: true,
  });

  const data = request.data?.data?.Data || [];

  const [finalQuery, _finalQuery] = useState<QueryTopCategoryListInput>({});

  useEffect(() => {
    request.run({ ...finalQuery });
  }, []);

  useEffect(() => {
    let now = myDayjs.utc();
    let monthAgo = now.add(-1, 'month');
    _finalQuery({
      StartTime: monthAgo.format(dateTimeFormat),
      EndTime: now.format(dateTimeFormat),
    });
  }, []);

  return (
    <>
      <Card
        title="TOP分类"
        extra={<Button type="link">更多</Button>}
        loading={request.loading}
        style={{ marginBottom: 10 }}
      >
        <Table columns={columns} dataSource={data} pagination={false} />
      </Card>
    </>
  );
};
