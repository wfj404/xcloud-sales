﻿using Volo.Abp.Modularity;
using XCloud.Core.Http.Dynamic;

namespace XCloud.Core.Http;

public static class HttpConfigurationExtension
{
    public static void ConfigureHttpClient(this ServiceConfigurationContext context)
    {
        context.Services.AddHttpClient();
        context.Services.AddTransient<IDynamicClientServiceDiscovery, ServiceDiscoveryViaConfiguration>();
    }
}