﻿using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Logging;

namespace XCloud.Sales.Application.Service.Report;

public interface IGoodsReportService : ISalesAppService
{
    Task<QuerySearchKeywordsReportResponse[]> QuerySearchKeywordsReportAsync(
        QuerySearchKeywordsReportInput dto);

    Task<QueryGoodsVisitReportResponse[]> TopVisitedGoodsAsync(QueryGoodsVisitReportInput dto);
}

public class GoodsReportService : SalesAppService, IGoodsReportService
{
    private readonly ISalesRepository<Sku> _salesRepository;

    public GoodsReportService(ISalesRepository<Sku> salesRepository)
    {
        _salesRepository = salesRepository;
    }

    public async Task<QuerySearchKeywordsReportResponse[]> QuerySearchKeywordsReportAsync(
        QuerySearchKeywordsReportInput dto)
    {
        var db = await _salesRepository.GetDbContextAsync();

        var logType = ActivityLogType.SearchGoods;
        var logQuery = db.Set<ActivityLog>().AsNoTracking().Where(x => x.ActivityLogTypeId == logType);

        if (dto.UserId != null)
            logQuery = logQuery.Where(x => x.StoreUserId == dto.UserId.Value);

        if (dto.StartTime != null)
            logQuery = logQuery.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            logQuery = logQuery.Where(x => x.CreationTime <= dto.EndTime.Value);

        logQuery = logQuery.Where(x => x.Value != null && x.Value != string.Empty);

        var groupedQuery = logQuery.GroupBy(x => x.Value).Select(x => new { x.Key, count = x.Count() });

        var data = await groupedQuery.OrderByDescending(x => x.count).Take(dto.MaxCount ?? 200).ToArrayAsync();

        var response = data
            .Select(x => new QuerySearchKeywordsReportResponse
            {
                Keywords = x.Key,
                Count = x.count
            })
            .OrderByDescending(x => x.Count)
            .ToArray();

        return response;
    }

    public async Task<QueryGoodsVisitReportResponse[]> TopVisitedGoodsAsync(QueryGoodsVisitReportInput dto)
    {
        var db = await _salesRepository.GetDbContextAsync();

        var logType = ActivityLogType.VisitGoods;
        var logQuery = db.Set<ActivityLog>().AsNoTracking().Where(x => x.ActivityLogTypeId == logType);

        if (dto.UserId != null)
            logQuery = logQuery.Where(x => x.StoreUserId == dto.UserId.Value);

        if (dto.StartTime != null)
            logQuery = logQuery.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            logQuery = logQuery.Where(x => x.CreationTime <= dto.EndTime.Value);

        var goodsQuery = db.Set<Goods>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.GoodsId))
            goodsQuery = goodsQuery.Where(x => x.Id == dto.GoodsId);

        var query = from log in logQuery
            join goods in goodsQuery.Select(x => new { x.Id })
                on log.SubjectId equals goods.Id
            select new { log, goods };

        var count = dto.Count ?? 10;

        var groupedQuery = query.GroupBy(x => x.goods.Id).Select(x => new { x.Key, count = x.Count() });

        var data = await groupedQuery.OrderByDescending(x => x.count).Take(count).ToArrayAsync();

        var response = data.Select(x => new QueryGoodsVisitReportResponse { GoodsId = x.Key, VisitedCount = x.count })
            .ToArray();

        if (response.Any())
        {
            var ids = response.Select(x => x.GoodsId).Distinct().ToArray();
            var goodsList = await db.Set<Goods>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToArrayAsync();
            foreach (var m in response)
            {
                var g = goodsList.FirstOrDefault(x => x.Id == m.GoodsId);
                if (g == null)
                    continue;
                m.Goods = ObjectMapper.Map<Goods, GoodsDto>(g);
            }
        }

        return response;
    }
}