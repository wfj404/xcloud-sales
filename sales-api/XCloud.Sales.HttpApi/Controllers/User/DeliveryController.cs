﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Delivery.ShortDistance;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/delivery")]
public class DeliveryController : ShopBaseController
{
    private readonly IStoreSettingService _storeSettingService;
    private readonly ServiceTimeService _serviceTimeService;
    private readonly IDeliveryService _deliveryService;
    private readonly IOrderService _orderService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;

    public DeliveryController(IStoreSettingService storeSettingService,
        ServiceTimeService serviceTimeService,
        IDeliveryService deliveryService, IOrderService orderService,
        IOrderAutoProcessService orderAutoProcessService)
    {
        _storeSettingService = storeSettingService;
        _serviceTimeService = serviceTimeService;
        _deliveryService = deliveryService;
        _orderService = orderService;
        _orderAutoProcessService = orderAutoProcessService;
    }

    [HttpPost("get-service-time")]
    public async Task<ApiResponse<AvailableServiceTimeRangeDto[]>> GetServiceTimeAsync()
    {
        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var settings = await _storeSettingService.GetSettingsAsync<StoreShortDistanceDeliverySettings>(storeId);

        var data = _serviceTimeService.ParseServiceTime(settings.ServiceTime, 7, timezoneOffset: 8).ToArray();

        return new ApiResponse<AvailableServiceTimeRangeDto[]>(data);
    }

    [HttpPost("by-order-id")]
    public async Task<ApiResponse<DeliveryRecordDto[]>> GetByOrderIdAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        var datalist = await _deliveryService.QueryByOrderIdAsync(order.Id);

        return new ApiResponse<DeliveryRecordDto[]>(datalist);
    }

    [HttpPost("mark-as-delivered")]
    public async Task<ApiResponse<object>> MarkAsDeliveredAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var shipment = await _deliveryService.EnsureUserDeliveryAsync(dto.Id, loginUser.Id);

        await _deliveryService.MarkAsDeliveredAsync(shipment.Id);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _deliveryService.TryUpdateOrderDeliveryStatusAsync(shipment.OrderId);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _orderAutoProcessService.AutoProcessOrderAsync(shipment.OrderId);

        return new ApiResponse<object>();
    }
}