﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction.MessageHandler.Impl;

[ExposeServices(typeof(IMessageHandler))]
public class BroadcastHandler : IMessageHandler, IScopedDependency
{
    public BroadcastHandler()
    {
        //
    }

    public string MessageType => MessageTypeConst.BroadCast;

    public int Sort => 1;

    public async Task HandleMessageFromTransportAsync(TransportMessageContext context)
    {
        var connections = context.MessengerServer.ConnectionManager.AsReadOnlyList();

        foreach (var connection in connections)
        {
            await connection.SendMessageToClientAsync(context.Message);
        }
    }

    public async Task HandleMessageFromClientAsync(ClientMessageContext context)
    {
        await context.MessengerServer.MessageClusterRouter.BroadCastAsync(context.Message);
    }
}