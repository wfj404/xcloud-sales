import { Col, Form, Input, InputNumber, Row, Space, Switch } from 'antd';

import LocationPicker from '@/components/common/map/location_picker';
import { getStoreLatLng } from '@/utils/biz';
import { StoreDto } from '@/utils/swagger';

export default ({
  model,
  onChange,
}: {
  model: StoreDto;
  onChange: (e: StoreDto) => void;
}) => {
  return (
    <>
      <Form
        labelCol={{ flex: '110px' }}
        labelAlign="right"
        wrapperCol={{ flex: 1 }}
      >
        <Row gutter={[10, 10]}>
          <Col span={8}>
            <Form.Item label="名称">
              <Input
                value={model.Name || ''}
                onChange={(e) => {
                  onChange({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="描述">
              <Input.TextArea
                rows={3}
                value={model.Description || ''}
                onChange={(e) => {
                  onChange({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="联系电话">
              <Input
                value={model.Telephone || ''}
                onChange={(e) => {
                  onChange({
                    ...model,
                    Telephone: e.target.value,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="坐标">
              <Space direction={'vertical'} style={{ width: '100%' }}>
                <InputNumber
                  disabled
                  placeholder="拖动地图获取坐标"
                  prefix={'Lat'}
                  value={model.Lat}
                  onChange={(e) => {
                    onChange({
                      ...model,
                      Lat: e || undefined,
                    });
                  }}
                  style={{
                    width: '100%',
                  }}
                />
                <InputNumber
                  disabled
                  placeholder="拖动地图获取坐标"
                  prefix={'Lon'}
                  value={model.Lon}
                  onChange={(e) => {
                    onChange({
                      ...model,
                      Lon: e || undefined,
                    });
                  }}
                  style={{
                    width: '100%',
                  }}
                />
                <Input.TextArea
                  style={{
                    width: '100%',
                  }}
                  rows={3}
                  placeholder={'门店地址'}
                  value={model.AddressDetail || ''}
                  onChange={(e) => {
                    onChange({
                      ...model,
                      AddressDetail: e.target.value,
                    });
                  }}
                />
              </Space>
            </Form.Item>

            <Form.Item label="可以开发票">
              <Switch
                checked={model.InvoiceSupported}
                onChange={(e) => {
                  onChange({
                    ...model,
                    InvoiceSupported: e,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="营业中">
              <Switch
                checked={model.Opening}
                onChange={(e) => {
                  onChange({
                    ...model,
                    Opening: e,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={16}>
            <div
              style={{
                height: '100%',
                border: '1px solid blue',
              }}
            >
              <LocationPicker
                marker_position={getStoreLatLng(model)}
                marker_title={model.Name || undefined}
                onMarkerPositionChange={(e) => {
                  onChange({
                    ...model,
                    Lat: e.lat,
                    Lon: e.lng,
                  });
                }}
              />
            </div>
          </Col>
        </Row>
      </Form>
    </>
  );
};
