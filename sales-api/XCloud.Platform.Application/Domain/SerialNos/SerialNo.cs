﻿namespace XCloud.Platform.Application.Domain.SerialNos;

public class SerialNo : PlatformBaseEntity, IHasModificationTime, IHasCreationTime
{
    public string Category { get; set; }

    public int NextId { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }
}