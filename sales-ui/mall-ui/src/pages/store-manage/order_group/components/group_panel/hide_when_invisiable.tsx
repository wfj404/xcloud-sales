import { useInViewport, useSize } from 'ahooks';
import { ReactNode, useEffect, useRef, useState } from 'react';

export default ({ children }: { children: ReactNode }) => {
  const ref = useRef(null);
  const [inViewport] = useInViewport(ref);

  const inner_ref = useRef(null);
  const size = useSize(inner_ref);

  const [wrapperHeight, _wrapperHeight] = useState<number | undefined>(
    undefined,
  );

  const [init, _init] = useState(true);

  const show = init || inViewport;

  useEffect(() => {
    setTimeout(() => {
      _init(false);
    }, 1000 * 13);
  }, []);

  useEffect(() => {
    if (show && size?.height) {
      _wrapperHeight(size.height);
    }
  }, [size?.height, show]);

  return (
    <>
      <div
        ref={ref}
        style={{
          height: wrapperHeight || 100,
          overflow: 'hidden',
          contentVisibility: 'auto',
        }}
      >
        {show && <div ref={inner_ref}>{children}</div>}
      </div>
    </>
  );
};
