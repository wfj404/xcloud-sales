import { SpecItemDto, SpecValueDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export const get_spec_combination_description = (
  spec_items: SpecItemDto[],
  spec_values: SpecValueDto[],
): string | null => {
  const desc = spec_items
    .map((x) => spec_values.find((d) => d.Id == x.SpecValueId)?.Name || '')
    .filter((x) => x.length > 0)
    .join(',');

  if (isStringEmpty(desc)) {
    return null;
  }

  return desc;
};
