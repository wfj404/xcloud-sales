﻿using System;
using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;

namespace XCloud.Elasticsearch.Core;

public class ElasticSearchOption : IEntityDto
{
    private string[] _hosts;

    [NotNull]
    public string[] Hosts
    {
        get => this._hosts ??= Array.Empty<string>();
        set => this._hosts = value;
    }
}