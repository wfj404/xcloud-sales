using XCloud.Application.Model;
using XCloud.Application.Utils;

namespace XCloud.Sales.Application.Domain.Catalog;

[TypeIdentityName("category")]
public class Category : SalesBaseEntity<string>, IHasDeletionTime, IHasCreationTime, IHasParent
{
    public string Name { get; set; }

    public string SeoName { get; set; }

    public string Description { get; set; }

    public string RootId { get; set; }

    public string NodePath { get; set; }

    public string ParentId { get; set; } = string.Empty;

    public string PictureMetaId { get; set; }

    public bool ShowOnHomePage { get; set; }

    public bool Published { get; set; }

    public DateTime? DeletionTime { get; set; }

    public bool IsDeleted { get; set; }

    public int DisplayOrder { get; set; }

    public bool Recommend { get; set; }

    public DateTime CreationTime { get; set; }
}