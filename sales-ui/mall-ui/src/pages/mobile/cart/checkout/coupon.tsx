import { useRequest } from 'ahooks';
import { Space } from 'antd-mobile';
import React, { useEffect } from 'react';

import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { CouponUserMappingDto, OrderDto } from '@/utils/swagger';
import {
  formatMoney,
  handleResponse,
  isArrayEmpty,
  isStringEmpty,
} from '@/utils/utils';
import { Typography } from '@mui/material';
import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

const empty_id = 'xxxxxxxxxddddddddddddddddddddd';

export default function (props: {
  value?: number | undefined | null;
  onSelected?: (d: CouponUserMappingDto | null | undefined) => void;
  order?: OrderDto;
}) {
  const { value, onSelected, order } = props;

  const couponListRequest = useRequest(apiClient.mall.couponBestCoupon, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {});
    },
  });

  const couponList = couponListRequest.data?.data?.Data || [];

  useEffect(() => {
    if (order) {
      couponListRequest.run({ ...order });
    } else {
      couponListRequest.mutate(undefined);
    }
  }, [order]);

  const renderCouponSelector = () => {
    if (couponListRequest.loading) {
      //return <DotsLoading />;
    }

    return (
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">优惠券</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          value={value || empty_id}
          label="配送地址"
          placeholder="请选择配送地址"
          multiple={false}
          disabled={isArrayEmpty(couponList) || couponListRequest.loading}
          onChange={(e) => {
            const userCoupon = couponList.find((x) => x.Id == e.target.value);
            onSelected && onSelected(userCoupon);
          }}
        >
          <MenuItem value={empty_id}>
            <Space direction="horizontal">
              <Typography variant="body1" color="text.disabled">
                不使用优惠券
              </Typography>
              {isArrayEmpty(couponList) && (
                <Typography variant="caption" color="text.disabled">
                  暂无可用
                </Typography>
              )}
            </Space>
          </MenuItem>
          {couponList.map((x, index) => (
            <MenuItem key={index} value={x.Id || ''}>
              <Space direction="horizontal">
                <b>{x.Coupon?.Name || '--'}</b>
                <Typography color="error" variant="body1">
                  {formatMoney(x.Coupon?.Price || 0)}
                </Typography>
                {isStringEmpty(x.ExpiredAt) || (
                  <Typography color="text.disabled" variant="caption">
                    {`过期：${formatRelativeTimeFromNow(x.ExpiredAt)}`}
                  </Typography>
                )}
              </Space>
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  };

  return <Box sx={{ py: 2 }}>{renderCouponSelector()}</Box>;
}
