﻿using System.Text;

namespace XCloud.Core.Security.Hash;

/// <summary>
/// Md5Helper encryption.
/// </summary>
public class Md5Helper
{
    public static byte[] EncryptBytes(byte[] bs)
    {
        if (bs == null)
            throw new ArgumentNullException(nameof(bs));

        using var md5 = System.Security.Cryptography.MD5.Create();

        var res = md5.ComputeHash(bs);

        return res;
    }

    public static string Encrypt(byte[] data)
    {
        var bs = EncryptBytes(data);

        var result = SecurityUtils.BsToStr(bs)
            .Replace("-", string.Empty)
            .ToLower();

        return result;
    }

    /// <summary>
    /// Md5Helper encrypt with 32 bits.
    /// </summary>
    public static string Encrypt(string str, Encoding encoding)
    {
        if (str == null)
            throw new ArgumentNullException(nameof(str));

        if (encoding == null)
            throw new ArgumentNullException(nameof(encoding));

        var data = encoding.GetBytes(str);

        var res = Encrypt(data);

        return res;
    }
}