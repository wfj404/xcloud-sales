﻿using System;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nacos.AspNetCore.V2;
using Steeltoe.Extensions.Configuration.Placeholder;
using Volo.Abp.Application.Dtos;
using XCloud.AspNetMvc.Configuration;

namespace XCloud.AspNetMvc.Nacos;

public static class NacosDefaults
{
    public static string DefaultSectionName => "nacos";
}

public class NacosConfiguration : IEntityDto
{
    public NacosConfiguration(IConfigurationSection section)
    {
        if (!section.Exists())
            return;

        this.Enabled = section.GetValue<bool>(nameof(this.Enabled));
        this.Config = section.GetSection(nameof(this.Config));
        this.Service = section.GetSection(nameof(this.Service));
    }

    public bool Enabled { get; set; } = false;

    public IConfigurationSection Config { get; set; }

    public bool ConfigExist => this.Config != null && this.Config.Exists();

    public IConfigurationSection Service { get; set; }

    public bool ServiceExist => this.Service != null && this.Service.Exists();
}

public static class NacosExtension
{
    [NotNull]
    public static NacosConfiguration GetNacosConfiguration(this IConfiguration configuration, string sectionName)
    {
        if (string.IsNullOrWhiteSpace(sectionName))
            throw new ArgumentNullException(nameof(sectionName));

        return new NacosConfiguration(configuration.GetSection(sectionName));
    }

    public static void TryAddNacosConfigProvider(this IConfigurationBuilder builder, string sectionName)
    {
        var config = builder.CopyBuilder().AddPlaceholderResolver().Build()
            .GetNacosConfiguration(sectionName);

        if (!config.Enabled)
            return;

        if (!config.ConfigExist)
            return;

        builder.AddNacosV2Configuration(config.Config);
    }

    public static void TryAddNacosServiceDiscovery(this IServiceCollection services, IConfiguration configuration,
        string sectionName)
    {
        var config = configuration.GetNacosConfiguration(sectionName);

        if (!config.Enabled)
            return;

        if (!config.ServiceExist)
            return;

        services.AddNacosAspNet(configuration, config.Service.Path);
    }
}