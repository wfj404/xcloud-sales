import { UserDto } from '@/utils/swagger';
import { Typography } from '@mui/material';
import XItem from './item';
import { Modal } from 'antd-mobile';
import { useState } from 'react';

export default function(props: { model: UserDto; ok: () => void }) {
  const { model, ok } = props;

  const [show, _show] = useState(false);

  const renderModelContent = () => {
    return <>
      <p>修改手机号</p>
    </>;
  };

  return (
    <>
      <Modal forceRender
             destroyOnClose
             closeOnMaskClick
             title={'修改手机号'}
             visible={show}
             content={renderModelContent()}
             onClose={() => {
               _show(false);
             }}
      ></Modal>
      <div onClick={() => {
        _show(true);
      }}>
        <XItem
          title={'手机号'}
          right={
            <Typography
              variant='overline'
              sx={{
                color: 'text.secondary',
              }}
            >
              {model.UserMobile?.MobilePhone || '--'}
            </Typography>
          }
        />
      </div>
    </>
  );
}
