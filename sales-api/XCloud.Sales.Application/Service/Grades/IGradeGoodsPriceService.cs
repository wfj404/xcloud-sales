using XCloud.Application.Mapper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Grades;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Grades;

public interface IGradeGoodsPriceService : ISalesAppService
{
    Task<SkuDto[]> AttachGradePricesAsync(SkuDto[] data);

    Task<SkuDto[]> AttachGradePriceModelAsync(SkuDto[] data, string gradeId);

    Task<GoodsGradePriceDto[]> QueryBySpecIdAsync(string specId);

    Task<GoodsGradePriceDto[]> AttachGradesAsync(GoodsGradePriceDto[] data);

    Task SaveGradePriceAsync(string combinationId, GoodsGradePriceDto[] gradePrices);
}

public class GradeGoodsPriceService : SalesAppService, IGradeGoodsPriceService
{
    private readonly ISalesRepository<GoodsGradePrice> _gradePriceRepository;

    public GradeGoodsPriceService(ISalesRepository<GoodsGradePrice> gradePriceRepository)
    {
        _gradePriceRepository = gradePriceRepository;
    }

    public async Task<SkuDto[]> AttachGradePriceModelAsync(SkuDto[] data, string gradeId)
    {
        if (string.IsNullOrWhiteSpace(gradeId))
            throw new ArgumentNullException(nameof(gradeId));

        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _gradePriceRepository.GetDbContextAsync();

        var gradePrices = await db.Set<GoodsGradePrice>().AsNoTracking()
            .Where(x => x.GradeId == gradeId)
            .Where(x => ids.Contains(x.SkuId))
            .ToArrayAsync();

        foreach (var m in data)
        {
            var price = gradePrices
                .FirstOrDefault(x => x.SkuId == m.Id && x.GradeId == gradeId);

            if (price == null)
                continue;

            m.PriceModel.GradePriceModel = price.MapTo<GoodsGradePrice, GoodsGradePriceDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<SkuDto[]> AttachGradePricesAsync(SkuDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _gradePriceRepository.GetDbContextAsync();

        var gradePrices = await db.Set<GoodsGradePrice>().AsNoTracking().Where(x => ids.Contains(x.SkuId))
            .ToArrayAsync();

        foreach (var m in data)
        {
            var list = gradePrices.Where(x => x.SkuId == m.Id).ToArray();

            m.GradePrices = list.MapArrayTo<GoodsGradePrice, GoodsGradePriceDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<GoodsGradePriceDto[]> AttachGradesAsync(GoodsGradePriceDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.GradeId).Distinct().ToArray();

        var db = await _gradePriceRepository.GetDbContextAsync();

        var grades = await db.Set<Grade>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            var grade = grades.FirstOrDefault(x => x.Id == m.GradeId);
            if (grade == null)
                continue;

            m.Grade = grade.MapTo<Grade, GradeDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<GoodsGradePriceDto[]> QueryBySpecIdAsync(string specId)
    {
        if (string.IsNullOrWhiteSpace(specId))
            throw new ArgumentNullException(nameof(specId));

        var data = await _gradePriceRepository.QueryManyAsync(x => x.SkuId == specId);

        return data.MapArrayTo<GoodsGradePrice, GoodsGradePriceDto>(ObjectMapper);
    }

    public async Task SaveGradePriceAsync(string combinationId, GoodsGradePriceDto[] gradePrices)
    {
        if (string.IsNullOrWhiteSpace(combinationId) || gradePrices == null)
            throw new ArgumentNullException(nameof(SaveGradePriceAsync));

        var db = await _gradePriceRepository.GetDbContextAsync();
        var combination = await db.Set<Sku>().AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == combinationId);
        if (combination == null)
            throw new EntityNotFoundException(nameof(combination));

        foreach (var m in gradePrices)
        {
            m.SkuId = combination.Id;
            if (string.IsNullOrWhiteSpace(m.GradeId))
                throw new ArgumentNullException(nameof(m.GradeId));
        }

        var set = db.Set<GoodsGradePrice>();

        var allGradePrices = await set.Where(x => x.SkuId == combination.Id).ToArrayAsync();
        var toSaveGradePrices = gradePrices
            .Select(x => ObjectMapper.Map<GoodsGradePriceDto, GoodsGradePrice>(x)).ToArray();

        string FingerPrint(GoodsGradePrice x) => $"{x.SkuId}-{x.GradeId}-{x.PriceOffset}";

        var toAdd = toSaveGradePrices.NotInBy(allGradePrices, FingerPrint).ToArray();
        var toDelete = allGradePrices.NotInBy(toSaveGradePrices, FingerPrint).ToArray();

        if (toAdd.Any())
        {
            foreach (var m in toAdd)
            {
                m.Id = GuidGenerator.CreateGuidString();
                m.CreationTime = Clock.Now;
            }

            set.AddRange(toAdd);
        }

        if (toDelete.Any())
            set.RemoveRange(toDelete);

        await db.TrySaveChangesAsync();
    }
}