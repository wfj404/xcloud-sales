﻿using System.Net.Http.Headers;

namespace XCloud.Core.Http;

public static class HttpExtension
{
    private static string ContentTypeHeaderName => "Content-Type";

    public static string GetContentTypeOrDefault(this HttpResponseHeaders headers)
    {
        headers.TryGetValues(ContentTypeHeaderName, out var res);

        var contentType = res?.FirstOrDefault();

        return contentType;
    }
}