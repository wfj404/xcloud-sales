namespace XCloud.Sales.Application.Domain.Shipping;

public class DeliveryRecord : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime
{
    public string OrderId { get; set; }

    public DateTime? PreferDeliveryStartTime { get; set; }

    public DateTime? PreferDeliveryEndTime { get; set; }

    public string DeliveryType { get; set; }

    public string ExpressName { get; set; }

    public string TrackingNumber { get; set; }

    public bool Delivering { get; set; }

    public DateTime? DeliveringTime { get; set; }

    public bool Delivered { get; set; }

    public DateTime? DeliveredTime { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}