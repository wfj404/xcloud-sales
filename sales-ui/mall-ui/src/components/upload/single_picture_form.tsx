import { Button, ButtonProps, Image, ImageProps, message, Space } from 'antd';
import { useRef, useState } from 'react';

import { config } from '@/utils/config';
import { resolveUrl } from '@/utils/storage';
import { StorageMetaDto } from '@/utils/swagger';
import {
  allowedImageExtensions,
  compressImageV2,
  isImage,
  uploadFileV2,
} from '@/utils/upload_file';
import { isStringEmpty } from '@/utils/utils';

export default ({
  data,
  ok,
  remove,
  loadingSave,
  width,
  defaultImage,
  imageProps,
  uploadButtonProps,
  removeButtonProps,
}: {
  defaultImage?: string;
  width?: number;
  data: StorageMetaDto | string;
  ok: (d: StorageMetaDto) => void;
  remove?: () => void;
  loadingSave?: boolean;
  imageProps?: ImageProps;
  uploadButtonProps?: ButtonProps;
  removeButtonProps?: ButtonProps;
}) => {
  const [loading, _loading] = useState<boolean>(false);
  const pictureUrl = resolveUrl(data, { width: width || 100 });
  const inputRef = useRef<HTMLInputElement>(null);

  const renderPicture = () => {
    const url = pictureUrl || defaultImage;

    if (!isStringEmpty(url)) {
      return (
        <Image src={url || ''} alt="" width={width || 50} {...imageProps} />
      );
    }

    return null;
  };

  const uploadFile = async (file?: File | null) => {
    if (!file) {
      return;
    }

    _loading(true);
    try {
      if (!isImage(file.name)) {
        message.error(
          `只支持 ${allowedImageExtensions().join('、')} 格式的图片`,
        );
        return;
      }

      file = await compressImageV2(file, config.upload.maxSize);
      if (file.size > config.upload.maxSize) {
        message.error('图片大小不能超过1MB');
        return;
      }

      const response = await uploadFileV2(file);
      if (response) {
        ok && ok(response);
      } else {
        message.error('上传失败');
      }
    } catch (e) {
      console.log(e);
      message.error('上传遇到错误');
    } finally {
      _loading(false);
    }
  };

  const renderClearButton = () => {
    if (isStringEmpty(pictureUrl)) {
      return null;
    }
    if (!remove) {
      return null;
    }
    return (
      <Button
        size={'small'}
        type="dashed"
        danger
        onClick={() => {
          if (confirm('删除？')) {
            remove && remove();
          }
        }}
        {...removeButtonProps}
      >
        移除图片
      </Button>
    );
  };

  return (
    <>
      <div style={{ display: 'none' }}>
        <input
          ref={inputRef}
          type={'file'}
          accept="image/*"
          multiple={false}
          onChange={(e) => {
            try {
              if (e.target.files && e.target.files.length > 0) {
                uploadFile(e.target.files.item(0));
              }
            } finally {
              if (inputRef.current) {
                inputRef.current.value = '';
                inputRef.current.files = null;
              }
            }
          }}
        />
      </div>
      <Space direction="horizontal">
        {renderPicture()}

        <Space direction="vertical">
          <Button
            size={'small'}
            loading={loading || loadingSave}
            type="dashed"
            onClick={() => {
              inputRef.current?.click();
            }}
            {...uploadButtonProps}
          >
            选择图片
          </Button>
          {renderClearButton()}
        </Space>
      </Space>
    </>
  );
};
