using Volo.Abp.Uow;
using XCloud.Application.Extension;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/user-auth")]
public class UserAuthController : ShopBaseController
{
    private readonly IGradeService _gradeService;
    private readonly IUserMobileService _userMobileService;
    private readonly IStoreUserService _storeUserService;

    public UserAuthController(
        IGradeService gradeService,
        IUserMobileService userMobileService,
        IStoreUserService storeUserService)
    {
        _gradeService = gradeService;
        _userMobileService = userMobileService;
        _storeUserService = storeUserService;
    }

    [HttpPost("auth")]
    [UnitOfWork(IsDisabled = true)]
    public async Task<StoreUserAuthResult> QueryUserProfileAsync()
    {
        var res = await this.StoreUserAuthService.GetStoreUserAsync();

        res.Data ??= new StoreUserDto();

        var storeUser = res.Data!;

        if (!storeUser.IsEmptyId())
        {
            //try set last activity time
            await this._storeUserService.TrySetLastActivityTimeAsync(storeUser.Id);

            storeUser.Grade = await _gradeService.GetGradeByUserIdAsync(storeUser.Id);

            await this._storeUserService.AttachPlatformUsersAsync(new[] { storeUser });
        }

        storeUser.User ??= new UserDto();

        if (!storeUser.User.IsEmptyId())
        {
            storeUser.User.UserMobile = await _userMobileService.GetMobileByUserIdAsync(storeUser.User.Id);
        }

        return res;
    }
}