using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Service.Warehouses;

public interface IWarehouseService : ISalesAppService
{
    Task UpdateStatusAsync(UpdateWarehouseStatusInput dto);

    Task InsertAsync(WarehouseDto dto);

    Task UpdateAsync(WarehouseDto dto);

    Task<PagedResponse<WarehouseDto>> QueryPagingAsync(QueryWarehousePagingInput dto);

    Task<WarehouseDto[]> QueryAllAsync();
}

public class WarehouseService : SalesAppService, IWarehouseService
{
    private readonly ISalesRepository<Warehouse> _repository;

    public WarehouseService(ISalesRepository<Warehouse> repository)
    {
        _repository = repository;
    }

    public async Task UpdateStatusAsync(UpdateWarehouseStatusInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var db = await _repository.GetDbContextAsync();

        var entity = await db.Set<Warehouse>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        if (dto.IsDeleted != null)
            entity.IsDeleted = dto.IsDeleted.Value;

        await db.TrySaveChangesAsync();
    }

    private string NormalizeName(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        return name.Trim();
    }

    private async Task<bool> CheckNameIsExistAsync(string name, string exceptId = null)
    {
        var db = await _repository.GetDbContextAsync();

        var query = db.Set<Warehouse>().AsNoTracking();
        query = query.Where(x => x.Name == name);
        if (!string.IsNullOrWhiteSpace(exceptId))
            query = query.Where(x => x.Id != exceptId);

        var exist = await query.AnyAsync();

        return exist;
    }

    public async Task InsertAsync(WarehouseDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        dto.Name = NormalizeName(dto.Name);

        if (await CheckNameIsExistAsync(dto.Name))
            throw new UserFriendlyException("duplicate name");

        var db = await _repository.GetDbContextAsync();

        var entity = ObjectMapper.Map<WarehouseDto, Warehouse>(dto);
        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        db.Set<Warehouse>().Add(entity);

        await db.SaveChangesAsync();
    }

    public async Task UpdateAsync(WarehouseDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        dto.Name = NormalizeName(dto.Name);

        var db = await _repository.GetDbContextAsync();

        var entity = await db.Set<Warehouse>().FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        if (await CheckNameIsExistAsync(dto.Name, entity.Id))
            throw new UserFriendlyException("duplicate name");

        entity.Name = dto.Name;
        entity.Address = dto.Address;
        entity.Lng = dto.Lng;
        entity.Lat = dto.Lat;

        await db.TrySaveChangesAsync();
    }

    public async Task<WarehouseDto[]> QueryAllAsync()
    {
        var db = await _repository.GetDbContextAsync();

        var query = db.Set<Warehouse>().AsNoTracking();

        var list = await query
            .OrderByDescending(x => x.CreationTime)
            .ToArrayAsync();

        var items = list.MapArrayTo<Warehouse, WarehouseDto>(ObjectMapper);

        return items;
    }

    public async Task<PagedResponse<WarehouseDto>> QueryPagingAsync(QueryWarehousePagingInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<Warehouse>().AsNoTracking();

        var count = await query.CountOrDefaultAsync(dto);

        var list = await query
            .OrderByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = list.MapArrayTo<Warehouse, WarehouseDto>(ObjectMapper);

        return new PagedResponse<WarehouseDto>(items, dto, count);
    }
}