import React, { useState } from 'react';

import { OrderStatus, PaymentStatus, ShippingStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { CheckOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Button } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function XConfirm(props: { model: OrderDto; ok: () => void }) {
  const { model, ok } = props;

  const [show, _show] = useState<boolean>(false);

  const [loadingSave, _loadingSave] = React.useState<boolean>(false);

  const hide = () => {
    _show(false);
  };

  const save = () => {
    _loadingSave(true);
    apiClient.mall
      .orderComplete({
        OrderId: model.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  if (model.OrderStatusId == OrderStatus.Finished) {
    return null;
  }

  if (
    model.PaymentStatusId == PaymentStatus.Paid &&
    model.ShippingStatusId == ShippingStatus.Shipped
  ) {
  } else {
    return null;
  }

  return (
    <>
      <Button
        startIcon={<CheckOutlined />}
        size={'large'}
        variant={'outlined'}
        onClick={() => {
          _show(true);
        }}
      >
        确认收货
      </Button>
      <Dialog open={show} onClose={() => hide && hide()} fullWidth>
        <DialogTitle>确认收货</DialogTitle>
        <DialogContent>
          <DialogContentText>确认收货将完成订单流程！</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => hide && hide()}>取消</Button>
          <LoadingButton
            onClick={() => {
              save();
            }}
            loading={loadingSave}
          >
            确认收货
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}
