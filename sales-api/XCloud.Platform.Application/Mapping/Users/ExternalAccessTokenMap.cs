﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Users;

namespace XCloud.Platform.Application.Mapping.Users;

public class ExternalAccessTokenMap : PlatformEntityTypeConfiguration<ExternalAccessToken>
{
    public override void Configure(EntityTypeBuilder<ExternalAccessToken> builder)
    {
        builder.ToTable("sys_external_access_token");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.Platform).IsRequired().HasMaxLength(100);
        builder.Property(x => x.AppId).IsRequired().HasMaxLength(100);

        builder.Property(x => x.UserId).HasMaxLength(100);
        builder.Property(x => x.Scope).HasMaxLength(100);

        builder.Property(x => x.AccessTokenType);
        builder.Property(x => x.AccessToken).IsRequired().HasMaxLength(1000);

        builder.Property(x => x.RefreshToken).HasMaxLength(1000);
        builder.Property(x => x.DataJson);
        builder.Property(x => x.ExpiredAt);

        base.Configure(builder);
    }
}