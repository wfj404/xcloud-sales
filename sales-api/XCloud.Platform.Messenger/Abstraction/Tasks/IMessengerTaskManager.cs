using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Helper;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction.Tasks;

public interface IMessengerTask
{
    TimeSpan Delay { get; }

    Task ExecuteAsync(CancellationToken cancellationToken);
}

public interface IMessengerTaskManager : IDisposable
{
    void RequestToClose();

    void StartTasks();
}

[ExposeServices(typeof(IMessengerTaskManager))]
public class MessengerTaskManager : IMessengerTaskManager, ISingletonDependency
{
    private readonly IReadOnlyList<IMessengerTask> _tasks;
    private readonly ILogger _logger;

    public MessengerTaskManager(IServiceProvider serviceProvider, ILogger<MessengerTaskManager> logger)
    {
        _logger = logger;
        _tasks = serviceProvider.GetServices<IMessengerTask>().ToArray();
    }

    private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

    private IReadOnlyList<Task> _threads;

    public void StartTasks()
    {
        if (_threads != null)
            throw new MessengerException("messenger task already started");

        _threads = _tasks.Select(x => Task.Run(() => RunMessengerTaskAsync(x), _cancellationTokenSource.Token))
            .ToArray();

        _logger.LogInformation($"{_threads.Count} messenger tasks started");
    }

    private async Task RunMessengerTaskAsync(IMessengerTask task)
    {
        var taskName = task.GetType().FullName;

        while (!_cancellationTokenSource.Token.IsCancellationRequested)
        {
            try
            {
                await task.ExecuteAsync(_cancellationTokenSource.Token);

                this._logger.LogInformation($"excuted:{taskName}");
            }
            catch (Exception e)
            {
                _logger.LogError(message: e.Message, exception: e);
            }

            await Task.Delay(task.Delay, _cancellationTokenSource.Token);
        }

        _logger.LogInformation($"task:{taskName} stopped gracefully");
    }

    public void RequestToClose()
    {
        this._cancellationTokenSource.Cancel();
    }

    public void Dispose()
    {
        this.RequestToClose();

        if (ValidateHelper.IsNotEmptyCollection(_threads))
        {
            var waitForThreads = Task.WhenAll(_threads.ToArray());
            var waitForDelay = Task.Delay(TimeSpan.FromSeconds(10));

            Task.WhenAny(waitForThreads, waitForDelay).Wait();
        }

        _cancellationTokenSource.Dispose();

        _logger.LogInformation($"messenger tasks disposed");
    }
}