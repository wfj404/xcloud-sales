﻿using XCloud.Platform.Application.Exceptions;

namespace XCloud.Sales.Application.Service.Stores;

public static class StoreManagerPermissionExtension
{
    public static async Task CheckRequiredSuperManagerAsync(this IStoreManagerPermissionService managerSecurityService,
        string managerId)
    {
        if (string.IsNullOrWhiteSpace(managerId))
            throw new ArgumentNullException(nameof(managerId));

        var response =
            await managerSecurityService.GetGrantedPermissionsAsync(
                managerId,
                new CacheStrategy { Cache = true });

        if (!response.IsPermissionValid("SUPER-MANAGER"))
        {
            throw new PermissionRequiredException($"super manager is required");
        }
    }

    public static async Task CheckRequiredPermissionAsync(this IStoreManagerPermissionService managerSecurityService,
        string managerId, string permissionKey)
    {
        if (string.IsNullOrWhiteSpace(managerId))
            throw new ArgumentNullException(nameof(managerId));

        if (string.IsNullOrWhiteSpace(permissionKey))
            throw new ArgumentNullException(nameof(permissionKey));

        var response =
            await managerSecurityService.GetGrantedPermissionsAsync(
                managerId,
                new CacheStrategy { Cache = true });

        if (!response.IsPermissionValid(permissionKey))
        {
            throw new PermissionRequiredException($"permission:${permissionKey} is required");
        }
    }
}