import { Button, Form, InputNumber, Modal } from 'antd';
import { useEffect, useState } from 'react';

import { OrderPriceConditionParameter } from '@/utils/swagger';

export default ({
  rule,
  ok,
}: {
  rule: OrderPriceConditionParameter;
  ok: (d: OrderPriceConditionParameter) => void;
}) => {
  const [settings, _settings] = useState<OrderPriceConditionParameter>({});
  const [show, _show] = useState(false);

  useEffect(() => {
    _settings({ ...rule });
  }, [rule]);

  return (
    <>
      <Button
        block
        onClick={() => {
          _show(true);
        }}
      >{`订单满减(${rule.LimitedOrderAmount || 0}元)`}</Button>

      <Modal
        title={'设置满减金额'}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        onOk={() => {
          ok({
            ...settings,
          });
          _show(false);
        }}
      >
        <Form>
          <Form.Item label={'满减金额'}>
            <InputNumber
              min={0}
              value={settings.LimitedOrderAmount || 0}
              onChange={(e) => {
                _settings({
                  ...settings,
                  LimitedOrderAmount: e || 0,
                });
              }}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
