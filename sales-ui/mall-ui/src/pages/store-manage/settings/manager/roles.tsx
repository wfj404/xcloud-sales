import { Alert, Button, Checkbox, message, Modal, Space, Tag } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StoreManagerDto, StoreRoleDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty } from '@/utils/utils';

export default ({ model, ok }: { model: StoreManagerDto; ok: () => void }) => {
  const [show, _show] = useState(false);
  const [selectedIds, _selectedIds] = useState<string[]>([]);
  const [loading, _loading] = useState(false);
  const [roles, _roles] = useState<StoreRoleDto[]>([]);

  const queryRoles = () => {
    _loading(false);
    apiClient.mallManager.storeManagerPermissionListRoles({})
      .then((res) => {
        handleResponse(res, () => {
          _roles(res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    show && queryRoles();
  }, [show]);

  const saveRoles = () => {
    _loading(false);
    apiClient.mallManager.storeManagerPermissionSetManagerRoles({
      Key: model.Id,
      Value: selectedIds,
    })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          _show(false);
          ok && ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const renderRoles = () => {
    if (isArrayEmpty(model.StoreRoles)) {
      return null;
    }
    return (
      <div>
        {model.StoreRoles?.map((x, i) => (
          <Tag
            style={{
              marginRight: 5,
              marginBottom: 5,
            }}
            key={i}
          >
            {x.Name}
          </Tag>
        ))}
      </div>
    );
  };

  useEffect(() => {
    model.StoreRoles && _selectedIds(model.StoreRoles.map((x) => x.Id || ''));
  }, [model]);

  const renderSelectionForm = () => {
    if (!roles || roles.length <= 0) {
      return (<Alert message={'无角色'}></Alert>);
    }

    return (<div>
      {roles.map((x, i) => (
        <Checkbox
          checked={selectedIds.indexOf(x.Id || '') >= 0}
          onChange={(e) => {
            let keys = selectedIds.filter((d) => d != x.Id);
            if (e.target.checked) {
              keys = [...keys, x.Id || '.'];
            }
            _selectedIds(keys);
          }}
          style={{
            marginRight: 5,
            marginBottom: 5,
          }}
          key={i}
          value={x.Id}
        >
          {x.Name}
        </Checkbox>
      ))}
    </div>);
  };

  return (
    <>
      <Space direction={'horizontal'}>
        {renderRoles()}
        <Button
          type={'link'}
          onClick={() => {
            _show(true);
          }}
        >
          修改
        </Button>
      </Space>
      <Modal
        title={'绑定角色'}
        confirmLoading={loading}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        onOk={() => {
          saveRoles();
        }}
      >
        {renderSelectionForm()}
      </Modal>
    </>
  );
};
