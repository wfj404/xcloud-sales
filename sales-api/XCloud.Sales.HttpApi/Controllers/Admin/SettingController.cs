﻿using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Dto;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/settings")]
public class SettingController : ShopBaseController
{
    private readonly ISettingService _settingService;
    private readonly CategoryViewService _categoryViewService;
    private readonly DashboardViewService _dashboardViewService;
    private readonly SearchViewService _searchViewService;
    private readonly IStoreService _storeService;

    public SettingController(DashboardViewService dashboardViewService,
        SearchViewService searchViewService,
        CategoryViewService categoryViewService,
        ISettingService settingService,
        IStoreService storeService)
    {
        _searchViewService = searchViewService;
        _dashboardViewService = dashboardViewService;
        _categoryViewService = categoryViewService;
        _settingService = settingService;
        _storeService = storeService;
    }

    [HttpPost("refresh-view-cache")]
    public async Task<ApiResponse<object>> RefreshViewCacheAsync([FromBody] RefreshViewCacheInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        if (dto.MallSettings)
        {
            await SettingService.GetSettingsAsync<MallSettingsDto>(new CacheStrategy { Refresh = true });
        }

        if (dto.Home)
        {
            await this.SettingService.GetSettingsAsync<HomePageBlocksDto>(new CacheStrategy() { Refresh = true });
        }

        if (dto.RootCategory)
        {
            var stores = await this._storeService.ListAllStoresAsync(false, 1000);
            foreach (var m in stores)
            {
                await this._categoryViewService.GetStoreRootCategoriesAsync(m.Id,
                    new CacheStrategy() { Refresh = true });
            }
        }

        if (dto.Dashboard)
        {
            await _dashboardViewService.CounterAsync(new CacheStrategy { Refresh = true });
        }

        if (dto.Search)
        {
            await _searchViewService.QuerySearchViewAsync(new CacheStrategy { Refresh = true });
        }

        return new ApiResponse<object>();
    }

    [HttpPost("home-page-blocks")]
    public async Task<ApiResponse<HomePageBlocksDto>> HomePageBlocksAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var data = await this.SettingService.GetSettingsAsync<HomePageBlocksDto>(new CacheStrategy() { Source = true });

        data ??= new HomePageBlocksDto();

        return new ApiResponse<HomePageBlocksDto>(data);
    }

    [HttpPost("save-home-page-blocks")]
    public async Task<ApiResponse<object>> SaveHomePageBlocksAsync([FromBody] HomePageBlocksDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        await this.SettingService.SaveSettingsAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("mall-settings")]
    public async Task<ApiResponse<MallSettingsDto>> GetMallSettingsAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var settings = await _settingService.GetSettingsAsync<MallSettingsDto>(new CacheStrategy() { Refresh = true });

        return new ApiResponse<MallSettingsDto>(settings);
    }

    [HttpPost("save-mall-settings")]
    public async Task<ApiResponse<object>> SaveMallSettingsAsync([FromBody] MallSettingsDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        await _settingService.SaveSettingsAsync(dto);

        return new ApiResponse<object>();
    }
}