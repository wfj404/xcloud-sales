namespace XCloud.Sales.Application.Domain.Shipping;

public class FreightTemplateGoodsMapping : SalesBaseEntity<string>, IHasCreationTime
{
    public string StoreId { get; set; }

    public string TemplateId { get; set; }

    public string GoodsId { get; set; }

    public DateTime CreationTime { get; set; }
}