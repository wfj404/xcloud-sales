import { Button, Form, message, Modal, Space, Switch } from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { apiClient } from '@/utils/client';
import { UpdateUserStatusDto, UserDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default (props: { ok: () => void; model: UserDto }) => {
  const { ok, model } = props;
  const [loading, _loading] = useState(false);
  const [show, _show] = useState(false);
  const [formdata, _formdata] = useState<UpdateUserStatusDto>({});

  const save = (e: UpdateUserStatusDto) => {
    _loading(true);
    apiClient.sys
      .manageUserUpdateStatus({ ...e, IsDeleted: false, Id: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          _show(false);
          ok && ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _formdata({
      IsActive: model?.IsActive,
      IsDeleted: model?.IsDeleted,
    });
  }, [model]);

  return (
    <>
      <Modal
        title='修改状态'
        confirmLoading={loading}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                let res = await apiClient.sys.manageUserUpdateStatus({
                  Id: model.Id,
                  IsDeleted: true,
                });
                handleResponse(res, () => {
                  _show(false);
                  ok();
                });
              }}
              hide={isStringEmpty(model.Id)} />
            <Button type={'primary'} onClick={() => {
              save(formdata);
            }}>保存</Button>
          </Space>
        }
      >
        <Form>
          <Form.Item label='是否激活'>
            <Switch checked={formdata.IsActive || false}
                    onChange={e => _formdata(d => ({ ...d, IsActive: e }))} />
          </Form.Item>
        </Form>
      </Modal>
      <Button
        type={'link'}
        onClick={() => {
          _show(true);
        }}
      >修改状态</Button>
    </>
  );
};
