import { useRequest } from 'ahooks';
import { Card } from 'antd';
import * as echarts from 'echarts';
import { useEffect, useRef } from 'react';

import { apiClient } from '@/utils/client';

export default () => {
  const reportRequest = useRequest(
    apiClient.mallAdmin.reportGroupByGeoLocation,
    {
      manual: true,
    },
  );

  const data = reportRequest.data?.data.Data || [];

  const chartRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!chartRef.current) {
      return;
    }
    let chart = echarts.init(chartRef.current);
    chart.setOption({
      legend: {
        top: 'bottom',
      },
      toolbox: {
        show: true,
        feature: {
          mark: { show: true },
          dataView: { show: true, readOnly: false },
          restore: { show: true },
          saveAsImage: { show: true },
        },
      },
      series: [
        {
          name: '城市分布',
          type: 'pie',
          radius: [50, 250],
          center: ['50%', '50%'],
          roseType: 'area',
          itemStyle: {
            borderRadius: 8,
          },
          data: data.map((x) => ({
            name: `${x.Country}-${x.City}`,
            value: x.Count,
          })),
        },
      ],
    });
  }, [data]);

  useEffect(() => {
    reportRequest.run({});
  }, []);

  return (
    <>
      <Card
        title="用户活跃时间段"
        style={{ marginBottom: 10 }}
        loading={reportRequest.loading}
      >
        <div style={{ minHeight: 500, margin: 20 }} ref={chartRef}></div>
      </Card>
    </>
  );
};
