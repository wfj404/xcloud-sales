﻿using System;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;

namespace XCloud.Platform.WeChat.Job;

public static class WechatJobConfiguration
{
    internal static void StartWechatJobs(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var manager = s.ServiceProvider.GetRequiredService<IRecurringJobManager>();

        manager.AddOrUpdate<WechatPaymentJobs>("update-wechat-payment-certificate",
            x => x.UpdateMchCertificateAsync(),
            Cron.Daily());

        manager.AddOrUpdate<WechatMpJobs>("refresh-mp-access-token",
            x => x.RefreshClientAccessTokenAsync(),
            Cron.Minutely());
    }
}