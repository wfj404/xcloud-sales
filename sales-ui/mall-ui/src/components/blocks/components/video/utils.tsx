import { StorageMetaDto } from '@/utils/swagger';

import { BlockItem, BlockItemProps } from '../../utils';

//-------------------------------------------------------
export const video_demo: BlockItem<VideoBlockProps> = {
  type: 'video',
  style: {
    marginBottom: 10,
  },
  body: {
    video_url: 'https://vjs.zencdn.net/v/oceans.mp4',
    storage: undefined,
    controls: true,
  },
};

export interface VideoBlockProps extends BlockItemProps {
  video_url?: string;
  poster?: StorageMetaDto | string;
  storage?: StorageMetaDto | string;
  auto_play?: boolean;
  loop?: boolean;
  controls?: boolean;
}
