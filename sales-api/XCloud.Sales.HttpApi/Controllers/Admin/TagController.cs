using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/tag")]
public class TagController : ShopBaseController
{
    private readonly ITagService _tagService;

    public TagController(ITagService tagService)
    {
        _tagService = tagService;
    }

    [HttpPost("list-tags")]
    public virtual async Task<ApiResponse<TagDto[]>> ListTagsAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var tags = await _tagService.QueryAllAsync();

        return new ApiResponse<TagDto[]>(tags);
    }

    [HttpPost("save-tag")]
    public virtual async Task<ApiResponse<object>> SaveTagAsync([FromBody] TagDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _tagService.InsertAsync(dto);
        }
        else
        {
            await _tagService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public virtual async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        await _tagService.DeleteByIdAsync(dto);

        return new ApiResponse<object>();
    }
}