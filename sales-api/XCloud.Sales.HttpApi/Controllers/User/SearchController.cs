using XCloud.Application.Model;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Extension;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Sales.Application.Service.Search;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/search")]
public class SearchController : ShopBaseController
{
    private readonly SearchViewService _searchViewService;
    private readonly GoodsViewService _goodsViewService;
    private readonly IBrandService _brandService;
    private readonly ICategoryService _categoryService;
    private readonly ITagService _tagService;
    private readonly IGoodsSearchService _goodsSearchService;
    private readonly IGoodsService _goodsService;
    private readonly IPriceCalculationService _priceCalculationService;

    public SearchController(SearchViewService searchViewService,
        IBrandService brandService,
        ICategoryService categoryService,
        ITagService tagService,
        IGoodsSearchService goodsSearchService,
        IGoodsService goodsService,
        IPriceCalculationService priceCalculationService,
        GoodsViewService goodsViewService)
    {
        _searchViewService = searchViewService;
        _brandService = brandService;
        _categoryService = categoryService;
        _tagService = tagService;
        _goodsSearchService = goodsSearchService;
        _goodsService = goodsService;
        _priceCalculationService = priceCalculationService;
        _goodsViewService = goodsViewService;
    }

    [HttpPost("search-view")]
    public async Task<ApiResponse<SearchView>> SearchViewAsync()
    {
        var response = await _searchViewService.QuerySearchViewAsync(new CacheStrategy { Cache = true });

        return new ApiResponse<SearchView>(response);
    }

    [HttpPost("search-options")]
    public async Task<ApiResponse<SearchOptionsDto>> SearchOptionsAsync([FromBody] QueryGoodsPaging dto)
    {
        var options = new SearchOptionsDto();

        if (!string.IsNullOrWhiteSpace(dto.BrandId))
        {
            var brand = await _brandService.GetByIdAsync(dto.BrandId);
            if (brand != null)
                options.Brand = ObjectMapper.Map<Brand, BrandDto>(brand);
        }

        if (!string.IsNullOrWhiteSpace(dto.CategoryId))
        {
            options.Category = await _categoryService.GetByIdAsync(dto.CategoryId);
        }

        if (!string.IsNullOrWhiteSpace(dto.TagId))
        {
            options.Tag = await _tagService.GetByIdAsync(dto.TagId);
        }

        return new ApiResponse<SearchOptionsDto>(options);
    }

    [HttpPost("list-by-tag-name")]
    public async Task<ApiResponse<GoodsDto[]>> ListByTagNameAsync([FromBody] ListByTagNameInput dto)
    {
        dto.Count ??= 20;
        dto.Count = Math.Min(dto.Count.Value, 100);

        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var goods = await this._goodsService.ListByTagNameAsync(dto);

        await this._goodsService.AttachSkusAsync(goods);

        var skus = goods.SelectMany(x => x.Skus).WhereNotNull().ToArray();

        var settings = await SettingService.GetCachedMallSettingsAsync();

        var loginUser = await StoreUserAuthService.GetStoreUserOrNullAsync();

        if (loginUser != null || settings.GoodsSettings.DisplayPriceForGuest)
        {
            await _priceCalculationService.AttachPriceModelAsync(skus, storeId, loginUser?.Id);
        }
        else
        {
            skus.HidePrice();
        }

        return new ApiResponse<GoodsDto[]>(goods);
    }

    [HttpPost("goods")]
    public async Task<PagedResponse<GoodsDto>> SearchGoodsListAsync([FromBody] QueryGoodsPaging dto)
    {
        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        dto.StoreId = storeId;
        dto.PageSize = 20;
        dto.SkipCalculateTotalCount = true;
        dto.IsPublished = true;

        var response = await _goodsSearchService.SearchGoodsAsync(dto);

        if (response.IsEmpty)
        {
            return response;
        }

        var loginUser = await StoreUserAuthService.GetStoreUserOrNullAsync();

        await this._goodsViewService.AttachPreviewInfoForListAsync(response.Items.ToArray(), storeId, loginUser);

        if (dto.Page == 1 && !string.IsNullOrWhiteSpace(dto.Keywords) && loginUser != null)
        {
            await SalesEventBusService.NotifyInsertActivityLog(new ActivityLog
            {
                StoreUserId = loginUser.Id,
                ActivityLogTypeId = ActivityLogType.SearchGoods,
                Value = dto.Keywords.Trim().ToLower(),
                Comment = "keyword-searching",
                Data = string.Empty
            });
        }

        return response;
    }
}