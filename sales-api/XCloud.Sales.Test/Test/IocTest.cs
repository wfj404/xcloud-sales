﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp;
using XCloud.Core.Extension;

namespace XCloud.Sales.Test.Test;

[TestClass]
public class IocTest
{
    interface IId
    {
    }

    class D : IId
    {
        public int Xx { get; set; }
    }

    class Dd : IId
    {
    }

    [TestMethod]
    public void autofac_repeat_reg_test()
    {
        var builder = new ContainerBuilder();

        builder.RegisterType<D>().AsSelf().AsImplementedInterfaces();
        builder.RegisterType<Dd>().AsSelf().AsImplementedInterfaces();
        builder.RegisterInstance(new Dd { }).AsSelf().AsImplementedInterfaces();

        using var con = builder.Build();
        (con.Resolve<IEnumerable<IId>>().ToArray().Length == 3).Should().BeTrue();
        (con.Resolve<IId>().GetType() == typeof(Dd)).Should().BeTrue();
    }

    [TestMethod]
    public async Task scope_test()
    {
        var collection = new ServiceCollection();
        collection.AddScoped<D>();

        await using var provider = collection.BuildServiceProvider();

        using var scope1 = provider.CreateScope();
        var xx = scope1.ServiceProvider.GetRequiredService<D>();
        xx.Xx = 88;
        (scope1.ServiceProvider.GetRequiredService<D>().Xx == xx.Xx).Should().BeTrue();

        using var scope2 = scope1.ServiceProvider.CreateScope();
        var dd = scope2.ServiceProvider.GetRequiredService<D>();
        (dd.Xx == 0).Should().BeTrue();
    }

    [TestMethod]
    public void single_instance()
    {
        var collection = new ServiceCollection();
        collection.AddSingleton(provider => new Service());

        if (collection.GetSingletonInstanceOrNull<Service>() != null)
            throw new AssertFailedException(nameof(Service));

        collection.RemoveAll<Service>();
        collection.AddSingleton(new Service());

        if (collection.GetSingletonInstanceOrNull<Service>() == null)
            throw new AbpException(nameof(single_instance));
    }

    interface ITransit : IDisposable
    {
        Action Cb { get; set; }
    }

    interface IScoped : IDisposable
    {
        Action Cb { get; set; }
    }

    interface ISingle : IDisposable
    {
        Action Cb { get; set; }
    }

    class Service : ITransit, IScoped, ISingle
    {
        public Action Cb { get; set; }

        public void Dispose()
        {
            Cb?.Invoke();
        }
    }

    /// <summary>
    /// 印证我的想法
    /// </summary>
    [TestMethod]
    public async Task di_test_transient()
    {
        var collection = new ServiceCollection()
            .AddTransient<ITransit, Service>()
            .AddScoped<IScoped, Service>()
            .AddSingleton<ISingle, Service>();

        await using var provider = collection.BuildServiceProvider();

        var singleDispose = 0;
        var scopedDispose = 0;
        var transitDispose = 0;

        Action addSingle = () => ++singleDispose;
        Action addScoped = () => ++scopedDispose;
        Action addTransit = () => ++transitDispose;

        var singleObject = provider.GetRequiredService<ISingle>();
        singleObject.Cb = addSingle;

        //作用域1
        using (var s = provider.CreateScope())
        {
            var anotherSingleObject = s.ServiceProvider.GetRequiredService<ISingle>();
            anotherSingleObject.Cb = addSingle;
            (singleObject == anotherSingleObject).Should().BeTrue();
        }

        //作用域2
        using (var s = provider.CreateScope())
        {
            var anotherSingleObject = s.ServiceProvider.GetRequiredService<ISingle>();
            anotherSingleObject.Cb = addSingle;
            (singleObject == anotherSingleObject).Should().BeTrue();

            var scopedObject = s.ServiceProvider.GetRequiredService<IScoped>();
            scopedObject.Cb = addScoped;
            var anotherScopedObject = s.ServiceProvider.GetRequiredService<IScoped>();
            anotherScopedObject.Cb = addScoped;
            (scopedObject == anotherScopedObject).Should().BeTrue();

            var transitObject = s.ServiceProvider.GetRequiredService<ITransit>();
            transitObject.Cb = addTransit;
            var anotherTransitObject = s.ServiceProvider.GetRequiredService<ITransit>();
            anotherTransitObject.Cb = addTransit;
            (transitObject != anotherTransitObject).Should().BeTrue();
        }

        //出了作用域
        (singleDispose == 0).Should().BeTrue();
        (scopedDispose == 1).Should().BeTrue();
        (transitDispose == 2).Should().BeTrue();
    }
}