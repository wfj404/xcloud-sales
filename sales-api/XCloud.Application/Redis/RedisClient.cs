﻿using System;
using StackExchange.Redis;

namespace XCloud.Application.Redis;

/// <summary>
/// https://stackexchange.github.io/StackExchange.Redis/Configuration
/// </summary>
public class RedisClient : IDisposable
{
    public string ConnectionString { get; }

    public ConnectionMultiplexer Connection { get; }

    public RedisClient(ConnectionMultiplexer con)
    {
        Connection = con ?? throw new ArgumentNullException(nameof(con));
    }

    private static ConnectionMultiplexer ConnectRedis(string connectionString)
    {
        if (string.IsNullOrWhiteSpace(connectionString))
            throw new ArgumentNullException(nameof(connectionString));

        var connection = ConnectionMultiplexer.Connect(connectionString);

        return connection;
    }

    public RedisClient(string connectionString) : this(ConnectRedis(connectionString))
    {
        if (string.IsNullOrWhiteSpace(connectionString))
            throw new ArgumentNullException(nameof(connectionString));
        
        ConnectionString = connectionString;
    }

    public void Dispose()
    {
        Connection?.Dispose();
    }
}