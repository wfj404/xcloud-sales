import { Button, InputNumber, message, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { pullAt } from 'lodash-es';

import XSkuSelector from '@/components/goods/sku/sku_selector';
import { StockItemDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { PlusOutlined } from '@ant-design/icons';

export default (props: {
  items: StockItemDto[];
  onChange: (d: StockItemDto[]) => void;
}) => {
  const { items, onChange } = props;

  const updateItem = (
    index: number,
    updateFunc: (e: StockItemDto) => StockItemDto,
  ) => {
    let dataCopy = [...items];
    dataCopy[index] = updateFunc(dataCopy[index]);

    //set data
    onChange(dataCopy);
  };

  const columns: ColumnType<StockItemDto>[] = [
    {
      title: '商品',
      width: 300,
      render: (d, x, i) => {
        return (
          <>
            {isStringEmpty(x.SkuId) && (
              <div
                style={{
                  fontWeight: 'bold',
                  color: 'red',
                  marginBottom: 5,
                }}
              >
                请选择
              </div>
            )}
            <XSkuSelector
              selectedSku={x.Sku || {}}
              onChange={(e) => {
                if (!e) {
                  return;
                }
                if (items.some((m, i) => i != i && m.SkuId == e.Id)) {
                  message.error('商品已经存在于采购单，请重新选择');
                  return;
                }

                updateItem(
                  i,
                  (d: StockItemDto): StockItemDto => ({
                    ...d,
                    Sku: e,
                    SkuId: e.Id,
                  }),
                );
              }}
              style={{
                width: '100%',
              }}
            />
          </>
        );
      },
    },
    {
      title: '数量',
      render: (d, x, i) => {
        return (
          <>
            {(x.Quantity && x.Quantity > 0) || (
              <div
                style={{
                  fontWeight: 'bold',
                  color: 'red',
                  marginBottom: 5,
                }}
              >
                请输入
              </div>
            )}
            <InputNumber
              min={1}
              value={x.Quantity}
              onChange={(e) => {
                updateItem(
                  i,
                  (d: StockItemDto): StockItemDto => ({
                    ...d,
                    Quantity: e || 0,
                  }),
                );
              }}
            />
          </>
        );
      },
    },
    {
      title: '进货价',
      render: (d, x, i) => {
        return (
          <>
            {(x.Price && x.Price > 0) || (
              <div
                style={{
                  fontWeight: 'bold',
                  color: 'red',
                  marginBottom: 5,
                }}
              >
                请输入
              </div>
            )}
            <InputNumber
              min={0}
              value={x.Price}
              onChange={(e) => {
                updateItem(
                  i,
                  (d: StockItemDto): StockItemDto => ({
                    ...d,
                    Price: e || 0,
                  }),
                );
              }}
            />
          </>
        );
      },
    },
    {
      title: '操作',
      render: (d, x, i) => {
        return (
          <Button
            type="link"
            danger
            onClick={() => {
              if (!confirm('确认删除？')) {
                return;
              }
              let datalist = [...items];
              pullAt(datalist, i);

              onChange(datalist);
            }}
          >
            删除
          </Button>
        );
      },
    },
  ];

  return (
    <>
      <Table
        dataSource={items}
        columns={columns}
        pagination={false}
        style={{ marginBottom: 10 }}
      />
      <Button
        icon={<PlusOutlined />}
        block
        type="dashed"
        onClick={() => {
          onChange([
            ...items,
            {
              Quantity: 10,
            },
          ]);
        }}
      >
        添加商品
      </Button>
    </>
  );
};
