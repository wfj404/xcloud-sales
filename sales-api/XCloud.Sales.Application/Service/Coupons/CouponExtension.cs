using XCloud.Sales.Application.Domain.Coupons;

namespace XCloud.Sales.Application.Service.Coupons;

public static class CouponExtension
{
    private static bool IsUserCouponExpired(this CouponUserMapping userCoupon, DateTime now)
    {
        return userCoupon.ExpiredAt != null && userCoupon.ExpiredAt.Value < now;
    }

    public static bool IsValid(this CouponUserMapping couponUserMapping, DateTime now, out string error)
    {
        error = string.Empty;

        if (couponUserMapping.IsUserCouponExpired(now))
        {
            error = "user coupon is expired";
            return false;
        }

        if (couponUserMapping.IsUsed)
        {
            error = "user coupon is used before";
            return false;
        }

        return true;
    }

    public static bool IsValid(this Coupon coupon, DateTime now, out string error)
    {
        error = string.Empty;

        if (!coupon.IsActive)
        {
            error = "coupon is inactive";
            return false;
        }

        if (coupon.IsDeleted)
        {
            error = "coupon is deleted";
            return false;
        }

        if (!coupon.IsTimeRangeValid(now))
        {
            error = "coupon is not available for now";
            return false;
        }

        return true;
    }

    public static bool AccountLimitationIsSet(this Coupon m, out int limitation)
    {
        limitation = default;
        if (m.AccountIssuedLimitCount != null && m.AccountIssuedLimitCount.Value > 0)
        {
            limitation = m.AccountIssuedLimitCount.Value;
            return true;
        }
        else
        {
            return false;
        }
    }
}