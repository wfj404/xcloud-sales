using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Orders.Validator.Validators;

public class OrderStoreConditionParameter : IRuleParameter
{
    public StoreDto[] SupportedStores { get; set; }
}

[ExposeServices(typeof(IOrderValidator))]
public class OrderStoreValidator : OrderValidatorBase<OrderStoreConditionParameter>, IOrderValidator,
    ITransientDependency
{
    public int Order => default;

    public string ValidatorType => OrderConditionValidatorTypes.Store;

    public Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule)
    {
        return Task.FromResult(rule.ValidatorType == ValidatorType);
    }

    public async Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule)
    {
        await Task.CompletedTask;
        return rule.Description ?? string.Empty;
    }

    public async Task<OrderConditionValidationResponse> IsConditionMatchedAsync(OrderConditionCheckDto input,
        OrderValidatorRule rule)
    {
        var response = new OrderConditionValidationResponse();

        if (string.IsNullOrWhiteSpace(rule.RuleJson))
        {
            response.SetError("promotion config error");
            return response;
        }

        var param = ParseRequiredRuleParameter(rule.RuleJson);

        if (param.SupportedStores == null)
        {
            response.SetError("coupon rule error:store");
            return response;
        }

        if (!param.SupportedStores.Ids().Contains(input.Order.StoreId))
        {
            response.SetError("下单门店不满足要求");
            return response;
        }

        await Task.CompletedTask;

        return response;
    }
}