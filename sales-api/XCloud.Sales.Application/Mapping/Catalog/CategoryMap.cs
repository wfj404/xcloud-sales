using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class CategoryMap : SalesEntityTypeConfiguration<Category>
{
    public override void Configure(EntityTypeBuilder<Category> builder)
    {
        builder.ToTable("sales_category");

        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigAbpSoftDeletionTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigTreeEntity();

        builder.Property(c => c.Name).IsRequired().HasMaxLength(100);
        builder.Property(c => c.SeoName).HasMaxLength(100);
        builder.Property(c => c.Description).HasMaxLength(1000);
        builder.Property(c => c.NodePath).HasMaxLength(1000);
        builder.Property(x => x.Recommend);

        builder.Property(x => x.RootId).HasMaxLength(100);
        builder.Property(x => x.PictureMetaId).HasMaxLength(100);
        builder.Property(x => x.ShowOnHomePage);
        builder.Property(x => x.Published);
        builder.Property(x => x.DisplayOrder);

        base.Configure(builder);
    }
}