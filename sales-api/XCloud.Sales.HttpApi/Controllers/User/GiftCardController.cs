﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.GiftCards;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/gift-card")]
public class GiftCardController : ShopBaseController
{
    private readonly IGiftCardService _prepaidCardService;

    public GiftCardController(IGiftCardService prepaidCardService)
    {
        _prepaidCardService = prepaidCardService;
    }

    [HttpPost("by-id")]
    public async Task<ApiResponse<GiftCardDto>> QueryByIdAsync([FromBody] IdDto dto)
    {
        var storeUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var cardDto = await _prepaidCardService.QueryByIdAsync(dto.Id);

        if (cardDto == null)
            throw new EntityNotFoundException(nameof(QueryByIdAsync));

        if (cardDto.EndTime != null && cardDto.EndTime.Value < Clock.Now)
            cardDto.Expired = true;

        return new ApiResponse<GiftCardDto>(cardDto);
    }

    [HttpPost("use-card")]
    public async Task<ApiResponse<object>> UseCardAsync([FromBody] UseGiftCardInput dto)
    {
        var storeUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = storeUser.Id;

        await _prepaidCardService.UseGiftCardAsync(dto);

        return new ApiResponse<object>();
    }
}