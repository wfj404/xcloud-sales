﻿using Microsoft.Extensions.Configuration;

namespace XCloud.Sales.Qingdao.Configuration;

public static class Sh3HConfigurationExtension
{
    public static Sh3HConfig GetRequired3HConfig(this IConfiguration configuration)
    {
        var config = new Sh3HConfig();

        var section = configuration.GetRequiredSection("3h");

        section.Bind(config);

        config.Validate();

        return config;
    }
}