import { httpClient } from '@/utils/http';
import { ApiResponse } from '@/utils/biz';
import { StorageMetaDto } from '@/utils/swagger';
import { getFileExtension } from '@/utils/utils';
import Compressor from 'compressorjs';

export const allowedImageExtensions = (): string[] => ['jpg', 'jpeg', 'png', 'gif'];

export const isImage = (filename: string): boolean => {
  let ext = getFileExtension(filename)?.toLowerCase() || '';

  return allowedImageExtensions().indexOf(ext) >= 0;
};

export const compressImageFunc = (
  file: File,
  maxHeight: number | undefined,
  maxWidth: number | undefined,
  quantity: number,
): Promise<File> => {
  return new Promise<File>((resolve, reject) => {
    const compressor = new Compressor(file, {
      maxHeight: maxHeight,
      maxWidth: maxWidth,
      quality: quantity,
      success: (result: File) => {
        resolve(result);
      },
      error: (error: Error) => {
        reject(error);
      },
    });
    console.log(compressor);
  });
};

export const compressImageV2 = async (
  file: File,
  maxFileSize: number,
): Promise<File> => {
  if (file.size <= maxFileSize) {
    return file;
  }

  //第一次只压缩质量
  file = await compressImageFunc(file, undefined, undefined, 0.9);

  if (file.size <= maxFileSize) {
    return file;
  }

  //只压缩质量不能满足需求
  //在有限的尝试内对尺寸裁剪
  const maxTry = 5;
  const step = 300;
  let maxImageSize = 1000;
  let i = 0;

  while (++i <= maxTry) {
    file = await compressImageFunc(file, maxImageSize, maxImageSize, 1);
    if (file.size <= maxFileSize) {
      return file;
    }
    //next loop
    maxImageSize -= step;
    if (maxImageSize <= 0) {
      break;
    }
  }

  return file;
};

export const uploadFileV2 = async (file: File): Promise<StorageMetaDto> => {
  let res = await uploadMultipleFiles([file]);

  let data = res.at(0);
  if (data == undefined) {
    throw new Error('response error');
  }

  return data;
};

export const uploadMultipleFiles = async (
  files: File[],
): Promise<StorageMetaDto[]> => {

  const formdata = new FormData();
  files.forEach((x, i) => {
    formdata.append(`formFileCollection`, x, x.name);
  });

  let res = await httpClient.post<ApiResponse<StorageMetaDto[]>>(
    '/api/platform/storage/upload',
    formdata,
  );

  if (res.data.Error) {
    throw new Error(res.data.Error.Message || 'upload error');
  }

  return res.data.Data || [];
};

export const fileAsBase64 = (file: File): Promise<string | null | undefined> => {
  return new Promise<string>((resolve, reject) => {
    let reader = new FileReader();
    reader.onload = function() {
      resolve(reader.result as string);
    };
    reader.onerror = function(error) {
      reject(error);
    };
    reader.readAsDataURL(file);
  });
};
