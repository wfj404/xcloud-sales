﻿using JetBrains.Annotations;
using Volo.Abp.Timing;
using XCloud.Platform.Application.Service.SerialNos;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Orders.PlaceOrder;
using XCloud.Sales.Application.Service.ShoppingCart;

namespace XCloud.Sales.Application.Utils;

[ExposeServices(typeof(OrderUtils))]
public class OrderUtils : SalesAppService
{
    private readonly ISerialNoService _serialNoService;
    private readonly IClock _clock;

    public OrderUtils(IClock clock,
        ISerialNoService serialNoService)
    {
        _clock = clock;
        _serialNoService = serialNoService;
    }

    public IEnumerable<PlaceOrderRequestItemDto> BuildFromShoppingCarts(ShoppingCartItemDto[] data)
    {
        if (data == null)
            throw new ArgumentNullException(nameof(data));

        foreach (var m in data)
        {
            var item = new PlaceOrderRequestItemDto
            {
                SkuId = m.SkuId,
                Quantity = m.Quantity,
                ValueAddedItemIds = m.ValueAddedItems?.Ids()
            };

            item.ValueAddedItemIds ??= Array.Empty<int>();

            yield return item;
        }
    }

    /// <summary>
    /// 202301-0000001
    /// 100_0000 max
    /// </summary>
    public async Task<string> GenerateOrderSnAsync()
    {
        //date format: yyyyMMdd
        string MonthString() => _clock.Now.ToString("yyyyMM");

        string PadLeft(int sn, int width)
        {
            var originSn = sn.ToString();
            var padLength = Math.Max(originSn.Length, width);
            return originSn.PadLeft(totalWidth: padLength, paddingChar: '0');
        }

        var month = MonthString();

        var sn =
            await _serialNoService.CreateSerialNoAsync(
                new CreateSerialNoDto($"order-sn-{month}"));

        if (sn >= 100_0000)
            throw new BusinessException(message: "order code is too large,in this month");

        var code = PadLeft(sn, 6);

        return $"{month}-{code}";
    }

    public string[] ActiveStatus() => new[] { SalesConstants.OrderStatus.Pending, SalesConstants.OrderStatus.Processing };

    [NotNull]
    [ItemNotNull]
    public async Task<Order> GetRequiredOrderForProcessAsync(ISalesRepository<Order> repository, string orderId)
    {
        var order = await repository.GetRequiredByIdAsync(orderId);

        if (order.HideForCustomer)
            throw new UserFriendlyException("order is deleted");

        if (!order.IsNormalGoodsOrder())
            throw new AbpException("wrong order type");

        return order;
    }

    public int CastMoneyToCent(decimal price) => (int)(price * 100);

    public bool IsMoneyEqual(decimal price1, decimal price2) => CastMoneyToCent(price1) == CastMoneyToCent(price2);
}