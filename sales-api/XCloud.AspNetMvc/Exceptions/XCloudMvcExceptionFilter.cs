using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Logging;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.AspNetMvc.Exceptions;

public class ExceptionResponse : ApiResponse<object>
{
    public string Message { get; set; }

    public string StackTrace { get; set; }

    public string Source { get; set; }
}

[ExposeServices(typeof(XCloudMvcExceptionFilter), typeof(IAsyncExceptionFilter))]
public class XCloudMvcExceptionFilter : IAsyncExceptionFilter, ITransientDependency
{
    private readonly IWebHostEnvironment _environment;
    private readonly ILogger _logger;

    public XCloudMvcExceptionFilter(ILogger<XCloudMvcExceptionFilter> logger,
        IWebHostEnvironment environment)
    {
        _logger = logger;
        _environment = environment;
    }

    public async Task OnExceptionAsync(ExceptionContext context)
    {
        await Task.CompletedTask;

        if (!ShouldHandle(context, out var e))
            return;

        var converters = context.HttpContext.RequestServices
            .GetServices<IErrorInfoConverter>()
            .OrderByDescending(x => x.Order)
            .ToArray();

        var error = converters.ConvertToComposedErrorInfoOrNull(context.HttpContext, e);

        if (error == null)
        {
            error = new ErrorInfo
            {
                HttpStatusCode = HttpStatusCode.InternalServerError,
                Message = "unhandled exception"
            };

            //_logger.LogError(message: error.Message, exception: e);
            _logger.LogExceptionX(exception: e, message: "global-exception-handler");
        }

        context.Result = ErrorToExceptionResponse(error, e);
        context.ExceptionHandled = true;
    }

    [NotNull]
    private IActionResult ErrorToExceptionResponse([NotNull] ErrorInfo error, [NotNull] Exception e)
    {
        var response = new ExceptionResponse() { Error = error };

        if (_environment.IsDevelopment() || _environment.IsStaging())
        {
            response.Message = e.Message;
            response.StackTrace = e.StackTrace;
            response.Source = e.Source;
        }

        var result = new JsonResult(response);

        if (error.HttpStatusCode != null)
        {
            result.StatusCode = (int)error.HttpStatusCode.Value;
        }

        return result;
    }

    private bool ShouldHandle(ExceptionContext context, out Exception e)
    {
        e = context.Exception;

        if (e == null || context.ExceptionHandled)
        {
            return false;
        }

        if (context.HttpContext.Response.HasStarted)
        {
            this._logger.LogError(exception: e, message: e.Message);
            return false;
        }

        return true;
    }
}