import {
  Alert,
  Badge,
  Button,
  Card,
  Col,
  Image,
  message,
  Progress,
  Row,
  Select,
  Space,
  Tooltip,
} from 'antd';
import { pullAt } from 'lodash-es';
import { useRef, useState } from 'react';

import { getPictureSrc, sortGoodsPictures } from '@/utils/biz';
import { config } from '@/utils/config';
import { GoodsPictureDto, SkuDto } from '@/utils/swagger';
import {
  allowedImageExtensions,
  compressImageV2,
  fileAsBase64,
  isImage,
  uploadFileV2,
} from '@/utils/upload_file';
import { isArrayEmpty, isStringEmpty, swapArrayPosition } from '@/utils/utils';
import {
  ArrowLeftOutlined,
  ArrowRightOutlined,
  DeleteOutlined,
  UploadOutlined,
} from '@ant-design/icons';

interface UploadingList {
  file: File;
  loading: boolean;
  done: boolean;
  error?: string;
  base64?: string | null;
}

export default ({
  data,
  ok,
  count,
  maxSize,
  title,
  skus,
}: {
  data: GoodsPictureDto[];
  ok: (d: GoodsPictureDto[]) => void;
  skus?: SkuDto[] | null;
  title?: string;
  count?: number;
  maxSize?: number;
}) => {
  const inputRef = useRef<HTMLInputElement>(null);
  const [uploadingList, _uploadingList] = useState<UploadingList[]>([]);

  const goodsPictureList = sortGoodsPictures(data || []);
  const setGoodsPictureList = (x: GoodsPictureDto[]) => {
    ok && ok(x.map((x, i) => ({ ...x, DisplayOrder: i })));
  };

  const pictureCountLimit = count || 20;
  const pictureCountRemain = pictureCountLimit - goodsPictureList.length;
  const pictureMaxSize = maxSize || config.upload.maxSize;

  const renderActionButton = (item: GoodsPictureDto, index: number) => {
    const skuList = skus || [];
    const selectedSku = skuList.find((x) => x.Id == item.SkuId);

    return (
      <>
        <Space direction={'horizontal'}>
          <Button.Group key={item.Id}>
            <Tooltip title="向前移动">
              <Button
                type={'dashed'}
                disabled={index <= 0}
                icon={<ArrowLeftOutlined />}
                onClick={() => {
                  const arr = [...goodsPictureList];
                  swapArrayPosition(arr, index - 1, index);
                  setGoodsPictureList(arr);
                }}
              ></Button>
            </Tooltip>
            <Tooltip title="向后移动">
              <Button
                type={'dashed'}
                disabled={index >= goodsPictureList.length - 1}
                icon={<ArrowRightOutlined />}
                onClick={() => {
                  const arr = [...goodsPictureList];
                  swapArrayPosition(arr, index, index + 1);
                  setGoodsPictureList(arr);
                }}
              ></Button>
            </Tooltip>
            <Tooltip title={'删除'}>
              <Button
                type={'dashed'}
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  const arr = [...goodsPictureList];
                  pullAt(arr, index);
                  setGoodsPictureList(arr);
                }}
              ></Button>
            </Tooltip>
          </Button.Group>
          <Tooltip title={'选择sku'}>
            <Select
              style={{
                minWidth: 100,
              }}
              placeholder={'选择sku'}
              value={selectedSku?.Id}
              onChange={(e) => {
                const arr = [...goodsPictureList];
                arr[index].SkuId = e;
                setGoodsPictureList(arr);
              }}
              onClear={() => {
                const arr = [...goodsPictureList];
                arr[index].SkuId = undefined;
                setGoodsPictureList(arr);
              }}
              allowClear
              options={skuList.map((x) => ({
                label: x.Name,
                value: x.Id,
              }))}
            />
          </Tooltip>
        </Space>
      </>
    );
  };

  const renderUploadButton = () => {
    if (pictureCountRemain <= 0) {
      return null;
    }
    return (
      <>
        <div
          style={{
            marginTop: 10,
            marginBottom: 10,
            padding: 10,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            justifyItems: 'flex-end',
          }}
        >
          <Button
            loading={!isArrayEmpty(uploadingList)}
            icon={<UploadOutlined />}
            type="dashed"
            onClick={() => {
              inputRef.current?.click();
            }}
          >
            选择图片
          </Button>
        </div>
      </>
    );
  };

  const renderPictureGrid = () => {
    return (
      <>
        <Row gutter={10}>
          {goodsPictureList.map((x, index) => {
            const picUrls = getPictureSrc(x.StorageMeta || {});

            return (
              <Col span={4} key={x.Id}>
                <div style={{ padding: 10 }}>
                  <Badge showZero count={(x.DisplayOrder || 0) + 1}>
                    <Image
                      src={picUrls.small || ''}
                      preview={{
                        src: picUrls.large || '',
                      }}
                      width={'100%'}
                      alt={`排序值：${x.DisplayOrder}`}
                    />
                  </Badge>
                  <div>{x.FileName || '--'}</div>
                </div>
                <div
                  style={{
                    padding: 10,
                  }}
                >
                  {renderActionButton(x, index)}
                </div>
              </Col>
            );
          })}
        </Row>
      </>
    );
  };

  const renderUploadingList = () => {
    if (isArrayEmpty(uploadingList)) {
      return null;
    }

    const doneCount = uploadingList.filter((x) => x.done).length;
    return (
      <>
        <Progress
          style={{
            marginBottom: 5,
          }}
          type={'line'}
          status={'active'}
          size={'small'}
          percent={parseInt(
            ((doneCount / uploadingList.length) * 100).toFixed(),
          )}
        />
        <div
          style={{
            margin: 10,
            padding: 10,
            backgroundColor: 'rgb(250,250,250)',
            border: '1px solid blue',
          }}
        >
          {uploadingList.map((x, i) => {
            return (
              <div key={i} style={{ marginBottom: 5 }}>
                <div>
                  {x.base64 && x.base64.length > 0 && (
                    <img width={80} height={80} src={x.base64} alt={''} />
                  )}
                  <b>{x.file?.name || '--'}</b>
                </div>
                <div>
                  {x.loading && <b>上传中...</b>}
                  {x.done && <b>完成</b>}
                  {isStringEmpty(x.error) || <b>{x.error}</b>}
                </div>
              </div>
            );
          })}
        </div>
      </>
    );
  };

  const handleUpload = async (
    currentUploadingList: UploadingList[],
  ): Promise<void> => {
    if (currentUploadingList.length > pictureCountRemain) {
      message.error(`还可以上传${pictureCountRemain}张图片，你选择的太多了`);
      return;
    }
    const uploadedList: GoodsPictureDto[] = [];
    _uploadingList((x) => [...currentUploadingList]);
    try {
      for (let i = 0; i < currentUploadingList.length; ++i) {
        const item = currentUploadingList[i];
        try {
          item.loading = true;
          _uploadingList((x) => [...currentUploadingList]);

          let file = item.file;
          if (!isImage(file.name)) {
            item.error = `只支持 ${allowedImageExtensions().join(
              '、',
            )} 格式的图片`;
            continue;
          }

          file = await compressImageV2(file, pictureMaxSize);
          if (file.size > pictureMaxSize) {
            item.error = '图片尺寸过大';
            continue;
          }

          let meta = await uploadFileV2(file);

          const pic: GoodsPictureDto = {
            GoodsId: undefined,
            SkuId: undefined,
            PictureMetaId: meta.Id,
            StorageMeta: meta,
            FileName: file.name,
          };

          uploadedList.push(pic);
        } finally {
          item.loading = false;
          item.done = true;
          _uploadingList((x) => [...currentUploadingList]);
        }
      }
    } finally {
      _uploadingList((x) => []);
      setGoodsPictureList([...goodsPictureList, ...uploadedList]);
    }
  };

  const renderUploadForm = () => {
    return (
      <>
        <div style={{ display: 'none' }}>
          <input
            ref={inputRef}
            type={'file'}
            accept="image/*"
            multiple={true}
            onChange={async (e) => {
              if (e.target.files == null) {
                return;
              }
              const currentUploadingList: UploadingList[] = [];
              for (let i = 0; i < e.target.files.length; ++i) {
                let f = e.target.files.item(i);
                if (f == null) {
                  continue;
                }
                currentUploadingList.push({
                  file: f,
                  loading: false,
                  done: false,
                  error: undefined,
                  base64: await fileAsBase64(f),
                });
              }
              await handleUpload(currentUploadingList);
            }}
          />
        </div>
      </>
    );
  };

  return (
    <>
      {renderUploadForm()}
      <Card title={title || '--'}>
        <div style={{ marginBottom: 20 }}>
          <Alert
            message={
              <>
                <div>{`还可以上传${pictureCountRemain}张图片`}</div>
              </>
            }
            type={'info'}
          ></Alert>
        </div>

        {renderPictureGrid()}
        {renderUploadingList()}
        {renderUploadButton()}
      </Card>
    </>
  );
};
