﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Payments;

namespace XCloud.Sales.Application.Mapping.Payments;

public class RefundBillMap : SalesEntityTypeConfiguration<RefundBill>
{
    public override void Configure(EntityTypeBuilder<RefundBill> builder)
    {
        builder.ToTable("sales_refund_bill");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.PaymentBillId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Price).HasPrecision(18, 2);

        builder.Property(x => x.Refunded);
        builder.Property(x => x.RefundTime);
        builder.Property(x => x.RefundTransactionId).HasMaxLength(200);
        builder.Property(x => x.RefundNotifyData);

        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}