using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Mapping.Shipping;

public class DeliveryRecordTrackMap : SalesEntityTypeConfiguration<DeliveryRecordTrack>
{
    public override void Configure(EntityTypeBuilder<DeliveryRecordTrack> builder)
    {
        builder.ToTable("sales_delivery_record_track");

        builder.PreConfigIntPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.DeliveryId).RequiredId();
        builder.Property(x => x.Lat);
        builder.Property(x => x.Lon);

        base.Configure(builder);
    }
}