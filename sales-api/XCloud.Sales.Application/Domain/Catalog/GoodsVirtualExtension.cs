namespace XCloud.Sales.Application.Domain.Catalog;

public class GoodsVirtualExtension : SalesBaseEntity<string>
{
    public string GoodsId { get; set; }

    public int? ExpiredAfterDays { get; set; }
}