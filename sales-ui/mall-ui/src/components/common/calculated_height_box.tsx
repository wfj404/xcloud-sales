import { useSize } from 'ahooks';
import { CSSProperties, ReactNode, useRef } from 'react';

export default ({
  rate,
  style,
  children,
}: {
  rate: number;
  children: ReactNode;
  style?: CSSProperties;
}) => {
  const div_ref = useRef<HTMLDivElement>(null);
  const size = useSize(div_ref);

  const width = size?.width || 0;

  return (
    <div
      ref={div_ref}
      style={{
        display: 'block',
        height: width * rate,
        ...style,
      }}
    >
      {children}
    </div>
  );
};
