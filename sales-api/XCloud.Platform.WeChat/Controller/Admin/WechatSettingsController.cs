using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Platform.WeChat.Controller.Admin;

[Route("/api/sys/wechat-settings")]
public class WechatSettingsController : PlatformBaseController
{
    private readonly ISettingService _settingService;

    public WechatSettingsController(ISettingService settingService)
    {
        _settingService = settingService;
    }

    [HttpPost("get-wechat-settings")]
    public async Task<ApiResponse<WechatSettings>> GetWechatSettingsAsync()
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var settings = new WechatSettings()
        {
            MpOption = await this._settingService.GetWechatMpOptionAsync(),
            OpenOption = await this._settingService.GetWechatOpenOptionAsync(),
            PaymentOption = await this._settingService.GetWechatPaymentOptionAsync()
        };

        return new ApiResponse<WechatSettings>(settings);
    }

    [HttpPost("save-wechat-settings")]
    public async Task<ApiResponse<object>> SaveWechatSettingsAsync([FromBody] WechatSettings dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        if (dto.MpOption != null)
        {
            await this._settingService.SaveSettingsAsync(dto.MpOption);
        }

        if (dto.OpenOption != null)
        {
            await this._settingService.SaveSettingsAsync(dto.OpenOption);
        }

        if (dto.PaymentOption != null)
        {
            await this._settingService.SaveSettingsAsync(dto.PaymentOption);
        }

        return new ApiResponse<object>();
    }
}