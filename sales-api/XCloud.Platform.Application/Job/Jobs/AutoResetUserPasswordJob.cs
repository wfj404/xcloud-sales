using System.IO;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Application.DistributedLock;
using XCloud.Platform.Application.Core;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Users;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Job.Jobs;

[UnitOfWork]
[ExposeServices(typeof(AutoResetUserPasswordJob))]
public class AutoResetUserPasswordJob : PlatformAppService
{
    private readonly IUserAccountService _userAccountService;
    private readonly AccountUtils _accountUtils;

    public AutoResetUserPasswordJob(IUserAccountService userAccountService, AccountUtils accountUtils)
    {
        _userAccountService = userAccountService;
        _accountUtils = accountUtils;
    }

    private async Task<string> ReadAndPruneUserPasswordFromDiskAsync()
    {
        var pwdPath = Configuration["app:config:passwordPath"];

        if (string.IsNullOrWhiteSpace(pwdPath))
            return null;

        if (!File.Exists(pwdPath))
            return null;

        var password = await File.ReadAllTextAsync(pwdPath);

        await File.WriteAllTextAsync(pwdPath, string.Empty);

        return password?.Trim().Trim('\n').Trim('\t').RemoveWhitespace();
    }

    private async Task TryUpdateUserPasswordAsync(string password)
    {
        var defaultUserName = AuthConstants.Account.DefaultUserName;

        var user = await _userAccountService.GetByIdentityNameAsync(defaultUserName);
        if (user == null)
            return;

        if (user.PassWord != _accountUtils.EncryptPassword(password))
        {
            await _userAccountService.SetPasswordAsync(user.Id, password);
        }
    }

    public virtual async Task ResetDefaultUserPasswordAsync()
    {
        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(ExternalAccessTokenJob)}.{nameof(ResetDefaultUserPasswordAsync)}",
            TimeSpan.FromSeconds(5));

        var password = await ReadAndPruneUserPasswordFromDiskAsync();
        if (!string.IsNullOrWhiteSpace(password))
        {
            await TryUpdateUserPasswordAsync(password);
        }
    }
}