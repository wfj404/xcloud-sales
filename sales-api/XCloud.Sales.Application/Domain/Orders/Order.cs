using XCloud.Application.Utils;
using XCloud.Core.Dto;

namespace XCloud.Sales.Application.Domain.Orders;

public interface ICustomerOrder : IFinder
{
    //
}

[TypeIdentityName("order")]
public class Order : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime, ICustomerOrder
{
    //base info
    public int GoodsType { get; set; }

    public string StoreId { get; set; }

    public string OrderSn { get; set; }

    public int UserId { get; set; }

    public string GradeId { get; set; }

    public string OrderStatusId { get; set; } = SalesConstants.OrderStatus.Pending;

    public DateTime? ServiceStartTime { get; set; }

    public DateTime? ServiceEndTime { get; set; }

    public DateTime? CompleteTime { get; set; }

    //shipping
    public string ShippingMethodId { get; set; }

    public string ShippingStatusId { get; set; } = SalesConstants.ShippingStatus.NotShipping;

    public string UserAddressId { get; set; }

    //payment

    /// <summary>
    /// online or offline
    /// </summary>
    public string PaymentMethodId { get; set; }

    public string PaymentStatusId { get; set; } = SalesConstants.PaymentStatus.Pending;

    public decimal PaidTotalPrice { get; set; }

    //refund
    public decimal RefundedAmount { get; set; }

    //price
    public decimal TotalPrice { get; set; }

    public decimal ItemsTotalPrice { get; set; }

    public decimal PackageFee { get; set; }

    public decimal ShippingFee { get; set; }

    public decimal SellerPriceOffset { get; set; }

    public decimal CouponPrice { get; set; }

    //promotion
    public string PromotionId { get; set; }

    public decimal PromotionPriceOffset { get; set; }

    //reward
    public int? RewardPointsHistoryId { get; set; }

    public decimal ExchangePointsAmount { get; set; }

    //other info
    public string UserInvoiceId { get; set; }

    public int AffiliateId { get; set; }

    public string Remark { get; set; }

    public string OrderIp { get; set; }

    public string PlatformId { get; set; }

    public bool HideForCustomer { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }

    public static implicit operator string(Order order) => order?.Id;
}