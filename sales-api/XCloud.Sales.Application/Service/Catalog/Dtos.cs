﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Common;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Service.Discounts;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Shipping.Delivery.Express;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Catalog;

public class ValueAddedItemGroupDto : ValueAddedItemGroup, IEntityDto
{
    public ValueAddedItemDto[] Items { get; set; }
}

public class ValueAddedItemDto : ValueAddedItem, IEntityDto
{
    //
}

public class QueryTagPaging : PagedRequest
{
    public string Name { get; set; }
}

public class ListByTagNameInput : NameDto
{
    public int? Count { get; set; }
}

public class QuerySkuBySelectedItemsDto : IEntityDto
{
    public string GoodsId { get; set; }

    public string StoreId { get; set; }

    public SpecItemDto[] Specs { get; set; }
}

public class SpecMenuGroupResponse : IEntityDto
{
    public SpecDto[] Specs { get; set; }
    public SkuDto[] FilteredSkuList { get; set; }
}

public class SetGoodsTagsInput : IEntityDto<string>
{
    public string Id { get; set; }
    public string[] TagIds { get; set; }
}

public class QuerySkuForSelectionInput : IEntityDto
{
    public int Take { get; set; }
    public string[] ExcludedSkuIds { get; set; }
    public string Keywords { get; set; }
}

public class SearchOptionsDto : IEntityDto
{
    public BrandDto Brand { get; set; }
    public CategoryDto Category { get; set; }
    public Tag Tag { get; set; }
}

public class QueryBrandPaging : PagedRequest
{
    public string Name { get; set; }
    public bool Published { get; set; }
    public bool ShowOnHomePage { get; set; }
}

public class BrandDto : Brand, IEntityDto<string>
{
    public StorageMetaDto Picture { get; set; }
}

public class CategoryDto : Category, IEntityDto<string>
{
    public CategoryDto[] ParentNodes { get; set; }

    public StorageMetaDto Picture { get; set; }
}

public class SpecValueDto : SpecValue, IEntityDto<int>
{
    public SpecDto Spec { get; set; }

    public SkuDto AssociatedSku { get; set; }

    public bool? Selected { get; set; }
    public bool? Available { get; set; }
}

public class SpecDto : Spec, IEntityDto<int>
{
    public SpecValueDto[] Values { get; set; }
}

public static class GoodsTypeEnum
{
    public static int Goods => 0;
    public static int Virtual => 1;
    public static int Service => 2;
}

public class GoodsRedirectToUrlDto : IEntityDto
{
    public string AppUrl { get; set; }
    public string H5Url { get; set; }
    public string WebUrl { get; set; }
    public string WxMiniProgramUrl { get; set; }
    public string AlipayMiniProgramUrl { get; set; }
}

public class QueryGoodsForSelection : PagedRequest
{
    public string Keyword { get; set; }
}

public class GoodsDto : Goods, IEntityDto<string>
{
    public GoodsRedirectToUrlDto RedirectToUrl { get; set; }

    public FreightTemplateDto CurrentFreightTemplate { get; set; }

    public ValueAddedItemDto[] ValueAddedItems { get; set; }

    public BrandDto Brand { get; set; }

    public CategoryDto Category { get; set; }

    public AreaDto Area { get; set; }

    public GoodsPictureDto[] GoodsPictures { get; set; }

    public TagDto[] Tags { get; set; }

    public SkuDto[] Skus { get; set; }
}

public class SpecItemDto : IEntityDto
{
    public SpecItemDto()
    {
        //
    }

    public SpecItemDto(KeyValuePair<int, int> keyValuePair) : this(keyValuePair.Key, keyValuePair.Value)
    {
        //
    }

    public SpecItemDto(int specId, int specValueId)
    {
        SpecId = specId;
        SpecValueId = specValueId;
    }

    public int SpecId { get; set; }
    public int SpecValueId { get; set; }

    public SpecDto Spec { get; set; }
    public SpecValueDto SpecValue { get; set; }

    public static implicit operator KeyValuePair<int, int>(SpecItemDto dto) =>
        new KeyValuePair<int, int>(dto.SpecId, dto.SpecValueId);

    public static implicit operator string(SpecItemDto dto) => $"{dto?.SpecId ?? 0}={dto?.SpecValueId ?? 0}";
}

public class GoodsPictureDto : GoodsPicture, IEntityDto<int>
{
    public StorageMetaDto StorageMeta { get; set; }
}

public class PriceCalculateInput : IEntityDto
{
    public PriceCalculateInput()
    {
        //
    }

    public PriceCalculateInput(PriceModel priceModel)
    {
        this.BasePrice = priceModel.BasePrice;
        this.DiscountRate = priceModel.DiscountModel?.DiscountPercentage ?? 0;
        this.GradePriceOffset = priceModel.GradePriceModel?.PriceOffset ?? decimal.Zero;
        this.ValueAddedPriceOffset = default;
    }

    public PriceCalculateInput(OrderItem orderItem)
    {
        this.BasePrice = orderItem.BasePrice;
        this.DiscountRate = orderItem.DiscountRate;
        this.GradePriceOffset = orderItem.GradePriceOffset;
        this.ValueAddedPriceOffset = orderItem.ValueAddedPriceOffset;
    }

    public decimal BasePrice { get; set; }

    public double DiscountRate { get; set; }

    public decimal GradePriceOffset { get; set; }

    public decimal ValueAddedPriceOffset { get; set; }
}

public class PriceModel : ExtensibleEntityDto
{
    public bool Empty { get; set; } = true;

    /// <summary>
    /// calculate price
    /// </summary>
    public decimal FinalPrice { get; set; }

    public decimal BasePrice { get; set; }

    public GoodsGradePriceDto GradePriceModel { get; set; }

    public DiscountDto DiscountModel { get; set; }
}

public class SkuDto : Sku, IEntityDto<string>
{
    public StockModel StockModel { get; set; }

    private PriceModel _priceModel;

    [NotNull]
    public PriceModel PriceModel
    {
        get => this._priceModel ??= new PriceModel();
        set => this._priceModel = value;
    }

    public StoreGoodsMappingDto[] StoreGoodsMapping { get; set; }

    public GoodsGradePriceDto[] GradePrices { get; set; }

    public DiscountDto[] Discounts { get; set; }

    public GoodsDto Goods { get; set; }

    public int? ShoppingCartQuantity { get; set; }

    public GoodsPictureDto[] GoodsPictures { get; set; }

    public SpecItemDto[] SpecItemList { get; set; }

    public string[] SpecCombinationErrors { get; set; }
}

public class TagDto : Tag, IEntityDto<string>
{
    //
}

public class StockModel : ExtensibleEntityDto
{
    public int StockQuantity { get; set; }

    public string StoreId { get; set; }

    public string Provider { get; set; }

    public string Description { get; set; }
}