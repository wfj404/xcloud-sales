import { useDebounceFn, useRequest } from 'ahooks';
import { Form, Input, InputNumber, Select } from 'antd';
import { useEffect, useState } from 'react';

import XBrandSelector from '@/components/manage/brand_selector';
import XCategorySelector from '@/components/manage/category_selector';
import { apiClient } from '@/utils/client';

import { HotGoodsBlockSearchParameter } from '../utils';

export default function ({
  data,
  onChange,
}: {
  data: HotGoodsBlockSearchParameter;
  onChange: (d: HotGoodsBlockSearchParameter) => void;
}) {
  const [kwd, _kwd] = useState<string | undefined>(data.keywords);

  const queryTag = useRequest(apiClient.mallAdmin.tagListTags, {
    manual: true,
  });

  const tagList = queryTag.data?.data?.Data || [];

  useEffect(() => {
    queryTag.run({});
  }, []);

  const setKwd = useDebounceFn(
    (e: string) => {
      onChange && onChange({ ...data, keywords: e });
    },
    {
      wait: 3000,
    },
  ).run;

  useEffect(() => {
    if (data.keywords != kwd) {
      setKwd(kwd || '');
    }
  }, [kwd]);

  return (
    <>
      <Form
        labelCol={{
          span: 3,
        }}
      >
        <Form.Item label={'关键词'}>
          <Input.Search
            maxLength={10}
            value={kwd}
            onChange={(e) => {
              _kwd(e.target.value);
            }}
          />
        </Form.Item>

        <Form.Item label={'品牌'}>
          <XBrandSelector
            value={data.brand?.Id}
            onChange={(e) => {
              onChange({
                ...data,
                brand: e,
              });
            }}
          />
        </Form.Item>

        <Form.Item label={'类目'}>
          <XCategorySelector
            value={data.category?.Id}
            onChange={(e) => {
              onChange({
                ...data,
                category: e,
              });
            }}
          />
        </Form.Item>

        <Form.Item label={'标签'}>
          <Select
            allowClear
            value={data.tag?.Id}
            options={tagList.map((x) => ({
              label: x.Name,
              value: x.Id,
            }))}
            onChange={(e) => {
              onChange({
                ...data,
                tag: tagList.find((x) => x.Id == e),
              });
            }}
          />
        </Form.Item>

        <Form.Item label={'数量'}>
          <InputNumber
            min={1}
            value={data?.count}
            onChange={(e) => {
              onChange && onChange({ ...data, count: e || undefined });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
}
