﻿using System;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nito.Disposables;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction.WebSocketImpl;

/// <summary>
/// 需要重写等号运算符
/// </summary>
public class WebsocketConnection : IConnection
{
    private readonly ILogger _logger;
    private readonly WebSocket _webSocket;
    private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

    public WebsocketConnection(IServiceProvider provider, IMessengerServer server, WebSocket webSocket)
    {
        Server = server;
        _webSocket = webSocket;
        _logger = provider.GetRequiredService<ILogger<WebsocketConnection>>();
    }

    public IMessengerServer Server { get; }

    private ClientIdentity _clientIdentity;

    public ClientIdentity ClientIdentity
    {
        get => this._clientIdentity ??= new ClientIdentity();
        set => this._clientIdentity = value;
    }

    public bool IsActive => _webSocket.IsOpen();

    public async Task SendMessageToClientAsync(MessageDto data)
    {
        if (data == null)
            throw new ArgumentNullException(nameof(data));

        var json = Server.MessageSerializer.SerializeToString(data);

        await this._webSocket.SendTextAsync(json, this.Server.Encoding, _cancellationTokenSource.Token);
    }

    public async Task<IAsyncDisposable> JoinServerAsync()
    {
        await this.Server.OnConnectedAsync(this);

        return new AsyncDisposable(async () => await Server.OnDisConnectedAsync(this));
    }

    public async Task StartBlockedMessageLoopAsync()
    {
        await _webSocket.StartReceiveLoopAsync(OnMessageFromClientAsync, cancellationToken: this.CancellationToken);

        async Task OnMessageFromClientAsync(byte[] bs)
        {
            try
            {
                var json = Server.Encoding.GetString(bs);
                var data = Server.MessageSerializer.DeserializeFromString<MessageDto>(json);
                if (data == null)
                {
                    this._logger.LogWarning($"{nameof(OnMessageFromClientAsync)}:null data");
                    return;
                }

                await Server.OnMessageFromClientAsync(data, this);
            }
            catch (Exception e)
            {
                _logger.LogError(message: e.Message, exception: e);
            }
        }
    }

    public CancellationToken CancellationToken => this._cancellationTokenSource.Token;

    public void RequestToClose()
    {
        this._cancellationTokenSource.Cancel();
    }

    public void Dispose()
    {
        this.RequestToClose();
        Task.Delay(TimeSpan.FromSeconds(2)).Wait();
        _cancellationTokenSource.Dispose();

        this._logger.LogInformation("connection disposed");
    }
}