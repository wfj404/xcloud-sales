import {
  Button,
  Card,
  Form,
  InputNumber,
  List,
  Modal,
  Space,
  Tag,
  TimePicker,
} from 'antd';
import { Dayjs } from 'dayjs';
import { pullAt } from 'lodash-es';
import { useEffect, useState } from 'react';

import { dateTimeFormat, hourMinuteFormat, myDayjs } from '@/utils/dayjs';
import { TimeRangeDto } from '@/utils/swagger';

const currentDate = (): Dayjs => myDayjs('00:00', hourMinuteFormat);

export default ({
  data,
  ok,
}: {
  data: TimeRangeDto[];
  ok: (d: TimeRangeDto[]) => void;
}) => {
  const [timeRange, _timeRange] = useState<TimeRangeDto | undefined>(undefined);

  useEffect(() => {
    console.log('time-range', timeRange || {});
  }, [timeRange]);

  return (
    <>
      <Modal
        title={'每天服务时间段'}
        open={timeRange != undefined}
        onOk={() => {
          let times = data || [];
          ok([...times, timeRange || {}]);
          _timeRange(undefined);
        }}
        onCancel={() => {
          _timeRange(undefined);
        }}
      >
        <Form labelCol={{ span: 4 }}>
          <Form.Item label={'时间范围'}>
            <TimePicker.RangePicker
              allowClear={false}
              format={hourMinuteFormat}
              value={[
                timeRange?.StartSecond == undefined
                  ? null
                  : currentDate().add(timeRange?.StartSecond || 0, 'second'),
                timeRange?.EndSecond == undefined
                  ? null
                  : currentDate().add(timeRange?.EndSecond || 0, 'second'),
              ]}
              onChange={(e) => {
                console.log(
                  e?.at(0)?.format(dateTimeFormat),
                  e?.at(1)?.format(dateTimeFormat),
                );
                const d = currentDate();
                _timeRange({
                  ...timeRange,
                  StartSecond: Math.abs(e?.at(0)?.diff(d, 'second') || 0),
                  EndSecond: Math.abs(e?.at(1)?.diff(d, 'second') || 0),
                });
              }}
            />
          </Form.Item>

          <Form.Item label={'可服务数量'}>
            <InputNumber
              changeOnWheel
              min={0}
              value={timeRange?.TotalTickets || 0}
              onChange={(e) => {
                _timeRange({
                  ...timeRange,
                  TotalTickets: e || undefined,
                });
              }}
            />
          </Form.Item>
        </Form>
      </Modal>

      <Card
        size="small"
        title={'每天服务时间段'}
        extra={
          <Button
            onClick={() => {
              _timeRange({});
            }}
            type={'link'}
          >
            添加
          </Button>
        }
      >
        <List
          dataSource={data}
          renderItem={(x: TimeRangeDto, i) => {
            return (
              <List.Item
                actions={[
                  <Button
                    type={'link'}
                    danger
                    onClick={() => {
                      let rules = data || [];
                      rules = [...rules];
                      pullAt(rules, i);
                      ok(rules);
                    }}
                  >
                    删除
                  </Button>,
                ]}
              >
                <List.Item.Meta
                  title={''}
                  description={
                    <div>
                      <Space direction={'horizontal'}>
                        <Tag>
                          {currentDate()
                            .add(x.StartSecond || 0, 'second')
                            .format(hourMinuteFormat) || '--'}
                        </Tag>
                        <b>到</b>
                        <Tag>
                          {currentDate()
                            .add(x.EndSecond || 0, 'second')
                            .format(hourMinuteFormat) || '--'}
                        </Tag>
                      </Space>
                    </div>
                  }
                ></List.Item.Meta>
              </List.Item>
            );
          }}
        ></List>
      </Card>
    </>
  );
};
