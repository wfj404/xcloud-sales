﻿using DotNetCore.CAP;
using Volo.Abp.Domain.Entities.Events.Distributed;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Application.Queue;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.Application.Queue;

public static class SalesEventTopics
{
    public const string OrderUpdated = "order.updated";

    public const string GoodsUpdated = "goods.updated";

    public const string ActivityLogInsert = "activity.log.insert";
}

[ExposeServices(typeof(SalesEventBusService))]
public class SalesEventBusService : SalesAppService
{
    private readonly ICapPublisher _capPublisher;

    public SalesEventBusService(ICapPublisher capPublisher)
    {
        _capPublisher = capPublisher;
    }

    public async Task NotifyOrderUpdatedAsync(string orderId)
    {
        await this._capPublisher.PublishEntityUpdatedAsync(
            SalesEventTopics.OrderUpdated,
            new Order() { Id = orderId });
    }

    public async Task NotifyRefreshGoodsInfo(Goods dto)
    {
        await this._capPublisher.PublishEntityUpdatedAsync(
            SalesEventTopics.GoodsUpdated, dto);
    }

    public async Task NotifyInsertActivityLog(ActivityLog log)
    {
        if (log == null)
            throw new ArgumentNullException(nameof(log));

        var activityLogService = LazyServiceProvider.LazyGetRequiredService<IActivityLogService>();

        log = await activityLogService.AttachHttpContextInfoAsync(log);

        await this._capPublisher.PublishEntityCreatedAsync(SalesEventTopics.ActivityLogInsert, log);
    }
}

[UnitOfWork]
public class SalesEventHandler : SalesAppService, ICapSubscribe
{
    private readonly IActivityLogService _activityLogService;
    private readonly IOrderReviewService _orderReviewService;
    private readonly GoodsViewService _goodsViewService;
    private readonly IOrderToGenericAdaptorService _orderToGenericAdaptorService;

    public SalesEventHandler(IActivityLogService activityLogService,
        IOrderReviewService orderReviewService,
        GoodsViewService goodsViewService,
        IOrderToGenericAdaptorService orderToGenericAdaptorService)
    {
        _activityLogService = activityLogService;
        _orderReviewService = orderReviewService;
        _goodsViewService = goodsViewService;
        _orderToGenericAdaptorService = orderToGenericAdaptorService;
    }

    [CapSubscribe(SalesEventTopics.ActivityLogInsert)]
    public async Task HandleActivityLogInsertAsync(EntityCreatedEto<ActivityLog> eventData)
    {
        var log = eventData.EnsureNotNull();

        log = await _activityLogService.TryResolveGeoAddressAsync(log);

        log = await _activityLogService.TryParseUserAgentAsync(log);

        await _activityLogService.InsertAsync(log);
    }

    [CapSubscribe(SalesEventTopics.GoodsUpdated)]
    public async Task HandleGoodsUpdatedAsync(EntityUpdatedEto<Goods> eventData)
    {
        var dto = eventData.EnsureNotNull();

        await _orderReviewService.UpdateGoodsReviewTotalsAsync(dto.Id);
        await _goodsViewService.QueryGoodsDetailByIdAsync(dto.Id, new CacheStrategy { Refresh = true });
    }

    [CapSubscribe(SalesEventTopics.OrderUpdated)]
    public async Task HandleOrderUpdatedAsync(EntityUpdatedEto<Order> eventData)
    {
        var id = eventData.EnsureNotNull().Id;

        await this._orderToGenericAdaptorService.CreateOrUpdateGenericOrderAsync(id);
    }
}