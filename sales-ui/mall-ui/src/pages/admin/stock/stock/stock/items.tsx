import { StockItemDto } from '@/utils/swagger';
import { ProColumns, ProTable } from '@ant-design/pro-components';

export default ({ data }: { data: StockItemDto[] }) => {

  const columns: ProColumns<StockItemDto>[] = [
    {
      title: '商品',
      render: (d, x: StockItemDto) => {
        return <span>{`${x.Goods?.Name || '--'}/${x.Sku?.Name || '--'}`}</span>;
      },
    },
    {
      title: '采购单价',
      render: (d, x: StockItemDto) => {
        return <span>{x.Price}</span>;
      },
    },
    {
      title: '采购数量',
      render: (d, x: StockItemDto) => {
        return <span>{x.Quantity}</span>;
      },
    },
  ];

  return (
    <>
      <ProTable
        options={false}
        search={false}
        bordered
        headerTitle={'库存批次明细'}
        columns={columns}
        dataSource={data || []}
        pagination={false}
      />
    </>
  );
};
