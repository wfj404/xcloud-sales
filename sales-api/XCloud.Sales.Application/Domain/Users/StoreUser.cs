namespace XCloud.Sales.Application.Domain.Users;

public class StoreUser : SalesBaseEntity, IHasModificationTime
{
    public StoreUser()
    {
        //
    }

    public string UserId { get; set; }

    public decimal Balance { get; set; }

    public int Points { get; set; }

    public int HistoryPoints { get; set; }

    public bool IsActive { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime LastActivityTime { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public static implicit operator int(StoreUser storeUser) => storeUser?.Id ?? default(int);
}