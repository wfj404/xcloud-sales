﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Sales.ThirdParty.Services.YiLianYun;

public static class YiLianYunConstant
{
    public static string PlatformId => "yi-lian-yun";
}

public class YiLianYunDeviceDto : IEntityDto
{
    public string MachineCode { get; set; }

    public string MSign { get; set; }

    public string PrinterName { get; set; }

    public string Phone { get; set; }
}

[TypeIdentityName("yi-lian-yun-config")]
public class YiLianYunConfigDto : ISettings
{
    public bool Enabled { get; set; }

    public string ClientId { get; set; }

    public string ClientSecret { get; set; }

    private YiLianYunDeviceDto[] _devices;

    [NotNull]
    public YiLianYunDeviceDto[] Devices
    {
        get => this._devices ??= [];
        set => this._devices = value;
    }

    public bool Available => this.Enabled &&
                             !string.IsNullOrWhiteSpace(this.ClientId) &&
                             !string.IsNullOrWhiteSpace(this.ClientSecret);
}