﻿using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/brand")]
public class BrandController : ShopBaseController
{
    private readonly BrandViewService _brandViewService;

    public BrandController(BrandViewService brandViewService)
    {
        _brandViewService = brandViewService;
    }

    [HttpPost("all")]
    public async Task<ApiResponse<BrandDto[]>> AllAsync()
    {
        var brands = await _brandViewService.QueryAllBrandsAsync(new CacheStrategy { Cache = true });

        return new ApiResponse<BrandDto[]>(brands);
    }

    [Obsolete]
    [HttpPost("list")]
    public async Task<ApiResponse<BrandDto[]>> ListAsync([FromBody] QueryBrandPaging dto)
    {
        var brands = await _brandViewService.QueryAllBrandsAsync(new CacheStrategy { Cache = true });

        return new ApiResponse<BrandDto[]>(brands);
    }
}