﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.SerialNos;

namespace XCloud.Platform.Application.Mapping.SerialNos;

public class SerialNoMap : PlatformEntityTypeConfiguration<SerialNo>
{
    public override void Configure(EntityTypeBuilder<SerialNo> builder)
    {
        builder.ToTable("sys_serial_no");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();
        
        builder.Property(x => x.Category).IsRequired().HasMaxLength(100);
        builder.Property(x => x.NextId).IsConcurrencyToken();

        builder.HasIndex(x => x.Category).IsUnique();
            
        base.Configure(builder);
    }
}