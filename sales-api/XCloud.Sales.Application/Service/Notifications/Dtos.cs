﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Notifications;

namespace XCloud.Sales.Application.Service.Notifications;

public class NotificationDto : Notification, IEntityDto<string>
{
    //
}

public class QueryNotificationPagingInput : PagedRequest
{
    public string AppKey { get; set; }
    public int UserId { get; set; }
}

public class UpdateNotificationStatusInput : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? Read { get; set; }
}