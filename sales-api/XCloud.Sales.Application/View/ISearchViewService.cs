using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Report;

namespace XCloud.Sales.Application.View;

[ExposeServices(typeof(SearchViewService))]
public class SearchViewService : SalesAppService
{
    private readonly IGoodsReportService _goodsReportService;
    private readonly ITagService _tagService;
    private readonly ISalesRepository<Brand> _repository;

    public SearchViewService(IGoodsReportService goodsReportService,
        ITagService tagService, ISalesRepository<Brand> repository)
    {
        _repository = repository;
        _tagService = tagService;
        _goodsReportService = goodsReportService;
    }

    private async Task<BrandDto[]> SuggestBrandsAsync()
    {
        var db = await _repository.GetDbContextAsync();

        var query = from goods in db.Set<Goods>().AsNoTracking()
            join brand in db.Set<Brand>().AsNoTracking()
                on goods.BrandId equals brand.Id
            select new { goods, brand };

        var groupedQuery = query.GroupBy(x => new
        {
            x.brand.Id
        }).Select(x => new
        {
            x.Key.Id,
            count = x.Count()
        });

        var data = await groupedQuery.OrderByDescending(x => x.count).Take(10).ToArrayAsync();

        if (!data.Any())
            return Array.Empty<BrandDto>();

        var ids = data.Select(x => x.Id).ToArray();
        var brands = await db.Set<Brand>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        var q = from d in data join b in brands on d.Id equals b.Id select new { b, d.count };

        var response = q.OrderByDescending(x => x.count).Select(x => ObjectMapper.Map<Brand, BrandDto>(x.b))
            .ToArray();

        return response;
    }

    private async Task<SearchView> QuerySearchViewAsync()
    {
        var keywords =
            await _goodsReportService.QuerySearchKeywordsReportAsync(
                new QuerySearchKeywordsReportInput { MaxCount = 5 });

        var dto = new SearchView
        {
            Keywords = keywords.Select(x => x.Keywords).ToArray(),
            Brands = await SuggestBrandsAsync(),
            Tags = await _tagService.QueryAllAsync()
        };

        return dto;
    }

    public async Task<SearchView> QuerySearchViewAsync(CacheStrategy cacheStrategy)
    {
        if (cacheStrategy == null)
            throw new ArgumentNullException(nameof(cacheStrategy));

        var key = $"mall.search.view.data";
        var option = new CacheOption<SearchView>(key, TimeSpan.FromMinutes(30));

        var data = await CacheProvider.ExecuteWithPolicyAsync(
            QuerySearchViewAsync,
            option,
            cacheStrategy);

        data = data ?? new SearchView();

        return data;
    }
}