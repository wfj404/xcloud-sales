using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Mapping;
using XCloud.Platform.Messenger.Domain.Messenger;

namespace XCloud.Platform.Messenger.Mapping.Messenger;

public class ServerInstanceMap : PlatformEntityTypeConfiguration<ServerInstance>
{
    public override void Configure(EntityTypeBuilder<ServerInstance> builder)
    {
        base.Configure(builder);

        builder.ToTable("sys_im_server_instance");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.InstanceId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.ConnectionCount);
        builder.Property(x => x.PingTime);
    }
}