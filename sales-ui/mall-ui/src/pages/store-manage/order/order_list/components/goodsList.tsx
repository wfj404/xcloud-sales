import { OrderItemDto } from '@/utils/swagger';
import { Divider, Image, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { formatMoney } from '@/utils/utils';
import { getSkuMainPictureUrl } from '@/utils/biz';
import EmptyUrl from '@/assets/empty.svg';

export default ({ items }: { items: OrderItemDto[] }) => {
  const columns: ColumnType<OrderItemDto>[] = [
    {
      title: '商品图片',
      render: (d, x: OrderItemDto) => {
        let url = getSkuMainPictureUrl(x.Sku || {}, {
          width: 200,
          height: 300,
        });

        if (!url) {
          url = EmptyUrl;
        }

        return (<Image
          preview={false}
          src={url}
          alt={''}
          width={100}
        />);
      },
    },
    {
      title: '商品规格',
      render: (d, x: OrderItemDto) => {
        return <>
          <Space size={'small'} direction={'horizontal'} split={<Divider type={'vertical'} />}>
            <span>{x.Sku?.Goods?.Name || '--'}</span>
            <Tag>{x.Sku?.Name || '--'}</Tag>
          </Space>
        </>;
      },
    },
    {
      title: '单价',
      render: (d, x: OrderItemDto) => <b>{formatMoney(x.UnitPrice || 0)}</b>,
    },
    {
      title: '数量',
      render: (d, x: OrderItemDto) => x.Quantity,
    },
    {
      title: '总计',
      render: (d, x: OrderItemDto) => <Tag color={'warning'}><b>{formatMoney(x.TotalPrice || 0)}</b></Tag>,
    },
  ];

  return (
    <>
      <Table columns={columns} dataSource={items} pagination={false} />
    </>
  );
};
