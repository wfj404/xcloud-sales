﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.Extension;
using XCloud.Sales.Application;
using XCloud.Sales.ThirdParty.Mapper;
using XCloud.Sales.ThirdParty.Services.YiLianYun;
using XCloud.Sales.Warehouses;

namespace XCloud.Sales.ThirdParty;

[DependsOn(
    typeof(SalesApplicationModule),
    typeof(SalesWarehouseApplicationModule)
)]
public class SalesThirdPartyModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.AddAutoMapperProfile<ThirdPartyAutoMapperConfiguration>();
        context.AddMvcPart<SalesThirdPartyModule>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var s = context.ServiceProvider.CreateScope();

        s.ServiceProvider.StartYiLianYunJobs();
    }
}