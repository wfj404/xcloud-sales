﻿namespace XCloud.Platform.Application.Domain.Users;

public class ExternalConnect : PlatformBaseEntity, IHasCreationTime
{
    public string UserId { get; set; }

    /// <summary>
    /// wechat-mini/wechat-mp/alipay/others
    /// </summary>
    public string Platform { get; set; }

    /// <summary>
    /// appid
    /// </summary>
    public string AppId { get; set; }

    /// <summary>
    /// oauth openid
    /// </summary>
    public string OpenId { get; set; }

    public DateTime CreationTime { get; set; }
}