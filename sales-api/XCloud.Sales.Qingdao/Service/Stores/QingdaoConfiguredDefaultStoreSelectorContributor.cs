﻿using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Qingdao.Configuration;

namespace XCloud.Sales.Qingdao.Service.Stores;

[DisableConventionalRegistration]
public class QingdaoConfiguredDefaultStoreSelectorContributor : SalesAppService, ICurrentStoreSelectorContributor
{
    private readonly IQingdaoStoreService _qingdaoStoreService;
    private readonly Sh3HClient _sh3HClient;

    public QingdaoConfiguredDefaultStoreSelectorContributor(Sh3HClient sh3HClient,
        IQingdaoStoreService qingdaoStoreService)
    {
        _sh3HClient = sh3HClient;
        _qingdaoStoreService = qingdaoStoreService;
    }

    public async Task<string> GetStoreIdOrEmptyAsync()
    {
        var config = this.Configuration.GetRequired3HConfig();

        var defaultOrgCode = config.StoreOrgCodeList?.FirstOrDefault();

        if (!string.IsNullOrWhiteSpace(defaultOrgCode))
        {
            var org = await this._sh3HClient.GetOrganizationByCodeAsync(defaultOrgCode);

            if (org != null)
            {
                var store = await this._qingdaoStoreService.PrepareStoreAsync(org);

                return store.Id;
            }
        }

        return default;
    }
}