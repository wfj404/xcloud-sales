﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Logging;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/common")]
public class CommonController : ShopBaseController
{
    private readonly IActivityLogService _activityLogService;
    private readonly ITagService _tagService;

    public CommonController(
        ITagService tagService,
        IActivityLogService activityLogService)
    {
        _tagService = tagService;
        _activityLogService = activityLogService;
    }

    [HttpPost("list-tags")]
    public virtual async Task<ApiResponse<TagDto[]>> ListTagsAsync()
    {
        var tags = await _tagService.QueryAllAsync();

        return new ApiResponse<TagDto[]>(tags);
    }

    [HttpPost("query-tag-by-id")]
    public async Task<ApiResponse<Tag>> QueryTagByIdAsync([FromBody] IdDto dto)
    {
        var tag = await _tagService.GetByIdAsync(dto.Id);

        return new ApiResponse<Tag>(tag);
    }

    [HttpPost("save-activity-log")]
    public async Task<ApiResponse<object>> SaveActivityLogAsync([FromBody] ActivityLog log)
    {
        var storeUser = await StoreUserAuthService.GetStoreUserOrNullAsync();
        if (storeUser != null && log != null)
        {
            log.StoreUserId = storeUser.Id;
            await SalesEventBusService.NotifyInsertActivityLog(log);
        }

        return new ApiResponse<object>();
    }
}