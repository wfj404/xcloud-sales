﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Common;

namespace XCloud.Platform.Application.Mapping.Common;

public class KeyStoreMap : PlatformEntityTypeConfiguration<KeyStore>
{
    public override void Configure(EntityTypeBuilder<KeyStore> builder)
    {
        builder.ToTable("sys_key_store");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.Key).IsRequired().HasMaxLength(200);
        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}