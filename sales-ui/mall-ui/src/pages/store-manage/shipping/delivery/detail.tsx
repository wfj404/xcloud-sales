import {
  Button,
  Descriptions,
  Form,
  Input,
  message,
  Modal,
  Result,
  Space,
  Spin,
  Table,
} from 'antd';
import { ColumnProps } from 'antd/es/table';
import React, { useEffect, useState } from 'react';

import XRenderTime from '@/components/common/time/render_time';
import TimeRange from '@/pages/store-manage/shipping/components/time_range';
import { DeliveryTypeList, getSkuName } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { DeliveryRecordDto, DeliveryRecordItemDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XStatus from '../components/delivery_status';

const XSummary = ({ data }: { data: DeliveryRecordDto }) => {
  return (
    <Descriptions bordered>
      <Descriptions.Item label="配送方式">
        {DeliveryTypeList.find((x) => x.id == data.DeliveryType)?.name || '--'}
      </Descriptions.Item>
      <Descriptions.Item label="物流名称">{data.ExpressName}</Descriptions.Item>
      <Descriptions.Item label="预计配送时间">
        <TimeRange
          start={data.PreferDeliveryStartTime}
          end={data.PreferDeliveryEndTime}
        />
      </Descriptions.Item>
      <Descriptions.Item label="配送状态">
        <XStatus data={data} />
      </Descriptions.Item>
      <Descriptions.Item label="发货时间">
        <XRenderTime timeStr={data.DeliveringTime} />
      </Descriptions.Item>
      <Descriptions.Item label="送达时间">
        <XRenderTime timeStr={data.DeliveredTime} />
      </Descriptions.Item>
      <Descriptions.Item label="创建时间" span={2}>
        <XRenderTime timeStr={data.CreationTime} />
      </Descriptions.Item>
    </Descriptions>
  );
};

const XItems = ({ data }: { data: DeliveryRecordDto }) => {
  const columns: ColumnProps<DeliveryRecordItemDto>[] = [
    {
      title: '商品',
      render: (d, x) => {
        let orderItem = data.Order?.Items?.find((m) => m.Id == x.OrderItemId);
        if (!orderItem) {
          return <span>--</span>;
        }
        return `${getSkuName(orderItem.Sku)}`;
      },
    },
    {
      title: '数量',
      render: (d, x) => x.Quantity || '--',
    },
  ];

  return (
    <Table columns={columns} dataSource={data.Items || []} pagination={false} />
  );
};

export default ({ detailId, hide }: { detailId: string; hide: () => void }) => {
  const [loading, _loading] = useState<boolean>(false);
  const [data, _data] = useState<DeliveryRecordDto>({});
  const show = !isStringEmpty(detailId);

  const [deliveringData, _deliveringData] = useState<
    DeliveryRecordDto | undefined
  >(undefined);
  const [loadingSaveDelivering, _loadingSaveDelivering] = useState(false);
  const [loadingDelivered, _loadingDelivered] = useState(false);

  const saveDelivered = (e: DeliveryRecordDto) => {
    if (!confirm('确认标记为已送达？')) {
      return;
    }
    _loadingDelivered(true);
    apiClient.mallManager
      .deliveryMarkAsDelivered({ Id: e.Id })
      .then((res) => {
        handleResponse(res, () => {
          message.success('操作成功');
          queryDetail(detailId);
        });
      })
      .finally(() => {
        _loadingDelivered(false);
      });
  };

  const saveDelivering = (e: DeliveryRecordDto) => {
    _loadingSaveDelivering(true);
    apiClient.mallManager
      .deliveryMarkAsDelivering({
        ...e,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('操作成功');
          _deliveringData(undefined);
          queryDetail(detailId);
        });
      })
      .finally(() => {
        _loadingSaveDelivering(false);
      });
  };

  const queryDetail = (id: string) => {
    if (isStringEmpty(id)) {
      _data({});
      return;
    }
    _loading(true);
    apiClient.mallManager
      .deliveryQueryById({ Id: id })
      .then((res) => {
        _data(res.data.Data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryDetail(detailId);
  }, [detailId]);

  const renderDeliveringButton = () => {
    if (data.DeliveringTime != null) {
      return null;
    }
    return (
      <Button.Group>
        <Button
          loading={loadingSaveDelivering}
          type={'primary'}
          onClick={() => {
            _deliveringData(data);
          }}
        >
          开始配送
        </Button>
      </Button.Group>
    );
  };

  const renderDeliveredButton = () => {
    if (data.DeliveringTime == null) {
      return null;
    }

    if (data.Delivered) {
      return null;
    }

    return (
      <Button.Group>
        <Button
          loading={loadingDelivered}
          type={'primary'}
          onClick={() => {
            saveDelivered(data);
          }}
        >
          已完成配送
        </Button>
      </Button.Group>
    );
  };

  const renderDeliveredMessage = () => {
    if (!data.Delivered) {
      return null;
    }

    return (
      <Result
        status="success"
        title="配送已经完成"
        subTitle={`配送单：${data.Id}已经于${formatRelativeTimeFromNow(
          data.DeliveredTime,
        )}完成配送`}
      />
    );
  };

  const renderContent = () => {
    if (isStringEmpty(data.Id)) {
      return <h1>data not found</h1>;
    }

    return (
      <>
        <XSummary data={data} />
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 20,
          }}
        >
          <Space direction={'horizontal'}>
            {renderDeliveringButton()}
            {renderDeliveredButton()}
            {renderDeliveredMessage()}
          </Space>
        </div>
        <XItems data={data} />
      </>
    );
  };

  return (
    <>
      <Modal
        title="发货"
        open={deliveringData != undefined}
        onCancel={() => {
          _deliveringData(undefined);
        }}
        onOk={() => {
          saveDelivering(deliveringData || {});
        }}
        okText="标记为已发货"
        confirmLoading={loadingSaveDelivering}
      >
        <h6
          style={{
            marginBottom: 10,
            display: 'block',
          }}
        >
          请填写物流信息
        </h6>
        <Form>
          <Form.Item label="物流名称">
            <Input
              placeholder="物流名称"
              value={deliveringData?.ExpressName || ''}
              onChange={(e) =>
                _deliveringData({
                  ...deliveringData,
                  ExpressName: e.target.value,
                })
              }
            />
          </Form.Item>
          <Form.Item label="物流单号">
            <Input
              placeholder="物流单号"
              value={deliveringData?.TrackingNumber || ''}
              onChange={(e) =>
                _deliveringData({
                  ...deliveringData,
                  TrackingNumber: e.target.value,
                })
              }
            />
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        width={'90%'}
        title={'配送单'}
        open={show}
        footer={false}
        onCancel={() => {
          hide();
        }}
        onOk={() => {}}
      >
        <Spin spinning={loading}>{renderContent()}</Spin>
      </Modal>
    </>
  );
};
