import { useRequest } from 'ahooks';
import { Col, Row, Spin, Tree } from 'antd';
import { useEffect, useState } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { getAntDesignTreeNodeData, getFlatNodes } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  AntDesignTreeNode1DepartmentDto,
  DepartmentDto,
} from '@/utils/swagger';

import XAdminList from './admin';
import XDepartment from './department';

const empty_id = '00000000000000.0000000000000000000';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  const getTreeRequest = useRequest(apiClient.sys.departmentDepartmentTree, {
    manual: true,
  });

  const [selectedId, _selectedId] = useState<string | undefined>(undefined);
  const [expandedKeys, _expandedKeys] = useState<string[]>([]);

  useEffect(() => {
    getTreeRequest.run({});
  }, []);

  const data = getTreeRequest.data?.data?.Data || [];

  const tree_data: AntDesignTreeNode1DepartmentDto[] = [
    {
      key: empty_id,
      raw_data: undefined,
      title: app.getAppName() || '组织架构',
      children: data,
    },
  ];

  useEffect(() => {
    const keys = tree_data
      .flatMap((x) => getFlatNodes(x))
      .map((x) => x.key || '');
    _expandedKeys(keys);
  }, [data]);

  const flat_tree_data = tree_data.flatMap(getFlatNodes);

  const selected_department = flat_tree_data.find((e) => e.key === selectedId);

  const ant_design_tree_node_data = tree_data.map((x) =>
    getAntDesignTreeNodeData(x, (d) => {
      const dept: DepartmentDto = d.raw_data;

      if (d.key == empty_id || !dept || (dept.MemberCount || 0) <= 0) {
        return d.title || '';
      }
      return `${d.title} (${dept.MemberCount})`;
    }),
  );

  return (
    <>
      <Row gutter={[10, 10]}>
        <Col span={6}>
          <Spin spinning={getTreeRequest.loading}>
            <Tree
              showIcon
              showLine
              treeData={ant_design_tree_node_data}
              blockNode
              onSelect={(e) => {
                console.log(e);
                _selectedId(e.at(0) as string);
              }}
              expandedKeys={expandedKeys}
              onExpand={(e) => {
                _expandedKeys(e as string[]);
              }}
              rootStyle={{
                background: 'none',
              }}
            />
          </Spin>
        </Col>
        <Col span={18}>
          <XDepartment
            node={selected_department}
            ok={() => {
              getTreeRequest.run({});
            }}
          />
          <XAdminList
            department={selected_department?.raw_data}
            ok={() => {
              getTreeRequest.run({});
            }}
          />
        </Col>
      </Row>
    </>
  );
};
