import { useDebounceFn } from 'ahooks';
import { Select, SelectProps, Spin } from 'antd';
import { CSSProperties, useEffect, useState } from 'react';

export interface SearchSelectorItem {
  label?: string;
  value: string;
}

export default ({
  onSearch,
  value,
  onChange,
  style,
  initItem,
  placeholder,
  props,
}: {
  onSearch: (keyword: string) => Promise<SearchSelectorItem[]>;
  value?: string;
  initItem?: SearchSelectorItem;
  onChange?: (d?: SearchSelectorItem) => void;
  placeholder?: string;
  style?: CSSProperties;
  props?: SelectProps;
}) => {
  const [loading, _loading] = useState(false);
  const [options, _options] = useState<SearchSelectorItem[]>([]);

  const onSearchFunction = useDebounceFn(
    (kwd: string) => {
      _loading(true);
      onSearch(kwd)
        .then((res) => {
          _options(res || []);
        })
        .finally(() => _loading(false));
    },
    {
      wait: 500,
    },
  );

  useEffect(() => {
    //
  }, []);

  const getOptions = (): SearchSelectorItem[] => {
    const data: SearchSelectorItem[] = [...options];

    if (initItem && !data.find((d) => d.value === initItem.value)) {
      data.push(initItem);
    }

    return data;
  };

  return (
    <>
      <Select
        style={{
          minWidth: 300,
          ...style,
        }}
        loading={loading}
        value={value}
        showSearch
        allowClear
        filterOption={false}
        onSearch={(e) => {
          onSearchFunction.run(e);
        }}
        onChange={(e) => {
          const selectedItem = options.find((x) => x.value == e);
          onChange && onChange(selectedItem);
        }}
        onClear={() => {
          onChange && onChange(undefined);
        }}
        placeholder={placeholder || '输入关键词'}
        notFoundContent={loading && <Spin size="small" />}
        options={getOptions()}
        {...props}
      ></Select>
    </>
  );
};
