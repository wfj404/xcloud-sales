import { apiClient } from '@/utils/client';
import { PagedResponse } from '@/utils/biz';
import { PickupRecordDto, QueryPickupPagingInput } from '@/utils/swagger';
import { Card, Space, Switch } from 'antd';
import { useEffect, useState } from 'react';
import XPickupList from './list';
import { handleResponse } from '@/utils/utils';

export default () => {
  const [loading, _loading] = useState(true);
  const [query, _query] = useState<QueryPickupPagingInput>({
    OrderToService: true,
  });
  const [data, _data] = useState<PagedResponse<PickupRecordDto>>({});

  const queryList = (q: QueryPickupPagingInput) => {
    _loading(true);
    apiClient.mallManager
      .pickupQueryPaging({ ...q })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <Card
        loading={loading}
        title='自提'
        extra={
          <Space direction={'horizontal'}>
            <Switch checked={query.OrderToService || false} onChange={e => {
              const q: QueryPickupPagingInput = {
                ...query,
                Page: 1,
                OrderToService: e,
              };
              _query(q);
              queryList(q);
            }} />
            <span>只展示待服务订单相关自提</span>
          </Space>
        }>
        <XPickupList
          data={data.Items || []}
          pagination={{
            total: data.TotalCount,
            pageSize: data.PageSize,
            current: query.Page,
            onChange: (e) => {
              const q: QueryPickupPagingInput = { ...query, Page: e };
              _query(q);
              queryList(q);
            },
          }}
          refresh={() => {
            queryList({ ...query });
          }}
        />
      </Card>
    </>
  );
};
