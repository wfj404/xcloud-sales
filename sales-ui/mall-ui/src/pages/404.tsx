import XLayout from './test_page/grid_layout';
import XWS from './test_page/ws';

export default function App() {
  return (
    <>
      <XWS />
      <div
        style={{
          padding: 20,
        }}
      >
        <h1>404 NOT FOUND .</h1>
      </div>
      <div
        style={{
          height: 500,
        }}
      >
        <XLayout />
      </div>
    </>
  );
}
