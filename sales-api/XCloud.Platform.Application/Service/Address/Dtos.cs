using System.Collections.Generic;
using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Platform.Application.Domain.Address;

namespace XCloud.Platform.Application.Service.Address;

public class UserAddressDto : UserAddress, IEntityDto<string>
{
    public UserAddressDto()
    {
        this.Distance = default;
    }

    public double? Distance { get; set; }

    private List<string> _errorList;

    [NotNull]
    public List<string> ErrorList
    {
        get => this._errorList ??= new List<string>();
        set => this._errorList = value;
    }
}