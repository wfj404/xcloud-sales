using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Mapping;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreMappingService : ISalesAppService
{
    IQueryable<T> ApplyStoreNotMappingFilter<T>(DbContext db, IQueryable<T> query,
        string[] storeIds)
        where T : class, IStoreNotMappingSupported;

    Task SaveStoreNotMappingAsync<T>(T entity, string[] storeIds)
        where T : class, IStoreNotMappingSupported;

    public IQueryable<T> ApplyStoreMappingFilter<T>(DbContext db, IQueryable<T> query,
        string[] storeIds)
        where T : class, IStoreMappingSupported;

    Task SaveStoreMappingAsync<T>(T entity, string[] storeIds)
        where T : class, IStoreMappingSupported;
}

public class StoreMappingService : SalesAppService, IStoreMappingService
{
    private readonly ISalesRepository<CommonEntityMapping> _repository;

    public StoreMappingService(ISalesRepository<CommonEntityMapping> repository)
    {
        _repository = repository;
    }

    public IQueryable<T> ApplyStoreNotMappingFilter<T>(DbContext db, IQueryable<T> query,
        string[] storeIds)
        where T : class, IStoreNotMappingSupported
    {
        if (storeIds == null)
            throw new ArgumentNullException(nameof(storeIds));

        var mappingQuery = db.Set<CommonEntityNotMapping>().AsNoTracking()
            .WhereMappingFromTo<CommonEntityNotMapping, T, Store>(this.CommonUtils);

        if (storeIds.Length == 1)
        {
            var id = storeIds.First();
            mappingQuery = mappingQuery.Where(x => x.TargetId == id);
        }
        else
        {
            mappingQuery = mappingQuery.Where(x => storeIds.Contains(x.TargetId));
        }

        return query.Where(x => !x.ExcludeStore || !mappingQuery.Any(d => d.EntityId == x.Id));
    }

    public IQueryable<T> ApplyStoreMappingFilter<T>(DbContext db, IQueryable<T> query,
        string[] storeIds)
        where T : class, IStoreMappingSupported
    {
        if (storeIds == null)
            throw new ArgumentNullException(nameof(storeIds));

        var mappingQuery = db.Set<CommonEntityMapping>().AsNoTracking()
            .WhereMappingFromTo<CommonEntityMapping, T, Store>(this.CommonUtils);

        if (storeIds.Length == 1)
        {
            var id = storeIds.First();
            mappingQuery = mappingQuery.Where(x => x.TargetId == id);
        }
        else
        {
            mappingQuery = mappingQuery.Where(x => storeIds.Contains(x.TargetId));
        }

        return query.Where(x => !x.LimitedToStore || mappingQuery.Any(d => d.EntityId == x.Id));
    }

    public async Task SaveStoreNotMappingAsync<T>(T entity, string[] storeIds)
        where T : class, IStoreNotMappingSupported
    {
        var db = await _repository.GetDbContextAsync();

        await db.SaveMappingEntitiesAsync<T, Store, CommonEntityNotMapping>(entity, storeIds, this.CommonUtils);
    }

    public async Task SaveStoreMappingAsync<T>(T entity, string[] storeIds)
        where T : class, IStoreMappingSupported
    {
        var db = await _repository.GetDbContextAsync();

        await db.SaveMappingEntitiesAsync<T, Store, CommonEntityMapping>(entity, storeIds, this.CommonUtils);
    }
}