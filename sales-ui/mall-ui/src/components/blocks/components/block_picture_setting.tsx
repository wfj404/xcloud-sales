import { InputNumber, Space } from 'antd';

import { BlockPictureProps } from '@/components/blocks/utils';
import { resolveUrl } from '@/utils/storage';
import { isStringEmpty } from '@/utils/utils';

import XBlockPicture from './block_picture';

export default ({
  data,
  onChange,
}: {
  data?: BlockPictureProps;
  onChange: (x: BlockPictureProps) => void;
}) => {
  const url = resolveUrl(data?.storage, {
    width: data?.width,
    height: data?.height,
  });

  if (!data || !url || isStringEmpty(url)) {
    return <span>无图片</span>;
  }

  return (
    <>
      <Space direction={'vertical'} style={{ width: '100%' }}>
        <XBlockPicture data={data} />
        <div style={{}}>
          <Space direction={'vertical'}>
            <InputNumber
              addonBefore={'宽'}
              size={'small'}
              value={data.width}
              onChange={(e) => {
                const block_picture: BlockPictureProps = {
                  ...data,
                  width: e || undefined,
                };
                onChange(block_picture);
              }}
            />
            <InputNumber
              addonBefore={'h/w'}
              size={'small'}
              value={data.rate}
              onChange={(e) => {
                const block_picture: BlockPictureProps = {
                  ...data,
                  rate: e || undefined,
                };
                onChange(block_picture);
              }}
            />
          </Space>
        </div>
      </Space>
    </>
  );
};
