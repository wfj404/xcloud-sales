using XCloud.Application.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/admin-auth")]
public class AdminAuthController : PlatformBaseController
{
    private readonly IAdminService _adminService;

    public AdminAuthController(IAdminService adminService)
    {
        _adminService = adminService;
    }

    [HttpPost("auth")]
    public async Task<AdminAuthResult> AdminAuthAsync()
    {
        var res = await AdminAuthService.GetAuthAdminAsync();

        res.Data ??= new AdminDto();

        if (!res.Data.IsEmptyId())
        {
            await this._adminService.AttachUsersAsync(new[] { res.Data });
        }

        res.Data.User ??= new UserDto();

        return res;
    }
}