import 'leaflet/dist/leaflet.css';

import L, { Marker } from 'leaflet';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import { useEffect } from 'react';
import {
  MapContainer,
  Marker as XMarker,
  Popup,
  TileLayer,
  useMapEvents,
} from 'react-leaflet';

import { isStringEmpty } from '@/utils/utils';

const tile_server =
  'https://webrd04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=7&x={x}&y={y}&z={z}';

const wuxi: L.LatLngExpression = {
  lat: 31.456489605661922,
  lng: 120.27968627233334,
};

const ClickEvent = ({
  onClickPositionChange,
}: {
  onClickPositionChange: (e: L.LatLng) => void;
}) => {
  const map = useMapEvents({
    click: (e) => {
      onClickPositionChange(e.latlng);
    },
  });
  return <></>;
};

export default ({
  marker_position,
  onMarkerPositionChange,
  default_center,
  marker_title,
}: {
  marker_position?: L.LatLngExpression;
  onMarkerPositionChange?: (e: L.LatLng) => void;
  default_center?: L.LatLngExpression;
  marker_title?: string;
}) => {
  useEffect(() => {
    console.log('maker-position', marker_position);
  }, [marker_position]);

  const setMarkerPosition = (e: L.LatLng) => {
    onMarkerPositionChange && onMarkerPositionChange(e);
  };

  return (
    <>
      <MapContainer
        center={default_center || marker_position || wuxi}
        zoom={13}
        style={{ height: '600px', width: '100%' }}
      >
        <ClickEvent
          onClickPositionChange={(e) => {
            //setMarkerPosition(e);
          }}
        />
        <TileLayer url={tile_server} />
        <XMarker
          draggable
          position={marker_position || default_center || wuxi}
          eventHandlers={{
            dragend: (e) => {
              const latlng = (e.target as Marker).getLatLng();
              console.log('drag end:', e, latlng);
              setMarkerPosition(latlng);
            },
          }}
          icon={L.icon({
            iconUrl: icon,
            shadowUrl: iconShadow,
          })}
        >
          {isStringEmpty(marker_title) || <Popup>{marker_title}</Popup>}
        </XMarker>
      </MapContainer>
    </>
  );
};
