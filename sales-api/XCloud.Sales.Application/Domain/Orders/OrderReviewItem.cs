﻿namespace XCloud.Sales.Application.Domain.Orders;

public class OrderReviewItem : SalesBaseEntity<string>
{
    public string ReviewId { get; set; }

    public string OrderItemId { get; set; }

    public string ReviewText { get; set; }

    public string PictureMetaIdsJson { get; set; }

    public double Rating { get; set; }
}