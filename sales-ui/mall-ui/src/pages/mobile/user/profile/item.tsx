import { ReactNode } from 'react';

import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { Box, CardActionArea, Divider, Typography } from '@mui/material';

export default function ({
  title,
  right,
}: {
  title?: ReactNode;
  right?: ReactNode;
}) {
  return (
    <>
      <CardActionArea sx={{}}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
            p: 2,
          }}
        >
          <Box sx={{}} flexGrow={1}>
            <Typography variant="button">{title || ''}</Typography>
          </Box>
          <Box sx={{}}>{right}</Box>
          <ChevronRightIcon sx={{ color: 'gray', ml: 1 }} />
        </Box>
        <Divider />
      </CardActionArea>
    </>
  );
}
