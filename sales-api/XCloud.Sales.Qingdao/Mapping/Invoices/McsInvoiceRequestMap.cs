﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Sales.Application.Mapping;
using XCloud.Sales.Qingdao.Domain.Invoices;

namespace XCloud.Sales.Qingdao.Mapping.Invoices;

public class McsInvoiceRequestMap : SalesEntityTypeConfiguration<McsInvoiceRequest>
{
    public override void Configure(EntityTypeBuilder<McsInvoiceRequest> builder)
    {
        builder.ToTable("mcs_invoice_request");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigAbpSoftDeleteEntity();

        builder.Property(x => x.BizType).IsRequired().HasMaxLength(100);
        builder.Property(x => x.TotalAmount).PricePrecision();
        builder.Property(x => x.Status).IsRequired().HasMaxLength(100);
        builder.Property(x => x.UserInvoiceId).RequiredId();
        builder.Property(x => x.UserInvoiceData);
        builder.Property(x => x.InvoicingTime);
        builder.Property(x => x.Email).HasMaxLength(100);
        builder.Property(x => x.Picked);
        builder.Property(x => x.PickedTime);

        builder.Property(x => x.McsUserId).RequiredId();
        builder.Property(x => x.PickupLocationId);
        builder.Property(x => x.PickupLocationName).HasMaxLength(200);

        builder.Property(x => x.AttachmentMetaId).OptionalId();

        base.Configure(builder);
    }
}