using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class GoodsMap : SalesEntityTypeConfiguration<Goods>
{
    public override void Configure(EntityTypeBuilder<Goods> builder)
    {
        builder.ToTable("sales_goods");

        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.GoodsType).HasDefaultValue(GoodsTypeEnum.Goods);
        builder.Property(x => x.StockDeductStrategy).RequiredId();
        builder.Property(x => x.PayAfterDeliveredSupported);
        builder.Property(x => x.LimitedToRedirect);
        builder.Property(x => x.RedirectToUrlJson);
        builder.Property(x => x.LimitedToContact);
        builder.Property(x => x.ContactTelephone).HasMaxLength(50);
        builder.Property(p => p.Name).IsRequired().HasMaxLength(400);
        builder.Property(p => p.SeoName).HasMaxLength(500);
        builder.Property(p => p.Description).HasMaxLength(1000);
        builder.Property(p => p.DetailInformation);
        builder.Property(p => p.AdminComment).HasMaxLength(500);

        builder.Property(x => x.Keywords).HasMaxLength(1000);

        builder.Property(x => x.StickyTop);

        builder.Property(x => x.CategoryId).HasMaxLength(100);
        builder.Property(x => x.BrandId).HasMaxLength(100);
        builder.Property(x => x.AreaId).HasMaxLength(100);
        builder.Property(x => x.ApprovedReviewsRates);
        builder.Property(x => x.ApprovedTotalReviews);

        builder.Property(x => x.DisplayAsInformationPage);
        builder.Property(x => x.AfterSalesSupported);
        builder.Property(x => x.CommentSupported);
        builder.Property(x => x.PlaceOrderSupported);

        builder.Property(x => x.Published);
        builder.Property(x => x.IsDeleted);
        builder.Property(x => x.CreationTime);
        builder.Property(x => x.LastModificationTime);

        base.Configure(builder);
    }
}