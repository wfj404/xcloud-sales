﻿using XCloud.Core.Dto;
using XCloud.Platform.Application.Framework;

namespace XCloud.Platform.HttpApi.InternalControllers;

[Route("/internal-api/platform/common")]
public class InternalCommonController : PlatformBaseController
{
    public InternalCommonController()
    {
        //
    }

    [HttpPost("test")]
    public async Task<ApiResponse<object>> TestAsync()
    {
        await Task.CompletedTask;
        return new ApiResponse<object>();
    }
}