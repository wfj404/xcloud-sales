import { Alert, Button, Form, Input, Modal, Select } from 'antd';
import React, { useEffect, useState } from 'react';

import {
  OrderStatusList,
  PaymentStatusList,
  ShippingStatusList,
} from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { WarningOutlined } from '@ant-design/icons';

export default function (props: { model: OrderDto; ok: () => void }) {
  const { model, ok } = props;

  const [show, _show] = useState<boolean>(false);

  const hide = () => _show(false);

  const [loadingSave, _loadingSave] = React.useState(false);

  const [comment, _comment] = React.useState('');

  const [form, _form] = React.useState<OrderDto>({});

  React.useEffect(() => {
    model &&
      _form({
        ...model,
      });
  }, [model]);

  const save = () => {
    _loadingSave(true);
    apiClient.mallManager
      .orderDangerouslyUpdateStatus({
        Id: model.Id,
        OrderStatus: form.OrderStatusId,
        PaymentStatus: form.PaymentStatusId,
        DeliveryStatus: form.ShippingStatusId,
      })
      .then((res) => {
        handleResponse(res, () => {
          hide && hide();
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  return (
    <>
      <Button.Group>
        <Button
          icon={<WarningOutlined />}
          danger
          type={'primary'}
          onClick={() => {
            _show(true);
          }}
        >
          强制修改状态
        </Button>
      </Button.Group>
      <Modal
        open={show}
        title="⚠️强制修改状态"
        onCancel={() => {
          hide && hide();
        }}
        onOk={() => {
          save();
        }}
        okText="危险：强制修改状态"
        confirmLoading={loadingSave}
      >
        <Alert
          message="错误的修改状态可能造成数据紊乱，请谨慎操作！"
          type="warning"
          style={{ marginBottom: 10 }}
        />

        <Form>
          <Form.Item label={'修改理由'}>
            <Input.TextArea
              autoFocus
              placeholder="修改理由"
              value={comment}
              onChange={(e) => _comment(e.target.value)}
            />
          </Form.Item>
          <Form.Item label={'订单状态'}>
            <Select
              value={form.OrderStatusId}
              onChange={(e) => {
                _form({
                  ...form,
                  OrderStatusId: e,
                });
              }}
              placeholder="订单状态"
              options={OrderStatusList.map((x) => ({
                label: x.name,
                value: x.id,
                key: x.id,
              }))}
            ></Select>
          </Form.Item>
          <Form.Item label={'支付状态'}>
            <Select
              value={form.PaymentStatusId}
              onChange={(e) => {
                _form({
                  ...form,
                  PaymentStatusId: e,
                });
              }}
              placeholder="支付状态"
              options={PaymentStatusList.map((x) => ({
                label: x.name,
                value: x.id,
                key: x.id,
              }))}
            ></Select>
          </Form.Item>
          <Form.Item label={'交付状态'}>
            <Select
              value={form.ShippingStatusId}
              onChange={(e) => {
                _form({
                  ...form,
                  ShippingStatusId: e,
                });
              }}
              placeholder="交付状态"
              options={ShippingStatusList.map((x) => ({
                label: x.name,
                value: x.id,
                key: x.id,
              }))}
            ></Select>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
}
