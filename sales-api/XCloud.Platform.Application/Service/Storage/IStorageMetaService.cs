﻿using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Service.Storage;

public interface IStorageMetaService : IXCloudAppService
{
    Task<StorageMetaDto[]> ByIdsAsync(string[] ids);

    Task<string> GetContentTypeOrNullAsync(string key);

    Task<StorageMeta> SaveOrUpdateResourceMetaAsync(StorageMeta model);

    Task<StorageMeta> GetFileByKeyAsync(string fileKey);

    Task<StorageMeta> GetFileByKeyAsync(string fileKey, CacheStrategy option);

    Task<PagedResponse<StorageMetaDto>> QueryPagingAsync(QueryStoragePaging dto);
}

public class StorageMetaService : PlatformAppService, IStorageMetaService
{
    private readonly IPlatformRepository<StorageMeta> _uploadRepo;
    private readonly StorageUtils _storageHelper;

    public StorageMetaService(
        IPlatformRepository<StorageMeta> uploadRepo,
        StorageUtils storageHelper)
    {
        _uploadRepo = uploadRepo;
        _storageHelper = storageHelper;
    }

    public async Task<StorageMetaDto[]> ByIdsAsync(string[] ids)
    {
        if (ids == null)
            throw new ArgumentNullException(nameof(ids));

        if (!ids.Any())
            return Array.Empty<StorageMetaDto>();

        var data = await this._uploadRepo.QueryManyAsync(x => ids.Contains(x.Id));

        return data.MapArrayTo<StorageMeta, StorageMetaDto>(ObjectMapper);
    }

    public async Task<PagedResponse<StorageMetaDto>> QueryPagingAsync(QueryStoragePaging dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _uploadRepo.GetDbContextAsync();

        var query = db.Set<StorageMeta>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.FileExtension))
        {
            var ext = _storageHelper.GetNormalizedFileExtension(dto.FileExtension);
            query = query.Where(x => x.FileExtension == ext);
        }

        if (!string.IsNullOrWhiteSpace(dto.ContentType))
            query = query.Where(x => x.ContentType == dto.ContentType);

        if (!string.IsNullOrWhiteSpace(dto.StorageProvider))
            query = query.Where(x => x.StorageProvider == dto.StorageProvider);

        if (dto.MinSize != null)
            query = query.Where(x => x.ResourceSize >= dto.MinSize.Value);

        if (dto.MaxSize != null)
            query = query.Where(x => x.ResourceSize <= dto.MaxSize.Value);

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime <= dto.EndTime.Value);

        var count = await query.CountOrDefaultAsync(dto);

        var data = await query
            .OrderByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = data.Select(x => ObjectMapper.Map<StorageMeta, StorageMetaDto>(x)).ToArray();

        return new PagedResponse<StorageMetaDto>(items, dto, count);
    }

    public async Task<string> GetContentTypeOrNullAsync(string key)
    {
        var contentType = _storageHelper.ResolveContentTypeOrNull(key);

        if (string.IsNullOrWhiteSpace(contentType))
        {
            var res = await GetFileByKeyAsync(key, new CacheStrategy { Cache = true });
            if (res != null)
                contentType = res.ContentType;
        }

        return contentType;
    }

    public async Task<StorageMeta> SaveOrUpdateResourceMetaAsync(StorageMeta model)
    {
        var previousUploaded = await GetPreviousUploadedResource(model);

        if (previousUploaded != null)
        {
            await PlusResourceMetaUploadTimes(previousUploaded);

            await GetFileByKeyAsync(previousUploaded.ResourceKey, new CacheStrategy { Refresh = true });

            return previousUploaded;
        }
        else
        {
            await InsertResourceMeta(model);

            await GetFileByKeyAsync(model.ResourceKey, new CacheStrategy { Refresh = true });

            return model;
        }
    }

    async Task<StorageMeta> GetPreviousUploadedResource(StorageMeta model)
    {
        var previousUploaded = await _uploadRepo.FirstOrDefaultAsync(x =>
            x.StorageProvider == model.StorageProvider &&
            x.HashType == model.HashType &&
            x.ResourceHash == model.ResourceHash &&
            x.ResourceSize == model.ResourceSize &&
            x.ResourceKey == model.ResourceKey);

        return previousUploaded;
    }

    private async Task PlusResourceMetaUploadTimes(StorageMeta previousUploaded)
    {
        ++previousUploaded.UploadTimes;
        previousUploaded.LastModificationTime = Clock.Now;

        await _uploadRepo.UpdateAsync(previousUploaded);
    }

    private async Task InsertResourceMeta(StorageMeta model)
    {
        //保存到数据库
        model.Id = GuidGenerator.CreateGuidString();
        model.CreationTime = Clock.Now;

        await _uploadRepo.InsertAsync(model);
    }

    public async Task<StorageMeta> GetFileByKeyAsync(string fileKey)
    {
        if (string.IsNullOrWhiteSpace(fileKey))
            throw new ArgumentNullException(nameof(fileKey));

        var previousUploaded = await _uploadRepo.FirstOrDefaultAsync(x => x.ResourceKey == fileKey);
        return previousUploaded;
    }

    public async Task<StorageMeta> GetFileByKeyAsync(string fileKey, CacheStrategy option)
    {
        var key = $"storage.meta.by-key:{fileKey}";

        var data = await CacheProvider.ExecuteWithPolicyAsync(
            () => GetFileByKeyAsync(fileKey),
            new CacheOption<StorageMeta>(key, TimeSpan.FromMinutes(1000)),
            option);

        return data;
    }
}