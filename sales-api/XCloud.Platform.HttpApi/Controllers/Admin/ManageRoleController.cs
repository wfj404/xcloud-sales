﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Roles;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/manage-role")]
public class ManageRoleController : PlatformBaseController
{
    private readonly IAdminRoleService _adminRoleService;
    private readonly IRoleService _roleService;
    private readonly IAdminSecurityService _securityService;

    public ManageRoleController(IAdminRoleService adminRoleService, IAdminSecurityService securityService,
        IRoleService roleService)
    {
        _adminRoleService = adminRoleService;
        _securityService = securityService;
        _roleService = roleService;
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<RoleDto>> QueryPagingAsync([FromBody] QueryRolePaging dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var response = await _roleService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _roleService.AttachPermissionAsync(response.Items.ToArray());
        }

        return response;
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _roleService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] RoleDto dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _roleService.InsertAsync(dto);
        }
        else
        {
            await _roleService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("set-admin-roles")]
    public async Task<ApiResponse<object>> SetAdminRolesAsync([FromBody] SetAdminRolesDto dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _adminRoleService.SetAdminRolesAsync(dto.Id, dto.RoleIds);

        return new ApiResponse<object>();
    }

    [HttpPost("set-role-permissions")]
    public async Task<ApiResponse<object>> SetRolePermissionsAsync([FromBody] SetPermissionToRoleDto dto)
    {
        var loginAdmin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _roleService.SetRolePermissionsAsync(dto.Id, dto.PermissionKeys);

        return new ApiResponse<object>();
    }
}