﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Users;

namespace XCloud.Platform.Application.Mapping.Users;

public class UserRealNameMap : PlatformEntityTypeConfiguration<UserRealName>
{
    public override void Configure(EntityTypeBuilder<UserRealName> builder)
    {
        builder.ToTable("sys_user_real_name");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.IdCardIdentity).IsRequired().HasMaxLength(100);
        builder.HasIndex(x => x.IdCardIdentity).IsUnique();

        builder.Property(x => x.UserId).IsRequired().HasMaxLength(100);
        builder.HasIndex(x => x.UserId);

        builder.Property(x => x.Data).HasMaxLength(4000);

        //id card
        builder.Property(x => x.IdCard).IsRequired().HasMaxLength(100);
        builder.Property(x => x.IdCardRealName).HasMaxLength(20);

        builder.Property(x => x.IdCardFrontUrl).HasMaxLength(1000);
        builder.Property(x => x.IdCardBackUrl).HasMaxLength(1000);
        builder.Property(x => x.IdCardHandUrl).HasMaxLength(1000);

        base.Configure(builder);

    }
}