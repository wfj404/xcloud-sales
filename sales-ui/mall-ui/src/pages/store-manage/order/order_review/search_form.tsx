import { QueryOrderReviewPaging } from '@/utils/swagger';
import { Button, Card, Col, Form, Input, Row } from 'antd';
import { useEffect, useState } from 'react';

export default ({
                  query, onSearch,
                }: {
  query: QueryOrderReviewPaging,
  onSearch: (d: QueryOrderReviewPaging) => void
}) => {

  const [form, _form] = useState<QueryOrderReviewPaging>({});

  useEffect(() => {
    _form({
      ...form,
      ...query,
    });
  }, [
    query,
  ]);

  return <>
    <Card bordered={false} style={{
      marginBottom: 10,
    }}>
      <Form onFinish={() => {
        onSearch({
          ...form,
          Page: 1,
        });
      }}>
        <Row gutter={10}>
          <Col span={6}>
            <Form.Item label={'关键词'}>
              <Input value={form.Keywords || ''} onChange={e => {
                _form({
                  ...form,
                  Keywords: e.target.value,
                });
              }} />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item>
              <Button
                htmlType={'submit'}
                type={'primary'}>搜索</Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  </>;
}
