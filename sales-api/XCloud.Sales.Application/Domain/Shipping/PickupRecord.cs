namespace XCloud.Sales.Application.Domain.Shipping;

public class PickupRecord : SalesBaseEntity<string>, IHasCreationTime
{
    public string OrderId { get; set; }

    public bool Prepared { get; set; }

    public DateTime? PreparedTime { get; set; }

    public bool? ReservationApproved { get; set; }

    public DateTime? ReservationApprovedTime { get; set; }

    public DateTime? PreferPickupStartTime { get; set; }

    public DateTime? PreferPickupEndTime { get; set; }

    public bool Picked { get; set; }

    public DateTime? PickedTime { get; set; }

    public string SecurityCode { get; set; }

    public DateTime CreationTime { get; set; }
}