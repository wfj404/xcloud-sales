﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Sales.ThirdParty.Services.KuaiMai;

public static class KuaiMaiConstrant
{
    public static string Endpoint => "https://cloud.kuaimai.com";
}

public class KuaiMaiResponse<T> : IEntityDto
{
    public bool status { get; set; }

    public T data { get; set; }

    public string message { get; set; }

    public string exceptionMessage { get; set; }

    public int? code { get; set; }
}

public class KuaiMaiDeviceDto : IEntityDto
{
    public string SerialNo { get; set; }

    public string DeviceKey { get; set; }

    public string BindName { get; set; }

    public string NoteName { get; set; }
}

[TypeIdentityName("kuaimai-config")]
public class KuaiMaiConfigDto : ISettings
{
    public bool Enabled { get; set; }

    public string ClientId { get; set; }

    public string ClientSecret { get; set; }

    private KuaiMaiDeviceDto[] _devices;

    [NotNull]
    public KuaiMaiDeviceDto[] Devices
    {
        get => this._devices ??= [];
        set => this._devices = value;
    }

    public bool Available => this.Enabled &&
                             !string.IsNullOrWhiteSpace(this.ClientId) &&
                             !string.IsNullOrWhiteSpace(this.ClientSecret);
}