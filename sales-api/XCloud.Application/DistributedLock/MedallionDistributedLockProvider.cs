﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Medallion.Threading;
using Volo.Abp.DependencyInjection;

namespace XCloud.Application.DistributedLock;

public class DistributedLockHandleWrapper : IDistributedLock
{
    [CanBeNull] private readonly IDistributedSynchronizationHandle _lockHandle;

    public DistributedLockHandleWrapper([CanBeNull] IDistributedSynchronizationHandle lockHandle)
    {
        _lockHandle = lockHandle;
    }

    public async ValueTask DisposeAsync()
    {
        if (_lockHandle != null)
        {
            await using (_lockHandle)
            {
                // dispose lock handle
            }
        }
    }

    public bool IsAcquired => _lockHandle != null;
}

[ExposeServices(typeof(IDistributedLockProvider))]
public class MedallionDistributedLockProvider : IDistributedLockProvider, ISingletonDependency
{
    private readonly Medallion.Threading.IDistributedLockProvider _distributedLockProvider;

    public MedallionDistributedLockProvider(Medallion.Threading.IDistributedLockProvider distributedLockProvider)
    {
        _distributedLockProvider = distributedLockProvider;
    }

    public async Task<IDistributedLock> CreateLockAsync(string resource, TimeSpan? expiryTime)
    {
        var timeout = expiryTime ?? TimeSpan.FromSeconds(5);

        var distributedLock = this._distributedLockProvider.CreateLock(name: resource);

        var handleOrNull = await distributedLock.TryAcquireAsync(timeout: timeout);

        return new DistributedLockHandleWrapper(handleOrNull);
    }
}