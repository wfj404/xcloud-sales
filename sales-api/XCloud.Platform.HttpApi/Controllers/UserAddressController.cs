﻿using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Domain.Address;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Address;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/user-address")]
public class UserAddressController : PlatformBaseController
{
    private readonly IUserAddressService _userAddressService;

    public UserAddressController(IUserAddressService userAddressService)
    {
        _userAddressService = userAddressService;
    }

    [HttpPost("list")]
    public async Task<ApiResponse<UserAddressDto[]>> ListAsync()
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        var allAddress = await _userAddressService.QueryByUserIdAsync(loginUser.Id);

        await _userAddressService.AttachErrorsAsync(allAddress);

        allAddress = allAddress
            .OrderByDescending(x => x.IsDefault.ToBoolInt())
            .ThenByDescending(x => x.CreationTime).ToArray();

        return new ApiResponse<UserAddressDto[]>().SetData(allAddress);
    }

    [HttpPost("get-by-id")]
    public async Task<ApiResponse<UserAddress>> GetByIdAsync([FromBody] IdDto dto)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        var address = await _userAddressService.QueryByIdAsync(dto.Id);

        if (address == null)
            return new ApiResponse<UserAddress>().SetError("not exist");

        (address.UserId == loginUser.Id).Should().BeTrue();

        return new ApiResponse<UserAddress>().SetData(address);
    }

    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] UserAddressDto dto)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            dto.UserId = loginUser.Id;
            var address = await _userAddressService.InsertAsync(dto);

            if (address.IsDefault)
                await _userAddressService.SetAsDefaultAsync(loginUser.Id, address.Id);
        }
        else
        {
            var address = await _userAddressService.QueryByIdAsync(dto.Id);
            if (address == null || address.UserId != loginUser.Id)
                throw new EntityNotFoundException(nameof(address));

            dto.UserId = loginUser.Id;
            await _userAddressService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("set-default")]
    public async Task<ApiResponse<object>> SetDefaultAsync([FromBody] IdDto dto)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        await _userAddressService.SetAsDefaultAsync(loginUser.Id, dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        var address = await _userAddressService.QueryByIdAsync(dto.Id);

        if (address == null)
            return new ApiResponse<object>();

        (address.UserId == loginUser.Id).Should().BeTrue();

        await _userAddressService.SoftDeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }
}