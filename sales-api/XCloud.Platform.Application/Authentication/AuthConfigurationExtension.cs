﻿using System.Text;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using XCloud.Platform.Application.Authentication.Authorization;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Core;

namespace XCloud.Platform.Application.Authentication;

public static class AuthConfigurationExtension
{
    private static void AddJwtAuthentication(this AuthenticationBuilder builder)
    {
        var configuration = builder.Services.GetConfiguration();
        var oauthOption = configuration.GetOAuthServerOption();

        if (string.IsNullOrWhiteSpace(oauthOption.JwtSecretKey))
            throw new ArgumentNullException(nameof(oauthOption.JwtSecretKey));

        if (string.IsNullOrWhiteSpace(oauthOption.Server))
            throw new ArgumentNullException(nameof(oauthOption.Server));

        var scheme = AuthConstants.Scheme.JwtTokenScheme;

        builder.AddJwtBearer(scheme, option =>
        {
            option.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true, //是否验证Issuer
                ValidateAudience = true, //是否验证Audience
                ValidateLifetime = true, //是否验证失效时间
                ClockSkew = TimeSpan.FromSeconds(30),
                ValidateIssuerSigningKey = true, //是否验证SecurityKey
                ValidAudience = oauthOption.Server, //Audience
                ValidIssuer = oauthOption.Server, //Issuer，这两项和前面签发jwt的设置一致
                IssuerSigningKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(oauthOption.JwtSecretKey)) //拿到SecurityKey
            };
        });
    }

    private static void AddExternalCookieAuthentication(this AuthenticationBuilder builder)
    {
        var cookieExternalScheme = AuthConstants.Scheme.ExternalLoginScheme;

        builder.AddCookie(cookieExternalScheme, option =>
        {
            //option.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Lax;

            option.Cookie.Name = $"ids_web_external_login_{cookieExternalScheme}";
            //三方登陆的信息只保存10分钟，请尽快引导用户登陆自己账号体系的账号
            option.ExpireTimeSpan = TimeSpan.FromMinutes(10);
        });
    }

    private static void AddNullHandler(this AuthenticationBuilder builder)
    {
        builder.AddScheme<NullTokenAuthHandlerOption, NullTokenAuthHandler>(
            authenticationScheme: AuthConstants.Scheme.NullHandlerScheme,
            displayName: AuthConstants.Scheme.NullHandlerScheme,
            configureOptions: option => { option.Key = string.Empty; });
    }

    public static void AddUserAuthentication(this IServiceCollection collection)
    {
        //认证配置
        var defaultAuthScheme = AuthConstants.Scheme.JwtTokenScheme;

        var builder = collection.AddAuthentication(defaultScheme: defaultAuthScheme);

        builder.AddExternalCookieAuthentication();
        builder.AddJwtAuthentication();
        builder.AddNullHandler();

        collection.Configure<AuthenticationOptions>(option => { option.DefaultScheme = defaultAuthScheme; });
    }

    /// <summary>
    /// 权限验证
    /// </summary>
    public static void AddAuthorizationHandlers([NotNull] this IServiceCollection serviceCollection)
    {
        if (serviceCollection == null) throw new ArgumentNullException(nameof(serviceCollection));
        //collection.RemoveAll<IAuthorizationHandler>();
        serviceCollection.AddSingleton<IAuthorizationHandler, AdminAuthorizationHandler>();
        //serviceCollection.AddSingleton<IAuthorizationHandler, AdminDepartmentAuthorizationHandler>();
    }
}