import { Button, Descriptions, Empty, message, Modal, Result, Space, Spin, Table } from 'antd';
import React, { useEffect, useState } from 'react';

import XRenderTime from '@/components/common/time/render_time';
import TimeRange from '@/pages/store-manage/shipping/components/time_range';
import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { PickupRecordDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XStatus from '../components/pickup_status';

const XSummary = ({ data }: { data: PickupRecordDto }) => {
  return (
    <Descriptions bordered>
      <Descriptions.Item label='预计配送时间'>
        <TimeRange start={data.PreferPickupStartTime} end={data.PreferPickupEndTime} />
      </Descriptions.Item>
      <Descriptions.Item label='配送状态'>
        <XStatus data={data} />
      </Descriptions.Item>
      <Descriptions.Item label='审批时间'>
        <XRenderTime timeStr={data.ReservationApprovedTime} />
      </Descriptions.Item>
      <Descriptions.Item label='准备完毕时间'>
        <XRenderTime timeStr={data.PreparedTime} />
      </Descriptions.Item>
      <Descriptions.Item label='取货时间'>
        <XRenderTime timeStr={data.PickedTime} />
      </Descriptions.Item>
      <Descriptions.Item label='创建时间' span={2}>
        <XRenderTime timeStr={data.CreationTime} />
      </Descriptions.Item>
    </Descriptions>
  );
};

const XPickupItems = ({ data }: { data: PickupRecordDto }) => {
  return <Table pagination={false} />;
};

export default ({
                  detailId,
                  hide,
                }: {
  detailId: string;
  hide: () => void;
}) => {
  const [loading, _loading] = useState<boolean>(false);
  const [data, _data] = useState<PickupRecordDto>({});
  const show = !isStringEmpty(detailId);

  const setApproved = (e: PickupRecordDto, approved: boolean) => {
    if (!confirm('确认审批？')) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .pickupApproveOrNot({ Key: e.Id, Value: approved })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          queryDetail(detailId);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const setPrepared = (e: PickupRecordDto) => {
    if (!confirm('确认货品已经准备完成？')) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .pickupSetPrepared({ Id: e.Id })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          queryDetail(detailId);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const setPicked = (e: PickupRecordDto) => {
    _loading(true);
    apiClient.mallManager
      .pickupSetPicked({
        Id: e.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('发货成功');
          queryDetail(detailId);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const queryDetail = (id: string) => {
    if (isStringEmpty(id)) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .pickupQueryById({ Id: id })
      .then((res) => {
        _data(res.data.Data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryDetail(detailId);
  }, [detailId]);

  const renderApproveButton = () => {
    if (data.ReservationApproved != null || data.ReservationApprovedTime != null) {
      return null;
    }

    return <Button.Group>
      <Button
        loading={loading}
        type={'primary'}
        onClick={() => {
          setApproved(data, true);
        }}
      >
        批准预约
      </Button>
      <Button
        loading={loading}
        type={'primary'}
        danger
        onClick={() => {
          setApproved(data, false);
        }}
      >
        拒绝预约
      </Button>
    </Button.Group>;
  };

  const renderPrepareButton = () => {
    if (!data.ReservationApproved) {
      return null;
    }
    if (data.Prepared) {
      return null;
    }
    return <Button.Group>
      <Button
        loading={loading}
        type={'primary'}
        onClick={() => {
          setPrepared(data);
        }}
      >
        货品已经准备完成
      </Button>
    </Button.Group>;
  };

  const renderPickedButton = () => {

    if (!data.Prepared) {
      return null;
    }

    if (data.Picked) {
      return null;
    }

    return <Button.Group>
      <Button
        loading={loading}
        type={'link'}
        onClick={() => {
          setPicked(data);
        }}
      >
        取货完成
      </Button>
    </Button.Group>;
  };

  const renderPickedMessage = () => {
    if (!data.Picked) {
      return null;
    }

    return (
      <Result
        status='success'
        title='提货已经完成'
        subTitle={`提货单：${data.Id}已经于${formatRelativeTimeFromNow(data.PickedTime)}完成提货`}
      />);
  };

  const renderContent = () => {
    if (isStringEmpty(data.Id)) {
      return <Empty />;
    }
    return <>
      <XSummary data={data} />
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 20,
        }}
      >
        <Space direction={'horizontal'}>
          {renderApproveButton()}

          {renderPrepareButton()}

          {renderPickedButton()}

          {renderPickedMessage()}
        </Space>
      </div>
      <XPickupItems data={data} />
    </>;
  };

  return (
    <>
      <Modal
        width={'90%'}
        title={'自提单'}
        open={show}
        footer={false}
        onCancel={() => {
          hide();
        }}
        onOk={() => {
        }}
      >
        <Spin spinning={loading}>
          {renderContent()}
        </Spin>
      </Modal>
    </>
  );
};
