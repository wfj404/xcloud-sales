﻿using JetBrains.Annotations;
using Volo.Abp.Authorization;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Authentication;

public static class StoreAuthExtension
{
    [ItemNotNull]
    public static async Task<StoreManagerDto> GetRequiredStoreManagerAsync(
        this IStoreManagerAuthService storeAuthService,
        string storeId = null)
    {
        var res = await storeAuthService.GetStoreManagerAsync(storeId);

        res.ThrowIfErrorOccured(x => new AbpAuthorizationException(x.Message ?? nameof(GetRequiredStoreManagerAsync)));

        var manager = res.StoreManager;

        if (manager == null || manager.IsEmptyId())
            throw new AbpAuthorizationException(nameof(manager));

        if (!manager.IsActive)
            throw new AbpAuthorizationException(nameof(manager.IsActive));

        if (string.IsNullOrWhiteSpace(manager.StoreId))
            throw new AbpAuthorizationException("store id is lost");

        return manager;
    }

    [NotNull]
    [ItemNotNull]
    public static async Task<StoreUserDto> GetRequiredStoreUserAsync(this IStoreUserAuthService storeAuthService)
    {
        var res = await storeAuthService.GetStoreUserAsync();

        res.ThrowIfErrorOccured(x => new AbpAuthorizationException(x.Message ?? nameof(GetRequiredStoreUserAsync)));

        var user = res.StoreUser;

        if (user == null || user.IsEmptyId())
            throw new AbpAuthorizationException(nameof(user));

        if (!user.IsActive)
            throw new AbpAuthorizationException(nameof(user.IsActive));

        return user;
    }

    [ItemCanBeNull]
    public static async Task<StoreUserDto> GetStoreUserOrNullAsync(this IStoreUserAuthService authenticationService)
    {
        var storeUser = await authenticationService.GetStoreUserAsync();

        return storeUser.StoreUser;
    }
}