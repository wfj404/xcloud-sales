﻿using System.IO;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Storage;

namespace XCloud.Platform.Application.Service.Storage;

/// <summary>
/// resource object
/// </summary>
public class StorageMetaDto : StorageMeta, IEntityDto<string>
{
    public StorageMetaDto()
    {
        //
    }

    public StorageMetaDto(string provider) : this()
    {
        StorageProvider = provider;
    }

    public string Url { get; set; }

    public string Error { get; set; }
}

public class QueryStoragePaging : PagedRequest
{
    public string ContentType { get; set; }
    public string FileExtension { get; set; }
    public string StorageProvider { get; set; }
    public double? MinSize { get; set; }
    public double? MaxSize { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class FileUploadArgs : IEntityDto
{
    public string FileName { get; set; }
    public string ContentType { get; set; }
}

public class FileUploadBytesArgs : FileUploadArgs
{
    public FileUploadBytesArgs()
    {
        //
    }

    public byte[] Bytes { get; set; }
}

public class FileUploadStreamArgs : FileUploadArgs
{
    public FileUploadStreamArgs(Stream stream)
    {
        Stream = stream;
    }

    public Stream Stream { get; set; }
}

public class FileUploadFromUrl : IEntityDto
{
    public string Url { get; set; }
    public string FileName { get; set; }
}