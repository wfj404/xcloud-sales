﻿using System;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using XCloud.Core.Configuration;
using XCloud.Core.Json;
using XCloud.Platform.Messenger.Abstraction.Core;
using XCloud.Platform.Messenger.Abstraction.MessageHandler;
using XCloud.Platform.Messenger.Abstraction.Router;
using XCloud.Platform.Messenger.Service;

namespace XCloud.Platform.Messenger.Abstraction;

public interface IMessengerServer : IDisposable
{
    string ServerInstanceId { get; }
    Encoding Encoding { get; }

    [NotNull] ConnectionManager ConnectionManager { get; }

    [NotNull] IJsonDataSerializer MessageSerializer { get; }
    [NotNull] IMessageClusterRouter MessageClusterRouter { get; }

    Task OnConnectedAsync(IConnection connection);
    Task OnDisConnectedAsync(IConnection connection);

    Task OnMessageFromClientAsync(MessageDto message, IConnection connection);
    Task OnMessageFromRouterAsync(MessageDto message);

    Task StartAsync();
}

public class MessengerServer : IMessengerServer
{
    private readonly ILogger _logger;
    private IServiceProvider RootServiceRootServiceProvider { get; }

    public ConnectionManager ConnectionManager { get; } = new ConnectionManager();

    public string ServerInstanceId { get; }
    public Encoding Encoding { get; }

    public IJsonDataSerializer MessageSerializer { get; }
    public IMessageClusterRouter MessageClusterRouter { get; }

    public MessengerServer(IServiceProvider rootServiceProvider, string serverInstanceId)
    {
        if (string.IsNullOrWhiteSpace(serverInstanceId))
            throw new ArgumentNullException(nameof(serverInstanceId));

        ServerInstanceId = serverInstanceId;

        RootServiceRootServiceProvider = rootServiceProvider;
        _logger = rootServiceProvider.GetRequiredService<ILogger<MessengerServer>>();
        Encoding = rootServiceProvider.GetRequiredService<AppConfig>().AppEncoding;
        MessageSerializer = rootServiceProvider.GetRequiredService<IJsonDataSerializer>();
        MessageClusterRouter = rootServiceProvider.GetRequiredService<IMessageClusterRouter>();
    }

    public async Task OnConnectedAsync(IConnection connection)
    {
        ConnectionManager.AddConnection(connection);

        //ping will trigger registration work
        var msg = new MessageDto { MessageType = MessageTypeConst.Ping };

        await OnMessageFromClientAsync(msg, connection);

        _logger.LogInformation($"{connection.ClientIdentity.ConnectionId} joined");
    }

    public async Task OnDisConnectedAsync(IConnection connection)
    {
        using var s = this.RootServiceRootServiceProvider.CreateScope();

        ConnectionManager.RemoveConnection(connection);

        var regService = s.ServiceProvider.GetRequiredService<IUserRegistrationService>();

        await regService.RemoveUserRegistrationAsync(
            connection.ClientIdentity.SubjectId,
            connection.ClientIdentity.DeviceType);

        _logger.LogInformation($"{connection.ClientIdentity.ConnectionId} leaved");
    }

    public async Task OnMessageFromClientAsync(MessageDto message, IConnection connection)
    {
        using var s = RootServiceRootServiceProvider.CreateScope();
        var handler = s.ServiceProvider.GetMessageHandlerOrNull(message.MessageType);
        if (handler != null)
        {
            var context = new ClientMessageContext(this)
            {
                ScopedServiceProvider = s.ServiceProvider,
                Connection = connection,
                Message = message
            };
            await handler.HandleMessageFromClientAsync(context);
        }
        else
        {
            _logger.LogWarning("no handler for this message type");
            await connection.SendMessageToClientAsync(new MessageDto
                { MessageType = "no handler for this message type" });
        }
    }

    public async Task OnMessageFromRouterAsync(MessageDto message)
    {
        using var s = RootServiceRootServiceProvider.CreateScope();
        var handler = s.ServiceProvider.GetMessageHandlerOrNull(message.MessageType);
        if (handler != null)
        {
            var context = new TransportMessageContext(this)
            {
                ScopedServiceProvider = s.ServiceProvider,
                Message = message
            };
            await handler.HandleMessageFromTransportAsync(context);
        }
        else
        {
            _logger.LogWarning(
                message: $"no handler for message:{MessageSerializer.SerializeToString(message)}");
        }
    }

    public async Task StartAsync()
    {
        //路由到这台服务器的消息
        await MessageClusterRouter.SubscribeFromMessageRouterAsync(ServerInstanceId, OnMessageFromRouterAsync);
    }

    public void Dispose()
    {
        //todo ioc will dispose obj
        foreach (var connection in ConnectionManager.AsReadOnlyList())
        {
            connection.RequestToClose();
        }
    }
}