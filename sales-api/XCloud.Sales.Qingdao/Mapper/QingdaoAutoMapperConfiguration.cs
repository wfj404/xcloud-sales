using AutoMapper;
using XCloud.Sales.Qingdao.Domain.Invoices;
using XCloud.Sales.Qingdao.Service.Invoices;

namespace XCloud.Sales.Qingdao.Mapper;

public class QingdaoAutoMapperConfiguration : Profile
{
    public QingdaoAutoMapperConfiguration()
    {
        this.CreateMap<McsInvoiceRequest, McsInvoiceRequestDto>().ReverseMap();
        this.CreateMap<McsInvoiceRequestItem, McsInvoiceRequestItemDto>().ReverseMap();
    }
}