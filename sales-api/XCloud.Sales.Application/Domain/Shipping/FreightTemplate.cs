using XCloud.Application.Utils;

namespace XCloud.Sales.Application.Domain.Shipping;

[TypeIdentityName("delivery-template")]
public class FreightTemplate : SalesBaseEntity<string>, IHasCreationTime
{
    public string Name { get; set; }

    public string Description { get; set; }

    public string StoreId { get; set; }

    public int? ShipInHours { get; set; }

    public decimal FreeShippingMinOrderTotalPrice { get; set; }

    public int CalculateType { get; set; }

    public string RulesJson { get; set; }

    public string ExcludedAreaJson { get; set; }

    public int Sort { get; set; }

    public DateTime CreationTime { get; set; }
}