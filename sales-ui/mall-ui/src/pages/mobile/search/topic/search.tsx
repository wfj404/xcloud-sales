

import { QueryGoodsPaging, SearchView } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box, Button, Typography } from '@mui/material';

export default function ({
  query,
  onChange,
  model,
}: {
  query: QueryGoodsPaging;
  onChange: (e: QueryGoodsPaging) => void;
  model: SearchView;
}) {
  return (
    <>
      {isArrayEmpty(model.Keywords) || (
        <Box sx={{ px: 2, py: 1 }}>
          <Typography variant="overline" color="text.disabled" gutterBottom>
            热门关键词
          </Typography>
          <Box sx={{ mt: 1 }}>
            {model.Keywords?.map((x, index) => (
              <Button
                key={index}
                size="small"
                variant="text"
                sx={{
                  color: (theme) => theme.palette.text.primary,
                }}
                onClick={() => {
                  onChange({
                    ...query,
                    Keywords: x,
                  });
                }}
              >
                {x}
              </Button>
            ))}
          </Box>
        </Box>
      )}
    </>
  );
}
