﻿using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Users;

public interface IExternalConnectService : IXCloudAppService
{
    Task<ExternalConnect> QueryByIdAsync(string connectionId);

    Task DeleteByIdAsync(string connectionId);

    Task SaveAsync(ExternalConnect connection);

    Task<ExternalConnect> FindByUserIdAsync(string platform, string appId, string userId);

    Task<ExternalConnect> FindByOpenIdAsync(string platform, string appId, string openId);
}

public class ExternalConnectService : PlatformAppService, IExternalConnectService
{
    private readonly IPlatformRepository<ExternalConnect> _repository;

    public ExternalConnectService(IPlatformRepository<ExternalConnect> repository)
    {
        _repository = repository;
    }

    public async Task<ExternalConnect> QueryByIdAsync(string connectionId)
    {
        if (string.IsNullOrWhiteSpace(connectionId))
            throw new ArgumentNullException(nameof(connectionId));

        var entity = await _repository.FirstOrDefaultAsync(x => x.Id == connectionId);

        return entity;
    }

    public async Task DeleteByIdAsync(string connectionId)
    {
        if (string.IsNullOrWhiteSpace(connectionId))
            throw new ArgumentNullException(nameof(connectionId));

        await _repository.DeleteAsync(x => x.Id == connectionId);
    }

    public async Task<ExternalConnect> FindByUserIdAsync(string platform, string appId, string userId)
    {
        if (string.IsNullOrWhiteSpace(platform) || string.IsNullOrWhiteSpace(appId))
            throw new ArgumentNullException(nameof(FindByUserIdAsync));

        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<ExternalConnect>().AsNoTracking();

        query = query.Where(x => x.Platform == platform && x.AppId == appId && x.UserId == userId);

        var entity = await query.OrderBy(x => x.CreationTime).FirstOrDefaultAsync();

        return entity;
    }

    public async Task<ExternalConnect> FindByOpenIdAsync(
        string platform, string appId, string openId)
    {
        if (string.IsNullOrWhiteSpace(platform) ||
            string.IsNullOrWhiteSpace(appId) ||
            string.IsNullOrWhiteSpace(openId))
            throw new ArgumentNullException(nameof(FindByOpenIdAsync));

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<ExternalConnect>().AsNoTracking()
            .Where(x =>
                x.Platform == platform &&
                x.AppId == appId &&
                x.OpenId == openId);

        var res = await query.OrderBy(x => x.CreationTime).FirstOrDefaultAsync();

        return res;
    }

    public async Task SaveAsync(ExternalConnect connection)
    {
        if (connection == null)
            throw new ArgumentNullException(nameof(connection));

        var db = await _repository.GetDbContextAsync();

        var set = db.Set<ExternalConnect>();

        var alreadyExist = await set.Where(x =>
            x.UserId == connection.UserId &&
            x.Platform == connection.Platform &&
            x.AppId == connection.AppId &&
            x.OpenId == connection.OpenId).AnyAsync();

        if (alreadyExist)
            return;

        connection.Id = GuidGenerator.CreateGuidString();
        connection.CreationTime = Clock.Now;

        set.Add(connection);

        await db.SaveChangesAsync();
    }
}