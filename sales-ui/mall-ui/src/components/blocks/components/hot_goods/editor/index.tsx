import { Divider, Form, Segmented } from 'antd';

import { GoodsDisplayType, GoodsDisplayTypeOptions } from '@/components/blocks/utils';
import GoodsList from '@/components/goods/multiple_goods_selector';
import { isStringEmpty } from '@/utils/utils';

import { HotGoodsBlockProps, HotGoodsBlockType } from '../utils';
import SearchParameter from './search';

export default function ({
  data,
  onChange,
}: {
  data: HotGoodsBlockProps;
  onChange: (d: HotGoodsBlockProps) => void;
}) {
  return (
    <>
      <Form
        labelCol={{
          span: 3,
        }}
      >
        <Form.Item label={'数据来源'}>
          <Segmented
            required
            defaultValue={'filtered-goods' as HotGoodsBlockType}
            options={[
              {
                label: '基于筛选条件',
                value: 'filtered-goods',
              },
              {
                label: '选择的商品',
                value: 'selected-goods',
              },
            ]}
            value={data?.type}
            onChange={(e) => {
              onChange({
                ...data,
                type: e as HotGoodsBlockType,
              });
            }}
          />
        </Form.Item>

        <Divider type="horizontal" />

        {(data.type == 'filtered-goods' || isStringEmpty(data.type)) && (
          <SearchParameter
            data={data.search || {}}
            onChange={(e) => {
              onChange({
                ...data,
                search: e,
              });
            }}
          />
        )}

        {data.type == 'selected-goods' && (
          <GoodsList
            goods_list={data.goods_list || []}
            onChange={(e) => {
              onChange({
                ...data,
                goods_list: e || [],
              });
            }}
          />
        )}

        <Divider type="horizontal" />

        <Form.Item label={'展示方式'}>
          <Segmented
            required
            defaultValue={'grid' as GoodsDisplayType}
            options={GoodsDisplayTypeOptions}
            value={data?.display_type}
            onChange={(e) => {
              if (!e) {
                return;
              }
              onChange({
                ...data,
                display_type: e as GoodsDisplayType,
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
}
