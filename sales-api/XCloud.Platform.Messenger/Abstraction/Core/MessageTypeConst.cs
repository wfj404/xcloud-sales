namespace XCloud.Platform.Messenger.Abstraction.Core;

public static class MessageTypeConst
{
    public const string UserToUser = "user-to-user";

    public const string Ping = "ping";
    
    public const string BroadCast = "broad-cast";

    public const string Group = "group";
}