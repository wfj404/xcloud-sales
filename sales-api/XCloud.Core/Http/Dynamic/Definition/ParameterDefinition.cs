﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace XCloud.Core.Http.Dynamic.Definition;

public class ParameterDefinition
{
    public ParameterInfo ParameterInfo { get; }
    public IBindingSourceMetadata BindingSourceMetadata { get; }
    public IBinderTypeProviderMetadata BinderTypeProviderMetadata { get; }

    public BindingSource BindingSource => BindingSourceMetadata?.BindingSource;

    public ParameterDefinition(ParameterInfo m)
    {
        ParameterInfo = m ?? throw new ArgumentNullException(nameof(m));

        var attrs = m.GetCustomAttributes().ToArray();
        BindingSourceMetadata = attrs.OfType<IBindingSourceMetadata>().FirstOrDefault();
        BinderTypeProviderMetadata = attrs.OfType<IBinderTypeProviderMetadata>().FirstOrDefault();
    }

    public bool IsValidated()
    {
        if (BindingSource == null)
        {
            return false;
        }

        return true;
    }
}