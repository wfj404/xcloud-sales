﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Admins;

namespace XCloud.Platform.Application.Mapping.Admins;

public class AdminMap : PlatformEntityTypeConfiguration<Admin>
{
    public override void Configure(EntityTypeBuilder<Admin> builder)
    {
        builder.ToTable("sys_admin");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.UserId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.IsActive);
        builder.Property(x => x.IsSuperAdmin);
        
        base.Configure(builder);

    }
}