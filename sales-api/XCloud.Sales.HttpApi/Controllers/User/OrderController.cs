﻿using Volo.Abp.Uow;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Coupons;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Orders.PlaceOrder;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Pickup;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.User;

/// <summary>
/// 订单接口
/// </summary>
[Route("api/mall/order")]
public class OrderController : ShopBaseController
{
    private readonly IOrderService _orderService;
    private readonly IOrderProcessingService _orderProcessingService;
    private readonly IPlaceOrderService _placeOrderService;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly OrderViewService _orderViewService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;
    private readonly IOrderNoteService _orderNoteService;
    private readonly IGoodsPictureService _goodsPictureService;
    private readonly IDeliveryService _deliveryService;
    private readonly IPickupService _pickupService;
    private readonly IOrderReviewService _orderReviewService;
    private readonly IPriceCalculationService _priceCalculationService;
    private readonly IOrderCouponService _orderCouponService;
    private readonly IOrderDeliveryService _orderDeliveryService;
    private readonly IOrderPickupService _orderPickupService;

    /// <summary>
    /// 构造器
    /// </summary>
    public OrderController(
        IOrderService orderService,
        IOrderPaymentBillService orderPaymentBillService,
        IPlaceOrderService placeOrderService,
        IOrderProcessingService orderProcessingService,
        OrderViewService orderViewService,
        IOrderAutoProcessService orderAutoProcessService,
        IOrderNoteService orderNoteService,
        IGoodsPictureService goodsPictureService,
        IDeliveryService deliveryService,
        IPickupService pickupService,
        IOrderReviewService orderReviewService,
        IOrderCouponService orderCouponService,
        IOrderDeliveryService orderDeliveryService,
        IOrderPickupService orderPickupService,
        IPriceCalculationService priceCalculationService,
        IOrderRefundBillService orderRefundBillService)
    {
        _placeOrderService = placeOrderService;
        _orderPaymentBillService = orderPaymentBillService;
        _orderService = orderService;
        _orderProcessingService = orderProcessingService;
        _orderViewService = orderViewService;
        _orderAutoProcessService = orderAutoProcessService;
        _orderNoteService = orderNoteService;
        _goodsPictureService = goodsPictureService;
        _deliveryService = deliveryService;
        _pickupService = pickupService;
        _orderReviewService = orderReviewService;
        _orderCouponService = orderCouponService;
        _orderDeliveryService = orderDeliveryService;
        _orderPickupService = orderPickupService;
        _priceCalculationService = priceCalculationService;
        _orderRefundBillService = orderRefundBillService;
    }

    [HttpPost("get-money-to-pay")]
    public async Task<ApiResponse<decimal>> GetMoneyToPayAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await this._orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        var money = await this._orderPaymentBillService.GetOrderRestMoneyToPayAsync(order.Id);

        return new ApiResponse<decimal>(money);
    }

    [HttpPost("pending-count")]
    public async Task<ApiResponse<int>> PendingCountAsync()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var count = await _orderService.QueryPendingCountAsync(new QueryPendingCountInput { UserId = loginUser.Id });

        return new ApiResponse<int>(count);
    }

    [HttpPost("pre-place-order")]
    public async Task<PlaceOrderResult> PrePlaceOrderWithoutSavingAsync([FromBody] PlaceOrderRequestDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = loginUser.Id;
        dto.StoreId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var result = await _placeOrderService.PrePlaceOrderWithoutSavingAsync(dto);

        if (result.HasError())
        {
            return result;
        }

        await this._orderDeliveryService.SetDeliveryAsync(result.Data);
        await this._orderPickupService.SetPickupAsync(result.Data);
        await this._orderCouponService.TryUseCouponAsync(result.Data);

        this._priceCalculationService.CalculateAndUpdateOrderPrice(result.Data!.Order);

        var skus = result.Data?.Order?.Items?.Select(x => x.Sku).WhereNotNull().ToArray();
        skus ??= [];

        var goods = skus.Select(x => x.Goods).WhereNotNull().ToArray();
        goods.HideDetail();

        await _goodsPictureService.AttachImagesAsync(skus);
        await _goodsPictureService.AttachImagesAsync(goods);

        return result;
    }

    /// <summary>
    /// 提交订单
    /// </summary>
    [UnitOfWork(isTransactional: true)]
    [HttpPost("place-order")]
    public async Task<PlaceOrderResult> PlaceOrderAsync([FromBody] PlaceOrderRequestDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = loginUser.Id;
        dto.StoreId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

        try
        {
            var result = await _placeOrderService.PrePlaceOrderWithoutSavingAsync(dto);

            if (result.HasError())
            {
                await uow.RollbackAsync();
            }
            else
            {
                await this._orderDeliveryService.SetDeliveryAsync(result.Data);
                await this._orderPickupService.SetPickupAsync(result.Data);
                await this._orderCouponService.TryUseCouponAsync(result.Data);

                this._priceCalculationService.CalculateAndUpdateOrderPrice(result.Data!.Order);

                await this._orderCouponService.TryMarkCouponsAsUsedAsync(result.Data);
                await this._placeOrderService.SaveOrderToDatabaseAsync(result.Data!);

                await uow.SaveChangesAsync();

                await this._placeOrderService.AfterPlacedOrderAsync(result.Data!);

                var order = result.Data!.Order!;
                order.Items.Select(x => x.Sku?.Goods).WhereNotNull().HideDetail();

                if (order.DeliveryRecord != null)
                {
                    var shipment = order.DeliveryRecord;
                    shipment.OrderId = order.Id;

                    var restItemsToShip = await _deliveryService.GetOrderRestItemsToShipAsync(order.Id);

                    shipment.Items =
                        restItemsToShip.MapArrayTo<DeliveryRecordItem, DeliveryRecordItemDto>(ObjectMapper);

                    await _deliveryService.CreateDeliveryAsync(shipment);
                }

                if (order.PickupRecord != null)
                {
                    await _pickupService.CreatePickupRecordAsync(order.PickupRecord);
                }

                await uow.SaveChangesAsync();

                await _orderAutoProcessService.AutoProcessOrderAsync(order.Id);

                await uow.SaveChangesAsync();

                await SalesEventBusService.NotifyOrderUpdatedAsync(order.Id);

                await SalesEventBusService.NotifyInsertActivityLog(new ActivityLog
                {
                    ActivityLogTypeId = ActivityLogType.PlaceOrder,
                    StoreUserId = loginUser.Id,
                    SubjectId = order.Id,
                    SubjectType = ActivityLogSubjectType.Order,
                    Comment = "创建订单"
                });

                //todo query latest order and set to result

                await uow.CompleteAsync();
            }

            return result;
        }
        catch
        {
            await uow.RollbackAsync();
            throw;
        }
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<OrderDto>> QueryOrderListAsync([FromBody] QueryOrderPagingInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = loginUser.Id;
        dto.HideForCustomer = false;

        var res = await _orderService.QueryOrderPagingAsync(dto);

        await _orderViewService.AttachOrderDetailDataAsync(res.Items.ToArray());

        var finishedOrders = res.Items.Where(x => x.IsFinished()).ToArray();
        await this._orderReviewService.AttachOrderReviewAsync(finishedOrders);

        return res;
    }

    /// <summary>
    /// 获取订单详情
    /// </summary>
    [HttpPost("detail")]
    public async Task<ApiResponse<OrderDto>> DetailAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.GetByIdAsync(dto.Id);

        if (order == null || order.UserId != loginUser.Id)
            throw new EntityNotFoundException(nameof(order));

        await _orderViewService.AttachOrderDetailDataAsync(new[] { order });

        await this._orderService.AttachShippingAddressAsync(new[] { order });

        await this._orderService.AttachInvoiceAsync(new[] { order });

        return new ApiResponse<OrderDto>(order);
    }

    [HttpPost("list-order-bill")]
    public async Task<ApiResponse<PaymentBillDto[]>> ListOrderBillAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        var bills = await _orderPaymentBillService.GetPaidBillsAsync(order.Id);

        await this._orderRefundBillService.AttachRefundedBillsAsync(bills);

        return new ApiResponse<PaymentBillDto[]>(bills);
    }

    [HttpPost("list-order-notes")]
    public async Task<ApiResponse<OrderNoteDto[]>> QueryOrderNotesAsync([FromBody] QueryOrderNotesInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.OrderId, loginUser.Id);

        dto.OnlyForUser = true;

        var notes = await _orderNoteService.QueryOrderNotesAsync(dto);

        return new ApiResponse<OrderNoteDto[]>(notes);
    }

    [HttpPost("complete")]
    public async Task<ApiResponse<object>> CompleteAsync([FromBody] FinishOrderInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.OrderId, loginUser.Id);

        await _orderProcessingService.FinishAsync(dto);

        return new ApiResponse<object>();
    }

    /// <summary>
    /// 取消订单
    /// </summary>
    [HttpPost("cancel")]
    public async Task<ApiResponse<object>> CancelOrderAsync([FromBody] CloseOrderInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.OrderId, loginUser.Id);

        await _orderProcessingService.CloseAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto<string> dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        await _orderService.HideForCustomerAsync(order.Id);

        return new ApiResponse<object>();
    }
}