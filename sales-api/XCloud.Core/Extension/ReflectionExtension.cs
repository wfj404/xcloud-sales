﻿using System.ComponentModel;
using System.Reflection;
using System.Threading.Tasks;
using XCloud.Core.Dto;

namespace XCloud.Core.Extension;

/// <summary>
/// BaseType是类
/// GetInterfaces是接口
/// IsGenericType是泛型
/// GetGenericTypeDefinition()获取泛型类型比如Consumer《string》
/// </summary>
public static class ReflectionExtension
{
    /// <summary>
    /// 用来获取枚举成员
    /// </summary>
    public static Dictionary<string, object> GetEnumFieldsValues(this Type t)
    {
        if (!t.IsEnum)
            throw new ArgumentException($"{t.FullName}must be enum");

        string GetFieldName(MemberInfo m)
        {
            var res = m.GetCustomAttribute<DescriptionAttribute>()?.Description ?? m.Name;
            return res;
        }

        var fields = t.GetFields(BindingFlags.Static | BindingFlags.Public);

        return fields.ToDictionaryUpdateDuplicateKey(x => GetFieldName(x), x => x.GetValue(null));
    }
    
    /// <summary>
    /// 找到依赖的所有程序集
    /// </summary>
    public static IEnumerable<Assembly> FindAllReferencedAssemblies(this Assembly entry,
        Func<Assembly, bool> filter = null)
    {
        filter ??= x => true;
        var walkPath = new List<Assembly>();

        var allAss = AppDomain.CurrentDomain.GetAssemblies();

        Assembly GetByName(AssemblyName name) =>
            allAss.FirstOrDefault(x => AssemblyName.ReferenceMatchesDefinition(x.GetName(), name));

        IEnumerable<Assembly> Find(Assembly ass)
        {
            if (ass == null)
                throw new ArgumentNullException(nameof(ass));

            if (!walkPath.Contains(ass) && filter.Invoke(ass))
            {
                yield return ass;
                walkPath.Add(ass);
                //找到依赖
                var referencedAss = ass.GetReferencedAssemblies().Select(x => GetByName(x)).WhereNotNull().ToArray();
                foreach (var m in referencedAss)
                {
                    foreach (var finded in Find(m))
                    {
                        yield return finded;
                    }
                }
            }
        }

        var res = Find(entry);
        return res;
    }

    public static bool IsNullable(this Type t)
    {
        var res = t.IsEqualToGenericType(typeof(Nullable<>));
        return res;
    }

    public static bool IsDatabaseTable(this Type t)
    {
        var res = t.IsNormalPublicClass() && t.IsAssignableToType<IDbTableFinder>();
        return res;
    }

    /// <summary>
    /// 获取所有类型
    /// </summary>
    /// <param name="ass"></param>
    /// <returns></returns>
    public static IEnumerable<Type> GetAllTypes(this IEnumerable<Assembly> ass)
    {
        var res = ass.SelectMany(x => x.GetTypes());
        return res;
    }

    public static bool IsAsyncResultType(this Type t)
    {
        var res = t.IsAssignableToType<IAsyncResult>();
        return res;
    }

    public static bool IsTask(this Type t)
    {
        var res =
            t == typeof(Task) ||
            t.IsEqualToGenericType(typeof(Task<>));
        return res;
    }

    public static bool IsValueTask(this Type t)
    {
        var res =
            t == typeof(ValueTask) ||
            t.IsEqualToGenericType(typeof(ValueTask<>));
        return res;
    }

    public static Type UnWrapTaskOrNull(this Type t)
    {
        if (t == null)
            throw new ArgumentNullException(nameof(t));

        if (t.IsEqualToGenericType(typeof(Task<>)))
        {
            var realType = t.GetGenericArguments().FirstOrDefault();
            return realType;
        }

        return null;
    }

    /// <summary>
    /// 是否可以赋值给
    /// </summary>
    public static bool IsAssignableToType<T>(this Type t) => t.IsAssignableToType(typeof(T));

    /// <summary>
    /// 是否可以赋值给
    /// </summary>
    public static bool IsAssignableToType(this Type t, Type type) => type.IsAssignableFrom(t);

    /// <summary>
    /// 不是抽象类，不是接口
    /// </summary>
    public static bool IsNormalPublicClass(this Type t)
    {
        var res = t.IsPublic && t.IsClass && !t.IsAbstract && !t.IsInterface;
        return res;
    }

    /// <summary>
    /// 是指定的泛型
    /// </summary>
    public static bool IsEqualToGenericType(this Type t, Type tt)
    {
        if (!tt.IsGenericType)
            throw new ArgumentException("传入参数必须是泛型");

        var res = t.IsGenericType && t.GetGenericTypeDefinition() == tt;
        return res;
    }

    /// <summary>
    /// 获取可以赋值给T的属性
    /// </summary>
    public static IList<T> GetCustomAttributesList<T>(this MemberInfo prop, bool inherit = true)
        where T : Attribute
    {
        var attrs = CustomAttributeExtensions.GetCustomAttributes(prop, inherit).ToArray();

        return attrs.OfType<T>().ToArray();
    }

    public static MethodInfo[] GetPrivateMethods(this Type t)
    {
        var res = t.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
        return res;
    }

    public static MethodInfo[] GetPublicStaticMethods(this Type t)
    {
        var res = t.GetMethods(BindingFlags.Public | BindingFlags.Static);
        return res;
    }
}