

import { QueryGoodsPaging, SearchView } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box, Chip, Typography } from '@mui/material';

export default function ({
  model,
  query,
  onChange,
}: {
  model: SearchView;
  query: QueryGoodsPaging;
  onChange: (e: QueryGoodsPaging) => void;
}) {
  const data = model.Tags || [];

  if (isArrayEmpty(data)) {
    return null;
  }

  return (
    <>
      <Box sx={{ px: 2, py: 1 }}>
        <Typography variant="overline" color="text.disabled" gutterBottom>
          标签
        </Typography>
        <Box sx={{ mt: 1 }}>
          {data.map((x, index) => {
            return (
              <Chip
                key={index}
                //variant='outlined'
                size="small"
                sx={{ mr: 1, mb: 1 }}
                label={x.Name}
                onClick={() => {
                  onChange({
                    ...query,
                    TagId: x.Id,
                  });
                }}
              />
            );
          })}
        </Box>
      </Box>
    </>
  );
}
