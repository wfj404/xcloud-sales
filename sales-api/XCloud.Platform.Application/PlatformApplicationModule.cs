﻿global using System;
global using System.Linq;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.DependencyInjection.Extensions;
global using Microsoft.Extensions.Logging;
global using Volo.Abp;
global using Volo.Abp.Auditing;
global using Volo.Abp.Uow;
global using XCloud.Core.Cache;
global using XCloud.Core.Extension;
using System.Threading.Tasks;
using Masuit.Tools.Config;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Logging;
using Volo.Abp.Authorization;
using Volo.Abp.BlobStoring.FileSystem;
using Volo.Abp.Modularity;
using Volo.Abp.MultiTenancy;
using XCloud.Application;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.AspNetMvc;
using XCloud.EntityFrameworkCore;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Core;
using XCloud.Platform.Application.Extension;
using XCloud.Platform.Application.Job;
using XCloud.Platform.Application.Localization;
using XCloud.Platform.Application.Mapper;
using XCloud.Platform.Application.Service.Storage;

namespace XCloud.Platform.Application;

[DependsOn(
    typeof(XCloudApplicationModule),
    typeof(XCloudAspNetMvcModule),
    typeof(AbpBlobStoringFileSystemModule),
    typeof(XCloudEntityFrameworkCoreModule),
    typeof(AbpAuthorizationModule)
)]
[MethodMetric]
public class PlatformApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //tools
        context.Services.GetConfiguration().AddToMasuitTools();
        context.ConfigStorageAll();
        context.ConfigPlatformLocalization();
        context.AddAutoMapperProfile<PlatformAutoMapperConfiguration>();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //add user authentication support
        //warning: add after identity server
        //or identity server will change default scheme
        context.Services.AddUserAuthentication();
        context.Services.AddAuthorizationHandlers();

        IdentityModelEventSource.ShowPII = true;
        context.Services.Configure<AuthenticationOptions>(option =>
        {
            //set default auth scheme
            option.DefaultScheme = AuthConstants.Scheme.JwtTokenScheme;
        });

        Configure<AbpMultiTenancyOptions>(option => option.IsEnabled = false);
        Configure<AbpTenantResolveOptions>(option => { option.TenantResolvers.Clear(); });
    }

    public override void OnPreApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        Task.Run(() => app.TryCreatePlatformDatabase()).Wait();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        Task.Run(() => app.ApplicationServices.StartPlatformJobsAsync()).Wait();
    }
}