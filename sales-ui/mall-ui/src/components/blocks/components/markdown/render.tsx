import XMarkdownView from '@/components/markdown';
import { isStringEmpty } from '@/utils/utils';

import { MarkdownBlockProps } from './utils';

export default ({ data }: { data?: MarkdownBlockProps }) => {

  const markdownText = data?.markdownText;

  if (!markdownText || isStringEmpty(markdownText)) {
    return null;
  }

  return <>
    <XMarkdownView md={markdownText} />
  </>;
};
