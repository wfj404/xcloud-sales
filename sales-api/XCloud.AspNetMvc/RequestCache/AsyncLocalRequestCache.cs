﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace XCloud.AspNetMvc.RequestCache;

public class AsyncLocalRequestCache : IRequestCacheProvider
{
    private static readonly AsyncLocal<ConcurrentDictionary<string, object>> AsyncLocal =
        new AsyncLocal<ConcurrentDictionary<string, object>>();

    public AsyncLocalRequestCache()
    {
        //
    }

    public T GetObject<T>(string key)
    {
        AsyncLocal.Value ??= new ConcurrentDictionary<string, object>();

        if (AsyncLocal.Value.TryGetValue(key, out var value))
        {
            if (value is T data)
            {
                return data;
            }
        }

        return default;
    }

    public bool HasKey(string key)
    {
        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        return AsyncLocal.Value.ContainsKey(key);
    }

    public void RemoveKey(string key)
    {
        AsyncLocal.Value ??= new ConcurrentDictionary<string, object>();

        AsyncLocal.Value.Remove(key, out var value);
    }

    public void SetObject<T>(string key, T data)
    {
        AsyncLocal.Value ??= new ConcurrentDictionary<string, object>();

        AsyncLocal.Value[key] = data;
    }
}