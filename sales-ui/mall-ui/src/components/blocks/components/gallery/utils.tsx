import { BlockItem, BlockItemProps, BlockPictureProps } from '../../utils';

export type GalleryStyleType =
  | '1-column'
  | '2-columns'
  | '3-columns'
  | '4-grids'
  | 'style-1';

export interface GalleryBlockProps extends BlockItemProps {
  style: GalleryStyleType;
  pictureList?: BlockPictureProps[];
  gutter?: number;
}

export const columns_1_demo: BlockItem<GalleryBlockProps> = {
  type: 'gallery',
  style: {
    marginBottom: 10,
  },
  body: {
    style: '1-column',
    pictureList: [
      {
        storage:
          'https://files.nsts.mvccms.cn/Upload/20210112/20210112132620_9045.jpg',
      },
    ],
  },
};

export const columns_2_demo: BlockItem<GalleryBlockProps> = {
  type: 'gallery',
  style: {
    marginBottom: 10,
  },
  body: {
    style: '2-columns',
    pictureList: [
      {
        storage:
          'https://files.nsts.mvccms.cn/Upload/20210411/20210411125545_6230.jpg',
      },
      {
        storage:
          'http://files.mvccms.cn/Upload/20201113/20201113103956_4506.jpg',
      },
    ],
  },
};

export const columns_style_demo: BlockItem<GalleryBlockProps> = {
  type: 'gallery',
  style: {
    marginBottom: 10,
  },
  body: {
    style: 'style-1',
    pictureList: [
      {
        storage:
          'https://files.nsts.mvccms.cn/Upload/20210221/20210221205737_9390.jpg',
      },
      {
        storage:
          'https://files.nsts.mvccms.cn/Upload/20210112/20210112132702_7671.jpg',
      },
      {
        storage:
          'https://files.nsts.mvccms.cn/Upload/20210112/20210112132727_1927.jpg',
      },
    ],
  },
};
