using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Report;

public interface IUserReportService : ISalesAppService
{
    Task<UserActivityGroupByHourResponse[]> UserActivityGroupByHourAsync(UserActivityGroupByHourInput dto);
    
    Task<int> TodayActiveUserCountAsync();
    
    int GetRegisteredUsersReport(int days);
}

public class UserReportService : SalesAppService, IUserReportService
{
    private readonly ISalesRepository<StoreUser> _userRepository;
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly IStoreUserService _userService;

    public UserReportService(ISalesRepository<StoreUser> userRepository,
        ISalesRepository<Order> orderRepository,
        IStoreUserService userService)
    {
        _userRepository = userRepository;
        _orderRepository = orderRepository;
        _userService = userService;
    }

    public async Task<UserActivityGroupByHourResponse[]> UserActivityGroupByHourAsync(UserActivityGroupByHourInput dto)
    {
        var db = await _userRepository.GetDbContextAsync();
        var logQuery = db.Set<ActivityLog>().AsNoTracking();

        if (dto.UserId != null)
            logQuery = logQuery.Where(x => x.StoreUserId == dto.UserId.Value);

        if (dto.StartTime != null)
            logQuery = logQuery.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            logQuery = logQuery.Where(x => x.CreationTime <= dto.EndTime.Value);

        var groupedQuery = logQuery.GroupBy(x => new { x.CreationTime.Hour, x.ActivityLogTypeId })
            .Select(x => new
            {
                x.Key.Hour,
                x.Key.ActivityLogTypeId,
                count = x.Count()
            });

        var data = await groupedQuery.OrderBy(x => x.Hour).ToArrayAsync();

        var response = data.Select(x => new UserActivityGroupByHourResponse
        {
            Hour = x.Hour,
            ActivityType = x.ActivityLogTypeId,
            Count = x.count
        }).ToArray();

        return response;
    }

    public async Task<int> TodayActiveUserCountAsync()
    {
        var db = await _userRepository.GetDbContextAsync();
        var query = db.Set<StoreUser>().AsNoTracking();

        var now = Clock.Now;
        var start = now.Date;
        var end = start.AddDays(1);

        query = query.Where(x => x.LastActivityTime >= start && x.LastActivityTime < end);

        var count = await query.Select(x => x.Id).Distinct().CountAsync();
        return count;
    }

    public virtual int GetRegisteredUsersReport(int days)
    {
        throw new NotImplementedException();
        //DateTime date = DateTime.Now.AddDays(-days);

        //var query = from c in _userRepository.Table
        //            where !c.IsDeleted &&
        //            c.CreationTime >= date
        //            select c;
        //int count = query.Count();
        //return count;
    }
}