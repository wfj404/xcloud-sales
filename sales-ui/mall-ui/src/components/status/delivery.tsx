import { ShippingStatus, ShippingStatusList } from '@/utils/biz';
import { OrderDto } from '@/utils/swagger';
import { Badge } from 'antd';

export default function(props: { model: OrderDto }) {
  const { model } = props;

  const name =
    ShippingStatusList.find((x) => x.id == model.ShippingStatusId)?.name ||
    '--';

  if (model.ShippingStatusId == ShippingStatus.Shipped) {
    return <Badge text={name} status={'success'} />;
  }

  if (model.ShippingStatusId == ShippingStatus.Shipping) {
    return <Badge text={name} status={'processing'} />;
  }

  return <Badge text={name} status={'default'} />;
}
