import { Button, Tooltip } from 'antd';
import { useState } from 'react';

export default ({ action, confirmText, hide }: {
  action: () => Promise<void>,
  hide?: boolean;
  confirmText?: string
}) => {
  const [loading, _loading] = useState<boolean>(false);

  const finalConfirmText = confirmText || '确定删除？';

  const executeDeleting = async () => {
    if (!confirm(finalConfirmText)) {
      return;
    }
    _loading(true);
    try {
      await action();
    } finally {
      _loading(false);
    }
  };

  if (hide) {
    return null;
  }

  return <>
    <Tooltip title={finalConfirmText}>
      <Button loading={loading}
              danger
              type={'link'}
              onClick={() => {
                executeDeleting();
              }}>删除</Button>
    </Tooltip>
  </>;
};
