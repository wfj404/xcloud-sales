﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Security;

namespace XCloud.Platform.Application.Mapping.Security;

public class ResourceAclMap : PlatformEntityTypeConfiguration<ResourceAcl>
{
    public override void Configure(EntityTypeBuilder<ResourceAcl> builder)
    {
        builder.ToTable("sys_resource_acl");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        
        //resource
        builder.Property(x => x.ResourceType).IsRequired().HasMaxLength(100);
        builder.Property(x => x.ResourceId).IsRequired().HasMaxLength(100);
        //permission type
        builder.Property(x => x.PermissionType).IsRequired().HasMaxLength(100);
        builder.Property(x => x.PermissionId).IsRequired().HasMaxLength(100);

        builder.Property(x => x.AccessControlType);

        base.Configure(builder);
    }
}