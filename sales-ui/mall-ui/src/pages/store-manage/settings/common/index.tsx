import { useRequest } from 'ahooks';
import { Button, Card, message } from 'antd';
import { useEffect, useState } from 'react';

import XStoreForm from '@/components/common/store_form';
import { apiClient } from '@/utils/client';
import { StoreDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

export default () => {
  const [model, _model] = useState<StoreDto>({});

  const get_store_request = useRequest(apiClient.mallManager.storeGetStore, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        _model(data.data?.Data || {});
      });
    },
  });

  const save_request = useRequest(apiClient.mallManager.storeUpdateStore, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        message.success('保存成功');
        get_store_request.run();
      });
    },
  });

  useEffect(() => {
    get_store_request.run();
  }, []);

  return (
    <>
      <Card
        loading={get_store_request.loading || save_request.loading}
        title="门店设置"
        extra={
          <Button
            type="primary"
            loading={save_request.loading}
            onClick={() => {
              save_request.run({ ...model });
            }}
          >
            保存
          </Button>
        }
      >
        <XStoreForm model={model} onChange={(e) => _model({ ...e })} />
      </Card>
    </>
  );
};
