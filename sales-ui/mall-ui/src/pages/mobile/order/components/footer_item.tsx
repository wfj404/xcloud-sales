import { Box, Typography } from '@mui/material';

export default ({ left, right, primary }: { left?: string | null; right?: string | null, primary?: boolean }) => {
  return (<Box sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
    <Typography
      variant='overline'
      color='primary'
      sx={{
        color: 'gray',
        display: 'inline',
      }}
    >
      {left}
    </Typography>

    <Typography
      variant='overline'
      color={primary ? 'primary' : 'text.disabled'}
      sx={{ display: 'inline' }}
    >
      {right}
    </Typography>
  </Box>);
};
