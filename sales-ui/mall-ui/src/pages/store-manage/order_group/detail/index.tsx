import { useLocalStorageState, useRequest } from 'ahooks';
import { Alert, FloatButton, Space } from 'antd';
import { useEffect, useState } from 'react';
import { useLocation } from 'umi';

import { parseQueryString } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderGroupDto } from '@/utils/swagger';
import { isStringEmpty, parseJsonOrEmpty } from '@/utils/utils';

import XOrderGroup from '../components/group_panel';
import XActions from './actions';

export default ({}: {}) => {
  const location = useLocation();
  const id = (parseQueryString(location.search) as { id?: string })?.id;

  const [has_change, _has_change] = useState(false);

  const [model, _model] = useLocalStorageState<OrderGroupDto>(`order-group`, {
    serializer(value) {
      return JSON.stringify(value || {});
    },
    deserializer(value) {
      return parseJsonOrEmpty(value) || {};
    },
  });

  const query_detail_request = useRequest(
    apiClient.mallManager.orderGroupGetById,
    {
      manual: true,
      onSuccess(data, params) {
        _model(data.data?.Data || {});
        _has_change(false);
      },
    },
  );

  const queryGroup = () => id && query_detail_request.run({ Id: id });

  useEffect(() => {
    queryGroup();
  }, []);

  const data_is_empty = isStringEmpty(model?.Id);

  if (query_detail_request.loading && data_is_empty) {
    return <h1>loading...</h1>;
  }

  if (data_is_empty) {
    return <h1>error model id</h1>;
  }

  return (
    <>
      <FloatButton.BackTop />
      <XOrderGroup
        group={model || {}}
        onChange={(e) => {
          _model(e);
          _has_change(true);
        }}
        actionRender={() => (
          <Space direction="vertical" style={{ width: '100%' }}>
            <XActions
              loadingData={query_detail_request.loading}
              group={model || {}}
              onChange={(e) => {
                _model(e);
                _has_change(true);
              }}
              onSaved={() => {
                _has_change(false);
              }}
              ok={() => {
                setTimeout(() => {
                  queryGroup();
                }, 1000);
              }}
            />
            {has_change && (
              <Alert message={'存在修改，请尽快保存'} type="error" />
            )}
          </Space>
        )}
      />
    </>
  );
};
