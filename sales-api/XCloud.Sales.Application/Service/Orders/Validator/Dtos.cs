using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Core.Dto;

namespace XCloud.Sales.Application.Service.Orders.Validator;

public interface IRuleParameter : IEntityDto
{
    //
}

public static class OrderConditionValidatorTypes
{
    public static string All => "*";

    public static string OrderPrice => "order-price";

    public static string OrderType => "order-type";

    public static string GoodsBrand => "goods-brand";

    public static string GoodsCategories => "goods-category";

    public static string Store => "store";
}

public class OrderValidatorRuleBase : IEntityDto
{
    public string ValidatorType { get; set; }

    public string Description { get; set; }
}

public class OrderValidatorRule<T> : OrderValidatorRuleBase
    where T : class, new()
{
    private T _configuration;

    [NotNull]
    public T Configuration
    {
        get => _configuration ??= new T();
        set => _configuration = value;
    }
}

public class OrderValidatorRule : OrderValidatorRule<dynamic>
{
    public string RuleJson { get; set; }
}

public class OrderConditionCheckDto : IEntityDto
{
    public OrderConditionCheckDto()
    {
        //
    }

    public OrderConditionCheckDto(OrderDto order)
    {
        Order = order;
    }

    public OrderDto Order { get; set; }
}

public class OrderConditionValidationResponse : ApiResponse<object>
{
    public OrderConditionValidationResponse()
    {
        //
    }
}