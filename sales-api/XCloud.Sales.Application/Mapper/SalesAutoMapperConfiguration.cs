﻿using AutoMapper;
using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Common;
using XCloud.Sales.Application.Domain.Coupons;
using XCloud.Sales.Application.Domain.Discounts;
using XCloud.Sales.Application.Domain.GenericOrders;
using XCloud.Sales.Application.Domain.GiftCards;
using XCloud.Sales.Application.Domain.Grades;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Domain.Notifications;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Payments;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Domain.ShoppingCart;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Service.AfterSale;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Common;
using XCloud.Sales.Application.Service.Coupons;
using XCloud.Sales.Application.Service.Discounts;
using XCloud.Sales.Application.Service.GenericOrders;
using XCloud.Sales.Application.Service.GiftCards;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Sales.Application.Service.Notifications;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Delivery.Express;
using XCloud.Sales.Application.Service.Shipping.Pickup;
using XCloud.Sales.Application.Service.ShoppingCart;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Mapper;

public class SalesAutoMapperConfiguration : Profile
{
    public SalesAutoMapperConfiguration()
    {
        CreateMap<TimeRangeDto, TimeRangeDto>().ReverseMap();
        CreateMap<ExceptionTime, ExceptionTime>().ReverseMap();
        CreateMap<OrderGroup, OrderGroupDto>().ReverseMap();
        CreateMap<ExternalSku, ExternalSkuDto>().ReverseMap();
        CreateMap<ExternalSkuMapping, ExternalSkuMappingDto>().ReverseMap();

        //base
        CreateMap<GoodsPicture, GoodsPictureDto>().ReverseMap();
        CreateMap<ActivityLog, ActivityLogDto>().ReverseMap();

        //notification
        CreateMap<Notification, NotificationDto>().ReverseMap();

        //store user
        CreateMap<StoreUser, StoreUserDto>().ReverseMap();
        CreateMap<PointsHistory, PointsHistoryDto>().ReverseMap();
        CreateMap<BalanceHistory, BalanceHistoryDto>().ReverseMap();
        CreateMap<StoreManager, StoreManagerDto>().ReverseMap();
        CreateMap<ShoppingCartItem, ShoppingCartItemDto>().ReverseMap();
        CreateMap<GiftCard, GiftCardDto>().ReverseMap();

        //grade
        CreateMap<Grade, GradeDto>().ReverseMap();
        CreateMap<GoodsGradePrice, GoodsGradePriceDto>().ReverseMap();
        CreateMap<StoreUserGradeMapping, StoreUserGradeMappingDto>().ReverseMap();

        //catalog
        CreateMap<Brand, BrandDto>().ReverseMap();
        CreateMap<Category, CategoryDto>().ReverseMap();
        CreateMap<Tag, TagDto>().ReverseMap();
        CreateMap<Goods, GoodsDto>().ReverseMap();
        CreateMap<Sku, SkuDto>().ReverseMap();
        CreateMap<Spec, SpecDto>().ReverseMap();
        CreateMap<SpecValue, SpecValueDto>().ReverseMap();
        CreateMap<ValueAddedItem, ValueAddedItemDto>().ReverseMap();
        CreateMap<ValueAddedItemGroup, ValueAddedItemGroupDto>().ReverseMap();

        //order
        CreateMap<Order, OrderDto>().ReverseMap();
        CreateMap<OrderItem, OrderItemDto>().ReverseMap();
        CreateMap<OrderNote, OrderNoteDto>().ReverseMap();
        CreateMap<PaymentBill, PaymentBillDto>().ReverseMap();
        CreateMap<RefundBill, RefundBillDto>().ReverseMap();
        CreateMap<OrderValueAddedItem, OrderValueAddedItemDto>().ReverseMap();
        CreateMap<OrderItemGift, OrderItemGiftDto>().ReverseMap();
        CreateMap<OrderCouponRecord, OrderCouponRecordDto>().ReverseMap();
        CreateMap<OrderReview, OrderReviewDto>().ReverseMap();
        CreateMap<OrderReviewItem, OrderReviewItemDto>().ReverseMap();

        //generic order
        CreateMap<GenericOrder, GenericOrderDto>().ReverseMap();

        //delivery
        CreateMap<DeliveryRecord, DeliveryRecordDto>().ReverseMap();
        CreateMap<DeliveryRecordItem, DeliveryRecordItemDto>().ReverseMap();
        CreateMap<DeliveryRecordTrack, DeliveryRecordTrackDto>().ReverseMap();
        CreateMap<FreightTemplate, FreightTemplateDto>().ReverseMap();
        CreateMap<PickupRecord, PickupRecordDto>().ReverseMap();

        //after sales
        CreateMap<AfterSales, AfterSalesDto>().ReverseMap();
        CreateMap<AfterSalesItem, AfterSalesItemDto>().ReverseMap();
        CreateMap<AfterSalesComment, AfterSalesCommentDto>().ReverseMap();

        //store
        CreateMap<Store, StoreDto>().ReverseMap();
        CreateMap<StoreGoodsMapping, StoreGoodsMappingDto>().ReverseMap();
        CreateMap<StoreRole, StoreRoleDto>().ReverseMap();

        //coupon
        CreateMap<Coupon, CouponDto>().ReverseMap();
        CreateMap<CouponUserMapping, CouponUserMappingDto>().ReverseMap();

        //discount
        CreateMap<Discount, DiscountDto>().ReverseMap();
    }
}