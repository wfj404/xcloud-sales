import { Swiper } from 'antd-mobile';
import { useState } from 'react';

import XEmpty from '@/components/common/empty';
import XItem from '@/components/goods/card/box';
import XDetailPreview from '@/components/goods/detail/detailPreview';
import { GoodsDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

export default ({ goods }: {
  goods: GoodsDto[];
}) => {
  const [detailId, _detailId] = useState<string | undefined>(undefined);

  if (isArrayEmpty(goods)) {
    return <XEmpty />;
  }

  return (
    <>
      <XDetailPreview
        detailId={detailId}
        onClose={() => {
          _detailId('');
        }}
      />
      
      <Swiper
        trackOffset={10}
        slideSize={40}
        defaultIndex={0}
        style={{
          '--track-padding': ' 0 0 10px',
        }}
      >
        {goods.map((x, index) => (
          <Swiper.Item key={index}>
            <Box
              onClick={() => {
                _detailId(x.Id || '');
              }}
              sx={{ py: 1, px: 1 }}
            >
              <XItem model={x} count={1} lazy={false} />
            </Box>
          </Swiper.Item>
        ))}
      </Swiper>
    </>
  );
};
