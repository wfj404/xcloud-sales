﻿namespace XCloud.Sales.Application.Domain.Stores;

/// <summary>
/// 用户角色关联
/// </summary>
public class StoreManagerRole : SalesBaseEntity<string>, IHasCreationTime
{
    public string RoleId { get; set; }

    public string ManagerId { get; set; }

    public DateTime CreationTime { get; set; }
}