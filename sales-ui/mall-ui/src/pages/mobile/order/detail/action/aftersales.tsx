import { Stepper } from 'antd-mobile';
import React, { useEffect, useState } from 'react';

import { getSkuName, OrderStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { myDayjs, parseAsDayjs } from '@/utils/dayjs';
import { AfterSalesDto, OrderDto, OrderItemDto } from '@/utils/swagger';
import { errorMsg, handleResponse, isArrayEmpty, isStringEmpty } from '@/utils/utils';
import { HomeRepairServiceOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Box, Button, Checkbox, Divider, TextField, Typography } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

interface SelectedItem {
  OrderItemId?: string;
  Quantity?: number;
}

export default function XAftersale(props: { model: OrderDto; ok: () => void }) {
  const { model, ok } = props;
  const items = model.Items || [];

  const [show, _show] = useState(false);

  const [aftersaleForm, _aftersaleForm] = React.useState<AfterSalesDto>({});
  const [selectedItems, _selectedItems] = React.useState<SelectedItem[]>([]);
  const [loadingSave, _loadingSave] = React.useState(false);

  const save = () => {
    if (isArrayEmpty(selectedItems)) {
      return;
    }
    if (
      isStringEmpty(aftersaleForm.ReasonForReturn) ||
      isStringEmpty(aftersaleForm.RequestedAction)
    ) {
      errorMsg('请输入售后诉求和原因');
      return;
    }

    _loadingSave(true);
    apiClient.mall
      .afterSaleCreateAfterSales({
        ...aftersaleForm,
        OrderId: model.Id,
        Items: selectedItems,
      })
      .then((res) => {
        handleResponse(res, () => {
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  React.useEffect(() => {
    if (show) {
      _selectedItems(
        items.map<SelectedItem>((x) => ({
          OrderItemId: x.Id || '',
          Quantity: x.Quantity || 0,
        })),
      );
      _aftersaleForm({});
    }
  }, [show]);

  const buildItemsRow = (row: OrderItemDto) => {
    let name = getSkuName(row.Sku || {});

    let selectedObj = selectedItems.find((x) => x.OrderItemId == row.Id);
    let selected = selectedObj != undefined;

    return (
      <>
        <Box
          sx={{
            py: 2,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Typography
            variant="overline"
            color="primary"
            component="div"
            sx={{
              flexGrow: 1,
            }}
          >
            {name}
          </Typography>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          >
            <Stepper
              style={{
                marginLeft: 10,
                marginRight: 10,
                width: 100,
              }}
              value={selectedObj?.Quantity || 0}
              min={1}
              max={row.Quantity}
              disabled={!selected}
              onChange={(e) => {
                if (selected) {
                  const obj: SelectedItem = {
                    ...selectedObj,
                    Quantity: e,
                  };
                  _selectedItems((x) => [
                    ...x.filter((x) => x.OrderItemId != row.Id),
                    obj,
                  ]);
                }
              }}
            />
            <Checkbox
              checked={selected}
              onChange={(e) => {
                const exclued: SelectedItem[] = selectedItems.filter(
                  (x) => x.OrderItemId != row.Id,
                );
                if (e.target.checked) {
                  exclued.push({
                    OrderItemId: row.Id || undefined,
                    Quantity: row.Quantity,
                  });
                }
                _selectedItems(exclued);
              }}
            />
          </Box>
        </Box>
        <Divider />
      </>
    );
  };

  if (model.OrderStatusId == OrderStatus.Finished) {
  } else {
    return null;
  }

  const days =
    parseAsDayjs(model.CompleteTime)?.diff(myDayjs.utc(), 'day') || 99999;

  if (Math.abs(days) > 7) {
    //return null;
  }

  return (
    <>
      <Button
        startIcon={<HomeRepairServiceOutlined />}
        variant={'outlined'}
        size={'large'}
        onClick={() => {
          _show(true);
        }}
      >
        申请售后
      </Button>
      <Dialog
        open={show}
        onClose={() => {
          _show(false);
        }}
        fullWidth
        scroll="body"
      >
        <DialogTitle>发起售后</DialogTitle>
        <DialogContent>
          <DialogContentText>
            请选择需要售后的商品，订单只能发起一次售后
          </DialogContentText>
          <Box
            sx={{
              mb: 2,
            }}
          >
            {items?.map((x, index) => (
              <Box key={index}>{buildItemsRow(x)}</Box>
            ))}
          </Box>
          <Box sx={{ mb: 2 }}>
            <TextField
              sx={{ mb: 2, width: '100%' }}
              placeholder={'输入退货或者换货'}
              label={'售后诉求'}
              value={aftersaleForm.RequestedAction}
              onChange={(e) => {
                _aftersaleForm({
                  ...aftersaleForm,
                  RequestedAction: e.target.value,
                });
              }}
            />
            <TextField
              sx={{ mb: 2, width: '100%' }}
              multiline
              minRows={3}
              placeholder={'简单描述一下售后原因'}
              label={'售后原因'}
              value={aftersaleForm.ReasonForReturn}
              onChange={(e) => {
                _aftersaleForm({
                  ...aftersaleForm,
                  ReasonForReturn: e.target.value,
                });
              }}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              _show(false);
            }}
          >
            取消
          </Button>
          <LoadingButton
            onClick={() => {
              save();
            }}
            disabled={isArrayEmpty(selectedItems)}
            loading={loadingSave}
          >
            发起售后
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}
