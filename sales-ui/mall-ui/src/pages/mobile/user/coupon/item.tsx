import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { CouponUserMappingDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Typography } from '@mui/material';
import Card from '@mui/material/Card';
import { styled } from '@mui/material/styles';

const StyledCard = styled(Card)(() => ({
  position: 'relative',
  borderRadius: 16,
  padding: 12,
  backgroundColor: '#e5fcfb',
  minWidth: 300,
  boxShadow: '0 0 20px 0 rgba(0,0,0,0.12)',
  transition: '0.3s',
  '&:hover': {
    transform: 'translateY(-3px)',
    boxShadow: '0 4px 20px 0 rgba(0,0,0,0.12)',
  },
}));

const StyledDiv = styled('div')(() => ({
  position: 'absolute',
  bottom: 0,
  right: 0,
  transform: 'translate(70%, 50%)',
  borderRadius: '50%',
  backgroundColor: 'rgba(71, 167, 162, 0.12)',
  padding: '40%',

  '&:before': {
    position: 'absolute',
    borderRadius: '50%',
    content: '""',
    display: 'block',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: '-16%',
    backgroundColor: 'rgba(71, 167, 162, 0.08)',
  },
}));

export default ({ model }: { model: CouponUserMappingDto }) => {
  return (
    <StyledCard>
      <Box sx={{ mr: 2 }}>
        <Box sx={{ mb: 3 }}>
          <Typography
            sx={{
              color: '#fb703c',
              fontSize: '1.125rem',
              fontWeight: 700,
              lineHeight: 1.6,
              letterSpacing: '0.0075em',
            }}
            gutterBottom
          >
            {model.Coupon?.Name || '--'}
          </Typography>
          <Typography
            sx={{
              color: '#fb703c',
              fontSize: '1.125rem',
              fontWeight: 700,
              lineHeight: 1.6,
              letterSpacing: '0.0075em',
            }}
            gutterBottom
          >
            {`优惠金额：${model.Coupon?.Price || 0}`}
          </Typography>
          {isStringEmpty(model.ExpiredAt) || (
            <Typography
              sx={{
                color: '#48bbb5',
                fontSize: '0.875rem',
                fontWeight: 500,
              }}
              gutterBottom
            >
              过期时间：{formatRelativeTimeFromNow(model.ExpiredAt || '')}
            </Typography>
          )}
        </Box>
      </Box>
      <StyledDiv />
    </StyledCard>
  );
};
