using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Application.Authentication;

public class OAuthServerOption : IEntityDto
{
    public string Server { get; set; }

    public string JwtSecretKey { get; set; }

    public void Validate()
    {
        if (string.IsNullOrWhiteSpace(this.Server))
            throw new ArgumentNullException(nameof(this.Server));

        if (string.IsNullOrWhiteSpace(this.JwtSecretKey))
            throw new ArgumentNullException(nameof(this.JwtSecretKey));
    }
}