﻿using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/goods")]
public class GoodsController : ShopBaseController
{
    private readonly GoodsViewService _goodsViewService;
    private readonly IGoodsService _goodsService;

    public GoodsController(GoodsViewService goodsViewService, IGoodsService goodsService)
    {
        _goodsViewService = goodsViewService;
        _goodsService = goodsService;
    }

    [HttpPost("by-ids")]
    public async Task<ApiResponse<GoodsDto[]>> ByIdsAsync([FromBody] string[] ids)
    {
        if (ValidateHelper.IsEmptyCollection(ids))
            throw new ArgumentException(message: nameof(ids));

        var storeId = await this.CurrentStoreContext.GetRequiredStoreIdAsync();

        var datalist = await this._goodsService.GetByIdsAsync(ids);

        if (datalist.Any())
        {
            var loginUser = await StoreUserAuthService.GetStoreUserOrNullAsync();

            await this._goodsViewService.AttachPreviewInfoForListAsync(datalist, storeId, loginUser);
        }

        return new ApiResponse<GoodsDto[]>(datalist);
    }

    [HttpPost("detail")]
    public async Task<ApiResponse<GoodsDto>> QueryGoodsDetailAsync([FromBody] IdDto<string> dto)
    {
        var goods = await _goodsViewService.QueryGoodsDetailByIdAsync(dto.Id,
            new CacheStrategy { Cache = true });

        if (goods == null || !goods.IsStatusOk())
            return new ApiResponse<GoodsDto>().SetError("goods is deleted");

        var loginUserOrNull = await StoreUserAuthService.GetStoreUserOrNullAsync();

        if (loginUserOrNull != null)
        {
            await SalesEventBusService.NotifyInsertActivityLog(new ActivityLog
            {
                StoreUserId = loginUserOrNull.Id,
                ActivityLogTypeId = ActivityLogType.VisitGoods,
                SubjectId = goods.Id,
                SubjectType = ActivityLogSubjectType.Goods,
                Comment = "查询商品详情"
            });
        }
        else
        {
            await SalesEventBusService.NotifyInsertActivityLog(new ActivityLog
            {
                StoreUserId = default,
                ActivityLogTypeId = ActivityLogType.VisitGoods,
                SubjectId = goods.Id,
                SubjectType = ActivityLogSubjectType.Goods,
                Comment = "未登录查询商品详情"
            });
        }

        return new ApiResponse<GoodsDto>(goods);
    }
}