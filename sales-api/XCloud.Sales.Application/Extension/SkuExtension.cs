﻿using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Extension;

public static class SkuExtension
{
    public static IList<SpecItemDto[]> GenerateCombination(this SpecItemDto[] mappings, int? maxCount = null)
    {
        maxCount ??= int.MaxValue;

        if (maxCount.Value <= 0)
            throw new ArgumentException($"{nameof(GenerateCombination)}:{nameof(maxCount)}");

        if (!mappings.Any())
            return Array.Empty<SpecItemDto[]>();

        if (mappings.Any(x => x.SpecId <= 0))
            throw new ArgumentNullException($"{nameof(mappings)}.spec-id");

        if (mappings.Select(x => $"{x.SpecId}:{x.SpecValueId}").AnyDuplicateString())
            throw new ArgumentException($"{nameof(mappings)}.duplicate key-value");

        var groupedData = mappings
            .GroupBy(x => x.SpecId)
            .Select(x => new
            {
                x.Key,
                Items = x.ToArray()
            }).ToArray();

        var groupedItems = groupedData.Select(x => x.Items).ToArray();

        var finalList = new List<SpecItemDto[]>();

        var temp = new SpecItemDto[groupedItems.Length];

        //trigger
        Generate(loopIndex: 0);

        return finalList;

        void Generate(int loopIndex)
        {
            var isLastLoop = loopIndex >= groupedItems.Length - 1;

            foreach (var item in groupedItems[loopIndex])
            {
                if (finalList.Count >= maxCount.Value)
                    break;

                temp[loopIndex] = item;

                if (isLastLoop)
                {
                    //last loop
                    var currentCombination = new List<SpecItemDto>();
                    currentCombination.AddRange(temp);
                    
                    finalList.Add(currentCombination.ToArray());
                }
                else
                {
                    Generate(loopIndex + 1);
                }
            }
        }
    }
}