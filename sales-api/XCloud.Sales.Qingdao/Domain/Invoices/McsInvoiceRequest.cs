﻿using System;
using Volo.Abp;
using Volo.Abp.Auditing;
using XCloud.Sales.Application.Domain;
using XCloud.Sales.Qingdao.Service.Invoices;

namespace XCloud.Sales.Qingdao.Domain.Invoices;

public class McsInvoiceRequest : SalesBaseEntity<string>, IHasCreationTime, ISoftDelete
{
    public string BizType { get; set; }

    public decimal TotalAmount { get; set; }

    public string Status { get; set; } = McsInvoiceRequestStatus.Pending;

    public string UserInvoiceId { get; set; }

    public string UserInvoiceData { get; set; }

    public DateTime? InvoicingTime { get; set; }

    public string Email { get; set; }

    public bool Picked { get; set; }

    public DateTime? PickedTime { get; set; }

    public string McsUserId { get; set; }

    public int PickupLocationId { get; set; }

    public string PickupLocationName { get; set; }

    public string AttachmentMetaId { get; set; }

    public DateTime CreationTime { get; set; }

    public bool IsDeleted { get; set; }
}