using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class ValueAddedItemMap : SalesEntityTypeConfiguration<ValueAddedItem>
{
    public override void Configure(EntityTypeBuilder<ValueAddedItem> builder)
    {
        builder.ToTable("sales_value_added_item");

        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.GroupId).RequiredId();
        builder.Property(x => x.PriceOffset).PricePrecision();

        base.Configure(builder);
    }
}