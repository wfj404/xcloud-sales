﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using XCloud.Core.Json;

namespace XCloud.Core.Cache;

public static class CacheExtension
{
    public static CacheResult<T> DeserializeCacheResultFromString<T>(this IJsonDataSerializer jsonDataSerializer,
        string json)
    {
        if (string.IsNullOrWhiteSpace(json))
            throw new ArgumentNullException(nameof(json));

        var res = jsonDataSerializer.DeserializeFromString<CacheResult<T>>(json);
        if (res == null)
            throw new CacheException("缓存中的数据无法转换成entity，转换后为null");

        res.Error = default;

        return res;
    }

    public static string SerializeCacheResultToString<T>(this IJsonDataSerializer jsonDataSerializer, T data)
    {
        var cacheObject = new CacheResult<T>()
        {
            Data = data
        };

        var res = jsonDataSerializer.SerializeToString(cacheObject);

        return res;
    }

    public static CacheProviderOption GetRequiredCacheProviderOption(this IConfiguration configuration)
    {
        var section = configuration.GetRequiredSection("CachingProvider");

        var option = new CacheProviderOption();

        section.Bind(option);

        return option;
    }

    public static async Task<T> ExecuteWithPolicyAsync<T>(this ICacheProvider provider, Func<Task<T>> source,
        CacheOption<T> option, CacheStrategy strategy)
    {
        if (strategy.Cache ?? false)
        {
            return await provider.GetOrSetAsync(source, option);
        }
        else if (strategy.CacheOnly ?? false)
        {
            var result = await provider.GetAsync<T>(option.Key);
            return result.Data;
        }
        else if (strategy.Refresh ?? false)
        {
            return await provider.RefreshCacheAsync(source, option);
        }
        else if (strategy.RemoveCache ?? false)
        {
            await provider.RemoveAsync(option.Key);
            return default;
        }
        else if (strategy.Source ?? false)
        {
            return await source.Invoke();
        }

        throw new NotSupportedException(nameof(strategy));
    }
}