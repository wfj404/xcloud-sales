import { Card } from 'antd';
import React, { useEffect } from 'react';

import { apiClient } from '@/utils/client';
import { DeliveryRecordDto, OrderDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XDeliveryList from '../../../shipping/delivery/list';

export default ({ model, ok }: { model: OrderDto, ok: () => void }) => {

  const [data, _data] = React.useState<DeliveryRecordDto[]>([]);
  const [loading, _loading] = React.useState<boolean>(false);

  const queryList = () => {
    if (isStringEmpty(model.Id)) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .deliveryByOrderId({ Id: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList();
  }, [model]);

  return (
    <>
      <Card
        loading={loading}
        title='配送单'
        style={{
          marginBottom: 10,
          borderColor: 'green',
        }}
      >
        <XDeliveryList
          data={data}
          pagination={false}
          refresh={() => {
            //queryList();
            ok();
          }}
        />
      </Card>
    </>
  );
};
