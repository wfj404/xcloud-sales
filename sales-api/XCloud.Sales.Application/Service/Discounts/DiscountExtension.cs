using XCloud.Sales.Application.Domain.Discounts;

namespace XCloud.Sales.Application.Service.Discounts;

public static class DiscountExtension
{
    public static T SelectDiscount<T>(this IEnumerable<T> discounts)
        where T : Discount
    {
        return discounts
            .OrderByDescending(x => x.LimitedToSku.ToBoolInt())
            .ThenByDescending(x => x.Sort)
            .ThenByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .FirstOrDefault();
    }
}