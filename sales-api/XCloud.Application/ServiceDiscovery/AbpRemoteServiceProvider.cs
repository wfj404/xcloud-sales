using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Http.Client;
using XCloud.Core.Dto;

namespace XCloud.Application.ServiceDiscovery;

[ExposeServices(typeof(IServiceDiscoveryProvider))]
public class AbpRemoteServiceProvider : IServiceDiscoveryProvider, IScopedDependency
{
    private readonly IRemoteServiceConfigurationProvider _remoteServiceConfigurationProvider;

    public AbpRemoteServiceProvider(IRemoteServiceConfigurationProvider remoteServiceConfigurationProvider)
    {
        _remoteServiceConfigurationProvider = remoteServiceConfigurationProvider;
    }

    public string ProviderName => nameof(AbpRemoteServiceProvider);
    
    public int Order => default;

    public async Task<ServiceDiscoveryResponseDto> ResolveServiceOrNullAsync(string name)
    {
        var response = await _remoteServiceConfigurationProvider.GetConfigurationOrDefaultOrNullAsync(name);

        if (response != null)
        {
            var service = new ServiceDiscoveryResponseDto();
            service.SetData(response.BaseUrl);
            return service;
        }

        return null;
    }
}