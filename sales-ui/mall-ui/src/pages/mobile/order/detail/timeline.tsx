import { useState } from 'react';

import XLoading from '@/components/common/loading';
import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { OrderDto, OrderNoteDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import {
    CelebrationOutlined, CheckOutlined, CloseOutlined, HourglassTopOutlined, InventoryOutlined,
    LocalShippingOutlined, MessageOutlined, PaidOutlined
} from '@mui/icons-material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { TimelineOppositeContent, timelineOppositeContentClasses } from '@mui/lab';
import Timeline from '@mui/lab/Timeline';
import TimelineConnector from '@mui/lab/TimelineConnector';
import TimelineContent from '@mui/lab/TimelineContent';
import TimelineDot from '@mui/lab/TimelineDot';
import TimelineItem from '@mui/lab/TimelineItem';
import TimelineSeparator from '@mui/lab/TimelineSeparator';
import { Box } from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';

export default function({ model }: { model: OrderDto }) {
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<OrderNoteDto[]>([]);

  const queryList = (id: string) => {
    if (isStringEmpty(id)) {
      _data([]);
      return;
    }
    _loading(true);
    apiClient.mall
      .orderQueryOrderNotes({
        OrderId: id,
      })
      .then((res) => {
        _data(res.data.Data || []);
      })
      .finally(() => {
        _loading(false);
      });
  };

  const renderTimelineDot = (x: OrderNoteDto) => {
    const type = x.OrderNoteExtended?.NoteType;

    if (type == 'created') {
      return <TimelineDot variant='outlined' color='info'>
        <CelebrationOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'paid') {
      return <TimelineDot variant='outlined' color='warning'>
        <PaidOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'processing') {
      return <TimelineDot variant='outlined' color='secondary'>
        <HourglassTopOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'shipping') {
      return <TimelineDot variant='outlined' color='grey'>
        <LocalShippingOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'shipped') {
      return <TimelineDot variant='outlined' color='primary'>
        <InventoryOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'finished') {
      return <TimelineDot variant='outlined' color='success'>
        <CheckOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'closed') {
      return <TimelineDot variant='outlined' color='error'>
        <CloseOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    if (type == 'message') {
      return <TimelineDot variant='outlined' color='warning'>
        <MessageOutlined fontSize={'small'} />
      </TimelineDot>;
    }

    return <TimelineDot variant='outlined' color='primary' />;
  };

  const renderExtendInfo = (x: OrderNoteDto) => {
    const type = x.OrderNoteExtended?.NoteType;

    if (type == 'paid') {
      const tips = x.OrderNoteExtended?.Data?.Tips;
      if (tips && tips.length > 0) {
        return <Typography variant={'caption'} color={'yellow'} component={'div'}>{tips}</Typography>;
      }
    }

    if (type == 'closed') {
      const comment = x.OrderNoteExtended?.Data?.Comment;
      if (comment && comment.length > 0) {
        return <Typography variant={'caption'} color={'red'} component={'div'}>{comment}</Typography>;
      }
    }

    return null;
  };

  const renderTimeline = () => {
    if (loading) {
      return <XLoading />;
    }
    if (data.length <= 0) {
      return null;
    }
    return (<Timeline sx={{
      [`& .${timelineOppositeContentClasses.root}`]: {
        flex: 0.2,
      },
    }}>
      {data.map((x, i) => (<TimelineItem key={i}>
        <TimelineOppositeContent color='textSecondary'>
        </TimelineOppositeContent>
        <TimelineSeparator>
          {renderTimelineDot(x)}
          {i < data.length - 1 && <TimelineConnector />}
        </TimelineSeparator>
        <TimelineContent>
          <Typography variant={'caption'} color={'text.disabled'}
                      component={'div'}>{formatRelativeTimeFromNow(x.CreationTime)}</Typography>
          <div>{x.Note || '--'}</div>
          {renderExtendInfo(x)}
        </TimelineContent>
      </TimelineItem>))}
    </Timeline>);
  };

  return (
    <>
      <Box sx={{ my: 1 }}>
        <Accordion
          onChange={(e, expanded) => {
            if (expanded) {
              queryList(model?.Id || '');
            }
          }}
        >
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>查看流转过程</Typography>
          </AccordionSummary>
          <AccordionDetails>
            {renderTimeline()}
          </AccordionDetails>
        </Accordion>
      </Box>
    </>
  );
}
