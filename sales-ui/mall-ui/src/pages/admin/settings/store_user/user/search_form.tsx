import { QueryUserPaging } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input } from 'antd';
import { useEffect, useState } from 'react';

const App = (props: {
  query: QueryUserPaging;
  onSearch: (d: QueryUserPaging) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QueryUserPaging>({});

  const triggerSearch = (e: QueryUserPaging) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form({ ...query });
  }, []);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        onFinish={(e) => triggerSearch(form)}
        autoComplete='off'
        layout='inline'
      >
        <Form.Item label='关键词' name='Name'>
          <Input value={form.Keyword || ''}
                 onChange={e => _form(x => ({ ...x, Keyword: e.target.value }))}
          />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type='primary' htmlType='submit' icon={<SearchOutlined />}>
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default App;
