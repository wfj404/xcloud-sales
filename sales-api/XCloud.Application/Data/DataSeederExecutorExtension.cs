using System;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;

namespace XCloud.Application.Data;

public static class DataSeederExecutorExtension
{
    public static async Task StartDataSeederJob(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var backgroundJobClient = s.ServiceProvider.GetRequiredService<IBackgroundJobClient>();

        backgroundJobClient.Schedule<DataSeederExecutor>(
            methodCall: x => x.Execute(),
            delay: TimeSpan.FromSeconds(10));

        await Task.CompletedTask;
    }
}