﻿namespace XCloud.Sales.Application.Domain.Common;

public class ExternalSku : SalesBaseEntity<string>, IHasCreationTime
{
    public string SystemName { get; set; }

    public string ExternalSkuId { get; set; }

    public string CombineId { get; set; }

    public string GoodsName { get; set; }

    public string SkuName { get; set; }

    public decimal Price { get; set; }

    public string DataJson { get; set; }

    public DateTime CreationTime { get; set; }
}