using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Steeltoe.Extensions.Configuration.Placeholder;
using XCloud.Core;
using XCloud.Sales.Application.Core;

namespace XCloud.Sales.Qingdao.Database.EntityFrameworkCore;

public class ShopMigrationDbContextFactory : IDesignTimeDbContextFactory<ShopSqlServerDbContext>
{
    public ShopSqlServerDbContext CreateDbContext(string[] args)
    {
        var optionBuilder = new DbContextOptionsBuilder<ShopSqlServerDbContext>();
        //optionBuilder.UseSqlite("./xx.db");
        var connectionString = BuildConfiguration()
            .GetRequiredConnectionString(SalesConstants.DbConnectionNames.SqlServer);
        optionBuilder.UseSqlServer(connectionString: connectionString);

        return new ShopSqlServerDbContext(optionBuilder.Options);
    }

    private IConfigurationRoot BuildConfiguration()
    {
        var configBuilder = new ConfigurationBuilder();

        configBuilder.AddJsonFile(
            Path.Combine(Directory.GetCurrentDirectory(), "../XCloud.Sales.Qingdao.WebApi/appsettings.json"),
            optional: false);

        configBuilder.AddPlaceholderResolver();

        return configBuilder.Build();
    }
}