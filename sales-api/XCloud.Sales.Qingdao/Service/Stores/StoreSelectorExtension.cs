﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp.Modularity;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.Service.Stores.Context.Contributors;

namespace XCloud.Sales.Qingdao.Service.Stores;

public static class StoreSelectorExtension
{
    public static void ReplaceStoreSelector(this ServiceConfigurationContext context)
    {
        context.Services.RemoveAll<ICurrentStoreContext>();
        context.Services.AddTransient<ICurrentStoreContext, QingdaoCurrentStoreContext>();

        //contributors
        context.Services.RemoveAll<ICurrentStoreSelectorContributor>();
        context.Services.TryAddTransient<QingdaoLocationBasedStoreSelectorContributor>();
        context.Services.TryAddTransient<QingdaoConfiguredDefaultStoreSelectorContributor>();
        context.Services.TryAddTransient<UserSelectedStoreSelectorContributor>();
        context.Services.TryAddTransient<GetOrCreateDefaultStoreSelectorContributor>();
    }
}