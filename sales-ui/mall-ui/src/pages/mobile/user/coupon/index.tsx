import { useInfiniteScroll } from 'ahooks';
import { ErrorBlock } from 'antd-mobile';
import React, { useEffect, useState } from 'react';

import ScrollLoading from '@/components/common/scroll/loading';
import XHeader from '@/components/mobile/simpleHeader';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  CouponUserMappingDto,
  QueryUserCouponPagingInput,
} from '@/utils/swagger';
import { Box, Container } from '@mui/material';

import XUserCouponItem from './item';

export default function () {
  const [finalQuery, _finalQuery] = React.useState<QueryUserCouponPagingInput>(
    {},
  );

  const { loading, loadingMore, mutate, noMore, data } = useInfiniteScroll<{
    list: CouponUserMappingDto[];
    res?: PagedResponse<CouponUserMappingDto>;
  }>(
    async (currentData) => {
      const res = await apiClient.mall.couponQueryUserCouponPaging({
        ...finalQuery,
        Page: (currentData?.res?.PageIndex || 0) + 1,
      });

      return {
        list: res.data.Items || [],
        res: res.data,
      };
    },
    {
      target: window.document,
      isNoMore: (x) => x?.res?.IsEmpty || false,
      reloadDeps: [finalQuery],
    },
  );

  const items = data?.list || [];

  React.useEffect(() => {
    mutate({
      list: [],
    });
  }, [finalQuery]);

  return (
    <>
      <XHeader>
        <Container maxWidth="sm" disableGutters>
          <Box sx={{ p: 1 }}>
            {items.length <= 0 && <ErrorBlock status="empty" />}
            {items.map((x, index) => {
              return (
                <Box
                  key={index}
                  sx={{ mb: 2 }}
                  onClick={() => {
                    //
                  }}
                >
                  <XUserCouponItem model={x} />
                </Box>
              );
            })}
          </Box>
          <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
        </Container>
      </XHeader>
    </>
  );
}
