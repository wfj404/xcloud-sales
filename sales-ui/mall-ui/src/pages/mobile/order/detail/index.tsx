import qs from 'query-string';
import { useEffect, useState } from 'react';
import { history } from 'umi';

import DotsLoading from '@/components/common/loading/dots';
import { ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Alert, Box, Divider } from '@mui/material';

import XAction from './action';
import XAftersales from './aftersale';
import XBill from './bill';
import XDelivery from './delivery';
import XGoods from './goods';
import XPickup from './pickup';
import XStatus from './status';
import XSummary from './summary';
import XTimeline from './timeline';

interface UrlQuery {
  id?: string;
}

export default function ({ orderId, ok }: { orderId: string; ok: () => void }) {
  const [id, _id] = useState<string>('');
  const [loading, _loading] = useState<boolean>(false);
  const [data, _data] = useState<OrderDto>({});

  const queryOrder = () => {
    if (isStringEmpty(id)) {
      _data({});
      return;
    }
    _loading(true);
    apiClient.mall
      .orderDetail({
        Id: id,
      })
      .then((res) => {
        _data(res.data.Data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryOrder();
  }, [id]);

  useEffect(() => {
    _id(orderId);
  }, [orderId]);

  useEffect(() => {
    const query = qs.parse(history.location.search) as UrlQuery;
    let queryId = query?.id;
    if (queryId && !isStringEmpty(queryId)) {
      _id(queryId);
    }
  }, []);

  if (loading) {
    return <DotsLoading />;
  }

  if (isStringEmpty(data.Id)) {
    return <Alert>订单不存在</Alert>;
  }

  return (
    <>
      <Box
        sx={{
          my: 2,
          py: 2,
        }}
      >
        <Box sx={{ mb: 3 }}>
          {<XStatus model={data} />}
          {<XAftersales model={data} />}
        </Box>
        <Divider />
      </Box>
      <Box sx={{ my: 2 }}>
        <XAction
          model={data}
          ok={() => {
            queryOrder();
            ok && ok();
          }}
        />
      </Box>
      <XSummary model={data} />
      <XGoods model={data} />
      <XBill model={data} />
      {data.ShippingMethodId != ShippingMethod.Pickup && (
        <XDelivery model={data} />
      )}
      {data.ShippingMethodId == ShippingMethod.Pickup && (
        <XPickup model={data} />
      )}
      <XTimeline model={data} />
    </>
  );
}
