﻿namespace XCloud.Core.Extension;

public class BooleanShould
{
    private readonly bool _boolean;

    public BooleanShould(bool boolean)
    {
        _boolean = boolean;
    }

    public void BeTrue(string message = null)
    {
        if (!_boolean)
            throw new ArgumentException(message ?? nameof(_boolean));
    }

    public void BeFalse(string message = null)
    {
        if (_boolean)
            throw new ArgumentException(message ?? nameof(_boolean));
    }
}

public class ActionShould
{
    private readonly Action _action;

    public ActionShould(Action action)
    {
        _action = action;
    }

    public void NotThrow()
    {
        try
        {
            _action.Invoke();
        }
        catch (Exception e)
        {
            throw new ArgumentException(nameof(NotThrow));
        }
    }

    public void Throw<Ex>() where Ex : Exception
    {
        try
        {
            _action.Invoke();
        }
        catch (Exception e)
        {
            if (e is Ex)
            {
                //
            }
            else
            {
                throw new ArgumentException(nameof(_action));
            }
        }
    }
}

public static class ShouldExtension
{
    public static BooleanShould Should(this bool boolean) => new BooleanShould(boolean);

    public static ActionShould Should(this Action action) => new ActionShould(action);
}