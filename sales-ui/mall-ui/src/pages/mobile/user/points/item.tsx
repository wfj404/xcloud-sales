import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { PointsHistoryDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Chip, Divider, Typography } from '@mui/material';

export default (props: { model: PointsHistoryDto }) => {
  const { model } = props;

  if (!model || model.Points == 0) {
    return null;
  }

  const renderPoints = () => {
    let points = model.Points;
    let action = model.ActionType;
    if (action == 1) {
      return (
        <Chip
          color='error'
          label={`+${points}`}
          variant='outlined'
          size='small'
        ></Chip>
      );
    } else if (action == -1) {
      return (
        <Chip
          color='success'
          label={`-${points}`}
          variant='outlined'
          size='small'
        ></Chip>
      );
    } else {
      return <Chip label={points} variant='outlined' size='small'></Chip>;
    }
  };

  return (
    <>
      <Box
        sx={{
          p: 2,
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box>{renderPoints()}</Box>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'end',
            }}
          >
            <Typography variant='caption' color={'text.disabled'} component={'div'}>
              {formatRelativeTimeFromNow(model.CreationTime)}
            </Typography>
            {isStringEmpty(model.Message) || (
              <Box sx={{ py: 1 }}>
                <Typography
                  variant='overline'
                  color='primary'
                  component={'div'}
                >
                  {model.Message}
                </Typography>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
      <Divider />
    </>
  );
};
