﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Security.Hash;
using XCloud.Sales.Application.Service;

namespace XCloud.Sales.ThirdParty.Services.KuaiMai;

[ExposeServices(typeof(KuaiMaiUtils))]
public class KuaiMaiUtils : SalesAppService
{
    public string TimeStampFormat => "yyyy-MM-dd HH:mm:ss";

    public IDictionary<string, object> PreHandleParameters(IDictionary<string, object> paramDict, string secret)
    {
        if (string.IsNullOrWhiteSpace(secret))
            throw new ArgumentNullException(nameof(secret));

        paramDict["timestamp"] = this.Clock.Now.AddHours(8).ToString(this.TimeStampFormat);
        paramDict["sign"] = CalculateSign(paramDict, secret);

        return paramDict;
    }

    private string CalculateSign(IDictionary<string, object> paramDict, string secret)
    {
        var filteredDict = paramDict
            .Where(x => x.Key != null)
            .Where(x => x.Key != "sign")
            .Where(x => x.Value != null)
            .ToDictionary(x => x.Key, x => x.Value.ToString());

        var sortedDictionary = new SortedDictionary<string, string>(filteredDict, StringComparer.Ordinal);

        var segments = sortedDictionary.Select(x => $"{x.Key}{x.Value}").ToArray();

        var strToSign = string.Join(string.Empty, segments);

        strToSign = $"{secret}{strToSign}{secret}";

        var md5 = Md5Helper.Encrypt(strToSign, encoding: Encoding.UTF8);

        return md5.Replace("-", string.Empty).ToLower();
    }
}