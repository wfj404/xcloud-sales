﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Application.Utils;
using XCloud.AspNetMvc.Extension;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.Filters;

/// <summary>
/// 验证签名
/// </summary>
public class ValidateSignAttribute : ActionFilterAttribute
{
    /// <summary>
    /// 配置文件里的key
    /// </summary>
    private string ConfigKey => "sign_salt_key";

    private string SignKey => "sign";

    /// <summary>
    /// 时间戳误差
    /// </summary>
    private int DeviationSeconds { get; set; } = 10;

    public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var signUtils = context.HttpContext.RequestServices.GetRequiredService<SignUtils>();
        var config = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();

        var requestParams = context.HttpContext.Request.Query.ToDictionary()
            .AddOrUpdate(context.HttpContext.Request.Form.ToDictionary());

        var salt = config[ConfigKey] ?? "default-salt";

        signUtils.ValidateTimestamp(requestParams, "timestamp", DeviationSeconds);
        signUtils.ValidateSign(requestParams, SignKey, salt);

        await next.Invoke();
    }
}