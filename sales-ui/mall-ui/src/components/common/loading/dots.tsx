import { DotLoading } from 'antd-mobile';

export default function () {
  return (
    <>
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          justifyItems: 'center',
        }}
      >
        <DotLoading />
      </div>
    </>
  );
}
