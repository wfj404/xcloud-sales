﻿using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Exceptions;
using XCloud.Platform.Application.Service.Address;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Dto;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.AfterSale;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderService : ISalesStringCrudAppService<Order, OrderDto, QueryOrderPagingInput>
{
    [NotNull]
    [ItemNotNull]
    Task<OrderDto> EnsureOrderIsToServiceAsync(string orderId);

    Task HideForCustomerAsync(string orderId);

    Task<OrderDto[]> AttachInvoiceAsync(OrderDto[] orders);

    Task<OrderDto[]> AttachShippingAddressAsync(OrderDto[] orders);

    [NotNull]
    [ItemNotNull]
    Task<OrderDto> EnsureUserOrderAsync(string orderId, int userId);

    [NotNull]
    [ItemNotNull]
    Task<OrderDto> EnsureStoreOrderAsync(string orderId, string storeId);

    Task<OrderDto[]> AttachStoreUsersAsync(OrderDto[] orders);

    Task<OrderDto[]> AttachStoresAsync(OrderDto[] orders);

    Task<OrderDto[]> AttachAfterSalesAsync(OrderDto[] orders);

    Task<OrderDto[]> AttachOrderItemsAsync(OrderDto[] orders);

    Task<PagedResponse<OrderDto>> QueryOrderPagingAsync(QueryOrderPagingInput dto);

    Task<int> QueryPendingCountAsync(QueryPendingCountInput dto);
}

public class OrderService : SalesStringCrudAppService<Order, OrderDto, QueryOrderPagingInput>, IOrderService
{
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly AfterSaleUtils _afterSaleUtils;
    private readonly OrderUtils _orderUtils;
    private readonly IUserAddressService _userAddressService;
    private readonly IUserInvoiceService _userInvoiceService;
    private readonly ISettingService _settingService;

    public OrderService(ISalesRepository<Order> orderRepository,
        AfterSaleUtils afterSaleUtils,
        OrderUtils orderUtils,
        IUserAddressService userAddressService,
        ISettingService settingService, IUserInvoiceService userInvoiceService) : base(orderRepository)
    {
        _orderUtils = orderUtils;
        _userAddressService = userAddressService;
        _settingService = settingService;
        _userInvoiceService = userInvoiceService;
        _afterSaleUtils = afterSaleUtils;
        _orderRepository = orderRepository;
    }

    public async Task<OrderDto> EnsureOrderIsToServiceAsync(string orderId)
    {
        var order = await this.Repository.GetRequiredByIdAsync(orderId);

        if (!order.IsToService())
        {
            throw new UserFriendlyException(nameof(this.EnsureOrderIsToServiceAsync));
        }

        return order.MapTo<Order, OrderDto>(ObjectMapper);
    }

    public override Task<Order> InsertAsync(OrderDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<Order> UpdateAsync(OrderDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task DeleteByIdAsync(string id)
    {
        throw new NotSupportedException();
    }

    public async Task<int> QueryPendingCountAsync(QueryPendingCountInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _orderRepository.GetDbContextAsync();

        var activeStatus = _orderUtils.ActiveStatus();

        var query = db.Set<Order>().Where(x => activeStatus.Contains(x.OrderStatusId));

        if (dto.UserId != null && dto.UserId.Value > 0)
        {
            query = query.Where(x => x.UserId == dto.UserId.Value);
        }

        var count = await query.CountAsync();

        return count;
    }

    public virtual async Task HideForCustomerAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var entity = await _orderRepository.FirstOrDefaultAsync(x => x.Id == orderId);

        if (entity == null)
            throw new EntityNotFoundException(nameof(HideForCustomerAsync));

        if (entity.HideForCustomer)
            return;

        if (this._orderUtils.ActiveStatus().Contains(entity.OrderStatusId))
            throw new UserFriendlyException("can't handle for now");

        entity.HideForCustomer = true;

        await _orderRepository.UpdateAsync(entity);
    }

    public async Task<OrderDto> EnsureStoreOrderAsync(string orderId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var order = await GetByIdAsync(orderId);

        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        if (order.StoreId != storeId)
            throw new PermissionRequiredException();

        return order.MapTo<Order, OrderDto>(ObjectMapper);
    }

    public async Task<OrderDto> EnsureUserOrderAsync(string orderId, int userId)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        var order = await GetByIdAsync(orderId);

        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        if (order.UserId != userId)
            throw new PermissionRequiredException();

        return order.MapTo<Order, OrderDto>(ObjectMapper);
    }

    public async Task<OrderDto[]> AttachShippingAddressAsync(OrderDto[] orders)
    {
        if (!orders.Any())
            return Array.Empty<OrderDto>();

        var ids = orders.Select(x => x.UserAddressId).WhereNotEmpty().Distinct().ToArray();

        if (!ids.Any())
            return orders;

        var items = await _userAddressService.QueryByIdsAsync(ids);

        foreach (var m in orders)
        {
            m.UserAddress = items.FirstOrDefault(x => x.Id == m.UserAddressId);
        }

        return orders;
    }

    public async Task<OrderDto[]> AttachInvoiceAsync(OrderDto[] orders)
    {
        if (!orders.Any())
            return Array.Empty<OrderDto>();

        var ids = orders.Select(x => x.UserInvoiceId).WhereNotEmpty().Distinct().ToArray();

        if (!ids.Any())
            return orders;

        var items = await _userInvoiceService.GetByIdsAsync(ids);

        foreach (var m in orders)
        {
            m.UserInvoice = items.FirstOrDefault(x => x.Id == m.UserInvoiceId);
        }

        return orders;
    }

    public async Task<OrderDto[]> AttachOrderItemsAsync(OrderDto[] orders)
    {
        if (!orders.Any())
            return Array.Empty<OrderDto>();

        var db = await _orderRepository.GetDbContextAsync();

        var query = db.Set<OrderItem>().AsNoTracking();

        var orderIds = orders.Ids();

        query = query.Where(x => orderIds.Contains(x.OrderId));

        var items = await query.OrderBy(x => x.Id).ToArrayAsync();

        foreach (var m in orders)
        {
            var itemArray = items.Where(x => x.OrderId == m.Id).ToArray();

            m.Items = itemArray.Select(x => ObjectMapper.Map<OrderItem, OrderItemDto>(x)).ToArray();
        }

        return orders;
    }

    public async Task<OrderDto[]> AttachAfterSalesAsync(OrderDto[] orders)
    {
        if (!orders.Any())
            return Array.Empty<OrderDto>();

        var db = await _orderRepository.GetDbContextAsync();

        var ids = orders.Ids();
        var afterSales = await db.Set<AfterSales>().AsNoTracking().Where(x => ids.Contains(x.OrderId))
            .ToArrayAsync();
        foreach (var m in orders)
        {
            var afterSalesEntity =
                afterSales.OrderByDescending(x => x.CreationTime).FirstOrDefault(x => x.OrderId == m.Id);
            if (afterSalesEntity == null)
                continue;
            m.AfterSales = ObjectMapper.Map<AfterSales, AfterSalesDto>(afterSalesEntity);
        }

        return orders;
    }

    public async Task<OrderDto[]> AttachStoresAsync(OrderDto[] orders)
    {
        if (!orders.Any())
            return Array.Empty<OrderDto>();

        var db = await _orderRepository.GetDbContextAsync();

        var storeIds = orders.Select(x => x.StoreId).WhereNotEmpty().Distinct().ToArray();

        var stores = await db.Set<Store>().AsNoTracking().Where(x => storeIds.Contains(x.Id)).ToArrayAsync();

        foreach (var m in orders)
        {
            var store = stores.FirstOrDefault(x => x.Id == m.StoreId);

            if (store == null)
                continue;

            m.Store = store.MapTo<Store, StoreDto>(ObjectMapper);
        }

        return orders;
    }

    public async Task<OrderDto[]> AttachStoreUsersAsync(OrderDto[] orders)
    {
        if (!orders.Any())
            return Array.Empty<OrderDto>();

        var db = await _orderRepository.GetDbContextAsync();

        var userIds = orders.Select(x => x.UserId).ToArray();

        var users = await db.Set<StoreUser>().AsNoTracking().IgnoreQueryFilters()
            .WhereIdIn(userIds)
            .ToArrayAsync();

        foreach (var m in orders)
        {
            var u = users.FirstOrDefault(x => x.Id == m.UserId);
            if (u == null)
                continue;

            m.StoreUser = ObjectMapper.Map<StoreUser, StoreUserDto>(u);
        }

        return orders;
    }

    private async Task<IQueryable<string>> BuildOrderIdsQueryAsync(QueryOrderPagingInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = from order in db.Set<Order>().AsNoTracking()
            select order;

        if (!string.IsNullOrWhiteSpace(dto.Sn))
            query = query.Where(x => x.OrderSn == dto.Sn);

        if (dto.AffiliateId != null)
            query = query.Where(x => x.AffiliateId == dto.AffiliateId);

        if (dto.UserId != null)
            query = query.Where(x => x.UserId == dto.UserId.Value);

        if (dto.StartTime != null)
            query = query.Where(x => dto.StartTime.Value <= x.CreationTime);

        if (dto.EndTime != null)
            query = query.Where(x => dto.EndTime.Value >= x.CreationTime);

        if (ValidateHelper.IsNotEmptyCollection(dto.Status))
            query = query.Where(x => dto.Status.Contains(x.OrderStatusId));

        if (ValidateHelper.IsNotEmptyCollection(dto.PaymentStatus))
            query = query.Where(x => dto.PaymentStatus.Contains(x.PaymentStatusId));

        if (ValidateHelper.IsNotEmptyCollection(dto.DeliveryStatus))
            query = query.Where(x => dto.DeliveryStatus.Contains(x.ShippingStatusId));

        if (dto.HideForCustomer != null)
            query = query.Where(x => x.HideForCustomer == dto.HideForCustomer.Value);

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            query = query.Where(x => x.StoreId == dto.StoreId);

        if (ValidateHelper.IsNotEmptyCollection(dto.ShippingMethodId))
            query = query.Where(x => dto.ShippingMethodId.Contains(x.ShippingMethodId));

        if (ValidateHelper.IsNotEmptyCollection(dto.PaymentMethodId))
            query = query.Where(x => dto.PaymentMethodId.Contains(x.PaymentMethodId));

        if (dto.ToReview ?? false)
        {
            var settings =
                await this._settingService.GetSettingsAsync<MallSettingsDto>(new CacheStrategy() { Cache = true });

            var days = settings.OrderSettings.ReviewEntryExpiredInDays ?? 7;
            var expired = this.Clock.Now.AddDays(-days);

            var reviewQuery = db.Set<OrderReview>().AsNoTracking();
            query = query
                .Where(x => x.OrderStatusId == SalesConstants.OrderStatus.Finished)
                .Where(x => x.CompleteTime != null && x.CompleteTime >= expired)
                .Where(x => !reviewQuery.Any(d => d.OrderId == x.Id));
        }

        if (dto.ToService ?? false)
        {
            query = query.Where(x => x.OrderStatusId == SalesConstants.OrderStatus.Processing);
            query = query.WhereToService();
        }

        //after sales relative query
        var afterSaleJoinQuery = from order in query
            join afterSale in db.Set<AfterSales>().AsNoTracking()
                on order.Id equals afterSale.OrderId into afterSaleGrouping
            from afterSaleOrNull in afterSaleGrouping.DefaultIfEmpty()
            select new { order, afterSaleOrNull };
        //ref to join query
        var originAfterSaleJoinQuery = afterSaleJoinQuery;

        if (dto.IsAfterSales ?? false)
        {
            afterSaleJoinQuery =
                afterSaleJoinQuery.Where(x => x.order.OrderStatusId == SalesConstants.OrderStatus.AfterSales);
        }

        if (dto.IsAfterSalesPending ?? false)
        {
            var pendingStatus = _afterSaleUtils.PendingStatus();

            afterSaleJoinQuery =
                afterSaleJoinQuery.Where(x => pendingStatus.Contains(x.afterSaleOrNull.AfterSalesStatusId));
        }

        if (ValidateHelper.IsNotEmptyCollection(dto.AfterSalesStatus))
        {
            afterSaleJoinQuery =
                afterSaleJoinQuery.Where(x => dto.AfterSalesStatus.Contains(x.afterSaleOrNull.AfterSalesStatusId));
        }

        if (!ReferenceEquals(originAfterSaleJoinQuery, afterSaleJoinQuery))
        {
            query = afterSaleJoinQuery.Select(x => x.order);
        }

        return query.Select(x => x.Id).Distinct();
    }

    public async Task<PagedResponse<OrderDto>> QueryOrderPagingAsync(QueryOrderPagingInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var idsQuery = await BuildOrderIdsQueryAsync(dto);

        var query = db.Set<Order>().AsNoTracking();

        query = query.Where(x => idsQuery.Contains(x.Id));

        var count = await query.CountOrDefaultAsync(dto);

        query = query
            .OrderByDescending(x => x.CreationTime)
            .ThenBy(x => x.Id);

        var items = await query.PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();

        var orderList = items.MapArrayTo<Order, OrderDto>(ObjectMapper);

        return new PagedResponse<OrderDto>(orderList, dto, count);
    }
}