namespace XCloud.Sales.Application.Domain.Orders;

public class OrderStockQuantityHistory : SalesBaseEntity, IHasCreationTime
{
    public string OrderItemId { get; set; }

    public DateTime CreationTime { get; set; }
}