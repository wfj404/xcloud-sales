import React from 'react';

import { CategoryDto } from '@/utils/swagger';
import { Badge, styled } from '@mui/material';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';

const XStyledTabItem = styled(Tab)((theme) => ({
  '&.Mui-selected': {
    color: '#282b31',
  },
}));

const XStyledTabs = styled(Tabs)((theme) => ({
  '.MuiTabs-indicator': {
    backgroundColor: theme.theme.palette.error.main,
    width: '2px',
  },
}));

export default function ({
  selectedId,
  onSelect,
  data,
}: {
  selectedId?: string;
  onSelect: (e: string) => void;
  data: CategoryDto[];
}) {
  if (!data || data.length <= 0) {
    return null;
  }

  return (
    <>
      <XStyledTabs
        orientation="vertical"
        variant="scrollable"
        scrollButtons={false}
        value={selectedId || false}
        onChange={(event: React.SyntheticEvent, newValue: string) => {
          onSelect(newValue);
        }}
        sx={{
          //backgroundColor: 'rgb(251,251,251)',
          height: '100%',
          width: '100%',
        }}
      >
        {data.map((x, index) => {
          return (
            <XStyledTabItem
              key={index}
              value={x.Id}
              label={
                <Badge
                  badgeContent="荐"
                  color="error"
                  variant="dot"
                  invisible={!x.Recommend}
                >
                  <span>{x.Name}</span>
                </Badge>
              }
            />
          );
        })}
      </XStyledTabs>
    </>
  );
}
