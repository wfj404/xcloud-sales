﻿using Volo.Abp;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/refund-bill")]
public class RefundBillController : ShopBaseController
{
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly IStoreUserBalanceService _storeUserBalanceService;

    public RefundBillController(IOrderRefundBillService orderRefundBillService,
        IStoreUserBalanceService storeUserBalanceService,
        IOrderPaymentBillService orderPaymentBillService)
    {
        _orderRefundBillService = orderRefundBillService;
        _storeUserBalanceService = storeUserBalanceService;
        _orderPaymentBillService = orderPaymentBillService;
    }

    [HttpPost("refund-to-balance")]
    public async Task<ApiResponse<object>> RefundToBalanceAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var bill = await this._orderPaymentBillService.GetRequiredPaidBillByIdAsync(dto.Id);

        await this._orderPaymentBillService.AttachOrderAsync(new[] { bill });

        bill.Order = bill.Order ?? throw new ArgumentException(message: "failed to get order");

        if (bill.Order.StoreId != manager.StoreId)
            throw new UserFriendlyException("订单不属于当前门店");

        var refundBill = await this._orderRefundBillService.CreateRefundBillAsync(new CreateRefundBillDto()
            { BillId = bill.Id, Description = "refund to balance" });

        await this._storeUserBalanceService.ChangeUserBalanceAsync(bill.Order.UserId, refundBill.Price,
            BalanceActionType.Add, comment: "refund to balance");

        await this._orderRefundBillService.MarkAsRefundedAsync(new MarkRefundBillAsRefundInput()
        {
            Id = refundBill.Id,
            RefundId = string.Empty,
            NotifyData = string.Empty
        });

        return new ApiResponse<object>();
    }
}