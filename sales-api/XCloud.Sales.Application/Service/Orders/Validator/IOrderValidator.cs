using XCloud.Core.Json;

namespace XCloud.Sales.Application.Service.Orders.Validator;

public interface IOrderValidator
{
    int Order { get; }

    string ValidatorType { get; }

    Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule);

    Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule);

    Task<OrderConditionValidationResponse> IsConditionMatchedAsync(OrderConditionCheckDto input,
        OrderValidatorRule rule);
}

public abstract class OrderValidatorBase<T> : SalesAppService
    where T : class, IRuleParameter
{
    protected T ParseRuleParameterOrDefault(string json)
    {
        var param =
            JsonDataSerializer.DeserializeFromStringOrDefault<T>(json,
                Logger);

        return param;
    }

    protected T ParseRequiredRuleParameter(string json)
    {
        var param = ParseRuleParameterOrDefault(json);

        if (param == null)
            throw new ArgumentException(nameof(ParseRequiredRuleParameter));

        return param;
    }
}