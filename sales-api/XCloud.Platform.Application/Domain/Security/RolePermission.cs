﻿namespace XCloud.Platform.Application.Domain.Security;

public class RolePermission : PlatformBaseEntity, IHasCreationTime
{
    public string PermissionKey { get; set; }

    public string RoleId { get; set; }

    public DateTime CreationTime { get; set; }
}