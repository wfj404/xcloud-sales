﻿using Volo.Abp.Application.Services;
using XCloud.Application.Service;
using XCloud.AspNetMvc.Core;
using XCloud.Sales.Application.Queue;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service;

public interface ISalesAppService : IApplicationService
{
    //
}

public abstract class SalesAppService : XCloudAppService, ISalesAppService
{
    protected SalesUtils SalesUtils => LazyServiceProvider.LazyGetRequiredService<SalesUtils>();

    protected IWebHelper WebHelper => LazyServiceProvider.LazyGetRequiredService<IWebHelper>();

    protected SalesEventBusService SalesEventBusService =>
        LazyServiceProvider.LazyGetRequiredService<SalesEventBusService>();
}