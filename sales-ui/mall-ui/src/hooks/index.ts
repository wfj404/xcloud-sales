import { useState } from 'react';

export const useStringOrUndefined = () => {
  const [stringOrUndefined, setStringOrUndefined] = useState<
    string | undefined
  >();

  return {
    strOrEmpty: stringOrUndefined || '',
    stringOrUndefined,
    setStringOrUndefined,
  };
};
