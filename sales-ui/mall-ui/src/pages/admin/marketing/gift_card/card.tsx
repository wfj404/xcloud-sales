import { Alert, Card, QRCode, Space, Tag } from 'antd';

import { isGiftCardExpired } from '@/utils/biz';
import { config } from '@/utils/config';
import { GiftCardDto } from '@/utils/swagger';
import { concatUrl, formatMoney } from '@/utils/utils';

export default ({ model }: { model: GiftCardDto }) => {
  if (!model) {
    return null;
  }

  if (model.Used) {
    return <Alert message="卡券已被使用，无法分享" type="info" />;
  }

  if (isGiftCardExpired(model)) {
    return <Alert message="卡券已过期，无法分享" type="error" />;
  }

  const qr_url = concatUrl([
    window.location.origin,
    config.appBase || '',
    '/ucenter/gift-card',
    model.Id || '',
  ]);

  return (
    <>
      <Card title="充值卡" bordered hoverable>
        <Space direction="vertical">
          <Space direction="horizontal">
            <h3>卡面金额</h3>
            <Tag color="warning" bordered>
              {formatMoney(model.Amount || 0)}
            </Tag>
          </Space>
          <Alert message="此卡为不记名充值卡，请勿泄露给他人！以免造成您财产损失。" />
          <QRCode
            value={qr_url}
            style={{
              width: 128,
            }}
          />
          <a href={qr_url} target="_blank">
            {qr_url}
          </a>
        </Space>
      </Card>
    </>
  );
};
