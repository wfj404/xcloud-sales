using System;
using XCloud.EntityFrameworkCore.Repository;
using XCloud.Sales.Application.Domain;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Qingdao.Database.EntityFrameworkCore;

namespace XCloud.Sales.Qingdao.Database.Repository;

/// <summary>
/// Represents the Entity Framework repository
/// </summary>
/// <typeparam name="TEntity">Entity type</typeparam>
public class SalesSqlServerRepository<TEntity> : EfRepositoryBase<ShopSqlServerDbContext, TEntity>, ISalesRepository<TEntity> where TEntity : class, ISalesBaseEntity
{
    public SalesSqlServerRepository(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        //
    }
}