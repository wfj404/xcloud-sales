﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Service;
using XCloud.Platform.Application.Extension;

namespace XCloud.Platform.Application.Service.Storage;

public interface IThumborService : IXCloudAppService
{
    /// <summary>
    /// load data to memory
    /// in this way,may cause out of memory
    /// </summary>
    /// <param name="url"></param>
    /// <param name="height"></param>
    /// <param name="width"></param>
    /// <returns></returns>
    Task<byte[]> ResizeOrNullAsync(string url, int height, int width);

    /// <summary>
    /// dispose stream after use
    /// </summary>
    /// <param name="url"></param>
    /// <param name="height"></param>
    /// <param name="width"></param>
    /// <returns></returns>
    Task<Stream> GetResizedStreamOrNullAsync(string url, int height, int width);

    /// <summary>
    /// copy data to target stream
    /// </summary>
    /// <param name="url"></param>
    /// <param name="height"></param>
    /// <param name="width"></param>
    /// <param name="outputStream"></param>
    /// <returns></returns>
    Task<bool> ResizeAndWriteToStreamAsync(string url, int height, int width, Stream outputStream);
}

[ExposeServices(typeof(IThumborService))]
public class ThumborService : PlatformAppService, IThumborService
{
    private readonly HttpClient _httpClient;

    public ThumborService(IHttpClientFactory httpClientFactory)
    {
        _httpClient = httpClientFactory.CreateClient(nameof(ThumborService));
    }

    /// <summary>
    /// https://thumbor.readthedocs.io/en/latest/getting_started.html#changing-its-size
    /// </summary>
    /// <param name="url"></param>
    /// <param name="height"></param>
    /// <param name="width"></param>
    /// <returns></returns>
    /// <exception cref="BusinessException"></exception>
    public async Task<byte[]> ResizeOrNullAsync(string url, int height, int width)
    {
        await using var ms = new MemoryStream();

        if (await ResizeAndWriteToStreamAsync(url, height, width, ms))
        {
            var bs = ms.ToArray();
            return bs;
        }

        return null;
    }

    private async Task<string> GetThumborUrlAsync(string url, int height, int width)
    {
        if (string.IsNullOrWhiteSpace(url))
            throw new ArgumentNullException(nameof(url));

        if (height < 0)
            throw new ArgumentNullException(nameof(height));

        if (width < 0)
            throw new ArgumentNullException(nameof(width));

        var gateway = await ServiceDiscoveryService.GetRequiredInternalGatewayAddressAsync();

        url = HttpUtility.UrlEncode(url);

        //http://localhost:7070/unsafe/900x200/https://t7.baidu.com/it/u=2621658848,3952322712&fm=193&f=GIF
        var finalUrl = new string[] { gateway, $"/internal-api/thumbor/unsafe/{width}x{height}/{url}" }.ConcatUrl();

        return finalUrl;
    }

    public async Task<Stream> GetResizedStreamOrNullAsync(string url, int height, int width)
    {
        var finalUrl = await this.GetThumborUrlAsync(url, height, width);

        var response = await _httpClient.GetAsync(finalUrl);

        if (!response.IsSuccessStatusCode)
        {
            this.Logger.LogWarning($"error response from thumbor,url:{finalUrl}");

            using (response)
            {
                //
            }

            return null;
        }

        var stream = await response.Content.ReadAsStreamAsync();

        return stream;
    }

    public async Task<bool> ResizeAndWriteToStreamAsync(string url, int height, int width, Stream outputStream)
    {
        var finalUrl = await this.GetThumborUrlAsync(url, height, width);

        using var response = await _httpClient.GetAsync(finalUrl);

        if (!response.IsSuccessStatusCode)
        {
            this.Logger.LogWarning($"error response from thumbor,url:{finalUrl}");

            return false;
        }

        await using var stream = await response.Content.ReadAsStreamAsync();

        await stream.CopyToAsync(outputStream);

        return true;
    }
}