import {
  Alert,
  Badge,
  Button,
  Card,
  DatePicker,
  Form,
  Input,
  List,
  Modal,
  Segmented,
  Space,
} from 'antd';
import { pullAt } from 'lodash-es';
import { useState } from 'react';

import XRenderTime from '@/components/common/time/render_time';
import {
  dateTimeFormat,
  myDayjs,
  parseAsDayjs,
  timezoneOffset,
} from '@/utils/dayjs';
import { ExceptionTime } from '@/utils/swagger';

export default ({
  data,
  ok,
}: {
  data: ExceptionTime[];
  ok: (d: ExceptionTime[]) => void;
}) => {
  const [exceptionTime, _exceptionTime] = useState<ExceptionTime | undefined>(
    undefined,
  );

  return (
    <>
      <Modal
        title={'特殊时间段'}
        open={exceptionTime != undefined}
        onOk={() => {
          const times = data || [];
          ok([...times, exceptionTime || {}]);
          _exceptionTime(undefined);
        }}
        onCancel={() => {
          _exceptionTime(undefined);
        }}
      >
        <Form labelCol={{ span: 4 }}>
          <Form.Item label={'名称'} tooltip={'比如：调休'}>
            <Input
              value={exceptionTime?.Name || ''}
              onChange={(e) => {
                _exceptionTime({
                  ...exceptionTime,
                  Name: e.target.value,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'时间范围'}>
            <DatePicker.RangePicker
              allowClear={false}
              value={[
                parseAsDayjs(exceptionTime?.StartTime)?.add(
                  timezoneOffset,
                  'hour',
                ),
                parseAsDayjs(exceptionTime?.EndTime)?.add(
                  timezoneOffset,
                  'hour',
                ),
              ]}
              onChange={(e) => {
                _exceptionTime({
                  ...exceptionTime,
                  StartTime: e
                    ?.at(0)
                    ?.add(-timezoneOffset, 'hour')
                    ?.format(dateTimeFormat),
                  EndTime: e
                    ?.at(1)
                    ?.add(-timezoneOffset, 'hour')
                    ?.format(dateTimeFormat),
                });
              }}
              presets={[
                {
                  label: '将来7天',
                  value: [
                    myDayjs().utc().add(timezoneOffset, 'hour'),
                    myDayjs().utc().add(timezoneOffset, 'hour').add(7, 'd'),
                  ],
                },
                {
                  label: '将来14天',
                  value: [
                    myDayjs().utc().add(timezoneOffset, 'hour'),
                    myDayjs().utc().add(timezoneOffset, 'hour').add(14, 'd'),
                  ],
                },
              ]}
            />
          </Form.Item>
          <Form.Item label={'服务状态'}>
            <Segmented
              value={exceptionTime?.Include ? 1 : 0}
              options={[
                {
                  label: '不提供服务',
                  value: 0,
                },
                {
                  label: '提供服务',
                  value: 1,
                },
              ]}
              onChange={(e) => {
                const val = e as number;
                _exceptionTime({
                  ...exceptionTime,
                  Include: val > 0,
                });
              }}
            />
          </Form.Item>
        </Form>
      </Modal>

      <Card
        size="small"
        title={'例外时间段'}
        extra={
          <Button
            onClick={() => {
              _exceptionTime({
                Name: '例外时间段',
              });
            }}
            type={'link'}
          >
            添加
          </Button>
        }
      >
        <Alert message={<span>已过期时间段请及时删除</span>}></Alert>
        <List
          dataSource={data}
          renderItem={(x: ExceptionTime, i) => {
            return (
              <List.Item
                actions={[
                  <Button
                    type={'link'}
                    danger
                    onClick={() => {
                      let rules = data || [];
                      rules = [...rules];
                      pullAt(rules, i);
                      ok(rules);
                    }}
                  >
                    删除
                  </Button>,
                ]}
              >
                <List.Item.Meta
                  title={
                    <Space direction={'horizontal'}>
                      <span>{x.Name || '--'}</span>
                      {x.Include ? (
                        <Badge status={'success'} text={'包含'} />
                      ) : (
                        <Badge status={'error'} text={'排除'} />
                      )}
                    </Space>
                  }
                  description={
                    <Space direction="horizontal">
                      <span>开始：</span>
                      <XRenderTime timeStr={x.StartTime} />
                      <span>结束：</span>
                      <XRenderTime timeStr={x.EndTime} />
                    </Space>
                  }
                ></List.Item.Meta>
              </List.Item>
            );
          }}
        ></List>
      </Card>
    </>
  );
};
