﻿namespace XCloud.Platform.Application.Domain.Users;

public class ExternalAccessToken : PlatformBaseEntity, IHasCreationTime
{
    public string Platform { get; set; }

    public string AppId { get; set; }

    public string UserId { get; set; }

    public string Scope { get; set; }

    public int AccessTokenType { get; set; }

    public string AccessToken { get; set; }

    public string RefreshToken { get; set; }

    public DateTime ExpiredAt { get; set; }

    public string DataJson { get; set; }

    public DateTime CreationTime { get; set; }
}