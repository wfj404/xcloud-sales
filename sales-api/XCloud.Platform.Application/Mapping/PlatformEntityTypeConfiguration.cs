using XCloud.EntityFrameworkCore.Mapping;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Application.Mapping;

public class PlatformEntityTypeConfiguration<T> : EfEntityTypeConfiguration<T>
    where T : class, IPlatformBaseEntity
{
    //
}