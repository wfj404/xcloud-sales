import { Button, Input, Space } from 'antd';
import { pullAt } from 'lodash-es';

import XLinkEditor from '@/components/link';
import XPictureUploader from '@/components/upload/single_picture_form';
import { swapArrayPosition } from '@/utils/utils';
import { DragSortTable, ProColumns } from '@ant-design/pro-components';

import BlockPictureSetting from '../components/block_picture_setting';
import { BlockPictureProps } from '../utils';

export default ({
  data,
  onChange,
}: {
  data: BlockPictureProps[];
  onChange: (x: BlockPictureProps[]) => void;
}) => {
  const columns: ProColumns<BlockPictureProps>[] = [
    {
      title: 'No',
      dataIndex: 'index',
      valueType: 'indexBorder',
    },
    {
      title: '排序',
      dataIndex: 'sort',
      className: 'drag-visible',
    },
    {
      title: '图片',
      width: 150,
      render: (d, x, i) => {
        return (
          <>
            <div style={{}}>
              <BlockPictureSetting
                data={x}
                onChange={(e) => {
                  const datalist = [...data];
                  datalist[i] = { ...e };
                  onChange(datalist);
                }}
              />
            </div>
          </>
        );
      },
    },
    {
      title: '跳转',
      render: (d, x, i) => {
        return (
          <>
            <XLinkEditor
              link={x.link || {}}
              onChange={(e) => {
                const datalist = [...(data || [])];
                datalist[i] = {
                  ...x,
                  link: e,
                };
                onChange(datalist);
              }}
            />
          </>
        );
      },
    },
    {
      title: '文字描述',
      render: (d, x, i) => {
        return (
          <>
            <Input.TextArea
              placeholder={'图片描述'}
              rows={2}
              maxLength={20}
              value={x.caption}
              onChange={(e) => {
                const datalist = [...(data || [])];
                datalist[i] = {
                  ...x,
                  caption: e.target.value,
                };
                onChange(datalist);
              }}
            />
          </>
        );
      },
    },
    {
      title: '操作',
      width: 100,
      render: (d, x, i) => {
        const len = data.length;
        return (
          <>
            <Space direction={'vertical'}>
              <Button
                size={'small'}
                type={'dashed'}
                disabled={i <= 0}
                onClick={() => {
                  let datalist = [...(data || [])];
                  datalist = swapArrayPosition(datalist, i - 1, i);
                  onChange(datalist);
                }}
              >
                上移
              </Button>
              <Button
                size={'small'}
                type={'dashed'}
                disabled={i >= len - 1}
                onClick={() => {
                  let datalist = [...(data || [])];
                  datalist = swapArrayPosition(datalist, i, i + 1);
                  onChange(datalist);
                }}
              >
                下移
              </Button>
              <Button
                size={'small'}
                type={'primary'}
                danger
                onClick={() => {
                  if (!confirm('确定删除？')) {
                    return;
                  }
                  let datalist = [...(data || [])];
                  pullAt(datalist, i);
                  onChange(datalist);
                }}
              >
                删除
              </Button>
            </Space>
          </>
        );
      },
    },
  ];

  if (data == undefined) {
    return <div>data error</div>;
  }

  const data_with_ids = data.map<BlockPictureProps>((x, i) => ({
    ...x,
    id: `pic-${i}`,
  }));

  //可拖动表格，再数据修改时会重新渲染
  return (
    <>
      <DragSortTable
        rowKey={'id'}
        size={'small'}
        search={false}
        toolBarRender={false}
        columns={columns}
        dataSource={data_with_ids}
        pagination={false}
        headerTitle={false}
        dragSortKey="sort"
        onDragSortEnd={(a, b, datalist) => {
          onChange([...datalist]);
        }}
      />

      <div style={{ margin: 10 }}>
        <Space direction={'horizontal'}>
          <b>你还可以继续添加图片</b>
          <XPictureUploader
            data={{}}
            ok={(x) => {
              let datalist: BlockPictureProps[] = [
                ...data_with_ids,
                {
                  storage: x,
                },
              ];
              onChange(datalist);
            }}
            remove={() => {
              //
            }}
            uploadButtonProps={{
              size: 'large',
              type: 'primary',
            }}
          />
        </Space>
      </div>
    </>
  );
};
