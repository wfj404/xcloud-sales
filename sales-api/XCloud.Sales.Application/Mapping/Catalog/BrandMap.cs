using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class BrandMap : SalesEntityTypeConfiguration<Brand>
{
    public override void Configure(EntityTypeBuilder<Brand> builder)
    {
        builder.ToTable("sales_brand");

        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigAbpSoftDeletionTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(m => m.Name).IsRequired().HasMaxLength(400);
        builder.Property(m => m.Description).HasMaxLength(500);
        builder.Property(m => m.MetaKeywords).HasMaxLength(400);
        builder.Property(x => x.PictureMetaId).HasMaxLength(100);

        builder.Property(x => x.Published);
        builder.Property(x => x.ShowOnPublicPage);
        builder.Property(x => x.DisplayOrder);

        base.Configure(builder);
    }
}