import { useRequest } from 'ahooks';
import { Space } from 'antd-mobile';
import { useEffect, useState } from 'react';

import XLoading from '@/components/common/loading';
import XModal from '@/components/common/modal';
import { DeliveryType, ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  dateTimeFormat,
  dateTimeUserFriendlyFormat,
  parseAsDayjs,
  timezoneOffset,
} from '@/utils/dayjs';
import { PlaceOrderRequestDto } from '@/utils/swagger';
import { CloseOutlined } from '@mui/icons-material';
import { Box, Button } from '@mui/material';

import XSelector from './selector';

export default ({
  model,
  onChange,
}: {
  model: PlaceOrderRequestDto;
  onChange: (e: PlaceOrderRequestDto) => void;
}) => {
  const short_distance_delivery =
    model.ShippingMethodId == ShippingMethod.Delivery &&
    model.DeliveryType == DeliveryType.ShortDistanceDelivery;
  const pickup = model.ShippingMethodId == ShippingMethod.Pickup;
  const available = pickup || short_distance_delivery;

  const [open, _open] = useState(false);

  const deliveryServiceTimeRequest = useRequest(
    apiClient.mall.deliveryGetServiceTime,
    {
      manual: true,
    },
  );

  const pickupServiceTimeRequest = useRequest(
    apiClient.mall.pickupGetServiceTime,
    {
      manual: true,
    },
  );

  useEffect(() => {
    if (open) {
      if (pickup) {
        pickupServiceTimeRequest.run();
      }
      if (short_distance_delivery) {
        deliveryServiceTimeRequest.run();
      }
    }
  }, [open]);

  const start = parseAsDayjs(model.PreferDeliveryStartTime)?.add(
    timezoneOffset,
    'hour',
  );
  const end = parseAsDayjs(model.PreferDeliveryEndTime)?.add(
    timezoneOffset,
    'hour',
  );
  const renderButton = () => {
    if (!start && !end) {
      return <span>选择交付时间</span>;
    }
    return (
      <span>{`${start?.format(dateTimeUserFriendlyFormat) || '?'} ~ ${end?.format(dateTimeUserFriendlyFormat) || '?'}`}</span>
    );
  };

  const render_short_distance_delivery_time_picker = () => {
    if (!short_distance_delivery) {
      return null;
    }

    if (deliveryServiceTimeRequest.loading) {
      return <XLoading />;
    }

    return (
      <XSelector
        datalist={deliveryServiceTimeRequest.data?.data?.Data || []}
        onSelect={(start, end) => {
          _open(false);
          onChange({
            ...model,
            PreferDeliveryStartTime: start
              ?.add(-timezoneOffset, 'hour')
              ?.format(dateTimeFormat),
            PreferDeliveryEndTime: end
              ?.add(-timezoneOffset, 'hour')
              ?.format(dateTimeFormat),
          });
        }}
      />
    );
  };

  const render_pickup_time_picker = () => {
    if (!pickup) {
      return null;
    }

    if (pickupServiceTimeRequest.loading) {
      return <XLoading />;
    }

    return (
      <XSelector
        datalist={pickupServiceTimeRequest.data?.data?.Data || []}
        onSelect={(start, end) => {
          _open(false);
          onChange({
            ...model,
            PreferDeliveryStartTime: start
              ?.add(-timezoneOffset, 'hour')
              ?.format(dateTimeFormat),
            PreferDeliveryEndTime: end
              ?.add(-timezoneOffset, 'hour')
              ?.format(dateTimeFormat),
          });
        }}
      />
    );
  };

  if (!available) {
    return null;
  }

  if (pickup) {
    return null;
  }

  return (
    <>
      <XModal
        open={open}
        onClose={() => _open(false)}
        title="选择交付时间"
        showCloseButton
      >
        <Box sx={{ py: 2 }}>
          {render_short_distance_delivery_time_picker()}
          {render_pickup_time_picker()}
        </Box>
      </XModal>
      <Space
        direction="horizontal"
        style={{
          width: '100%',
        }}
      >
        <Button
          variant="outlined"
          fullWidth
          sx={{ width: '100%' }}
          onClick={() => {
            _open(true);
          }}
        >
          {renderButton()}
        </Button>
        {(start != undefined || end != undefined) && (
          <Button
            color="error"
            startIcon={<CloseOutlined fontSize="inherit" />}
            onClick={() => {
              onChange({
                ...model,
                PreferDeliveryStartTime: undefined,
                PreferDeliveryEndTime: undefined,
              });
            }}
          >
            不限时间
          </Button>
        )}
      </Space>
    </>
  );
};
