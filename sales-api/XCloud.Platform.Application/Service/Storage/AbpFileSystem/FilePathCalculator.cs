﻿using System.Collections.Generic;
using System.IO;
using Volo.Abp.BlobStoring;
using Volo.Abp.BlobStoring.FileSystem;

namespace XCloud.Platform.Application.Service.Storage.AbpFileSystem;

public class FilePathCalculator : IBlobFilePathCalculator
{
    private readonly IFileNamePrefixCalculator _fileNamePrefixCalculator;
    private readonly IFileHashProvider _fileHashProvider;

    public FilePathCalculator(IFileNamePrefixCalculator fileNamePrefixCalculator, IFileHashProvider fileHashProvider)
    {
        _fileNamePrefixCalculator = fileNamePrefixCalculator;
        _fileHashProvider = fileHashProvider;
    }

    public string Calculate(BlobProviderArgs args)
    {
        var config = args.Configuration.GetFileSystemConfiguration();

        var pathSegments = new List<string> { config.BasePath };

        if (config.AppendContainerNameToBasePath)
        {
            pathSegments.Add(args.ContainerName);
        }

        pathSegments.Add(this._fileHashProvider.HashType);

        var prefix = _fileNamePrefixCalculator.GetFileNamePrefixArray(args.BlobName);

        pathSegments.AddRange(prefix);

        pathSegments.Add(args.BlobName);

        var res = Path.Combine(pathSegments.WhereNotEmpty().ToArray());

        return res;
    }
}