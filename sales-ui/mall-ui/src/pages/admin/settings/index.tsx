import { useOutlet } from 'umi';

import ValidatePermission from '@/components/login/admin/validate_permission';
import { AdminPermissionKeys } from '@/utils/biz';

export default () => {
  const children = useOutlet();

  return (
    <>
      <ValidatePermission permissionKey={AdminPermissionKeys.Settings}>
        {children}
      </ValidatePermission>
    </>
  );
};
