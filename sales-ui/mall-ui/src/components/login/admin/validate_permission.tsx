import { ReactElement } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { adminHasPermission } from '@/utils/biz';

export default ({
  children,
  permissionKey,
}: {
  permissionKey: string;
  children: ReactElement | any;
}) => {
  const app = useSnapshot(storeState) as StoreType;

  if (app.adminPermissions == undefined) {
    return <h1>权限未加载</h1>;
  }

  if (!adminHasPermission(app.adminPermissions, permissionKey)) {
    return <h1>无权访问</h1>;
  }

  return children;
};
