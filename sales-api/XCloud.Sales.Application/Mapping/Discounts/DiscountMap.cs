using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Discounts;

namespace XCloud.Sales.Application.Mapping.Discounts;

public class DiscountMap : SalesEntityTypeConfiguration<Discount>
{
    public override void Configure(EntityTypeBuilder<Discount> builder)
    {
        builder.ToTable("sales_discount");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeleteEntity();

        builder.PreConfigLimitedToStore();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.DiscountPercentage);
        builder.Property(x => x.StartTime);
        builder.Property(x => x.EndTime);
        builder.Property(x => x.IsActive);
        builder.Property(x => x.Sort);
        builder.Property(x => x.LimitedToSku).HasDefaultValue(false);
        builder.Property(x => x.CreationTime);
        builder.Property(x => x.IsDeleted);

        base.Configure(builder);
    }
}