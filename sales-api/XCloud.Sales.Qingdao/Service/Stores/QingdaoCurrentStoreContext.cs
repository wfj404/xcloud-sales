﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.Service.Stores.Context.Contributors;

namespace XCloud.Sales.Qingdao.Service.Stores;

[DisableConventionalRegistration]
public class QingdaoCurrentStoreContext : SalesAppService, ICurrentStoreContext
{
    private readonly QingdaoLocationBasedStoreSelectorContributor _qingdaoLocationBasedStoreSelectorContributor;
    private readonly UserSelectedStoreSelectorContributor _userSelectedStoreSelectorContributor;
    private readonly GetOrCreateDefaultStoreSelectorContributor _getOrCreateDefaultStoreSelectorContributor;
    private readonly QingdaoConfiguredDefaultStoreSelectorContributor _qingdaoConfiguredDefaultStoreSelectorContributor;

    public QingdaoCurrentStoreContext(
        QingdaoLocationBasedStoreSelectorContributor qingdaoLocationBasedStoreSelectorContributor,
        UserSelectedStoreSelectorContributor userSelectedStoreSelectorContributor,
        GetOrCreateDefaultStoreSelectorContributor getOrCreateDefaultStoreSelectorContributor,
        QingdaoConfiguredDefaultStoreSelectorContributor qingdaoConfiguredDefaultStoreSelectorContributor)
    {
        _qingdaoLocationBasedStoreSelectorContributor = qingdaoLocationBasedStoreSelectorContributor;
        _userSelectedStoreSelectorContributor = userSelectedStoreSelectorContributor;
        _getOrCreateDefaultStoreSelectorContributor = getOrCreateDefaultStoreSelectorContributor;
        _qingdaoConfiguredDefaultStoreSelectorContributor = qingdaoConfiguredDefaultStoreSelectorContributor;
    }

    public async Task<string> GetStoreIdOrEmptyAsync()
    {
        var providerList = new ICurrentStoreSelectorContributor[]
        {
            this._userSelectedStoreSelectorContributor,
            this._qingdaoLocationBasedStoreSelectorContributor,
            this._qingdaoConfiguredDefaultStoreSelectorContributor,
            this._getOrCreateDefaultStoreSelectorContributor
        };

        var storeId = await providerList.GetComposedStoreIdOrEmptyAsync();

        return storeId;
    }
}