﻿using JetBrains.Annotations;
using Volo.Abp.Authorization;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Json;
using XCloud.Sales.Application.Domain.Common;
using XCloud.Sales.Application.Repository;
using Z.EntityFramework.Plus;

namespace XCloud.Sales.Application.Service.Common;

public interface IOrderGroupService : ISalesAppService
{
    Task<OrderGroupDto> GetByIdAsync(string id);

    [NotNull]
    [ItemNotNull]
    Task<OrderGroupDto> EnsureStoreOrderGroupAsync(string id, string storeId);

    Task<OrderGroup> InsertAsync(OrderGroupDto dto);

    Task<OrderGroup> UpdateAsync(OrderGroupDto dto);

    Task<OrderGroup> UpdateWithConcurrencyCheckAsync(OrderGroupDto dto);

    Task<PagedResponse<OrderGroupDto>> QueryPagingAsync(QueryOrderGroupPagingInput dto);

    Task DeleteByIdAsync(string id);
}

public class OrderGroupService : SalesAppService, IOrderGroupService
{
    private readonly ISalesRepository<OrderGroup> _repository;

    public OrderGroupService(ISalesRepository<OrderGroup> repository)
    {
        _repository = repository;
    }

    public async Task<OrderGroupDto> GetByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await this._repository.FirstOrDefaultAsync(x => x.Id == id);

        var dto = entity?.MapTo<OrderGroup, OrderGroupDto>(this.ObjectMapper);

        if (dto != null)
        {
            dto.GroupData =
                this.JsonDataSerializer.DeserializeFromStringOrDefault<OrderGroupDataDto>(dto.OrderDataJson,
                    this.Logger) ?? new OrderGroupDataDto();
        }

        return dto;
    }

    public async Task<OrderGroupDto> EnsureStoreOrderGroupAsync(string id, string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var group = await this.GetByIdAsync(id);

        if (group == null)
            throw new EntityNotFoundException(nameof(EnsureStoreOrderGroupAsync));

        if (group.StoreId != storeId)
            throw new AbpAuthorizationException(message: $"{nameof(storeId)}");

        return group;
    }

    private async Task<bool> CheckNameExistAsync(string name, string exceptId = default)
    {
        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<OrderGroup>().AsNoTracking();

        query = query.Where(x => x.Name == name);

        if (!string.IsNullOrWhiteSpace(exceptId))
            query = query.Where(x => x.Id != exceptId);

        return await query.AnyAsync();
    }

    private void PreHandleDto(OrderGroupDto dto)
    {
        foreach (var m in dto.GroupData.Orders)
        {
            if (string.IsNullOrWhiteSpace(m.Id))
            {
                m.Id = this.GuidGenerator.CreateGuidString();
            }
        }
    }

    public async Task<OrderGroup> InsertAsync(OrderGroupDto dto)
    {
        dto.EnsureStoreId();
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        PreHandleDto(dto);

        var entity = dto.MapTo<OrderGroupDto, OrderGroup>(this.ObjectMapper);

        entity.OrderDataJson = this.JsonDataSerializer.SerializeToString(dto.GroupData);

        entity.Id = this.GuidGenerator.CreateGuidString();
        entity.LastModificationTime = entity.CreationTime = this.Clock.Now;

        entity.RowVersion++;

        if (await this.CheckNameExistAsync(entity.Name))
        {
            throw new UserFriendlyException(message: "group name exist");
        }

        await this._repository.InsertAsync(entity);

        return entity;
    }

    public async Task<OrderGroup> UpdateWithConcurrencyCheckAsync(OrderGroupDto dto)
    {
        if (dto.IsEmptyId())
            throw new ArgumentNullException(nameof(dto.Id));

        dto.EnsureStoreId();
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        if (dto.RowVersion <= 0)
            throw new ArgumentNullException(nameof(dto.RowVersion));

        PreHandleDto(dto);

        if (await this.CheckNameExistAsync(dto.Name, exceptId: dto.Id))
        {
            throw new UserFriendlyException(message: "group name exist");
        }

        var jsonData = this.JsonDataSerializer.SerializeToString(dto.GroupData);

        var db = await this._repository.GetDbContextAsync();

        var rows = await db.Set<OrderGroup>().AsTracking()
            .Where(x => x.Id == dto.Id)
            .Where(x => x.RowVersion == dto.RowVersion)
            .UpdateAsync(x => new OrderGroup()
            {
                Name = dto.Name,
                Description = dto.Description,
                OrderDataJson = jsonData,
                LastModificationTime = this.Clock.Now,
                RowVersion = x.RowVersion + 1
            });

        if (rows <= 0)
            throw new UserFriendlyException("failed to update");

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        return entity;
    }

    public async Task<OrderGroup> UpdateAsync(OrderGroupDto dto)
    {
        if (dto.IsEmptyId())
            throw new ArgumentNullException(nameof(dto.Id));

        dto.EnsureStoreId();
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        PreHandleDto(dto);

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.OrderDataJson = this.JsonDataSerializer.SerializeToString(dto.GroupData);
        entity.LastModificationTime = this.Clock.Now;

        entity.RowVersion++;

        if (await this.CheckNameExistAsync(entity.Name, exceptId: entity.Id))
        {
            throw new UserFriendlyException(message: "group name exist");
        }

        await this._repository.UpdateAsync(entity);

        return entity;
    }

    public async Task<PagedResponse<OrderGroupDto>> QueryPagingAsync(QueryOrderGroupPagingInput dto)
    {
        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<OrderGroup>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            query = query.Where(x => x.StoreId == dto.StoreId);

        var count = await query.CountOrDefaultAsync(dto);
        var datalist = await query
            .OrderByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = datalist.MapArrayTo<OrderGroup, OrderGroupDto>(this.ObjectMapper);

        return new PagedResponse<OrderGroupDto>(items, dto, count);
    }

    public async Task DeleteByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        await this._repository.DeleteAsync(x => x.Id == id);
    }
}