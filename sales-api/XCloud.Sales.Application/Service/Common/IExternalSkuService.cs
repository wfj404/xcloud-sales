﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Common;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Common;

public interface IExternalSkuService : ISalesAppService
{
    Task<ExternalSkuGroupingDto[]> AttachSkusAsync(ExternalSkuGroupingDto[] data);

    Task<PagedResponse<ExternalSkuGroupingDto>> QueryPagingAsync(QueryExternalSkuInput dto);

    Task InsertManyAsync(ExternalSkuDto[] data);

    Task SaveExternalSkuMappingAsync(string combineId, string skuIdOrEmpty = default);

    Task DeleteExpiredAsync(DateTime expireTime);
}

public class ExternalSkuService : SalesAppService, IExternalSkuService
{
    private readonly ISalesRepository<ExternalSku> _repository;

    public ExternalSkuService(ISalesRepository<ExternalSku> repository)
    {
        _repository = repository;
    }

    public async Task<ExternalSkuGroupingDto[]> AttachSkusAsync(ExternalSkuGroupingDto[] data)
    {
        if (data.Any())
        {
            var db = await this._repository.GetDbContextAsync();

            var query = from mapping in db.Set<ExternalSkuMapping>().AsNoTracking()
                join sku in db.Set<Sku>().AsNoTracking()
                    on mapping.SkuId equals sku.Id
                select new { mapping, sku };

            var ids = data.Select(x => x.CombineId).Distinct().ToArray();

            query = query.Where(x => ids.Contains(x.mapping.CombineId));

            var datalist = await query.OrderByDescending(x => x.sku.CreationTime).ToArrayAsync();

            foreach (var m in data)
            {
                var item = datalist.FirstOrDefault(x => x.mapping.CombineId == m.CombineId);
                if (item == null)
                    continue;

                m.Sku = item.sku.MapTo<Sku, SkuDto>(this.ObjectMapper);
            }
        }

        return data;
    }

    public async Task<PagedResponse<ExternalSkuGroupingDto>> QueryPagingAsync(QueryExternalSkuInput dto)
    {
        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<ExternalSku>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.SystemName))
        {
            query = query.Where(x => x.SystemName == dto.SystemName);
        }

        query = query.WhereCreationTimeBetween(dto.StartTime, dto.EndTime);

        var grouping = query
            .GroupBy(x => x.CombineId)
            .Select(x => new
            {
                x.Key,
                CreationTime = x.Max(d => d.CreationTime),
                Items = x.ToArray()
            });

        var count = await grouping.CountOrDefaultAsync(dto);

        var groupingList = await grouping
            .OrderByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var groupList = new List<ExternalSkuGroupingDto>();

        foreach (var m in groupingList)
        {
            var group = new ExternalSkuGroupingDto()
            {
                CombineId = m.Key,
                CreationTime = m.CreationTime,
                Items = m.Items?
                    .OrderByDescending(x => x.CreationTime)
                    .MapArrayTo<ExternalSku, ExternalSkuDto>(this.ObjectMapper)
            };

            groupList.Add(group);
        }

        return new PagedResponse<ExternalSkuGroupingDto>(groupList, dto, count);
    }

    private string GetCombineId(string systemName, string externalSkuId) => $"{externalSkuId}@{systemName}";

    public async Task InsertManyAsync(ExternalSkuDto[] data)
    {
        if (!data.Any())
            return;

        var now = this.Clock.Now;

        foreach (var m in data)
        {
            if (string.IsNullOrWhiteSpace(m.SystemName))
                throw new ArgumentException(message: nameof(m.SystemName));

            if (string.IsNullOrWhiteSpace(m.ExternalSkuId))
                throw new ArgumentException(message: nameof(m.ExternalSkuId));

            m.Id = this.GuidGenerator.CreateGuidString();
            m.CreationTime = now;

            m.GoodsName ??= string.Empty;
            m.SkuName ??= string.Empty;
            m.CombineId = GetCombineId(m.SystemName, m.ExternalSkuId);
        }

        var entities = data.MapArrayTo<ExternalSkuDto, ExternalSku>(this.ObjectMapper);

        await this._repository.InsertManyAsync(entities);
    }

    public async Task SaveExternalSkuMappingAsync(string combineId, string skuIdOrEmpty)
    {
        if (string.IsNullOrWhiteSpace(combineId))
            throw new ArgumentNullException(nameof(combineId));

        var db = await this._repository.GetDbContextAsync();

        var set = db.Set<ExternalSkuMapping>();

        var query = set.Where(x => x.CombineId == combineId);

        var mappings = query.ToArray();

        if (mappings.Any())
        {
            set.RemoveRange(mappings);
        }

        if (!string.IsNullOrWhiteSpace(skuIdOrEmpty))
        {
            var entity = new ExternalSkuMapping
            {
                Id = this.GuidGenerator.CreateGuidString(),
                CombineId = combineId,
                SkuId = skuIdOrEmpty
            };

            set.Add(entity);
        }

        await db.TrySaveChangesAsync();
    }

    public async Task DeleteExpiredAsync(DateTime expireTime)
    {
        var db = await this._repository.GetDbContextAsync();

        var batchSize = 500;

        while (true)
        {
            var set = db.Set<ExternalSku>();

            var query = set.AsTracking();

            query = query.Where(x => x.CreationTime < expireTime);

            var datalist = await query.OrderBy(x => x.CreationTime).Take(batchSize).ToArrayAsync();

            if (!datalist.Any())
            {
                break;
            }

            set.RemoveRange(datalist);
            await db.TrySaveChangesAsync();
        }
    }
}