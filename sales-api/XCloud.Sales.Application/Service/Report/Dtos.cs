﻿using Volo.Abp.Application.Dtos;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Report;

public class TopAfterSaleMallUsersResponse : IEntityDto
{
    public StoreUserDto StoreUser { get; set; }

    public int UserId { get; set; }
    public int Count { get; set; }
    public decimal Amount { get; set; }
}

public class TopAfterSaleMallUsersInput : IEntityDto
{
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public int? MaxCount { get; set; }
}

public class UserActivityGroupByGeoLocationInput : IEntityDto
{
    public int? UserId { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }

    public int? MaxCount { get; set; }
}

public class UserActivityGroupByGeoLocationResponse : IEntityDto
{
    public string Country { get; set; }
    public string City { get; set; }
    public int Count { get; set; }
}

public class UserActivityGroupByHourInput : IEntityDto
{
    public int? UserId { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class UserActivityGroupByHourResponse : IEntityDto
{
    public int Hour { get; set; }
    public int ActivityType { get; set; }
    public int Count { get; set; }
}

public class QueryGoodsVisitReportResponse : IEntityDto
{
    public GoodsDto Goods { get; set; }
    public string GoodsId { get; set; }
    public int VisitedCount { get; set; }
}

public class QuerySearchKeywordsReportResponse : IEntityDto
{
    public string Keywords { get; set; }
    public int Count { get; set; }
}

public class QuerySearchKeywordsReportInput : IEntityDto
{
    public int? UserId { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public int? MaxCount { get; set; }
}

public class QueryGoodsVisitReportInput : IEntityDto
{
    public int? UserId { get; set; }
    public string GoodsId { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public int? Count { get; set; }
}

public class BaseOrderReportInput : IEntityDto
{
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public int? MaxCount { get; set; }
}

public class QueryTopStoreInput : BaseOrderReportInput
{
    //
}

public class StoreOrderStatics : IEntityDto
{
    public StoreDto Store { get; set; }
    public string StoreId { get; set; }
    public decimal TotalPrice { get; set; }
    public int TotalQuantity { get; set; }
}

public class QueryTopSellerInput : BaseOrderReportInput
{
    //
}

public class TopSellersList : IEntityDto
{
    public int SellerId { get; set; }
    public string GlobalUserId { get; set; }
    public string SellerName { get; set; }
    public decimal TotalPrice { get; set; }
    public int TotalQuantity { get; set; }
}

public class QueryTopCustomerInput : BaseOrderReportInput
{
    //
}

public class TopCustomersList : IEntityDto
{
    public int CustomerId { get; set; }
    public string GlobalUserId { get; set; }
    public string CustomerName { get; set; }
    public decimal TotalPrice { get; set; }
    public int TotalQuantity { get; set; }
}

public class QueryTopSkuListInput : BaseOrderReportInput
{
    //
}

public class TopSkuList : IEntityDto
{
    public string SkuId { get; set; }
    public string Name { get; set; }
    public decimal TotalPrice { get; set; }
    public int TotalQuantity { get; set; }
}

public class QueryTopBrandListInput : BaseOrderReportInput
{
    //
}

public class TopBrandList : IEntityDto
{
    public string BrandId { get; set; }
    public string BrandName { get; set; }
    public decimal TotalPrice { get; set; }
    public int TotalQuantity { get; set; }
}

public class QueryTopCategoryListInput : BaseOrderReportInput
{
    //
}

public class TopCategoryList : IEntityDto
{
    public string RootCategoryId { get; set; }
    public string RootCategoryName { get; set; }
    public string CategoryId { get; set; }
    public string CategoryName { get; set; }
    public decimal TotalPrice { get; set; }
    public int TotalQuantity { get; set; }
}

public class OrderSumByDateInput : BaseOrderReportInput
{
    //
}

public class OrderSumByDateResponse : IEntityDto
{
    public DateTime Date { get; set; }
    public int Total { get; set; }
    public decimal Amount { get; set; }
}