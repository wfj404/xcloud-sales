import { ConfigProvider } from 'antd-mobile';
import mobileZhCN from 'antd-mobile/es/locales/zh-CN';
import { ReactElement } from 'react';

export default function ({ children }: { children: ReactElement }) {
  return (
    <>
      <ConfigProvider locale={mobileZhCN}>{children}</ConfigProvider>
    </>
  );
}
