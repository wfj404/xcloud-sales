using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using Volo.Abp.Modularity;
using XCloud.Core.Configuration;
using XCloud.Core.Extension;
using XCloud.Core.Helper;
using XCloud.Elasticsearch.Core;

namespace XCloud.Application.Logging;

public static class SerilogElasticSearchSinkExtension
{
    public static void TryAddElasticSearchProvider(this ILoggingBuilder builder, ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        var loggingOption = config.GetLoggingOptionOrDefault();

        if (!loggingOption.ElasticSearch.Enabled)
        {
            return;
        }

        var options = config.GetRequiredElasticSearchOption();

        var hosts = options.Hosts.WhereNotEmpty().Select(x => new Uri(x)).ToArray();

        if (ValidateHelper.IsEmptyCollection(hosts))
        {
            return;
        }

        var appName = context.Services.GetAppName();
        //all logs
        var esLogger = config.CreateSerilogConfig(appName)
            .Filter.ByIncludingOnly(x =>
                SerilogConfigurationExtension.ErrorLevelType.Contains(x.Level) || x.Level >= LogEventLevel.Information)
            .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(hosts)
            {
                IndexFormat = $"{appName}-all-serilog-{{0:yyyy.MM.dd}}",
                AutoRegisterTemplate = true,
                OverwriteTemplate = true,
                NumberOfReplicas = 1,
                NumberOfShards = 1,
                QueueSizeLimit = 10_0000,
                ConnectionTimeout = TimeSpan.FromSeconds(3)
            }).CreateLogger();

        builder.AddSerilog(esLogger);
    }
}