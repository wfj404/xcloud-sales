﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Coupons;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Coupons;

public static class CouponTypes
{
    public static int Normal => 0;

    public static int FreeShippingFee => 1;
}

public class CouponDto : Coupon, IEntityDto<string>
{
    public bool? CanBeIssued { get; set; }

    public string UnableToIssueReason { get; set; }

    public int IssuedAmount { get; set; }

    public int UsedAmount { get; set; }

    public StoreDto[] Stores { get; set; }

    public string[] ValidationErrors { get; set; }
}

public class CouponUserMappingDto : CouponUserMapping, IEntityDto<int>
{
    public CouponDto Coupon { get; set; }

    public StoreUserDto StoreUser { get; set; }
}

public class IssueCouponInput : IEntityDto
{
    public string CouponId { get; set; }
    
    public int UserId { get; set; }
}

public class QueryUserCouponPagingInput : PagedRequest
{
    public int? UserId { get; set; }

    public bool? Used { get; set; }

    public DateTime? AvailableFor { get; set; }

    public bool? IsDeleted { get; set; }
}

public class QueryCouponPagingInput : PagedRequest
{
    public DateTime? AvailableFor { get; set; }

    public string StoreId { get; set; }
}