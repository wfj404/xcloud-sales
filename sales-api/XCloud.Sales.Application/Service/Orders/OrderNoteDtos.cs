﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;

namespace XCloud.Sales.Application.Service.Orders;

public static class OrderNoteExtendedType
{
    public static string Common => "common";

    public static string Message => "message";

    public static string Created => "created";

    public static string Paid => "paid";

    public static string Processing => "processing";

    public static string Shipping => "shipping";

    public static string Shipped => "shipped";

    public static string Delivery => "delivery";

    public static string Pickup => "pickup";

    public static string Finished => "finished";

    public static string Closed => "closed";
}

public class OrderNoteExtended : IEntityDto
{
    public string NoteType { get; set; } = OrderNoteExtendedType.Common;

    private Dictionary<string, string> _data;

    [NotNull]
    public Dictionary<string, string> Data
    {
        get => this._data ??= new Dictionary<string, string>();
        set => this._data = value;
    }
}