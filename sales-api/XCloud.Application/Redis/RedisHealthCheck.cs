﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;

namespace XCloud.Application.Redis;

public class RedisHealthCheck : IHealthCheck, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;
    public RedisHealthCheck(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        using var s = _serviceProvider.CreateScope();

        try
        {
            var client = s.ServiceProvider.GetRequiredService<RedisClient>();

            await client.Connection.GetDatabase().KeyExistsAsync(nameof(RedisHealthCheck));

            return HealthCheckResult.Healthy();
        }
        catch (Exception e)
        {
            s.ServiceProvider.GetRequiredService<ILogger<RedisHealthCheck>>().LogError(message: e.Message, exception: e);
            return HealthCheckResult.Unhealthy(e.Message);
        }
    }
}