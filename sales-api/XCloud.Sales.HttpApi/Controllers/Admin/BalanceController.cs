﻿using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/balance")]
public class BalanceController : ShopBaseController
{
    private readonly IStoreUserBalanceService _userBalanceService;

    public BalanceController(IStoreUserBalanceService userBalanceService)
    {
        _userBalanceService = userBalanceService;
    }

    [HttpPost("update-user-balance")]
    public async Task<ApiResponse<object>> UpdateUserBalanceAsync([FromBody] BalanceHistoryDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageFinance);

        await _userBalanceService.UpdateUserBalanceAsync(dto);

        return new ApiResponse<object>();
    }
}