﻿namespace XCloud.Platform.Application.Domain.Security;

/// <summary>
/// 数据权限
/// </summary>
public class ResourceAcl : PlatformBaseEntity, IHasCreationTime
{
    /// <summary>
    /// 按钮、接口、模块、功能
    /// </summary>
    public string ResourceType { get; set; }

    public string ResourceId { get; set; }

    /// <summary>
    /// 权限、角色、部门
    /// </summary>
    public string PermissionType { get; set; }

    public string PermissionId { get; set; }

    /// <summary>
    /// allow、deny
    /// </summary>
    public int AccessControlType { get; set; }

    public DateTime CreationTime { get; set; }
}