﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.AfterSale;

public class AfterSalesCommentDto : AfterSalesComment, IEntityDto
{
    public AfterSalesDto AfterSales { get; set; }

    public StorageMetaDto[] Pictures { get; set; }
}

public class QueryAfterSalesCommentPaging : PagedRequest
{
    public string AfterSalesId { get; set; }
}

public class DangerouslyUpdateAfterSalesStatusDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string Status { get; set; }
}

public class ApproveAfterSaleDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string Comment { get; set; }
}

public class RejectAfterSaleInput : IEntityDto<string>
{
    public string Id { get; set; }
    public string Comment { get; set; }
}

public class CancelAfterSaleDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string Comment { get; set; }
}

public class UpdateAfterSaleStatusDto : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? IsDeleted { get; set; }
}

public class CompleteAfterSaleDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string Comment { get; set; }
}

public class AfterSalesDto : AfterSales, IEntityDto<string>
{
    public AfterSalesItemDto[] Items { get; set; }

    public OrderDto Order { get; set; }
}

public class AfterSalesItemDto : AfterSalesItem, IEntityDto
{
    public OrderItemDto OrderItem { get; set; }
}

public class QueryAfterSalePaging : PagedRequest
{
    public int? UserId { get; set; }
    public string[] Status { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public bool? IsDeleted { get; set; }

    public string StoreId { get; set; }

    public bool? IsAfterSalesPending { get; set; }
}