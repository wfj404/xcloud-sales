﻿using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Core.Json;

namespace XCloud.AspNetMvc.Filters;

public static class ActionFilterExtension
{
    public static string GetHttpContextInfoToPrint(this ActionExecutingContext context,
        ActionExecutedContext actionExecutedContext, int? maxLength = default)
    {
        if (actionExecutedContext == null)
            throw new ArgumentNullException(nameof(actionExecutedContext));

        maxLength ??= 4000;

        var jsonDataSerializer = context.HttpContext.RequestServices.GetRequiredService<IJsonDataSerializer>();

        var sb = new StringBuilder();
        sb.AppendLine($"{context.HttpContext.Request.Method} {context.HttpContext.Request.Path}");

        if (jsonDataSerializer.TrySerializeToString(context.ActionArguments, out var reqJson))
        {
            sb.AppendLine("request_params:");
            sb.AppendLine(reqJson);
        }

        if (actionExecutedContext.Result is ObjectResult or && or.Value != null)
        {
            if (jsonDataSerializer.TrySerializeToString(or.Value, out var resJson))
            {
                if (resJson.Length <= maxLength.Value)
                {
                    sb.AppendLine("response:");
                    sb.AppendLine(resJson);
                }
                else
                {
                    sb.AppendLine("response is too long to display here");
                    sb.AppendLine(resJson.Substring(startIndex: 0, length: maxLength.Value));
                }
            }
        }

        return sb.ToString();
    }
}