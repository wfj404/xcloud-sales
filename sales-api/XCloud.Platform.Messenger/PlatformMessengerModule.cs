﻿using System;
using Microsoft.AspNetCore.WebSockets;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.Configuration;
using XCloud.Core.Configuration;
using XCloud.Platform.Application;
using XCloud.Platform.Messenger.Abstraction.Router;
using XCloud.Platform.Messenger.Abstraction.Tasks;
using XCloud.Platform.Messenger.Configuration;
using XCloud.Platform.Messenger.Mapper;

namespace XCloud.Platform.Messenger;

[DependsOn(
    typeof(PlatformApplicationModule))]
[MethodMetric]
public class PlatformMessengerModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        var builder = context.Services.GetRequiredXCloudBuilder();

        context.AddAutoMapperProfile<MessengerAutoMapperConfiguration>();

        context.Services.AddWebSockets(option =>
        {
            option.KeepAliveInterval = TimeSpan.FromSeconds(60);
            option.AddConfiguredCors(context);
        });

        builder.AddMessenger()
            .AddDefaultServerProvider();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.RemoveAll<IMessageClusterRouter>();
        context.Services.AddSingleton<IMessageClusterRouter, CapMessageClusterRouter>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        using var s = context.ServiceProvider.CreateScope();

        //cluster jobs
        s.ServiceProvider.StartImWorker();

        //local tasks
        var manager = s.ServiceProvider.GetRequiredService<IMessengerTaskManager>();
        manager.StartTasks();
    }
}