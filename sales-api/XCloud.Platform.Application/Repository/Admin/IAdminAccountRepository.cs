namespace XCloud.Platform.Application.Repository.Admin;

public interface IAdminAccountRepository : IPlatformRepository<Domain.Admins.Admin>
{
    //
}