﻿using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Grades;
using XCloud.Sales.Application.Service.Grades;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/grade")]
public class GradeController : ShopBaseController
{
    private readonly IGradeService _userGradeService;

    public GradeController(IGradeService userGradeService)
    {
        _userGradeService = userGradeService;
    }

    [HttpPost("set-user-grade")]
    public virtual async Task<ApiResponse<object>> SetUserGradeAsync([FromBody] StoreUserGradeMappingDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        await _userGradeService.SetUserGradeAsync(dto);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _userGradeService.UpdateGradeUserCountAsync(dto.GradeId);

        return new ApiResponse<object>();
    }

    [HttpPost("list")]
    public async Task<ApiResponse<Grade[]>> ListGradesAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var data = await _userGradeService.QueryAllGradesAsync();

        return new ApiResponse<Grade[]>(data);
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> SaveAsync([FromBody] GradeDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _userGradeService.InsertAsync(dto);
        }
        else
        {
            await _userGradeService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("update-status")]
    public virtual async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateGradeStatusInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        await _userGradeService.UpdateUserGradeStatusAsync(dto);

        return new ApiResponse<object>();
    }
}