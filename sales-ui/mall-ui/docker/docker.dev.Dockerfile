#production stage
FROM nginx:1.15 as production-stage

WORKDIR /usr/share/nginx/html
RUN mkdir sales
RUN mkdir sales-manage

COPY ./dist ./sales
#COPY ./dist ./sales-manage

WORKDIR /etc/nginx/
RUN rm ./nginx.conf
COPY ./docker/nginx.conf ./nginx.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]
