import { useRequest } from 'ahooks';
import { Button, Form, Modal, TreeSelect } from 'antd';
import { useEffect, useState } from 'react';

import { getFlatNodes, getTreeSelectOptionsV2 } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { GoodsCategoriesValidatorParameter } from '@/utils/swagger';

export default ({
  rule,
  ok,
}: {
  rule: GoodsCategoriesValidatorParameter;
  ok: (d: GoodsCategoriesValidatorParameter) => void;
}) => {
  const [settings, _settings] = useState<GoodsCategoriesValidatorParameter>({});
  const [show, _show] = useState(false);

  const queryCategory = useRequest(apiClient.mallAdmin.categoryQueryAntdTree, {
    manual: true,
  });
  const dataList = queryCategory.data?.data?.Data || [];

  const flatData = dataList.flatMap((x) => getFlatNodes(x));

  useEffect(() => {
    _settings({ ...rule });
  }, [rule]);

  useEffect(() => {
    if (show) {
      queryCategory.run({});
    }
  }, [show]);

  return (
    <>
      <Button
        block
        onClick={() => {
          _show(true);
        }}
      >{`限定分类(${
        rule.SupportedCategories?.map((x) => x.Name).join(',') || '--'
      })`}</Button>

      <Modal
        title={'限定分类可用'}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        onOk={() => {
          ok({
            ...settings,
          });
          _show(false);
        }}
      >
        <Form>
          <Form.Item label={'分类'}>
            <TreeSelect
              multiple
              maxTagCount={10}
              treeDefaultExpandAll
              showSearch
              loading={queryCategory.loading}
              value={settings.SupportedCategories?.map((x) => x.Id)}
              onChange={(e) => {
                _settings({
                  ...settings,
                  SupportedCategories: flatData
                    .filter((x) => e.indexOf(x.key) >= 0)
                    .map((x) => x.raw_data),
                });
              }}
              treeData={dataList.map((x) => getTreeSelectOptionsV2(x))}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
