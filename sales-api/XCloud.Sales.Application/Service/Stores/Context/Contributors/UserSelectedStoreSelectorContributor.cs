﻿namespace XCloud.Sales.Application.Service.Stores.Context.Contributors;

[ExposeServices(typeof(UserSelectedStoreSelectorContributor))]
public class UserSelectedStoreSelectorContributor : SalesAppService, ICurrentStoreSelectorContributor
{
    private readonly IStoreService _storeService;

    public UserSelectedStoreSelectorContributor(IStoreService storeService)
    {
        _storeService = storeService;
    }

    public async Task<string> GetStoreIdOrEmptyAsync()
    {
        var success =
            this.WebHelper.RequiredHttpContext.Request.Headers.TryGetValue("x-selected-store", out var storeId);

        if (success && !string.IsNullOrWhiteSpace(storeId))
        {
            var store = await this._storeService.GetByIdAsync(storeId);
            if (store != null)
            {
                return store.Id;
            }

            this.Logger.LogWarning(message: $"用户指定的门店[{storeId}]无效，自动跳过");
        }

        return default;
    }
}