//import '/node_modules/react-grid-layout/css/styles.css';
//import '/node_modules/react-resizable/css/styles.css';

import { useSize, useThrottleFn } from 'ahooks';
import React, {
  ReactNode,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import GridLayout, { Layout } from 'react-grid-layout';

const DynamicSizeContent = ({
  onHeightChange,
  children,
}: {
  onHeightChange: (e: number) => void;
  children?: ReactNode;
}) => {
  const [dh, _dh] = useState<number>(0);

  const ref = useRef<HTMLDivElement>(null);
  const size = useSize(ref);

  const height = size?.height || 0;

  const triggerHeightChange = useCallback(
    useThrottleFn(onHeightChange, { wait: 100 }).run,
    [],
  );

  useEffect(() => {
    triggerHeightChange(height);
  }, [height]);

  useEffect(() => {
    const x = setInterval(() => {
      _dh(Math.random() * 50);
    }, 1000);
    return () => {
      clearInterval(x);
    };
  }, []);

  return (
    <div ref={ref}>
      <div>height:{height}</div>
      {children}
      <div style={{ paddingTop: dh, backgroundColor: 'red' }}>{dh}</div>
    </div>
  );
};

export default () => {
  const [layout, _layout] = useState<Layout[]>([
    { i: 'a', x: 0, y: 0, w: 1, h: 1 },
    { i: 'b', x: 1, y: 1, w: 1, h: 1 },
    { i: 'c', x: 2, y: 2, w: 1, h: 1 },
    { i: 'd', x: 3, y: 3, w: 1, h: 1 },
    { i: 'e', x: 4, y: 4, w: 1, h: 1 },
    { i: 'f', x: 5, y: 5, w: 1, h: 1 },
    { i: 'g', x: 6, y: 6, w: 1, h: 1 },
    { i: 'h', x: 7, y: 7, w: 1, h: 1 },
  ]);

  const rowHeight = 2;
  const margin: [number, number] = [10, 10];

  return (
    <div>
      <GridLayout
        className="layout"
        style={{ border: '1px solid blue' }}
        cols={12}
        rowHeight={rowHeight}
        width={1200}
        margin={margin}
        layout={layout}
        onLayoutChange={(e) => {
          _layout(e);
        }}
        isResizable={false}
      >
        {layout.map((x, i) => {
          return (
            <div
              key={x.i}
              style={{
                backgroundColor: 'gray',
                border: '1px solid red',
                overflow: 'hidden',
              }}
            >
              <div style={{ position: 'absolute' }}>
                <div
                  style={{
                    position: 'relative',
                    zIndex: 10,
                    left: 0,
                    top: 0,
                    right: 0,
                  }}
                >
                  <DynamicSizeContent
                    onHeightChange={(e) => {
                      //(rowHeight * h) + (marginH * (h - 1))=actual-height
                      //(rowHeight * h) + (marginH * h) - marginH
                      //(rowHeight + marginH)* h - marginH= actual-height
                      _layout((pre) => {
                        const data: Layout[] = [...pre];

                        const h = (e + margin[1]) / (rowHeight + margin[1]);

                        const divide = e / rowHeight;
                        const mod = e % rowHeight;
                        data[i] = {
                          ...x,
                          h: h,
                        };
                        return data;
                      });
                    }}
                  >
                    <div>
                      <b>
                        {i + 1}-{x.i}
                      </b>
                    </div>
                    <div style={{ wordBreak: 'break-word', display: 'none' }}>
                      {JSON.stringify(x)}
                    </div>
                  </DynamicSizeContent>
                </div>
              </div>
            </div>
          );
        })}
      </GridLayout>
    </div>
  );
};
