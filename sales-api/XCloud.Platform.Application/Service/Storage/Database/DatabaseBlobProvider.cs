﻿using System.IO;
using System.Threading.Tasks;
using Volo.Abp.BlobStoring;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Guids;
using Volo.Abp.Timing;
using XCloud.Application.Extension;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Storage.Database;

public class DatabaseBlobProvider : BlobProviderBase, ITransientDependency
{
    private readonly IDatabaseFileNameCalculator _databaseFileNameCalculator;
    private readonly IPlatformRepository<BlobData> _repository;
    private readonly IGuidGenerator _guidGenerator;
    private readonly IClock _clock;

    public DatabaseBlobProvider(IPlatformRepository<BlobData> repository,
        IGuidGenerator guidGenerator,
        IClock clock,
        IDatabaseFileNameCalculator databaseFileNameCalculator)
    {
        _repository = repository;
        _guidGenerator = guidGenerator;
        _clock = clock;
        _databaseFileNameCalculator = databaseFileNameCalculator;
    }

    public override async Task SaveAsync(BlobProviderSaveArgs args)
    {
        if (args == null)
            throw new ArgumentNullException(nameof(args));

        if (args.BlobStream == null)
            throw new ArgumentNullException(nameof(args.BlobStream));

        var name = this._databaseFileNameCalculator.CalculateDatabaseBlobFileName(args);

        var entity = await _repository.FirstOrDefaultAsync(x => x.Name == name);

        if (entity == null)
        {
            entity = new BlobData
            {
                Id = _guidGenerator.CreateGuidString(),
                Name = name,
                Content = await args.BlobStream.GetAllBytesAsync(),
                Base64Text = string.Empty,
                LastModificationTime = default,
                CreationTime = _clock.Now
            };

            await _repository.InsertAsync(entity);
        }
        else
        {
            if (!args.OverrideExisting)
                throw new BlobAlreadyExistsException("blob data is already exist in database");

            entity.Content = await args.BlobStream.GetAllBytesAsync();
            entity.LastModificationTime = _clock.Now;

            await _repository.UpdateAsync(entity);
        }
    }

    public override async Task<bool> DeleteAsync(BlobProviderDeleteArgs args)
    {
        var name = this._databaseFileNameCalculator.CalculateDatabaseBlobFileName(args);

        await _repository.DeleteAsync(x => x.Name == name);

        return true;
    }

    public override async Task<bool> ExistsAsync(BlobProviderExistsArgs args)
    {
        var name = this._databaseFileNameCalculator.CalculateDatabaseBlobFileName(args);

        return await _repository.AnyAsync(x => x.Name == name);
    }

    public override async Task<Stream> GetOrNullAsync(BlobProviderGetArgs args)
    {
        var name = this._databaseFileNameCalculator.CalculateDatabaseBlobFileName(args);

        var entity = await _repository.FirstOrDefaultAsync(x => x.Name == name);

        if (entity == null || entity.Content == null || ValidateHelper.IsEmptyCollection(entity.Content))
            return null;

        return new MemoryStream(entity.Content);
    }
}