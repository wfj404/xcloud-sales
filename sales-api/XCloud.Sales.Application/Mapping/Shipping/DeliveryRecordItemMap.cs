using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Mapping.Shipping;

public class DeliveryRecordItemMap : SalesEntityTypeConfiguration<DeliveryRecordItem>
{
    public override void Configure(EntityTypeBuilder<DeliveryRecordItem> builder)
    {
        builder.ToTable("sales_delivery_record_item");

        builder.PreConfigStringPrimaryKey();
        builder.Property(x => x.DeliveryId).RequiredId();
        builder.Property(x => x.OrderItemId).RequiredId();
        builder.Property(x => x.Quantity);

        base.Configure(builder);
    }
}