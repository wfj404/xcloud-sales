import { take } from 'lodash-es';
import { useEffect, useState } from 'react';

import GoodsItem from '@/components/blocks/components/goods_item';
import XLoading from '@/components/common/loading/skeleton';
import { apiClient } from '@/utils/client';
import { GoodsDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box, Typography } from '@mui/material';

import GoodsGrid from '../goods_grid';
import GoodsSlider from '../goods_slider';
import { HotGoodsBlockProps, HotGoodsBlockSearchParameter } from './utils';

export default ({ data }: { data: HotGoodsBlockProps }) => {
  const [goods, _goods] = useState<GoodsDto[]>([]);
  const [loading, _loading] = useState(false);

  const queryGoods = (q: HotGoodsBlockSearchParameter) => {
    _loading(true);
    apiClient.mall
      .searchSearchGoodsList({
        IsPublished: true,
        Keywords: q.keywords,
        BrandId: q.brand?.Id,
        CategoryId: q.category?.Id,
        TagId: q.tag?.Id,
        Page: 1,
        PageSize: q.count || 10,
        SkipCalculateTotalCount: true,
      })
      .then((res) => {
        _goods(res.data.Items || []);
      })
      .finally(() => _loading(false));
  };

  const querySelectedGoods = (q: GoodsDto[]) => {
    if (isArrayEmpty(q)) {
      return;
    }
    const ids = q.map((x) => x.Id || '');

    _loading(true);
    apiClient.mall
      .goodsByIds(ids)
      .then((res) => {
        const datalist: GoodsDto[] = [];

        //按照id排序
        q.forEach((x) => {
          const m = res.data.Data?.find((d) => d.Id == x.Id);
          if (m) {
            datalist.push(m);
          }
        });

        _goods(datalist);
      })
      .finally(() => _loading(false));
  };

  useEffect(() => {
    if (data.type == 'selected-goods') {
      querySelectedGoods(data.goods_list || []);
    } else {
      queryGoods(data.search || {});
    }
  }, [data]);

  const renderGoods = () => {
    if (loading) {
      return <XLoading />;
    }
    let goods_list = [...goods];
    if (data.type == 'selected-goods') {
    } else {
      goods_list = take(goods_list, data.search?.count || 1000);
    }

    if (data.display_type == 'slider') {
      return <GoodsSlider goods={goods_list} />;
    } else if (data.display_type == 'grid') {
      return <GoodsGrid goods={goods_list} />;
    } else if (data.display_type == 'item') {
      return <GoodsItem goods={goods_list} />;
    }
    return <GoodsGrid goods={goods_list} />;
  };

  return (
    <>
      <Box
        sx={{
          px: 1,
          py: 2,
        }}
      >
        <Typography variant="h3" component={'div'} gutterBottom>
          {`热销商品🔥`}
        </Typography>
        {renderGoods()}
      </Box>
    </>
  );
};
