﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Settings;

namespace XCloud.Platform.Application.Mapping.Settings;

public class SettingMap : PlatformEntityTypeConfiguration<Setting>
{
    public override void Configure(EntityTypeBuilder<Setting> builder)
    {
        builder.ToTable("sys_setting");
        
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(300);
        builder.Property(x => x.Value);
            
        base.Configure(builder);
    }
}