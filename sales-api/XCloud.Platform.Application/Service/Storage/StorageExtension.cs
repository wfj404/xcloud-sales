﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;
using Volo.Abp.Http;
using Volo.Abp.Validation;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Domain.Storage;

namespace XCloud.Platform.Application.Service.Storage;

public static class StorageExtension
{
    public static async Task<string> CalculateHashStringAsync(this IFileHashProvider fileHashProvider, byte[] bs)
    {
        if (ValidateHelper.IsEmptyCollection(bs))
            throw new ArgumentNullException(nameof(bs));

        using var ms = new MemoryStream();

        await ms.WriteAsync(bs);

        ms.ResetPositionOrThrow();

        return await fileHashProvider.CalculateHashStringAsync(ms);
    }

    public static void Simplify(this StorageMetaDto dto)
    {
        dto.ResourceSize = default;
        dto.HashType = default;
        dto.ResourceHash = default;
        dto.ExtraData = default;
    }

    private static int StreamOriginPosition => 0;

    public static void EnsureOriginPosition(this Stream stream)
    {
        if (stream.Position != StreamOriginPosition)
            throw new IOException(nameof(ResetPositionOrThrow));
    }

    public static void ResetPositionOrThrow(this Stream stream)
    {
        if (stream.Position == StreamOriginPosition)
            return;

        if (stream.CanSeek)
        {
            stream.Seek(0, SeekOrigin.Begin);
            stream.Position = StreamOriginPosition;
        }

        EnsureOriginPosition(stream);
    }

    public static async Task<byte[]> GetFileBytesOrNullAsync(this IStorageService storageService, string fileKey)
    {
        var stream = await storageService.GetFileStreamOrNullAsync(fileKey);

        if (stream != null)
        {
            await using (stream)
            {
                using var ms = new MemoryStream();
                await stream.CopyToAsync(ms);
                var bs = ms.ToArray();
                return bs;
            }
        }

        return null;
    }

    public static async Task<string> GetContentTypeOrDefaultAsync(this IStorageMetaService storageMetaService,
        string key, string defaultContentType = default)
    {
        var contentType = await storageMetaService.GetContentTypeOrNullAsync(key);

        if (string.IsNullOrWhiteSpace(contentType))
        {
            contentType = defaultContentType ?? MimeTypes.Application.OctetStream;
        }

        return contentType;
    }

    public static async Task<StorageMeta> UploadBytesAsync(this IStorageService storageService,
        FileUploadBytesArgs args)
    {
        if (args == null || ValidateHelper.IsEmptyCollection(args.Bytes))
            throw new AbpValidationException(nameof(UploadBytesAsync));

        using var ms = new MemoryStream();
        await ms.WriteAsync(args.Bytes);

        var streamArgs = new FileUploadStreamArgs(ms)
        {
            FileName = args.FileName,
            ContentType = args.ContentType
        };

        var model = await storageService.UploadStreamAsync(streamArgs);

        return model;
    }

    public static async Task<StorageMeta> UploadFromUrlAsync(this IStorageService storageService,
        HttpClient httpClient,
        FileUploadFromUrl dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Url))
            throw new ArgumentNullException(nameof(dto.Url));

        using var response = await httpClient.GetAsync(dto.Url);

        await using var stream = await response.Content.ReadAsStreamAsync();

        var args = new FileUploadStreamArgs(stream)
        {
            FileName = dto.FileName,
            ContentType = response.Content.Headers.ContentType?.MediaType
        };

        if (response.Headers.TryGetValues(HeaderNames.ContentType, out var val))
        {
            args.ContentType = val.FirstOrDefault();
        }

        return await storageService.UploadStreamAsync(args);
    }
}