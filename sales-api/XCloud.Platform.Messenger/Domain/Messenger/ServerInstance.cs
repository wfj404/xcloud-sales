using System;
using Volo.Abp.Auditing;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Messenger.Domain.Messenger;

public class ServerInstance : PlatformBaseEntity, IHasCreationTime
{
    public string InstanceId { get; set; }
    public int ConnectionCount { get; set; }
    public DateTime PingTime { get; set; }
    public DateTime CreationTime { get; set; }
}