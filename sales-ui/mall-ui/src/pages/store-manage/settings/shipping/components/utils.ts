import { Dayjs } from 'dayjs';
import { groupBy, sortBy } from 'lodash-es';

import { dateFormat, myDayjs, parseAsDayjs } from '@/utils/dayjs';

//王冰清给的格式
let data_format_from_wbq = {
  data: [
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '21',
      WEEK: '3',
      TIMES: [
        {
          ST: '09:00',
          ET: '10:00',
          TYPE: 'AM',
        },
        {
          ST: '10:00',
          ET: '11:00',
          TYPE: 'AM',
        },
        {
          ST: '11:00',
          ET: '12:00',
          TYPE: 'AM',
        },
        {
          ST: '12:00',
          ET: '13:00',
          TYPE: 'PM',
        },
        {
          ST: '13:00',
          ET: '14:00',
          TYPE: 'PM',
        },
        {
          ST: '14:00',
          ET: '15:00',
          TYPE: 'PM',
        },
        {
          ST: '15:00',
          ET: '16:00',
          TYPE: 'PM',
        },
        {
          ST: '16:00',
          ET: '17:00',
          TYPE: 'PM',
        },
      ],
    },
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '22',
      WEEK: '4',
      TIMES: [],
    },
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '23',
      WEEK: '5',
      TIMES: [],
    },
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '24',
      WEEK: '6',
      TIMES: [],
    },
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '25',
      WEEK: '7',
      TIMES: [
        {
          ST: '09:00',
          ET: '10:00',
          TYPE: 'AM',
        },
        {
          ST: '10:00',
          ET: '11:00',
          TYPE: 'AM',
        },
        {
          ST: '11:00',
          ET: '12:00',
          TYPE: 'AM',
        },
        {
          ST: '12:00',
          ET: '13:00',
          TYPE: 'PM',
        },
        {
          ST: '13:00',
          ET: '14:00',
          TYPE: 'PM',
        },
        {
          ST: '14:00',
          ET: '15:00',
          TYPE: 'PM',
        },
        {
          ST: '15:00',
          ET: '16:00',
          TYPE: 'PM',
        },
        {
          ST: '16:00',
          ET: '17:00',
          TYPE: 'PM',
        },
      ],
    },
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '26',
      WEEK: '1',
      TIMES: [
        {
          ST: '09:00',
          ET: '10:00',
          TYPE: 'AM',
        },
        {
          ST: '10:00',
          ET: '11:00',
          TYPE: 'AM',
        },
        {
          ST: '11:00',
          ET: '12:00',
          TYPE: 'AM',
        },
        {
          ST: '12:00',
          ET: '13:00',
          TYPE: 'PM',
        },
        {
          ST: '13:00',
          ET: '14:00',
          TYPE: 'PM',
        },
        {
          ST: '14:00',
          ET: '15:00',
          TYPE: 'PM',
        },
        {
          ST: '15:00',
          ET: '16:00',
          TYPE: 'PM',
        },
        {
          ST: '16:00',
          ET: '17:00',
          TYPE: 'PM',
        },
      ],
    },
    {
      YEAR: '2023',
      MONTH: '06',
      DAY: '27',
      WEEK: '2',
      TIMES: [
        {
          ST: '09:00',
          ET: '10:00',
          TYPE: 'AM',
        },
        {
          ST: '10:00',
          ET: '11:00',
          TYPE: 'AM',
        },
        {
          ST: '11:00',
          ET: '12:00',
          TYPE: 'AM',
        },
        {
          ST: '12:00',
          ET: '13:00',
          TYPE: 'PM',
        },
        {
          ST: '13:00',
          ET: '14:00',
          TYPE: 'PM',
        },
        {
          ST: '14:00',
          ET: '15:00',
          TYPE: 'PM',
        },
        {
          ST: '15:00',
          ET: '16:00',
          TYPE: 'PM',
        },
        {
          ST: '16:00',
          ET: '17:00',
          TYPE: 'PM',
        },
      ],
    },
  ],
};

let data: string[] = [
  '2023-07-06 09:00:00',
  '2023-07-06 10:00:00',
  '2023-07-06 11:00:00',
  '2023-07-06 13:00:00',
  '2023-07-06 14:00:00',
  '2023-07-06 15:00:00',
  '2023-07-06 16:00:00',
  '2023-07-06 17:00:00',
  '2023-07-07 00:00:00',
  '2023-07-07 09:00:00',
  '2023-07-07 10:00:00',
  '2023-07-07 11:00:00',
  '2023-07-07 13:00:00',
  '2023-07-07 14:00:00',
  '2023-07-07 15:00:00',
  '2023-07-07 16:00:00',
  '2023-07-07 17:00:00',
  '2023-07-10 00:00:00',
  '2023-07-10 09:00:00',
  '2023-07-10 10:00:00',
  '2023-07-10 11:00:00',
  '2023-07-10 13:00:00',
  '2023-07-10 14:00:00',
  '2023-07-10 15:00:00',
  '2023-07-10 16:00:00',
  '2023-07-10 17:00:00',
  '2023-07-11 00:00:00',
  '2023-07-11 09:00:00',
  '2023-07-11 10:00:00',
  '2023-07-11 11:00:00',
  '2023-07-11 13:00:00',
  '2023-07-11 14:00:00',
  '2023-07-11 15:00:00',
  '2023-07-11 16:00:00',
  '2023-07-11 17:00:00',
  '2023-07-12 00:00:00',
  '2023-07-12 09:00:00',
  '2023-07-12 10:00:00',
  '2023-07-12 11:00:00',
  '2023-07-12 13:00:00',
  '2023-07-12 14:00:00',
  '2023-07-12 15:00:00',
  '2023-07-12 16:00:00',
  '2023-07-12 17:00:00',
  '2023-07-13 00:00:00',
];

interface ServiceTime {
  time: Dayjs;
  time_str: string;
  type: string;
}

interface ServiceDate {
  date: Dayjs;
  week: number;
  time: ServiceTime[];
}

const handle_times = (times: Dayjs[]): ServiceTime[] => {
  times = sortBy(times, (d) => d.valueOf());

  return times.map((x) => ({
    time: x,
    time_str: `${x.hour().toString().padStart(2, '0')}:${x
      .minute()
      .toString()
      .padStart(2, '0')}`,
    type: x.hour() >= 12 ? 'PM' : 'AM',
  }));
};

export const test_convert = (): ServiceDate[] => {
  let time_list = data.map<Dayjs>((x) => parseAsDayjs(x) || myDayjs.utc());

  let grouped_list = groupBy(time_list, (x) => x.format(dateFormat));

  //the key is date format string
  let keys = Object.keys(grouped_list);
  keys = sortBy(keys, (x) => parseAsDayjs(x)?.valueOf() || 0);

  //transform
  let key_values = keys.map<ServiceDate>((x) => {
    //the date
    let date = myDayjs(x);
    //resolve time ranges at current date
    return {
      date: date,
      week: date.day() + 1,
      time: handle_times(grouped_list[x] || []),
    };
  });

  console.log(key_values);

  return key_values;
};
