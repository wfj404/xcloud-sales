﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XCloud.Sales.Qingdao.Application.Migrations
{
    /// <inheritdoc />
    public partial class invoice : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "mcs_invoice_request",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    BizType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    TotalAmount = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Status = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    InvoicingTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Picked = table.Column<bool>(type: "bit", nullable: false),
                    PickedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    McsUserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PickupLocationId = table.Column<int>(type: "int", nullable: false),
                    PickupLocationName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    AttachmentMetaId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mcs_invoice_request", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mcs_invoice_request_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    InvoiceRequestId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    McsBizOrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mcs_invoice_request_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_activity_log",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActivityLogTypeId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    StoreUserId = table.Column<int>(type: "int", nullable: false),
                    AdministratorId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ManagerId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Comment = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Value = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UrlReferrer = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    BrowserType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Device = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    UserAgent = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    IpAddress = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    GeoCountry = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    GeoCity = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Lng = table.Column<double>(type: "float", nullable: true),
                    Lat = table.Column<double>(type: "float", nullable: true),
                    RequestPath = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    SubjectType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SubjectId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SubjectIntId = table.Column<int>(type: "int", nullable: false, defaultValue: 0),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_activity_log", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_after_sales",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ReasonForReturn = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    RequestedAction = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    UserComments = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    StaffNotes = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Images = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    AfterSalesStatusId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    HideForCustomer = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_after_sales", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_after_sales_comment",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    AfterSaleId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Content = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    PictureJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsAdmin = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_after_sales_comment", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_after_sales_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    AfterSalesId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_after_sales_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_balance_history",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    LatestBalance = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    ActionType = table.Column<int>(type: "int", nullable: false),
                    Message = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_balance_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_brand",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    ShowOnPublicPage = table.Column<bool>(type: "bit", nullable: false),
                    MetaKeywords = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    PictureMetaId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_brand", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_category",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SeoName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    RootId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    NodePath = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    ParentCategoryId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PictureMetaId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ShowOnHomePage = table.Column<bool>(type: "bit", nullable: false),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false),
                    Recommend = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_common_entity_mapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EntityId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    EntityName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    TargetId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    TargetName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_common_entity_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_common_entity_not_mapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EntityId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    EntityName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    TargetId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    TargetName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_common_entity_not_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_coupon",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CouponType = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    OrderConditionRulesJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExpiredInDays = table.Column<int>(type: "int", nullable: true),
                    Amount = table.Column<int>(type: "int", nullable: false),
                    AccountIssuedLimitCount = table.Column<int>(type: "int", nullable: true),
                    LimitedToStore = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_coupon", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_coupon_user_mapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CouponId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    IsUsed = table.Column<bool>(type: "bit", nullable: false),
                    UsedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ExpiredAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_coupon_user_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_delivery_record",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PreferDeliveryStartTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PreferDeliveryEndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeliveryType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ExpressName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    TrackingNumber = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Delivering = table.Column<bool>(type: "bit", nullable: false),
                    DeliveringTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Delivered = table.Column<bool>(type: "bit", nullable: false),
                    DeliveredTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_delivery_record", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_delivery_record_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    DeliveryId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_delivery_record_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_delivery_record_track",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeliveryId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Lon = table.Column<double>(type: "float", nullable: false),
                    Lat = table.Column<double>(type: "float", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_delivery_record_track", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_discount",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    DiscountPercentage = table.Column<double>(type: "float", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    LimitedToStore = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    LimitedToSku = table.Column<bool>(type: "bit", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_discount", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_freight_template",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ShipInHours = table.Column<int>(type: "int", nullable: true),
                    FreeShippingMinOrderTotalPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    CalculateType = table.Column<int>(type: "int", nullable: false),
                    RulesJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExcludedAreaJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_freight_template", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_freight_template_goods_mapping",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    TemplateId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_freight_template_goods_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_generic_order",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Paid = table.Column<bool>(type: "bit", nullable: false),
                    Serviced = table.Column<bool>(type: "bit", nullable: false),
                    ToService = table.Column<bool>(type: "bit", nullable: false),
                    Reviewed = table.Column<bool>(type: "bit", nullable: false),
                    Finished = table.Column<bool>(type: "bit", nullable: false),
                    FinishedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Closed = table.Column<bool>(type: "bit", nullable: false),
                    ClosedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ItemsJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExtraDataJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StoreUserId = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_generic_order", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_gift_card",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Used = table.Column<bool>(type: "bit", nullable: false),
                    UsedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_gift_card", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_goods",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    GoodsType = table.Column<int>(type: "int", nullable: false, defaultValue: 0),
                    Name = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    SeoName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Keywords = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    DetailInformation = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdminComment = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CategoryId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BrandId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    LimitedToRedirect = table.Column<bool>(type: "bit", nullable: false),
                    RedirectToUrlJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LimitedToContact = table.Column<bool>(type: "bit", nullable: false),
                    ContactTelephone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ApprovedReviewsRates = table.Column<double>(type: "float", nullable: false),
                    ApprovedTotalReviews = table.Column<int>(type: "int", nullable: false),
                    StockDeductStrategy = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    StickyTop = table.Column<bool>(type: "bit", nullable: false),
                    DisplayAsInformationPage = table.Column<bool>(type: "bit", nullable: false),
                    PlaceOrderSupported = table.Column<bool>(type: "bit", nullable: false),
                    AfterSalesSupported = table.Column<bool>(type: "bit", nullable: false),
                    PayAfterDeliveredSupported = table.Column<bool>(type: "bit", nullable: false),
                    CommentSupported = table.Column<bool>(type: "bit", nullable: false),
                    Published = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_goods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_goods_grade_price",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    GradeId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_goods_grade_price", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_goods_picture",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PictureMetaId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    FileName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    DisplayOrder = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_goods_picture", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_goods_virtual_extension",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ExpiredAfterDays = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_goods_virtual_extension", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_grade",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    UserCount = table.Column<int>(type: "int", nullable: false),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_grade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_notification",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Content = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    BusinessId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    App = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Read = table.Column<bool>(type: "bit", nullable: false),
                    ReadTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    ActionType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SenderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SenderType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_notification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    GoodsType = table.Column<int>(type: "int", nullable: false),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderSn = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    GradeId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OrderStatusId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ServiceStartTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ServiceEndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CompleteTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShippingMethodId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ShippingStatusId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    UserAddressId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsAfterSales = table.Column<bool>(type: "bit", nullable: false),
                    PaymentMethodId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PaymentStatusId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PaidTotalPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    RefundedAmount = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    ItemsTotalPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    PackageFee = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    ShippingFee = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    SellerPriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    CouponPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    PromotionId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PromotionPriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    RewardPointsHistoryId = table.Column<int>(type: "int", nullable: true),
                    ExchangePointsAmount = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    UserInvoiceId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    AffiliateId = table.Column<int>(type: "int", nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    OrderIp = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PlatformId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    HideForCustomer = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_coupon_record",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    UserCouponId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_coupon_record", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_inventory_deduction_history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_inventory_deduction_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    GoodsName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    SkuName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    SkuDescription = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Weight = table.Column<int>(type: "int", nullable: false),
                    StockDeductStrategy = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    BasePrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    GradePriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    DiscountRate = table.Column<double>(type: "float", nullable: false),
                    ValueAddedPriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Remark = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_item_gift",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    OrderItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_item_gift", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_note",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Note = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    DisplayToUser = table.Column<bool>(type: "bit", nullable: false),
                    ExtendedJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_note", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_review",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Approved = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    ReviewText = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Rating = table.Column<double>(type: "float", nullable: false),
                    PictureMetaIdsJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExtraData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IpAddress = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_review", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_review_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    ReviewId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OrderItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ReviewText = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    PictureMetaIdsJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rating = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_review_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_order_value_added_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ValueAddedItemId = table.Column<int>(type: "int", nullable: false),
                    PriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_order_value_added_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_page",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    SeoName = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    CoverImageResourceData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    BodyContent = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReadCount = table.Column<int>(type: "int", nullable: false),
                    IsPublished = table.Column<bool>(type: "bit", nullable: false),
                    PublishedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_page", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_payment_bill",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    PaymentChannel = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Paid = table.Column<bool>(type: "bit", nullable: false),
                    PayTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PaymentTransactionId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    NotifyData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_payment_bill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_pickup_record",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Prepared = table.Column<bool>(type: "bit", nullable: false),
                    PreparedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ReservationApproved = table.Column<bool>(type: "bit", nullable: true),
                    ReservationApprovedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PreferPickupStartTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PreferPickupEndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Picked = table.Column<bool>(type: "bit", nullable: false),
                    PickedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SecurityCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_pickup_record", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_points_history",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Points = table.Column<int>(type: "int", nullable: false),
                    PointsBalance = table.Column<int>(type: "int", nullable: false),
                    ActionType = table.Column<int>(type: "int", nullable: false),
                    UsedAmount = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Message = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_points_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_refund_bill",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    OrderId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PaymentBillId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Approved = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    ApprovedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Refunded = table.Column<bool>(type: "bit", nullable: false),
                    RefundTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RefundTransactionId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    RefundNotifyData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_refund_bill", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_shopping_cart_item",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_shopping_cart_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_shopping_cart_value_added_item",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShoppingCartItemId = table.Column<int>(type: "int", nullable: false),
                    ValueAddedItemId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_shopping_cart_value_added_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_sku",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    SkuCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Color = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    MaxAmountInOnePurchase = table.Column<int>(type: "int", nullable: false),
                    MinAmountInOnePurchase = table.Column<int>(type: "int", nullable: false),
                    Weight = table.Column<int>(type: "int", nullable: false),
                    SpecCombinationJson = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "规格配置参数"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_sku", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_spec",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_spec", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_spec_value",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SpecId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    PriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    AssociatedSkuId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_spec_value", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_stock",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    No = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SupplierId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    WarehouseId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Remark = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Approved = table.Column<bool>(type: "bit", nullable: false),
                    ApprovedByUserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ApprovedTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_stock", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_stock_item",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    StockId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    DeductQuantity = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_stock_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_stock_usage_history",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    StockItemId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_stock_usage_history", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    AddressDetail = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Opening = table.Column<bool>(type: "bit", nullable: false),
                    InvoiceSupported = table.Column<bool>(type: "bit", nullable: false),
                    Lon = table.Column<double>(type: "float", nullable: false),
                    Lat = table.Column<double>(type: "float", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    ExternalId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_goods_mapping",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    SkuId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    StockQuantity = table.Column<int>(type: "int", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    OverridePrice = table.Column<bool>(type: "bit", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_goods_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_manager",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsSuperManager = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_manager", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_manager_role",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    RoleId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ManagerId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_manager_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_role",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    StoreId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_role_permission",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    PermissionKey = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_role_permission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_user",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Points = table.Column<int>(type: "int", nullable: false),
                    HistoryPoints = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastActivityTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_user", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_store_user_grade_mapping",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    GradeId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_store_user_grade_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_supplier",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ContactName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_supplier", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_tag",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Alert = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    Link = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_tag_goods_mapping",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_tag_goods_mapping", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_value_added_item",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GroupId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PriceOffset = table.Column<decimal>(type: "decimal(18,2)", precision: 18, scale: 2, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_value_added_item", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_value_added_item_group",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    GoodsId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsRequired = table.Column<bool>(type: "bit", nullable: false),
                    MultipleChoice = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_value_added_item_group", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sales_warehouse",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Lat = table.Column<double>(type: "float", nullable: true),
                    Lng = table.Column<double>(type: "float", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sales_warehouse", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_admin",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsSuperAdmin = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_admin", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_admin_role",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    RoleId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    AdminId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_admin_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_blob_data",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Content = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    Base64Text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Format = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_blob_data", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_external_access_token",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Platform = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    AppId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Scope = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    AccessTokenType = table.Column<int>(type: "int", nullable: false),
                    AccessToken = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    RefreshToken = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    ExpiredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_external_access_token", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_external_connect",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Platform = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    AppId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OpenId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_external_connect", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_im_message",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Content = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    MessageType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Data = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_im_message", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_im_online_status",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    DeviceId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ServerInstanceId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PingTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_im_online_status", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_im_server_instance",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    InstanceId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ConnectionCount = table.Column<int>(type: "int", nullable: false),
                    PingTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_im_server_instance", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_permission",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    PermissionKey = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Group = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    AppKey = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_permission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_resource_acl",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    ResourceType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ResourceId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PermissionType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PermissionId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    AccessControlType = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_resource_acl", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_role",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_role_permission",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    PermissionKey = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_role_permission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_serial_no",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Category = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    NextId = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_serial_no", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_setting",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Name = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_setting", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_storage_meta",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    FileExtension = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ContentType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ResourceSize = table.Column<long>(type: "bigint", nullable: false),
                    ResourceKey = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    ResourceHash = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    HashType = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    ExtraData = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StorageProvider = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ReferenceCount = table.Column<int>(type: "int", nullable: false),
                    UploadTimes = table.Column<int>(type: "int", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_storage_meta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_user",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    IdentityName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OriginIdentityName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ExternalId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    NickName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PassWord = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Avatar = table.Column<string>(type: "nvarchar(2000)", maxLength: 2000, nullable: true),
                    LastPasswordUpdateTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间"),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "更新时间，同时也是乐观锁"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_user_address",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Lat = table.Column<double>(type: "float", nullable: true),
                    Lon = table.Column<double>(type: "float", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    NationCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Nation = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ProvinceCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Province = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CityCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    City = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    AreaCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Area = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    AddressDescription = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    AddressDetail = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    PostalCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Tel = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    ExtendedJson = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user_address", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_user_identity",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Identity = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Data = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: true),
                    IdentityType = table.Column<int>(type: "int", nullable: false),
                    MobilePhone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, defaultValue: ""),
                    MobileAreaCode = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true, defaultValue: ""),
                    MobileConfirmed = table.Column<bool>(type: "bit", nullable: true),
                    MobileConfirmedTimeUtc = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true, defaultValue: ""),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: true),
                    EmailConfirmedTimeUtc = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user_identity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_user_invoice",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Head = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Code = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    InvoiceType = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BankName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BankCode = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Content = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false, comment: "是否删除"),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "删除时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user_invoice", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_user_real_name",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    IdCardIdentity = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Data = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: true),
                    IdCardType = table.Column<int>(type: "int", nullable: false),
                    IdCard = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IdCardRealName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    IdCardFrontUrl = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    IdCardBackUrl = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    IdCardHandUrl = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    StartTimeUtc = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndTimeUtc = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdCardStatus = table.Column<int>(type: "int", nullable: false),
                    IdCardConfirmed = table.Column<bool>(type: "bit", nullable: true),
                    IdCardConfirmedTimeUtc = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "创建时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_user_real_name", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_wx_union",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false, comment: "主键，最好是顺序生成的GUID"),
                    Platform = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    AppId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    OpenId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    UnionId = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_wx_union", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_mcs_invoice_request_CreationTime",
                table: "mcs_invoice_request",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_mcs_invoice_request_IsDeleted",
                table: "mcs_invoice_request",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_activity_log_CreationTime",
                table: "sales_activity_log",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_after_sales_CreationTime",
                table: "sales_after_sales",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_after_sales_LastModificationTime",
                table: "sales_after_sales",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_after_sales_comment_CreationTime",
                table: "sales_after_sales_comment",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_balance_history_CreationTime",
                table: "sales_balance_history",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_brand_CreationTime",
                table: "sales_brand",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_brand_IsDeleted",
                table: "sales_brand",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_category_CreationTime",
                table: "sales_category",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_category_IsDeleted",
                table: "sales_category",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_coupon_CreationTime",
                table: "sales_coupon",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_coupon_IsDeleted",
                table: "sales_coupon",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_coupon_user_mapping_CreationTime",
                table: "sales_coupon_user_mapping",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_delivery_record_CreationTime",
                table: "sales_delivery_record",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_delivery_record_LastModificationTime",
                table: "sales_delivery_record",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_delivery_record_track_CreationTime",
                table: "sales_delivery_record_track",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_discount_CreationTime",
                table: "sales_discount",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_discount_IsDeleted",
                table: "sales_discount",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_freight_template_CreationTime",
                table: "sales_freight_template",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_freight_template_goods_mapping_CreationTime",
                table: "sales_freight_template_goods_mapping",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_generic_order_CreationTime",
                table: "sales_generic_order",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_generic_order_LastModificationTime",
                table: "sales_generic_order",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_gift_card_CreationTime",
                table: "sales_gift_card",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_gift_card_IsDeleted",
                table: "sales_gift_card",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_goods_IsDeleted",
                table: "sales_goods",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_goods_LastModificationTime",
                table: "sales_goods",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_grade_IsDeleted",
                table: "sales_grade",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_notification_CreationTime",
                table: "sales_notification",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_notification_LastModificationTime",
                table: "sales_notification",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_order_CreationTime",
                table: "sales_order",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_order_LastModificationTime",
                table: "sales_order",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_order_inventory_deduction_history_CreationTime",
                table: "sales_order_inventory_deduction_history",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_order_note_CreationTime",
                table: "sales_order_note",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_order_review_CreationTime",
                table: "sales_order_review",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_order_review_LastModificationTime",
                table: "sales_order_review",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_page_CreationTime",
                table: "sales_page",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_page_IsDeleted",
                table: "sales_page",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_payment_bill_CreationTime",
                table: "sales_payment_bill",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_payment_bill_IsDeleted",
                table: "sales_payment_bill",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_pickup_record_CreationTime",
                table: "sales_pickup_record",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_points_history_CreationTime",
                table: "sales_points_history",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_refund_bill_CreationTime",
                table: "sales_refund_bill",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_refund_bill_IsDeleted",
                table: "sales_refund_bill",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_refund_bill_LastModificationTime",
                table: "sales_refund_bill",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_shopping_cart_item_CreationTime",
                table: "sales_shopping_cart_item",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_shopping_cart_item_LastModificationTime",
                table: "sales_shopping_cart_item",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_sku_CreationTime",
                table: "sales_sku",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_sku_IsDeleted",
                table: "sales_sku",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_sku_LastModificationTime",
                table: "sales_sku",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_spec_IsDeleted",
                table: "sales_spec",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_spec_value_IsDeleted",
                table: "sales_spec_value",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_stock_CreationTime",
                table: "sales_stock",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_stock_item_LastModificationTime",
                table: "sales_stock_item",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_stock_usage_history_CreationTime",
                table: "sales_stock_usage_history",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_CreationTime",
                table: "sales_store",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_IsDeleted",
                table: "sales_store",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_goods_mapping_CreationTime",
                table: "sales_store_goods_mapping",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_goods_mapping_LastModificationTime",
                table: "sales_store_goods_mapping",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_manager_CreationTime",
                table: "sales_store_manager",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_manager_role_CreationTime",
                table: "sales_store_manager_role",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_role_CreationTime",
                table: "sales_store_role",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_role_permission_CreationTime",
                table: "sales_store_role_permission",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_user_LastModificationTime",
                table: "sales_store_user",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_store_user_grade_mapping_CreationTime",
                table: "sales_store_user_grade_mapping",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_supplier_CreationTime",
                table: "sales_supplier",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_supplier_IsDeleted",
                table: "sales_supplier",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_tag_IsDeleted",
                table: "sales_tag",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sales_warehouse_CreationTime",
                table: "sales_warehouse",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sales_warehouse_IsDeleted",
                table: "sales_warehouse",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sys_admin_CreationTime",
                table: "sys_admin",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_admin_role_CreationTime",
                table: "sys_admin_role",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_blob_data_CreationTime",
                table: "sys_blob_data",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_blob_data_LastModificationTime",
                table: "sys_blob_data",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_blob_data_Name",
                table: "sys_blob_data",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sys_external_access_token_CreationTime",
                table: "sys_external_access_token",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_external_connect_CreationTime",
                table: "sys_external_connect",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_im_message_CreationTime",
                table: "sys_im_message",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_im_online_status_CreationTime",
                table: "sys_im_online_status",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_im_server_instance_CreationTime",
                table: "sys_im_server_instance",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_permission_CreationTime",
                table: "sys_permission",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_resource_acl_CreationTime",
                table: "sys_resource_acl",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_role_CreationTime",
                table: "sys_role",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_role_permission_CreationTime",
                table: "sys_role_permission",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_serial_no_Category",
                table: "sys_serial_no",
                column: "Category",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sys_serial_no_CreationTime",
                table: "sys_serial_no",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_serial_no_LastModificationTime",
                table: "sys_serial_no",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_storage_meta_CreationTime",
                table: "sys_storage_meta",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_storage_meta_LastModificationTime",
                table: "sys_storage_meta",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_CreationTime",
                table: "sys_user",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_IdentityName",
                table: "sys_user",
                column: "IdentityName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_IsDeleted",
                table: "sys_user",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_LastModificationTime",
                table: "sys_user",
                column: "LastModificationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_address_CreationTime",
                table: "sys_user_address",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_address_IsDeleted",
                table: "sys_user_address",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_identity_CreationTime",
                table: "sys_user_identity",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_identity_Identity",
                table: "sys_user_identity",
                column: "Identity",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_identity_UserId",
                table: "sys_user_identity",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_invoice_CreationTime",
                table: "sys_user_invoice",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_invoice_IsDeleted",
                table: "sys_user_invoice",
                column: "IsDeleted");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_real_name_CreationTime",
                table: "sys_user_real_name",
                column: "CreationTime");

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_real_name_IdCardIdentity",
                table: "sys_user_real_name",
                column: "IdCardIdentity",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_sys_user_real_name_UserId",
                table: "sys_user_real_name",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mcs_invoice_request");

            migrationBuilder.DropTable(
                name: "mcs_invoice_request_item");

            migrationBuilder.DropTable(
                name: "sales_activity_log");

            migrationBuilder.DropTable(
                name: "sales_after_sales");

            migrationBuilder.DropTable(
                name: "sales_after_sales_comment");

            migrationBuilder.DropTable(
                name: "sales_after_sales_item");

            migrationBuilder.DropTable(
                name: "sales_balance_history");

            migrationBuilder.DropTable(
                name: "sales_brand");

            migrationBuilder.DropTable(
                name: "sales_category");

            migrationBuilder.DropTable(
                name: "sales_common_entity_mapping");

            migrationBuilder.DropTable(
                name: "sales_common_entity_not_mapping");

            migrationBuilder.DropTable(
                name: "sales_coupon");

            migrationBuilder.DropTable(
                name: "sales_coupon_user_mapping");

            migrationBuilder.DropTable(
                name: "sales_delivery_record");

            migrationBuilder.DropTable(
                name: "sales_delivery_record_item");

            migrationBuilder.DropTable(
                name: "sales_delivery_record_track");

            migrationBuilder.DropTable(
                name: "sales_discount");

            migrationBuilder.DropTable(
                name: "sales_freight_template");

            migrationBuilder.DropTable(
                name: "sales_freight_template_goods_mapping");

            migrationBuilder.DropTable(
                name: "sales_generic_order");

            migrationBuilder.DropTable(
                name: "sales_gift_card");

            migrationBuilder.DropTable(
                name: "sales_goods");

            migrationBuilder.DropTable(
                name: "sales_goods_grade_price");

            migrationBuilder.DropTable(
                name: "sales_goods_picture");

            migrationBuilder.DropTable(
                name: "sales_goods_virtual_extension");

            migrationBuilder.DropTable(
                name: "sales_grade");

            migrationBuilder.DropTable(
                name: "sales_notification");

            migrationBuilder.DropTable(
                name: "sales_order");

            migrationBuilder.DropTable(
                name: "sales_order_coupon_record");

            migrationBuilder.DropTable(
                name: "sales_order_inventory_deduction_history");

            migrationBuilder.DropTable(
                name: "sales_order_item");

            migrationBuilder.DropTable(
                name: "sales_order_item_gift");

            migrationBuilder.DropTable(
                name: "sales_order_note");

            migrationBuilder.DropTable(
                name: "sales_order_review");

            migrationBuilder.DropTable(
                name: "sales_order_review_item");

            migrationBuilder.DropTable(
                name: "sales_order_value_added_item");

            migrationBuilder.DropTable(
                name: "sales_page");

            migrationBuilder.DropTable(
                name: "sales_payment_bill");

            migrationBuilder.DropTable(
                name: "sales_pickup_record");

            migrationBuilder.DropTable(
                name: "sales_points_history");

            migrationBuilder.DropTable(
                name: "sales_refund_bill");

            migrationBuilder.DropTable(
                name: "sales_shopping_cart_item");

            migrationBuilder.DropTable(
                name: "sales_shopping_cart_value_added_item");

            migrationBuilder.DropTable(
                name: "sales_sku");

            migrationBuilder.DropTable(
                name: "sales_spec");

            migrationBuilder.DropTable(
                name: "sales_spec_value");

            migrationBuilder.DropTable(
                name: "sales_stock");

            migrationBuilder.DropTable(
                name: "sales_stock_item");

            migrationBuilder.DropTable(
                name: "sales_stock_usage_history");

            migrationBuilder.DropTable(
                name: "sales_store");

            migrationBuilder.DropTable(
                name: "sales_store_goods_mapping");

            migrationBuilder.DropTable(
                name: "sales_store_manager");

            migrationBuilder.DropTable(
                name: "sales_store_manager_role");

            migrationBuilder.DropTable(
                name: "sales_store_role");

            migrationBuilder.DropTable(
                name: "sales_store_role_permission");

            migrationBuilder.DropTable(
                name: "sales_store_user");

            migrationBuilder.DropTable(
                name: "sales_store_user_grade_mapping");

            migrationBuilder.DropTable(
                name: "sales_supplier");

            migrationBuilder.DropTable(
                name: "sales_tag");

            migrationBuilder.DropTable(
                name: "sales_tag_goods_mapping");

            migrationBuilder.DropTable(
                name: "sales_value_added_item");

            migrationBuilder.DropTable(
                name: "sales_value_added_item_group");

            migrationBuilder.DropTable(
                name: "sales_warehouse");

            migrationBuilder.DropTable(
                name: "sys_admin");

            migrationBuilder.DropTable(
                name: "sys_admin_role");

            migrationBuilder.DropTable(
                name: "sys_blob_data");

            migrationBuilder.DropTable(
                name: "sys_external_access_token");

            migrationBuilder.DropTable(
                name: "sys_external_connect");

            migrationBuilder.DropTable(
                name: "sys_im_message");

            migrationBuilder.DropTable(
                name: "sys_im_online_status");

            migrationBuilder.DropTable(
                name: "sys_im_server_instance");

            migrationBuilder.DropTable(
                name: "sys_permission");

            migrationBuilder.DropTable(
                name: "sys_resource_acl");

            migrationBuilder.DropTable(
                name: "sys_role");

            migrationBuilder.DropTable(
                name: "sys_role_permission");

            migrationBuilder.DropTable(
                name: "sys_serial_no");

            migrationBuilder.DropTable(
                name: "sys_setting");

            migrationBuilder.DropTable(
                name: "sys_storage_meta");

            migrationBuilder.DropTable(
                name: "sys_user");

            migrationBuilder.DropTable(
                name: "sys_user_address");

            migrationBuilder.DropTable(
                name: "sys_user_identity");

            migrationBuilder.DropTable(
                name: "sys_user_invoice");

            migrationBuilder.DropTable(
                name: "sys_user_real_name");

            migrationBuilder.DropTable(
                name: "sys_wx_union");
        }
    }
}
