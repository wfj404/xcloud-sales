﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Gis;

namespace XCloud.Sales.Qingdao.Service;

public static class GisBizType
{
    public static string PAISHUI => "PAISHUI";
    public static string GONGSHUI => "GONGSHUI";
}

public class GetOrgByLocationInput : IEntityDto
{
    public string BizType { get; set; }

    public GeoLocationDto Location { get; set; }
}

public class GisResponse<T> : IEntityDto
{
    public string errMsg { get; set; }

    public bool success { get; set; }

    public T data { get; set; }
}

public class GisArea : IEntityDto
{
    public string areaName { get; set; }

    public string ECode { get; set; }
}

public class PipelineTypeLocation : IEntityDto
{
    public double x { get; set; }

    public double y { get; set; }
}

public class GisAuthResponse : GisResponse<object>
{
    public string authorId { get; set; }
}