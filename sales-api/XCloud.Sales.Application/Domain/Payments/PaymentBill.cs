﻿namespace XCloud.Sales.Application.Domain.Payments;

public class PaymentBill : SalesBaseEntity<string>, IHasCreationTime
{
    public string OrderId { get; set; }

    public decimal Price { get; set; }

    public string Description { get; set; }

    public string PaymentChannel { get; set; } = SalesConstants.PaymentChannel.None;

    //payment
    public bool Paid { get; set; } = false;

    public DateTime? PayTime { get; set; }

    public string PaymentTransactionId { get; set; }

    public string NotifyData { get; set; }

    public DateTime CreationTime { get; set; }
}