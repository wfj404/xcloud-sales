using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class GoodsVirtualExtensionMap : SalesEntityTypeConfiguration<GoodsVirtualExtension>
{
    public override void Configure(EntityTypeBuilder<GoodsVirtualExtension> builder)
    {
        builder.ToTable("sales_goods_virtual_extension");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.GoodsId).RequiredId();
        builder.Property(x => x.ExpiredAfterDays);

        base.Configure(builder);
    }
}