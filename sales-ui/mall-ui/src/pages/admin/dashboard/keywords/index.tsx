import { useRequest } from 'ahooks';
import { Card } from 'antd';
import * as echarts from 'echarts';
import { useEffect, useRef } from 'react';

import { apiClient } from '@/utils/client';

export default () => {
  const reportRequest = useRequest(
    apiClient.mallAdmin.reportQuerySearchKeywordsReport,
    {
      manual: true,
    },
  );

  const data = reportRequest.data?.data?.Data || [];

  const chartRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    reportRequest.run({});
  }, []);

  const chartsData = data.map((x) => ({
    value: x.Count || 0,
    name: x.Keywords || '',
    path: x.Keywords || '',
  }));

  useEffect(() => {
    if (chartRef.current == null) {
      return;
    }
    let chart = echarts.init(chartRef.current);
    chart.setOption({
      series: [
        {
          name: '关键词',
          type: 'treemap',
          visibleMin: 300,
          label: {
            show: true,
            formatter: '{b}',
          },
          itemStyle: {
            borderColor: '#fff',
          },
          levels: [
            {
              itemStyle: {
                borderWidth: 0,
                gapWidth: 5,
              },
            },
            {
              itemStyle: {
                gapWidth: 1,
              },
            },
            {
              itemStyle: {
                gapWidth: 1,
              },
            },
          ],
          data: chartsData,
        },
      ],
    });
  }, [chartsData, chartRef]);

  return (
    <Card title="搜索关键词统计" loading={reportRequest.loading}>
      <div style={{ minHeight: 500, margin: 20 }} ref={chartRef}></div>
    </Card>
  );
};
