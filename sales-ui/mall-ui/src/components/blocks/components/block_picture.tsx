import { redirectToLinkObject } from '@/utils/biz';
import { resolveUrl } from '@/utils/storage';
import { isStringEmpty } from '@/utils/utils';
import { BlockPictureProps } from '@/components/blocks/utils';
import XImage from '@/components/common/image';

export default ({ data }: { data?: BlockPictureProps }) => {
  if (!data) {
    return null;
  }

  const url = resolveUrl(data.storage, {
    width: data.width || 500,
    height: data.height,
  });

  if (!url || isStringEmpty(url)) {
    return null;
  }

  return (
    <>
      <XImage
        rate={data.rate}
        src={url}
        fit={'cover'}
        width={'100%'}
        alt={data.caption || undefined}
        onClick={() => {
          data?.link && redirectToLinkObject(data.link);
        }}
      />
    </>
  );
};
