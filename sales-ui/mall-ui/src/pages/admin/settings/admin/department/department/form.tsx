import { useRequest } from 'ahooks';
import { Button, Form, Input, message, Modal, Space, Spin } from 'antd';
import { useEffect, useState } from 'react';

import XDepartmentSelector from '@/components/manage/department_selector';
import { apiClient } from '@/utils/client';
import { DepartmentDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: DepartmentDto;
  ok: () => void;
}) => {
  const [model, _model] = useState<DepartmentDto>({});

  const save_request = useRequest(apiClient.sys.departmentSave, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        message.success('保存成功');
        ok();
      });
    },
  });

  const save = (row: DepartmentDto) => {
    row.Name = row.Name?.trim();

    if (isStringEmpty(row.Name)) {
      return;
    }

    save_request.run(row);
  };

  useEffect(() => {
    _model({
      ...(data || {}),
    });
  }, [data]);

  return (
    <>
      <Modal
        title={'部门'}
        forceRender
        destroyOnClose
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <Button
              type={'primary'}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={save_request.loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="父级部门">
              {show && (
                <XDepartmentSelector
                  value={model.ParentId || undefined}
                  onChange={(e) => {
                    _model({
                      ...model,
                      ParentId: e?.Id,
                    });
                  }}
                />
              )}
            </Form.Item>
            <Form.Item label="名称" required>
              <Input
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
              {isStringEmpty(model.Name) && (
                <p style={{ color: 'red' }}>必填</p>
              )}
            </Form.Item>
            <Form.Item label="描述">
              <Input.TextArea
                maxLength={200}
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="部门类型" style={{ display: 'none' }}>
              <Input
                maxLength={50}
                value={model.DeptType || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    DeptType: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
