using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderCouponRecordMap: SalesEntityTypeConfiguration<OrderCouponRecord>
{
    public override void Configure(EntityTypeBuilder<OrderCouponRecord> builder)
    {
        builder.ToTable("sales_order_coupon_record");
        
        builder.PreConfigStringPrimaryKey();
        
        builder.Property(x=>x.OrderId).RequiredId();
        builder.Property(x => x.UserCouponId);
        
        base.Configure(builder);
    }
}