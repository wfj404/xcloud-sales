﻿using XCloud.Sales.Application.Domain;

namespace XCloud.Sales.Qingdao.Domain.Invoices;

public class McsInvoiceRequestItem : SalesBaseEntity<string>
{
    public string InvoiceRequestId { get; set; }

    public string McsBizOrderId { get; set; }

    public decimal Price { get; set; }

    public string Description { get; set; }
}