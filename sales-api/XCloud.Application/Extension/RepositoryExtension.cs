using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;

namespace XCloud.Application.Extension;

public static class RepositoryExtension
{
    public static async Task SoftDeleteAsync<T>(this IRepository<T> repository, string id, DateTime time)
        where T : class, IEntity<string>, ISoftDelete
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await repository.FindAsync(x => x.Id == id);
        if (entity == null)
            return;

        entity.SetPropertyValue(nameof(ISoftDelete.IsDeleted), true);

        if (entity is IHasDeletionTime)
        {
            entity.SetPropertyValue(nameof(IHasDeletionTime.DeletionTime), time);
        }

        await repository.UpdateAsync(entity);
    }

    [NotNull]
    [ItemNotNull]
    public static async Task<T> GetRequiredByIdAsync<T>(this IRepository<T> repository, string id)
        where T : class, IEntity<string>
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var query = await repository.GetQueryableAsync();

        var entity = await query.FirstOrDefaultAsync(x => x.Id == id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(GetRequiredByIdAsync));

        return entity;
    }

    [NotNull]
    public static async Task<T> GetRequiredByIdAsync<T>(this IRepository<T> repository, int id)
        where T : class, IEntity<int>
    {
        if (id <= 0)
            throw new ArgumentNullException(nameof(id));

        var query = await repository.GetQueryableAsync();

        var entity = await query.FirstOrDefaultAsync(x => x.Id == id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(GetRequiredByIdAsync));

        return entity;
    }
}