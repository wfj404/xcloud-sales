﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Dept;

namespace XCloud.Platform.Application.Mapping.Dept;

public class DepartmentMappingMap : PlatformEntityTypeConfiguration<DepartmentMapping>
{
    public override void Configure(EntityTypeBuilder<DepartmentMapping> builder)
    {
        builder.ToTable("sys_dept_mapping");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.AdminId).RequiredId();
        builder.Property(x => x.DepartmentId).RequiredId();

        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}