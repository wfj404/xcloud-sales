import { apiClient } from '@/utils/client';
import { StoreManagerDto } from '@/utils/swagger';
import { Form, message, Modal, Spin, Switch } from 'antd';
import { useEffect, useState } from 'react';
import { handleResponse } from '@/utils/utils';

export default ({
                  show,
                  hide,
                  data,
                  ok,
                }: {
  show: boolean;
  hide: () => void;
  data: StoreManagerDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [model, _model] = useState<StoreManagerDto>({});

  const save = (row: StoreManagerDto) => {
    _loading(true);

    apiClient.mallAdmin
      .storeSaveStoreManager({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...(data || {}),
    });
  }, [data]);

  return (
    <>
      <Modal
        title={'store manager'}
        open={show}
        onCancel={() => hide()}
        onOk={() => save(model)}
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign='right'
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label='是否激活'>
              <Switch
                checked={model.IsActive}
                onChange={(e) => {
                  _model({
                    ...model,
                    IsActive: e,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
