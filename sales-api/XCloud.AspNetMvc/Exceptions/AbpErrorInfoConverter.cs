﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.ExceptionHandling;
using Volo.Abp.Http;
using Volo.Abp.ObjectMapping;
using XCloud.Application.Mapper;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.AspNetMvc.Exceptions;

public class AbpErrorInfoConverter : IErrorInfoConverter //, ISingleInstance
{
    public int Order => int.MinValue;

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        var exceptionToErrorInfoConverter =
            httpContext.RequestServices.GetRequiredService<IExceptionToErrorInfoConverter>();

        //get exception detail from abp system
        var remoteServiceErrorInfo = exceptionToErrorInfoConverter.Convert(e);

        if (remoteServiceErrorInfo != null)
        {
            var mapper = httpContext.RequestServices.GetRequiredService<IObjectMapper>();
            var error = remoteServiceErrorInfo.MapTo<RemoteServiceErrorInfo, ErrorInfo>(mapper);

            var exceptionStatusCodeFinder =
                httpContext.RequestServices.GetRequiredService<IHttpExceptionStatusCodeFinder>();

            error.HttpStatusCode = exceptionStatusCodeFinder.GetStatusCode(httpContext, e);

            return error;
        }

        return null;
    }
}