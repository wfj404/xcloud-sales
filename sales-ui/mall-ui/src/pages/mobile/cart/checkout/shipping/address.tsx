import { useRequest } from 'ahooks';
import { sortBy } from 'lodash-es';
import React, { useEffect, useState } from 'react';

import DotsLoading from '@/components/common/loading/dots';
import XModal from '@/components/common/modal';
import XAddressEditor from '@/pages/mobile/user/address/address_editor';
import XAddressItem from '@/pages/mobile/user/address/item';
import { DeliveryType, ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { config } from '@/utils/config';
import { PlaceOrderRequestDto, UserAddressDto } from '@/utils/swagger';
import { LocationCityOutlined } from '@mui/icons-material';
import { Button, Typography } from '@mui/material';
import Box from '@mui/material/Box';

export default function ({
  model,
  value,
  onSelectedAddress,
}: {
  model: PlaceOrderRequestDto;
  value?: string | null;
  onSelectedAddress: (d: UserAddressDto | undefined) => void;
}) {
  const getAddressListRequest = useRequest(apiClient.platform.userAddressList, {
    manual: true,
  });
  const addressList = getAddressListRequest.data?.data?.Data || [];
  const loading = getAddressListRequest.loading;

  const [show, _show] = useState(false);

  const [distance, _distance] = useState<number | undefined>(undefined);
  const currentAddress = addressList.find((x) => x.Id == value);

  useEffect(() => {
    getAddressListRequest.run();
  }, []);

  useEffect(() => {
    //calculate distance
    if (
      model.ShippingMethodId == ShippingMethod.Delivery &&
      model.DeliveryType == DeliveryType.ShortDistanceDelivery
    ) {
      if (currentAddress?.Lat && currentAddress?.Lon) {
        apiClient.mall
          .storeGetDistanceToStore({
            Lat: currentAddress?.Lat,
            Lon: currentAddress?.Lon,
          })
          .then((res) => {
            _distance(res.data.Data || undefined);
          });
      }
    } else {
      _distance(undefined);
    }
  }, [
    currentAddress?.Lat,
    currentAddress?.Lon,
    model.ShippingMethodId,
    model.DeliveryType,
  ]);

  useEffect(() => {
    if (addressList.length <= 0) {
      return;
    }
    if (!addressList.some((x) => x.Id == value)) {
      const defaultAddress = sortBy(
        addressList,
        (x) => -(x.IsDefault ? 1 : 0),
      ).at(0);
      onSelectedAddress && onSelectedAddress(defaultAddress);
    }
  }, [value, addressList]);

  const renderSelect = () => {
    if (loading) {
      return <DotsLoading />;
    }
    if (currentAddress == undefined) {
      return (
        <Button
          fullWidth
          startIcon={<LocationCityOutlined />}
          variant="outlined"
          size="large"
        >
          选择收货地址
        </Button>
      );
    }
    return (
      <XAddressItem item={currentAddress} distance={distance} hideEditButton />
    );
  };

  return (
    <Box sx={{}}>
      <XModal
        showCloseButton
        title="选择收件地址"
        open={show}
        onClose={() => {
          _show(false);
        }}
        bodyStyle={{
          backgroundColor: config.color.gray,
        }}
      >
        <XAddressEditor
          header={
            <Typography variant="body1" gutterBottom>
              选择或者修改收货地址
            </Typography>
          }
          addressList={addressList}
          onClick={(e) => {
            onSelectedAddress && onSelectedAddress(e);
            _show(false);
          }}
          onRefresh={() => {
            getAddressListRequest.run({});
          }}
          itemStyle={{
            cursor: 'pointer',
          }}
        />
      </XModal>
      <Box
        sx={{
          cursor: 'pointer',
        }}
        onClick={() => {
          _show(true);
        }}
      >
        {renderSelect()}
      </Box>
    </Box>
  );
}
