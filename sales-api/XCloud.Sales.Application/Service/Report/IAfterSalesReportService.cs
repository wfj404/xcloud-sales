using XCloud.Sales.Application.Domain.AfterSale;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Report;

public interface IAfterSalesReportService : ISalesAppService
{
    Task<TopAfterSaleMallUsersResponse[]> TopAfterSalesMallUsersAsync(TopAfterSaleMallUsersInput dto);
}

public class AfterSalesReportService : SalesAppService, IAfterSalesReportService
{
    private readonly ISalesRepository<AfterSales> _repository;

    public AfterSalesReportService(ISalesRepository<AfterSales> repository)
    {
        _repository = repository;
    }

    public async Task<TopAfterSaleMallUsersResponse[]> TopAfterSalesMallUsersAsync(TopAfterSaleMallUsersInput dto)
    {
        var db = await _repository.GetDbContextAsync();

        var query = from aftersaleItem in db.Set<AfterSalesItem>().AsNoTracking()
            join aftersale in db.Set<AfterSales>().AsNoTracking()
                on aftersaleItem.AfterSalesId equals aftersale.Id
            join order in db.Set<Order>().AsNoTracking()
                on aftersale.OrderId equals order.Id
            join order_item in db.Set<OrderItem>().AsNoTracking()
                on aftersaleItem.OrderItemId equals order_item.Id
            select new { aftersale_item = aftersaleItem, aftersale, order_item, order };

        if (dto.StartTime != null)
            query = query.Where(x => x.aftersale.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.aftersale.CreationTime <= dto.EndTime.Value);

        var groupedQuery = query.GroupBy(x => new
        {
            x.order.UserId,
        }).Select(x => new
        {
            x.Key.UserId,
            count = x.Select(d => d.aftersale.Id).Distinct().Count(),
            amount = x.Sum(d => d.order_item.TotalPrice)
        });

        var data = await groupedQuery.OrderByDescending(x => x.count).ThenByDescending(x => x.amount)
            .Take(dto.MaxCount ?? 100)
            .ToArrayAsync();

        var response = data.Select(x => new TopAfterSaleMallUsersResponse
        {
            UserId = x.UserId,
            Count = x.count,
            Amount = x.amount
        }).ToArray();

        if (response.Any())
        {
            var userids = response.Select(x => x.UserId).Distinct().ToArray();
            var users = await db.Set<StoreUser>().IgnoreQueryFilters().AsNoTracking().Where(x => userids.Contains(x.Id))
                .ToArrayAsync();
            foreach (var m in response)
            {
                var u = users.FirstOrDefault(x => x.Id == m.UserId);
                if (u == null)
                    continue;
                m.StoreUser = ObjectMapper.Map<StoreUser, StoreUserDto>(u);
            }
        }

        return response;
    }
}