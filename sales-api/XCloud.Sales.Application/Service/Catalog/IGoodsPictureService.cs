﻿using XCloud.Application.Mapper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface IGoodsPictureService : ISalesAppService
{
    Task SaveGoodsImagesAsync(string goodsId, GoodsPictureDto[] pictures);

    Task<GoodsDto[]> AttachImagesAsync(GoodsDto[] data);

    Task<SkuDto[]> AttachImagesAsync(SkuDto[] data);
}

public class GoodsPictureService : SalesAppService, IGoodsPictureService
{
    private readonly ISalesRepository<Goods> _goodsRepository;
    private readonly IStorageMetaService _storageMetaService;

    public GoodsPictureService(ISalesRepository<Goods> goodsRepository, IStorageMetaService storageMetaService)
    {
        _goodsRepository = goodsRepository;
        _storageMetaService = storageMetaService;
    }

    public async Task<GoodsPictureDto[]> AttachStorageMetaAsync(GoodsPictureDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.PictureMetaId).WhereNotEmpty().Distinct().ToArray();

        var metas = await this._storageMetaService.ByIdsAsync(ids);

        foreach (var m in data)
        {
            m.StorageMeta = metas.FirstOrDefault(x => x.Id == m.PictureMetaId);
        }

        return data;
    }

    public async Task<SkuDto[]> AttachImagesAsync(SkuDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids().Distinct().ToArray();

        var db = await _goodsRepository.GetDbContextAsync();

        var query = db.Set<GoodsPicture>().AsNoTracking();

        query = query.Where(x => ids.Contains(x.SkuId));

        var pics = await query
            .OrderBy(x => x.DisplayOrder)
            .TakeUpTo5000()
            .ToArrayAsync();

        foreach (var m in data)
        {
            var xdata = pics
                .Where(x => x.SkuId == m.Id)
                .ToArray();

            m.GoodsPictures = xdata.MapArrayTo<GoodsPicture, GoodsPictureDto>(ObjectMapper);
        }

        var pictures = data.SelectMany(x => x.GoodsPictures).ToArray();

        await this.AttachStorageMetaAsync(pictures);

        return data;
    }

    public async Task<GoodsDto[]> AttachImagesAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var goodsIds = data.Select(x => x.Id).Distinct().ToArray();

        var db = await _goodsRepository.GetDbContextAsync();

        var query = db.Set<GoodsPicture>().AsNoTracking();

        query = query.Where(x => goodsIds.Contains(x.GoodsId));

        var pics = await query
            .OrderBy(x => x.DisplayOrder)
            .TakeUpTo5000()
            .ToArrayAsync();

        foreach (var m in data)
        {
            var xdata = pics
                .Where(x => x.GoodsId == m.Id)
                .ToArray();

            m.GoodsPictures = xdata.MapArrayTo<GoodsPicture, GoodsPictureDto>(ObjectMapper);
        }

        var pictures = data.SelectMany(x => x.GoodsPictures).ToArray();

        await this.AttachStorageMetaAsync(pictures);

        return data;
    }

    public async Task SaveGoodsImagesAsync(string goodsId, GoodsPictureDto[] pictures)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            throw new ArgumentNullException(nameof(goodsId));

        if (pictures == null)
            throw new ArgumentNullException(nameof(pictures));

        foreach (var pic in pictures)
        {
            pic.GoodsId = goodsId;
        }

        var db = await _goodsRepository.GetDbContextAsync();

        var set = db.Set<GoodsPicture>();

        var currentImages = await set
            .Where(x => x.GoodsId == goodsId)
            .OrderByDescending(x => x.DisplayOrder)
            .ThenByDescending(x => x.Id)
            .ToArrayAsync();

        string FingerPrint(GoodsPicture d) => $"{d.GoodsId}.{d.PictureMetaId}";

        var toDelete = currentImages.NotInBy(pictures, FingerPrint).ToArray();
        var toAdd = pictures.NotInBy(currentImages, FingerPrint).ToArray();
        var toModified = currentImages.InBy(pictures, FingerPrint).ToArray();

        //delete
        if (toDelete.Any())
        {
            set.RemoveRange(toDelete);
        }

        //add
        if (toAdd.Any())
        {
            foreach (var m in toAdd)
            {
                m.Id = default;
            }

            set.AddRange(toAdd);
        }

        //modify
        foreach (var m in toModified)
        {
            var fp = FingerPrint(m);

            var intersect = pictures.FirstOrDefault(x => FingerPrint(x) == fp);
            if (intersect == null)
                continue;

            m.SkuId = intersect.SkuId;
            m.FileName = intersect.FileName;
            m.DisplayOrder = intersect.DisplayOrder;
        }

        await db.TrySaveChangesAsync();
    }
}