﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Volo.Abp.AspNetCore.ExceptionHandling;
using Volo.Abp.Modularity;
using XCloud.AspNetMvc.DynamicApi;
using XCloud.AspNetMvc.Exceptions;
using XCloud.AspNetMvc.Filters;
using XCloud.AspNetMvc.ModelBinder;
using XCloud.AspNetMvc.ModelValidation;

namespace XCloud.AspNetMvc.Configuration;

public static class AspNetMvcExtension
{
    public static void ConfigModelValidation(this ServiceConfigurationContext context)
    {
        context.Services.RemoveAll<IObjectModelValidator>()
            .AddSingleton<IObjectModelValidator, NullObjectModelValidator>();
    }

    public static void ConfigDynamicApi(this ServiceConfigurationContext context)
    {
        context.Services.Configure<IMvcBuilder>(builder =>
        {
            builder.PartManager.FeatureProviders.Add(new DynamicApiControllerFeatureProvider());
            builder.Services.AddTransient<IApplicationModelProvider, DynamicApiApplicationModelProvider>();
        });

        context.Services.Configure<MvcOptions>(option =>
        {
            option.Conventions.Add(new DynamicApiApplicationModelConvention());
        });
    }

    public static void ConfigureMvcOptions(this ServiceConfigurationContext context)
    {
        var env = context.Services.GetHostingEnvironment();

        context.Services.Configure<MvcOptions>(option =>
        {
            //option.ValueProviderFactories.Add(new QueryStringValueProviderFactory());
            option.ModelBinderProviders.Insert(0, new XCloudModelBinderProvider());
            option.Filters.AddService<ActionLoggingFilter>();
            option.Filters.AddService<SensitiveDataResultFilter>();

            if (env.IsStaging() || env.IsDevelopment())
            {
                option.Filters.AddService<DeprecatedActionLoggingFilter>();
            }

            option.AddExceptionHandler();
        });

        //异常捕捉
        context.Services.Configure<AbpExceptionHandlingOptions>(option =>
        {
            option.SendExceptionsDetailsToClients = env.IsDevelopment() || env.IsStaging();
#if DEBUG
            option.SendExceptionsDetailsToClients = true;
#endif
        });
    }

    public static void ConfigureMvc(this ServiceConfigurationContext context)
    {
        //context.Services.AddRouting();
        //var builder = context.Services.AddMvc();
        //context.Services.AddSingleton(builder);
    }
}