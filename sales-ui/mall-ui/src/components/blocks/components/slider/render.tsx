import { Swiper } from 'antd-mobile';

import { isArrayEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

import XPicture from '../block_picture';
import { SliderBlockProps } from './utils';

export default ({ data }: { data?: SliderBlockProps }) => {
  if (!data || !data.sliders || isArrayEmpty(data.sliders)) {
    return null;
  }

  return (
    <>
      <Swiper
        //trackOffset={10}
        //slideSize={90}
        allowTouchMove
        defaultIndex={0}
        autoplay={data.auto_play}
        autoplayInterval={
          data.auto_play_interval == undefined
            ? undefined
            : data.auto_play_interval * 1000
        }
        loop={data.loop}
        direction={data.vertical ? 'vertical' : 'horizontal'}
        indicator={data.hide_indicator ? () => null : undefined}
      >
        {data.sliders.map((x, i) => (
          <Swiper.Item key={`slider-${i}`}>
            <Box sx={{ p: 0 }}>
              <XPicture data={{ ...x, rate: 3 / 4 }} />
            </Box>
          </Swiper.Item>
        ))}
      </Swiper>
    </>
  );
};
