import { useRequest } from 'ahooks';
import { Button, Card, message, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XRenderTime from '@/components/common/time/render_time';
import XSkuSelector from '@/components/goods/sku/sku_selector';
import { apiClient } from '@/utils/client';
import { ExternalSkuGroupingDto, QueryExternalSkuInput } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XForm from './form';
import { ReloadOutlined } from '@ant-design/icons';

export default () => {
  const [query, _query] = useState<QueryExternalSkuInput>({
    Page: 1,
  });
  const query_request = useRequest(apiClient.mallAdmin.externalSkuQueryPaging, {
    manual: true,
  });
  const data = query_request.data?.data || {};

  const [show, _show] = useState(false);

  useEffect(() => {
    query_request.run({ ...query });
  }, []);

  const [loadingId, _loadingId] = useState<string | null | undefined>(
    undefined,
  );
  const set_mapping_request = useRequest(
    apiClient.mallAdmin.externalSkuSaveMapping,
    {
      manual: true,
      onSuccess: (e) => {
        handleResponse(e, () => {
          message.success('保存成功');
          query_request.run({ ...query });
        });
      },
      onFinally: () => _loadingId(undefined),
    },
  );

  const columns: ColumnType<ExternalSkuGroupingDto>[] = [
    {
      title: '唯一标识',
      render: (d, x) => <Tag>{x.CombineId}</Tag>,
    },
    {
      title: '规格名称',
      render: (d, x) => {
        const row = x.Items?.at(0);
        if (!row) {
          return <span>--</span>;
        }
        const arr = [row.GoodsName || '', row.SkuName || ''].filter(x => x.length > 0);
        return <><span>{arr.join('-')}</span></>;
      },
    },
    {
      title: '关联规格',
      render: (d, x, i) => (
        <XSkuSelector
          selectedSku={x.Sku || undefined}
          props={{
            loading: loadingId === x.CombineId,
          }}
          onChange={(e) => {
            console.log(i, e);
            _loadingId(x.CombineId);
            set_mapping_request.run({
              CombineId: x.CombineId,
              SkuId: e?.Id,
            });
          }}
        />
      ),
    },
    {
      title: '记录数量',
      render: (d, x) => <b>{x.Items?.length || 0}</b>,
    },
    {
      title: '最新更新时间',
      render: (d, x) => <XRenderTime timeStr={x.CreationTime} />,
    },
  ];

  return (
    <>
      <XForm
        show={show}
        close={() => _show(false)}
        ok={() => {
          _show(false);
          query_request.run({ ...query });
        }}
      />
      <Card
        extra={
          <Space direction={'horizontal'}>
            <Button
              icon={<ReloadOutlined />}
              size={'small'}
              loading={query_request.loading}
              type={'dashed'}
              onClick={() => {
                query_request.run({ ...query });
              }}></Button>
            <Button
              type='primary'
              size='small'
              onClick={() => {
                _show(true);
              }}
            >
              导入
            </Button>
          </Space>
        }
      >
        <Table
          columns={columns}
          dataSource={data.Items || []}
          expandable={{
            expandedRowRender: (e) => {
              return <>{JSON.stringify(e)}</>;
            },
          }}
          pagination={{
            total: data.TotalCount,
            pageSize: data.PageSize,
            current: data.PageIndex,
            onChange: (e) => {
              const q: QueryExternalSkuInput = { ...query, Page: e };
              query_request.run(q);
              _query(q);
            },
          }}
        />
      </Card>
    </>
  );
};
