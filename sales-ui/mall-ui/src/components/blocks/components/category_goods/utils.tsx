import { CategoryDto } from '@/utils/swagger';

import { BlockItem, BlockItemProps, GoodsDisplayType } from '../../utils';

export interface CategoryGoodsBlockProps extends BlockItemProps {
  category?: CategoryDto;
  count?: number;
  display_type?: GoodsDisplayType;
}

export const category_goods_demo: BlockItem<CategoryGoodsBlockProps> = {
  type: 'category-goods',
  style: {
    marginBottom: 10,
  },
  body: {
    display_type: 'grid',
    category: {
      Name: '护肤',
      Id: '1',
    },
  },
};
