﻿namespace XCloud.Sales.Application.Domain.Payments;

public class RefundBill : SalesBaseEntity<string>, IHasCreationTime
{
    public string Description { get; set; }

    public string PaymentBillId { get; set; }

    public decimal Price { get; set; }

    public bool Refunded { get; set; } = false;

    public DateTime? RefundTime { get; set; }

    public string RefundTransactionId { get; set; }

    public string RefundNotifyData { get; set; }

    public DateTime CreationTime { get; set; }
}