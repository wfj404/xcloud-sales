﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Modularity;
using XCloud.Application.Extension;
using XCloud.Sales.Application;
using XCloud.Sales.Warehouses.Mapper;

namespace XCloud.Sales.Warehouses;

[ExposeServices(typeof(SalesApplicationModule))]
public class SalesWarehouseApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.AddAutoMapperProfile<SalesWarehouseAutoMapperConfiguration>();
    }
}