﻿using System.Threading.Tasks;
using Masuit.Tools;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Service.Users;

/// <summary>
/// 用户身份证
/// </summary>
public interface IUserRealNameService : IXCloudAppService
{
    Task<UserDto[]> AttachRealNameAsync(UserDto[] data);

    Task<ApiResponse<object>> SetUserIdCardAsync(SetIdCardDto dto);

    Task<UserRealName> QueryUserIdCardAsync(string userId);
}

public class UserRealNameService : PlatformAppService, IUserRealNameService
{
    private readonly IPlatformRepository<UserRealName> _userIdCardRepository;
    private readonly AccountUtils _accountUtils;

    public UserRealNameService(IPlatformRepository<UserRealName> userIdCardRepository, AccountUtils accountUtils)
    {
        _userIdCardRepository = userIdCardRepository;
        _accountUtils = accountUtils;
    }

    public async Task<UserDto[]> AttachRealNameAsync(UserDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var datalist = await _userIdCardRepository.QueryManyAsync(x => ids.Contains(x.UserId));

        foreach (var m in data)
        {
            var realName = datalist.FirstOrDefault(x => x.UserId == m.Id);
            if (realName == null)
                continue;

            m.UserRealName = realName.MapTo<UserRealName, UserRealNameDto>(ObjectMapper);
        }

        return data;
    }

    private void CheckIdCard(string idCard)
    {
        if (string.IsNullOrWhiteSpace(idCard))
            throw new ArgumentNullException(nameof(idCard));
        if (!idCard.MatchIdentifyCard())
        {
            throw new UserFriendlyException("身份证格式错误");
        }
    }

    public async Task<UserRealName> QueryUserIdCardAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var data = await _userIdCardRepository.QueryManyAsync(x => x.UserId == userId, 500);

        var res = data.FirstOrDefault();

        return res;
    }

    public async Task<ApiResponse<object>> SetUserIdCardAsync(SetIdCardDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        CheckIdCard(dto.IdCard);

        var db = await _userIdCardRepository.GetDbContextAsync();
        var set = db.Set<UserRealName>();

        var entity = await set.OrderByDescending(x => x.CreationTime).FirstOrDefaultAsync(x => x.UserId == dto.UserId);

        if (entity == null)
        {
            entity = new UserRealName
            {
                IdCardType = (int)IdCardTypeEnum.IdCard,
                IdCardIdentity = default,
                UserId = dto.UserId,
                IdCard = dto.IdCard,
                IdCardRealName = dto.RealName,
                IdCardConfirmed = false,
                Id = GuidGenerator.CreateGuidString(),
                CreationTime = Clock.Now
            };

            entity.IdCardIdentity = _accountUtils.IdCardIdentity(entity.IdCardType, entity.IdCard);

            await db.Set<UserRealName>().AddAsync(entity);

            await db.SaveChangesAsync();
        }
        else
        {
            entity.IdCard = dto.IdCard;
            entity.IdCardRealName = dto.RealName;

            await db.SaveChangesAsync();
        }

        return new ApiResponse<object>();
    }
}