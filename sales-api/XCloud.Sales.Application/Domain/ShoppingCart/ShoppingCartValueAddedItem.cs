namespace XCloud.Sales.Application.Domain.ShoppingCart;

public class ShoppingCartValueAddedItem : SalesBaseEntity
{
    public int ShoppingCartItemId { get; set; }

    public int ValueAddedItemId { get; set; }
}