namespace XCloud.Sales.Application.Domain.Catalog;

public class GoodsPicture : SalesBaseEntity
{
    public string GoodsId { get; set; }

    public string SkuId { get; set; }

    public string PictureMetaId { get; set; }

    public string FileName { get; set; }

    public int DisplayOrder { get; set; }
}