using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Mapping.Shipping;

public class DeliveryRecordMap : SalesEntityTypeConfiguration<DeliveryRecord>
{
    public override void Configure(EntityTypeBuilder<DeliveryRecord> builder)
    {
        builder.ToTable("sales_delivery_record");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(s => s.OrderId).RequiredId();
        builder.Property(s => s.ExpressName).HasMaxLength(100);
        builder.Property(s => s.TrackingNumber).HasMaxLength(100);
        builder.Property(s => s.PreferDeliveryEndTime);
        builder.Property(s => s.PreferDeliveryStartTime);
        builder.Property(s => s.DeliveryType).IsRequired().HasMaxLength(100);

        builder.Property(x => x.Delivering);
        builder.Property(x => x.DeliveringTime);

        builder.Property(s => s.Delivered);
        builder.Property(s => s.DeliveredTime);

        base.Configure(builder);
    }
}