﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;
using XCloud.Sales.Application.Localization;

namespace XCloud.Sales.Qingdao.Localization;

public static class ConfigurationExtension
{
    public static void ConfigQingdaoSalesLocalization(this ServiceConfigurationContext context)
    {
        context.Services.Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<QingdaoSalesApplicationModule>(
                baseNamespace: typeof(QingdaoSalesApplicationModule).Namespace);
        });

        context.Services.Configure<AbpLocalizationOptions>(options =>
        {
            //try override base resource
            options.Resources.GetOrNull(typeof(SalesResource))?
                .AddVirtualJson("/Localization/Resources/Sales");

            //qingdao resource
            options.Resources
                .Add<QingdaoResource>(defaultCultureName: "zh-Hans")
                .AddVirtualJson("/Localization/Resources/Sales")
                .AddBaseTypes(typeof(SalesResource));
        });
    }
}