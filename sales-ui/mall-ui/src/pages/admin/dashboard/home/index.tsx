import { Col, Row } from 'antd';

import XCount from './count';
import XTopStores from './top_stores';
import XTopbrands from './topbrands';
import XTopCategory from './topcategory';
import XTopSeller from './topseller';
import XTopSku from './topsku';
import XTopuser from './topuser';
import XTopVisitedGoods from './topVisitedGoods';

export default () => {
  return (
    <>
      <div style={{ marginBottom: 20 }}>
        <XCount />
      </div>
      <div style={{ marginBottom: 20 }}>
        <Row gutter={10}>
          <Col span={12}>
            <XTopVisitedGoods />
          </Col>
          <Col span={12}>
            <XTopStores />
            <XTopuser />
            <XTopSeller />
            <XTopCategory />
            <XTopbrands />
            <XTopSku />
          </Col>
        </Row>
      </div>
    </>
  );
};
