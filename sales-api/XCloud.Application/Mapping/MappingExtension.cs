﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Volo.Abp;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.ValueGenerators;

namespace XCloud.Application.Mapping;

public static class MappingExtension
{
    public static void RequiredId(this PropertyBuilder<string> builder) => builder.IsRequired().HasMaxLength(100);

    public static void OptionalId(this PropertyBuilder<string> builder) => builder.HasMaxLength(100);

    public static void PricePrecision(this PropertyBuilder<decimal> builder) => builder.HasPrecision(18, 2);

    public static void PreConfigStringPrimaryKey<T>(this EntityTypeBuilder<T> builder)
        where T : class, IEntity<string>
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .IsRequired()
            .HasMaxLength(100)
            .ValueGeneratedNever()
            .HasComment("主键，最好是顺序生成的GUID");
    }

    public static void PreConfigIntPrimaryKey<T>(this EntityTypeBuilder<T> builder)
        where T : class, IEntity<int>
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id)
            .IsRequired()
            .ValueGeneratedOnAdd();
    }

    public static void PreConfigAbpSoftDeleteEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, ISoftDelete
    {
        builder.Property(x => x.IsDeleted).IsRequired().HasDefaultValue(false).HasComment("是否删除");

        builder.HasIndex(x => x.IsDeleted);

        builder.HasQueryFilter(x => x.IsDeleted == false);
    }

    public static void PreConfigAbpCreationTimeEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, IHasCreationTime
    {
        builder.Property(x => x.CreationTime)
            .HasValueGenerator<UtcTimeGenerator>()
            .ValueGeneratedOnAdd()
            .HasComment("创建时间");

        builder.HasIndex(x => x.CreationTime);
    }

    public static void PreConfigAbpSoftDeletionTimeEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, IHasDeletionTime
    {
        builder.Property(x => x.DeletionTime).HasComment("删除时间");
    }

    public static void PreConfigTreeEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, IHasParent
    {
        builder.Property(x => x.ParentId)
            .HasMaxLength(100)
            .HasDefaultValue(string.Empty)
            .HasComment("树的父级节点id");
    }

    public static void PreConfigModificationTimeEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, IHasModificationTime
    {
        builder.Property(x => x.LastModificationTime)
            .HasComment("更新时间，同时也是乐观锁");

        builder.HasIndex(x => x.LastModificationTime);
    }
}