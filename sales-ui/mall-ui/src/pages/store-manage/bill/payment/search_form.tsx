import { Button, Card, Col, DatePicker, Form, Input, Row, Select } from 'antd';
import { useEffect, useState } from 'react';

import { PaymentChannelList } from '@/utils/biz';
import { dateTimeFormatWithSecond, parseAsDayjs, timezoneOffset } from '@/utils/dayjs';
import { QueryOrderBillPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';

const App = ({
  query,
  onSearch,
}: {
  query: QueryOrderBillPagingInput;
  onSearch: (d: QueryOrderBillPagingInput) => void;
}) => {
  const [model, _model] = useState<QueryOrderBillPagingInput>({});

  const triggerSearch = (e: QueryOrderBillPagingInput) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _model({
      ...(query || {}),
    });
  }, [query]);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form onFinish={() => triggerSearch(model)} autoComplete="off">
        <Row gutter={10}>
          <Col span={8}>
            <Form.Item label="订单ID">
              <Input
                value={model.OrderId || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    OrderId: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="订单号">
              <Input
                value={model.OrderNo || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    OrderNo: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={8}></Col>
        </Row>
        <Row gutter={10}>
          <Col span={8}>
            <Form.Item label="支付渠道">
              <Select
                allowClear
                options={PaymentChannelList.map((x) => ({
                  label: x.name,
                  value: x.id,
                }))}
                value={model.PaymentMethod}
                onChange={(e) => {
                  _model({
                    ...model,
                    PaymentMethod: e,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="支付系统流水号">
              <Input
                value={model.PaymentTransactionId || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    PaymentTransactionId: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="时间">
              <DatePicker.RangePicker
                value={[
                  parseAsDayjs(model.StartTime)?.add(timezoneOffset, 'hour') ||
                    null,
                  parseAsDayjs(model.EndTime)?.add(timezoneOffset, 'hour') ||
                    null,
                ]}
                onChange={(e) => {
                  _model({
                    ...model,
                    StartTime: e
                      ?.at(0)
                      ?.add(-timezoneOffset, 'hour')
                      .format(dateTimeFormatWithSecond),
                    EndTime: e
                      ?.at(1)
                      ?.add(-timezoneOffset, 'hour')
                      .format(dateTimeFormatWithSecond),
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="退款单号">
              <Input
                value={model.OutRefundNo || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    OutRefundNo: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="退款流水号">
              <Input
                value={model.RefundId || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    RefundId: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={10}>
          <Col span={8}>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                icon={<SearchOutlined />}
              >
                搜索
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};

export default App;
