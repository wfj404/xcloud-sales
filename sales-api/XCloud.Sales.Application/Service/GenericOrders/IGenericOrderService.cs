﻿using JetBrains.Annotations;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Json;
using XCloud.Sales.Application.Domain.GenericOrders;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.GenericOrders;

public interface
    IGenericOrderService : ISalesStringCrudAppService<GenericOrder, GenericOrderDto, QueryGenericOrderInput>
{
    [NotNull]
    [ItemNotNull]
    Task<GenericOrder> GetOrCreateAsync(int userId, string orderType, string orderId);

    Task<GenericOrder> FindOrderAsync(string orderType, string orderId);

    Task<GenericOrderDto[]> AttachItemsAsync(GenericOrderDto[] data);

    Task<GenericOrderDto[]> AttachStoreUsersAsync(GenericOrderDto[] data);

    Task<GenericOrderDto[]> AttachXCloudSalesOrdersAsync(GenericOrderDto[] datalist);
}

public class GenericOrderService : SalesStringCrudAppService<GenericOrder, GenericOrderDto, QueryGenericOrderInput>,
    IGenericOrderService
{
    private readonly IOrderService _orderService;
    private readonly IStoreUserService _storeUserService;

    public GenericOrderService(ISalesRepository<GenericOrder> repository,
        IOrderService orderService, IStoreUserService storeUserService) : base(repository)
    {
        _orderService = orderService;
        _storeUserService = storeUserService;
        //
    }

    public async Task<GenericOrderDto[]> AttachXCloudSalesOrdersAsync(GenericOrderDto[] datalist)
    {
        var typeName = this.CommonUtils.GetRequiredTypeIdentityName<Order>();

        var data = datalist.Where(x => x.OrderType == typeName).ToArray();

        if (!data.Any())
            return data;

        var ids = data.Select(x => x.OrderId).WhereNotEmpty().Distinct().ToArray();

        var orders = await this._orderService.GetByIdsAsync(ids);

        foreach (var m in data)
        {
            m.Order = orders.FirstOrDefault(x => x.Id == m.OrderId);
        }

        return data;
    }

    public async Task<GenericOrderDto[]> AttachStoreUsersAsync(GenericOrderDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.StoreUserId).Where(x => x > 0).ToArray();

        var users = await this._storeUserService.GetByIdsAsync(ids);

        foreach (var m in data)
        {
            m.StoreUser = users.FirstOrDefault(x => x.Id == m.StoreUserId);
        }

        return data;
    }

    public async Task<GenericOrderDto[]> AttachItemsAsync(GenericOrderDto[] data)
    {
        if (!data.Any())
            return data;

        foreach (var m in data)
        {
            m.Items = this.JsonDataSerializer.DeserializeArrayFromStringOrEmpty<GenericOrderItemDto>(m.ItemsJson,
                this.Logger);
        }

        await Task.CompletedTask;

        return data;
    }

    public override Task<GenericOrder> InsertAsync(GenericOrderDto dto)
    {
        throw new NotSupportedException();
    }

    protected override async Task SetFieldsBeforeUpdateAsync(GenericOrder entity, GenericOrderDto dto)
    {
        entity.Paid = dto.Paid;
        entity.Serviced = dto.Serviced;
        entity.ToService = dto.ToService;
        entity.Reviewed = dto.Reviewed;

        entity.Finished = dto.Finished;
        entity.FinishedTime = dto.FinishedTime;

        entity.Closed = dto.Closed;
        entity.ClosedTime = dto.ClosedTime;

        entity.ItemsJson = dto.ItemsJson;
        entity.ExtraDataJson = dto.ItemsJson;

        entity.LastModificationTime = this.Clock.Now;

        await Task.CompletedTask;
    }

    public override async Task<GenericOrder> UpdateAsync(GenericOrderDto dto)
    {
        return await base.UpdateAsync(dto);
    }

    public Task<GenericOrder> FindOrderAsync(string orderType, string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderType))
            throw new ArgumentNullException(nameof(orderType));

        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        return this.Repository.FindAsync(x => x.OrderType == orderType && x.OrderId == orderId);
    }

    public async Task<GenericOrder> GetOrCreateAsync(int userId, string orderType, string orderId)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        var entity = await FindOrderAsync(orderType, orderId);

        if (entity == null)
        {
            var key = $"{nameof(GetOrCreateAsync)}.{userId}.{orderType}.{orderId}";

            await using var _ = await
                this.DistributedLockProvider.CreateRequiredLockAsync(resource: key,
                    expiryTime: TimeSpan.FromSeconds(5));

            entity = await FindOrderAsync(orderType, orderId);

            if (entity == null)
            {
                entity = new GenericOrder()
                {
                    Id = this.GuidGenerator.CreateGuidString(),
                    StoreUserId = userId,
                    OrderType = orderType,
                    OrderId = orderId,
                    CreationTime = this.Clock.Now
                };

                await this.Repository.InsertAsync(entity);

                await RequiredCurrentUnitOfWork.SaveChangesAsync();
            }
        }

        if (entity.StoreUserId != userId)
            throw new BusinessException(message: "you are not order owner");

        return entity;
    }

    public override async Task<PagedResponse<GenericOrderDto>> QueryPagingAsync(QueryGenericOrderInput dto)
    {
        var db = await this.Repository.GetDbContextAsync();

        var query = db.Set<GenericOrder>().AsNoTracking();

        if (dto.UserId > 0)
            query = query.Where(x => x.StoreUserId == dto.UserId);

        if (dto.Paid != null)
            query = query.Where(x => x.Paid == dto.Paid.Value);

        if (dto.ToService != null)
            query = query.Where(x => x.ToService == dto.ToService.Value);

        if (dto.Serviced != null)
            query = query.Where(x => x.Serviced == dto.Serviced.Value);

        if (dto.Reviewed != null)
            query = query.Where(x => x.Reviewed == dto.Reviewed.Value);

        if (dto.Finished != null)
            query = query.Where(x => x.Finished == dto.Finished.Value);

        if (dto.Closed != null)
            query = query.Where(x => x.Closed == dto.Closed.Value);

        var count = await query.CountOrDefaultAsync(dto);

        var items = await query
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var datalist = items.MapArrayTo<GenericOrder, GenericOrderDto>(ObjectMapper);

        return new PagedResponse<GenericOrderDto>(datalist, dto, count);
    }
}