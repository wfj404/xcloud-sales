import 'leaflet/dist/leaflet.css';

import { usePrevious } from 'ahooks';
import { Space } from 'antd-mobile';
import L from 'leaflet';
import { useEffect, useRef, useState } from 'react';
import { Circle, MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';

import { isStringEmpty } from '@/utils/utils';
import { RedoOutlined } from '@mui/icons-material';
import { Box, IconButton, Tooltip } from '@mui/material';

import MyMapEventListener from './map_event_listener';
import { marker_icon, tile_server, wuxi_default_location } from './utils';

export default ({
  marker_position,
  onMarkerPositionChange,
  marker_title,
  use_map_center,
  use_click_position,
}: {
  marker_position?: L.LatLngLiteral;
  onMarkerPositionChange?: (e: L.LatLngLiteral) => void;
  marker_title?: string;
  use_map_center?: boolean;
  use_click_position?: boolean;
}) => {
  const map = useRef<L.Map | null>(null);

  const [shadow_point, _shadow_point] = useState<L.LatLngLiteral | undefined>(
    undefined,
  );

  const final_marker_position = marker_position || wuxi_default_location;

  const previous_location = usePrevious(final_marker_position);

  useEffect(() => {
    console.log('maker-position', marker_position);
  }, [marker_position]);

  useEffect(() => {
    map.current?.setView(final_marker_position);
  }, [final_marker_position, map.current]);

  const setMarkerPosition = (e: L.LatLngLiteral) => {
    onMarkerPositionChange && onMarkerPositionChange(e);
  };

  const renderHeader = () => {
    return (
      <>
        <Box
          sx={{
            backgroundColor: 'rgb(240,240,240)',
            px: 1,
            py: 0.3,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}
        >
          <Space direction="horizontal">
            <Tooltip title="撤销修改">
              <IconButton
                size="small"
                disabled={previous_location == undefined}
                onClick={() => {
                  previous_location && setMarkerPosition(previous_location);
                }}
              >
                <RedoOutlined fontSize="inherit" />
              </IconButton>
            </Tooltip>
          </Space>
        </Box>
      </>
    );
  };

  return (
    <>
      <div
        style={{
          height: '100%',
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
        }}
      >
        {renderHeader()}
        <Box flexGrow={1}>
          <MapContainer
            ref={map}
            fadeAnimation
            markerZoomAnimation
            zoomAnimation
            attributionControl={false}
            center={final_marker_position}
            zoom={13}
            style={{ height: '100%', width: '100%' }}
            whenReady={() => {
              setTimeout(() => {
                //map.current?.fireEvent('resize');
                //map.current?.invalidateSize(true);
                window.dispatchEvent(new Event('resize'));
              }, 3000);
            }}
          >
            <MyMapEventListener
              events={{
                click: (e) => {
                  use_click_position && setMarkerPosition(e.latlng);
                },
                dragstart: (e) => {},
                drag: (e) => {
                  use_map_center && _shadow_point(map.current?.getCenter());
                },
                dragend: (e) => {
                  const latlng = map.current?.getCenter();
                  use_map_center && latlng && setMarkerPosition(latlng);
                  _shadow_point(undefined);
                },
              }}
            />
            <TileLayer url={tile_server} attribution={''} />
            <Circle
              center={final_marker_position}
              radius={10}
              fillColor="blue"
              fillOpacity={0.3}
              pathOptions={{
                color: 'red',
              }}
            />
            <Marker
              draggable={!use_map_center}
              position={final_marker_position}
              icon={marker_icon}
              eventHandlers={{
                drag: (e) => {
                  if (use_map_center) {
                    //
                  } else {
                    const latlng = (e.target as L.Marker).getLatLng();
                    _shadow_point(latlng);
                  }
                },
                dragend: (e) => {
                  const latlng = (e.target as L.Marker).getLatLng();
                  console.log('drag end:', e, latlng);
                  setMarkerPosition(latlng);
                  _shadow_point(undefined);
                },
              }}
            >
              {isStringEmpty(marker_title) || <Popup>{marker_title}</Popup>}
            </Marker>
            {shadow_point != undefined && (
              <Circle
                center={shadow_point}
                radius={10}
                fillColor="blue"
                fillOpacity={0.3}
                pathOptions={{
                  color: 'red',
                }}
              />
            )}
          </MapContainer>
        </Box>
      </div>
    </>
  );
};
