﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;
using Serilog.Settings.Configuration;
using Volo.Abp.Modularity;
using XCloud.Core.Configuration;
using XCloud.Core.Extension;

namespace XCloud.Application.Logging;

public static class SerilogConfigurationExtension
{
    private static string OutputTemplate =>
        "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}";

    public static readonly IReadOnlyCollection<LogEventLevel> ErrorLevelType = new[]
    {
        LogEventLevel.Error,
        LogEventLevel.Fatal
    };

    public static LoggerConfiguration CreateSerilogConfig(this IConfiguration config, string appName)
    {
        return new LoggerConfiguration()
            //https://github.com/serilog/serilog-settings-configuration
            .ReadFrom.Configuration(config, new ConfigurationReaderOptions { SectionName = "Serilog" })
            .Enrich.WithProperty("app_name", appName)
            .Enrich.With(new SerilogEnRicher());
    }

    private static LoggerConfiguration RollingFile(this LoggerSinkConfiguration config, string logFilePath)
    {
        if (string.IsNullOrEmpty(logFilePath))
            throw new ArgumentNullException(paramName: nameof(logFilePath));

        return config.File(
            outputTemplate: OutputTemplate,
            path: logFilePath,
            //formatter: textFormatter ?? new SerilogJsonFormatter(),
            flushToDiskInterval: TimeSpan.FromSeconds(5),
            fileSizeLimitBytes: 1024 * 2014 * 100,
            retainedFileCountLimit: 31,
            rollOnFileSizeLimit: true,
            rollingInterval: RollingInterval.Day);
    }

    private static Serilog.ILogger CreateStartupFileLogger(this LoggerConfiguration config)
    {
        var logDir = Path.Combine(Directory.GetCurrentDirectory(), "logs");

        new DirectoryInfo(logDir).CreateIfNotExist();

        var logFile = Path.Combine(logDir, "startup.log");

        var logger = config.WriteTo.RollingFile(logFile).CreateLogger();

        return logger;
    }

    public static string ToLogLevelString(this LogEventLevel level)
    {
        var res = level switch
        {
            LogEventLevel.Debug => nameof(LogEventLevel.Debug),
            LogEventLevel.Error => nameof(LogEventLevel.Error),
            LogEventLevel.Fatal => nameof(LogEventLevel.Fatal),
            LogEventLevel.Information => nameof(LogEventLevel.Information),
            LogEventLevel.Verbose => nameof(LogEventLevel.Verbose),
            LogEventLevel.Warning => nameof(LogEventLevel.Warning),
            _ => Enum.GetName(level) ?? string.Empty,
        };
        return res;
    }

    public static void AddStartupLoggerProvider(this ILoggingBuilder builder)
    {
        builder.AddSerilog(new LoggerConfiguration().WriteTo.Console(outputTemplate: OutputTemplate).CreateLogger());
        builder.AddSerilog(new LoggerConfiguration().WriteTo.Debug(outputTemplate: OutputTemplate).CreateLogger());
        builder.AddSerilog(new LoggerConfiguration().CreateStartupFileLogger());
    }

    public static void AddSerilogProvider(this ILoggingBuilder loggingBuilder, ServiceConfigurationContext context)
    {
        var hostEnv = context.Services.GetHostingEnvironment();
        var config = context.Services.GetConfiguration();
        var option = config.GetLoggingOptionOrDefault();

        if (hostEnv.IsDevelopment() || hostEnv.IsStaging() || option.PrintLog)
        {
            loggingBuilder.AddSerilog(new LoggerConfiguration().WriteTo.Console(outputTemplate: OutputTemplate)
                .CreateLogger());
        }

#if DEBUG
        loggingBuilder.AddSerilog(
            new LoggerConfiguration().WriteTo.Debug(outputTemplate: OutputTemplate).CreateLogger());
#endif

        loggingBuilder.AddFileProvider(context);
        //try send to es
        loggingBuilder.TryAddElasticSearchProvider(context);
        //if you are not assigning the logger to Log.Logger, then you need to add your logger here.
    }

    private static void AddFileProvider(this ILoggingBuilder builder, ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();
        var appName = context.Services.GetAppName();
        var savePath = context.GetOrCreateLoggingPath();
        //https://github.com/serilog/serilog-sinks-file

        var logPath = Path.Combine(savePath, $"{appName}-all.log");

        var infoLogger = config.CreateSerilogConfig(appName)
            .Filter.ByIncludingOnly(x => x.Level >= LogEventLevel.Information)
            .WriteTo.RollingFile(logPath)
            .CreateLogger();

        builder.AddSerilog(infoLogger);
    }
}