using AutoMapper;
using XCloud.Platform.Messenger.Domain.Messenger;
using XCloud.Platform.Messenger.Service;

namespace XCloud.Platform.Messenger.Mapper;

public class MessengerAutoMapperConfiguration : Profile
{
    public MessengerAutoMapperConfiguration()
    {
        CreateMap<ServerInstance, ServerInstanceDto>().ReverseMap();
        CreateMap<OnlineStatus, OnlineStatusDto>().ReverseMap();
    }
}