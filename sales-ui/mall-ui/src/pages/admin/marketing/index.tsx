import { useOutlet } from 'umi';

import ValidatePermission from '@/components/login/admin/validate_permission';
import HideFor3H from '@/layouts/sh3h/hide';
import { AdminPermissionKeys } from '@/utils/biz';

export default () => {
  const children = useOutlet();

  return (
    <>
      <HideFor3H>
        <ValidatePermission permissionKey={AdminPermissionKeys.Market}>
          {children}
        </ValidatePermission>
      </HideFor3H>
    </>
  );
};
