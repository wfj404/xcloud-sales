import { useRequest } from 'ahooks';
import { Card } from 'antd';
import * as echarts from 'echarts';
import { useEffect, useRef } from 'react';

import { apiClient } from '@/utils/client';
import { dateFormat, parseAsDayjs } from '@/utils/dayjs';
import { OrderSumByDateResponse } from '@/utils/swagger';

interface OrderSumByDate extends OrderSumByDateResponse {
  formatedDate: string;
}

export default () => {
  const reportRequest = useRequest(apiClient.mallAdmin.reportOrderSumByDate, {
    manual: true,
  });

  const data =
    reportRequest.data?.data?.Data?.map<OrderSumByDate>((x) => ({
      ...x,
      formatedDate: parseAsDayjs(x.Date)?.format(dateFormat) || x.Date || '--',
    })) || [];

  const chart1Ref = useRef<HTMLDivElement>(null);
  const chart2Ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    reportRequest.run({});
  }, []);

  useEffect(() => {
    if (chart1Ref.current == null) {
      return;
    }
    let chart = echarts.init(chart1Ref.current);
    chart.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
      },
      xAxis: [
        {
          type: 'category',
          data: data.map((x) => x.formatedDate),
          axisTick: {
            alignWithLabel: true,
          },
        },
      ],
      yAxis: [
        {
          type: 'value',
        },
      ],
      series: [
        {
          name: '订单总数',
          type: 'bar',
          barWidth: '60%',
          data: data.map((x) => x.Total),
        },
      ],
    });
  }, [data]);

  useEffect(() => {
    if (chart2Ref.current == null) {
      return;
    }
    let chart = echarts.init(chart2Ref.current);
    chart.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
      },
      xAxis: [
        {
          type: 'category',
          data: data.map((x) => x.formatedDate),
          axisTick: {
            alignWithLabel: true,
          },
        },
      ],
      yAxis: [
        {
          type: 'value',
        },
      ],
      series: [
        {
          name: '销售金额💰',
          type: 'bar',
          barWidth: '60%',
          data: data.map((x) => x.Amount),
        },
      ],
    });
  }, [data]);

  return (
    <>
      <Card
        title="订单数量统计"
        style={{ marginBottom: 10 }}
        loading={reportRequest.loading}
      >
        <div style={{ minHeight: 400 }} ref={chart1Ref}></div>
      </Card>

      <Card
        title="订单金额统计"
        style={{ marginBottom: 10 }}
        loading={reportRequest.loading}
      >
        <div style={{ minHeight: 400 }} ref={chart2Ref}></div>
      </Card>
    </>
  );
};
