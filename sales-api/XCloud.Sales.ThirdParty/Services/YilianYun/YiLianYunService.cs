﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;
using Qc.YilianyunSdk;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Mapper;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.ThirdParty.Services.YiLianYun;

[ExposeServices(typeof(YiLianYunService))]
public class YiLianYunService : SalesAppService
{
    private readonly IStoreSettingService _storeSettingService;
    private readonly YiLianServiceFactory _yiLianServiceFactory;
    private readonly IExternalAccessTokenService _externalAccessTokenService;

    public YiLianYunService(IStoreSettingService storeSettingService,
        YiLianServiceFactory yiLianServiceFactory,
        IExternalAccessTokenService externalAccessTokenService)
    {
        _storeSettingService = storeSettingService;
        _yiLianServiceFactory = yiLianServiceFactory;
        _externalAccessTokenService = externalAccessTokenService;
    }

    public async Task<string> GetOrCreateAccessTokenAsync([NotNull] YiLianYunConfigDto settings,
        [NotNull] string machineCode)
    {
        if (!settings.Available)
        {
            return null;
        }

        var client = await this._yiLianServiceFactory.CreateYiLianYunServiceAsync(settings);

        var device = settings.Devices.FirstOrDefault(x => x.MachineCode == machineCode);

        device = device ?? throw new ArgumentException(message: "device machine code not found");

        var appId = $"{device.MachineCode}@{settings.ClientId}";

        var token = await this._externalAccessTokenService.GetValidClientAccessTokenAsync(
            new GetValidClientAccessTokenDto()
            {
                Platform = YiLianYunConstant.PlatformId,
                AppId = appId
            });

        if (token == null || token.ExpiredAt <= this.Clock.Now.AddHours(24))
        {
            var response = client.AuthTerminal(
                machine_code: device.MachineCode,
                device.MSign,
                phone: device.Phone,
                print_name: device.PrinterName, skipSave: false);

            if (!response.IsSuccess())
            {
                this.Logger.LogWarning(message: this.JsonDataSerializer.SerializeToString(response));
                throw new UserFriendlyException(message: "failed to get access token");
            }

            var model = response.Body!;

            token = new ExternalAccessToken()
            {
                Platform = YiLianYunConstant.PlatformId,
                AppId = appId,
                UserId = string.Empty,
                Scope = string.Empty,
                AccessToken = model.Access_Token,
                RefreshToken = model.Refresh_Token,
                ExpiredAt = this.Clock.Now.AddSeconds(model.Expires_In ?? TimeSpan.FromDays(3).TotalSeconds),
                DataJson = this.JsonDataSerializer.SerializeToString(model),
                AccessTokenType = (int)AccessTokenTypeEnum.Client
            };

            var dto = token.MapTo<ExternalAccessToken, UserExternalAccessTokenDto>(this.ObjectMapper);

            await this._externalAccessTokenService.InsertAccessTokenAsync(dto);
        }

        return token.AccessToken;
    }

    public async Task<string> GetOrCreateAccessTokenAsync([NotNull] string storeId, [NotNull] string machineCode)
    {
        var settings = await this._storeSettingService.GetSettingsAsync<YiLianYunConfigDto>(storeId);

        return await this.GetOrCreateAccessTokenAsync(settings, machineCode);
    }
}