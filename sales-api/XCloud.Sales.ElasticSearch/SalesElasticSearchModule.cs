using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Extension;
using XCloud.Elasticsearch;
using XCloud.Sales.Application;
using XCloud.Sales.ElasticSearch.Configuration;
using XCloud.Sales.ElasticSearch.Mapper;

namespace XCloud.Sales.ElasticSearch;

[DependsOn(typeof(ElasticSearchModule), typeof(SalesApplicationModule))]
public class SalesElasticSearchModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.ConfigEsHealthCheck();

        context.AddAutoMapperProfile<SalesElasticSearchMapperConfiguration>();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //context.Services.RemoveAll<IGoodsSearchService>();
        //context.Services.AddTransient<IGoodsSearchService, EsGoodsSearchService>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();
        using var s = app.ApplicationServices.CreateScope();
        s.ServiceProvider.ConfigElasticSearchJobs();
    }
}