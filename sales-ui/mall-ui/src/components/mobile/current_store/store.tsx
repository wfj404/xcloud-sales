import React, { useEffect, useState } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isStringEmpty, setSelectedStoreId } from '@/utils/utils';
import { LocationOnOutlined } from '@mui/icons-material';
import { Chip, Tooltip } from '@mui/material';

import StoreSelector from './select_store_modal';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  const [loading, _loading] = useState(true);
  const [show, _show] = useState(false);
  const hasStore = !isStringEmpty(app.currentStore?.Id);

  useEffect(() => {
    if (
      !app.mostNearbyStore?.Id ||
      isStringEmpty(app.mostNearbyStore?.Id) ||
      !hasStore
    ) {
      return;
    }

    if (app.mostNearbyStore.Id != app.currentStore?.Id) {
      if (confirm(`是否切换到附近的门店：[${app.mostNearbyStore.Id}]`)) {
        setSelectedStoreId(app.mostNearbyStore.Id || '');
        setTimeout(() => {
          window.location.reload();
        }, 500);
      } else {
        app.setMostNearbyStore(undefined);
      }
    }
  }, [app.mostNearbyStore, app.currentStore]);

  const queryStore = () => {
    _loading(true);
    app.queryCurrentStore().finally(() => {
      _loading(false);
    });
  };

  useEffect(() => {
    queryStore();
  }, []);

  useEffect(() => {
    if (app.currentStore && app.currentStore.Id) {
      if (!app.currentStore.Opening) {
        if (
          confirm(
            `当前门店[${app.currentStore.Name || '--'}]已经闭店，是否更换门店`,
          )
        ) {
          _show(true);
        }
      }
    }
  }, [app.currentStore]);

  if (loading) {
    return <span>...</span>;
  }

  if (!hasStore) {
    return <span>无门店信息</span>;
  }

  return (
    <>
      <StoreSelector
        show={show}
        close={() => {
          _show(false);
        }}
      />
      <Tooltip title={'选择门店'}>
        <Chip
          size={'small'}
          variant={'filled'}
          color={'default'}
          icon={<LocationOnOutlined fontSize="inherit" />}
          label={app.currentStore?.Name || '--'}
          onClick={() => {
            _show(true);
          }}
          sx={{
            '&:hover': {
              //border: (t) => `1px solid ${t.palette.primary.main}`,
            },
          }}
        />
      </Tooltip>
    </>
  );
};
