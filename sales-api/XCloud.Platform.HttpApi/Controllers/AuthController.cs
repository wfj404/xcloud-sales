﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp;
using XCloud.Application.Model;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Authentication.Authorization;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/auth")]
public class AuthController : PlatformBaseController
{
    private readonly IAuthenticationSchemeProvider _authenticationSchemeProvider;
    private readonly ITokenService _tokenService;
    private readonly IUserAccountService _userAccountService;
    private readonly IUserLoginGuardService _userLoginGuardService;

    public AuthController(IAuthenticationSchemeProvider authenticationSchemeProvider,
        ITokenService tokenService,
        IUserAccountService userAccountService,
        IUserLoginGuardService userLoginGuardService)
    {
        _authenticationSchemeProvider = authenticationSchemeProvider;
        _tokenService = tokenService;
        _userAccountService = userAccountService;
        _userLoginGuardService = userLoginGuardService;
    }

    [HttpPost("auth")]
    public async Task<UserAuthResult> UserAuthAsync()
    {
        return await UserAuthService.GetAuthUserAsync();
    }

    /// <summary>
    /// 密码授权模式
    /// </summary>
    [HttpPost("password-login")]
    public async Task<ApiResponse<AuthTokenDto>> PasswordLoginAsync([FromBody] PasswordLoginDto model)
    {
        if (model == null || string.IsNullOrWhiteSpace(model.IdentityName) || string.IsNullOrWhiteSpace(model.Password))
            throw new ArgumentNullException(nameof(model));

        var failureRecords =
            await this._userLoginGuardService.ListUserRecentLoginFailureRecordsAsync(model.IdentityName);

        if (failureRecords.Length > 5)
            throw new UserFriendlyException($"尝试次数过多，请稍后再试");

        var response = await _userAccountService.PasswordLoginAsync(model);

        var user = response.Data;

        if (response.HasError() || user == null)
        {
            await this._userLoginGuardService.AddLoginFailureRecordsAsync(model.IdentityName);
            return new ApiResponse<AuthTokenDto>().SetError(response.Error?.Message);
        }

        //refresh user auth validation data
        await this._userAccountService.GetUserAuthValidationDataAsync(user.Id, new CacheStrategy()
        {
            Refresh = true
        });

        var identity = new ClaimsIdentity()
            .SetSubjectId(user.Id)
            .SetCreationTime(Clock.Now);

        var token = await _tokenService.CreateTokenAsync(identity.Claims.ToArray());

        return new ApiResponse<AuthTokenDto>(new AuthTokenDto
        {
            AccessToken = token
        });
    }

    [HttpPost("test-jwt-service")]
    public async Task<string> TestJwtServiceAsync()
    {
        var sub = "test";
        var identity = new ClaimsIdentity().SetSubjectId(sub);

        var token = await _tokenService.CreateTokenAsync(identity.Claims.ToArray());

        var principal = await _tokenService.ParseTokenAsync(token);

        if (principal.Identity == null || !principal.Identity.IsAuthenticated)
        {
            return "auth failed";
        }

        if (principal.Claims.GetSubjectId() != sub)
        {
            return "sub error";
        }

        return "ok";
    }

    [Obsolete]
    [HttpPost(nameof(authorization_test))]
    public async Task<string> authorization_test([FromServices] IAuthorizationService authorizationService)
    {
        var res = await authorizationService.AuthorizeAsync(null, new AdminPermissionRequirement
        {
            Permissions = new[] { "delete-user" }
        });
        return "123";
    }
}