﻿using System;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Authentication;

[DisableConventionalRegistration]
public class QingdaoStoreManagerPermissionService : SalesAppService, IStoreManagerPermissionService
{
    private readonly IStoreManagerService _storeManagerService;
    private readonly Sh3HClient _sh3HClient;

    public QingdaoStoreManagerPermissionService(IStoreManagerService storeManagerService, Sh3HClient sh3HClient)
    {
        _storeManagerService = storeManagerService;
        _sh3HClient = sh3HClient;
    }

    private async Task<StoreManagerGrantedPermissionResponse> GetGrantedPermissionsAsync(string managerId)
    {
        var response = new StoreManagerGrantedPermissionResponse();

        var manager = await this._storeManagerService.GetByIdAsync(managerId);

        if (manager == null)
            return response;

        response.SetData(manager);

        await this._storeManagerService.AttachUsersAsync(new[] { manager });

        if (manager.User != null)
        {
            response.Permissions = await this._sh3HClient.GetPermissionsAsync(manager.User.ExternalId);
        }

        return response;
    }

    public async Task<StoreManagerGrantedPermissionResponse> GetGrantedPermissionsAsync(string managerId,
        CacheStrategy cacheStrategy)
    {
        if (string.IsNullOrWhiteSpace(managerId))
            throw new ArgumentNullException(nameof(managerId));

        var option =
            new CacheOption<StoreManagerGrantedPermissionResponse>(
                key: $"3h.store.manager.permission.by.{managerId}",
                expiration: TimeSpan.FromSeconds(30));

        var result = await this.CacheProvider.ExecuteWithPolicyAsync(
            () => GetGrantedPermissionsAsync(managerId),
            option, cacheStrategy);

        return result ?? new StoreManagerGrantedPermissionResponse();
    }
}