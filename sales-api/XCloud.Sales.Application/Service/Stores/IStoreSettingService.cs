using JetBrains.Annotations;
using Microsoft.Extensions.Caching.Memory;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.Application.Utils;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreSettingService : ISalesAppService
{
    Task SaveSettingsAsync<T>(string storeId, T dto)
        where T : class, ISettings, new();

    [NotNull]
    Task<T> GetSettingsAsync<T>(string storeId)
        where T : class, ISettings, new();
    
    [NotNull]
    Task<T> GetSettingsAsync<T>(string storeId, CacheStrategy cacheStrategy)
        where T : class, ISettings, new();
}

public class StoreSettingService : SalesAppService, IStoreSettingService
{
    private readonly IKeyValueService _keyValueService;
    private readonly SettingUtils _settingUtils;

    public StoreSettingService(IKeyValueService keyValueService, SettingUtils settingUtils)
    {
        _keyValueService = keyValueService;
        _settingUtils = settingUtils;
    }

    private string CacheKey(string name)
    {
        var key = $"sales.settings.dto.by.name={name}";
        return key;
    }

    public async Task SaveSettingsAsync<T>(string storeId, T dto)
        where T : class, ISettings, new()
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var key = _settingUtils.GetSettingsRequiredTypeIdentityName<T>(storeId);

        await _keyValueService.SaveObjectAsync(key, dto);

        await GetSettingsAsync<T>(storeId, new CacheStrategy { Refresh = true });
    }

    public async Task<T> GetSettingsAsync<T>(string storeId)
        where T : class, ISettings, new()
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var key = _settingUtils.GetSettingsRequiredTypeIdentityName<T>(storeId);

        var settings = await _keyValueService.GetObjectOrDefaultAsync<T>(key);

        if (settings != null)
            return settings;

        return new T();
    }

    public async Task<T> GetSettingsAsync<T>(string storeId, CacheStrategy cacheStrategy)
        where T : class, ISettings, new()
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var key = _settingUtils.GetSettingsRequiredTypeIdentityName<T>(storeId);

        var option = new CacheOption<T>(key: CacheKey(key), TimeSpan.FromMinutes(10));

        if (cacheStrategy.RemoveCache ?? false || (cacheStrategy.Refresh ?? false))
            MemoryCache.Remove(option.Key);

        var settings = await MemoryCache.GetOrCreateAsync(option.Key, async x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30);
            return await CacheProvider.ExecuteWithPolicyAsync(async () => await GetSettingsAsync<T>(storeId),
                option,
                cacheStrategy);
        });

        return settings ?? new T();
    }
}