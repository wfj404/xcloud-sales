﻿CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` (
    `MigrationId` varchar(150) CHARACTER SET utf8mb4 NOT NULL,
    `ProductVersion` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
) CHARACTER SET=utf8mb4;

START TRANSACTION;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    ALTER DATABASE CHARACTER SET utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_activity_log` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `ActivityLogTypeId` int NOT NULL,
        `UserId` int NOT NULL,
        `AdministratorId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Comment` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `Value` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Data` longtext CHARACTER SET utf8mb4 NULL,
        `UrlReferrer` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `BrowserType` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Device` varchar(100) CHARACTER SET utf8mb4 NULL,
        `UserAgent` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `IpAddress` varchar(100) CHARACTER SET utf8mb4 NULL,
        `GeoCountry` varchar(200) CHARACTER SET utf8mb4 NULL,
        `GeoCity` varchar(200) CHARACTER SET utf8mb4 NULL,
        `Lng` double NULL,
        `Lat` double NULL,
        `RequestPath` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `SubjectType` varchar(100) CHARACTER SET utf8mb4 NULL,
        `SubjectId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `SubjectIntId` int NOT NULL,
        `CreationTime` datetime(6) NOT NULL,
        CONSTRAINT `PK_sales_activity_log` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_after_sales` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UserId` int NOT NULL,
        `ReasonForReturn` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        `RequestedAction` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        `UserComments` varchar(500) CHARACTER SET utf8mb4 NULL,
        `StaffNotes` varchar(500) CHARACTER SET utf8mb4 NULL,
        `Images` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `AfterSalesStatusId` int NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_after_sales` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_after_sales_comment` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `AfterSaleId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Content` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
        `PictureJson` longtext CHARACTER SET utf8mb4 NULL,
        `IsAdmin` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_after_sales_comment` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_after_sales_item` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `AfterSalesId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `OrderItemId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Quantity` int NOT NULL,
        CONSTRAINT `PK_sales_after_sales_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_balance_history` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` int NOT NULL,
        `Balance` decimal(18,2) NOT NULL,
        `LatestBalance` decimal(18,2) NOT NULL,
        `ActionType` int NOT NULL,
        `Message` varchar(300) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_balance_history` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_brand` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(400) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `ShowOnPublicPage` tinyint(1) NOT NULL,
        `MetaKeywords` varchar(400) CHARACTER SET utf8mb4 NULL,
        `PictureId` int NOT NULL,
        `Published` tinyint(1) NOT NULL,
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `DisplayOrder` int NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_brand` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_category` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SeoName` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Description` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `RootId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `NodePath` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `ParentCategoryId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `PictureId` int NOT NULL,
        `ShowOnHomePage` tinyint(1) NOT NULL,
        `Published` tinyint(1) NOT NULL,
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `DisplayOrder` int NOT NULL,
        `Recommend` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_category` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_common_entity_mapping` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `EntityId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `EntityName` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        `TargetId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `TargetName` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        CONSTRAINT `PK_sales_common_entity_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_common_entity_not_mapping` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `EntityId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `EntityName` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        `TargetId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `TargetName` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        CONSTRAINT `PK_sales_common_entity_not_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_coupon` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `CouponType` int NOT NULL,
        `Price` decimal(18,2) NOT NULL,
        `StartTime` datetime(6) NULL,
        `EndTime` datetime(6) NULL,
        `OrderConditionRulesJson` longtext CHARACTER SET utf8mb4 NULL,
        `Exclusive` tinyint(1) NOT NULL,
        `ExpiredInDays` int NULL,
        `IsAmountLimit` tinyint(1) NOT NULL,
        `Amount` int NOT NULL,
        `AccountIssuedLimitCount` int NULL,
        `IssuedAmount` int NOT NULL,
        `UsedAmount` int NOT NULL,
        `LimitedToStore` tinyint(1) NOT NULL DEFAULT FALSE,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_coupon` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_coupon_user_mapping` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `CouponId` longtext CHARACTER SET utf8mb4 NULL,
        `UserId` int NOT NULL,
        `IsUsed` tinyint(1) NOT NULL,
        `UsedTime` datetime(6) NULL,
        `ExpiredAt` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_coupon_user_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_discount` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `MaxQuantityLimitation` int NULL,
        `DiscountPercentage` decimal(65,30) NOT NULL,
        `StartTime` datetime(6) NULL,
        `EndTime` datetime(6) NULL,
        `IsActive` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `Sort` int NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `LimitedToStore` tinyint(1) NOT NULL DEFAULT FALSE,
        CONSTRAINT `PK_sales_discount` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_freight_template` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `StoreId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ShipInHours` int NULL,
        `FreeShippingMinOrderTotalPrice` decimal(18,2) NOT NULL,
        `CalculateType` int NOT NULL,
        `RulesJson` longtext CHARACTER SET utf8mb4 NULL,
        `ExcludedAreaJson` longtext CHARACTER SET utf8mb4 NULL,
        `Sort` int NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_freight_template` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_freight_template_goods_mapping` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `StoreId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `TemplateId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_freight_template_goods_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_gift_card` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Amount` decimal(18,2) NOT NULL,
        `EndTime` datetime(6) NULL,
        `UserId` int NOT NULL,
        `Used` tinyint(1) NOT NULL,
        `UsedTime` datetime(6) NULL,
        `IsActive` tinyint(1) NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_gift_card` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `GoodsType` int NOT NULL DEFAULT 0,
        `Name` varchar(400) CHARACTER SET utf8mb4 NOT NULL,
        `SeoName` varchar(500) CHARACTER SET utf8mb4 NULL,
        `Description` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `Keywords` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `DetailInformation` longtext CHARACTER SET utf8mb4 NULL,
        `AdminComment` varchar(500) CHARACTER SET utf8mb4 NULL,
        `CategoryId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `BrandId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `LimitedToRedirect` tinyint(1) NOT NULL,
        `RedirectToUrlJson` longtext CHARACTER SET utf8mb4 NULL,
        `LimitedToContact` tinyint(1) NOT NULL,
        `ContactTelephone` varchar(50) CHARACTER SET utf8mb4 NULL,
        `ApprovedReviewsRates` double NOT NULL,
        `ApprovedTotalReviews` int NOT NULL,
        `StockDeductStrategy` int NOT NULL,
        `StickyTop` tinyint(1) NOT NULL,
        `IsNew` tinyint(1) NOT NULL,
        `IsHot` tinyint(1) NOT NULL,
        `DisplayAsInformationPage` tinyint(1) NOT NULL,
        `PlaceOrderSupported` tinyint(1) NOT NULL,
        `AfterSalesSupported` tinyint(1) NOT NULL,
        `PayAfterDeliveredSupported` tinyint(1) NOT NULL,
        `CommentSupported` tinyint(1) NOT NULL,
        `CouponSupported` tinyint(1) NOT NULL,
        `DiscountSupported` tinyint(1) NOT NULL,
        `Published` tinyint(1) NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `CreationTime` datetime(6) NOT NULL,
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_goods` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_attribute` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Value` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_goods_attribute` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_gift` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GiftSkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Quantity` int NOT NULL,
        CONSTRAINT `PK_sales_goods_gift` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_grade_price` (
        `Id` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GradeId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PriceOffset` decimal(18,2) NOT NULL,
        `CreationTime` datetime(6) NOT NULL,
        CONSTRAINT `PK_sales_goods_grade_price` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_picture` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `PictureId` int NOT NULL,
        `DisplayOrder` int NOT NULL,
        CONSTRAINT `PK_sales_goods_picture` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_price_history` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PreviousPrice` decimal(18,2) NOT NULL,
        `Price` decimal(18,2) NOT NULL,
        `Comment` varchar(500) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_goods_price_history` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_review` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UserId` int NOT NULL,
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Title` varchar(500) CHARACTER SET utf8mb4 NULL,
        `ReviewText` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `Rating` int NOT NULL,
        `IpAddress` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL,
        CONSTRAINT `PK_sales_goods_review` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_goods_virtual_extension` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ExpiredAfterDays` int NULL,
        CONSTRAINT `PK_sales_goods_virtual_extension` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_grade` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `UserCount` int NOT NULL,
        `Sort` int NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_grade` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_notification` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Title` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Content` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `BusinessId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `App` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Read` tinyint(1) NOT NULL,
        `ReadTime` datetime(6) NULL,
        `UserId` int NOT NULL,
        `ActionType` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Data` longtext CHARACTER SET utf8mb4 NULL,
        `SenderId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `SenderType` varchar(50) CHARACTER SET utf8mb4 NULL,
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_notification` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_order` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `GoodsType` int NOT NULL,
        `PickupInStore` tinyint(1) NOT NULL,
        `StoreId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `OrderSn` varchar(100) CHARACTER SET utf8mb4 NULL,
        `UserId` int NOT NULL,
        `GradeId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `OrderStatusId` int NOT NULL,
        `DeliveryMethodId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `DeliveryStatusId` int NOT NULL,
        `UserAddressId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `IsAfterSales` tinyint(1) NOT NULL,
        `PaymentMethodId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `PaymentStatusId` int NOT NULL,
        `PaidTotalPrice` decimal(18,2) NOT NULL,
        `RefundedAmount` decimal(18,2) NOT NULL,
        `TotalPrice` decimal(18,2) NOT NULL,
        `ItemsTotalPrice` decimal(18,2) NOT NULL,
        `PackageFee` decimal(18,2) NOT NULL,
        `DeliveryFee` decimal(18,2) NOT NULL,
        `DeliveryFeeOffset` decimal(18,2) NOT NULL,
        `SellerPriceOffset` decimal(18,2) NOT NULL,
        `CouponPrice` decimal(18,2) NOT NULL,
        `PromotionId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `PromotionPriceOffset` decimal(18,2) NOT NULL,
        `RewardPointsHistoryId` int NULL,
        `ExchangePointsAmount` decimal(18,2) NOT NULL,
        `AffiliateId` int NOT NULL,
        `Remark` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `OrderIp` varchar(100) CHARACTER SET utf8mb4 NULL,
        `PlatformId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `CreationTime` datetime(6) NOT NULL,
        CONSTRAINT `PK_sales_order` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_order_coupon_record` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UserCouponId` int NOT NULL,
        CONSTRAINT `PK_sales_order_coupon_record` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_order_item` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SpecDescription` varchar(500) CHARACTER SET utf8mb4 NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GoodsName` varchar(500) CHARACTER SET utf8mb4 NULL,
        `GoodsDescription` varchar(500) CHARACTER SET utf8mb4 NULL,
        `SkuName` varchar(500) CHARACTER SET utf8mb4 NULL,
        `Weight` int NOT NULL,
        `Height` int NOT NULL,
        `Width` int NOT NULL,
        `Length` int NOT NULL,
        `StockDeductStrategy` int NOT NULL,
        `Quantity` int NOT NULL,
        `BasePrice` decimal(18,2) NOT NULL,
        `BasePriceProvider` varchar(100) CHARACTER SET utf8mb4 NULL,
        `BasePriceDescription` varchar(500) CHARACTER SET utf8mb4 NULL,
        `GradePriceOffset` decimal(18,2) NOT NULL,
        `DiscountPriceOffset` decimal(18,2) NOT NULL,
        `ValueAddedPriceOffset` decimal(18,2) NOT NULL,
        `TotalPrice` decimal(18,2) NOT NULL,
        `Remark` varchar(100) CHARACTER SET utf8mb4 NULL,
        CONSTRAINT `PK_sales_order_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_order_item_gift` (
        `Id` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
        `OrderItemId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` longtext CHARACTER SET utf8mb4 NULL,
        `Quantity` int NOT NULL,
        CONSTRAINT `PK_sales_order_item_gift` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_order_note` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Note` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `DisplayToUser` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_order_note` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_order_value_added_item` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderItemId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ValueAddedItemId` int NOT NULL,
        `PriceOffset` decimal(65,30) NOT NULL,
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        CONSTRAINT `PK_sales_order_value_added_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_page` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `SeoName` varchar(500) CHARACTER SET utf8mb4 NOT NULL,
        `CoverImageResourceData` longtext CHARACTER SET utf8mb4 NULL,
        `Title` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Description` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `BodyContent` longtext CHARACTER SET utf8mb4 NULL,
        `ReadCount` int NOT NULL,
        `IsPublished` tinyint(1) NOT NULL,
        `PublishedTime` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_page` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_payment_bill` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `BillType` int NOT NULL,
        `EntityId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Price` decimal(18,2) NOT NULL,
        `PaymentChannel` int NOT NULL,
        `Paid` tinyint(1) NOT NULL,
        `PayTime` datetime(6) NULL,
        `PaymentTransactionId` varchar(200) CHARACTER SET utf8mb4 NULL,
        `NotifyData` longtext CHARACTER SET utf8mb4 NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_payment_bill` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_pickup_record` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Prepared` tinyint(1) NOT NULL,
        `PreparedTime` datetime(6) NULL,
        `ReservationApproved` tinyint(1) NOT NULL,
        `ReservationApprovedTime` datetime(6) NULL,
        `PreferPickupStartTime` datetime(6) NULL,
        `PreferPickupEndTime` datetime(6) NULL,
        `Picked` tinyint(1) NOT NULL,
        `PickedTime` datetime(6) NULL,
        `SecurityCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_pickup_record` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_picture` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `MimeType` varchar(40) CHARACTER SET utf8mb4 NOT NULL,
        `SeoFilename` varchar(300) CHARACTER SET utf8mb4 NULL,
        `ResourceId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `ResourceData` longtext CHARACTER SET utf8mb4 NULL,
        CONSTRAINT `PK_sales_picture` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_points_history` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `UserId` int NOT NULL,
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Points` int NOT NULL,
        `PointsBalance` int NOT NULL,
        `ActionType` int NOT NULL,
        `UsedAmount` decimal(18,2) NOT NULL,
        `Message` varchar(300) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_points_history` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_refund_bill` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `BillType` int NOT NULL,
        `EntityId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `BillId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Price` decimal(18,2) NOT NULL,
        `Approved` tinyint(1) NOT NULL DEFAULT FALSE,
        `ApprovedTime` datetime(6) NULL,
        `Refunded` tinyint(1) NOT NULL,
        `RefundTime` datetime(6) NULL,
        `RefundTransactionId` varchar(200) CHARACTER SET utf8mb4 NULL,
        `RefundNotifyData` longtext CHARACTER SET utf8mb4 NULL,
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_refund_bill` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_shipment_record` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PreferDeliveryStartTime` datetime(6) NULL,
        `PreferDeliveryEndTime` datetime(6) NULL,
        `DeliveryMethod` longtext CHARACTER SET utf8mb4 NULL,
        `ExpressName` varchar(100) CHARACTER SET utf8mb4 NULL,
        `TrackingNumber` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Delivering` tinyint(1) NOT NULL,
        `DeliveringTime` datetime(6) NULL,
        `Delivered` tinyint(1) NOT NULL,
        `DeliveredTime` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_shipment_record` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_shipment_record_track` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `DeliveryId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Lon` double NOT NULL,
        `Lat` double NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_shipment_record_track` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_shopping_cart_item` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `UserId` int NOT NULL,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Quantity` int NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_shopping_cart_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_shopping_cart_value_added_item` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `ShoppingCartItemId` int NOT NULL,
        `ValueAddedItemId` int NOT NULL,
        CONSTRAINT `PK_sales_shopping_cart_value_added_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_sku` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `SkuCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Color` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CostPrice` decimal(18,2) NOT NULL,
        `OldPrice` decimal(18,2) NOT NULL,
        `Price` decimal(18,2) NOT NULL,
        `MaxAmountInOnePurchase` int NOT NULL,
        `MinAmountInOnePurchase` int NOT NULL,
        `Weight` int NOT NULL,
        `Length` int NOT NULL,
        `Height` int NOT NULL,
        `Width` int NOT NULL,
        `NumberOfItems` int NOT NULL,
        `SpecCombinationJson` longtext CHARACTER SET utf8mb4 NULL COMMENT '规格配置参数',
        `IsActive` tinyint(1) NOT NULL,
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_sku` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_spec` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_spec` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_spec_value` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `SpecId` int NOT NULL,
        `Name` varchar(400) CHARACTER SET utf8mb4 NOT NULL,
        `PriceOffset` decimal(18,2) NOT NULL,
        `AssociatedSkuId` int NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_spec_value` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_stock` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `No` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SupplierId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `WarehouseId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Remark` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `Approved` tinyint(1) NOT NULL,
        `ApprovedByUserId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `ApprovedTime` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_stock` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_stock_item` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `WarehouseStockId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Quantity` int NOT NULL,
        `Price` decimal(18,2) NOT NULL,
        `DeductQuantity` int NOT NULL,
        `RuningOut` tinyint(1) NOT NULL,
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_stock_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_stock_usage_history` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `OrderItemId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `WarehouseStockItemId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Quantity` int NOT NULL,
        `Revert` tinyint(1) NOT NULL DEFAULT FALSE,
        `RevertTime` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_stock_usage_history` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_store` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `AddressDetail` varchar(500) CHARACTER SET utf8mb4 NULL,
        `Telephone` varchar(50) CHARACTER SET utf8mb4 NULL,
        `Closed` tinyint(1) NOT NULL,
        `PickupSupported` tinyint(1) NOT NULL,
        `InvoiceSupported` tinyint(1) NOT NULL,
        `Lon` double NOT NULL,
        `Lat` double NOT NULL,
        `IsActive` tinyint(1) NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_store` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_store_goods_mapping` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `StoreId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `StockQuantity` int NOT NULL,
        `Price` decimal(65,30) NOT NULL,
        `OverridePrice` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_store_goods_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_store_goods_quantity_history` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `StoreId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `SkuId` longtext CHARACTER SET utf8mb4 NULL,
        `OrderItemId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_store_goods_quantity_history` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_store_manager` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `StoreId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Role` int NOT NULL,
        `IsActive` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_store_manager` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_store_user` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Balance` decimal(18,2) NOT NULL,
        `Points` int NOT NULL,
        `HistoryPoints` int NOT NULL,
        `IsActive` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL,
        `LastActivityTime` datetime(6) NOT NULL,
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        CONSTRAINT `PK_sales_store_user` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_store_user_grade_mapping` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` int NOT NULL,
        `GradeId` longtext CHARACTER SET utf8mb4 NULL,
        `StartTime` datetime(6) NULL,
        `EndTime` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sales_store_user_grade_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_supplier` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ContactName` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Telephone` varchar(50) CHARACTER SET utf8mb4 NULL,
        `Address` varchar(500) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_supplier` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_tag` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `Alert` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `Link` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_tag` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_tag_goods_mapping` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `TagId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        CONSTRAINT `PK_sales_tag_goods_mapping` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_value_added_item` (
        `Id` int NOT NULL AUTO_INCREMENT,
        `GroupId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PriceOffset` decimal(65,30) NOT NULL,
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        CONSTRAINT `PK_sales_value_added_item` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_value_added_item_group` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `GoodsId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `IsRequired` tinyint(1) NOT NULL,
        `MultipleChoice` tinyint(1) NOT NULL,
        CONSTRAINT `PK_sales_value_added_item_group` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sales_warehouse` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Address` varchar(500) CHARACTER SET utf8mb4 NULL,
        `Lat` double NULL,
        `Lng` double NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        CONSTRAINT `PK_sales_warehouse` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_admin` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `IsActive` tinyint(1) NOT NULL,
        `IsSuperAdmin` tinyint(1) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_admin` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_admin_role` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `RoleId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `AdminId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_admin_role` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_external_access_token` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Platform` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `AppId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UserId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Scope` varchar(100) CHARACTER SET utf8mb4 NULL,
        `AccessTokenType` int NOT NULL,
        `AccessToken` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
        `RefreshToken` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `ExpiredAt` datetime(6) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_external_access_token` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_external_connect` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Platform` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `AppId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `OpenId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_external_connect` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_im_message` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Content` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `MessageType` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Data` longtext CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_im_message` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_im_online_status` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `DeviceId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ServerInstanceId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PingTime` datetime(6) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_im_online_status` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_im_server_instance` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `InstanceId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ConnectionCount` int NOT NULL,
        `PingTime` datetime(6) NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_im_server_instance` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_permission` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `PermissionKey` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(500) CHARACTER SET utf8mb4 NULL,
        `Group` varchar(100) CHARACTER SET utf8mb4 NULL,
        `AppKey` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_permission` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_resource_acl` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `ResourceType` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ResourceId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PermissionType` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `PermissionId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `AccessControlType` int NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_resource_acl` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_role` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
        `Description` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_role` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_role_permission` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `PermissionKey` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `RoleId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_role_permission` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_serial_no` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Category` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `NextId` int NOT NULL,
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_serial_no` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_setting` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Name` varchar(300) CHARACTER SET utf8mb4 NOT NULL,
        `Value` longtext CHARACTER SET utf8mb4 NULL,
        CONSTRAINT `PK_sys_setting` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_storage_meta` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `FileExtension` varchar(30) CHARACTER SET utf8mb4 NULL,
        `ContentType` varchar(100) CHARACTER SET utf8mb4 NULL,
        `ResourceSize` bigint NOT NULL,
        `ResourceKey` varchar(500) CHARACTER SET utf8mb4 NULL,
        `ResourceHash` varchar(100) CHARACTER SET utf8mb4 NULL,
        `HashType` varchar(20) CHARACTER SET utf8mb4 NULL,
        `ExtraData` longtext CHARACTER SET utf8mb4 NULL,
        `StorageProvider` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `ReferenceCount` int NOT NULL,
        `UploadTimes` int NOT NULL,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_storage_meta` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_user` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `IdentityName` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `OriginIdentityName` varchar(100) CHARACTER SET utf8mb4 NULL,
        `ExternalSystemName` varchar(100) CHARACTER SET utf8mb4 NULL,
        `ExternalId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `NickName` varchar(30) CHARACTER SET utf8mb4 NULL,
        `PassWord` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Avatar` varchar(2000) CHARACTER SET utf8mb4 NULL,
        `LastPasswordUpdateTime` datetime(6) NULL,
        `Gender` int NOT NULL,
        `IsActive` tinyint(1) NOT NULL DEFAULT FALSE,
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        `LastModificationTime` datetime(6) NULL COMMENT '更新时间，同时也是乐观锁',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_user` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_user_address` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Lat` double NULL,
        `Lon` double NULL,
        `Name` varchar(100) CHARACTER SET utf8mb4 NULL,
        `NationCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Nation` varchar(100) CHARACTER SET utf8mb4 NULL,
        `ProvinceCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Province` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CityCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `City` varchar(100) CHARACTER SET utf8mb4 NULL,
        `AreaCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Area` varchar(100) CHARACTER SET utf8mb4 NULL,
        `AddressDescription` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `AddressDetail` varchar(500) CHARACTER SET utf8mb4 NULL,
        `PostalCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `Tel` varchar(100) CHARACTER SET utf8mb4 NULL,
        `IsDefault` tinyint(1) NOT NULL,
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_user_address` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_user_identity` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Identity` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
        `Data` varchar(4000) CHARACTER SET utf8mb4 NULL,
        `IdentityType` int NOT NULL,
        `MobilePhone` varchar(20) CHARACTER SET utf8mb4 NULL DEFAULT '',
        `MobileAreaCode` varchar(10) CHARACTER SET utf8mb4 NULL DEFAULT '',
        `MobileConfirmed` tinyint(1) NULL,
        `MobileConfirmedTimeUtc` datetime(6) NULL,
        `Email` varchar(100) CHARACTER SET utf8mb4 NULL DEFAULT '',
        `EmailConfirmed` tinyint(1) NULL,
        `EmailConfirmedTimeUtc` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_user_identity` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_user_invoice` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Head` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Code` varchar(100) CHARACTER SET utf8mb4 NULL,
        `InvoiceType` int NOT NULL,
        `Address` varchar(500) CHARACTER SET utf8mb4 NULL,
        `PhoneNumber` varchar(100) CHARACTER SET utf8mb4 NULL,
        `BankName` varchar(100) CHARACTER SET utf8mb4 NULL,
        `BankCode` varchar(100) CHARACTER SET utf8mb4 NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        `IsDeleted` tinyint(1) NOT NULL DEFAULT FALSE COMMENT '是否删除',
        `DeletionTime` datetime(6) NULL COMMENT '删除时间',
        CONSTRAINT `PK_sys_user_invoice` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_user_real_name` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `IdCardIdentity` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UserId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `Data` varchar(4000) CHARACTER SET utf8mb4 NULL,
        `IdCardType` int NOT NULL,
        `IdCard` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `IdCardRealName` varchar(20) CHARACTER SET utf8mb4 NULL,
        `IdCardFrontUrl` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `IdCardBackUrl` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `IdCardHandUrl` varchar(1000) CHARACTER SET utf8mb4 NULL,
        `StartTimeUtc` datetime(6) NULL,
        `EndTimeUtc` datetime(6) NULL,
        `IdCardStatus` int NOT NULL,
        `IdCardConfirmed` tinyint(1) NULL,
        `IdCardConfirmedTimeUtc` datetime(6) NULL,
        `CreationTime` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
        CONSTRAINT `PK_sys_user_real_name` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE TABLE `sys_wx_union` (
        `Id` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键，最好是顺序生成的GUID',
        `Platform` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `AppId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `OpenId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        `UnionId` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
        CONSTRAINT `PK_sys_wx_union` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_after_sales_CreationTime` ON `sales_after_sales` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_after_sales_IsDeleted` ON `sales_after_sales` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_after_sales_LastModificationTime` ON `sales_after_sales` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_after_sales_comment_CreationTime` ON `sales_after_sales_comment` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_balance_history_CreationTime` ON `sales_balance_history` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_brand_CreationTime` ON `sales_brand` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_brand_IsDeleted` ON `sales_brand` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_category_CreationTime` ON `sales_category` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_category_IsDeleted` ON `sales_category` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_coupon_CreationTime` ON `sales_coupon` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_coupon_IsDeleted` ON `sales_coupon` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_coupon_user_mapping_CreationTime` ON `sales_coupon_user_mapping` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_discount_CreationTime` ON `sales_discount` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_discount_IsDeleted` ON `sales_discount` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_freight_template_CreationTime` ON `sales_freight_template` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_freight_template_goods_mapping_CreationTime` ON `sales_freight_template_goods_mapping` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_gift_card_CreationTime` ON `sales_gift_card` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_gift_card_IsDeleted` ON `sales_gift_card` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_goods_IsDeleted` ON `sales_goods` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_goods_LastModificationTime` ON `sales_goods` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_goods_attribute_CreationTime` ON `sales_goods_attribute` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_goods_price_history_CreationTime` ON `sales_goods_price_history` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_grade_IsDeleted` ON `sales_grade` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_notification_CreationTime` ON `sales_notification` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_notification_LastModificationTime` ON `sales_notification` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_order_IsDeleted` ON `sales_order` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_order_LastModificationTime` ON `sales_order` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_order_note_CreationTime` ON `sales_order_note` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_page_CreationTime` ON `sales_page` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_page_IsDeleted` ON `sales_page` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_payment_bill_CreationTime` ON `sales_payment_bill` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_payment_bill_IsDeleted` ON `sales_payment_bill` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_pickup_record_CreationTime` ON `sales_pickup_record` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_points_history_CreationTime` ON `sales_points_history` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_refund_bill_CreationTime` ON `sales_refund_bill` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_refund_bill_IsDeleted` ON `sales_refund_bill` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_refund_bill_LastModificationTime` ON `sales_refund_bill` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_shipment_record_CreationTime` ON `sales_shipment_record` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_shipment_record_LastModificationTime` ON `sales_shipment_record` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_shipment_record_track_CreationTime` ON `sales_shipment_record_track` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_shopping_cart_item_CreationTime` ON `sales_shopping_cart_item` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_shopping_cart_item_LastModificationTime` ON `sales_shopping_cart_item` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_sku_CreationTime` ON `sales_sku` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_sku_IsDeleted` ON `sales_sku` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_sku_LastModificationTime` ON `sales_sku` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_spec_IsDeleted` ON `sales_spec` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_spec_value_IsDeleted` ON `sales_spec_value` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_stock_CreationTime` ON `sales_stock` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_stock_item_LastModificationTime` ON `sales_stock_item` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_stock_usage_history_CreationTime` ON `sales_stock_usage_history` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_CreationTime` ON `sales_store` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_IsDeleted` ON `sales_store` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_goods_mapping_CreationTime` ON `sales_store_goods_mapping` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_goods_mapping_LastModificationTime` ON `sales_store_goods_mapping` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_goods_quantity_history_CreationTime` ON `sales_store_goods_quantity_history` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_manager_CreationTime` ON `sales_store_manager` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_user_LastModificationTime` ON `sales_store_user` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_store_user_grade_mapping_CreationTime` ON `sales_store_user_grade_mapping` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_supplier_CreationTime` ON `sales_supplier` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_supplier_IsDeleted` ON `sales_supplier` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_tag_IsDeleted` ON `sales_tag` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_warehouse_CreationTime` ON `sales_warehouse` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sales_warehouse_IsDeleted` ON `sales_warehouse` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_admin_CreationTime` ON `sys_admin` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_admin_role_CreationTime` ON `sys_admin_role` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_external_access_token_CreationTime` ON `sys_external_access_token` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_external_connect_CreationTime` ON `sys_external_connect` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_im_message_CreationTime` ON `sys_im_message` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_im_online_status_CreationTime` ON `sys_im_online_status` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_im_server_instance_CreationTime` ON `sys_im_server_instance` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_permission_CreationTime` ON `sys_permission` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_resource_acl_CreationTime` ON `sys_resource_acl` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_role_CreationTime` ON `sys_role` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_role_permission_CreationTime` ON `sys_role_permission` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE UNIQUE INDEX `IX_sys_serial_no_Category` ON `sys_serial_no` (`Category`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_serial_no_CreationTime` ON `sys_serial_no` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_serial_no_LastModificationTime` ON `sys_serial_no` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_storage_meta_CreationTime` ON `sys_storage_meta` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_storage_meta_IsDeleted` ON `sys_storage_meta` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_storage_meta_LastModificationTime` ON `sys_storage_meta` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_CreationTime` ON `sys_user` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE UNIQUE INDEX `IX_sys_user_IdentityName` ON `sys_user` (`IdentityName`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_IsDeleted` ON `sys_user` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_LastModificationTime` ON `sys_user` (`LastModificationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_address_CreationTime` ON `sys_user_address` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_address_IsDeleted` ON `sys_user_address` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_identity_CreationTime` ON `sys_user_identity` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE UNIQUE INDEX `IX_sys_user_identity_Identity` ON `sys_user_identity` (`Identity`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_identity_UserId` ON `sys_user_identity` (`UserId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_invoice_CreationTime` ON `sys_user_invoice` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_invoice_IsDeleted` ON `sys_user_invoice` (`IsDeleted`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_real_name_CreationTime` ON `sys_user_real_name` (`CreationTime`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE UNIQUE INDEX `IX_sys_user_real_name_IdCardIdentity` ON `sys_user_real_name` (`IdCardIdentity`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    CREATE INDEX `IX_sys_user_real_name_UserId` ON `sys_user_real_name` (`UserId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20230629082319_xx') THEN

    INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
    VALUES ('20230629082319_xx', '6.0.9');

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

COMMIT;

