import { useEffect } from 'react';
import { useOutlet, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { GlobalContainer } from '@/utils/utils';

import AntdConfig from './antd_config';
import AntdMobileConfig from './antd_mobile_config';
import LoadAppSettingsWrapper from './app_settings';
import XGlobalComponents from './global_components';
import MuiConfig from './mui_config';
import SetTokenWrapper from './set_token';

export default function ({}: {}) {
  const children = useOutlet();
  const app = useSnapshot(storeState) as StoreType;

  useEffect(() => {
    GlobalContainer.store = app;
    return () => {
      GlobalContainer.store = undefined;
    };
  }, [app]);

  return (
    <>
      <AntdConfig>
        <AntdMobileConfig>
          <MuiConfig>
            <SetTokenWrapper>
              <LoadAppSettingsWrapper>
                <XGlobalComponents>{children}</XGlobalComponents>
              </LoadAppSettingsWrapper>
            </SetTokenWrapper>
          </MuiConfig>
        </AntdMobileConfig>
      </AntdConfig>
    </>
  );
}
