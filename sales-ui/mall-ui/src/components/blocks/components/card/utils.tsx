import { GoodsDto } from '@/utils/swagger';

import { BlockItem, BlockItemProps, BlockPictureProps } from '../../utils';

export interface CardBlockProps extends BlockItemProps {
  picture?: BlockPictureProps;
  tag?: string;
  title?: string;
  subtitle?: string;
  caption?: string;
  goods_list?: GoodsDto[];

  color?: string;
  borderWidth?: number;
  borderRadius?: number;
}

export const card_demo: BlockItem<CardBlockProps> = {
  type: 'card',
  body: {
    picture: {
      storage:
        'https://www.apple.com.cn/v/tv-home/k/images/overview/homepod__eam53jjm772a_large.jpg',
      rate: 10 / 9,
    },
    tag: '上墙',
    title: '一周榜单',
    subtitle: '限时活动',
    caption: '6月新品周榜单',
    borderRadius: 10,
    borderWidth: 3,
    color: 'purple',
  },
};
