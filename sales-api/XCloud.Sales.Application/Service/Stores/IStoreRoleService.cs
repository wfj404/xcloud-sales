﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreRoleService : ISalesStringCrudAppService<StoreRole, StoreRoleDto, PagedRequest>
{
    Task<StoreManagerDto[]> AttachStoreRolesAsync(StoreManagerDto[] data);

    Task<StoreRoleDto[]> AttachPermissionKeysAsync(StoreRoleDto[] data);

    Task<StoreRoleDto[]> ListAllRolesAsync(string storeId);

    Task SetRolePermissionsAsync(string roleId, string[] permissionKeys);

    Task SetManagerRolesAsync(string managerId, string[] roleIds);
}

public class StoreRoleService : SalesStringCrudAppService<StoreRole, StoreRoleDto, PagedRequest>, IStoreRoleService
{
    public StoreRoleService(ISalesRepository<StoreRole> repository) : base(repository)
    {
        //
    }

    public async Task<StoreManagerDto[]> AttachStoreRolesAsync(StoreManagerDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await this.Repository.GetDbContextAsync();

        var query = from managerRole in db.Set<StoreManagerRole>().AsNoTracking()
            join role in db.Set<StoreRole>().AsNoTracking()
                on managerRole.RoleId equals role.Id
            select new { managerRole, role };

        query = query.Where(x => ids.Contains(x.managerRole.ManagerId));

        var datalist = await query.ToArrayAsync();

        foreach (var m in data)
        {
            m.StoreRoles = datalist
                .Where(x => x.managerRole.ManagerId == m.Id)
                .Select(x => x.role)
                .MapArrayTo<StoreRole, StoreRoleDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<StoreRoleDto[]> AttachPermissionKeysAsync(StoreRoleDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await this.Repository.GetDbContextAsync();

        var datalist = await db.Set<StoreRolePermission>().AsNoTracking().Where(x => ids.Contains(x.RoleId))
            .ToArrayAsync();

        foreach (var m in data)
        {
            m.PermissionKeys = datalist
                .Where(x => x.RoleId == m.Id)
                .Select(x => x.PermissionKey)
                .Distinct()
                .ToArray();
        }

        return data;
    }

    public async Task<StoreRoleDto[]> ListAllRolesAsync(string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var roles = await this.Repository.GetListAsync(x => x.StoreId == storeId);

        return roles.MapArrayTo<StoreRole, StoreRoleDto>(ObjectMapper);
    }

    public override Task<PagedResponse<StoreRoleDto>> QueryPagingAsync(PagedRequest dto)
    {
        throw new NotSupportedException();
    }

    public async Task SetRolePermissionsAsync(string roleId, string[] permissionKeys)
    {
        if (string.IsNullOrWhiteSpace(roleId))
            throw new ArgumentNullException(nameof(roleId));

        if (permissionKeys == null)
            throw new ArgumentNullException(nameof(permissionKeys));

        var datalist = permissionKeys.Select(x => new StoreRolePermission()
        {
            RoleId = roleId,
            PermissionKey = x,
        }).ToArray();

        var db = await this.Repository.GetDbContextAsync();

        var set = db.Set<StoreRolePermission>();

        var origin = await set.Where(x => x.RoleId == roleId).ToArrayAsync();

        var toDelete = origin.NotInBy(datalist, x => x.PermissionKey).ToArray();
        var toInsert = datalist.NotInBy(origin, x => x.PermissionKey).ToArray();

        if (toDelete.Any())
        {
            set.RemoveRange(toDelete);
        }

        if (toInsert.Any())
        {
            var now = this.Clock.Now;
            foreach (var m in toInsert)
            {
                m.Id = this.GuidGenerator.CreateGuidString();
                m.CreationTime = now;
            }

            set.AddRange(toInsert);
        }

        await db.TrySaveChangesAsync();
    }

    public async Task SetManagerRolesAsync(string managerId, string[] roleIds)
    {
        if (string.IsNullOrWhiteSpace(managerId))
            throw new ArgumentNullException(nameof(managerId));

        if (roleIds == null)
            throw new ArgumentNullException(nameof(roleIds));

        var datalist = roleIds.Select(x => new StoreManagerRole()
        {
            ManagerId = managerId,
            RoleId = x,
        }).ToArray();

        var db = await this.Repository.GetDbContextAsync();

        var set = db.Set<StoreManagerRole>();

        var origin = await set.Where(x => x.ManagerId == managerId).ToArrayAsync();

        var toDelete = origin.NotInBy(datalist, x => x.RoleId).ToArray();
        var toInsert = datalist.NotInBy(origin, x => x.RoleId).ToArray();

        if (toDelete.Any())
        {
            set.RemoveRange(toDelete);
        }

        if (toInsert.Any())
        {
            var now = this.Clock.Now;
            foreach (var m in toInsert)
            {
                m.Id = this.GuidGenerator.CreateGuidString();
                m.CreationTime = now;
            }

            set.AddRange(toInsert);
        }

        await db.TrySaveChangesAsync();
    }

    protected override async Task SetFieldsBeforeInsertAsync(StoreRole entity)
    {
        entity.Id = this.GuidGenerator.CreateGuidString();
        entity.CreationTime = this.Clock.Now;

        await Task.CompletedTask;
    }

    public override Task<StoreRole> InsertAsync(StoreRoleDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.StoreId))
            throw new ArgumentNullException(nameof(dto.StoreId));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        return base.InsertAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(StoreRole entity, StoreRoleDto dto)
    {
        entity.Name = dto.Name;
        entity.Description = dto.Description;

        await Task.CompletedTask;
    }

    public override Task<StoreRole> UpdateAsync(StoreRoleDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.StoreId))
            throw new ArgumentNullException(nameof(dto.StoreId));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        return base.UpdateAsync(dto);
    }
}