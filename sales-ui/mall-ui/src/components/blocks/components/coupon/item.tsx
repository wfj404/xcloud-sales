import { useRequest } from 'ahooks';
import { Space } from 'antd-mobile';

import { apiClient } from '@/utils/client';
import { CouponDto } from '@/utils/swagger';
import {
  formatMoney,
  GlobalContainer,
  handleResponse,
  isStringEmpty,
} from '@/utils/utils';
import { LoadingButton } from '@mui/lab';
import { Box, Paper, Tooltip, Typography } from '@mui/material';

export default ({
  data,
  onUpdate,
}: {
  data: CouponDto;
  onUpdate: () => void;
}) => {
  if (!data) {
    return null;
  }

  const getCouponRequest = useRequest(apiClient.mall.couponIssueCoupon, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        GlobalContainer.message?.success('领取成功');
        onUpdate();
      });
    },
  });

  const renderButton = () => {
    if (!data.CanBeIssued) {
      return (
        <Tooltip arrow title={data.UnableToIssueReason || undefined}>
          <Typography variant="caption" color="text.disabled">
            <span>无法领取</span>
          </Typography>
        </Tooltip>
      );
    }

    return (
      <LoadingButton
        loading={getCouponRequest.loading}
        disabled={!data.CanBeIssued || getCouponRequest.loading}
        size="small"
        onClick={() => {
          getCouponRequest.run({ CouponId: data.Id });
        }}
      >
        领取
      </LoadingButton>
    );
  };

  return (
    <>
      <Paper
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          p: 1,
        }}
        elevation={1}
      >
        <Box>
          <Space direction="horizontal">
            <Typography variant="subtitle1">{data.Name}</Typography>
            <Typography variant="overline" color="error">
              {formatMoney(data.Price || 0)}
            </Typography>
          </Space>
          {isStringEmpty(data.Description) || (
            <Typography
              variant="caption"
              color="text.secondary"
              component={'div'}
            >
              {data.Description}
            </Typography>
          )}
        </Box>
        <Box>{renderButton()}</Box>
      </Paper>
    </>
  );
};
