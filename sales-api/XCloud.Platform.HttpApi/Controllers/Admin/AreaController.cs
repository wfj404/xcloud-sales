﻿using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Common;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/area")]
public class AreaController : PlatformBaseController
{
    private readonly IAreaService _areaService;

    public AreaController(IAreaService areaService)
    {
        _areaService = areaService;
    }

    [Obsolete]
    [HttpPost("by-parent")]
    public async Task<ApiResponse<AreaDto[]>> GetByParentAsync([FromBody] IdDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var children = await this._areaService.GetByParentIdAsync(dto.Id);

        await this._areaService.AttachPictureMetaAsync(children);

        return new ApiResponse<AreaDto[]>(children);
    }

    [HttpPost("load-tree")]
    public async Task<ApiResponse<IList<AntDesignTreeNode<AreaDto>>>> LoadTreeAsync()
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var datalist = await this._areaService.GetAllAsync();

        await this._areaService.AttachPictureMetaAsync(datalist);

        var treeNodes = datalist
            .BuildAntTreeV2(x => x.Id, x => x.ParentId, x => x.Name)
            .ToArray();

        return new ApiResponse<IList<AntDesignTreeNode<AreaDto>>>(treeNodes);
    }

    [HttpPost("save")]
    public async Task<ApiResponse<object>> SaveAsync([FromBody] AreaDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        if (dto.IsEmptyId())
        {
            await this._areaService.InsertAsync(dto);
        }
        else
        {
            var entity = await this._areaService.GetByIdAsync(dto.Id);

            entity = entity ?? throw new EntityNotFoundException(nameof(entity));

            await this._areaService.UpdateAsync(dto);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

            if (entity.ParentId != dto.ParentId)
            {
                await this._areaService.AdjustParentIdAsync(dto.Id, dto.ParentId);
            }
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete-by-id")]
    public async Task<ApiResponse<object>> DeleteByIdAsync([FromBody] IdDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._areaService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }
}