import { useRequest, useTitle } from 'ahooks';
import { useEffect } from 'react';

import XLoading from '@/components/common/loading/dots';
import XHeader from '@/components/mobile/simpleHeader';
import { apiClient } from '@/utils/client';
import { Box } from '@mui/material';

import XAddressEditor from './address_editor';

export default function () {
  useTitle('我的收件地址');

  const getAddressListRequest = useRequest(apiClient.platform.userAddressList, {
    manual: true,
  });

  useEffect(() => {
    getAddressListRequest.run({});
  }, []);

  const renderEditor = () => {
    if (getAddressListRequest.loading) {
      return <XLoading />;
    }
    return (
      <Box sx={{ mx: 2, my: 1 }}>
        <XAddressEditor
          onRefresh={() => {
            getAddressListRequest.run({});
          }}
          addressList={getAddressListRequest.data?.data?.Data || []}
        />
      </Box>
    );
  };

  return (
    <>
      <XHeader>{renderEditor()}</XHeader>
    </>
  );
}
