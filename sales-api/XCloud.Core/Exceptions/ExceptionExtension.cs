﻿using Microsoft.AspNetCore.Http;
using XCloud.Core.Dto;

namespace XCloud.Core.Exceptions;

public static class ExceptionExtension
{
    public static ErrorInfo ConvertToComposedErrorInfoOrNull(this IErrorInfoConverter[] converters,
        HttpContext httpContext, Exception e)
    {
        foreach (var h in converters.OrderByDescending(x => x.Order))
        {
            var response = h.ConvertToErrorInfoOrNull(httpContext, e);
            if (response != null)
            {
                return response;
            }
        }

        return null;
    }
}