﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Platform.Application.Mapping;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Mapping.Stores;

public class StoreMap : SalesEntityTypeConfiguration<Store>
{
    public override void Configure(EntityTypeBuilder<Store> builder)
    {
        builder.ToTable("sales_store");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigExternalBinding();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.Telephone).HasMaxLength(50);
        builder.Property(x => x.AddressDetail).HasMaxLength(500);
        builder.Property(x => x.Opening);
        builder.Property(x => x.InvoiceSupported);
        builder.Property(x => x.CreationTime);
        builder.Property(x => x.IsDeleted);

        builder.Property(x => x.Lat);
        builder.Property(x => x.Lon);

        base.Configure(builder);
    }
}