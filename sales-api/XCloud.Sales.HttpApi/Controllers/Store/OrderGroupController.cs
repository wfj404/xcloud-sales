﻿using System.IO;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Core.Json;
using XCloud.Platform.Application.Service.Address;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Common;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.WeChat;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/order-group")]
public class OrderGroupController : ShopBaseController
{
    private readonly IWebHostEnvironment _webHostEnvironment;
    private readonly IOrderGroupService _orderGroupService;

    public OrderGroupController(IWebHostEnvironment webHostEnvironment, IOrderGroupService orderGroupService)
    {
        _webHostEnvironment = webHostEnvironment;
        _orderGroupService = orderGroupService;
    }

    [HttpPost("get-by-id")]
    public async Task<ApiResponse<OrderGroupDto>> GetByIdAsync([FromBody] IdDto dto)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var group = await this._orderGroupService.EnsureStoreOrderGroupAsync(dto.Id, manager.StoreId);

        group.GroupData =
            this.JsonDataSerializer.DeserializeFromStringOrDefault<OrderGroupDataDto>(group.OrderDataJson,
                this.Logger) ?? new OrderGroupDataDto();

        return new ApiResponse<OrderGroupDto>(group);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<OrderGroupDto>> QueryPagingAsync([FromBody] QueryOrderGroupPagingInput model)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        model.PageSize = 20;
        model.StoreId = manager.StoreId;

        var data = await _orderGroupService.QueryPagingAsync(model);

        return data;
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> SaveAsync([FromBody] OrderGroupDto dto)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();
        dto.StoreId = manager.StoreId;

        if (!dto.IsEmptyId())
        {
            await _orderGroupService.UpdateAsync(dto);
        }
        else
        {
            await _orderGroupService.InsertAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public virtual async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._orderGroupService.EnsureStoreOrderGroupAsync(dto.Id, manager.StoreId);

        await _orderGroupService.DeleteByIdAsync(dto);

        return new ApiResponse<object>();
    }

    [NonAction]
    [CanBeNull]
    private string GetItemGoodsName(OrderItemDto dto)
    {
        if (!string.IsNullOrWhiteSpace(dto.Sku?.Name))
        {
            return dto.Sku.Name;
        }

        if (!string.IsNullOrWhiteSpace(dto.Remark))
        {
            return dto.Remark;
        }

        return null;
    }

    [NonAction]
    private ICellStyle GetPrimaryCellStyle(IWorkbook wb,
        short? bgColor = default,
        bool? fontBold = default,
        short? fontColor = default)
    {
        var style = wb.CreateCellStyle();
        style.FillForegroundColor = style.FillBackgroundColor = bgColor ?? IndexedColors.Grey25Percent.Index;
        style.FillPattern = FillPattern.SolidForeground;
        //((XSSFColor)style.FillBackgroundColorColor).SetRgb(IndexedColors.Black.RGB);

        style.Alignment = HorizontalAlignment.Center;
        style.VerticalAlignment = VerticalAlignment.Center;

        var font = wb.CreateFont();
        font.IsBold = fontBold ?? true;
        font.Color = fontColor ?? IndexedColors.Black.Index;
        style.SetFont(font);

        // 设置边框样式
        style.BorderBottom = BorderStyle.Thin;
        style.BorderTop = BorderStyle.Thin;
        style.BorderLeft = BorderStyle.Thin;
        style.BorderRight = BorderStyle.Thin;

        return style;
    }

    [NonAction]
    private async Task TryWriteFirstLineLogoAsync(IWorkbook wb, ISheet sheet)
    {
        var filePath = Path.Join(this._webHostEnvironment.ContentRootPath, "data", "logo.png");

        if (System.IO.File.Exists(filePath))
        {
            var bs = await System.IO.File.ReadAllBytesAsync(filePath);
            var pictureId = wb.AddPicture(pictureData: bs, PictureType.PNG);

            var row = sheet.CreateRow(sheet.LastRowNum + 1);
            row.Height = 80 * 20;
            var cell = row.CreateCell(row.LastCellNum + 1);
            cell.CellStyle = GetPrimaryCellStyle(wb);

            var patriarch = sheet.CreateDrawingPatriarch();
            var anchor = new NPOI.XSSF.UserModel.XSSFClientAnchor(
                0, 0, 0, 0,
                0, 0, 1, 1);
            var picture = patriarch.CreatePicture(anchor, pictureId);
        }
    }

    [NonAction]
    private void WriteAddress(IRow row, UserAddressDto addressOrNull)
    {
        row.CreateCell(row.LastCellNum + 1).SetCellValue(addressOrNull?.Name ?? "unknown-user");
        row.CreateCell(row.LastCellNum + 1).SetCellValue(addressOrNull?.Tel ?? "mobile-phone");
        row.CreateCell(row.LastCellNum + 1)
            .SetCellValue(addressOrNull?.AddressDetail ?? "address-detail");
    }

    [NonAction]
    private async Task WriteOrderSheetAsync(IWorkbook wb, OrderGroupDto dto)
    {
        await Task.CompletedTask;

        var sheet = wb.CreateSheet("group-by-order");

        await this.TryWriteFirstLineLogoAsync(wb, sheet);

        var headerRow = sheet.CreateRow(sheet.LastRowNum + 1).SetStyle(GetPrimaryCellStyle(wb));

        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("商品");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("单价");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("数量");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("单品金额");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("收件人");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("收件人电话");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("收件地址");

        var sortedOrders = dto.GroupData.Orders.OrderByDescending(x => x.TotalPrice).ToArray();

        var orderIndex = 0;
        foreach (var order in sortedOrders)
        {
            var orderHeaderRow = sheet.CreateRow(sheet.LastRowNum + 1).SetStyle(GetPrimaryCellStyle(wb));
            orderHeaderRow.CreateCell(orderHeaderRow.LastCellNum + 1).SetCellValue($"order#{++orderIndex}");

            var itemIndex = 0;
            foreach (var m in order.Items)
            {
                ++itemIndex;
                var row = sheet.CreateRow(sheet.LastRowNum + 1);

                row.CreateCell(row.LastCellNum + 1).SetCellValue(GetItemGoodsName(m) ?? "unknown-goods");
                row.CreateCell(row.LastCellNum + 1).SetCellValue((double)m.UnitPrice);
                row.CreateCell(row.LastCellNum + 1).SetCellValue(m.Quantity);
                row.CreateCell(row.LastCellNum + 1).SetCellValue((double)m.TotalPrice);

                if (itemIndex == 1)
                {
                    row.CreateCell(row.LastCellNum + 1)
                        .SetStyle(GetPrimaryCellStyle(wb, bgColor: IndexedColors.Pink.Index))
                        .SetCellValue("to:");
                    WriteAddress(row, order.UserAddress);

                    /*
                     row.CreateCell(row.LastCellNum + 1).SetCellValue(order.UserAddress?.Name ?? "unknown-user");
                    row.CreateCell(row.LastCellNum + 1).SetCellValue(order.UserAddress?.Tel ?? "mobile-phone");
                    row.CreateCell(row.LastCellNum + 1)
                        .SetCellValue(order.UserAddress?.AddressDetail ?? "address-detail");
                     */

                    row.CreateCell(row.LastCellNum + 1)
                        .SetStyle(GetPrimaryCellStyle(wb, bgColor: IndexedColors.DarkYellow.Index))
                        .SetCellValue("from:");
                    WriteAddress(row, order.SpecificSender ? order.Sender : dto.GroupData.SharedSender);
                }
            }
        }

        sheet.CreateRow(sheet.LastRowNum + 1);
        var footerRow = sheet.CreateRow(sheet.LastRowNum + 1).SetStyle(GetPrimaryCellStyle(wb));

        footerRow.CreateCell(footerRow.LastCellNum + 1)
            .SetCellValue($"总单数：{sortedOrders.Length}");
        footerRow.CreateCell(footerRow.LastCellNum + 1)
            .SetCellValue($"总件数：{sortedOrders.SelectMany(x => x.Items).Sum(x => x.Quantity)}");
        footerRow.CreateCell(footerRow.LastCellNum + 1)
            .SetCellValue($"总金额：{sortedOrders.Sum(x => x.TotalPrice)}");
    }

    [NonAction]
    private async Task WriteGoodsSheetAsync(IWorkbook wb, OrderGroupDto dto)
    {
        var sheet = wb.CreateSheet("group-by-goods");

        await this.TryWriteFirstLineLogoAsync(wb, sheet);

        var headerRow = sheet.CreateRow(sheet.LastRowNum + 1).SetStyle(GetPrimaryCellStyle(wb));

        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("商品");
        headerRow.CreateCell(headerRow.LastCellNum + 1).SetCellValue("总数");

        var groups = dto.GroupData.Orders.SelectMany(x => x.Items)
            .GroupBy(x => GetItemGoodsName(x) ?? "#")
            .Select(x => new { x.Key, Items = x.ToArray() })
            .OrderByDescending(x => x.Items.Sum(d => d.Quantity))
            .ToArray();

        foreach (var g in groups)
        {
            var row = sheet.CreateRow(sheet.LastRowNum + 1);

            row.CreateCell(row.LastCellNum + 1).SetCellValue(g.Key);
            row.CreateCell(row.LastCellNum + 1).SetCellValue(g.Items.Sum(x => x.Quantity));
        }

        sheet.CreateRow(sheet.LastRowNum + 1);
        var footerRow = sheet.CreateRow(sheet.LastRowNum + 1).SetStyle(GetPrimaryCellStyle(wb));

        footerRow.CreateCell(footerRow.LastCellNum + 1)
            .SetCellValue($"总件数：{groups.SelectMany(x => x.Items).Sum(x => x.Quantity)}");
    }

    [HttpPost("export-excel")]
    public async Task<IActionResult> ExportExcelAsync([FromBody] OrderGroupDto dto)
    {
        dto.GroupData ??= new OrderGroupDataDto();
        foreach (var order in dto.GroupData.Orders)
        {
            order.Items ??= [];
            order.PaymentBills ??= [];
            order.RefundBills ??= [];
            foreach (var m in order.Items)
            {
                m.TotalPrice = m.UnitPrice * m.Quantity;
            }

            order.TotalPrice = order.Items.Sum(x => x.TotalPrice);
        }

        using var wb = new XSSFWorkbook();

        await this.WriteOrderSheetAsync(wb, dto);
        await this.WriteGoodsSheetAsync(wb, dto);

        using var ms = new MemoryStream();
        wb.Write(ms);
        await ms.FlushAsync();

        return File(fileContents: ms.ToArray(),
            fileDownloadName:
            $"order.group.{this.Clock.Now.ToString(format: this.Configuration.GetDateformatOrDefault())}.xlsx",
            contentType: "application/vnd.ms-excel");
    }
}