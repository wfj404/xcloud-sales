using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Payments;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Payments;

public class QueryOrderBillPagingInput : PagedRequest
{
    public string StoreId { get; set; }

    public string OrderId { get; set; }

    public string OrderNo { get; set; }

    public string PaymentMethod { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public bool? Paid { get; set; }

    public string PaymentTransactionId { get; set; }
    
    public bool? Refunded { get; set; }

    public string OutTradeNo { get; set; }

    public string OutRefundNo { get; set; }

    public string RefundId { get; set; }
}

public class PaymentBillDto : PaymentBill, IEntityDto<string>
{
    public OrderDto Order { get; set; }

    public RefundBillDto[] RefundBills { get; set; }
}

public class RefundBillDto : RefundBill, IEntityDto<string>
{
    public PaymentBillDto PaymentBill { get; set; }

    public OrderDto Order { get; set; }
}

public class MarkBillAsPayedInput : IEntityDto<string>
{
    public string Id { get; set; }
    public string PaymentMethod { get; set; }
    public string TransactionId { get; set; }
    public string NotifyData { get; set; }
    public DateTime? Time { get; set; }
}

public class QueryRefundBillPagingInput : PagedRequest
{
    public string RefundId { get; set; }
}

public class CreateRefundBillDto : IEntityDto
{
    public string BillId { get; set; }
    public decimal? PriceOrNull { get; set; }
    public string Description { get; set; }
}

public class MarkRefundBillAsRefundInput : IEntityDto<string>
{
    public string Id { get; set; }
    public string RefundId { get; set; }
    public string NotifyData { get; set; }
    public DateTime? Time { get; set; }
}