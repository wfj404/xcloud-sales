import { PaymentStatus, PaymentStatusList } from '@/utils/biz';
import { OrderDto } from '@/utils/swagger';
import { Badge } from 'antd';

export default function(props: { model: OrderDto }) {
  const { model } = props;

  const name =
    PaymentStatusList.find((x) => x.id == model.PaymentStatusId)?.name || '--';

  if (model.PaymentStatusId == PaymentStatus.Paid) {
    return <Badge text={name} status={'success'} />;
  }

  return <Badge text={name} status={'default'} />;
}
