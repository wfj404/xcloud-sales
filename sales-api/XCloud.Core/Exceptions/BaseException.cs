﻿using Volo.Abp;

namespace XCloud.Core.Exceptions;

public class ConfigException : BusinessException
{
    public ConfigException()
    {
        //
    }

    public ConfigException(string message) : base(message)
    {
        //
    }
}