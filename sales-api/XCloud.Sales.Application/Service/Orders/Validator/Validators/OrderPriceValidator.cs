﻿using XCloud.Core.Dto;
using XCloud.Core.Json;

namespace XCloud.Sales.Application.Service.Orders.Validator.Validators;

public class OrderPriceConditionParameter : IRuleParameter
{
    public decimal LimitedOrderAmount { get; set; }
}

[ExposeServices(typeof(IOrderValidator))]
public class OrderPriceValidator : OrderValidatorBase<OrderPriceConditionParameter>, IOrderValidator,
    ITransientDependency
{
    private readonly IJsonDataSerializer _jsonDataSerializer;

    public OrderPriceValidator(IJsonDataSerializer jsonDataSerializer)
    {
        _jsonDataSerializer = jsonDataSerializer;
    }

    public int Order => default;

    public string ValidatorType => OrderConditionValidatorTypes.OrderPrice;

    public async Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule)
    {
        await Task.CompletedTask;
        return rule.Description ?? string.Empty;
    }

    public async Task<OrderConditionValidationResponse> IsConditionMatchedAsync(
        OrderConditionCheckDto input,
        OrderValidatorRule rule)
    {
        var response = new OrderConditionValidationResponse();

        if (string.IsNullOrWhiteSpace(rule.RuleJson))
        {
            response.SetError("promotion config error");
            return response;
        }

        var param = ParseRequiredRuleParameter(rule.RuleJson);

        if (input.Order.TotalPrice < param.LimitedOrderAmount)
        {
            response.SetError("订单价格不满足促销条件");
            return response;
        }

        await Task.CompletedTask;

        return response;
    }

    public Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule)
    {
        return Task.FromResult(rule.ValidatorType == ValidatorType);
    }
}