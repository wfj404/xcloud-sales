# ui-components

## 通过swagger生成客户端代码：

https://www.npmjs.com/package/swagger-typescript-api

```shell
npx swagger-typescript-api -p http://localhost:6001/api/mall/swagger/swagger.json -o ./ -n swagger.ts --axios
```

## swagger codegen

https://swagger.io/tools/swagger-codegen/
https://github.com/swagger-api/swagger-codegen