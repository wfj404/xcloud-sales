﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Grades;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Grades;

public interface IGradeService : ISalesStringCrudAppService<Grade, GradeDto, PagedRequest>
{
    Task<StoreUserDto[]> AttachGradeAsync(StoreUserDto[] data);

    Task<Grade> GetGradeByUserIdAsync(int userId);

    Task SetUserGradeAsync(StoreUserGradeMappingDto dto);

    Task UpdateUserGradeStatusAsync(UpdateGradeStatusInput input);

    Task UpdateGradeUserCountAsync(string gradeId);

    Task<Grade[]> QueryAllGradesAsync();
}

public class GradeService : SalesStringCrudAppService<Grade, GradeDto, PagedRequest>, IGradeService
{
    private readonly ISalesRepository<Grade> _userGradeRepository;

    public GradeService(ISalesRepository<Grade> userGradeRepository) : base(userGradeRepository)
    {
        _userGradeRepository = userGradeRepository;
    }

    public async Task<StoreUserDto[]> AttachGradeAsync(StoreUserDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _userGradeRepository.GetDbContextAsync();

        var query = from mapping in db.Set<StoreUserGradeMapping>().AsNoTracking()
            join grade in db.Set<Grade>().AsNoTracking()
                on mapping.GradeId equals grade.Id
            select new { mapping, grade };

        query = query.Where(x => ids.Contains(x.mapping.UserId));

        var list = await query.ToArrayAsync();

        foreach (var m in data)
        {
            var grade = list.FirstOrDefault(x => x.mapping.UserId == m.Id)?.grade;

            if (grade == null)
                continue;

            m.Grade = grade.MapTo<Grade, GradeDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<Grade> GetGradeByUserIdAsync(int userId)
    {
        var db = await _userGradeRepository.GetDbContextAsync();

        var query = from mapping in db.Set<StoreUserGradeMapping>().AsNoTracking()
            join grade in db.Set<Grade>().AsNoTracking()
                on mapping.GradeId equals grade.Id
            select new { mapping, grade };

        query = query.Where(x => x.mapping.UserId == userId);

        var now = Clock.Now;
        query = query.Where(x => x.mapping.StartTime == null || x.mapping.StartTime <= now);
        query = query.Where(x => x.mapping.EndTime == null || x.mapping.EndTime >= now);

        query = query.OrderByDescending(x => x.mapping.CreationTime);

        var data = await query.FirstOrDefaultAsync();

        if (data == null)
            return null;

        return data.grade;
    }

    public async Task<Grade[]> QueryAllGradesAsync()
    {
        var db = await _userGradeRepository.GetDbContextAsync();

        var query = db.Set<Grade>().AsNoTracking();

        var data = await query.OrderByDescending(x => x.Sort).ToArrayAsync();

        return data;
    }

    public async Task UpdateGradeUserCountAsync(string gradeId)
    {
        var db = await _userGradeRepository.GetDbContextAsync();

        var userGrade = await db.Set<Grade>().FirstOrDefaultAsync(x => x.Id == gradeId);

        if (userGrade == null)
            return;

        var query = from mapping in db.Set<StoreUserGradeMapping>().AsNoTracking()
            join grade in db.Set<Grade>().AsNoTracking()
                on mapping.GradeId equals grade.Id
            select new { mapping, grade };
        var now = Clock.Now;
        query = query.Where(x => x.mapping.StartTime == null || x.mapping.StartTime <= now);
        query = query.Where(x => x.mapping.EndTime == null || x.mapping.EndTime >= now);

        userGrade.UserCount = await query.Select(x => x.grade.Id).CountAsync();

        await db.TrySaveChangesAsync();
    }

    protected override async Task SetFieldsBeforeInsertAsync(Grade entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
    }

    public override Task<Grade> InsertAsync(GradeDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        return base.InsertAsync(dto);
    }

    public override Task<Grade> UpdateAsync(GradeDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        return base.UpdateAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Grade entity, GradeDto dto)
    {
        await Task.CompletedTask;

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.Sort = dto.Sort;
    }

    public async Task UpdateUserGradeStatusAsync(UpdateGradeStatusInput input)
    {
        var db = await _userGradeRepository.GetDbContextAsync();

        var grade = await db.Set<Grade>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == input.Id);

        if (grade == null)
            throw new EntityNotFoundException(nameof(UpdateUserGradeStatusAsync));

        if (input.IsDeleted != null)
            grade.IsDeleted = input.IsDeleted.Value;

        await db.TrySaveChangesAsync();
    }

    public async Task SetUserGradeAsync(StoreUserGradeMappingDto dto)
    {
        var db = await _userGradeRepository.GetDbContextAsync();

        var set = db.Set<StoreUserGradeMapping>();

        var mappings = await set.Where(x => x.UserId == dto.UserId).ToArrayAsync();

        if (mappings.Any())
            set.RemoveRange(mappings);

        var entity = ObjectMapper.Map<StoreUserGradeMappingDto, StoreUserGradeMapping>(dto);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        set.Add(entity);

        await db.TrySaveChangesAsync();
    }
}