﻿using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/category")]
public class CategoryController : ShopBaseController
{
    private readonly ICategoryService _categoryService;

    public CategoryController(ICategoryService categoryService)
    {
        _categoryService = categoryService;
    }

    [HttpPost("tree")]
    public async Task<ApiResponse<AntDesignTreeNode<CategoryDto>[]>> QueryAntdTreeAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var data = await _categoryService.GetAllAsync();

        var dataDto = data.Select(x => ObjectMapper.Map<Category, CategoryDto>(x)).ToArray();

        await _categoryService.AttachPictureMetaAsync(dataDto);

        var tree = dataDto.BuildAntTreeV2(
            x => x.Id.ToString(),
            x => x.ParentId,
            x => x.Name).ToArray();

        tree = tree.SortAllLevelsV2();

        return new ApiResponse<AntDesignTreeNode<CategoryDto>[]>(tree);
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> SaveCategoryAsync([FromBody] Category dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        if (!string.IsNullOrWhiteSpace(dto.Id))
        {
            var entity = await this._categoryService.GetByIdAsync(dto.Id);
            entity = entity ?? throw new EntityNotFoundException(nameof(entity));

            await _categoryService.UpdateAsync(dto);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

            if (entity.ParentId != dto.ParentId)
            {
                await this._categoryService.AdjustParentIdAsync(dto.Id, dto.ParentId);
            }
        }
        else
        {
            await _categoryService.InsertAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public virtual async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        await _categoryService.DeleteAsync(dto);

        return new ApiResponse<object>();
    }
}