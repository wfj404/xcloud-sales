﻿using System.Security.Claims;
using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Application.Service;
using XCloud.AspNetMvc.Core;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;

namespace XCloud.Platform.Application.Authentication;

public class PrincipalInfo : ApiResponse<object>
{
    public string SubjectId { get; set; }

    public DateTime? TokenCreationTime { get; set; }

    public Claim[] Claims { get; set; }
}

public interface IHttpPrincipalService : IXCloudAppService
{
    [NotNull]
    Task<PrincipalInfo> GetPrincipalInfoAsync();
}

public class HttpPrincipalService : XCloudAppService, IHttpPrincipalService
{
    private readonly IWebHelper _webHelper;

    public HttpPrincipalService(IWebHelper webHelper)
    {
        _webHelper = webHelper;
    }

    public async Task<PrincipalInfo> GetPrincipalInfoAsync()
    {
        await Task.CompletedTask;

        var res = new PrincipalInfo();

        var principal = this._webHelper.RequiredHttpContext.User;
        if (principal == null || !principal.IsAuthenticated(out var claims))
        {
            res.SetError(nameof(principal));
            return res;
        }

        res.SubjectId = claims.GetSubjectId();
        res.TokenCreationTime = claims.GetCreationTime();
        res.Claims = claims;

        return res;
    }
}