using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;

namespace XCloud.Sales.Application.Configuration;

public static class SalesOptionExtension
{
    [NotNull]
    public static SalesOption GetSalesOptionOrDefault(this IConfiguration configuration)
    {
        var option = new SalesOption();

        var section = configuration.GetSection("Sales");

        if (section.Exists())
        {
            section.Bind(option);
        }

        return option;
    }
}