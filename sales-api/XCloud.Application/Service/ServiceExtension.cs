﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;
using XCloud.Application.Model;

namespace XCloud.Application.Service;

public static class ServiceExtension
{
    [NotNull]
    [ItemNotNull]
    public static async Task<TEntityDto> GetRequiredByIdAsync<TEntity, TEntityDto, TPagingRequest, TKey>(
        this IXCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, TKey> service, TKey id)
        where TEntity : class, IEntity<TKey>
        where TEntityDto : class, IEntityDto<TKey>
        where TPagingRequest : PagedRequest
    {
        var data = await service.GetByIdAsync(id);

        if (data == null)
            throw new EntityNotFoundException(nameof(GetRequiredByIdAsync));

        return data;
    }
}