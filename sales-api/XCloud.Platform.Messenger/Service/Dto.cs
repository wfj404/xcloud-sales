﻿using Volo.Abp.Application.Dtos;
using XCloud.Platform.Messenger.Domain.Messenger;

namespace XCloud.Platform.Messenger.Service;

public class OnlineStatusDto : OnlineStatus, IEntityDto<string>
{
    //
}

public class ServerInstanceDto : ServerInstance, IEntityDto<string>
{
    //
}