﻿using Hangfire;
using Hangfire.AspNetCore;
using Hangfire.Redis.StackExchange;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.Modularity;
using XCloud.Application.Redis;
using XCloud.Core.Configuration;

namespace XCloud.Application.Job;

public static class HangfireExtension
{
    internal static void AddHangfireJobProvider(this ServiceConfigurationContext context)
    {
        context.Services.AddHangfire((provider, config) =>
        {
            var redis = provider.GetRequiredService<RedisClient>();
            var loggerFactory = provider.GetRequiredService<ILoggerFactory>();
            var serviceScopeFactory = provider.GetRequiredService<IServiceScopeFactory>();

            var options = new RedisStorageOptions
            {
                Db = RedisDatabasePurpose.JobScheduler,
                Prefix = context.Services.GetAppName()
            };

            config.UseRedisStorage(nameOrConnectionString: redis.ConnectionString, options: options);

            config.UseLogProvider(new AspNetCoreLogProvider(loggerFactory));
            config.UseActivator(new AspNetCoreJobActivator(serviceScopeFactory));
            config.UseFilter(new AutomaticRetryAttribute { Attempts = 5 });
        });

        context.Services.AddHangfireServer();
    }

    /// <summary>
    /// dashboard ui不会自动提交token，所以必须使用cookie来实现登录。
    /// 方案：
    /// 1.在bearer token的基础上再加一个cookie authentication。
    /// 2.先登录admin拿到access token。
    /// 3.用token请求授权接口，写入临时cookie authentication（10分钟过期）
    /// 4.hangfire使用cookie authentication验证身份
    /// </summary>
    internal static void UseJobWorker(this IApplicationBuilder builder)
    {
        var options = new DashboardOptions
        {
            Authorization = new[] { new HangfireDashboardAuthorizationFilter() }
        };
        builder.UseHangfireDashboard("/internal/hangfire", options: options);
    }
}