import { Avatar, Image, Popover } from 'antd';
import { useState } from 'react';

import { getPictureSrc, PictureSrc } from '@/utils/biz';
import { StorageMetaDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { PictureOutlined } from '@ant-design/icons';

import XImage from './image';

export default ({
  data,
  count,
}: {
  data: StorageMetaDto[];
  count?: number;
}) => {
  const [previewIndex, _previewIndex] = useState<number | undefined>(undefined);

  const metas = data || [];
  const previewCount = count || 3;

  const pictureList = metas.map<PictureSrc>((x) => getPictureSrc(x));

  const renderPreview = (x: PictureSrc) => {
    return (
      <div style={{ maxWidth: 500 }}>
        <XImage src={x.small || ''} alt="" placeholder={<b>loading...</b>} />
      </div>
    );
  };

  if (isArrayEmpty(pictureList)) {
    return null;
  }

  return (
    <>
      <div style={{ display: 'none' }}>
        <Image.PreviewGroup
          preview={{
            current: Math.max(previewIndex || 0, 0),
            visible: previewIndex != undefined,
            onVisibleChange: (visible, prevValue, current) => {
              if (visible) {
                _previewIndex(current);
              } else {
                _previewIndex(undefined);
              }
            },
            onChange: (current) => {
              if (previewIndex == undefined) {
                return;
              }
              _previewIndex(current);
            },
          }}
        >
          {pictureList.map((x, i) => (
            <Image key={i} src={x.large || ''} alt="" />
          ))}
        </Image.PreviewGroup>
      </div>
      <div style={{}}>
        <Avatar.Group maxCount={previewCount} shape="square">
          {pictureList.map((x, i) => {
            return (
              <Popover
                key={i}
                title="预览"
                content={renderPreview(x)}
                placement="left"
              >
                <Avatar
                  style={{ backgroundColor: 'gray' }}
                  icon={<PictureOutlined />}
                  src={x.small || ''}
                  size={'large'}
                  onClick={() => {
                    _previewIndex(i);
                  }}
                />
              </Popover>
            );
          })}
        </Avatar.Group>
      </div>
    </>
  );
};
