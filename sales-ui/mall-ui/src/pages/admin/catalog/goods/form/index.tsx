import { Alert, Button, Card, message, Modal, Space, Spin, Tabs } from 'antd';
import React, { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { GoodsType, OrderStockDeductStrategy } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { GoodsDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XForm from './form';
import XMultiplePictureUploader from './goods_picture_form';
import XSpec from './spec';
import XValueAddedService from './value_added_service';

export default ({
  goodsId,
  ok,
  hide,
  show,
}: {
  goodsId?: string | null;
  ok: (d: GoodsDto) => void;
  hide: () => void;
  show: boolean;
}) => {
  const [goods, _goods] = useState<GoodsDto>({});
  const [tab, _tab] = useState<string>('1');
  const [loading, _loading] = useState(false);

  const empty = isStringEmpty(goods?.Id);

  const queryGoods = () => {
    if (isStringEmpty(goodsId)) {
      _goods({
        GoodsType: GoodsType.Normal,
        AfterSalesSupported: true,
        CommentSupported: true,
        PayAfterDeliveredSupported: true,
        PlaceOrderSupported: true,
        Published: true,
        StockDeductStrategy: OrderStockDeductStrategy.AfterPlaceOrder,
      });
      return;
    }

    _loading(true);

    apiClient.mallAdmin
      .goodsGetById({ Id: goodsId })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _goods(res.data.Data || {});
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const renderWithCheck = (com: React.ReactElement) => {
    if (empty) {
      return (
        <Card style={{ margin: 10 }}>
          <Alert message="请先保存商品再编辑此模块"></Alert>
        </Card>
      );
    }

    return <>{com}</>;
  };

  const save = (row: GoodsDto) => {
    row.Name = row.Name?.trim();
    if (isStringEmpty(row.Name)) {
      return;
    }

    _loading(true);

    apiClient.mallAdmin
      .goodsSaveGoods({
        ...row,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok && ok(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    show && _tab('1');
  }, [show]);

  useEffect(() => {
    queryGoods();
  }, [goodsId]);

  return (
    <>
      <Modal
        title="编辑商品"
        forceRender
        open={show}
        destroyOnClose
        onCancel={() => {
          hide && hide();
        }}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                let res = await apiClient.mallAdmin.goodsDelete({
                  Id: goods.Id,
                });
                handleResponse(res, () => {
                  hide();
                  ok({
                    ...goods,
                    IsDeleted: true,
                  });
                });
              }}
              hide={isStringEmpty(goods.Id)}
            />
            <Button
              type={'primary'}
              onClick={() => {
                save(goods);
              }}
            >
              保存
            </Button>
          </Space>
        }
        width={'95%'}
      >
        <Spin spinning={loading}>
          <Tabs
            activeKey={tab}
            onChange={(e) => {
              _tab(e);
            }}
            destroyInactiveTabPane
            items={[
              {
                key: '1',
                label: '基本信息',
                children: (
                  <>
                    <XForm
                      show={show}
                      data={goods}
                      ok={(d) => {
                        _goods(d || {});
                      }}
                    />
                    <div style={{ marginBottom: 10 }}>
                      <XMultiplePictureUploader
                        title="商品图片"
                        skus={goods.Skus}
                        data={goods.GoodsPictures || []}
                        ok={(x) => {
                          _goods({
                            ...goods,
                            GoodsPictures: x,
                          });
                        }}
                      />
                    </div>
                  </>
                ),
              },
              {
                key: '2',
                label: '规格明细',
                children: (
                  <>
                    {renderWithCheck(
                      <XSpec
                        model={goods}
                        ok={() => {
                          queryGoods();
                        }}
                      />,
                    )}
                  </>
                ),
              },
              {
                key: '3',
                label: '增值服务',
                children: (
                  <>
                    {renderWithCheck(
                      <XValueAddedService
                        model={goods}
                        ok={() => {
                          queryGoods();
                        }}
                      />,
                    )}
                  </>
                ),
              },
            ]}
          ></Tabs>
        </Spin>
      </Modal>
    </>
  );
};
