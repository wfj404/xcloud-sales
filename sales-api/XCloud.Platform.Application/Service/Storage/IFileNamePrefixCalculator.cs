﻿using System.Text;
using Volo.Abp.DependencyInjection;

namespace XCloud.Platform.Application.Service.Storage;

public interface IFileNamePrefixCalculator
{
    string[] GetFileNamePrefixArray(string fileName);

    string ConcatFileNamePrefix(string fileName, string joiner = default);
}

[ExposeServices(typeof(IFileNamePrefixCalculator))]
public class FileNamePrefixCalculator : IFileNamePrefixCalculator, ISingletonDependency
{
    private void CheckFileName(string fileName)
    {
        if (string.IsNullOrWhiteSpace(fileName))
            throw new ArgumentNullException(nameof(fileName));

        var notAllowedChars = new[] { '/', '\\' };

        foreach (var c in notAllowedChars)
        {
            if (fileName.IndexOf(c) >= 0)
                throw new ArgumentException(message: $"{c} not allowed in filename");
        }
    }

    /// <summary>
    /// 按照key设计一个规则把文件分散到不同的文件夹或者空间里
    /// </summary>
    public string[] GetFileNamePrefixArray(string fileName)
    {
        this.CheckFileName(fileName);

        var len = 3;

        fileName = fileName.Split('.').First();

        fileName = fileName.Replace(' '.ToString(), string.Empty);

        fileName = new StringBuilder().Append(fileName.Take(len).ToArray()).ToString();

        fileName = fileName.PadRight(len, '0');

        var prefix = fileName.Select(x => x.ToString()).ToArray();

        return prefix;
    }

    public string ConcatFileNamePrefix(string fileName, string joiner)
    {
        this.CheckFileName(fileName);

        if (string.IsNullOrWhiteSpace(joiner))
        {
            joiner = "/";
        }

        var arr = GetFileNamePrefixArray(fileName);

        arr = arr.Append(fileName).ToArray();

        var path = string.Join(joiner, arr);
        return path;
    }
}