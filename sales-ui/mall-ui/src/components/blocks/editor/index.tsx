import { Alert, Button, Card, Drawer, Space } from 'antd';
import { useEffect, useState } from 'react';

import XCommon from '@/components/blocks/editor/common';
import XContainerStyleEditor from '@/components/blocks/editor/container_style';
import XBlockEditorRender from '@/components/blocks/editor/render_block_editor';
import XPreviewRender from '@/components/blocks/render_block_item';
import { GlobalContainer } from '@/utils/utils';

import { BlockItem, BlockItemProps } from '../utils';

export default ({
  data,
  onChangeCommitted,
  onClose,
  title,
}: {
  onClose: () => void;
  title: string;
  data?: BlockItem<BlockItemProps>;
  onChangeCommitted: (d: BlockItem<BlockItemProps>) => void;
}) => {
  const [updated, _updated] = useState(false);
  const [block, _block] = useState<BlockItem<BlockItemProps> | undefined>(
    undefined,
  );

  const onBlockUpdated = (x: BlockItem<BlockItemProps>) => {
    _block(x);
    _updated(true);
  };

  useEffect(() => {
    _block(data);
    _updated(false);
  }, [data]);

  const renderEditorArea = () => {
    if (!block) {
      return <div>block is undefined</div>;
    }

    return (
      <>
        <Space direction={'vertical'} style={{ width: '100%' }}>
          <Card title={'通用配置'}>
            <XCommon
              item={block || {}}
              onChange={(e) => {
                onBlockUpdated({
                  ...e,
                });
              }}
            />
          </Card>
          <Card title={'容器样式'}>
            <XContainerStyleEditor
              sx={block.style || {}}
              onChange={(e) => {
                onBlockUpdated({
                  ...block,
                  style: e,
                });
              }}
            />
          </Card>
          <Card title={'组件参数'}>
            <XBlockEditorRender
              block={block}
              onBlockUpdated={(e) => {
                onBlockUpdated({
                  ...e,
                });
              }}
            />
          </Card>
          <Card title={'组件预览'}>
            <XPreviewRender block={block} />
          </Card>
        </Space>
      </>
    );
  };

  return (
    <>
      <Drawer
        open={data != undefined}
        onClose={() => onClose()}
        title={title}
        width={'70%'}
        extra={
          <Space direction={'horizontal'}>
            {updated && (
              <Alert
                message={'组件参数已经修改，点击应用才能在左侧视图生效'}
                type={'warning'}
              />
            )}
            <Button
              disabled={!updated}
              size={'large'}
              type={'primary'}
              danger
              onClick={() => {
                if (!block) {
                  return;
                }
                onChangeCommitted && onChangeCommitted(block);
                _updated(false);
                GlobalContainer.message?.success('应用成功');
              }}
            >
              应用更改
            </Button>
          </Space>
        }
      >
        {renderEditorArea()}
      </Drawer>
    </>
  );
};
