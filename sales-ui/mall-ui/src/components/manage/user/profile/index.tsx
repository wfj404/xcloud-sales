import { Col, Empty, Row, Skeleton } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { UserDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

import XAdmin from './admin';
import XMain from './main';

export default ({ userId }: { userId: string }) => {
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<UserDto>({});

  const queryProfile = (id: string) => {
    if (isStringEmpty(id)) {
      return;
    }
    _loading(true);
    apiClient.sys
      .manageUserGetProfileById({ Id: id })
      .then((res) => {
        _data(res.data.Data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    userId && queryProfile(userId);
  }, [userId]);

  if (loading) {
    return <Skeleton />;
  }
  if (isStringEmpty(data?.Id)) {
    return <Empty />;
  }
  return (
    <>
      <Row gutter={10}>
        <Col span={12}>
          <XMain user={data} />
        </Col>
        <Col span={12}>
          <XAdmin user={data} />
        </Col>
      </Row>
    </>
  );
};
