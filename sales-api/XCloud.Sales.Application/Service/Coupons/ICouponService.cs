﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Coupons;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Mapping;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Coupons;

public interface ICouponService : ISalesAppService
{
    Task<CouponDto> GetByIdAsync(string id);

    Task DeleteByIdAsync(string id);

    Task<Coupon> InsertAsync(CouponDto dto);

    Task<CouponDto[]> AttachStoresAsync(CouponDto[] data);

    Task<CouponDto[]> AttachStatisticsAsync(CouponDto[] data);

    Task<int> ActiveCouponCountAsync();

    Task<PagedResponse<CouponDto>> QueryPagingAsync(QueryCouponPagingInput dto);
}

public class CouponService : SalesAppService, ICouponService
{
    private readonly ISalesRepository<Coupon> _couponRepository;
    private readonly IStoreMappingService _storeMappingService;

    public CouponService(ISalesRepository<Coupon> couponRepository,
        IStoreMappingService storeMappingService)
    {
        _storeMappingService = storeMappingService;
        _couponRepository = couponRepository;
    }

    public async Task<CouponDto> GetByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            return default;

        var entity = await this._couponRepository.FirstOrDefaultAsync(x => x.Id == id);

        return entity?.MapTo<Coupon, CouponDto>(ObjectMapper);
    }

    public Task DeleteByIdAsync(string id)
    {
        return this._couponRepository.SoftDeleteAsync(id, this.Clock.Now);
    }

    public async Task<CouponDto[]> AttachStatisticsAsync(CouponDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _couponRepository.GetDbContextAsync();

        var query = db.Set<CouponUserMapping>().AsNoTracking()
            .Where(x => ids.Contains(x.CouponId));

        var datalist = await query
            .GroupBy(x => new { x.CouponId, x.IsUsed })
            .Select(x => new { x.Key.CouponId, x.Key.IsUsed, Count = x.Count() })
            .ToArrayAsync();

        foreach (var m in data)
        {
            var items = datalist.Where(x => x.CouponId == m.Id).ToArray();

            m.IssuedAmount = items.Sum(x => x.Count);
            m.UsedAmount = items.Where(x => x.IsUsed).Sum(x => x.Count);
        }

        return data;
    }

    public async Task<CouponDto[]> AttachStoresAsync(CouponDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Where(x => x.LimitedToStore).Ids();

        if (ids.Any())
        {
            var db = await _couponRepository.GetDbContextAsync();

            var mappingQuery = db.Set<CommonEntityMapping>().AsNoTracking()
                .WhereMappingFromTo<CommonEntityMapping, Coupon, Store>(this.CommonUtils);

            var query = from mapping in mappingQuery
                join store in db.Set<Store>().AsNoTracking()
                    on mapping.TargetId equals store.Id
                select new { mapping, store };

            query = query.Where(x => ids.Contains(x.mapping.EntityId));

            var list = await query.ToArrayAsync();

            foreach (var m in data)
            {
                var stores = list.Where(x => x.mapping.EntityId == m.Id).Select(x => x.store).ToArray();

                m.Stores = stores.MapArrayTo<Store, StoreDto>(ObjectMapper);
            }
        }

        return data;
    }

    public async Task<int> ActiveCouponCountAsync()
    {
        var db = await _couponRepository.GetDbContextAsync();

        var query = db.Set<Coupon>().AsNoTracking();

        var now = Clock.Now;

        query = query.Where(x => x.IsActive);

        query = query.FilterByTimeRange(now);

        var count = await query.CountAsync();

        return count;
    }

    public async Task<Coupon> InsertAsync(CouponDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException(nameof(dto.Name));

        if (dto.Price <= 0)
            throw new UserFriendlyException(nameof(dto.Price));

        var entity = dto.MapTo<CouponDto, Coupon>(this.ObjectMapper);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        await this._couponRepository.InsertAsync(entity);

        return entity;
    }

    protected async Task SetFieldsBeforeUpdateAsync(Coupon entity, CouponDto dto)
    {
        await Task.CompletedTask;

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.Price = dto.Price;
        entity.StartTime = dto.StartTime;
        entity.EndTime = dto.EndTime;
        entity.OrderConditionRulesJson = dto.OrderConditionRulesJson;
        entity.ExpiredInDays = dto.ExpiredInDays;
        entity.Amount = dto.Amount;
        entity.AccountIssuedLimitCount = dto.AccountIssuedLimitCount;
        entity.LimitedToStore = dto.LimitedToStore;
        entity.IsActive = dto.IsActive;

        throw new NotImplementedException();
    }

    public async Task<PagedResponse<CouponDto>> QueryPagingAsync(QueryCouponPagingInput dto)
    {
        var db = await this._couponRepository.GetDbContextAsync();

        var query = db.Set<Coupon>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
        {
            query = _storeMappingService.ApplyStoreMappingFilter(db, query, new[] { dto.StoreId });
        }

        if (dto.AvailableFor != null)
        {
            query = query.Where(x => x.IsActive);
            query = query.Where(x => x.Amount > 0);
            query = query.FilterByTimeRange(dto.AvailableFor.Value);
        }

        var count = await query.CountOrDefaultAsync(dto);

        var list = await query
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id).ToArrayAsync();

        var data = list.MapArrayTo<Coupon, CouponDto>(ObjectMapper);

        return new PagedResponse<CouponDto>(data, dto, count);
    }
}