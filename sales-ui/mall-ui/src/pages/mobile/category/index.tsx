import { useRequest, useSize, useTitle } from 'ahooks';
import { sortBy } from 'lodash-es';
import qs from 'query-string';
import React, { useEffect, useRef, useState } from 'react';
import { useLocation, useSnapshot } from 'umi';

import XImage from '@/components/common/image';
import XLoading from '@/components/common/loading/dots';
import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { resolveUrl } from '@/utils/storage';
import { CategoryDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Container } from '@mui/material';
import Box from '@mui/material/Box';

import GoodsListByCategory from './goods_list';
import XSimpleHeader from './header';
import SelectedCategoryInfo from './selected_category';
import XSideBar from './side_bar';

export default function () {
  useTitle('类目');

  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const side_ref = useRef<HTMLDivElement>(null);
  const side_size = useSize(side_ref);

  const container_ref = useRef<HTMLDivElement>(null);
  const [container_left, _container_left] = useState<number>(0);
  const window_size = useSize(window.document.body);

  useEffect(() => {
    _container_left(container_ref.current?.getBoundingClientRect().left || 0);
  }, [window_size, container_ref]);

  const queryCategoryRequest = useRequest(
    apiClient.mall.categoryStoreRootCategory,
    {
      manual: true,
    },
  );

  const data: CategoryDto[] = queryCategoryRequest.data?.data?.Data || [];

  const [selectedId, _selectedId] = useState<string | undefined | null>(
    undefined,
  );
  const [subCategoryId, _subCategoryId] = useState<string | undefined | null>(
    undefined,
  );

  const selectedRootModel = data.find((x) => x.Id == selectedId);

  const finalFilteredCategoryId = subCategoryId || selectedId;

  useEffect(() => {
    const id = qs.parse(location.search)?.cat as string;
    const selectedParent = sortBy(data, (x) => (x.Id == id ? -1 : 0)).at(0);
    if (selectedParent && selectedParent.Id != selectedId) {
      _selectedId(selectedParent?.Id);
    }
  }, [data]);

  useEffect(() => {
    queryCategoryRequest.run({});
  }, []);

  useEffect(() => {
    window && window.scrollTo({ top: 0, left: 0 });
  }, [finalFilteredCategoryId]);

  useEffect(() => {
    const originColor = document.body.style.backgroundColor;
    document.body.style.backgroundColor = 'white';
    return () => {
      document.body.style.backgroundColor = originColor;
    };
  }, []);

  const renderCategoryPicture = () => {
    const url = resolveUrl(selectedRootModel?.Picture, {
      width: 300,
    });
    if (!url) {
      return null;
    }

    return (
      <Box sx={{ mb: 1 }}>
        <XImage
          src={url}
          alt={selectedRootModel?.Name || ''}
          fit={'contain'}
          width={'100%'}
        />
      </Box>
    );
  };

  const renderContent = () => {
    if (queryCategoryRequest.loading) {
      return <XLoading />;
    }

    return (
      <>
        <div
          ref={side_ref}
          style={{
            position: 'fixed',
            top: app.headerHeight,
            bottom: app.bottomHeight,
            left:
              container_left ||
              container_ref.current?.getBoundingClientRect().left ||
              0,
            overflow: 'hidden',
            zIndex: 1,
          }}
        >
          <XSideBar
            selectedId={selectedId || undefined}
            data={data}
            onSelect={(e) => {
              _selectedId(e);
            }}
          />
        </div>
        <div style={{ marginLeft: `${(side_size?.width || 0) + 5}px` }}>
          {renderCategoryPicture()}

          {selectedRootModel == null || (
            <SelectedCategoryInfo
              model={selectedRootModel}
              selectedSubCategoryId={subCategoryId}
              onSubCategorySelect={(e) => {
                _subCategoryId(e);
              }}
            />
          )}

          {isStringEmpty(finalFilteredCategoryId) || (
            <GoodsListByCategory categoryId={finalFilteredCategoryId} />
          )}
        </div>
      </>
    );
  };

  return (
    <>
      <XSimpleHeader>
        <Container disableGutters maxWidth="sm">
          <div ref={container_ref}>{renderContent()}</div>
        </Container>
      </XSimpleHeader>
    </>
  );
}
