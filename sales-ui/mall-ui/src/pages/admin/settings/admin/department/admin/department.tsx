import { useRequest } from 'ahooks';
import { Button, message, Modal, Space, Tag } from 'antd';
import { useEffect, useState } from 'react';

import XDepartmentSelector from '@/components/manage/department_selector';
import { apiClient } from '@/utils/client';
import { AdminDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({ data, ok }: { data: AdminDto; ok: () => void }) => {
  const [show, _show] = useState(false);
  const [ids, _ids] = useState<string[]>([]);

  const set_department_request = useRequest(
    apiClient.sys.departmentSetAdminDepartments,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          message.success('设置成功');
          _show(false);
          ok();
        });
      },
    },
  );

  useEffect(() => {
    _ids(data.Departments?.map((item) => item.Id || '') || []);
  }, [data]);

  return (
    <>
      <Modal
        open={show}
        title="修改部门"
        onOk={() => {
          set_department_request.run({
            Key: data.Id,
            Value: ids,
          });
        }}
        onCancel={() => {
          _show(false);
        }}
      >
        <XDepartmentSelector
          style={{
            width: '100%',
          }}
          value={ids.at(0)}
          onChange={(e) => {
            _ids([e?.Id || ''].filter((x) => !isStringEmpty(x)));
          }}
        />
      </Modal>
      <Space direction="horizontal">
        {data.Departments?.map((x, i) => {
          return <Tag key={i}>{x.Name}</Tag>;
        })}
        <Button type="link" onClick={() => _show(true)}>
          修改
        </Button>
      </Space>
    </>
  );
};
