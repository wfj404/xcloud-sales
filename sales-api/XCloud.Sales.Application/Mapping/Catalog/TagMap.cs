﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class TagMap : SalesEntityTypeConfiguration<Tag>
{
    public override void Configure(EntityTypeBuilder<Tag> builder)
    {
        builder.ToTable("sales_tag");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeleteEntity();

        builder.Property(x => x.Id).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(1000);
        builder.Property(x => x.Alert).HasMaxLength(1000);
        builder.Property(x => x.Link).HasMaxLength(1000);

        base.Configure(builder);
    }
}