using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Volo.Abp.Application.Dtos;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Apm;
using XCloud.Application.DistributedLock;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Roles;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Service.Users;

public class PlatformData : IEntityDto
{
    public UserInfo SuperUser { get; set; }

    public class UserInfo : IEntityDto
    {
        public string UserName { get; set; }

        public string AvatarUrl { get; set; }
    }
}

[UnitOfWork]
[ExposeServices(typeof(EnsureSuperUserCreatedService))]
[Apm(Disabled = true)]
public class EnsureSuperUserCreatedService : PlatformAppService
{
    private readonly IPlatformRepository<User> _repository;
    private readonly AccountUtils _accountUtils;
    private readonly IAdminAccountService _adminAccountService;
    private readonly IUserService _userService;
    private readonly IUserAccountService _userAccountService;
    private readonly IRoleService _roleService;
    private readonly IAdminRoleService _adminRoleService;
    private readonly IWebHostEnvironment _webHostEnvironment;

    public EnsureSuperUserCreatedService(
        IAdminAccountService adminAccountService,
        IUserAccountService userAccountService,
        IPlatformRepository<User> repository,
        AccountUtils accountUtils,
        IRoleService roleService,
        IAdminRoleService adminRoleService,
        IUserService userService,
        IWebHostEnvironment webHostEnvironment)
    {
        _adminAccountService = adminAccountService;
        _userAccountService = userAccountService;
        _repository = repository;
        _accountUtils = accountUtils;
        _roleService = roleService;
        _adminRoleService = adminRoleService;
        _userService = userService;
        _webHostEnvironment = webHostEnvironment;
    }

    public virtual async Task EnsureSuperUserCreatedAsync()
    {
        var dataPath = Path.Combine(_webHostEnvironment.ContentRootPath, "data", "platform.json");
        if (!File.Exists(dataPath))
        {
            Logger.LogInformation("platform data not exist , skip restore");
            return;
        }

        var json = await File.ReadAllTextAsync(dataPath);

        var data = JsonDataSerializer.DeserializeFromString<PlatformData>(json);

        if (data == null)
            throw new AbpException("failed to parse platform.json");

        if (data.SuperUser == null)
        {
            Logger.LogInformation("super user is empty,skip restore");
            return;
        }

        await using var redLock = await DistributedLockProvider.CreateRequiredLockAsync(
            resource:
            $"ensure.super.user.created.username={data.SuperUser.UserName}",
            expiryTime: TimeSpan.FromSeconds(30));

        //添加测试数据
        var user = await EnsureUserCreatedAsync(data.SuperUser);
        //admin identity
        var admin = await TryCreateAdminIdentityAsync(user);
        //role
        var role = await EnsureSuperRoleCreatedAsync();
        //bind role
        await TryBindRoleAsync(admin, role);

        Logger.LogInformation(nameof(EnsureSuperUserCreatedService));
    }

    private async Task<User> EnsureUserCreatedAsync(PlatformData.UserInfo dto)
    {
        var normalizeIdentityName = _accountUtils.NormalizeIdentityName(dto.UserName);

        var db = await _repository.GetDbContextAsync();

        var user = await db.Set<User>()
            .IgnoreQueryFilters().AsNoTracking()
            .FirstOrDefaultAsync(x => x.IdentityName == normalizeIdentityName);

        if (user == null)
        {
            user = await _userAccountService.CreateUserAccountV1Async(new IdentityNameDto(dto.UserName));

            await RequiredCurrentUnitOfWork.SaveChangesAsync();

            //update pwd
            await _userAccountService.SetPasswordAsync(user.Id, "123");

            //update profile
            var profile = user.MapTo<User, UserDto>(ObjectMapper);
            profile.NickName = profile.IdentityName;
            profile.Avatar = dto.AvatarUrl;
            await _userService.UpdateProfileAsync(profile);
        }

        await _userAccountService.UpdateStatusAsync(new UpdateUserStatusDto
        {
            Id = user.Id,
            IsActive = true,
            IsDeleted = false
        });

        return user;
    }

    private async Task<Admin> TryCreateAdminIdentityAsync(User user)
    {
        var db = await _repository.GetDbContextAsync();

        var admin = await db.Set<Admin>().IgnoreQueryFilters().AsNoTracking()
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync(x => x.UserId == user.Id);

        if (admin == null)
        {
            admin = await _adminAccountService.GetOrCreateByUserIdAsync(user.Id);

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        await _adminAccountService.UpdateStatusAsync(new UpdateAdminStatusDto
        {
            Id = admin.Id,
            IsActive = true,
            IsSuperAdmin = true
        });

        return admin;
    }

    private async Task<Domain.Security.Role> EnsureSuperRoleCreatedAsync()
    {
        var superRoleName = "super-role";

        var db = await _repository.GetDbContextAsync();

        var role = await db.Set<Domain.Security.Role>().IgnoreQueryFilters().AsNoTracking()
            .FirstOrDefaultAsync(x => x.Name == superRoleName);

        if (role == null)
        {
            role = new Domain.Security.Role
            {
                Name = superRoleName,
                Description = "created by system,do not modify it"
            };
            role = await _roleService.InsertAsync(ObjectMapper.Map<Domain.Security.Role, RoleDto>(role));

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        await _roleService.SetRolePermissionsAsync(role.Id, new[] { "*" });

        return role;
    }

    private async Task TryBindRoleAsync(Admin admin, Domain.Security.Role role)
    {
        var adminRoles =
            await _adminRoleService.QueryByAdminIdAsync(admin.Id, new CacheStrategy { Source = true });

        var ids = adminRoles.Ids();

        if (!ids.Contains(role.Id))
        {
            ids = ids.Append(role.Id).Distinct().ToArray();

            await _adminRoleService.SetAdminRolesAsync(admin.Id, ids);
        }
    }
}