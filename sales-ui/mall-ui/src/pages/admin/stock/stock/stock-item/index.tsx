import { apiClient } from '@/utils/client';
import { PagedResponse } from '@/utils/biz';
import { QueryWarehouseStockItemInput, StockItemDto } from '@/utils/swagger';
import { Card, message, Progress, Table, Tag, Tooltip } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import XSearchForm from './search_form';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<StockItemDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QueryWarehouseStockItemInput>({
    Page: 1,
  });

  const queryList = (q: QueryWarehouseStockItemInput) => {
    _loading(true);
    apiClient.mallAdmin
      .stockItemStockItemPaging(q)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data || {});
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnProps<StockItemDto>[] = [
    {
      title: '批次号',
      render: (d, x) => <b>{x.Stock?.No || '--'}</b>,
    },
    {
      title: '商品',
      render: (d, x) => {
        return <span>{`${x.Goods?.Name || '--'}/${x.Sku?.Name || '--'}`}</span>;
      },
    },
    {
      title: '仓库',
      render: (d, x) => {
        return <span>{x.Stock?.Warehouse?.Name || '--'}</span>;
      },
    },
    {
      title: '供货商',
      render: (d, x) => {
        return <span>{x.Stock?.Supplier?.Name || '--'}</span>;
      },
    },
    {
      title: '采购单价',
      render: (d, x) => {
        return <span>{x.Price}</span>;
      },
    },
    {
      title: '采购数量',
      render: (d, x) => {
        return <span>{x.Quantity}</span>;
      },
    },
    {
      title: '库存结余',
      render: (d, x) => {
        if ((x.DeductQuantity || 0) >= (x.Quantity || 0)) {
          return <Tag color='red'>已无库存</Tag>;
        }
        if (x.Quantity && x.Quantity > 0) {
        } else {
          return <span>采购数量异常</span>;
        }
        const surplus = (x.Quantity || 0) - (x.DeductQuantity || 0);
        let percent = (surplus / (x.Quantity || 0)) * 100;
        if (percent > 100) {
          return <span>计算错误</span>;
        }
        return (
          <Tooltip title={`剩余${surplus}`}>
            <Progress percent={percent} status='active' />
          </Tooltip>
        );
      },
    },
  ];

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card title='库存明细'>
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              const q: QueryWarehouseStockItemInput = {
                ...query,
                Page: e,
              };
              _query(q);
              queryList(q);
            },
          }}
        />
      </Card>
    </>
  );
};
