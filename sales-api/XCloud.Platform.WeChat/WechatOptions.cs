﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Platform.WeChat;

public class WechatSettings : IEntityDto
{
    public WechatMpOption MpOption { get; set; }

    public WechatOpenOption OpenOption { get; set; }

    public WechatPaymentOption PaymentOption { get; set; }
}

public abstract class WechatOptionBase
{
    public bool Empty => string.IsNullOrWhiteSpace(this.AppId);

    public string AppId { get; set; }

    public string AppSecret { get; set; }
}

[TypeIdentityName("wechat-mp-settings")]
public class WechatMpOption : WechatOptionBase, ISettings
{
    //
}

[TypeIdentityName("wechat-open-settings")]
public class WechatOpenOption : WechatOptionBase, ISettings
{
    //
}

/// <summary>
/// WeChatPay 配置选项
/// </summary>
[TypeIdentityName("wechat-payment-settings")]
public class WechatPaymentOption : ISettings
{
    public bool Empty => string.IsNullOrWhiteSpace(this.MchId);

    /// <summary>
    /// 商户号
    /// </summary>
    public string MchId { get; set; }

    /// <summary>
    /// 商户APIv3密钥
    /// </summary>
    public string ApiV3Key { get; set; }

    public string MerchantCertificatePrivateKey { get; set; }

    public string MerchantCertificateSerialNumber { get; set; }
}