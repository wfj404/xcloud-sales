import {
  Button,
  Card,
  Checkbox,
  Empty,
  Input,
  message,
  Space,
  Timeline,
} from 'antd';
import React, { useEffect, useState } from 'react';

import RenderTime from '@/components/common/time/render_time';
import { apiClient } from '@/utils/client';
import { OrderDto, OrderNoteDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty, isStringEmpty } from '@/utils/utils';

const App = (props: { model: OrderDto }) => {
  const { model } = props;
  const [loading, _loading] = useState<boolean>(false);
  const [data, _data] = useState<OrderNoteDto[]>([]);

  const [formdata, _formdata] = useState<OrderNoteDto>({});
  const [loadingSave, _loadingSave] = useState<boolean>(false);

  const [displayToUser, _displayToUser] = useState(false);
  const [loadingDeleteId, _loadingDeleteId] = useState<
    number | undefined | null
  >(undefined);

  let filteredData = data;
  if (displayToUser) {
    filteredData = filteredData.filter((x) => x.DisplayToUser);
  }

  const deleteNote = (id: number) => {
    if (!confirm('确定删除当前记录？')) {
      return;
    }

    _loadingDeleteId(id);
    apiClient.mallManager
      .orderNoteDelete({ Id: id })
      .then((res) => {
        handleResponse(res, () => {
          message.success('删除成功');
          queryNotes();
        });
      })
      .finally(() => _loadingDeleteId(undefined));
  };

  const saveNotes = () => {
    if (isStringEmpty(formdata.Note)) {
      message.error('请输入内容');
      return;
    }
    _loadingSave(true);
    apiClient.mallManager
      .orderNoteAddOrderNote({
        ...formdata,
        OrderId: model.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          _formdata({
            ...formdata,
            Note: '',
            DisplayToUser: false,
          });
          queryNotes();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const queryNotes = () => {
    if (model && model.Id) {
    } else {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .orderNoteQueryOrderNotes({ OrderId: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryNotes();
  }, [model]);

  const renderTimeline = () => {
    if (isArrayEmpty(filteredData)) {
      return <Empty />;
    }

    return (
      <>
        <div
          style={{
            marginBottom: 20,
            marginTop: 20,
          }}
        >
          <Space direction={'horizontal'}>
            <Checkbox
              value={displayToUser}
              onChange={(e) => {
                _displayToUser(e.target.checked);
              }}
            />
            <span>只展示用户可见</span>
          </Space>
        </div>
        <Timeline
          items={filteredData.map((x) => ({
            color: 'green',
            pending: false,
            children: (
              <>
                <p>
                  <RenderTime timeStr={x.CreationTime} />
                </p>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <p>{x.Note}</p>
                  <Button
                    size={'small'}
                    danger
                    loading={loadingDeleteId == x.Id}
                    type={'link'}
                    onClick={() => {
                      deleteNote(x.Id || 0);
                    }}
                  >
                    删除
                  </Button>
                </div>
              </>
            ),
          }))}
        ></Timeline>
      </>
    );
  };

  return (
    <>
      <Card
        title="订单处理记录"
        loading={loading}
        style={{
          marginBottom: 10,
        }}
      >
        <div style={{ marginBottom: 10 }}>
          <Input.TextArea
            style={{ marginBottom: 10 }}
            value={formdata.Note || ''}
            onChange={(e) => {
              _formdata({
                ...formdata,
                Note: e.target.value,
              });
            }}
            maxLength={100}
            placeholder="请输入内容"
          />
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          >
            <Checkbox
              checked={formdata.DisplayToUser}
              onChange={(e) => {
                _formdata({
                  ...formdata,
                  DisplayToUser: e.target.checked,
                });
              }}
              style={{ marginRight: 10 }}
            >
              展示给买家
            </Checkbox>
            <Button
              type="primary"
              loading={loadingSave}
              disabled={isStringEmpty(formdata.Note)}
              onClick={() => {
                saveNotes();
              }}
            >
              保存
            </Button>
          </div>
        </div>

        {renderTimeline()}
      </Card>
    </>
  );
};

export default App;
