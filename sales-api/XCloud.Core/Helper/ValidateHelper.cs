﻿namespace XCloud.Core.Helper;

public static class ValidateHelper
{
    public static bool RoutePathEqual(string path1, string path2)
    {
        if (path1 == null || path2 == null)
            return false;

        if (path1 == path2)
            return true;

        path1 = path1.Split('/').ToArray().ConcatUrl();
        path2 = path2.Split('/').ToArray().ConcatUrl();

        return string.Equals(path1, path2, StringComparison.OrdinalIgnoreCase);
    }

    /// <summary>
    /// string dict都是list
    /// </summary>
    public static bool IsNotEmptyCollection<T>(IEnumerable<T> list)
    {
        /*
        IEnumerable<char> x = "fasdfas";
        IEnumerable<string> y = new List<string>();
        IEnumerable<KeyValuePair<string, string>> d = new Dictionary<string, string>();
        */

        return list != null && list.Any();
    }

    /// <summary>
    /// 为空
    /// </summary>
    public static bool IsEmptyCollection<T>(IEnumerable<T> list) => !IsNotEmptyCollection(list);
}