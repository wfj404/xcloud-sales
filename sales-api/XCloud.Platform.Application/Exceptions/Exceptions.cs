using Volo.Abp.Authorization;

namespace XCloud.Platform.Application.Exceptions;

public class PermissionRequiredException : AbpAuthorizationException
{
    public PermissionRequiredException(string msg = null) : base(message: msg ?? "没有权限")
    {
        //
    }
}