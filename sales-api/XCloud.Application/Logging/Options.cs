﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;

namespace XCloud.Application.Logging;

public class DiskLoggingOption : IEntityDto
{
    public string BasePath { get; set; }
}

public class ElasticSearchLoggingOption : IEntityDto
{
    public bool Enabled { get; set; } = false;
}

public class LoggingOption : IEntityDto
{
    public bool PrintLog { get; set; } = false;

    private DiskLoggingOption _diskLoggingOption;

    [NotNull]
    public DiskLoggingOption Disk
    {
        get => _diskLoggingOption ??= new DiskLoggingOption();
        set => _diskLoggingOption = value;
    }

    private ElasticSearchLoggingOption _elasticSearchLoggingOption;

    [NotNull]
    public ElasticSearchLoggingOption ElasticSearch
    {
        get => _elasticSearchLoggingOption ??= new ElasticSearchLoggingOption();
        set => _elasticSearchLoggingOption = value;
    }
}