﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.GiftCards;

namespace XCloud.Sales.Application.Mapping.GiftCards;

public class GiftCardMap : SalesEntityTypeConfiguration<GiftCard>
{
    public override void Configure(EntityTypeBuilder<GiftCard> builder)
    {
        base.Configure(builder);

        builder.ToTable("sales_gift_card");
        
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeleteEntity();

        builder.Property(x => x.Amount).HasPrecision(18, 2);
        builder.Property(x => x.EndTime);

        builder.Property(x => x.Used);
        builder.Property(x => x.UserId);
        builder.Property(x => x.UsedTime);

        builder.Property(x => x.IsActive);
    }
}