import {
    BankOutlined, CarOutlined, CarryOutOutlined, CreditCardOutlined, CustomerServiceOutlined,
    FolderOpenOutlined, GroupOutlined, HomeOutlined, PayCircleOutlined, SendOutlined,
    SettingOutlined, ShoppingCartOutlined, TeamOutlined
} from '@ant-design/icons';
import { MenuDataItem } from '@ant-design/pro-layout';

const routes: Array<MenuDataItem> = [
  {
    name: '在售商品',
    path: '/store-manage/sku',
    icon: <ShoppingCartOutlined />,
    children: [
      {
        name: '在售商品',
        path: '/store-manage/sku',
        icon: <FolderOpenOutlined />,
      },
    ],
  },
  {
    name: '订单',
    path: '/store-manage/order',
    key: 'order',
    icon: <CreditCardOutlined />,
    children: [
      {
        name: '订单列表',
        path: '/store-manage/order/list',
        icon: <CarryOutOutlined />,
      },
      {
        name: '订单评论',
        path: '/store-manage/order/review',
        icon: <CustomerServiceOutlined />,
      },
    ],
  },
  {
    name: '财务',
    path: '/store-manage/bill',
    key: 'finance',
    icon: <BankOutlined />,
    children: [
      {
        name: '支付查询',
        path: '/store-manage/bill/payment',
        icon: <PayCircleOutlined />,
      },
    ],
  },
  {
    name: '交付',
    path: '/store-manage/shipping',
    key: 'shipping',
    icon: <SendOutlined />,
    children: [
      {
        name: '配送',
        path: '/store-manage/shipping/delivery',
        icon: <CarOutlined />,
      },
      {
        name: '自提',
        path: '/store-manage/shipping/pickup',
        icon: <ShoppingCartOutlined />,
      },
    ],
  },
  {
    name: '设置',
    path: '/store-manage/settings',
    key: 'mall-settings',
    icon: <SettingOutlined />,
    children: [
      {
        name: '门店设置',
        path: '/store-manage/settings/common',
        icon: <HomeOutlined />,
      },
      {
        name: '成员',
        path: '/store-manage/settings/manager',
        icon: <TeamOutlined />,
      },
      {
        name: '角色',
        path: '/store-manage/settings/role',
        icon: <GroupOutlined />,
      },
      {
        name: '配送方式',
        path: '/store-manage/settings/shipping',
        icon: <CarOutlined />,
      },
    ],
  },
];

export default routes;
