using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace XCloud.Platform.Messenger.Abstraction.WebSocketImpl;

public static class WebsocketExtension
{
    public static void TryStartMessengerServer(this IApplicationBuilder app)
    {
        //start server
        using var s = app.ApplicationServices.CreateScope();

        s.ServiceProvider.GetRequiredService<IMessengerServer>().StartAsync();
    }

    public static void UseWebSocketEndpoint(this IApplicationBuilder app, string path)
    {
        app.UseMiddleware<WebsocketEndpointMiddleware>(args: new object[] { path });
    }

    public static async Task SendTextAsync(this WebSocket ws, string text, Encoding encoding,
        CancellationToken? cancellationToken = default)
    {
        var bs = encoding.GetBytes(text);

        await ws.SendAsync(
            buffer: new ArraySegment<byte>(bs),
            WebSocketMessageType.Text,
            endOfMessage: true,
            cancellationToken: cancellationToken ?? CancellationToken.None);
    }

    public static async Task NormalCloseAsync(this WebSocket ws, string description = default,
        CancellationToken? cancellationToken = default)
    {
        await ws.CloseAsync(WebSocketCloseStatus.NormalClosure,
            statusDescription: description ?? nameof(NormalCloseAsync),
            cancellationToken ?? CancellationToken.None);
    }

    public static bool IsOpen(this WebSocket ws) => ws.State == WebSocketState.Open;

    public static void EnsureOpen(this WebSocket ws)
    {
        if (!ws.IsOpen())
        {
            throw new WebSocketException(message: "websocket is not opened");
        }
    }

    internal static async Task StartReceiveLoopAsync(this WebSocket ws,
        Func<byte[], Task> onMessage, CancellationToken cancellationToken,
        int? bufferSize = default)
    {
        bufferSize ??= 1024 * 4;

        var buffer = new byte[bufferSize.Value];

        var data = new List<byte>();

        while (ws.IsOpen() && !cancellationToken.IsCancellationRequested)
        {
            var result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), cancellationToken);

            switch (result.MessageType)
            {
                case WebSocketMessageType.Text:
                    if (result.Count <= 0)
                        await Task.Delay(TimeSpan.FromMilliseconds(100));

                    data.AddRange(buffer.Take(result.Count));

                    if (data.Count > 1024 * 1024 * 0.5)
                        throw new NotSupportedException("message is too big,server is closing this connection");

                    if (result.EndOfMessage)
                    {
                        var bs = data.ToArray();
                        await onMessage.Invoke(bs);
                        //reset
                        data = new List<byte> { };
                    }

                    continue;
                case WebSocketMessageType.Close:
                    break;
                case WebSocketMessageType.Binary:
                    throw new NotSupportedException(message: nameof(WebSocketMessageType.Binary));
                default:
                    throw new NotSupportedException(message: $"message-type:{result.MessageType}");
            }
        }
    }
}