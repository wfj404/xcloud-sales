import { PickupRecordDto } from '@/utils/swagger';
import { Badge, Button, Table, TablePaginationConfig, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import React, { useState } from 'react';
import XStatus from '../components/pickup_status';
import XDetail from './detail';
import XTime from '@/components/common/time';
import TimeRange from '@/pages/store-manage/shipping/components/time_range';
import { isOrderToService } from '@/utils/biz';

export default ({
                  data,
                  pagination,
                  refresh,
                }: {
  data: PickupRecordDto[];
  pagination: TablePaginationConfig | undefined | false;
  refresh: () => void;
}) => {
  const [loading, _loading] = React.useState<boolean>(false);
  const [detailId, _detailId] = useState<string>('');

  const columns: ColumnProps<PickupRecordDto>[] = [
    {
      title: '订单状态',
      render: (d, x) => {
        const service = isOrderToService(x.Order);
        return <Badge status={service ? 'success' : 'default'} text={service ? '可服务' : '等待订单状态'} />;
      },
    },
    {
      title: '配送时间',
      render: (d, x) => {
        return (
          <div>
            <TimeRange start={x.PreferPickupStartTime} end={x.PreferPickupEndTime} />
          </div>
        );
      },
    },
    {
      title: '校验码',
      render: (d, x) => <Tag>{x.SecurityCode}</Tag>,
    },
    {
      title: '状态',
      render: (d, x) => {
        return <XStatus data={x} />;
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type={'link'}
              onClick={() => {
                _detailId(x.Id || '');
              }}
            >
              查看
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  return (
    <>
      <XDetail
        detailId={detailId}
        hide={() => {
          _detailId('');
          refresh();
        }}
      />

      <Table
        loading={loading}
        dataSource={data}
        columns={columns}
        rowKey={(x) => x.Id || ''}
        pagination={pagination}
      />
    </>
  );
};
