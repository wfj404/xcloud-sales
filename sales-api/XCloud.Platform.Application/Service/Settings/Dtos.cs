﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Settings;

namespace XCloud.Platform.Application.Service.Settings;

public class SettingDto : Setting, IEntityDto<string>
{
    //
}

public class QuerySettingsPagingInput : PagedRequest
{
    //
}