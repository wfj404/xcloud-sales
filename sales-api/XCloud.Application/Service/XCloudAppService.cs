﻿using System;
using JetBrains.Annotations;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Volo.Abp.Application.Services;
using Volo.Abp.Uow;
using Volo.Abp.Validation;
using XCloud.Application.DistributedLock;
using XCloud.Application.ServiceDiscovery;
using XCloud.Application.Utils;
using XCloud.Core.Cache;
using XCloud.Core.Json;

namespace XCloud.Application.Service;

public interface IXCloudAppService : IApplicationService
{
    //
}

public abstract class XCloudAppService : ApplicationService
{
    protected XCloudAppService()
    {
        this.LocalizationResource = null;
    }

    [NotNull]
    protected IUnitOfWork RequiredCurrentUnitOfWork =>
        this.CurrentUnitOfWork ?? throw new NotSupportedException(message: nameof(CurrentUnitOfWork));

    protected IConfiguration Configuration => LazyServiceProvider.LazyGetRequiredService<IConfiguration>();

    protected ICacheProvider CacheProvider => LazyServiceProvider.LazyGetRequiredService<ICacheProvider>();

    protected IMemoryCache MemoryCache => LazyServiceProvider.LazyGetRequiredService<IMemoryCache>();

    protected IJsonDataSerializer JsonDataSerializer =>
        LazyServiceProvider.LazyGetRequiredService<IJsonDataSerializer>();

    protected IObjectValidator ObjectValidator =>
        LazyServiceProvider.LazyGetRequiredService<IObjectValidator>();

    protected IServiceDiscoveryService ServiceDiscoveryService =>
        LazyServiceProvider.LazyGetRequiredService<IServiceDiscoveryService>();

    protected IDistributedLockProvider DistributedLockProvider =>
        LazyServiceProvider.LazyGetRequiredService<IDistributedLockProvider>();

    protected CommonUtils CommonUtils => LazyServiceProvider.LazyGetRequiredService<CommonUtils>();
}