﻿using System;
using Volo.Abp.Auditing;
using XCloud.Sales.Application.Domain;

namespace XCloud.Sales.Warehouses.Domain.Warehouses;

public class StockItem : SalesBaseEntity<string>, IHasModificationTime
{
    public string StockId { get; set; }
    public string SkuId { get; set; }
    public int Quantity { get; set; }
    public decimal Price { get; set; }

    public int DeductQuantity { get; set; }
    
    public DateTime? LastModificationTime { get; set; }
}