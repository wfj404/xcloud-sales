import { Stack, Typography } from '@mui/material';
import XLoading from '@/components/common/loading/dots';

export default ({ loading, hasMore }: { loading: boolean, hasMore: boolean }) => {

  if (loading) {
    return <XLoading />;
  }

  if (!hasMore) {
    return (<Stack spacing={2} justifyContent='center' direction='row'>
      <Typography
        variant='caption'
        color={'text.disabled'}
        sx={{
          display: 'inline',
        }}
      >
        没有更多了
      </Typography>
    </Stack>);
  }

  return null;
};
