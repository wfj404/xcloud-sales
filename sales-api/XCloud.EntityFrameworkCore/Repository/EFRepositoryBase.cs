﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using XCloud.EntityFrameworkCore.Extensions;

namespace XCloud.EntityFrameworkCore.Repository;

public interface IEfRepository<T> : IEfCoreRepository<T> where T : class, IEntity
{
    Task<T[]> QueryManyAsync(Expression<Func<T, bool>> where, int? count = null,
        CancellationToken? cancellationToken = default);
}

public abstract class EfRepositoryBase<DbContextType, T> : EfCoreRepository<DbContextType, T>, IEfRepository<T>
    where DbContextType : DbContext, IEfCoreDbContext
    where T : class, IEntity
{
    protected EfRepositoryBase(IServiceProvider serviceProvider) :
        base(serviceProvider.GetRequiredService<IDbContextProvider<DbContextType>>())
    {
        ServiceProvider = serviceProvider;
    }

    private CancellationToken GetCancellationTokenOrDefault(CancellationToken? token) =>
        token ?? CancellationTokenProvider.Token;

    public virtual async Task InsertNowAsync(T model, CancellationToken? cancellationToken = default)
    {
        var db = await GetDbContextAsync();

        var set = db.Set<T>();

        set.AddRange(new[] { model });

        await db.SaveChangesAsync(GetCancellationTokenOrDefault(cancellationToken));
    }

    public virtual async Task DeleteNowAsync(Expression<Func<T, bool>> where,
        CancellationToken? cancellationToken = default)
    {
        var db = await GetDbContextAsync();

        var set = db.Set<T>();

        set.RemoveRange(set.Where(where));

        await db.SaveChangesAsync(GetCancellationTokenOrDefault(cancellationToken));
    }

    public virtual async Task DeleteNowAsync(T model, CancellationToken? cancellationToken = default)
    {
        var db = await GetDbContextAsync();

        var entry = db.AttachEntityIfNot(model);

        if (entry.State != EntityState.Deleted)
        {
            entry.State = EntityState.Deleted;
        }

        await db.SaveChangesAsync(GetCancellationTokenOrDefault(cancellationToken));
    }

    public virtual async Task UpdateNowAsync(T model, CancellationToken? cancellationToken = default)
    {
        var db = await GetDbContextAsync();

        var entry = db.AttachEntityIfNot(model);

        if (entry.State != EntityState.Modified && !entry.Properties.Any(x => x.IsModified))
        {
            entry.State = EntityState.Modified;
        }

        await db.SaveChangesAsync(GetCancellationTokenOrDefault(cancellationToken));
    }


    private async Task<T[]> QueryManyImplAsync(IQueryable<T> query,
        Expression<Func<T, bool>> where, int? count = null,
        CancellationToken? cancellationToken = default)
    {
        query = query.Where(where);

        if (count != null)
        {
            query = query.Take(count.Value);
        }

        var res = await query.ToArrayAsync(GetCancellationTokenOrDefault(cancellationToken));

        return res;
    }

    public async Task<T[]> QueryManyAsync(Expression<Func<T, bool>> where, int? count = null,
        CancellationToken? cancellationToken = default)
    {
        var db = await GetDbContextAsync();

        var query = db.Set<T>().AsTracking();

        return await QueryManyImplAsync(query, where, count, cancellationToken);
    }

    public virtual async Task<T> QueryOneAsync(Expression<Func<T, bool>> where,
        CancellationToken? cancellationToken = default)
    {
        var query = await GetQueryableAsync();

        query = query.Where(where);

        var res = await query.FirstOrDefaultAsync(GetCancellationTokenOrDefault(cancellationToken));

        return res;
    }
}