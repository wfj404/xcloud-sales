import { CSSProperties, ReactNode } from 'react';

import { firstNotEmpty, isStringEmpty } from '@/utils/utils';
import SearchIcon from '@mui/icons-material/Search';
import { Button, IconButton, InputBase, Paper } from '@mui/material';

export default function ({
  keywords,
  onSearch,
  onChange,
  placeholder,
  addonAfter,
  addonBefore,
  hideSearchButton,
  hideSearchIcon,
  style,
}: {
  keywords?: string;
  onSearch?: (d: string | undefined) => void;
  onChange?: (d: string) => void;
  placeholder?: string;
  addonBefore?: ReactNode;
  addonAfter?: ReactNode;
  hideSearchButton?: boolean;
  hideSearchIcon?: boolean;
  style?: CSSProperties;
}) {
  const triggerSearch = () => {
    onSearch && onSearch(firstNotEmpty([keywords, placeholder]));
  };

  const convertPlaceholderOrEmpty = (): string | undefined => {
    if (placeholder && !isStringEmpty(placeholder)) {
      return `${placeholder}...`;
    }
    return undefined;
  };

  return (
    <>
      <Paper
        sx={{
          display: 'flex',
          alignItems: 'center',
          border: (theme) => `2px solid ${theme.palette.error.main}`,
        }}
        style={{ ...style }}
        component="form"
        onSubmit={(e) => {
          e.preventDefault();
          triggerSearch();
        }}
      >
        {hideSearchIcon || (
          <IconButton size="small">
            <SearchIcon />
          </IconButton>
        )}
        {addonBefore}
        <div
          style={{
            flexGrow: 1,
          }}
        >
          <InputBase
            sx={{ ml: 1, width: '100%' }}
            value={keywords}
            onChange={(e) => {
              onChange && onChange(e.target.value);
            }}
            placeholder={convertPlaceholderOrEmpty() || '搜索...'}
          />
        </div>
        {addonAfter}
        {hideSearchButton || (
          <Button
            size="small"
            color="error"
            onClick={() => {
              triggerSearch();
            }}
          >
            搜 索
          </Button>
        )}
      </Paper>
    </>
  );
}
