using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderItemGiftMap : SalesEntityTypeConfiguration<OrderItemGift>
{
    public override void Configure(EntityTypeBuilder<OrderItemGift> builder)
    {
        builder.ToTable("sales_order_item_gift");

        builder.Property(x => x.OrderItemId).RequiredId();
        builder.Property(x => x.SkuId);
        builder.Property(x => x.Quantity);

        base.Configure(builder);
    }
}