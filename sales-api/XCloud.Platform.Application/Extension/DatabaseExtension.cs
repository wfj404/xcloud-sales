using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using XCloud.Application.DistributedLock;
using XCloud.Platform.Application.Configuration;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Extension;

public static class DatabaseExtension
{
    public static async Task TryCreatePlatformDatabase(this IApplicationBuilder app)
    {
        using var s = app.ApplicationServices.CreateScope();

        var option = s.ServiceProvider.GetRequiredService<IConfiguration>().GetPlatformOptionOrDefault();

        if (option.AutoCreateDatabase)
        {
            await using var _ = await s.ServiceProvider.GetRequiredService<IDistributedLockProvider>()
                .CreateRequiredLockAsync("try-create-platform-database-lock-key", TimeSpan.FromSeconds(30));

            var repository = s.ServiceProvider.GetRequiredService<IPlatformRepository<User>>();

            await using var db = await repository.GetDbContextAsync();

            await db.Database.EnsureCreatedAsync();
        }
    }
}