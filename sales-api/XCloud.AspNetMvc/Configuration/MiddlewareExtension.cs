using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Core.Development;
using XCloud.Core.Json;

namespace XCloud.AspNetMvc.Configuration;

public static class MiddlewareExtension
{
    public static void UseDevelopmentInformation(this IApplicationBuilder app)
    {
        app.Map("/dev", option =>
        {
            option.Run(async context =>
            {
                var data = await context.RequestServices.GetDevelopmentInformation();
                var json = context.RequestServices.GetRequiredService<IJsonDataSerializer>().SerializeToString(data);

                context.Response.StatusCode = 200;
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(json);
            });
        });
    }
}