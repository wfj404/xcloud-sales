﻿namespace XCloud.Platform.Application.Domain.Security;

public class Role : PlatformBaseEntity, IHasCreationTime
{
    public string Name { get; set; }

    public string Description { get; set; }

    public DateTime CreationTime { get; set; }
}