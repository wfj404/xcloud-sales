import { GoodsDto } from '@/utils/swagger';
import { Alert, AlertTitle } from '@mui/material';

export default ({ model }: { model: GoodsDto }) => {
  if (!model.Published) {
    return (
      <Alert security="error">
        <AlertTitle>{model.Name || '--'}</AlertTitle>
        <p>商品可能被下架</p>
      </Alert>
    );
  }

  if (model.IsDeleted) {
    return <h5>商品不存在</h5>;
  }

  return <></>;
};
