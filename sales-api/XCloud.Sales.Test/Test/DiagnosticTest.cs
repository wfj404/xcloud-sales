﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace XCloud.Sales.Test.Test;

[TestClass]
public class DiagnosticTest
{
    public class XxObserver : IObserver<KeyValuePair<string, object>>
    {
        public void OnCompleted()
        {
            //
        }

        public void OnError(Exception error)
        {
            //
        }

        public void OnNext(KeyValuePair<string, object> value)
        {
            //
        }
    }

    public class HttpObserver : IObserver<KeyValuePair<string, object>>
    {
        public void OnCompleted()
        {
            //
        }

        public void OnError(Exception error)
        {
            //
        }

        public void OnNext(KeyValuePair<string, object> value)
        {
            //
        }
    }

    public class DiagnosticListenerObserver : IObserver<DiagnosticListener>
    {
        public void OnCompleted()
        {
            //
        }

        public void OnError(Exception error)
        {
            //
        }

        public void OnNext(DiagnosticListener value)
        {
            if (value.Name == "xx")
            {
                value.Subscribe(new XxObserver());
            }

            if (value.Name == "HttpHandlerDiagnosticListener")
            {
                value.Subscribe(new HttpObserver());
            }
        }
    }

    public class TestTable
    {
        public int Id { get; set; }
    }

    public class InMemoryDbContext : DbContext
    {
        public InMemoryDbContext(DbContextOptions<InMemoryDbContext> options) : base(options)
        {
            //
        }

        public DbSet<TestTable> TestTables { get; set; }
    }

    [TestMethod]
    public async Task D()
    {
        using var sub = DiagnosticListener.AllListeners.Subscribe(new DiagnosticListenerObserver());

        using (var source = new DiagnosticListener("xx"))
        {
            if (source.IsEnabled("dd"))
            {
                source.Write("dd", new { DateTime.UtcNow });
            }

            using var activity = source.StartActivity(new Activity(""), null);

            activity.AddEvent(new ActivityEvent("123") { });
        }

        var collection = new ServiceCollection();

        collection.AddDbContext<InMemoryDbContext>(option => { option.UseInMemoryDatabase("xx"); });
        collection.AddHttpClient();

        await using var provider = collection.BuildServiceProvider();

        using var s = provider.CreateScope();
        {
            var client = s.ServiceProvider.GetRequiredService<IHttpClientFactory>().CreateClient("d");

            using var res = await client.GetAsync("https://www.baidu.com");

            var html = await res.Content.ReadAsStringAsync();

            //

            var db = s.ServiceProvider.GetRequiredService<InMemoryDbContext>();
            
            await db.Database.EnsureCreatedAsync();

            var list = await db.TestTables.ToArrayAsync();
        }
    }
}