select TABLE_NAME,
       COLUMN_NAME,
       COLUMN_TYPE,
       IS_NULLABLE,
       concat(TABLE_NAME, '@', COLUMN_NAME, '@', COLUMN_TYPE, '@', IS_NULLABLE) as 'str'
from information_schema.COLUMNS
where table_schema = 'xcloud-sales'
order by TABLE_NAME asc, COLUMN_NAME asc
limit 999999999;

select DATA_TYPE, count(1)
from information_schema.COLUMNS
where table_schema = 'xcloud-sales'
group by DATA_TYPE;

select TABLE_NAME,
       COLUMN_NAME,
       COLUMN_TYPE,
       IS_NULLABLE
from information_schema.COLUMNS
where table_schema = 'xcloud-sales'
  and DATA_TYPE like '%text%'
  and lower(DATA_TYPE) != 'longtext';