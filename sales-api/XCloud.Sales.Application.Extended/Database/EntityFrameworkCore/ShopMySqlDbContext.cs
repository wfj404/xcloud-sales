﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using XCloud.Application.Core;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.EntityFrameworkCore.MySql;
using XCloud.Platform.Application;
using XCloud.Platform.Application.Domain;
using XCloud.Platform.Messenger;
using XCloud.Platform.WeChat;
using XCloud.Sales.Application.Domain;
using XCloud.Sales.Warehouses;

namespace XCloud.Sales.Application.Extended.Database.EntityFrameworkCore;

//[ConnectionStringName("XSales")]
public class ShopMySqlDbContext : AbpDbContext<ShopMySqlDbContext>
{
    public ShopMySqlDbContext(DbContextOptions<ShopMySqlDbContext> options) : base(options)
    {
        //
    }

    [MethodMetric]
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyUtf8Mb4ForAll();

        modelBuilder.ConfigEntityMapperFromAssemblies<ISalesBaseEntity>(new[]
        {
            typeof(SalesApplicationModule).Assembly,
            typeof(SalesWarehouseApplicationModule).Assembly,
        });
        modelBuilder.ConfigEntityMapperFromAssemblies<IPlatformBaseEntity>(new[]
        {
            typeof(PlatformApplicationModule).Assembly,
            typeof(PlatformMessengerModule).Assembly,
            typeof(PlatformWechatModule).Assembly
        });
    }
}