﻿using NPOI.SS.UserModel;

namespace XCloud.Sales.WeChat;

public static class NpoiExtension
{
    public static IRow SetStyle(this IRow row, ICellStyle style)
    {
        row.RowStyle = style;
        return row;
    }

    public static ICell SetStyle(this ICell cell, ICellStyle style)
    {
        cell.CellStyle = style;
        return cell;
    }
}