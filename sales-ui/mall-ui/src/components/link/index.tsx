import { Button, Modal, Popover, Segmented, Typography } from 'antd';
import React, { useEffect, useState } from 'react';

import { LinkDto, LinkType } from '@/utils/biz';
import { Box } from '@mui/material';

import CouponEditor from './coupon';
import GoodsEditor from './goods';
import UrlEditor from './url';

export default ({
  link,
  onChange,
}: {
  link: LinkDto;
  onChange: (x: LinkDto) => void;
}) => {
  const [data, _data] = useState<LinkDto>({});
  const [show, _show] = useState<boolean>(false);

  useEffect(() => {
    _data({ ...link });
  }, [link]);

  if (!data) {
    return null;
  }

  const renderLinkOriginData = () => {
    return (
      <>
        <p>原始数据：</p>
        <div style={{ maxWidth: 500, overflowY: 'scroll' }}>
          <Typography.Paragraph code copyable>
            {JSON.stringify(data)}
          </Typography.Paragraph>
        </div>
      </>
    );
  };

  const renderLinkDescription = () => {
    if (data.type == 'goods') {
      return (
        <>
          <p>跳转到商品详情页:{data.goods?.Name || '--'}</p>
        </>
      );
    } else if (data.type == 'coupon') {
      return (
        <>
          <p>跳转到优惠券</p>
        </>
      );
    } else {
      return (
        <>
          <p>跳转到链接</p>
        </>
      );
    }
  };

  return (
    <>
      <Modal
        open={show}
        title={'编辑链接'}
        onCancel={() => _show(false)}
        onOk={() => {
          onChange(data);
          _show(false);
        }}
      >
        <Segmented
          value={data.type || 'link'}
          onChange={(e) => {
            _data({
              ...data,
              type: e as LinkType,
            });
          }}
          options={[
            {
              label: '链接',
              value: 'link',
            },
            {
              label: '商品',
              value: 'goods',
            },
            {
              label: '优惠券',
              value: 'coupon',
              disabled: true,
            },
          ]}
          style={{ marginBottom: 10 }}
        />

        {(data.type == 'link' || data.type == undefined) && (
          <UrlEditor link={data} onChange={(x) => _data(x)} />
        )}
        {data.type == 'goods' && (
          <GoodsEditor link={data} onChange={(x) => _data(x)} />
        )}
        {data.type == 'coupon' && (
          <CouponEditor link={data} onChange={(x) => _data(x)} />
        )}
      </Modal>

      <Box>
        <Popover
          content={
            <div>
              {renderLinkDescription()}
              {renderLinkOriginData()}
            </div>
          }
        >
          <Button
            type={'link'}
            onClick={() => {
              _show(true);
            }}
          >
            跳转链接
          </Button>
        </Popover>
      </Box>
    </>
  );
};
