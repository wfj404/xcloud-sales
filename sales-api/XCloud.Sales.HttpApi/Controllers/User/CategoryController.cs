﻿using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/category")]
public class CategoryController : ShopBaseController
{
    private readonly CategoryViewService _categoryViewService;

    public CategoryController(CategoryViewService categoryViewService)
    {
        _categoryViewService = categoryViewService;
    }

    [HttpPost("store-root-category")]
    public async Task<ApiResponse<CategoryDto[]>> StoreRootCategoryAsync()
    {
        var storeId = await this.CurrentStoreContext.GetRequiredStoreIdAsync();

        var datalist =
            await this._categoryViewService.GetStoreRootCategoriesAsync(storeId, new CacheStrategy() { Cache = true });

        return new ApiResponse<CategoryDto[]>(datalist);
    }

    [HttpPost("by-parent")]
    public async Task<ApiResponse<CategoryDto[]>> ByParent([FromBody] IdDto<string> dto)
    {
        var storeId = await this.CurrentStoreContext.GetRequiredStoreIdAsync();

        var data = await _categoryViewService.ByParentIdAsync(
            storeId, dto.Id,
            new CacheStrategy { Cache = true });

        return new ApiResponse<CategoryDto[]>(data);
    }

    [Obsolete]
    [HttpPost("by-id")]
    public async Task<ApiResponse<CategoryDto>> ByIdAsync([FromBody] IdDto<string> dto)
    {
        var data = await this._categoryViewService.ByIdAsync(dto.Id, new CacheStrategy() { Cache = true });

        return new ApiResponse<CategoryDto>(data);
    }
}