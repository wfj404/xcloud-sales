﻿namespace XCloud.Sales.Application.Service.Users
{
    public static class UsersExtension
    {
        public static async Task ChangeUserBalanceAsync(this IStoreUserBalanceService userBalanceService,
            int userId, decimal amount, BalanceActionType actionType, string comment = null)
        {
            if (userId <= 0)
                throw new ArgumentNullException(nameof(userId));

            if (amount <= decimal.Zero)
                throw new ArgumentNullException(nameof(amount));

            var history = new BalanceHistoryDto(userId, amount, actionType, comment);

            await userBalanceService.UpdateUserBalanceAsync(history);
        }
    }
}