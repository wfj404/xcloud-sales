import XMD from '@/components/markdown';
import { GoodsDto } from '@/utils/swagger';
import { Box } from '@mui/material';
import { isStringEmpty } from '@/utils/utils';

export default ({ model }: { model: GoodsDto }) => {

  if (isStringEmpty(model.DetailInformation)) {
    return null;
  }

  return (
    <>
      <Box sx={{ my: 2, px: 1 }}>
        <XMD md={model.DetailInformation || ''} />
      </Box>
    </>
  );
};
