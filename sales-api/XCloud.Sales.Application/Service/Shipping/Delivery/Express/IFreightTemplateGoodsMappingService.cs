using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Shipping.Delivery.Express;

public interface IFreightTemplateGoodsMappingService : ISalesAppService
{
    Task SaveGoodsExpressTemplateAsync(SaveGoodsStoreExpressTemplateInput dto);
}

public class FreightTemplateGoodsMappingService : SalesAppService, IFreightTemplateGoodsMappingService
{
    private readonly ISalesRepository<FreightTemplateGoodsMapping> _repository;

    public FreightTemplateGoodsMappingService(ISalesRepository<FreightTemplateGoodsMapping> repository)
    {
        _repository = repository;
    }

    public async Task SaveGoodsExpressTemplateAsync(SaveGoodsStoreExpressTemplateInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.GoodsId))
            throw new ArgumentNullException(nameof(dto.GoodsId));

        if (string.IsNullOrWhiteSpace(dto.StoreId))
            throw new ArgumentNullException(nameof(dto.StoreId));

        if (dto.TemplateIds == null)
            throw new ArgumentNullException(nameof(dto.TemplateIds));

        var entities = dto.TemplateIds.Select(x => new FreightTemplateGoodsMapping
        {
            GoodsId = dto.GoodsId,
            StoreId = dto.StoreId,
            TemplateId = x
        }).ToArray();

        var db = await _repository.GetDbContextAsync();

        var set = db.Set<FreightTemplateGoodsMapping>();

        string FingerPrint(FreightTemplateGoodsMapping x) => $"{x.GoodsId}.{x.StoreId}.{x.TemplateId}";

        var originData = await set.Where(x => x.GoodsId == dto.GoodsId && x.StoreId == dto.StoreId).ToArrayAsync();

        var deleteList = originData.NotInBy(entities, FingerPrint).ToArray();
        var insertList = entities.NotInBy(originData, FingerPrint).ToArray();

        if (deleteList.Any())
            set.RemoveRange(deleteList);

        if (insertList.Any())
        {
            var now = Clock.Now;
            foreach (var m in insertList)
            {
                m.Id = GuidGenerator.CreateGuidString();
                m.CreationTime = now;
            }

            set.AddRange(insertList);
        }

        await db.TrySaveChangesAsync();
    }
}