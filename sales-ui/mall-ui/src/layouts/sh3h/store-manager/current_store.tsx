import { Button, Tooltip } from 'antd';
import { setSelectedStoreId } from '@/utils/utils';
import { storeState, StoreType } from '@/store';
import { useSnapshot } from 'umi';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  const selectedStore = app.storeManager?.Store || {};

  return (
    <>
      <Tooltip title={'当前二级单位'}>
        <Button
          type={'dashed'}
          size={'small'}
          disabled
          onClick={() => {
            if (!confirm('更换门店？')) {
              return;
            }
            setSelectedStoreId('');
            setTimeout(() => {
              window.location.reload();
            }, 500);
          }}>{selectedStore?.Name || '--'}</Button>
      </Tooltip>
    </>
  );
};
