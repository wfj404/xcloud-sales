﻿namespace XCloud.Sales.Application.Framework;

public interface ISalesWorkContext
{
    //
}

/// <summary>
/// Working context for web application
/// </summary>
[ExposeServices(typeof(ISalesWorkContext))]
public class WebWorkContext : ISalesWorkContext, ITransientDependency
{
    //
}