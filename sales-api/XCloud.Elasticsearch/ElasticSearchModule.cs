using Volo.Abp.Modularity;
using XCloud.Core;
using XCloud.Elasticsearch.Core;

namespace XCloud.Elasticsearch;

[DependsOn(typeof(XCloudCoreModule))]
public class ElasticSearchModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //add es-client
        context.AddElasticSearchClient();
    }
}