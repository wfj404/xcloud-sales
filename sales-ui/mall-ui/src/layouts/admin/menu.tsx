import {
  ApartmentOutlined,
  BarChartOutlined,
  BarsOutlined,
  ClockCircleOutlined,
  CodeOutlined,
  ContainerOutlined,
  DashboardOutlined,
  DotChartOutlined,
  EnvironmentOutlined,
  FlagOutlined,
  FolderOpenOutlined,
  GiftOutlined,
  GoldOutlined,
  HomeOutlined,
  PayCircleOutlined,
  PieChartOutlined,
  PlusOutlined,
  ProjectOutlined,
  RedEnvelopeOutlined,
  SearchOutlined,
  SettingOutlined,
  ShopFilled,
  ShoppingCartOutlined,
  TagOutlined,
  UserOutlined,
  WechatOutlined,
} from '@ant-design/icons';
import { MenuDataItem } from '@ant-design/pro-layout';

const routes: Array<MenuDataItem> = [
  {
    name: '统计',
    path: '/admin/dashboard/home',
    key: 'dashboard',
    icon: <BarChartOutlined />,
    children: [
      {
        name: '统计报表',
        path: '/admin/dashboard/home',
        icon: <DashboardOutlined />,
      },
      {
        name: '销售统计',
        path: '/admin/dashboard/sales',
        icon: <DotChartOutlined />,
      },
      {
        name: '关键词',
        path: '/admin/dashboard/keywords',
        icon: <SearchOutlined />,
      },
      {
        name: '活跃时间',
        path: '/admin/dashboard/activity',
        icon: <ClockCircleOutlined />,
      },
      {
        name: '活跃城市',
        path: '/admin/dashboard/activity-geo',
        icon: <EnvironmentOutlined />,
      },
    ],
  },
  {
    name: '商品',
    path: '/admin/catalog/goods',
    key: 'catalog',
    icon: <ShoppingCartOutlined />,
    children: [
      {
        name: '商品管理',
        path: '/admin/catalog/goods',
        icon: <FolderOpenOutlined />,
      },
      {
        name: '品牌管理',
        path: '/admin/catalog/brand',
        icon: <FlagOutlined />,
      },
      {
        name: '分类管理',
        path: '/admin/catalog/category',
        icon: <BarsOutlined />,
      },
      {
        name: '标签管理',
        path: '/admin/catalog/tag',
        icon: <TagOutlined />,
      },
    ],
  },
  {
    name: '促销',
    path: '/admin/marketing/coupon',
    key: 'marketing',
    icon: <PieChartOutlined />,
    children: [
      {
        name: '折扣',
        path: '/admin/marketing/discount',
        icon: <PayCircleOutlined />,
      },
      {
        name: '优惠券',
        path: '/admin/marketing/coupon',
        icon: <RedEnvelopeOutlined />,
      },
      {
        name: '预售卡',
        path: '/admin/marketing/gift-card',
        icon: <GiftOutlined />,
      },
    ],
  },
  {
    name: '库存',
    path: '/admin/stock/stock',
    key: 'warehouse',
    icon: <ProjectOutlined />,
    children: [
      {
        name: '采购单',
        path: '/admin/stock/stock',
        icon: <PlusOutlined />,
      },
      {
        name: '库存查询',
        path: '/admin/stock/stock-items',
        icon: <GoldOutlined />,
      },
      {
        name: '仓库管理',
        path: '/admin/stock/warehouse',
        icon: <HomeOutlined />,
      },
      {
        name: '供应商管理',
        path: '/admin/stock/supplier',
        icon: <UserOutlined />,
      },
    ],
  },
  {
    name: '设置',
    path: '/admin/settings/store',
    key: 'settings',
    icon: <SettingOutlined />,
    children: [
      {
        name: '商城管理',
        path: '/admin/settings/store',
        icon: <ShopFilled />,
      },
      {
        name: '用户管理',
        path: '/admin/settings/store_user',
        icon: <UserOutlined />,
      },
      {
        name: '系统管理员',
        path: '/admin/settings/admin',
        icon: <ApartmentOutlined />,
      },
      {
        name: '微信设置',
        path: '/admin/settings/wechat',
        icon: <WechatOutlined />,
      },
      {
        name: '国家和地区',
        path: '/admin/settings/area',
        icon: <EnvironmentOutlined />,
      },
      {
        name: '环境变量',
        path: '/admin/settings/env',
        icon: <CodeOutlined />,
      },
      {
        name: '活动日志',
        path: '/admin/settings/log',
        icon: <ContainerOutlined />,
      },
    ],
  },
];

export default routes;
