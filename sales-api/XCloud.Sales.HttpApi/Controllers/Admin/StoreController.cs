﻿using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/store")]
public class StoreController : ShopBaseController
{
    private readonly IStoreService _storeService;
    private readonly IStoreManagerService _storeManagerService;
    private readonly IStoreManagerAccountService _storeManagerAccountService;

    public StoreController(IStoreService storeService,
        IStoreManagerService storeManagerService,
        IStoreManagerAccountService storeManagerAccountService)
    {
        _storeService = storeService;
        _storeManagerService = storeManagerService;
        _storeManagerAccountService = storeManagerAccountService;
    }

    [HttpPost("list")]
    public async Task<ApiResponse<StoreDto[]>> QueryStores()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var response = await _storeService.QueryPagingAsync(new QueryStorePagingInput
        {
            Page = 1,
            PageSize = 1000,
            SkipCalculateTotalCount = true
        });

        return new ApiResponse<StoreDto[]>(response.Items.ToArray());
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> CreateStore([FromBody] StoreDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            await _storeService.InsertAsync(dto);
        }
        else
        {
            await _storeService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public virtual async Task<ApiResponse<object>> DeleteStore([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var store = await _storeService.GetByIdAsync(dto.Id);

        if (store == null)
            throw new EntityNotFoundException(nameof(store));

        await _storeService.DeleteByIdAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("manager-list")]
    public async Task<ApiResponse<StoreManagerDto[]>> QueryStoreManager([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var store = await _storeService.GetByIdAsync(dto.Id);

        if (store == null)
            throw new EntityNotFoundException(nameof(store));

        var managers = await _storeManagerService.QueryByStoreIdAsync(dto.Id);

        await _storeManagerService.AttachUsersAsync(managers);

        return new ApiResponse<StoreManagerDto[]>(managers);
    }

    [HttpPost("save-manager")]
    public virtual async Task<ApiResponse<StoreManager>> SaveStoreManager([FromBody] StoreManagerDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var manager = await this._storeManagerAccountService.GetOrCreateByUserIdAsync(dto.StoreId, dto.UserId);

        return new ApiResponse<StoreManager>(manager);
    }

    [HttpPost("delete-manager")]
    public virtual async Task<ApiResponse<object>> DeleteStoreManager([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var manager = await _storeManagerService.GetByIdAsync(dto.Id);

        if (manager == null)
            throw new EntityNotFoundException(nameof(manager));

        var store = await _storeService.GetByIdAsync(manager.StoreId);

        if (store == null)
            throw new EntityNotFoundException(nameof(store));

        await _storeManagerAccountService.UpdateStatusAsync(new UpdateStoreManagerStatusInput
        {
            ManagerId = manager.Id,
            IsActive = false
        });

        return new ApiResponse<object>();
    }
}