﻿using System;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.EntityFrameworkCore.Exceptions;

public class EfCoreErrorInfoConverter : IErrorInfoConverter, ITransientDependency
{
    public int Order => default;

    private string ExceptionDetail(DbUpdateException e)
    {
        var sb = new StringBuilder();

        sb.AppendLine($"message:{e.Message}");
        sb.AppendLine($"detail:{e.StackTrace}");
        sb.AppendLine($"inner_message:{e.InnerException?.Message}");
        sb.AppendLine($"inner_detail:{e.InnerException?.Message}");

        return sb.ToString();
    }

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        if (e is DbUpdateConcurrencyException dbUpdateConcurrencyException)
        {
            return new ErrorInfo
            {
                Message = "db concurrency error",
                Details = ExceptionDetail(dbUpdateConcurrencyException)
            };
        }

        if (e is DbUpdateException dbUpdateException)
        {
            return new ErrorInfo
            {
                Message = "db error",
                Details = ExceptionDetail(dbUpdateException)
            };
        }

        if (e is UnSubmitChangesException unSubmitChangesException)
        {
            return new ErrorInfo
            {
                Message = unSubmitChangesException.Message,
                Details = nameof(UnSubmitChangesException)
            };
        }

        return null;
    }
}