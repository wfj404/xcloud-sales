import { OrderReviewDto, OrderReviewItemDto } from '@/utils/swagger';
import { Rate } from 'antd';
import { ProColumns, ProTable } from '@ant-design/pro-components';
import PreviewGroup from '@/components/common/preview_group';
import { getSkuName } from '@/utils/biz';

export default ({ review }: { review: OrderReviewDto }) => {
  const columns: ProColumns<OrderReviewItemDto>[] = [
    {
      title: '商品信息',
      render: (d, x) => {
        return <>
          <div>
            {getSkuName(x.OrderItem?.Sku || {})}
          </div>
        </>;
      },
    },
    {
      title: '评论',
      render: (d, x) => {
        return <>
          <div>{x.ReviewText || '--'}</div>
        </>;
      },
    },
    {
      title: '图片',
      render: (d, x) => {
        if (x.StorageMeta == null || x.StorageMeta.length <= 0) {
          return null;
        }

        return <PreviewGroup data={x.StorageMeta} count={10} />;
      },
    },
    {
      title: '得分',
      render: (d, x) => <Rate value={x.Rating || 0} allowHalf disabled />,
    },
  ];

  return <>
    <ProTable
      search={false}
      headerTitle={'详情'}
      options={false}
      columns={columns}
      dataSource={review.Items || []}
      pagination={false}
    />
  </>;
};
