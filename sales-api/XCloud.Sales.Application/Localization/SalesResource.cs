using Volo.Abp.Localization;

namespace XCloud.Sales.Application.Localization;

[LocalizationResourceName(nameof(SalesResource))]
public class SalesResource
{
    //
}