import { Result } from 'antd';
import { useEffect, useState } from 'react';

import { ApiResponse } from '@/utils/biz';
import { httpClient } from '@/utils/http';
import { StoreDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';

export default () => {

  const [stores, _stores] = useState<StoreDto[]>([]);
  const [loading, _loading] = useState(true);

  useEffect(() => {
    _loading(true);
    httpClient.post<ApiResponse<StoreDto[]>>('/api/mall/qingdao/store/stores-list', {})
      .then(res => {
        _stores(res.data.Data || []);
      })
      .finally(() => _loading(false));
  }, []);

  const renderTips = () => {
    if (loading) {
      return <p>loading...</p>;
    }
    if (isArrayEmpty(stores)) {
      return <p>你不在有效的组织内</p>;
    }
    return (<div>
      <p>你不在以下组织：</p>
      {stores.map((x, i) => <p key={i}>{i + 1}.{x.Name || '--'}</p>)}
    </div>);
  };

  return <>
    <Result status={403} title={'无权操作'} subTitle={renderTips()} />
  </>;
};
