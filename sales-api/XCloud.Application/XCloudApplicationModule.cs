﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.Application;
using Volo.Abp.Application.Services;
using Volo.Abp.Auditing;
using Volo.Abp.Authorization;
using Volo.Abp.AutoMapper;
using Volo.Abp.DynamicProxy;
using Volo.Abp.Features;
using Volo.Abp.GlobalFeatures;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Uow;
using Volo.Abp.Validation;
using Volo.Abp.VirtualFileSystem;
using XCloud.Application.Apm.ElasticApm;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.Application.Job;
using XCloud.Application.Localization;
using XCloud.Application.Logging;
using XCloud.Application.Queue;
using XCloud.Application.Redis;
using XCloud.Application.Validation;
using XCloud.Core.Extension;
using XCloud.EntityFrameworkCore;

namespace XCloud.Application;

[DependsOn(
    typeof(XCloudEntityFrameworkCoreModule),
    //abp
    typeof(AbpLocalizationModule),
    typeof(AbpVirtualFileSystemModule),
    typeof(AbpDddApplicationModule),
    typeof(AbpValidationModule),
    typeof(AbpAutoMapperModule),
    typeof(AbpAuditingModule),
    typeof(AbpUnitOfWorkModule)
)]
[MethodMetric]
public class XCloudApplicationModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        var env = context.Services.GetHostingEnvironment();

        context.Services.OnRegistered(ctx =>
        {
            //add apm interceptor
            ctx.TryAddElasticApmInterceptor();
            if (env.IsStaging() || env.IsDevelopment())
            {
                if (ctx.ImplementationType.IsNormalPublicClass() &&
                    ctx.ImplementationType.IsAssignableToType<IApplicationService>())
                {
                    ctx.Interceptors.TryAdd<DeprecatedMethodLoggingInterceptor>();
                }
            }
        });
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //localization
        context.ConfigApplicationSharedLocalization();

        //logging
        context.AddLoggingProvider();

        //redis
        context.AddRedisAll();

        //jobs
        context.AddHangfireJobProvider();

        //queue
        context.AddCapMessageProvider();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAuditingOptions>(option =>
        {
            //auditing
            option.IsEnabled = false;
        });

        context.Services.RemoveAll<IMethodInvocationValidator>()
            .AddTransient<IMethodInvocationValidator, NullMethodInvocationValidator>();

        StaticLoggerFactoryHolder.LoggerFactory?.CreateLogger<XCloudApplicationModule>()
            .LogWarning("remove useless interceptors");

        context.Services.OnRegistered(registryContext =>
        {
            registryContext.Interceptors.TryRemove<IAbpInterceptor, GlobalFeatureInterceptor>();
            registryContext.Interceptors.TryRemove<IAbpInterceptor, FeatureInterceptor>();
            registryContext.Interceptors.TryRemove<IAbpInterceptor, AuthorizationInterceptor>();
            registryContext.Interceptors.TryRemove<IAbpInterceptor, ValidationInterceptor>();
            registryContext.Interceptors.TryRemove<IAbpInterceptor, AuditingInterceptor>();
        });
    }

    public override void OnPreApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        app.TryUseElasticApm();
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        //jobs
        app.UseJobWorker();
        //queue
        app.UseMessageBusWorker();
    }
}