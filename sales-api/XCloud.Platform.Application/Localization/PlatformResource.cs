﻿using Volo.Abp.Localization;

namespace XCloud.Platform.Application.Localization;

[LocalizationResourceName(nameof(PlatformResource))]
public class PlatformResource
{
    //
}