using Volo.Abp.Application.Dtos;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.View;

public class SearchView : IEntityDto
{
    public string[] Keywords { get; set; }
    public TagDto[] Tags { get; set; }
    public BrandDto[] Brands { get; set; }
}

public class DashboardCountView : IEntityDto
{
    public int Goods { get; set; }
    public int Orders { get; set; }
    public int Brand { get; set; }
    public int Category { get; set; }
    public int Tag { get; set; }
    public int AfterSale { get; set; }
    public int ActiveUser { get; set; }
    public int Coupon { get; set; }
    public int Promotion { get; set; }
    public decimal Balance { get; set; }
    public decimal PrepaidCard { get; set; }
}

public class RefreshViewCacheInput : IEntityDto
{
    public bool Home { get; set; }
    public bool RootCategory { get; set; }
    public bool Dashboard { get; set; }
    public bool Search { get; set; }
    public bool MallSettings { get; set; }
}