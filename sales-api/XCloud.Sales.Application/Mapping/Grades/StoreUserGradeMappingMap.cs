﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Grades;

namespace XCloud.Sales.Application.Mapping.Grades;

public class StoreUserGradeMappingMap : SalesEntityTypeConfiguration<StoreUserGradeMapping>
{
    public override void Configure(EntityTypeBuilder<StoreUserGradeMapping> builder)
    {
        builder.ToTable("sales_store_user_grade_mapping");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.UserId);
        builder.Property(x => x.GradeId);
        builder.Property(x => x.StartTime);
        builder.Property(x => x.EndTime);

        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}