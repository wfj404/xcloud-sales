﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Logging;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.Filters;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
public class NonActionLoggingAttribute : Attribute
{
    //
}

[ExposeServices(typeof(ActionLoggingFilter), typeof(IAsyncActionFilter))]
public class ActionLoggingFilter : IAsyncActionFilter, ITransientDependency
{
    private readonly ILogger _logger;

    public ActionLoggingFilter(ILogger<ActionLoggingFilter> logger)
    {
        _logger = logger;
    }

    private bool LoggingIsEnabled(ActionExecutingContext context)
    {
        if (!context.ActionDescriptor.IsControllerAction())
        {
            return false;
        }

        var actionMethod = context.ActionDescriptor.GetMethodInfo();

        var memoryCache = context.HttpContext.RequestServices.GetRequiredService<IMemoryCache>();

        return memoryCache.GetOrCreate(actionMethod, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);

            var classAttr = actionMethod.DeclaringType.GetCustomAttributesList<NonActionLoggingAttribute>();
            var methodAttr = actionMethod.GetCustomAttributesList<NonActionLoggingAttribute>();
            var actionMethodAttr = actionMethod.GetCustomAttributesList<HttpMethodAttribute>();

            return actionMethod.IsPublic && actionMethodAttr.Any() &&
                   !classAttr.Any() && !methodAttr.Any();
        });
    }

    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var actionExecutedContext = await next.Invoke();

        try
        {
            var config = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
            var option = config.GetLoggingOptionOrDefault();

            if (option.PrintLog && this.LoggingIsEnabled(context))
            {
                var info = context.GetHttpContextInfoToPrint(actionExecutedContext);
                this._logger.LogInformation(message: info);
            }
        }
        catch (Exception e)
        {
            this._logger.LogError(message: e.Message, exception: e);
        }
    }
}