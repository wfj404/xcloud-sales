import 'leaflet/dist/leaflet.css';

import L from 'leaflet';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

export const marker_icon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
  iconSize: [27, 32],
  iconAnchor: [13, 32],
  popupAnchor: [0, -30],
});

export const tile_server =
  'https://webrd04.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=7&x={x}&y={y}&z={z}';

export const wuxi_default_location: L.LatLngLiteral = {
  lat: 31.456489605661922,
  lng: 120.27968627233334,
};
