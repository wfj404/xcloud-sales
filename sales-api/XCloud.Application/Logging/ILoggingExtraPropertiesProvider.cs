﻿using System.Collections.Generic;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Tracing;

namespace XCloud.Application.Logging;

public interface ILoggingExtraPropertiesProvider
{
    IDictionary<string, string> GetExtraProperties();
}

[ExposeServices(typeof(ILoggingExtraPropertiesProvider))]
public class DefaultLoggingExtraPropertiesProvider : ILoggingExtraPropertiesProvider, ITransientDependency
{
    private readonly ICorrelationIdProvider _correlationIdProvider;

    public DefaultLoggingExtraPropertiesProvider(ICorrelationIdProvider correlationIdProvider)
    {
        _correlationIdProvider = correlationIdProvider;
    }

    public IDictionary<string, string> GetExtraProperties()
    {
        var dict = new Dictionary<string, string>();

        var correlationId = this._correlationIdProvider.Get();

        if (!string.IsNullOrWhiteSpace(correlationId))
        {
            dict[nameof(correlationId)] = correlationId;
        }

        return dict;
    }
}