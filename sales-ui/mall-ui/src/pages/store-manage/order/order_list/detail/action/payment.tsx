import { Button, message, Modal } from 'antd';
import { useState } from 'react';

import { OrderStatus, PaymentStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';
import { PayCircleOutlined } from '@ant-design/icons';

export default function ({ model, ok }: { model: OrderDto; ok: () => void }) {
  const [show, _show] = useState<boolean>(false);

  const hide = () => _show(false);

  const [loadingSave, _loadingSave] = useState(false);

  const startPayment = async () => {
    if (isStringEmpty(model.Id)) {
      return;
    }
    _loadingSave(true);

    apiClient.mallManager
      .paymentBillCreateOfflinePayment({ Key: model.Id, Value: undefined })
      .then((res) => {
        handleResponse(res, () => {
          message.success('支付成功');
          hide && hide();
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  return (
    <>
      <Button.Group>
        <Button
          disabled={
            model.OrderStatusId == OrderStatus.Closed ||
            model.PaymentStatusId == PaymentStatus.Paid
          }
          icon={<PayCircleOutlined />}
          type={'primary'}
          color={'green'}
          onClick={() => {
            _show(true);
          }}
        >
          手动支付
        </Button>
      </Button.Group>
      <Modal
        title="收银台"
        open={show}
        onCancel={() => {
          hide && hide();
        }}
        onOk={() => {
          startPayment();
        }}
        okText="标记订单为已支付"
        confirmLoading={loadingSave}
      >
        <h6
          style={{
            marginBottom: 10,
            display: 'block',
          }}
        >
          当买家已经在线下完成支付，可以在此手动将订单标记为【已支付】
        </h6>
      </Modal>
    </>
  );
}
