using XCloud.AspNetMvc.Filters;

namespace XCloud.Sales.WebApi.Controllers;

//[Route("[controller]/[action]")]
[NonActionLogging]
public class HomeController : ShopBaseController
{
    [HttpGet]
    public ActionResult Index() => View();

    [HttpGet]
    public ActionResult Swagger() => Redirect("/swagger/index.html");
}