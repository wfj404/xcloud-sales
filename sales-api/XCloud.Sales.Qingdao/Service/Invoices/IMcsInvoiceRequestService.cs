﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Authorization;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Domain.Invoices;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Domain.Invoices;

namespace XCloud.Sales.Qingdao.Service.Invoices;

public interface IMcsInvoiceRequestService : ISalesAppService
{
    Task EnsureUserInvoiceRequestAsync(string mcsUserId, string requestId);

    Task EnsureMcsBizOrderAsync(string[] bizIds);

    Task<McsInvoiceRequest> CreateInvoiceRequestAsync(McsInvoiceRequestDto dto);

    Task MarkAsSuccessAsync(string requestId);

    Task MarkAsFailedAsync(string requestId);

    Task MarkAsPickedAsync(string requestId);

    [Obsolete]
    Task SetAttachmentIdAsync(string requestId, string attachmentId);

    Task<McsInvoiceRequestDto[]> AttachUserInvoiceAsync(McsInvoiceRequestDto[] data);

    Task SoftDeleteByIdAsync(string requestId);

    Task<PagedResponse<McsInvoiceRequestDto>> QueryPagingAsync(QueryMcsInvoiceRequestInput dto);

    Task<McsInvoiceRequestDto[]> AttachItemsAsync([NotNull] McsInvoiceRequestDto[] data);
}

public class McsInvoiceRequestService : SalesAppService, IMcsInvoiceRequestService
{
    private readonly ISalesRepository<McsInvoiceRequest> _repository;

    public McsInvoiceRequestService(ISalesRepository<McsInvoiceRequest> repository)
    {
        _repository = repository;
    }

    public async Task EnsureUserInvoiceRequestAsync(string mcsUserId, string requestId)
    {
        if (string.IsNullOrWhiteSpace(requestId))
            throw new ArgumentNullException(nameof(requestId));

        var entity = await this._repository.GetRequiredByIdAsync(requestId);

        if (entity.McsUserId != mcsUserId)
            throw new AbpAuthorizationException(message: nameof(EnsureUserInvoiceRequestAsync));
    }

    public async Task EnsureMcsBizOrderAsync(string[] bizIds)
    {
        if (ValidateHelper.IsEmptyCollection(bizIds))
            throw new ArgumentNullException(nameof(bizIds));

        var db = await this._repository.GetDbContextAsync();

        var items = await db.Set<McsInvoiceRequestItem>().AsNoTracking()
            .Where(x => bizIds.Contains(x.McsBizOrderId))
            .ToArrayAsync();

        foreach (var m in bizIds)
        {
            var item = items.FirstOrDefault(x => x.McsBizOrderId == m);
            if (item != null)
                throw new ArgumentException(message: "部分业务单存在开票记录");
        }
    }

    public async Task<McsInvoiceRequest> CreateInvoiceRequestAsync(McsInvoiceRequestDto dto)
    {
        if (dto == null || ValidateHelper.IsEmptyCollection(dto.Items))
            throw new ArgumentException(message: "entity or items is empty");

        dto.InitFields();

        var entity = dto.MapTo<McsInvoiceRequestDto, McsInvoiceRequest>(ObjectMapper);
        var items = dto.Items.MapArrayTo<McsInvoiceRequestItemDto, McsInvoiceRequestItem>(ObjectMapper);

        entity.Id = this.GuidGenerator.CreateGuidString();
        entity.TotalAmount = items.Sum(x => x.Price);
        entity.CreationTime = this.Clock.Now;

        foreach (var m in items)
        {
            if (m.Price < decimal.Zero)
                throw new ArgumentException(message: $"error price");

            m.Id = this.GuidGenerator.CreateGuidString();
            m.InvoiceRequestId = entity.Id;
        }

        var db = await this._repository.GetDbContextAsync();

        db.Set<McsInvoiceRequest>().Add(entity);
        db.Set<McsInvoiceRequestItem>().AddRange(items);

        await db.SaveChangesAsync();

        return entity;
    }

    public async Task MarkAsSuccessAsync(string requestId)
    {
        if (string.IsNullOrWhiteSpace(requestId))
            throw new ArgumentNullException(nameof(requestId));

        var entity = await this._repository.GetRequiredByIdAsync(requestId);

        if (entity.Status != McsInvoiceRequestStatus.Pending)
        {
            throw new UserFriendlyException(message: "status error:!pending");
        }

        entity.Status = McsInvoiceRequestStatus.Success;
        entity.InvoicingTime = this.Clock.Now;

        await this._repository.UpdateAsync(entity);
    }

    public async Task MarkAsFailedAsync(string requestId)
    {
        if (string.IsNullOrWhiteSpace(requestId))
            throw new ArgumentNullException(nameof(requestId));

        var entity = await this._repository.GetRequiredByIdAsync(requestId);

        if (entity.Status != McsInvoiceRequestStatus.Pending)
        {
            throw new UserFriendlyException(message: "status error:!pending");
        }

        entity.Status = McsInvoiceRequestStatus.Failed;

        await this._repository.UpdateAsync(entity);
    }

    public async Task MarkAsPickedAsync(string requestId)
    {
        if (string.IsNullOrWhiteSpace(requestId))
            throw new ArgumentNullException(nameof(requestId));

        var entity = await this._repository.GetRequiredByIdAsync(requestId);

        if (entity.Picked)
            return;

        if (entity.Status != McsInvoiceRequestStatus.Success)
        {
            throw new UserFriendlyException(message: "status error:!success");
        }

        entity.Picked = true;
        entity.PickedTime = this.Clock.Now;

        await this._repository.UpdateAsync(entity);
    }

    public async Task SetAttachmentIdAsync(string requestId, string attachmentId)
    {
        if (string.IsNullOrWhiteSpace(requestId))
            throw new ArgumentNullException(nameof(requestId));

        var entity = await this._repository.GetRequiredByIdAsync(requestId);

        entity.AttachmentMetaId = attachmentId;

        await this._repository.UpdateAsync(entity);
    }

    public async Task SoftDeleteByIdAsync(string requestId)
    {
        if (string.IsNullOrWhiteSpace(requestId))
            throw new ArgumentNullException(nameof(requestId));

        var entity = await this._repository.FirstOrDefaultAsync(x => x.Id == requestId);

        if (entity == null)
            return;

        entity.IsDeleted = true;

        await this._repository.UpdateAsync(entity);
    }

    public async Task<McsInvoiceRequestDto[]> AttachItemsAsync(McsInvoiceRequestDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await this._repository.GetDbContextAsync();
        var ids = data.Ids();

        var items = await db.Set<McsInvoiceRequestItem>().AsNoTracking()
            .Where(x => ids.Contains(x.InvoiceRequestId))
            .OrderByDescending(x => x.Id)
            .ToArrayAsync();

        foreach (var m in data)
        {
            m.Items = items.Where(x => x.InvoiceRequestId == m.Id)
                .MapArrayTo<McsInvoiceRequestItem, McsInvoiceRequestItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<McsInvoiceRequestDto[]> AttachUserInvoiceAsync(McsInvoiceRequestDto[] data)
    {
        if (data.Any())
        {
            var ids = data.Select(x => x.UserInvoiceId).ToArray();

            var db = await this._repository.GetDbContextAsync();
            var items = await db.Set<UserInvoice>().IgnoreQueryFilters().AsNoTracking()
                .WhereIdIn(ids)
                .OrderByDescending(x => x.Id)
                .ToArrayAsync();

            foreach (var m in data)
            {
                m.UserInvoice =
                    items.FirstOrDefault(x => x.Id == m.UserInvoiceId)?
                        .MapTo<UserInvoice, UserInvoiceDto>(ObjectMapper);
            }
        }

        return data;
    }

    public async Task<PagedResponse<McsInvoiceRequestDto>> QueryPagingAsync(QueryMcsInvoiceRequestInput dto)
    {
        var db = await this._repository.GetDbContextAsync();

        var requestQuery = db.Set<McsInvoiceRequest>().AsNoTracking();

        if (dto.PickupLocationId > 0)
            requestQuery = requestQuery.Where(x => x.PickupLocationId == dto.PickupLocationId);

        if (!string.IsNullOrWhiteSpace(dto.McsUserId))
            requestQuery = requestQuery.Where(x => x.McsUserId == dto.McsUserId);

        if (!string.IsNullOrWhiteSpace(dto.Status))
            requestQuery = requestQuery.Where(x => x.Status == dto.Status);

        if (dto.Picked ?? false)
            requestQuery = requestQuery.Where(x => x.Picked == true);

        if (!string.IsNullOrWhiteSpace(dto.BizType))
            requestQuery = requestQuery.Where(x => x.BizType == dto.BizType);

        requestQuery = requestQuery.WhereCreationTimeBetween(dto.StartTime, dto.EndTime);

        if (!string.IsNullOrWhiteSpace(dto.BizOrderId))
        {
            var itemQuery = db.Set<McsInvoiceRequestItem>().AsNoTracking()
                .Where(x => x.McsBizOrderId == dto.BizOrderId);

            requestQuery = requestQuery.Where(x => itemQuery.Any(d => d.InvoiceRequestId == x.Id));
        }

        var count = await requestQuery.CountOrDefaultAsync(dto);

        var datalist = await requestQuery
            .OrderByDescending(x => x.CreationTime).ThenByDescending(x => x.Id)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = datalist.MapArrayTo<McsInvoiceRequest, McsInvoiceRequestDto>(ObjectMapper);

        return new PagedResponse<McsInvoiceRequestDto>(items, dto, count);
    }
}