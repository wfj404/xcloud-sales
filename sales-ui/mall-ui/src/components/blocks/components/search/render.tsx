import React, { useState } from 'react';

import XSearchBox from '@/components/common/search_box';
import { redirectToGoodsSearch } from '@/utils/biz';
import { Box } from '@mui/material';

import { SearchBlockProps } from './utils';

export default ({ data }: { data: SearchBlockProps }) => {
  const [kwd, _kwd] = useState('');

  if (!data) {
    return null;
  }

  return (
    <>
      <Box sx={{}}>
        <XSearchBox
          keywords={kwd}
          placeholder={data.placeholder || undefined}
          onChange={(e: string) => {
            _kwd(e);
          }}
          onSearch={(e) => {
            redirectToGoodsSearch({ Keywords: e });
          }}
        />
      </Box>
    </>
  );
};
