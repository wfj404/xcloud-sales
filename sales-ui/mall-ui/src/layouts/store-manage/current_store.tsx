import { Button, Tooltip } from 'antd';
import { setSelectedStoreId } from '@/utils/utils';
import { storeState, StoreType } from '@/store';
import { useSnapshot } from 'umi';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  const selectedStore = app.storeManager?.Store;

  if (!selectedStore) {
    return null;
  }

  return (
    <>
      <Tooltip title={'点击更换门店'}>
        <Button
          type={'dashed'}
          size={'small'}
          onClick={() => {
            if (!confirm('更换门店？')) {
              return;
            }
            setSelectedStoreId('');
            setTimeout(() => {
              window.location.reload();
            }, 500);
          }}>{selectedStore?.Name || '--'}</Button>
      </Tooltip>
    </>
  );
};
