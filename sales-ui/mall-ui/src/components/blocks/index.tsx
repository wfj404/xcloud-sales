import { isArrayEmpty } from '@/utils/utils';

import XRenderBlockItem from './render_block_item';
import { BlockItem, BlockItemProps } from './utils';

export default ({
  blocks,
  showHidden,
}: {
  showHidden?: boolean;
  blocks?: BlockItem<BlockItemProps>[];
}) => {
  const visibleBlocks = showHidden ? blocks : blocks?.filter((x) => !x.hide);

  if (!visibleBlocks || isArrayEmpty(visibleBlocks)) {
    return null;
  }

  return (
    <>
      {visibleBlocks.map((x, i) => {
        return (
          <div key={i}>
            <XRenderBlockItem block={x} />
          </div>
        );
      })}
    </>
  );
};
