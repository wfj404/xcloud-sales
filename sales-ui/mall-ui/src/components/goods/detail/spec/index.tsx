import { useRequest } from 'ahooks';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import {
  GoodsDto,
  SkuDto,
  SpecDto,
  SpecItemDto,
  SpecValueDto,
} from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Typography } from '@mui/material';

import XMenu from './menu';
import XSkuList from './sku_list';
import { get_spec_combination_description } from './utils';

export default ({
  goods,
  skuId,
  onSkuSelect,
}: {
  goods: GoodsDto;
  skuId?: string;
  onSkuSelect: (e?: SkuDto) => void;
}) => {
  const [selectedSpecItems, _selectedSpecItems] = useState<SpecItemDto[]>([]);

  const get_menu_request = useRequest(apiClient.mall.skuGetSpecMenuGroup, {
    manual: true,
  });

  const data = get_menu_request.data?.data?.Data || {};

  const specs: SpecDto[] = data.Specs || [];
  const all_specs_values: SpecValueDto[] = specs.flatMap((x) => x.Values || []);

  const filtered_sku_list: SkuDto[] = data.FilteredSkuList || [];

  useEffect(() => {
    //get_menu_request.mutate(undefined);
    if (goods && goods.Id) {
      get_menu_request.run({
        GoodsId: goods.Id,
        Specs: selectedSpecItems || [],
      });
    }
  }, [goods, selectedSpecItems]);

  const renderSelectionDescription = () => {
    const desc = get_spec_combination_description(
      selectedSpecItems,
      all_specs_values,
    );

    if (isStringEmpty(desc)) {
      return null;
    }

    return (
      <Typography variant="caption" color="text.disabled" component={'span'}>
        {desc}
      </Typography>
    );
  };

  if (get_menu_request.loading) {
    //return null;
  }

  return (
    <>
      <Box
        sx={{
          p: 1,
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'flex-start',
          }}
        >
          <Typography variant="button" component={'span'} sx={{ mr: 1 }}>
            选择规格
          </Typography>
          {renderSelectionDescription()}
        </Box>
        <XMenu
          loading={get_menu_request.loading}
          specs={specs}
          selectedSpecItems={selectedSpecItems}
          onSpecItemsSelect={(e) => _selectedSpecItems(e)}
        />
        <Box sx={{}}>
          <XSkuList
            spec_values={all_specs_values}
            loading={get_menu_request.loading}
            skus_list={filtered_sku_list}
            skuId={skuId}
            onSkuSelect={(e) => {
              onSkuSelect(e);
            }}
          />
        </Box>
      </Box>
    </>
  );
};
