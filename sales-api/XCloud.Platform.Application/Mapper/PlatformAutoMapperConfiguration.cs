﻿using AutoMapper;
using XCloud.Platform.Application.Domain.Address;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Domain.Common;
using XCloud.Platform.Application.Domain.Dept;
using XCloud.Platform.Application.Domain.Invoices;
using XCloud.Platform.Application.Domain.Security;
using XCloud.Platform.Application.Domain.Settings;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Service.Address;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Common;
using XCloud.Platform.Application.Service.Dept;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Mapper;

public class PlatformAutoMapperConfiguration : Profile
{
    public PlatformAutoMapperConfiguration()
    {
        CreateMap<Setting, SettingDto>().ReverseMap();
        CreateMap<StorageMeta, StorageMetaDto>().ReverseMap();
        CreateMap<UserAddress, UserAddressDto>().ReverseMap();
        CreateMap<UserInvoice, UserInvoiceDto>().ReverseMap();

        CreateMap<Area, AreaDto>().ReverseMap();

        CreateMap<ExternalConnect, UserExternalConnectDto>().ReverseMap();
        CreateMap<ExternalAccessToken, UserExternalAccessTokenDto>().ReverseMap();

        CreateMap<Admin, AdminDto>().ReverseMap();
        CreateMap<User, UserDto>().ReverseMap();
        CreateMap<UserRealName, UserRealNameDto>().ReverseMap();

        CreateMap<Department, DepartmentDto>().ReverseMap();

        CreateMap<Role, RoleDto>().ReverseMap();
        CreateMap<Permission, PermissionDto>().ReverseMap();
    }
}