import { Alert, Card, Col, message, Row, Select } from 'antd';
import { DefaultOptionType } from 'antd/es/select';
import { useEffect } from 'react';

import XLoading from '@/components/common/loading';
import { apiClient } from '@/utils/client';
import { StoreDto } from '@/utils/swagger';
import { getSelectedStoreId, isStringEmpty, setSelectedStoreId } from '@/utils/utils';
import { useRequest } from 'ahooks';

export default () => {
  const query_manager_request = useRequest(apiClient.mallManager.storeManagerAuthStoreManagerList, { manual: true });

  const managerList = query_manager_request.data?.data.Data || [];

  const stores = managerList
    .map<StoreDto>((x) => x.Store || {})
    .filter((x) => !isStringEmpty(x.Id));

  useEffect(() => {
    query_manager_request.run();
  }, []);

  useEffect(() => {
    if (stores.length == 1) {
      const defaultStore: StoreDto = stores.at(0) || {};
      if (defaultStore.Id != getSelectedStoreId()) {
        setSelectedStoreId(defaultStore.Id || '');
        message.info(`已经自动为你选择门店：${defaultStore.Name || '--'}`);
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    }
  }, [stores]);

  const renderStoreSelector = () => {
    if (stores.length <= 0) {
      return <Alert
        type={'warning'}
        message={'无法进入系统'}
        description={'你不在任何门店任职，如有需要请联系系统管理员'} />;
    }

    const options = stores.map<DefaultOptionType>((x) => ({
      value: x.Id || '--',
      label: x.Name || '--',
    }));

    return (
      <>
        <div style={{ padding: 100 }}>
          <p style={{ marginBottom: 10 }}>
            <h3>选择需要进入的门店</h3>
          </p>
          <p>
            <Select
              style={{
                width: '100%',
              }}
              placeholder={'选择门店'}
              size={'large'}
              defaultActiveFirstOption
              options={options}
              onChange={(e) => {
                setSelectedStoreId(e);
                setTimeout(() => {
                  window.location.reload();
                }, 1000);
              }}
            />
          </p>
        </div>
      </>
    );
  };

  if (query_manager_request.loading) {
    return <XLoading />;
  }

  return (
    <>
      <Row
        justify='center'
        style={{
          paddingTop: '100px',
        }}
      >
        <Col span={12}>
          <Card title='提示'>{renderStoreSelector()}</Card>
        </Col>
      </Row>
    </>
  );
};
