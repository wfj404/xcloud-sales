using JetBrains.Annotations;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface ICategoryService : ISalesAppService
{
    Task<CategoryDto[]> GetNodePathAsync(Category category);

    Task<CategoryDto[]> AttachPictureMetaAsync(CategoryDto[] data);

    Task<CategoryDto[]> AttachNodePathAsync(CategoryDto[] data);

    Task TryFixTreeAsync();

    Task<CategoryDto> GetBySeoNameAsync(string seoName);

    Task<int> CountAsync();

    Task AdjustParentIdAsync([NotNull] string id, [NotNull] string parentId);

    Task<CategoryDto[]> GetAllAsync();

    Task UpdateNodePathAsync(string catId);

    Task<CategoryDto[]> GetAllChildrenAsync(string catId);

    Task InsertAsync(Category category);

    Task UpdateAsync(Category category);

    Task DeleteAsync(string id);

    Task<CategoryDto[]> GetByParentIdAsync([CanBeNull] string parentId = default);

    Task<CategoryDto> GetByIdAsync([NotNull] string id);
}

public class CategoryService : SalesAppService, ICategoryService
{
    private readonly ISalesRepository<Category> _categoryRepository;
    private readonly IStorageMetaService _storageMetaService;

    public CategoryService(
        ISalesRepository<Category> categoryRepository,
        IStorageMetaService storageMetaService)
    {
        _categoryRepository = categoryRepository;
        _storageMetaService = storageMetaService;
    }

    public async Task DeleteAsync(string id)
    {
        var children = await this.GetByParentIdAsync(id);

        if (children.Any())
            throw new UserFriendlyException("children exist");

        await this._categoryRepository.SoftDeleteAsync(id, this.Clock.Now);
    }

    public async Task<CategoryDto> GetBySeoNameAsync(string seoName)
    {
        if (string.IsNullOrWhiteSpace(seoName))
            throw new ArgumentNullException(nameof(seoName));

        var normalizedName = NormalizeSeoName(seoName);

        var category = await _categoryRepository.FirstOrDefaultAsync(x => x.SeoName == normalizedName);
        if (category == null)
            return null;

        return ObjectMapper.Map<Category, CategoryDto>(category);
    }

    private string NormalizeSeoName(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            return null;

        var normalizedName = name.Trim().RemoveWhitespace();
        return normalizedName;
    }

    public async Task<int> CountAsync()
    {
        var db = await _categoryRepository.GetDbContextAsync();

        var query = db.Set<Category>().AsNoTracking();

        var count = await query.CountAsync();

        return count;
    }

    public async Task AdjustParentIdAsync(string id, string parentId)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        if (string.IsNullOrWhiteSpace(parentId))
            throw new ArgumentNullException(nameof(parentId));

        if (id == parentId)
            throw new ArgumentException("id & parent id should not be equal");

        var allChildren = await this.GetAllChildrenAsync(id);
        var ids = allChildren.Ids().Append(id).Distinct().ToArray();

        var entity = await _categoryRepository.GetRequiredByIdAsync(id);

        if (string.IsNullOrWhiteSpace(parentId))
        {
            entity.ParentId = string.Empty;
        }
        else
        {
            if (ids.Contains(parentId))
            {
                throw new UserFriendlyException("loop reference");
            }

            var parent = await _categoryRepository.GetRequiredByIdAsync(parentId);
            entity.ParentId = parent.Id;
        }

        var all = await this.GetAllAsync();
        all.Where(x => x.Id != entity.Id).Append(entity).EnsureTreeNodes();

        await _categoryRepository.UpdateAsync(entity);

        foreach (var m in ids)
        {
            await UpdateNodePathAsync(m);
        }
    }

    public async Task<CategoryDto> GetByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var category = await _categoryRepository.FirstOrDefaultAsync(x => x.Id == id);

        if (category == null)
            return null;

        var dto = ObjectMapper.Map<Category, CategoryDto>(category);
        return dto;
    }

    public async Task<CategoryDto[]> GetByParentIdAsync(string parentId)
    {
        var db = await _categoryRepository.GetDbContextAsync();

        var query = db.Set<Category>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(parentId))
        {
            query = query.Where(x => x.ParentId == parentId);
        }
        else
        {
            query = query.Where(x => x.ParentId == null || x.ParentId == string.Empty);
        }

        var data = await query.OrderBy(x => x.CreationTime).ToArrayAsync();

        return data.MapArrayTo<Category, CategoryDto>(this.ObjectMapper);
    }

    public async Task<CategoryDto[]> AttachPictureMetaAsync(CategoryDto[] data)
    {
        var picIds = data
            .Select(x => x.PictureMetaId)
            .WhereNotEmpty()
            .Distinct()
            .ToArray();

        if (picIds.Any())
        {
            var pics = await this._storageMetaService.ByIdsAsync(picIds);
            foreach (var m in data)
            {
                var pic = pics.FirstOrDefault(x => x.Id == m.PictureMetaId);
                if (pic == null)
                    continue;

                m.Picture = pic;
                m.Picture.Simplify();
            }
        }

        return data;
    }

    public async Task<CategoryDto[]> AttachNodePathAsync(CategoryDto[] data)
    {
        if (!data.Any())
            return data;

        var all = await GetAllAsync();

        foreach (var m in data)
        {
            m.ParentNodes = all.GetNodePath(
                idSelector: x => x.Id,
                parentIdSelector: x => x.ParentId,
                nodeId: m.Id);
        }

        return data;
    }

    public async Task<CategoryDto[]> GetNodePathAsync(Category category)
    {
        var datalist = await this.GetAllAsync();

        var nodePath = datalist.GetNodePath(x => x.Id, x => x.ParentId, category.Id);

        return nodePath.MapArrayTo<Category, CategoryDto>(this.ObjectMapper);
    }

    public async Task<CategoryDto[]> GetAllChildrenAsync(string catId)
    {
        var datalist = await this.GetAllAsync();

        var node = datalist.FirstOrDefault(x => x.Id == catId) ?? throw new ArgumentNullException(nameof(catId));

        var children = datalist.FindNodeChildrenRecursively(node);

        return children.MapArrayTo<Category, CategoryDto>(this.ObjectMapper);
    }

    public async Task<CategoryDto[]> GetAllAsync()
    {
        var db = await _categoryRepository.GetDbContextAsync();

        var data = await db.Set<Category>().AsNoTracking().TakeUpTo5000().ToArrayAsync();

        return data.MapArrayTo<Category, CategoryDto>(this.ObjectMapper);
    }

    private async Task<bool> CheckSeoNameIsExistAsync(DbContext db, string normalizedName, string exceptIdOrNull)
    {
        var query = db.Set<Category>().AsNoTracking();

        query = query.Where(x => x.SeoName == normalizedName);

        if (!string.IsNullOrWhiteSpace(exceptIdOrNull))
        {
            query = query.Where(x => x.Id != exceptIdOrNull);
        }

        return await query.AnyAsync();
    }

    public async Task TryFixTreeAsync()
    {
        await using var _ =
            await DistributedLockProvider.CreateRequiredLockAsync(resource: "mall.category.try.fix.tree",
                expiryTime: TimeSpan.FromSeconds(30));

        //handle with dead nodes
        //await this.HandleWithDeadNodesAsync();

        //refresh root
        var allData = await GetAllAsync();
        foreach (var m in allData)
        {
            try
            {
                //node path
                await UpdateNodePathAsync(m.Id);
            }
            catch (Exception e)
            {
                Logger.LogError(message: e.Message, exception: e);
                await Task.Delay(TimeSpan.FromSeconds(3));
            }
        }
    }

    public async Task UpdateNodePathAsync(string catId)
    {
        var db = await _categoryRepository.GetDbContextAsync();
        var cat = await db.Set<Category>().FirstOrDefaultAsync(x => x.Id == catId);
        if (cat == null)
        {
            return;
        }

        var nodePath = await GetNodePathAsync(cat);
        var nodes = nodePath.Select(x => x.Id).ToArray();

        cat.RootId = nodes.FirstOrDefault();
        cat.NodePath = JsonDataSerializer.SerializeToString(nodes);

        await db.SaveChangesAsync();
    }

    public virtual async Task InsertAsync(Category category)
    {
        if (category == null)
            throw new ArgumentNullException(nameof(category));

        if (string.IsNullOrWhiteSpace(category.Name))
            throw new ArgumentNullException(nameof(category.Name));

        var db = await _categoryRepository.GetDbContextAsync();

        if (!string.IsNullOrWhiteSpace(category.ParentId))
        {
            var parent = await GetByIdAsync(category.ParentId);
            if (parent == null)
            {
                category.ParentId = string.Empty;
            }
        }

        category.SeoName = NormalizeSeoName(category.SeoName);

        if (!string.IsNullOrWhiteSpace(category.SeoName))
        {
            if (await CheckSeoNameIsExistAsync(db, category.SeoName, null))
            {
                throw new UserFriendlyException("seo name exist already");
            }
        }

        category.Id = GuidGenerator.CreateGuidString();
        category.CreationTime = Clock.Now;

        var all = await this.GetAllAsync();
        all.Append(category).EnsureTreeNodes();

        await _categoryRepository.InsertAsync(category);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await UpdateNodePathAsync(category.Id);
    }

    public virtual async Task UpdateAsync(Category dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        var category = await _categoryRepository.GetRequiredByIdAsync(dto.Id);

        category.Name = dto.Name;
        category.SeoName = dto.SeoName;
        category.Description = dto.Description;
        category.DisplayOrder = dto.DisplayOrder;
        category.PictureMetaId = dto.PictureMetaId;
        category.Published = dto.Published;
        category.Recommend = dto.Recommend;
        category.ShowOnHomePage = dto.ShowOnHomePage;

        var db = await _categoryRepository.GetDbContextAsync();
        category.SeoName = NormalizeSeoName(category.SeoName);
        if (!string.IsNullOrWhiteSpace(category.SeoName))
        {
            if (await CheckSeoNameIsExistAsync(db, category.SeoName, category.Id))
            {
                throw new UserFriendlyException("seo name exist already");
            }
        }

        var all = await this.GetAllAsync();
        all.Where(x => x.Id != category.Id).Append(category).EnsureTreeNodes();

        await _categoryRepository.UpdateAsync(category);
    }
}