import { apiClient } from '@/utils/client';
import { StoreUserDto, UpdateUserStatusInput } from '@/utils/swagger';
import { Button, Form, Input, message, Modal, Space, Switch, Tag } from 'antd';
import { useEffect, useState } from 'react';
import { handleResponse } from '@/utils/utils';

const AdminPage = (props: { ok: () => void; model: StoreUserDto }) => {
  const { ok, model } = props;
  const [loading, _loading] = useState(false);
  const [show, _show] = useState(false);

  const [form, _form] = useState<UpdateUserStatusInput>({});

  const save = (e: UpdateUserStatusInput) => {
    _loading(true);
    apiClient.mallAdmin
      .userUpdateStatus({ ...e })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok && ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _form({
      IsActive: model.IsActive,
      Id: model.Id,
    });
  }, [model]);

  return (
    <>
      <Modal
        title='修改状态'
        confirmLoading={loading}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        onOk={() => {
          save(form);
        }}
        style={{
          padding: 0,
        }}
      >
        <Form>
          <Form.Item hidden name={'Id'}>
            <Input />
          </Form.Item>
          <Form.Item label='是否激活'>
            <Switch checked={form.IsActive || false} onChange={e => {
              _form(d => ({ ...d, IsActive: e }));
            }} />
          </Form.Item>
        </Form>
      </Modal>
      <Space>
        {model.IsActive ? (
          <Tag color='green'>正常</Tag>
        ) : (
          <Tag color='error'>禁用</Tag>
        )}
        <Button
          size={'small'}
          type={'link'}
          onClick={() => {
            _show(true);
          }}
        >修改</Button>
      </Space>
    </>
  );
};

export default AdminPage;
