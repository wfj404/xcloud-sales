using System;
using XCloud.EntityFrameworkCore.Repository;
using XCloud.Sales.Application.Domain;
using XCloud.Sales.Application.Extended.Database.EntityFrameworkCore;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Extended.Database.Repository;

/// <summary>
/// Represents the Entity Framework repository
/// </summary>
/// <typeparam name="TEntity">Entity type</typeparam>
public class SalesMySqlRepository<TEntity> : EfRepositoryBase<ShopMySqlDbContext, TEntity>, ISalesRepository<TEntity> where TEntity : class, ISalesBaseEntity
{
    public SalesMySqlRepository(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        //
    }
}