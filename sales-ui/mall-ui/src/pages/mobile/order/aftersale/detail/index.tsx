import { useEffect, useState } from 'react';

import DotsLoading from '@/components/common/loading/dots';
import { OrderStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { AfterSalesDto, OrderDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

import XAction from './action';
import XComment from './comment';
import XGoods from './goods';
import XSummary from './summary';

export default ({ order }: { order: OrderDto }) => {
  const [data, _data] = useState<AfterSalesDto>({});
  const [loading, _loading] = useState(false);

  const queryData = () => {
    if (
      isStringEmpty(order.Id) ||
      order.OrderStatusId != OrderStatus.AfterSales
    ) {
      return;
    }
    _loading(true);
    apiClient.mall
      .afterSaleGetByOrderId({ Id: order.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryData();
  }, [order]);

  const renderDetail = () => {
    if (loading) {
      return <DotsLoading />;
    }

    if (isStringEmpty(data.Id)) {
      return null;
    }

    return (
      <>
        <XAction
          model={data}
          ok={() => {
            queryData();
          }}
        />
        <XSummary model={data} />
        <XGoods model={data} />
        <XComment model={data} />
      </>
    );
  };

  return (
    <>
      <Box sx={{}}>{renderDetail()}</Box>
    </>
  );
};
