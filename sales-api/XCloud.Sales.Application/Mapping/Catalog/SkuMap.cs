using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class SkuMap : SalesEntityTypeConfiguration<Sku>
{
    public override void Configure(EntityTypeBuilder<Sku> builder)
    {
        builder.ToTable("sales_sku");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();
        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeletionTimeEntity();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.SkuCode).HasMaxLength(100);
        builder.Property(x => x.Price).HasPrecision(18, 2);
        builder.Property(x => x.Color).HasMaxLength(100);
        builder.Property(x => x.MaxAmountInOnePurchase);
        builder.Property(x => x.MinAmountInOnePurchase);
        builder.Property(x => x.Weight);
        builder.Property(x => x.GoodsId).RequiredId();
        builder.Property(x => x.SpecCombinationJson).HasComment("规格配置参数");
        builder.Property(x => x.IsActive);

        base.Configure(builder);
    }
}