using XCloud.Application.Service;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderStockQuantityAdjustService : ISalesAppService
{
    Task DeductOrderInventoryAfterPaymentAsync(string orderId);

    Task DeductOrderInventoryAfterPlaceOrderAsync(string orderId);

    Task DeductOrderInventoryAsync(string orderId);

    Task ReturnBackOrderInventoryAsync(string orderId);
}

public class OrderStockQuantityAdjustService : SalesAppService, IOrderStockQuantityAdjustService
{
    private readonly IStoreGoodsStockService _storeGoodsStockService;
    private readonly ISalesRepository<OrderStockQuantityHistory> _repository;
    private readonly IOrderService _orderService;
    private readonly IOrderItemService _orderItemService;

    public OrderStockQuantityAdjustService(IStoreGoodsStockService storeGoodsStockService,
        ISalesRepository<OrderStockQuantityHistory> repository,
        IOrderService orderService,
        IOrderItemService orderItemService)
    {
        _storeGoodsStockService = storeGoodsStockService;
        _repository = repository;
        _orderService = orderService;
        _orderItemService = orderItemService;
    }

    private async Task<OrderStockQuantityHistory[]> GetHistoryAsync(string orderItemId)
    {
        var db = await this._repository.GetDbContextAsync();

        var historyList = await db.Set<OrderStockQuantityHistory>().AsNoTracking()
            .Where(x => x.OrderItemId == orderItemId)
            .ToArrayAsync();

        return historyList;
    }

    private async Task DeductOrderItemQuantityAsync(string orderItemId)
    {
        var historyList = await this.GetHistoryAsync(orderItemId);

        if (historyList.Any())
        {
            this.Logger.LogInformation($"{nameof(DeductOrderItemQuantityAsync)}.skip");
            return;
        }

        var orderItem = await this._orderItemService.GetByIdAsync(orderItemId);
        if (orderItem == null)
            throw new EntityNotFoundException(nameof(DeductOrderItemQuantityAsync));

        var order = await this._orderService.GetRequiredByIdAsync(orderItem.OrderId);

        await this._storeGoodsStockService.UpdateStockQuantityByOffsetAsync(
            skuId: orderItem.SkuId,
            storeId: order.StoreId,
            quantityOffset: -orderItem.Quantity);

        var history = new OrderStockQuantityHistory()
        {
            OrderItemId = orderItemId,
            CreationTime = this.Clock.Now
        };

        await this._repository.InsertAsync(history);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
    }

    public async Task DeductOrderInventoryAsync(string orderId)
    {
        var order = await this._orderService.GetRequiredByIdAsync(orderId);

        var orderItems = await this._orderItemService.ListByOrderIdAsync(order.Id);

        foreach (var m in orderItems)
        {
            await this.DeductOrderItemQuantityAsync(m.Id);
        }
    }

    public async Task DeductOrderInventoryAfterPlaceOrderAsync(string orderId)
    {
        var orderItems = await this._orderItemService.ListByOrderIdAsync(orderId);

        foreach (var m in orderItems)
        {
            if (m.StockDeductStrategy != SalesConstants.OrderStockDeductStrategy.AfterPlaceOrder)
                continue;

            await this.DeductOrderItemQuantityAsync(m.Id);
        }
    }

    public async Task DeductOrderInventoryAfterPaymentAsync(string orderId)
    {
        var order = await this._orderService.GetRequiredByIdAsync(orderId);

        if (!order.IsPaid())
            return;

        var orderItems = await this._orderItemService.ListByOrderIdAsync(orderId);

        foreach (var m in orderItems)
        {
            if (m.StockDeductStrategy != SalesConstants.OrderStockDeductStrategy.AfterPayment)
                continue;

            await this.DeductOrderItemQuantityAsync(m.Id);
        }
    }

    public async Task ReturnBackOrderInventoryAsync(string orderId)
    {
        var order = await this._orderService.GetRequiredByIdAsync(orderId);

        var orderItems = await this._orderItemService.ListByOrderIdAsync(orderId);

        var itemIds = orderItems.Ids();

        var db = await _repository.GetDbContextAsync();

        var histories = await db.Set<OrderStockQuantityHistory>()
            .Where(x => itemIds.Contains(x.OrderItemId))
            .ToArrayAsync();

        if (!histories.Any())
        {
            this.Logger.LogInformation($"{nameof(ReturnBackOrderInventoryAsync)}.skip");
            return;
        }

        foreach (var m in orderItems)
        {
            if (histories.FirstOrDefault(x => x.OrderItemId == m.Id) == null)
            {
                continue;
            }

            await _storeGoodsStockService.UpdateStockQuantityByOffsetAsync(
                skuId: m.SkuId,
                storeId: order.StoreId,
                quantityOffset: +m.Quantity);
        }

        await _repository.DeleteManyAsync(histories);
    }
}