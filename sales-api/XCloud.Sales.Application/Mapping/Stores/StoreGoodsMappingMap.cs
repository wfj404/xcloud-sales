﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Mapping.Stores;

public class StoreGoodsMappingMap : SalesEntityTypeConfiguration<StoreGoodsMapping>
{
    public override void Configure(EntityTypeBuilder<StoreGoodsMapping> builder)
    {
        builder.ToTable("sales_store_goods_mapping");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigModificationTimeEntity();

        builder.Property(m => m.StoreId).IsRequired().HasMaxLength(100);
        builder.Property(m => m.SkuId).RequiredId();
        builder.Property(m => m.StockQuantity);
        builder.Property(m => m.Price).PricePrecision();
        builder.Property(m => m.OverridePrice);
        builder.Property(m => m.Active);
        builder.Property(m => m.CreationTime);
        builder.Property(m => m.LastModificationTime);

        base.Configure(builder);
    }
}