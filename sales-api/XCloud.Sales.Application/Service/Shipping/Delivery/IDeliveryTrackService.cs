using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Shipping.Delivery;

public interface IDeliveryTrackService : ISalesAppService
{
    Task<DeliveryRecordTrackDto[]> QueryByShipmentIdAsync(string id);
}

public class DeliveryTrackService : SalesAppService, IDeliveryTrackService
{
    private readonly ISalesRepository<DeliveryRecordTrack> _repository;

    public DeliveryTrackService(ISalesRepository<DeliveryRecordTrack> repository)
    {
        _repository = repository;
    }

    public async Task<DeliveryRecordTrackDto[]> QueryByShipmentIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var data = await _repository.QueryManyAsync(x => x.DeliveryId == id, count: 500);

        return data.MapArrayTo<DeliveryRecordTrack, DeliveryRecordTrackDto>(ObjectMapper);
    }
}