import { useRequest, useTitle } from 'ahooks';
import { useEffect } from 'react';
import { history, useSnapshot } from 'umi';

import DotsLoading from '@/components/common/loading/dots';
import XHeader from '@/components/mobile/simpleHeader';
import { storeState, StoreType } from '@/store';
import { isStoreUserLogin } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { handleResponse, setAccessToken } from '@/utils/utils';
import { Box, Button, Container } from '@mui/material';

import XAvatar from './avatar';
import XIdentityName from './identityname';
import XItem from './item';
import XMobile from './mobile';
import XNickname from './nickname';

export default function () {
  useTitle('个人信息');

  const app = useSnapshot(storeState) as StoreType;

  const model = app.storeUser?.User || {};

  const query_profile_request = useRequest(
    apiClient.mall.userAuthQueryUserProfile,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          app.setStoreUserAuthResult(data.data || {});
        });
      },
    },
  );

  useEffect(() => {
    query_profile_request.run({});
  }, []);

  return (
    <>
      <XHeader>
        <Container maxWidth="sm" disableGutters>
          {query_profile_request.loading && <DotsLoading />}
          <Box
            sx={{
              backgroundColor: 'white',
              borderRadius: 1,
              overflow: 'hidden',
              mb: 2,
            }}
          >
            <XAvatar
              model={model}
              ok={() => {
                query_profile_request.run({});
              }}
            />
            <XIdentityName
              model={model}
              ok={() => {
                query_profile_request.run({});
              }}
            />
            <XNickname
              model={model}
              ok={() => {
                query_profile_request.run({});
              }}
            />
            <XMobile
              model={model}
              ok={() => {
                query_profile_request.run({});
              }}
            />
          </Box>
          <Box
            sx={{
              backgroundColor: 'white',
              borderRadius: 1,
              overflow: 'hidden',
              mb: 2,
            }}
          >
            <Box
              onClick={() => {
                //
              }}
            >
              <XItem title={'服务细则'} right={null} />
            </Box>
            <Box
              onClick={() => {
                //
              }}
            >
              <XItem title={'关于我们'} right={null} />
            </Box>
          </Box>
          {isStoreUserLogin(app.storeUser) && (
            <Box sx={{ margin: 2, mt: 4 }}>
              <Button
                fullWidth
                variant="contained"
                color="error"
                onClick={() => {
                  if (confirm('确定退出嘛？')) {
                    setAccessToken('');
                    history.push({
                      pathname: '/',
                    });
                  }
                }}
              >
                退出登录
              </Button>
            </Box>
          )}
        </Container>
      </XHeader>
    </>
  );
}
