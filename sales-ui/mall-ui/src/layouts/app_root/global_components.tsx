import { parse } from 'query-string';
import React, { ReactNode, useEffect, useState } from 'react';
import { useLocation } from 'umi';

import XDetailPreview from '@/components/goods/detail/detailPreview';
import { GlobalContainer } from '@/utils/utils';

export default ({ children }: { children: ReactNode }) => {
  const [detailId, setDetailId] = React.useState<string | undefined | null>(
    undefined,
  );

  const location = useLocation();
  const q: { preview_goods_id?: string } = parse(location.search) || {};
  const { preview_goods_id } = q;

  useEffect(() => {
    preview_goods_id && setDetailId(preview_goods_id);
  }, [preview_goods_id]);

  useEffect(() => {
    GlobalContainer.setGoodsDetailId = (x) => setDetailId(x);
    return () => {
      GlobalContainer.setGoodsDetailId = undefined;
    };
  }, []);

  return (
    <>
      <XDetailPreview
        detailId={detailId}
        onClose={() => {
          setDetailId(undefined);
        }}
      />
      <div style={{}}>{children}</div>
    </>
  );
};
