using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Shipping.Delivery.Express;

public interface
    IFreightTemplateService : ISalesStringCrudAppService<FreightTemplate, FreightTemplateDto,
    PagedRequest>
{
    Task<FreightTemplateDto[]> QueryAllAsync(string storeId);

    Task<GoodsDto[]> AttachDeliveryTemplateAsync(GoodsDto[] data, string deliveryMethodId);
}

public class
    FreightTemplateService : SalesStringCrudAppService<FreightTemplate, FreightTemplateDto,
    PagedRequest>, IFreightTemplateService
{
    private readonly SalesUtils _salesUtils;

    public FreightTemplateService(ISalesRepository<FreightTemplate> repository,
        SalesUtils salesUtils) : base(repository)
    {
        _salesUtils = salesUtils;
        //
    }

    protected override async Task SetFieldsBeforeInsertAsync(FreightTemplate entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
    }

    protected override async Task SetFieldsBeforeUpdateAsync(FreightTemplate entity, FreightTemplateDto dto)
    {
        await Task.CompletedTask;

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.ShipInHours = dto.ShipInHours;
        entity.FreeShippingMinOrderTotalPrice = dto.FreeShippingMinOrderTotalPrice;
        entity.CalculateType = dto.CalculateType;
        entity.RulesJson = dto.RulesJson;
        entity.ExcludedAreaJson = dto.ExcludedAreaJson;
        entity.Sort = dto.Sort;
    }

    public async Task<FreightTemplateDto[]> QueryAllAsync(string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var db = await Repository.GetDbContextAsync();

        var data = await db.Set<FreightTemplate>().AsNoTracking()
            .Where(x => x.StoreId == storeId)
            .TakeUpTo5000().ToArrayAsync();

        return data.MapArrayTo<FreightTemplate, FreightTemplateDto>(ObjectMapper);
    }

    public async Task<GoodsDto[]> AttachDeliveryTemplateAsync(GoodsDto[] data, string deliveryMethodId)
    {
        if (string.IsNullOrWhiteSpace(deliveryMethodId))
            throw new ArgumentNullException(nameof(deliveryMethodId));

        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await Repository.GetDbContextAsync();

        var templateQuery = db.Set<FreightTemplate>().AsNoTracking();

        var mappingQuery = db.Set<FreightTemplateGoodsMapping>().AsNoTracking();

        var query = from mapping in mappingQuery
            join template in templateQuery
                on mapping.TemplateId equals template.Id
            select new { template, mapping };

        query = query.Where(x => ids.Contains(x.mapping.GoodsId));

        var templates = await query.ToArrayAsync();

        foreach (var m in data)
        {
            m.CurrentFreightTemplate = templates.OrderByDescending(x => x.mapping.CreationTime)
                .FirstOrDefault(x => x.mapping.GoodsId == m.Id)?.template
                .MapTo<FreightTemplate, FreightTemplateDto>(ObjectMapper);
        }

        return data;
    }
}