import { Grid, Space } from 'antd-mobile';

import XPicture from '@/components/blocks/components/block_picture';
import { Box } from '@mui/material';

import { GalleryBlockProps } from './utils';

export default ({ data }: { data?: GalleryBlockProps }) => {
  if (!data) {
    return null;
  }

  const gap = data.gutter || 0;

  if (data.style == '1-column') {
    return (
      <Box sx={{ p: 0 }}>
        <XPicture data={data.pictureList?.at(0)} />
      </Box>
    );
  }

  if (data.style == '2-columns') {
    return (
      <Grid columns={2} gap={gap}>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(0)} />
        </Grid.Item>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(1)} />
        </Grid.Item>
      </Grid>
    );
  }

  if (data.style == '3-columns') {
    return (
      <Grid columns={3} gap={gap}>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(0)} />
        </Grid.Item>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(1)} />
        </Grid.Item>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(2)} />
        </Grid.Item>
      </Grid>
    );
  }

  if (data.style == '4-grids') {
    return (
      <Grid columns={2} gap={gap}>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(0)} />
        </Grid.Item>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(1)} />
        </Grid.Item>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(2)} />
        </Grid.Item>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(3)} />
        </Grid.Item>
      </Grid>
    );
  }

  if (data.style == 'style-1') {
    return (
      <Grid columns={2} gap={gap}>
        <Grid.Item>
          <XPicture data={data.pictureList?.at(0)} />
        </Grid.Item>
        <Grid.Item>
          <Space
            block
            direction={'vertical'}
            align={'center'}
            justify={'around'}
          >
            <XPicture data={data.pictureList?.at(1)} />
            <XPicture data={data.pictureList?.at(2)} />
          </Space>
        </Grid.Item>
      </Grid>
    );
  }

  return (
    <>
      <p>component error</p>
    </>
  );
};
