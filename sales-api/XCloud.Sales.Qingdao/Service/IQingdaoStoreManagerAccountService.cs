﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Qingdao.Service;

public interface IQingdaoStoreManagerAccountService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<StoreManagerDto> PrepareStoreManagerAsync(StoreDto store, UserDto user, WapAdminDto wapAdminDto);
}

public class QingdaoStoreManagerAccountService : SalesAppService, IQingdaoStoreManagerAccountService
{
    private readonly IStoreManagerAccountService _storeManagerAccountService;
    private readonly IStoreManagerService _storeManagerService;

    public QingdaoStoreManagerAccountService(IStoreManagerAccountService storeManagerAccountService,
        IStoreManagerService storeManagerService)
    {
        _storeManagerAccountService = storeManagerAccountService;
        _storeManagerService = storeManagerService;
    }

    public async Task<StoreManagerDto> PrepareStoreManagerAsync(StoreDto store, UserDto user, WapAdminDto wapAdminDto)
    {
        if (store == null)
            throw new ArgumentNullException(nameof(store));

        if (user == null)
            throw new ArgumentNullException(nameof(user));

        if (wapAdminDto == null)
            throw new ArgumentNullException(nameof(wapAdminDto));

        var storeManager =
            await this._storeManagerAccountService.GetOrCreateByUserIdAsync(storeId: store.Id, userId: user.Id);

        if (!storeManager.IsSuperManager || storeManager.IsActive != wapAdminDto.active)
        {
            await this._storeManagerAccountService.UpdateStatusAsync(new UpdateStoreManagerStatusInput()
            {
                ManagerId = storeManager.Id,
                IsActive = wapAdminDto.active,
                IsSuperManager = true
            });
        }

        await this.RemoveFromOtherStoresAsync(
            userId: storeManager.UserId,
            storeId: storeManager.StoreId);

        return storeManager;
    }

    private async Task RemoveFromOtherStoresAsync(string userId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var managers = await this._storeManagerService.ListActiveManagersByUserIdAsync(userId);

        managers = managers.Where(x => x.StoreId != storeId).ToArray();

        foreach (var m in managers)
        {
            await this._storeManagerAccountService.UpdateStatusAsync(new UpdateStoreManagerStatusInput()
            {
                ManagerId = m.Id,
                IsActive = false,
                IsSuperManager = false
            });
        }
    }
}