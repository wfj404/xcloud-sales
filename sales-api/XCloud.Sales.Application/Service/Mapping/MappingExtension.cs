using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Utils;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Mapping;

namespace XCloud.Sales.Application.Service.Mapping;

public static class MappingExtension
{
    public static void PreConfigEntityMappingEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, IMappingTable
    {
        builder.Property(x => x.EntityName).IsRequired().HasMaxLength(500);
        builder.Property(x => x.EntityId).RequiredId();

        builder.Property(x => x.TargetName).IsRequired().HasMaxLength(500);
        builder.Property(x => x.TargetId).RequiredId();
    }

    public static IQueryable<MappingEntity> WhereMappingFromTo<MappingEntity, MappingFromType, MappingToType>(
        this IQueryable<MappingEntity> query,
        CommonUtils commonUtils)
        where MappingEntity : class, IMappingTable
        where MappingFromType : class, IMappingSupported
        where MappingToType : class, IMappingSupported
    {
        var fromTypeName = commonUtils.GetRequiredTypeIdentityName<MappingFromType>();
        var toTypeName = commonUtils.GetRequiredTypeIdentityName<MappingToType>();

        var mappingQuery = query.Where(x => x.EntityName == fromTypeName && x.TargetName == toTypeName);

        return mappingQuery;
    }

    public static IQueryable<T> WithTypes<T>(this IQueryable<T> queryable, string entityName, string targetName)
        where T : class, IMappingTable
    {
        return queryable.Where(x => x.EntityName == entityName && x.TargetName == targetName);
    }

    public static async Task SaveMappingEntitiesAsync<SourceEntity, TargetEntity, MappingEntity>(
        this DbContext db, SourceEntity entity, string[] targetIds, CommonUtils commonUtils)
        where SourceEntity : class, IMappingSupported
        where TargetEntity : class, IMappingSupported
        where MappingEntity : class, IMappingTable, new()
    {
        if (string.IsNullOrWhiteSpace(entity.Id))
            throw new ArgumentNullException(nameof(entity));

        if (targetIds == null)
            throw new ArgumentNullException(nameof(targetIds));

        targetIds = targetIds.WhereNotEmpty().Distinct().ToArray();

        var sourceTypeName = commonUtils.GetRequiredTypeIdentityName<SourceEntity>();
        var targetTypeName = commonUtils.GetRequiredTypeIdentityName<TargetEntity>();

        var mappings = targetIds.Select(x => new MappingEntity
        {
            EntityId = entity.Id,
            EntityName = sourceTypeName,
            TargetId = x,
            TargetName = targetTypeName
        }).ToArray();

        var set = db.Set<MappingEntity>();

        var origin = await set
            .WithTypes(sourceTypeName, targetTypeName)
            .Where(x => x.EntityId == entity.Id)
            .ToArrayAsync();

        string FingerPrint(MappingEntity x) => $"{x.EntityId}.{x.EntityName}.{x.TargetId}.{x.TargetName}";

        var deleteList = origin.NotInBy(mappings, FingerPrint).ToArray();
        var insertList = mappings.NotInBy(origin, FingerPrint).ToArray();

        if (deleteList.Any())
        {
            set.RemoveRange(deleteList);
        }

        if (insertList.Any())
        {
            set.AddRange(insertList);
        }

        await db.TrySaveChangesAsync();
    }
}