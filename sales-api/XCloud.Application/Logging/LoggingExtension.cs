﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Core.Exceptions;
using XCloud.Core.Extension;

namespace XCloud.Application.Logging;

public static class LoggingExtension
{
    /// <summary>
    /// log all inner exceptions
    /// </summary>
    public static void LogExceptionX(this ILogger logger, Exception exception, string message = default)
    {
        var handledExceptionList = new List<Exception>();

        void HandleException([NotNull] string[] previousMessages, [CanBeNull] Exception e)
        {
            if (e == null)
            {
                return;
            }

            var handled = handledExceptionList.Any(x => object.ReferenceEquals(x, e));

            if (handled)
            {
                return;
            }

            handledExceptionList.Add(e);

            var messagePath = new List<string>()
                .AddManyItems(previousMessages)
                .Append(e.Message)
                .WhereNotEmpty()
                .ToArray();

            HandleException(messagePath, e.InnerException);

            logger.LogError(message: string.Join("->", messagePath), exception: e);
        }

        HandleException(new string[] { message }, exception);
    }

    public static IDisposable Metric(this ILogger logger, string message)
    {
        if (string.IsNullOrWhiteSpace(message))
            throw new ArgumentNullException(nameof(message));

        if (logger == null)
            throw new ArgumentNullException(nameof(logger));

        var watch = new Stopwatch();

        watch.Start();

        return new DisposeAction(() =>
        {
            watch.Stop();

            logger.LogInformation($"{message}:耗时：{watch.Elapsed.TotalSeconds}秒");
        });
    }

    internal static void AddLoggingProvider(this ServiceConfigurationContext context)
    {
        context.Services.AddLogging(loggingBuilder =>
        {
            loggingBuilder.ClearProviders();

            loggingBuilder.AddFilter(level => true);

            loggingBuilder.AddSerilogProvider(context);
        });
    }

    [NotNull]
    public static LoggingOption GetLoggingOptionOrDefault(this IConfiguration configuration)
    {
        var option = new LoggingOption();

        var section = configuration.GetSection("LoggingConfig");

        if (section.Exists())
        {
            section.Bind(option);
        }

        return option;
    }

    public static string GetOrCreateLoggingPath(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        var envOrNull = context.Services.GetSingletonInstanceOrNull<IWebHostEnvironment>();

        var configuredLoggingDir = config.GetLoggingOptionOrDefault().Disk.BasePath;

        if (string.IsNullOrWhiteSpace(configuredLoggingDir))
        {
            configuredLoggingDir = Path.Combine(
                envOrNull?.ContentRootPath ?? Directory.GetCurrentDirectory(),
                "logs");
        }

        if (string.IsNullOrWhiteSpace(configuredLoggingDir))
            throw new ConfigException($"{nameof(configuredLoggingDir)} is required");

        new DirectoryInfo(configuredLoggingDir).CreateIfNotExist();

        return configuredLoggingDir;
    }
}