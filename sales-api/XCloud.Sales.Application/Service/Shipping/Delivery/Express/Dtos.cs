﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Utils;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Service.Shipping.Delivery.Express;

public class DeliveryRule : IEntityDto
{
    public bool IsDefault { get; set; }

    private DeliveryArea[] _areas;

    [NotNull]
    public DeliveryArea[] Areas
    {
        get => this._areas ??= [];
        set => this._areas = value;
    }

    public TieredPrice TieredPrice { get; set; }
}

public class FreightTemplateDto : FreightTemplate, IEntityDto<string>
{
    private DeliveryRule[] _rules;

    [NotNull]
    public DeliveryRule[] Rules
    {
        get => this._rules ??= [];
        set => this._rules = value;
    }

    private DeliveryArea[] _excludedAreas;

    [NotNull]
    public DeliveryArea[] ExcludedAreas
    {
        get => this._excludedAreas ??= [];
        set => this._excludedAreas = value;
    }
}

[TypeIdentityName("store-express-settings")]
public class StoreExpressSettings : ShippingSettingsBase
{
    //
}