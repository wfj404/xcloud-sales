namespace XCloud.Sales.Application.Domain.Orders;

public class OrderReview : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime
{
    public string OrderId { get; set; }

    public bool Approved { get; set; }

    public string ReviewText { get; set; }

    public double Rating { get; set; }

    public string PictureMetaIdsJson { get; set; }

    public string ExtraData { get; set; }

    public string IpAddress { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}