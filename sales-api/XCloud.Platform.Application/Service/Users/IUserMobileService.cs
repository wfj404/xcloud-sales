﻿using System.Threading.Tasks;
using Masuit.Tools;
using Volo.Abp.Domain.Entities;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Users;

/// <summary>
/// 用户手机号
/// </summary>
public interface IUserMobileService : IXCloudAppService
{
    Task<UserDto[]> AttachAccountMobileAsync(UserDto[] data);

    string NormalizeMobilePhone(string mobile);

    Task<bool> IsPhoneExistAsync(string phone, string exceptUserId = default);

    Task<ApiResponse<UserIdentity>> SetUserMobilePhoneAsync(string userId, string phone);

    Task<UserMobileDto> GetMobileByUserIdAsync(string userId);

    Task SendSmsAsync(UserPhoneBindSmsMessage message);

    Task<User> GetUserByPhoneAsync(string phone);

    Task RemoveUserMobilesAsync(string userId);

    Task ConfirmMobileAsync(string identityId);
}

public class UserMobileService : PlatformAppService, IUserMobileService
{
    private readonly IPlatformRepository<UserIdentity> _userMobileRepository;

    public UserMobileService(IPlatformRepository<UserIdentity> userMobileRepository)
    {
        _userMobileRepository = userMobileRepository;
    }

    private int IdentityType => (int)UserIdentityTypeEnum.MobilePhone;

    public async Task<UserDto[]> AttachAccountMobileAsync(UserDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _userMobileRepository.GetDbContextAsync();

        var ids = data.Ids();

        var identityType = IdentityType;
        var mobileList = await db.Set<UserIdentity>().AsNoTracking()
            .Where(x => ids.Contains(x.UserId) && x.IdentityType == identityType)
            .ToArrayAsync();

        foreach (var m in data)
        {
            var list = mobileList.Where(x => x.UserId == m.Id).ToArray();
            m.UserMobile = list.Select(x => new UserMobileDto(x)).FirstOrDefault();
        }

        return data;
    }

    public string NormalizeMobilePhone(string mobile)
    {
        return AccountUtils.NormalizeMobilePhone(mobile);
    }

    public async Task RemoveUserMobilesAsync(string userId)
    {
        var db = await _userMobileRepository.GetDbContextAsync();

        var identityType = IdentityType;

        var set = db.Set<UserIdentity>();

        var mobiles = await set.Where(x => x.IdentityType == identityType && x.UserId == userId).ToArrayAsync();

        if (mobiles.Any())
            set.RemoveRange(mobiles);

        await db.TrySaveChangesAsync();
    }

    private void CheckMobilePhone(string mobilePhone)
    {
        if (string.IsNullOrWhiteSpace(mobilePhone))
            throw new UserFriendlyException("手机号码不能为空");

        if (!mobilePhone.MatchPhoneNumber())
            throw new UserFriendlyException("手机号格式不对");
    }

    public async Task<UserMobileDto> GetMobileByUserIdAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var db = await _userMobileRepository.GetDbContextAsync();

        var identityType = IdentityType;

        var data = await db.Set<UserIdentity>().AsNoTracking()
            .Where(x => x.UserId == userId && x.IdentityType == identityType)
            .OrderByDescending(x => x.CreationTime)
            .ToArrayAsync();

        var res = data.Select(x => new UserMobileDto(x)).FirstOrDefault();

        return res;
    }

    public async Task<User> GetUserByPhoneAsync(string phone)
    {
        phone = AccountUtils.NormalizeMobilePhone(phone);

        var db = await _userMobileRepository.GetDbContextAsync();

        var query = from userIdentity in db.Set<UserIdentity>().AsNoTracking()
            join u in db.Set<User>().AsNoTracking()
                on userIdentity.UserId equals u.Id into userGrouping
            from user in userGrouping.DefaultIfEmpty()
            select new { userIdentity, user };

        var identityType = IdentityType;

        query = query.Where(
            x => x.userIdentity.IdentityType == identityType && x.userIdentity.Identity == phone);

        var data = await query.FirstOrDefaultAsync();

        if (data == null)
            return null;

        return data.user;
    }

    public async Task<bool> IsPhoneExistAsync(string phone, string exceptUserId = default)
    {
        if (string.IsNullOrWhiteSpace(phone))
            throw new ArgumentNullException(nameof(phone));

        phone = AccountUtils.NormalizeMobilePhone(phone);

        var db = await _userMobileRepository.GetDbContextAsync();

        var identityType = IdentityType;

        var query = db.Set<UserIdentity>().AsNoTracking().IgnoreQueryFilters()
            .Where(x => x.IdentityType == identityType && x.Identity == phone);

        if (!string.IsNullOrWhiteSpace(exceptUserId))
            query = query.Where(x => x.UserId != exceptUserId);

        return await query.AnyAsync();
    }

    public async Task ConfirmMobileAsync(string identityId)
    {
        if (string.IsNullOrWhiteSpace(identityId))
            throw new ArgumentNullException(nameof(ConfirmMobileAsync));

        var db = await _userMobileRepository.GetDbContextAsync();

        var set = db.Set<UserIdentity>();

        var identityType = IdentityType;

        var mobile = await set.FirstOrDefaultAsync(x => x.Id == identityId && x.IdentityType == identityType);
        if (mobile == null)
            throw new EntityNotFoundException(nameof(ConfirmMobileAsync));

        mobile.MobileConfirmed = true;
        mobile.MobileConfirmedTimeUtc = Clock.Now;

        await db.TrySaveChangesAsync();
    }

    public async Task<ApiResponse<UserIdentity>> SetUserMobilePhoneAsync(string userId, string phone)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        if (string.IsNullOrWhiteSpace(phone))
            throw new ArgumentNullException(nameof(phone));

        phone = AccountUtils.NormalizeMobilePhone(phone);

        CheckMobilePhone(phone);

        if (await IsPhoneExistAsync(phone, userId))
            return new ApiResponse<UserIdentity>().SetError("mobile phone is already exist");

        var db = await _userMobileRepository.GetDbContextAsync();

        var set = db.Set<UserIdentity>();

        var identityType = IdentityType;

        var data = await set.Where(x => x.UserId == userId && x.IdentityType == identityType).ToArrayAsync();
        if (data.Any())
            set.RemoveRange(data);

        var map = new UserIdentity
        {
            UserId = userId,
            Identity = phone,
            MobilePhone = phone,
            MobileAreaCode = "",
            MobileConfirmed = false,
            MobileConfirmedTimeUtc = null,
            IdentityType = identityType,
            Id = GuidGenerator.CreateGuidString(),
            CreationTime = Clock.Now
        };

        set.Add(map);

        await db.SaveChangesAsync();

        return new ApiResponse<UserIdentity>(map);
    }

    public Task SendSmsAsync(UserPhoneBindSmsMessage message)
    {
        throw new NotImplementedException();
    }
}