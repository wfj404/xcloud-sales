import {
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Input,
  Row,
  Select,
  Space,
} from 'antd';
import { useEffect, useState } from 'react';

import XBrandSelector from '@/components/manage/brand_selector';
import XCategorySelector from '@/components/manage/category_selector';
import XMultipleTagSelector from '@/components/manage/multiple_tag_selector';
import { QueryGoodsPaging } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { SearchOutlined } from '@ant-design/icons';

export default ({
  query,
  onSearch,
}: {
  query: QueryGoodsPaging;
  onSearch: (d: QueryGoodsPaging) => void;
}) => {
  const [model, _model] = useState<QueryGoodsPaging>({ ...query });

  const triggerSearch = (e: QueryGoodsPaging) => {
    onSearch &&
      onSearch({
        ...e,
        Page: 1,
      });
  };

  useEffect(() => {
    _model({
      ...query,
    });
  }, [query]);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        labelCol={{
          span: 8,
        }}
        autoComplete="off"
      >
        <Row gutter={10}>
          <Col span={6}>
            <Form.Item label="关键词">
              <Input
                value={model.Keywords || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Keywords: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="上下架">
              <Select
                allowClear
                value={model.IsPublished}
                onChange={(e) => {
                  _model({
                    ...model,
                    IsPublished: e,
                  });
                }}
                onClear={() => {
                  _model({
                    ...model,
                    IsPublished: undefined,
                  });
                }}
                options={[
                  {
                    value: true,
                    label: '上架',
                  },
                  {
                    value: false,
                    label: '下架',
                  },
                ]}
              ></Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="品牌">
              <XBrandSelector
                value={model.BrandId}
                onChange={(e) => {
                  _model({ ...model, BrandId: e?.Id || undefined });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="分类">
              <XCategorySelector
                value={model.CategoryId}
                onChange={(e) => {
                  _model({ ...model, CategoryId: e?.Id || undefined });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="标签">
              <XMultipleTagSelector
                single_select
                value={[model.TagId || ''].filter((x) => !isStringEmpty(x))}
                onChange={(e) => {
                  _model({ ...model, TagId: e.at(0)?.Id || undefined });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label="没有品牌">
              <Checkbox
                checked={!!model.WithoutBrand}
                onChange={(e) => {
                  _model({
                    ...model,
                    WithoutBrand: e.target.checked,
                  });
                }}
              />
            </Form.Item>
          </Col>

          <Col span={6}>
            <Form.Item label="没有分类">
              <Checkbox
                checked={!!model.WithoutCategory}
                onChange={(e) => {
                  _model({
                    ...model,
                    WithoutCategory: e.target.checked,
                  });
                }}
              />
            </Form.Item>
          </Col>

          <Col span={6}>
            <Space>
              <Button
                type="primary"
                htmlType="submit"
                icon={<SearchOutlined />}
                onClick={() => triggerSearch(model)}
              >
                搜索
              </Button>
            </Space>
          </Col>
          <Col span={6}></Col>
        </Row>
      </Form>
    </Card>
  );
};
