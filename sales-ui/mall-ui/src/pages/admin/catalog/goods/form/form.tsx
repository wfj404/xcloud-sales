import { Alert, Card, Col, Form, Input, InputRef, Row, Switch, Typography } from 'antd';
import React, { useEffect, useState } from 'react';

import UrlEditor from '@/components/link/url';
import XAreaSelector from '@/components/manage/area_selector';
import XBrandSelector from '@/components/manage/brand_selector';
import XCategorySelector from '@/components/manage/category_selector';
import XMultipleTagSelector from '@/components/manage/multiple_tag_selector';
import XMDEditor from '@/components/markdown/editor';
import { GoodsType, OrderStockDeductStrategyList } from '@/utils/biz';
import { GoodsDto } from '@/utils/swagger';
import { getPinyinSeoNameStyle, isStringEmpty, parseJsonOrEmpty } from '@/utils/utils';
import { CheckCard } from '@ant-design/pro-components';

export default ({
  data,
  ok,
  show,
}: {
  data?: GoodsDto;
  ok: (d: GoodsDto) => void;
  show: boolean;
}) => {
  const [loading, _loading] = useState(false);

  const model: GoodsDto = {
    ...data,
    RedirectToUrl: parseJsonOrEmpty(data?.RedirectToUrlJson) || {},
  };
  const setGoodsModel = ok;

  const formEnter = React.useRef<InputRef>(null);

  useEffect(() => {
    if (show) {
      setTimeout(() => {
        formEnter.current?.focus();
      }, 100);
    }
  }, [show]);

  const renderContact = () => {
    if (model.GoodsType != GoodsType.Service) {
      //return null;
    }

    return (
      <Row gutter={10}>
        <Col span={6}>
          <Form.Item label="只允许电话咨询">
            <Switch
              checked={model.LimitedToContact}
              onChange={(e) => {
                setGoodsModel({
                  ...model,
                  LimitedToContact: e,
                });
              }}
            />
          </Form.Item>
        </Col>
        <Col span={18}>
          <Form.Item label="电话号码" tooltip={'开启后展示联系方式'}>
            <Input
              placeholder={'400-88888-xxx'}
              disabled={!model.LimitedToContact}
              value={model.LimitedToContact ? model.ContactTelephone || '' : ''}
              onChange={(e) => {
                setGoodsModel({
                  ...model,
                  ContactTelephone: e.target.value,
                });
              }}
            />
          </Form.Item>
        </Col>
      </Row>
    );
  };

  const renderJump = () => {
    if (model.GoodsType != GoodsType.Service) {
      //return null;
    }

    return (
      <>
        <Row gutter={10}>
          <Col span={6}>
            <Form.Item
              label="自动跳转"
              tooltip={'开启后商品详情页展示跳转按钮'}
            >
              <Switch
                checked={model.LimitedToRedirect}
                onChange={(e) => {
                  setGoodsModel({
                    ...model,
                    LimitedToRedirect: e,
                  });
                }}
              />
            </Form.Item>
          </Col>
          <Col span={18}>
            <Form.Item label="跳转链接">
              {model.LimitedToRedirect && (
                <>
                  {isStringEmpty(model.RedirectToUrlJson) || (
                    <Typography.Paragraph ellipsis={true} code>
                      {model.RedirectToUrlJson}
                    </Typography.Paragraph>
                  )}
                  <UrlEditor
                    link={model.RedirectToUrl || {}}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        RedirectToUrl: e,
                        RedirectToUrlJson: JSON.stringify(e),
                      });
                    }}
                  />
                </>
              )}
              {model.LimitedToRedirect || <span>...</span>}
            </Form.Item>
          </Col>
        </Row>
      </>
    );
  };

  return (
    <>
      <Card
        title="基础信息"
        loading={loading}
        style={{
          marginBottom: 10,
        }}
      >
        <Form
          labelCol={{ flex: '110px' }}
          labelAlign="right"
          wrapperCol={{ flex: 1 }}
        >
          <Form.Item label="商品类型" tooltip="创建后不可以修改">
            <CheckCard.Group
              multiple={false}
              value={model.GoodsType}
              onChange={(e) => {
                setGoodsModel({
                  ...model,
                  GoodsType: e as number,
                });
              }}
              disabled={!isStringEmpty(model.Id)}
              options={[
                {
                  title: '实物商品',
                  description: '需要物流',
                  value: GoodsType.Normal,
                },
                {
                  title: '虚拟商品',
                  description: '下单生成核销券，无需物流',
                  value: GoodsType.Virtual,
                  disabled: true,
                },
                {
                  title: '服务商品',
                  description: '下单后预约上门服务',
                  value: GoodsType.Service,
                },
              ]}
            ></CheckCard.Group>
          </Form.Item>
          <Row gutter={10}>
            <Col span={12}>
              <Form.Item label="名称" required>
                <Input
                  ref={formEnter}
                  autoFocus
                  placeholder="输入商品名称，比如【膳魔师保温杯】"
                  count={{
                    show: true,
                    max: 20,
                  }}
                  maxLength={20}
                  required
                  value={model.Name || ''}
                  onChange={(e) => {
                    const tryGenerateSeoName = (): string | undefined => {
                      if (!isStringEmpty(model.Id)) {
                        return model.SeoName || undefined;
                      }

                      return getPinyinSeoNameStyle(e.target.value || '');
                    };

                    setGoodsModel({
                      ...model,
                      Name: e.target.value,
                      SeoName: tryGenerateSeoName(),
                    });
                  }}
                />
                {isStringEmpty(model.Name) && (
                  <p style={{ color: 'red' }}>不能为空</p>
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Seo Name"
                tooltip={'在新建时自动生成，后续需要手动修改更新！'}
              >
                <Input
                  value={model.SeoName || ''}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      SeoName: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={10}>
            <Col span={12}>
              <Form.Item<GoodsDto> label="简短介绍" rules={[{ max: 50 }]}>
                <Input.TextArea
                  placeholder="将在商品详情页显示"
                  value={model.Description || ''}
                  count={{
                    show: true,
                    max: 100,
                  }}
                  maxLength={100}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      Description: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item label="店家评论" rules={[{ max: 50 }]}>
                <Input.TextArea
                  placeholder="可以显示在搜索结果中"
                  value={model.AdminComment || ''}
                  count={{
                    show: true,
                    max: 100,
                  }}
                  maxLength={100}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      AdminComment: e.target.value,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={10}>
            <Col span={8}>
              <Form.Item label="品牌">
                <XBrandSelector
                  value={model.BrandId}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      BrandId: e?.Id,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="分类">
                <XCategorySelector
                  value={model.CategoryId}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      CategoryId: e?.Id,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item label="标签">
                <XMultipleTagSelector
                  value={model.Tags?.map((x) => x.Id || '') || []}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      Tags: e,
                    });
                  }}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="产地">
                <XAreaSelector
                  value={model.AreaId}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      AreaId: e?.Id,
                    });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          {renderContact()}

          {renderJump()}

          <Row gutter={10}>
            <Col span={24}>
              <Form.Item label="库存扣减策略">
                <CheckCard.Group
                  bordered
                  value={model.StockDeductStrategy || ''}
                  onChange={(e) => {
                    setGoodsModel({
                      ...model,
                      StockDeductStrategy: e as string,
                    });
                  }}
                  options={OrderStockDeductStrategyList.map((x) => ({
                    title: x.name || '',
                    value: x.id || '',
                  }))}
                ></CheckCard.Group>
              </Form.Item>
            </Col>
          </Row>

          <Card size={'small'} title={'状态'} bordered={false}>
            <Row gutter={10}>
              <Col span={6}>
                <Form.Item label="置顶展示">
                  <Switch
                    checked={model.StickyTop}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        StickyTop: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="在售">
                  <Switch
                    checked={model.Published}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        Published: e,
                      });
                    }}
                  />
                  {model.Published || (
                    <Alert message={'商品下所有sku都不能销售'} />
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card size={'small'} title={'限制'} bordered={false}>
            <Row gutter={10}>
              <Col span={6}>
                <Form.Item label="支持下单">
                  <Switch
                    checked={model.PlaceOrderSupported}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        PlaceOrderSupported: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="支持售后">
                  <Switch
                    checked={model.AfterSalesSupported}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        AfterSalesSupported: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card size={'small'} title={'配送'} bordered={false}>
            <Row gutter={10}>
              <Col span={6}>
                <Form.Item label="支持货到付款">
                  <Switch
                    checked={model.PayAfterDeliveredSupported}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        PayAfterDeliveredSupported: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card size={'small'} title={'其他'} bordered={false}>
            <Row gutter={10}>
              <Col span={6}>
                <Form.Item label="可以评论">
                  <Switch
                    checked={model.CommentSupported}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        CommentSupported: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="仅作为信息页">
                  <Switch
                    checked={model.DisplayAsInformationPage}
                    onChange={(e) => {
                      setGoodsModel({
                        ...model,
                        DisplayAsInformationPage: e,
                      });
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Row gutter={10}>
            <Col span={24}>
              <Form.Item label="商品详情" rules={[{ max: 10000 }]}>
                <Alert
                  message="使用markdown语法"
                  style={{
                    marginBottom: 10,
                  }}
                ></Alert>
                <XMDEditor
                  value={model.DetailInformation || ''}
                  onChange={(text) => {
                    setGoodsModel({ ...model, DetailInformation: text });
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
    </>
  );
};
