import { ErrorBlock } from 'antd-mobile';
import { useEffect } from 'react';
import { history, useParams } from 'umi';

import XGoodsDetail from '@/components/goods/detail';
import XHeader from '@/components/mobile/simpleHeader';
import { isStringEmpty } from '@/utils/utils';
import { Container } from '@mui/material';

export default () => {
  const param = useParams<{ id?: string }>();

  useEffect(() => {
    if (isStringEmpty(param.id)) {
      history.push({
        pathname: '/goods',
      });
      return;
    }
  }, []);

  if (isStringEmpty(param.id)) {
    return <ErrorBlock />;
  }

  return (
    <>
      <XHeader>
        <Container maxWidth={'sm'}>
          <XGoodsDetail goodsId={param.id} />
        </Container>
      </XHeader>
    </>
  );
};
