import { Tag } from 'antd-mobile';
import { CSSProperties } from 'react';

import { isLocationEmpty } from '@/utils/biz';
import { UserAddressDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { SettingsOutlined } from '@mui/icons-material';
import {
  Avatar,
  Box,
  IconButton,
  Paper,
  Stack,
  Typography,
} from '@mui/material';

export default function ({
  item,
  distance,
  onClick,
  hideEditButton,
  style,
  hideLocationTips,
}: {
  item: UserAddressDto;
  distance?: number;
  onClick?: (e: UserAddressDto) => void;
  hideEditButton?: boolean;
  style?: CSSProperties;
  hideLocationTips?: boolean;
}) {
  if (!item) {
    return null;
  }

  const location_is_empty = isLocationEmpty(item);

  const renderTitle = () => {
    return (
      <>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box flexGrow={1}>
            <Stack
              direction={'row'}
              alignItems={'center'}
              justifyContent={'flex-start'}
              spacing={1}
            >
              <Typography variant="h5">{item.Name}</Typography>
              <Typography variant="body2" color="primary">
                {item.Tel}
              </Typography>
              {item.IsDefault && (
                <Tag
                  color="danger"
                  fill="outline"
                  round
                  style={{ fontSize: 10 }}
                >
                  默认
                </Tag>
              )}
              {location_is_empty && !hideLocationTips && (
                <Typography variant="caption" color="text.disabled">
                  低精度地址
                </Typography>
              )}
              {distance && distance > 0 && (
                <Typography variant="caption" color="text.disabled">
                  {`${(distance / 1000).toFixed(2)}km`}
                </Typography>
              )}
            </Stack>
          </Box>
          {hideEditButton || (
            <IconButton
              size="small"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                onClick && onClick(item);
              }}
            >
              <SettingsOutlined fontSize="inherit" />
            </IconButton>
          )}
        </Box>
      </>
    );
  };

  return (
    <>
      <Paper
        style={{ ...style }}
        sx={{
          px: 2,
          py: 1,
          border: `1px dashed gray`,
          borderColor: 'transparent',
          '&:hover': {
            borderColor: (t) => t.palette.primary.main,
          },
        }}
        elevation={1}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <Avatar>{item.Name}</Avatar>
          <Box
            flexGrow={1}
            sx={{
              ml: 2,
            }}
          >
            {renderTitle()}
            <Typography
              variant="caption"
              color={'text.disabled'}
              component="div"
            >
              {[item.Province || '', item.City || '', item.Area || '']
                .filter((x) => !isStringEmpty(x))
                .join('-')}
            </Typography>
            <Typography variant="overline">{item.AddressDetail}</Typography>
          </Box>
        </div>
      </Paper>
    </>
  );
}
