import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Tooltip } from '@mui/material';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Zoom from '@mui/material/Zoom';
import { useScroll } from 'ahooks';

export default ({ hide }: { hide?: boolean }) => {
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const scroll = useScroll(window.document);
  const top = scroll?.top || 0;

  return (
    <Zoom in={trigger && !hide}>
      <Box
        onClick={() => {
          window &&
          window.scrollTo({
            left: 0,
            top: 0,
          });
        }}
        role='presentation'
        sx={{
          position: 'fixed',
          bottom: 70,
          right: 16,
          zIndex: 9999,
          display: 'inline-block',
        }}
      >
        <Tooltip title='返回顶部'>
          <Fab sx={{}} color='primary' size='small'>
            <KeyboardArrowUpIcon />
          </Fab>
        </Tooltip>
      </Box>
    </Zoom>
  );
};
