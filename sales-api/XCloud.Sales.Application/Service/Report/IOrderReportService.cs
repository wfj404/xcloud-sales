using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Report;

public interface IOrderReportService : ISalesAppService
{
    Task<StoreOrderStatics[]> TopStoresAsync(QueryTopStoreInput dto);

    Task<TopCustomersList[]> TopCustomersAsync(QueryTopCustomerInput dto);

    Task<TopSkuList[]> TopSkuListsAsync(QueryTopSkuListInput dto);

    Task<TopSellersList[]> TopSellersAsync(QueryTopSellerInput dto);

    Task<TopBrandList[]> TopBrandListsAsync(QueryTopBrandListInput dto);

    Task<TopCategoryList[]> TopCategoryListsAsync(QueryTopCategoryListInput dto);

    Task<OrderSumByDateResponse[]> OrderSumByDateAsync(OrderSumByDateInput dto);
}

public class OrderReportService : SalesAppService, IOrderReportService
{
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly IStoreService _storeService;

    public OrderReportService(ISalesRepository<Order> orderRepository, IStoreService storeService)
    {
        _orderRepository = orderRepository;
        _storeService = storeService;
    }

    public async Task<OrderSumByDateResponse[]> OrderSumByDateAsync(OrderSumByDateInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));
        if (dto.StartTime == null || dto.EndTime == null)
            throw new UserFriendlyException("time range");
        if (dto.EndTime.Value <= dto.StartTime.Value)
            throw new UserFriendlyException("time range error");
        if (dto.EndTime.Value - dto.EndTime.Value > TimeSpan.FromDays(100))
            throw new UserFriendlyException("time range error");

        var db = await _orderRepository.GetDbContextAsync();

        var query = db.Set<Order>().AsNoTracking();

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime < dto.EndTime.Value);

        var grouped = query.GroupBy(x => x.CreationTime.AddHours(8).Date).Select(x =>
            new { x.Key, count = x.Count(), total = x.Sum(d => d.TotalPrice) });
        var data = await grouped.Take(100).ToArrayAsync();

        var response = data.OrderBy(x => x.Key).Select(x => new OrderSumByDateResponse
        {
            Date = x.Key,
            Total = x.count,
            Amount = x.total,
        }).ToArray();

        return response;
    }

    public async Task<TopSkuList[]> TopSkuListsAsync(QueryTopSkuListInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = from orderItem in db.Set<OrderItem>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on orderItem.OrderId equals order.Id
            select new { orderItem, order };

        if (dto.StartTime != null)
            query = query.Where(x => x.order.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.order.CreationTime < dto.EndTime.Value);

        var status = SalesConstants.OrderStatus.Finished;
        query = query.Where(x => x.order.OrderStatusId == status);

        var groupedQuery = query.GroupBy(x => x.orderItem.SkuId).Select(x => new
        {
            x.Key,
            totalAmount = x.Sum(d => d.orderItem.TotalPrice),
            totalQuantity = x.Sum(d => d.orderItem.Quantity)
        });

        groupedQuery = groupedQuery.OrderByDescending(x => x.totalAmount).ThenByDescending(x => x.totalQuantity);

        var count = dto.MaxCount ?? 20;

        var data = await groupedQuery.Take(count).ToArrayAsync();

        var topskus = data.Select(x => new TopSkuList
        {
            SkuId = x.Key,
            Name = String.Empty,
            TotalPrice = x.totalAmount,
            TotalQuantity = x.totalQuantity
        }).ToArray();

        if (topskus.Any())
        {
            var ids = topskus.Select(x => x.SkuId).ToArray();

            var q = from sku in db.Set<Sku>().AsNoTracking()
                join goods in db.Set<Goods>().AsNoTracking()
                    on sku.GoodsId equals goods.Id
                select new { sku, goods };

            var skus = await q.Where(x => ids.Contains(x.sku.Id)).ToArrayAsync();

            foreach (var m in topskus)
            {
                var b = skus.FirstOrDefault(x => x.sku.Id == m.SkuId);
                if (b == null)
                    continue;
                m.Name = $"{b.goods.Name}-{b.sku.Name}";
            }
        }

        return topskus;
    }

    public async Task<TopBrandList[]> TopBrandListsAsync(QueryTopBrandListInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = from orderItem in db.Set<OrderItem>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on orderItem.OrderId equals order.Id
            join spec in db.Set<Sku>().AsNoTracking()
                on orderItem.SkuId equals spec.Id
            join goods in db.Set<Goods>().AsNoTracking()
                on spec.GoodsId equals goods.Id
            select new { orderItem, spec, goods, order };

        query = query.Where(x => x.goods.BrandId != null && x.goods.BrandId != string.Empty);

        if (dto.StartTime != null)
            query = query.Where(x => x.order.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.order.CreationTime < dto.EndTime.Value);

        var status = SalesConstants.OrderStatus.Finished;
        query = query.Where(x => x.order.OrderStatusId == status);

        var groupedQuery = query.GroupBy(x => x.goods.BrandId).Select(x => new
        {
            x.Key,
            totalAmount = x.Sum(d => d.orderItem.TotalPrice),
            totalQuantity = x.Sum(d => d.orderItem.Quantity)
        });

        groupedQuery = groupedQuery.OrderByDescending(x => x.totalAmount).ThenByDescending(x => x.totalQuantity);

        var count = dto.MaxCount ?? 20;

        var data = await groupedQuery.Take(count).ToArrayAsync();

        var topbrands = data.Select(x => new TopBrandList
        {
            BrandId = x.Key,
            BrandName = String.Empty,
            TotalPrice = x.totalAmount,
            TotalQuantity = x.totalQuantity
        }).ToArray();

        if (topbrands.Any())
        {
            var ids = topbrands.Select(x => x.BrandId).ToArray();
            var brands = await db.Set<Brand>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToArrayAsync();

            foreach (var m in topbrands)
            {
                var b = brands.FirstOrDefault(x => x.Id == m.BrandId);
                if (b == null)
                    continue;
                m.BrandName = b.Name;
            }
        }

        return topbrands;
    }

    public async Task<TopCategoryList[]> TopCategoryListsAsync(QueryTopCategoryListInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = from orderItem in db.Set<OrderItem>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on orderItem.OrderId equals order.Id
            join spec in db.Set<Sku>().AsNoTracking()
                on orderItem.SkuId equals spec.Id
            join goods in db.Set<Goods>().AsNoTracking()
                on spec.GoodsId equals goods.Id
            join category in db.Set<Category>().AsNoTracking()
                on goods.CategoryId equals category.Id
            select new { orderItem, spec, goods, category, order };

        query = query.Where(x => x.goods.CategoryId != null && x.goods.CategoryId != string.Empty);

        if (dto.StartTime != null)
            query = query.Where(x => x.order.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.order.CreationTime < dto.EndTime.Value);

        var status = SalesConstants.OrderStatus.Finished;
        query = query.Where(x => x.order.OrderStatusId == status);

        var groupedQuery = query.GroupBy(x => new { x.category.RootId, x.category.Id }).Select(x => new
        {
            x.Key.RootId,
            x.Key.Id,
            totalAmount = x.Sum(d => d.orderItem.TotalPrice),
            totalQuantity = x.Sum(d => d.orderItem.Quantity)
        });

        groupedQuery = groupedQuery.OrderByDescending(x => x.totalAmount).ThenByDescending(x => x.totalQuantity);

        var count = dto.MaxCount ?? 20;

        var data = await groupedQuery.Take(count).ToArrayAsync();

        var topCategories = data.Select(x => new TopCategoryList
        {
            RootCategoryId = x.RootId,
            RootCategoryName = string.Empty,
            CategoryId = x.Id,
            CategoryName = String.Empty,
            TotalPrice = x.totalAmount,
            TotalQuantity = x.totalQuantity
        }).ToArray();

        if (topCategories.Any())
        {
            var ids = topCategories.SelectMany(x => new[] { x.RootCategoryId, x.CategoryId }).Distinct().ToArray();
            var categories = await db.Set<Category>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToArrayAsync();

            foreach (var m in topCategories)
            {
                var root = categories.FirstOrDefault(x => x.Id == m.RootCategoryId);
                var cat = categories.FirstOrDefault(x => x.Id == m.CategoryId);

                if (root != null)
                    m.RootCategoryName = root.Name;

                if (cat != null)
                    m.CategoryName = cat.Name;
            }
        }

        return topCategories;
    }

    public async Task<TopSellersList[]> TopSellersAsync(QueryTopSellerInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = db.Set<Order>().AsNoTracking();

        query = query.Where(x => x.AffiliateId > 0);

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime < dto.EndTime.Value);

        var status = SalesConstants.OrderStatus.Finished;
        query = query.Where(x => x.OrderStatusId == status);

        var groupedQuery = query.GroupBy(x => x.AffiliateId).Select(x => new
        {
            x.Key,
            totalAmount = x.Sum(d => d.TotalPrice),
            totalQuantity = x.Count()
        });

        groupedQuery = groupedQuery.OrderByDescending(x => x.totalAmount).ThenByDescending(x => x.totalQuantity);

        var count = dto.MaxCount ?? 20;

        var data = await groupedQuery.Take(count).ToArrayAsync();

        var sellers = data.Select(x => new TopSellersList
        {
            SellerId = x.Key,
            SellerName = String.Empty,
            TotalPrice = x.totalAmount,
            TotalQuantity = x.totalQuantity
        }).ToArray();

        if (sellers.Any())
        {
            var ids = sellers.Select(x => x.SellerId).ToArray();
            var managers = await db.Set<StoreUser>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToArrayAsync();

            foreach (var m in sellers)
            {
                var manager = managers.FirstOrDefault(x => x.Id == m.SellerId);
                if (manager == null)
                    continue;
                m.GlobalUserId = manager.UserId;
            }
        }

        return sellers;
    }

    public async Task<StoreOrderStatics[]> TopStoresAsync(QueryTopStoreInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = db.Set<Order>().AsNoTracking();

        query = query.Where(x => x.UserId > 0);

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime < dto.EndTime.Value);

        var status = SalesConstants.OrderStatus.Finished;
        query = query.Where(x => x.OrderStatusId == status);

        var groupedQuery = query.GroupBy(x => x.StoreId).Select(x => new
        {
            x.Key,
            totalAmount = x.Sum(d => d.TotalPrice),
            totalQuantity = x.Count()
        });

        groupedQuery = groupedQuery.OrderByDescending(x => x.totalAmount).ThenByDescending(x => x.totalQuantity);

        var count = dto.MaxCount ?? 20;

        var data = await groupedQuery.Take(count).ToArrayAsync();

        var datalist = data.Select(x => new StoreOrderStatics()
        {
            StoreId = x.Key,
            TotalPrice = x.totalAmount,
            TotalQuantity = x.totalQuantity
        }).ToArray();

        if (datalist.Any())
        {
            var ids = datalist.Select(x => x.StoreId).ToArray();

            var stores = await this._storeService.GetByIdsAsync(ids);

            foreach (var m in datalist)
            {
                m.Store = stores.FirstOrDefault(x => x.Id == m.StoreId);
            }
        }

        return datalist;
    }

    public async Task<TopCustomersList[]> TopCustomersAsync(QueryTopCustomerInput dto)
    {
        var db = await _orderRepository.GetDbContextAsync();

        var query = db.Set<Order>().AsNoTracking();

        query = query.Where(x => x.UserId > 0);

        if (dto.StartTime != null)
            query = query.Where(x => x.CreationTime >= dto.StartTime.Value);

        if (dto.EndTime != null)
            query = query.Where(x => x.CreationTime < dto.EndTime.Value);

        var status = SalesConstants.OrderStatus.Finished;
        query = query.Where(x => x.OrderStatusId == status);

        var groupedQuery = query.GroupBy(x => x.UserId).Select(x => new
        {
            x.Key,
            totalAmount = x.Sum(d => d.TotalPrice),
            totalQuantity = x.Count()
        });

        groupedQuery = groupedQuery.OrderByDescending(x => x.totalAmount).ThenByDescending(x => x.totalQuantity);

        var count = dto.MaxCount ?? 20;

        var data = await groupedQuery.Take(count).ToArrayAsync();

        var sellers = data.Select(x => new TopCustomersList
        {
            CustomerId = x.Key,
            CustomerName = String.Empty,
            TotalPrice = x.totalAmount,
            TotalQuantity = x.totalQuantity
        }).ToArray();

        if (sellers.Any())
        {
            var ids = sellers.Select(x => x.CustomerId).ToArray();
            var managers = await db.Set<StoreUser>().AsNoTracking().Where(x => ids.Contains(x.Id)).ToArrayAsync();

            foreach (var m in sellers)
            {
                var manager = managers.FirstOrDefault(x => x.Id == m.CustomerId);
                if (manager == null)
                    continue;
                m.GlobalUserId = manager.UserId;
            }
        }

        return sellers;
    }
}