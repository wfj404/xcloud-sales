import { LinkDto } from '@/utils/biz';

import { BlockItem, BlockItemProps } from '../../utils';

export interface TitleBlockProps extends BlockItemProps {
  title?: string;
  moreText?: string;
  moreLink?: LinkDto;
}

export const title_demo: BlockItem<TitleBlockProps> = {
  type: 'title',
  style: {
    marginBottom: 10,
  },
  body: {
    title: '标题1',
    moreText: '更多',
    moreLink: {},
  },
};
