﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Common;

namespace XCloud.Sales.Application.Mapping.Common;

public class OrderGroupMap : SalesEntityTypeConfiguration<OrderGroup>
{
    public override void Configure(EntityTypeBuilder<OrderGroup> builder)
    {
        base.Configure(builder);

        builder.ToTable("sales_order_group");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.StoreId).RequiredId();
        builder.Property(x => x.OrderDataJson);
        builder.Property(x => x.RowVersion).HasDefaultValue(1);

        builder.Property(x => x.CreationTime);
    }
}