﻿using System;
using System.Threading.Tasks;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction.Router;

public interface IMessageClusterRouter : IDisposable
{
    Task RouteToUserAsync(string userId, MessageDto data);

    Task BroadCastAsync(MessageDto dto);

    Task SubscribeFromMessageRouterAsync(string serverInstanceKey, Func<MessageDto, Task> callback);
}