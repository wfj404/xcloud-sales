using XCloud.EntityFrameworkCore.Mapping;
using XCloud.Sales.Application.Domain;

namespace XCloud.Sales.Application.Mapping;

/// <summary>
/// Represents base entity mapping configuration
/// </summary>
/// <typeparam name="TEntity">Entity type</typeparam>
public abstract class SalesEntityTypeConfiguration<TEntity> : EfEntityTypeConfiguration<TEntity>
    where TEntity : class, ISalesBaseEntity
{
    //
}