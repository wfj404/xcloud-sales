﻿using Volo.Abp;

namespace XCloud.Core.Json;

public class SerializeException : BusinessException
{
    public SerializeException(string message) : base(message: message)
    {
        //
    }

    public SerializeException(string message, Exception e) : base(message: message, innerException: e)
    {
        //
    }
}