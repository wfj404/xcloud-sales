using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Models;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities;
using XCloud.Core.Extension;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Extension;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.Application.Service.Users;
using XCloud.Platform.WeChat;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Users;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.WeChat.Service.Payments;

[ExposeServices(typeof(WechatMpPaymentService))]
public class WechatMpPaymentService : SalesAppService
{
    private readonly IOrderService _orderService;
    private readonly OrderUtils _orderUtils;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly ISettingService _settingService;
    private readonly IExternalConnectService _externalConnectService;
    private readonly IStoreUserService _storeUserService;
    private readonly WechatClientFactory _wechatClientFactory;

    public WechatMpPaymentService(IOrderService orderService,
        OrderUtils orderUtils,
        IOrderPaymentBillService orderPaymentBillService,
        ISettingService settingService,
        IExternalConnectService externalConnectService,
        IStoreUserService storeUserService,
        WechatClientFactory wechatClientFactory)
    {
        _orderService = orderService;
        _orderUtils = orderUtils;
        _orderPaymentBillService = orderPaymentBillService;
        _settingService = settingService;
        _externalConnectService = externalConnectService;
        _storeUserService = storeUserService;
        _wechatClientFactory = wechatClientFactory;
    }

    private async Task<ExternalConnect> QueryUserWechatMpConnectionAsync(int userId)
    {
        var option = await this._settingService.GetWechatMpOptionAsync();

        option.EnsureWechatOptionNotEmpty();

        var user = await _storeUserService.GetByIdAsync(userId);

        if (user == null)
            throw new EntityNotFoundException(nameof(user));

        if (string.IsNullOrWhiteSpace(user.UserId))
            throw new UserFriendlyException("global user id is required");

        var connection = await _externalConnectService.FindByUserIdAsync(
            ThirdPartyPlatforms.WxMp,
            option.AppId,
            user.UserId);

        return connection;
    }

    private async Task<string> GetRequiredOpenIdAsync(int userId)
    {
        var connection = await QueryUserWechatMpConnectionAsync(userId);

        if (connection == null || string.IsNullOrWhiteSpace(connection.OpenId))
            throw new UserFriendlyException("pls bind wechat first");

        return connection.OpenId;
    }

    private async Task<GetPayTransactionByOutTradeNumberResponse> QueryTransactionByOutTradeNoAsync(
        string outTradeNumber)
    {
        var wechatPaymentClient = await this._wechatClientFactory.BuildTenPayClientAsync();

        var response =
            await wechatPaymentClient.ExecuteGetPayTransactionByOutTradeNumberAsync(new()
            {
                MerchantId = wechatPaymentClient.Credentials.MerchantId,
                OutTradeNumber = outTradeNumber
            });

        response.ThrowIfNotSuccess(this.JsonDataSerializer, "failed to query transaction");

        return response;
    }

    public async Task TryUpdateBillStatusAsync(string billId)
    {
        var bill = await this._orderPaymentBillService.GetRequiredByIdAsync(billId);

        if (bill.Paid)
        {
            return;
        }

        if (bill.CreationTime < this.Clock.Now.AddDays(-1))
        {
            return;
        }

        var response = await this.QueryTransactionByOutTradeNoAsync(bill.Id);

        if (response.TradeState?.ToUpper() == "SUCCESS")
        {
            await this._orderPaymentBillService.MarkAsPaidAsync(new MarkBillAsPayedInput()
            {
                Id = bill.Id,
                TransactionId = response.TransactionId,
                NotifyData = this.JsonDataSerializer.SerializeToString(response),
                PaymentMethod = SalesConstants.PaymentChannel.WechatMp,
                Time = response.SuccessTime?.DateTime
            });
        }
        else
        {
            var json = this.JsonDataSerializer.SerializeToString(response);
            this.Logger.LogWarning(message: $"payment not finished:{json}");
        }
    }

    public async Task<CreatePayTransactionJsapiResponse> CreatePaymentOrderAsync(string billId)
    {
        var option = await this._settingService.GetWechatMpOptionAsync();
        option.EnsureWechatOptionNotEmpty();

        var bill = await this._orderPaymentBillService.GetRequiredBillReadyForPaymentAsync(billId);

        var order = await _orderService.GetByIdAsync(bill.OrderId);

        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        var openId = await GetRequiredOpenIdAsync(order.UserId);

        var wechatPaymentClient = await this._wechatClientFactory.BuildTenPayClientAsync();

        var apiGateway = await ServiceDiscoveryService.GetRequiredPublicGatewayAddressAsync();

        var request = new CreatePayTransactionJsapiRequest
        {
            AppId = option.AppId,
            OutTradeNumber = bill.Id,
            Description = string.Empty,
            NotifyUrl = new[]
            {
                apiGateway,
                $"api/mall/wechat-mp-payment/m-{wechatPaymentClient.Credentials.MerchantId}/payment-notify"
            }.ConcatUrl(),
            Detail = new CreatePayPartnerTransactionJsapiRequest.Types.Detail
            {
                GoodsList = new List<CreatePayTransactionAppRequest.Types.Detail.Types.GoodsDetail>
                {
                    new CreatePayTransactionAppRequest.Types.Detail.Types.GoodsDetail
                    {
                        GoodsName = "bill payment"
                    }
                }
            },
            Amount = new CreatePayPartnerTransactionJsapiRequest.Types.Amount
            {
                Total = _orderUtils.CastMoneyToCent(bill.Price)
            },
            Payer = new CreatePayTransactionJsapiRequest.Types.Payer
            {
                OpenId = openId
            }
        };

        var response = await wechatPaymentClient.ExecuteCreatePayTransactionJsapiAsync(request);

        response.ThrowIfNotSuccess(this.JsonDataSerializer, "failed to create payment order");

        return response;
    }
}