import { Badge, Button, Table, TablePaginationConfig, Tag } from 'antd';
import { ColumnProps, ColumnType } from 'antd/es/table';
import React, { useState } from 'react';

import XTime from '@/components/common/time';
import { getDeliveryType, getSkuName, isOrderToService } from '@/utils/biz';
import { DeliveryRecordDto, DeliveryRecordItemDto } from '@/utils/swagger';

import XStatus from '../components/delivery_status';
import TimeRange from '../components/time_range';
import XDetail from './detail';

const DeliveryItemView = ({ delivery }: { delivery: DeliveryRecordDto }) => {
  const columns: ColumnType<DeliveryRecordItemDto>[] = [
    {
      title: '',
      render: (d, x, i) => {
        return (
          <span>
            <b>{i + 1}</b>
          </span>
        );
      },
    },
    {
      title: '商品',
      render: (d, x) => {
        let orderItem = delivery.Order?.Items?.find(
          (m) => m.Id == x.OrderItemId,
        );
        if (orderItem == undefined) {
          return <span>--</span>;
        }
        return <span>{getSkuName(orderItem.Sku)}</span>;
      },
    },
    {
      title: '重量',
      render: (d, x) => x.OrderItem?.Sku?.Weight || '--',
    },
    {
      title: '数量',
      render: (d, x) => x.Quantity || '--',
    },
  ];

  return (
    <>
      <h2>配送单详情</h2>
      <Table
        size={'small'}
        bordered
        columns={columns}
        dataSource={delivery.Items || []}
        pagination={false}
      ></Table>
    </>
  );
};

export default ({
  data,
  pagination,
  refresh,
}: {
  data: DeliveryRecordDto[];
  pagination: TablePaginationConfig | undefined | false;
  refresh: () => void;
}) => {
  const [loading, _loading] = React.useState<boolean>(false);
  const [detailId, _detailId] = useState<string | undefined>(undefined);

  const columns: ColumnProps<DeliveryRecordDto>[] = [
    {
      title: '订单状态',
      render: (d, x) => {
        const service = isOrderToService(x.Order);
        return (
          <Badge
            status={service ? 'success' : 'default'}
            text={service ? '可服务' : '等待订单状态'}
          />
        );
      },
    },
    {
      title: '配送方式',
      render: (d, x) => <Tag>{getDeliveryType(x)}</Tag>,
    },
    {
      title: '配送时间',
      render: (d, x) => {
        return (
          <div>
            <TimeRange
              start={x.PreferDeliveryStartTime}
              end={x.PreferDeliveryEndTime}
            />
          </div>
        );
      },
    },
    {
      title: '物流名称',
      render: (d, x) => x.ExpressName || '--',
    },
    {
      title: '物流单号',
      render: (d, x) => x.TrackingNumber || '--',
    },
    {
      title: '收货状态',
      render: (d, x) => {
        return <XStatus data={x} />;
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type={'link'}
              onClick={() => {
                _detailId(x.Id || '');
              }}
            >
              查看
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  return (
    <>
      <XDetail
        detailId={detailId || ''}
        hide={() => {
          _detailId(undefined);
          refresh();
        }}
      />

      <Table
        loading={loading}
        dataSource={data}
        columns={columns}
        rowKey={(x) => x.Id || ''}
        expandable={{
          expandedRowRender: (e) => {
            return (
              <div style={{ padding: 10, border: '5px dashed orange' }}>
                <DeliveryItemView delivery={e} />
              </div>
            );
          },
        }}
        pagination={pagination}
      />
    </>
  );
};
