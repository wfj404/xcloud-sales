# 数据字典

## 业务账单表BIZ_BILL

| 字段名            | 类型           | NULL | 描述                                                         |
| ----------------- | -------------- | ---- | ------------------------------------------------------------ |
| BILL_ID           | int            | N    | 账单ID                                                       |
| BUSINESS_ID       | varchar(50)    | Y    | 业务表NO                                                     |
| BUSINESS_TYPE     | int            | Y    | 业务类型 1自助抄表2用水保修3用户反馈                         |
| MCS_USER_ID       | varchar(50)    | Y    | 用户标识                                                     |
| SOURCE_TYPE       | int            | Y    | 账单来源系统 1工单 2营销 3其他                               |
| CHECK_TIME        | datetime       | Y    | 出账时间                                                     |
| BILL_MONEY        | decimal(18, 2) | Y    | 合计                                                         |
| COUPON            | decimal(18, 2) | Y    | 优惠金额                                                     |
| LATE_FEE          | decimal(18, 2) | Y    | 违约金                                                       |
| TOTAL_MONEY       | decimal(18, 2) | Y    | 总价, 合计 - 优惠 + 违约金                                   |
| PAY_STATE         | int            | Y    | 缴费状态。 未收（欠费）：0；<br /> 已收费（缴费成功）：1； <br />已销账（已结账、已对账）：2；<br /> 已核销：3 |
| PAY_TIME          | datetime       | Y    | 收费时间                                                     |
| PS_T_METHOD       | varchar(200)   | Y    | 收费途径（兴业银行-CIB）                                     |
| CHARGE_DATE       | datetime       | Y    | 销账时间                                                     |
| CAN_INVOICING     | bit            | Y    | // 是否允许开票，业务系统根据业务配置返回值。<br />改为是否已经申请开票 |
| CAN_VAT_INVOICING | bit            | Y    | 是否允许开增值税票                                           |
| REMARK            | varchar(200)   | Y    | 备注                                                         |
| TITLE             | varchar(200)   | Y    | 工程标题                                                     |
| COMPANY_CODE      | varchar(200)   | Y    | 水司清算单位                                                 |

## 业务账单明细表BIZ_BILL_DETAIL

| 字段名         | 类型           | NULL | 描述                    |
| -------------- | -------------- | ---- | ----------------------- |
| BILL_DETAIL_ID | int            | N    | 账单明细ID              |
| BILL_ID        | int            | Y    | 账单ID                  |
| PRODUCT_ID     | int            | Y    | 产品编号（手动录入为0） |
| PRODUCT_NAME   | varchar(200)   | Y    | 产品名称                |
| INTRODUCE      | varchar(200)   | Y    | 产品介绍                |
| MONEY          | decimal(18, 2) | Y    | 产品明细小计            |
| REMARK         | varchar(200)   | Y    | 备注                    |

# 接口

## 获取账单发票记录

> 根据筛选条件查询所有账单发票记录


```
URL:/mcs-biz-api/v3/mf/e-contract
请求方式：GET
```

- 请求参数

qt：查询类型。包括：业务编号:01、用户号: 02

qv：查询值

s：状态：包括：未申请开票:0、已申请开票:1、全部:-1

st：销账时间范围，开始值。格式：yyyy-MM-dd

et：销账时间范围，结束值。格式：yyyy-MM-dd

- 返回参数


返回参数结构：

```
{
    "code":0,
    "message":"成功",
    "data":[{
        "billId":0,
        "bizId":"xxxxxxxxxxxxxxxxxx",
        "bizType":3,
        "title":"",
        "userId":"",
        "sourceType":"",
        "billMoney":"10.00",
        "coupon":"1.00",
        "lateFee":"0.00",
        "totalMoney":"9.00",
        "billDate":"2023-11-24 00:00:00",
        "payTime":"2023-11-24 00:00:00",
        "chargeDate":"2023-11-24 00:00:00",
        "canInvoicing":true,
        "remark":"",
        "outletId":16,
        "outletTitle":"城南营业所",
        "details":[...]
    }]
}
```

返回参数说明：

| 参数项       | 参数名称                             | 类型          | 必须 |
| ------------ | ------------------------------------ | ------------- | ---- |
| billId       | 账单ID                               | int           | Y    |
| bizId        | 业务表NO                             | String        | Y    |
| bizType      | 业务类型 1自助抄表2用水保修3用户反馈 | int           | Y    |
| title        | 工程标题                             | String        | Y    |
| userId       | 用户标识                             | String        | Y    |
| sourceType   | 账单来源系统 1工单 2营销 3其他       | int           | Y    |
| billMoney    | 合计                                 | double        | Y    |
| coupon       | 优惠金额                             | double        | Y    |
| lateFee      | 违约金                               | double        | Y    |
| totalMoney   | 总价, 合计 - 优惠 + 违约金           | double        | Y    |
| billDate     | 出账时间                             | LocalDateTime | Y    |
| payTime      | 收费时间                             | LocalDateTime | Y    |
| chargeDate   | 销账时间                             | LocalDateTime | Y    |
| canInvoicing | 是否已经申请开票                     | boolean       | Y    |
| remark       | 备注                                 | String        | Y    |
| outletId     | 业务站点关联发票领取站点ID           | int           | Y    |
| outletTitle  | 业务站点关联发票领取站点标题         | String        | Y    |
| details      | 明细合集                             | List          | Y    |

detail

> 账单明细

| 参数项       | 参数名称                | 类型   | 必须 |
| ------------ | ----------------------- | ------ | ---- |
| billDetailId | 账单明细ID              | int    | Y    |
| productId    | 产品编号（手动录入为0） | int    | Y    |
| productName  | 产品名称                | String | Y    |
| introduce    | 产品介绍                | String | Y    |
| money        | 产品明细小计            | double | Y    |
| remark       | 备注                    | String | Y    |

## 申请发票

> 申请发票回填


```
URL:/mcs-biz-api/v3/mf/e-contract/apply
请求方式：POST
```

- - 请求参数

返回参数结构：

```
{
    "bizIds":[
        "xxxxxxxxxxxxxxxxxx",
        "xxxxxxxxxxxxxxxxxx"
    ]
}
```

请求参数说明：

| 参数项 | 参数名称     | 类型     | 必须 |
| ------ | ------------ | -------- | ---- |
| bizIds | 业务表NO集合 | String[] | Y    |
- 返回参数


返回参数结构：

```
{
  "code": 0,
  "message": "成功",
  "data": {
    "success": 10,
    "fail": 0,
    "bills": [
      {
        "billId": 0,
        "bizId": "xxxxxxxxxxxxxxxxxx",
        "bizType": 3,
        "title": "",
        "userId": "",
        "sourceType": "",
        "billMoney": "10.00",
        "coupon": "1.00",
        "lateFee": "0.00",
        "totalMoney": "9.00",
        "billDate": "2023-11-24 00:00:00",
        "payTime": "2023-11-24 00:00:00",
        "chargeDate": "2023-11-24 00:00:00",
        "canInvoicing": true,
        "remark": "",
        "outletId": 16,
        "outletTitle": "城南营业所",
        "details": [...]
      }
    ]
  }
}
```

返回参数说明：

| 参数项  | 参数名称 | 类型 | 必须 |
| ------- | -------- | ---- | ---- |
| success | 成功数   | int  | Y    |
| fail    | 失败数   | int  | Y    |
| bills   | 账单合集 | List | Y    |

bills

> 账单

| 参数项       | 参数名称                             | 类型          | 必须 |
| ------------ | ------------------------------------ | ------------- | ---- |
| billId       | 账单ID                               | int           | Y    |
| bizId        | 业务表NO                             | String        | Y    |
| bizType      | 业务类型 1自助抄表2用水保修3用户反馈 | int           | Y    |
| title        | 工程标题                             | String        | Y    |
| userId       | 用户标识                             | String        | Y    |
| sourceType   | 账单来源系统 1工单 2营销 3其他       | int           | Y    |
| billMoney    | 合计                                 | double        | Y    |
| coupon       | 优惠金额                             | double        | Y    |
| lateFee      | 违约金                               | double        | Y    |
| totalMoney   | 总价, 合计 - 优惠 + 违约金           | double        | Y    |
| billDate     | 出账时间                             | LocalDateTime | Y    |
| payTime      | 收费时间                             | LocalDateTime | Y    |
| chargeDate   | 销账时间                             | LocalDateTime | Y    |
| canInvoicing | 是否已经申请开票                     | boolean       | Y    |
| remark       | 备注                                 | String        | Y    |
| outletId     | 业务站点关联发票领取站点ID           | int           | Y    |
| outletTitle  | 业务站点关联发票领取站点标题         | String        | Y    |
| details      | 明细合集                             | List          | Y    |

detail

> 账单明细

| 参数项       | 参数名称                | 类型   | 必须 |
| ------------ | ----------------------- | ------ | ---- |
| billDetailId | 账单明细ID              | int    | Y    |
| productId    | 产品编号（手动录入为0） | int    | Y    |
| productName  | 产品名称                | String | Y    |
| introduce    | 产品介绍                | String | Y    |
| money        | 产品明细小计            | double | Y    |
| remark       | 备注                    | String | Y    |

## 根据业务编号获取多笔账单记录

> 根据筛选条件查询所有账单发票记录


```
URL:/mcs-biz-api/v3/mf/e-contract/query/multi
请求方式：POST
```

- 请求参数

返回参数结构：

```
{
    "bizIds":[
        "xxxxxxxxxxxxxxxxxx",
        "xxxxxxxxxxxxxxxxxx"
    ]
}
```

请求参数说明：

| 参数项 | 参数名称     | 类型     | 必须 |
| ------ | ------------ | -------- | ---- |
| bizIds | 业务表NO集合 | String[] | Y    |


- 返回参数


返回参数结构：

```
{
    "code":0,
    "message":"成功",
    "data":[{
        "billId":0,
        "bizId":"xxxxxxxxxxxxxxxxxx",
        "bizType":3,
        "title":"",
        "userId":"",
        "sourceType":"",
        "billMoney":"10.00",
        "coupon":"1.00",
        "lateFee":"0.00",
        "totalMoney":"9.00",
        "billDate":"2023-11-24 00:00:00",
        "payTime":"2023-11-24 00:00:00",
        "chargeDate":"2023-11-24 00:00:00",
        "canInvoicing":true,
        "remark":"",
        "outletId":16,
        "outletTitle":"城南营业所",
        "details":[...]
    }]
}
```

返回参数说明：

| 参数项       | 参数名称                             | 类型          | 必须 |
| ------------ | ------------------------------------ | ------------- | ---- |
| billId       | 账单ID                               | int           | Y    |
| bizId        | 业务表NO                             | String        | Y    |
| bizType      | 业务类型 1自助抄表2用水保修3用户反馈 | int           | Y    |
| title        | 工程标题                             | String        | Y    |
| userId       | 用户标识                             | String        | Y    |
| sourceType   | 账单来源系统 1工单 2营销 3其他       | int           | Y    |
| billMoney    | 合计                                 | double        | Y    |
| coupon       | 优惠金额                             | double        | Y    |
| lateFee      | 违约金                               | double        | Y    |
| totalMoney   | 总价, 合计 - 优惠 + 违约金           | double        | Y    |
| billDate     | 出账时间                             | LocalDateTime | Y    |
| payTime      | 收费时间                             | LocalDateTime | Y    |
| chargeDate   | 销账时间                             | LocalDateTime | Y    |
| canInvoicing | 是否已经申请开票                     | boolean       | Y    |
| remark       | 备注                                 | String        | Y    |
| outletId     | 业务站点关联发票领取站点ID           | int           | Y    |
| outletTitle  | 业务站点关联发票领取站点标题         | String        | Y    |
| details      | 明细合集                             | List          | Y    |

detail

> 账单明细

| 参数项       | 参数名称                | 类型   | 必须 |
| ------------ | ----------------------- | ------ | ---- |
| billDetailId | 账单明细ID              | int    | Y    |
| productId    | 产品编号（手动录入为0） | int    | Y    |
| productName  | 产品名称                | String | Y    |
| introduce    | 产品介绍                | String | Y    |
| money        | 产品明细小计            | double | Y    |
| remark       | 备注                    | String | Y    |
