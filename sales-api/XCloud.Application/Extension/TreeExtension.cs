﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Model;
using XCloud.Core.Extension;
using XCloud.EntityFrameworkCore.Repository;

namespace XCloud.Application.Extension;

public static class TreeExtension
{
    public static void EnsureTreeNodes<T>(this IEnumerable<T> flatNodes) where T : IHasParent
    {
        WalkTreeNodes(flatNodes: flatNodes,
            callback: (a, b) => { },
            firstNodeId: default);
    }

    public static void WalkTreeNodes<T>(this IEnumerable<T> flatNodes,
        Action<T, T[]> callback,
        string firstNodeId = default) where T : IHasParent
    {
        WalkTreeNodes(flatNodes: flatNodes.ToArray(),
            idSelector: x => x.Id,
            parentIdSelector: x => x.ParentId,
            callback: callback,
            firstNodeId: firstNodeId);
    }

    public static void WalkTreeNodes<T>(this T[] flatNodes,
        Func<T, string> idSelector,
        Func<T, string> parentIdSelector,
        Action<T, T[]> callback,
        string firstNodeId = default)
    {
        var entryNodes = new List<T>();

        if (string.IsNullOrWhiteSpace(firstNodeId))
        {
            //walk from roots
            var treeRoot = flatNodes.Where(x => string.IsNullOrWhiteSpace(parentIdSelector.Invoke(x))).ToArray();
            entryNodes.AddManyItems(treeRoot);
        }
        else
        {
            var firstNode = flatNodes.FirstOrDefault(x => idSelector.Invoke(x) == firstNodeId) ??
                            throw new ArgumentException("first node id dose not exist in tree");
            entryNodes.Add(firstNode);
        }

        foreach (var root in entryNodes)
        {
            root.WalkTreeNodes(
                idSelector: idSelector,
                childrenSelector: (node, currentNodeId) =>
                {
                    return flatNodes.Where(x => parentIdSelector.Invoke(x) == currentNodeId).ToArray();
                },
                callback: callback);
        }
    }

    public static void WalkTreeNodes<T>(this T firstNode,
        Func<T, string> idSelector,
        Func<T, string, T[]> childrenSelector,
        Action<T, T[]> callback)
    {
        var loopCheck = new List<string>();

        void WalkNode(T node)
        {
            var currentNodeId = idSelector.Invoke(node);

            if (string.IsNullOrWhiteSpace(currentNodeId))
                throw new ArgumentException(message: nameof(currentNodeId));

            loopCheck.AddOnceOrThrow(currentNodeId, errorMsg: "loop reference");

            var children = childrenSelector.Invoke(node, currentNodeId);

            callback.Invoke(node, children);

            foreach (var child in children)
            {
                WalkNode(child);
            }
        }

        WalkNode(firstNode);
    }

    public static IEnumerable<AntDesignTreeNode<T>> BuildAntTreeV2<T>(this T[] list,
        Func<T, string> idSelector,
        Func<T, string> parentIdSelector,
        Func<T, string> titleSelector)
    {
        if (idSelector == null)
            throw new ArgumentNullException(nameof(idSelector));

        if (parentIdSelector == null)
            throw new ArgumentNullException(nameof(parentIdSelector));

        if (titleSelector == null)
            throw new ArgumentNullException(nameof(titleSelector));

        var handledNodes = new List<string>();

        AntDesignTreeNode<T> BindChildren(T node, int level)
        {
            var nodeId = idSelector.Invoke(node);
            if (string.IsNullOrWhiteSpace(nodeId))
                throw new ArgumentNullException(nameof(nodeId));

            handledNodes.AddOnceOrThrow(nodeId, "树存在错误");

            var children = list.Where(x => parentIdSelector.Invoke(x) == nodeId).ToArray();

            return new AntDesignTreeNode<T>(node)
            {
                key = nodeId,
                title = titleSelector.Invoke(node),
                node_level = level,
                //递归下一级
                children = children.Select(x => BindChildren(x, level + 1)).ToArray(),
            };
        }

        var data = list.Where(x => string.IsNullOrWhiteSpace(parentIdSelector.Invoke(x))).ToArray();

        var res = data.Select(x => BindChildren(x, 1)).ToArray();

        return res;
    }

    public static IEnumerable<KeyValuePair<string, T>> BuildFlatTitleList<T>(this T[] list,
        Func<T, string> idSelector,
        Func<T, string> parentIdSelector,
        Func<T, string> titleSelector,
        string split = default)
    {
        split ??= "->";

        foreach (var m in list)
        {
            var nodePath = list.GetNodePath(
                idSelector: idSelector,
                parentIdSelector: parentIdSelector,
                nodeId: idSelector.Invoke(m));

            var segments = nodePath.Select(titleSelector).ToArray();

            yield return new KeyValuePair<string, T>(string.Join(split, segments), m);
        }
    }

    public static List<T> FindNodeChildrenRecursively<T>(this IEnumerable<T> dataSource, T firstNode)
        where T : class, IHasParent, IEntity<string>
    {
        var list = new List<T>();

        dataSource.ToArray().WalkTreeNodes(
            idSelector: x => x.Id,
            parentIdSelector: x => x.ParentId,
            callback: (node, children) => list.Add(node),
            firstNodeId: firstNode.Id);

        return list;
    }

    public static bool IsTreeRoot<T>(this T model) where T : class, IHasParent
    {
        return string.IsNullOrWhiteSpace(model.ParentId?.Trim());
    }

    public static T[] GetNodePath<T>(this T[] list,
        Func<T, string> idSelector,
        Func<T, string> parentIdSelector,
        string nodeId)
    {
        var nodePath = new List<T>();
        var repeatCheck = new List<string>();

        var currentId = nodeId;

        while (true)
        {
            repeatCheck.AddOnceOrThrow(currentId);

            var currentNode = list.FirstOrDefault(x => idSelector.Invoke(x) == currentId);
            if (currentNode == null)
                break;

            nodePath.Insert(0, currentNode);

            //next
            currentId = parentIdSelector.Invoke(currentNode);
        }

        return nodePath.ToArray();
    }

    public static async Task<T[]> GetNodePathAsync<T>(this IEfRepository<T> repo, T node)
        where T : class, IHasParent, IEntity<string>
    {
        if (node == null)
            throw new ArgumentNullException(nameof(node));

        var nodePath = new List<T>();

        var currentNode = node;

        while (true)
        {
            if (nodePath.Ids().Contains(currentNode.Id))
            {
                throw new UserFriendlyException(message: "error tree");
            }

            nodePath.Insert(0, currentNode);

            if (currentNode.IsTreeRoot())
            {
                break;
            }

            currentNode = await repo.FirstOrDefaultAsync(x => x.Id == currentNode.ParentId);

            if (currentNode == null)
                break;
        }

        return nodePath.ToArray();
    }
}