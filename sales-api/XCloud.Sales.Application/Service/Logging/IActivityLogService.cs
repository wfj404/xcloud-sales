﻿using MaxMind.GeoIP2;
using UAParser;
using XCloud.Application.Apm;
using XCloud.Application.DistributedLock;
using XCloud.Application.Logging;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Logging;

public static class ActivityLogType
{
    public static int VisitGoods => 1;
    public static int SearchGoods => 2;
    public static int PlaceOrder => 3;
    public static int VisitPage => 5;
    public static int GetCoupon => 6;
    public static int AddShoppingCart => 7;
    public static int DeleteShoppingCart => 8;
    public static int AuditLog => 11;
}

public static class ActivityLogSubjectType
{
    public const string Goods = "goods";
    public const string Sku = "sku";
    public const string Order = "order";
    public const string Coupon = "coupon";
    public const string Page = "page";
}

public interface IActivityLogService : ISalesAppService
{
    Task<ActivityLogDto[]> AttachAdminAsync(ActivityLogDto[] data);

    Task<ActivityLogDto[]> AttachStoreUserAsync(ActivityLogDto[] data);

    Task<ActivityLog> TryParseUserAgentAsync(ActivityLog log);

    Task<ActivityLog> TryResolveGeoAddressAsync(ActivityLog log);

    Task<PagedResponse<ActivityLogDto>> QueryPagingAsync(QueryActivityLogPagingInput dto);

    Task<ActivityLog> AttachHttpContextInfoAsync(ActivityLog log);

    Task InsertAsync(ActivityLog activityLog);

    Task DeleteAsync(int id);

    Task ClearExpiredDataWithLockAsync(DateTime endTime);
}

public class ActivityLogService : SalesAppService, IActivityLogService
{
    private readonly ISalesRepository<ActivityLog> _activityLogRepository;
    private readonly IAdminService _adminService;
    private readonly IStoreUserService _storeUserService;

    public ActivityLogService(ISalesRepository<ActivityLog> activityLogRepository,
        IAdminService adminService,
        IStoreUserService storeUserService)
    {
        _activityLogRepository = activityLogRepository;
        _adminService = adminService;
        _storeUserService = storeUserService;
    }

    public async Task<ActivityLog> TryResolveGeoAddressAsync(ActivityLog log)
    {
        var geoDatabasePath = this.Configuration["app:config:geo_database"];
        
        if (!string.IsNullOrWhiteSpace(log.IpAddress) && !string.IsNullOrWhiteSpace(geoDatabasePath))
        {
            try
            {
                using var reader = new DatabaseReader(geoDatabasePath);

                if (reader.TryCity(log.IpAddress, out var response) && response != null)
                {
                    log.GeoCountry = response.Country.Name;
                    log.GeoCity = response.City.Name;
                    log.Lng = response.Location.Longitude;
                    log.Lat = response.Location.Latitude;
                }
            }
            catch (Exception e)
            {
                Logger.LogError(message: e.Message, exception: e);
            }
        }

        await Task.CompletedTask;
        return log;
    }

    [Apm]
    public virtual async Task<ActivityLog> TryParseUserAgentAsync(ActivityLog log)
    {
        if (log == null)
            throw new ArgumentNullException(nameof(log));

        if (string.IsNullOrWhiteSpace(log.UserAgent))
        {
            return log;
        }

        using var _ = this.Logger.Metric(nameof(TryParseUserAgentAsync));
        try
        {
            var parser = Parser.GetDefault();
            var ua = parser.Parse(log.UserAgent);
            if (ua != null)
            {
                log.Device = ua.OS.Family;
                log.BrowserType = ua.UA.Family;
            }
        }
        catch (Exception e)
        {
            Logger.LogError(exception: e, message: e.Message);
            await Task.CompletedTask;
        }

        return log;
    }

    public async Task<ActivityLog> AttachHttpContextInfoAsync(ActivityLog log)
    {
        var context = this.WebHelper.HttpContext;
        
        if (context == null)
        {
            this.Logger.LogInformation($"skip-{nameof(AttachHttpContextInfoAsync)},{nameof(context)}");
            return log;
        }

        await Task.CompletedTask;

        log.RequestPath = context.Request.Path;
        log.UrlReferrer = context.Request.Headers.Referer;
        log.UserAgent = context.Request.Headers.UserAgent;
        log.IpAddress = this.WebHelper.GetCurrentIpAddressOrNull();

        return log;
    }

    private async Task<bool> IgnoreActivityByKeyAsync(string key, TimeSpan timeToExpired)
    {
        var ignore = true;
        var option = new CacheOption<IdDto>(key, timeToExpired);

        await CacheProvider.GetOrSetAsync(async () =>
        {
            ignore = false;
            return await Task.FromResult(new IdDto());
        }, option);

        return ignore;
    }

    private async Task<bool> IgnoreActivityAsync(ActivityLog log)
    {
        if (log.ActivityLogTypeId == ActivityLogType.SearchGoods)
        {
            var key = $"activity.log.{log.ActivityLogTypeId}.{log.StoreUserId}.{log.Value}";
            if (await IgnoreActivityByKeyAsync(key, TimeSpan.FromMinutes(1)))
                return true;
        }
        else if (log.ActivityLogTypeId == ActivityLogType.VisitGoods)
        {
            var key = $"activity.log.{log.ActivityLogTypeId}.{log.StoreUserId}.{log.SubjectId}";
            if (await IgnoreActivityByKeyAsync(key, TimeSpan.FromMinutes(1)))
                return true;
        }
        else if (log.ActivityLogTypeId == ActivityLogType.VisitPage)
        {
            var key = $"activity.log.{log.ActivityLogTypeId}.{log.StoreUserId}.{log.SubjectId}";
            if (await IgnoreActivityByKeyAsync(key, TimeSpan.FromMinutes(1)))
                return true;
        }

        return false;
    }

    public virtual async Task InsertAsync(ActivityLog activityLog)
    {
        if (activityLog == null)
            throw new ArgumentNullException(nameof(activityLog));

        if (await IgnoreActivityAsync(activityLog))
            return;

        activityLog.Id = default;
        activityLog.CreationTime = Clock.Now;

        await _activityLogRepository.InsertAsync(activityLog);
    }

    public virtual async Task DeleteAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentNullException(nameof(id));

        var log = await _activityLogRepository.FirstOrDefaultAsync(x => x.Id == id);

        if (log == null)
            return;

        await _activityLogRepository.DeleteAsync(log);
    }

    public async Task<ActivityLogDto[]> AttachAdminAsync(ActivityLogDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.AdministratorId).WhereNotEmpty().Distinct().ToArray();

        var datalist = await this._adminService.GetByIdsAsync(ids);

        foreach (var m in data)
        {
            m.Admin = datalist.FirstOrDefault(x => x.Id == m.AdministratorId);
        }

        return data;
    }

    public async Task<ActivityLogDto[]> AttachStoreUserAsync(ActivityLogDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.StoreUserId).Where(x => x > 0).Distinct().ToArray();

        var datalist = await this._storeUserService.GetByIdsAsync(ids);

        foreach (var m in data)
        {
            m.StoreUser = datalist.FirstOrDefault(x => x.Id == m.StoreUserId);
        }

        return data;
    }

    public virtual async Task<PagedResponse<ActivityLogDto>> QueryPagingAsync(QueryActivityLogPagingInput dto)
    {
        var db = await _activityLogRepository.GetDbContextAsync();

        var loggingQuery = db.Set<ActivityLog>().AsNoTracking();

        loggingQuery = loggingQuery.WhereCreationTimeBetween(dto.StartTime, dto.EndTime);

        if (dto.ActivityLogTypeId != null)
            loggingQuery = loggingQuery.Where(x => x.ActivityLogTypeId == dto.ActivityLogTypeId.Value);

        if (dto.UserId != null)
            loggingQuery = loggingQuery.Where(x => x.StoreUserId == dto.UserId.Value);

        var count = await loggingQuery.CountOrDefaultAsync(dto);

        var datalist = await loggingQuery
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var items = datalist.MapArrayTo<ActivityLog, ActivityLogDto>(ObjectMapper);

        return new PagedResponse<ActivityLogDto>(items, dto, count);
    }

    public virtual async Task ClearExpiredDataWithLockAsync(DateTime endTime)
    {
        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(ClearExpiredDataWithLockAsync)}-clear-expired-activity-log",
            expiryTime: TimeSpan.FromMinutes(1));

        var maxId = 0;
        var batchSize = 200;
        var clearCount = default(int);

        while (true)
        {
            using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

            try
            {
                var db = await _activityLogRepository.GetDbContextAsync();

                var set = db.Set<ActivityLog>();

                var list = await set.AsTracking()
                    .Where(x => x.CreationTime <= endTime)
                    .Where(x => x.Id > maxId)
                    .OrderBy(x => x.Id)
                    .Take(batchSize)
                    .ToArrayAsync();

                if (!list.Any())
                {
                    break;
                }

                await this._activityLogRepository.DeleteManyAsync(list);

                await uow.CompleteAsync();

                //next loop
                maxId = list.Max(x => x.Id);
                clearCount += list.Length;
            }
            catch
            {
                await uow.RollbackAsync();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        this.Logger.LogInformation($"{nameof(ClearExpiredDataWithLockAsync)}-clear-count:{clearCount}");
    }
}