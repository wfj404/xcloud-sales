﻿using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface ITagService : ISalesStringCrudAppService<Tag, TagDto, QueryTagPaging>
{
    Task<TagDto[]> QueryAllAsync();
}

public class TagService : SalesStringCrudAppService<Tag, TagDto, QueryTagPaging>, ITagService
{
    public TagService(ISalesRepository<Tag> tagRepository) : base(tagRepository)
    {
        //
    }

    protected override async Task<IQueryable<Tag>> GetPagingFilteredQueryableAsync(DbContext db, QueryTagPaging dto)
    {
        await Task.CompletedTask;

        var query = db.Set<Tag>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.Name))
            query = query.Where(x => x.Name.Contains(dto.Name));

        return query;
    }

    public async Task<TagDto[]> QueryAllAsync()
    {
        var db = await Repository.GetDbContextAsync();

        var query = db.Set<Tag>().AsNoTracking();

        var data = await query.TakeUpTo5000().ToArrayAsync();

        var res = data.MapArrayTo<Tag, TagDto>(ObjectMapper);

        return res;
    }

    protected override async Task SetFieldsBeforeInsertAsync(Tag entity)
    {
        await base.SetFieldsBeforeInsertAsync(entity);
        entity.IsDeleted = false;
    }

    public override Task<Tag> InsertAsync(TagDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException("tag name is required");

        return base.InsertAsync(dto);
    }

    public override Task<Tag> UpdateAsync(TagDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException("tag name is required");

        return base.UpdateAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Tag entity, TagDto dto)
    {
        await Task.CompletedTask;

        entity.SetEntityFields(dto, x => new string[]
        {
            nameof(dto.Id),
            nameof(dto.IsDeleted),
        }.Contains(x.Name));
    }

    public override async Task DeleteByIdAsync(string id)
    {
        await this.Repository.SoftDeleteAsync(id, this.Clock.Now);
    }
}