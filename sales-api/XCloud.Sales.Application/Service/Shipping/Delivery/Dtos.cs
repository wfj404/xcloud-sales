﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Gis;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Shipping.Delivery;

public abstract class ShippingSettingsBase : ISettings
{
    public bool Enabled { get; set; }

    public bool PayAfterShippedSupported { get; set; }
}

public class DeliveryRecordItemDto : DeliveryRecordItem, IEntityDto<string>
{
    public OrderItemDto OrderItem { get; set; }
}

public class DeliveryRecordDto : DeliveryRecord, IEntityDto<string>
{
    public OrderDto Order { get; set; }
    public DeliveryRecordItemDto[] Items { get; set; }
}

public class QueryDeliveryRecordPagingInput : PagedRequest
{
    public bool? OrderToService { get; set; }

    public string OrderId { get; set; }

    public string StoreId { get; set; }
}

public class DeliveryRecordTrackDto : DeliveryRecordTrack, IEntityDto<int>
{
    //
}

public class QueryDeliveryRecordTrackPagingInput : PagedRequest
{
    //
}

public class DeliveryStatusEvent : IEntityDto, IHasCreationTime
{
    public string Status { get; set; }

    public string EventName { get; set; }

    public GeoLocationDto GeoLocation { get; set; }

    public DateTime CreationTime { get; set; }
}

public class DeliveryArea : IEntityDto
{
    public DeliveryArea()
    {
        //
    }

    public string ProvinceId { get; set; }

    public string CityId { get; set; }

    public string AreaId { get; set; }
}

public class TieredPrice : IEntityDto
{
    public int StartingAmount { get; set; }

    public decimal StartingPrice { get; set; }

    public int AddedUnitAmount { get; set; }

    public decimal AddedUnitPrice { get; set; }
}

public class SaveGoodsStoreExpressTemplateInput : IEntityDto
{
    public string GoodsId { get; set; }

    public string StoreId { get; set; }

    public string[] TemplateIds { get; set; }
}