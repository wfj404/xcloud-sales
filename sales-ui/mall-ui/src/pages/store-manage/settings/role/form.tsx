import { Button, Form, Input, message, Modal, Space, Spin } from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { apiClient } from '@/utils/client';
import { StoreRoleDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default (props: {
  show: boolean;
  hide: () => void;
  data: StoreRoleDto;
  ok: () => void;
}) => {
  const { show, hide, data, ok } = props;
  const [loading, _loading] = useState(false);
  const [form, _form] = useState<StoreRoleDto>({});

  const save = (row: StoreRoleDto) => {
    _loading(true);

    apiClient.mallManager.storeManagerPermissionSaveRole({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _form({
      ...data,
    });
  }, [data]);

  return (
    <>
      <Modal title={'角色'}
             open={show}
             onCancel={() => hide()}
             footer={
               <Space>
                 <XDeleteButton
                   action={async () => {
                     let res = await apiClient.sys.manageRoleDelete({ Id: form.Id });
                     handleResponse(res, () => {
                       hide();
                       ok();
                     });
                   }}
                   hide={isStringEmpty(form.Id)} />
                 <Button type={'primary'} onClick={() => {
                   save(form);
                 }}>保存</Button>
               </Space>
             }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign='right'
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item
              label='名称'
              rules={[{ required: true }, { max: 10 }]}
            >
              <Input value={form.Name || ''} onChange={e => {
                _form({
                  ...form,
                  Name: e.target.value,
                });
              }} />
            </Form.Item>
            <Form.Item label='描述' rules={[{ max: 50 }]}>
              <Input value={form.Description || ''} onChange={e => {
                _form({
                  ...form,
                  Description: e.target.value,
                });
              }} />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
