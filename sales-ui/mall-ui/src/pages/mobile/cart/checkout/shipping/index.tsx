import { Space } from 'antd-mobile';

import { DeliveryType, PaymentMethod, ShippingMethod } from '@/utils/biz';
import { PlaceOrderRequestDto } from '@/utils/swagger';
import { GlobalContainer } from '@/utils/utils';
import {
  DeliveryDiningOutlined,
  LocalShippingOutlined,
  LocationOnOutlined,
} from '@mui/icons-material';
import { Button, ButtonGroup } from '@mui/material';

import XAddress from './address';
import XPickupPoint from './pickup_point';

export default ({
  model,
  onChange,
}: {
  model: PlaceOrderRequestDto;
  onChange: (e: PlaceOrderRequestDto) => void;
}) => {
  const pickup = model.ShippingMethodId == ShippingMethod.Pickup;
  const delivery = model.ShippingMethodId == ShippingMethod.Delivery;
  const express = model.DeliveryType == DeliveryType.Express;
  const short_distance =
    model.DeliveryType == DeliveryType.ShortDistanceDelivery;

  const renderButtonGroup = () => {
    return (
      <ButtonGroup fullWidth variant="outlined">
        <Button
          startIcon={<LocalShippingOutlined />}
          variant={delivery && express ? 'contained' : 'outlined'}
          onClick={() => {
            if (model.PaymentMethodId != PaymentMethod.Online) {
              GlobalContainer.message?.warning(
                '当前交付方式只支持在线支付，已为您更改',
              );
            }
            onChange({
              ...model,
              ShippingMethodId: ShippingMethod.Delivery,
              DeliveryType: DeliveryType.Express,
              PaymentMethodId: PaymentMethod.Online,
              PreferDeliveryStartTime: undefined,
              PreferDeliveryEndTime: undefined,
            });
          }}
        >
          物流
        </Button>
        <Button
          startIcon={<DeliveryDiningOutlined />}
          variant={delivery && short_distance ? 'contained' : 'outlined'}
          onClick={() => {
            if (model.PaymentMethodId != PaymentMethod.Online) {
              GlobalContainer.message?.warning(
                '当前交付方式只支持在线支付，已为您更改',
              );
            }
            onChange({
              ...model,
              ShippingMethodId: ShippingMethod.Delivery,
              DeliveryType: DeliveryType.ShortDistanceDelivery,
              PaymentMethodId: PaymentMethod.Online,
              PreferDeliveryStartTime: undefined,
              PreferDeliveryEndTime: undefined,
            });
          }}
        >
          配送
        </Button>
        <Button
          startIcon={<LocationOnOutlined />}
          variant={pickup ? 'contained' : 'outlined'}
          onClick={() => {
            onChange({
              ...model,
              ShippingMethodId: ShippingMethod.Pickup,
              DeliveryType: undefined,
              PreferDeliveryStartTime: undefined,
              PreferDeliveryEndTime: undefined,
            });
          }}
        >
          自提
        </Button>
      </ButtonGroup>
    );
  };

  return (
    <>
      <Space direction="vertical">
        <div style={{ paddingBottom: 8 }}>{renderButtonGroup()}</div>
        {model.ShippingMethodId == ShippingMethod.Pickup || (
          <XAddress
            model={model}
            value={model.AddressId}
            onSelectedAddress={(address) => {
              console.log('address-select', address);
              onChange({
                ...model,
                AddressId: address?.Id,
              });
            }}
          />
        )}
        {model.ShippingMethodId == ShippingMethod.Pickup && <XPickupPoint />}
      </Space>
    </>
  );
};
