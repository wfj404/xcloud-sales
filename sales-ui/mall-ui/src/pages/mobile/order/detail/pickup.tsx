import { useEffect, useState } from 'react';

import { ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto, PickupRecordDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';

export default function(props: { model: OrderDto }) {
  const { model } = props;

  const [data, _data] = useState<PickupRecordDto[]>([]);
  const [loading, _loading] = useState(false);

  const queryList = (id: string) => {
    if (isStringEmpty(id)) {
      _data([]);
      return;
    }
    _loading(true);
    apiClient.mall
      .pickupGetByOrderId({ Id: id })
      .then((res) => {
        _data(res.data.Data || []);
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    model && queryList(model.Id || '');
  }, [model]);

  if (model.ShippingMethodId != ShippingMethod.Pickup) {
    return null;
  }

  return <>{JSON.stringify(data)}</>;
}
