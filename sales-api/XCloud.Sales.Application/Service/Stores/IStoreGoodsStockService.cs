using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreGoodsStockService : ISalesAppService
{
    Task UpdateStockQuantityAsync(string skuId, string storeId, int quantity);

    Task UpdateStockQuantityByOffsetAsync(string skuId, string storeId, int quantityOffset);
}

public class StoreGoodsStockService : SalesAppService, IStoreGoodsStockService
{
    private readonly ISalesRepository<StoreGoodsMapping> _repository;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;

    public StoreGoodsStockService(ISalesRepository<StoreGoodsMapping> repository,
        IStoreGoodsMappingService storeGoodsMappingService)
    {
        _repository = repository;
        _storeGoodsMappingService = storeGoodsMappingService;
    }

    public virtual async Task UpdateStockQuantityAsync(string skuId, string storeId, int quantity)
    {
        if (string.IsNullOrWhiteSpace(skuId))
            throw new ArgumentNullException(nameof(skuId));

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var mapping = await this._storeGoodsMappingService.GetOrCreateAsync(skuId, storeId);

        var entity = await this._repository.GetRequiredByIdAsync(mapping.Id);

        entity.StockQuantity = quantity;
        entity.LastModificationTime = Clock.Now;

        await _repository.UpdateAsync(entity);

        if (entity.StockQuantity <= 0)
        {
            Logger.LogWarning($"stock quantity is running out:{entity.Id}");
        }
    }

    public virtual async Task UpdateStockQuantityByOffsetAsync(string skuId, string storeId, int quantityOffset)
    {
        if (string.IsNullOrWhiteSpace(skuId))
            throw new ArgumentNullException(nameof(skuId));

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        if (quantityOffset == default)
        {
            this.Logger.LogWarning(message: $"{nameof(UpdateStockQuantityByOffsetAsync)}.offset==0");
            return;
        }

        var entity = await this._storeGoodsMappingService.GetOrCreateAsync(skuId, storeId);

        await this.UpdateStockQuantityAsync(skuId, storeId, entity.StockQuantity + quantityOffset);
    }
}