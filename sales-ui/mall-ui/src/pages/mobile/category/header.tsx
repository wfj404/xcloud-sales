import { useSize } from 'ahooks';
import React, { ReactNode, useRef } from 'react';
import { history, useSnapshot } from 'umi';

import XStore from '@/components/mobile/current_store/store';
import { storeState, StoreType } from '@/store';
import { SearchTwoTone } from '@mui/icons-material';
import { alpha, Container, IconButton, Stack } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Slide from '@mui/material/Slide';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import useScrollTrigger from '@mui/material/useScrollTrigger';

export default function ({ children }: { children?: ReactNode }) {
  const app = useSnapshot(storeState) as StoreType;

  const trigger = useScrollTrigger({
    target: window,
    threshold: 100,
  });

  const ref = useRef<HTMLElement>(null);
  const rect = useSize(ref);

  React.useEffect(() => {
    if (trigger) {
      app.setHeaderHeight(0);
    } else {
      app.setHeaderHeight(rect?.height || 0);
    }
  }, [rect, trigger]);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Slide appear={false} direction="down" in={!trigger}>
        <AppBar
          ref={ref}
          sx={(theme) => ({
            boxShadow: 'none',
            backdropFilter: 'blur(6px)',
            WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
            backgroundColor: alpha('#ffffff', 0.72),
            zIndex: 1,
          })}
          position={'fixed'}
        >
          <Container maxWidth="sm" disableGutters>
            <Toolbar
              sx={(theme) => ({
                color: 'black',
              })}
            >
              <Stack
                direction="row"
                alignItems="center"
                spacing={{ xs: 0.5, sm: 1.5 }}
              >
                <XStore />
              </Stack>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                flexGrow={1}
              >
                <Typography variant="h6">
                  {window.document.title || app.getAppName()}
                </Typography>
              </Box>
              <Stack
                direction="row"
                alignItems="center"
                spacing={{ xs: 0.5, sm: 1.5 }}
              >
                <IconButton
                  onClick={() => {
                    history.push({
                      pathname: '/search',
                    });
                  }}
                >
                  <SearchTwoTone color="action" fontSize="inherit" />
                </IconButton>
              </Stack>
            </Toolbar>
          </Container>
        </AppBar>
      </Slide>

      <Box sx={{ mt: `${app.headerHeight + 10}px` }}>{children}</Box>
    </Box>
  );
}
