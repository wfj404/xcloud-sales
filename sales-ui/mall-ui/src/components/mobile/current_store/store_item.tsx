import { StoreDto } from '@/utils/swagger';
import { isStringEmpty, setSelectedStoreId } from '@/utils/utils';
import { Divider, Paper, Stack, Typography } from '@mui/material';

export default ({
  store,
  currentId,
}: {
  store: StoreDto;
  currentId?: string | null;
}) => {
  if (!store) {
    return null;
  }

  const current = store.Id == currentId;

  const renderSpanText = (text: string) => {
    return (
      <Typography variant={'caption'} color="text.disabled" fontSize={10}>
        {text}
      </Typography>
    );
  };

  return (
    <>
      <Paper
        sx={{
          my: 1,
          p: 1,
          cursor: 'pointer',
          border: (t) => `1px dashed ${t.palette.primary.main}`,
          borderColor: 'transparent',
          '&:hover': {
            borderColor: (t) => t.palette.primary.main,
            backgroundColor: 'rgb(253,253,253)',
          },
        }}
        elevation={1}
        onClick={() => {
          if (!confirm(`确定选择[${store.Name}]作为当前门店？`)) {
            return;
          }
          setSelectedStoreId(store.Id || '');
          window.location.reload();
        }}
      >
        <Stack direction={'column'} spacing={1}>
          <Stack direction={'row'} spacing={1}>
            <Typography variant={'h6'} color={'black'} gutterBottom>
              {store.Name}
            </Typography>
            {current && (
              <Typography variant={'caption'} color={'error'}>
                当前选择
              </Typography>
            )}
          </Stack>
          <Stack
            direction={'row'}
            spacing={1}
            divider={<Divider orientation="vertical" flexItem />}
          >
            {isStringEmpty(store.Telephone) || (
              <Typography
                variant={'caption'}
                gutterBottom
                color={'text.disabled'}
              >
                {store.Telephone}
              </Typography>
            )}
            {isStringEmpty(store.AddressDetail) || (
              <Typography
                variant={'caption'}
                gutterBottom
                color={'text.disabled'}
              >
                {store.AddressDetail}
              </Typography>
            )}
          </Stack>
          <Stack
            direction={'row'}
            spacing={1}
            divider={<Divider orientation="vertical" flexItem />}
          >
            {store.Opening && renderSpanText('营业中')}
            {store.InvoiceSupported && renderSpanText('支持开发票')}
            {store.Distance &&
              store.Distance > 0 &&
              renderSpanText(`${(store.Distance / 1000).toFixed(2)}km`)}
            {store.InServiceArea != undefined &&
              store.InServiceArea &&
              renderSpanText('在服务范围内')}
          </Stack>
        </Stack>
      </Paper>
    </>
  );
};
