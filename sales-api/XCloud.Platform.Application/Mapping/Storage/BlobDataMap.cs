﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Storage;

namespace XCloud.Platform.Application.Mapping.Storage;

public class BlobDataMap : PlatformEntityTypeConfiguration<BlobData>
{
    public override void Configure(EntityTypeBuilder<BlobData> builder)
    {
        base.Configure(builder);

        builder.ToTable("sys_blob_data");

        builder.PreConfigModificationTimeEntity();
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Content);
        builder.Property(x => x.Base64Text);
        builder.Property(x => x.Format);

        builder.HasIndex(x => x.Name).IsUnique();
    }
}