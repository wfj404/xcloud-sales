import { Card, message, Table, Tag, Typography } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { sumBy } from 'lodash-es';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XRenderTime from '@/components/common/time/render_time';
import { PagedResponse, PaymentChannelList } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { PaymentBillDto, QueryOrderBillPagingInput } from '@/utils/swagger';
import { formatMoney } from '@/utils/utils';

import XDetail from './detail';
import XSearchForm from './search_form';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<PaymentBillDto>>({});
  const [query, _query] = useState<QueryOrderBillPagingInput>({
    Page: 1,
  });

  const queryList = (e: QueryOrderBillPagingInput) => {
    _loading(true);
    apiClient.mallManager
      .paymentBillQueryPaging({
        ...query,
        Paid: true,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data || {});
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnProps<PaymentBillDto>[] = [
    {
      title: '支付单号',
      width: 200,
      render: (d, x) => {
        return (
          <span>
            <Typography.Paragraph copyable>{x.Id}</Typography.Paragraph>
          </span>
        );
      },
    },
    {
      title: '订单号',
      render: (d, x) => {
        return (
          <>
            <div>{x.Order?.OrderSn || '--'}</div>
            <div>
              总金额：<b>{formatMoney(x.Order?.TotalPrice || 0)}</b>
            </div>
          </>
        );
      },
    },
    {
      title: '金额',
      render: (d, x) => {
        const refund_price = sumBy(x.RefundBills || [], (x) => x.Price || 0);
        return (
          <>
            <Tag color="blue">{formatMoney(x.Price || 0)}</Tag>
            {refund_price > 0 && (
              <p style={{ color: 'red' }}>退款：{formatMoney(refund_price)}</p>
            )}
          </>
        );
      },
    },
    {
      title: '支付方式',
      render: (d, x) => {
        return (
          <span>
            <b>
              {PaymentChannelList.find((d) => d.id == x.PaymentChannel)?.name ||
                x.PaymentChannel ||
                '--'}
            </b>
          </span>
        );
      },
    },
    {
      title: '支付状态',
      render: (d, x) => {
        return x.Paid && <Tag color="green">已支付</Tag>;
      },
    },
    {
      title: '支付时间',
      render: (d, x) => <XRenderTime timeStr={x.PayTime} />,
    },
    {
      title: '时间',
      render: (d, x) => {
        return <XTime model={x} />;
      },
    },
  ];

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <XSearchForm
        query={query}
        onSearch={(q: QueryOrderBillPagingInput) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card title="支付单">
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          expandable={{
            expandedRowRender: (x) => (
              <div style={{ padding: 10, border: '5px dashed orange' }}>
                <XDetail model={x} />
              </div>
            ),
          }}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
