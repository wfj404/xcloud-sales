import { useSnapshot } from 'umi';

import avatar_url from '@/assets/static/images/avatars/avatar_21.jpg';
import { storeState, StoreType } from '@/store';
import { getUserName } from '@/utils/biz';
import { resolveAvatar } from '@/utils/storage';
import { UserDto } from '@/utils/swagger';
import { isStringEmpty, simpleString } from '@/utils/utils';
import { Avatar, Box, Paper, Typography } from '@mui/material';

import XNotification from './components/notification';

const index = function() {
  const app = useSnapshot(storeState) as StoreType;

  const renderUserChip = () => {
    if (!isStringEmpty(app.storeUser?.Grade?.Name)) {
      return (
        <Typography
          sx={{
            display: 'inline-block',
            textTransform: 'uppercase',
            whiteSpace: 'nowrap',
            lineHeight: 1,
            fontWeight: 600,
            fontStyle: 'normal',
            //marginInlineStart: '.66em',
            borderRadius: '.25em',
            padding: '.33em',
            color: '#000',
            backgroundColor: '#fec846',
          }}
          variant='overline'
          component={'em'}
        >
          <span>{app.storeUser?.Grade?.Name}</span>
        </Typography>
      );
    }

    return null;
  };

  return (
    <>
      <Paper
        sx={{
          mb: 3,
          pt: 5,
          pb: 3,
          px: 2,
          background: 'none',
        }}
        elevation={0}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}
        >
          <Avatar
            variant='rounded'
            src={resolveAvatar(app.storeUser?.User?.Avatar || '', {
              width: 80,
              height: 80,
            })}
            sx={{
              width: 80,
              height: 80,
            }}
          >
            <img src={avatar_url} alt={''} />
          </Avatar>
          <Box sx={{ ml: 2 }}>
            <Typography
              gutterBottom
              variant='h5'
              component='div'
              sx={{
                whiteSpace: 'nowrap',
              }}
            >
              {simpleString(getUserName(app.storeUser?.User as UserDto) || '新用户', 8)}
            </Typography>
            <Typography variant='caption' color='text.disabled' component={'div'}>
              <span>{`ID:@${app.storeUser?.Id}`}</span>
            </Typography>
            <div>
              {renderUserChip()}
            </div>
          </Box>
          <Box
            sx={{
              width: '100%',
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          >
            <Box
              sx={{
                display: 'inline-block',
              }}
            >
              <XNotification />
            </Box>
          </Box>
        </Box>
      </Paper>
    </>
  );
};

export default index;
