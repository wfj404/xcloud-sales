﻿using System.Reflection;

namespace XCloud.Core.Http.Dynamic.Proxy;

public class MethodWrapper
{
    public MethodInfo Method { get; }

    public MethodWrapper(MethodInfo m)
    {
        Method = m ?? throw new ArgumentNullException(nameof(m));
    }
}