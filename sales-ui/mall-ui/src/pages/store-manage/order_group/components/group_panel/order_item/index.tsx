import { Alert, Checkbox, Space } from 'antd';

import { OrderGroupOrderDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';

import XAddress from './address';
import XForm from './form';
import XItems from './items';
import XPaymentBills from './payment_bill';

export default ({
  order,
  onChange,
}: {
  order: OrderGroupOrderDto;
  onChange: (e: OrderGroupOrderDto) => void;
}) => {
  if (!order || !onChange) {
    return <h1>error</h1>;
  }

  const renderSenderPanel = () => {
    return (
      <div>
        <Space direction="horizontal">
          <Checkbox
            checked={order.SpecificSender}
            onChange={(e) => {
              onChange({ ...order, SpecificSender: e.target.checked });
            }}
          />
          <b>使用单独发件人</b>
        </Space>
        {order.SpecificSender && (
          <XAddress
            nameLabel={'发件人'}
            address={order.Sender || {}}
            onChange={(e) => {
              onChange({ ...order, Sender: e });
            }}
          />
        )}
      </div>
    );
  };

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        {renderSenderPanel()}
        <XAddress
          address={order.UserAddress || {}}
          onChange={(e) => {
            onChange({ ...order, UserAddress: e });
          }}
        />
        <XForm
          order={order}
          onChange={(e) => {
            onChange(e || {});
          }}
        />
        {isArrayEmpty(order.Items) && (
          <Alert message={'请添加商品'} type="error" />
        )}
        <XItems
          items={order.Items || []}
          onChange={(e) => {
            onChange({
              ...order,
              Items: e,
            });
          }}
        />
        <XPaymentBills
          bills={order.PaymentBills || []}
          items={order.Items || []}
          onChange={(e) => {
            onChange({
              ...order,
              PaymentBills: e,
            });
          }}
        />
      </Space>
    </>
  );
};
