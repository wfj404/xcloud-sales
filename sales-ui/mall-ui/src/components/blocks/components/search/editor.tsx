import { Form, Input } from 'antd';

import { SearchBlockProps } from './utils';

export default function({ data, onChange }: {
  data: SearchBlockProps,
  onChange: (d: SearchBlockProps) => void
}) {

  if (!data || !onChange) {
    return null;
  }

  return <>
    <Form labelCol={{ span: 3 }}>
      <Form.Item label={'placeholder'}>
        <Input value={data?.placeholder} onChange={e => {
          onChange && onChange({ ...data, placeholder: e.target.value });
        }} />
      </Form.Item>
    </Form>
  </>;
}
