﻿using Volo.Abp.Application.Dtos;
using XCloud.Platform.Application.Domain.Common;
using XCloud.Platform.Application.Service.Storage;

namespace XCloud.Platform.Application.Service.Common;

public static class AreaTypeEnum
{
    public static string Other => "other";
    public static string Continent => "continent";
    public static string Country => "country";
    public static string Area => "area";
}

public class AreaDto : Area, IEntityDto<string>
{
    public AreaDto Parent { get; set; }
    public AreaDto[] Children { get; set; }
    public AreaDto[] PathList { get; set; }
    public StorageMetaDto StorageMeta { get; set; }
}