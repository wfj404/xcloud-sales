﻿global using System;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.Logging;
global using Microsoft.AspNetCore.Mvc;
global using XCloud.Sales.Application.Framework;
global using XCloud.Core.Extension;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Logging;
using XCloud.Core.Configuration;
using XCloud.Sales.Application.Extended;
using XCloud.Sales.Configuration;

namespace XCloud.Sales.WebApi;

[DependsOn(
    typeof(SalesApplicationExtendedModule),
    typeof(SalesConfigurationModule)
)]
[MethodMetric]
public class SalesWebApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddXCloudBuilder<SalesWebApiModule>();
        StaticLoggerFactoryHolder.LoggerFactory?.CreateLogger<SalesWebApiModule>()
            .LogInformation("add x-cloud builder");
    }
}