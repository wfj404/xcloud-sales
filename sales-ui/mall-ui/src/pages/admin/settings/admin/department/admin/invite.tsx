import { useRequest } from 'ahooks';
import { message, Modal } from 'antd';

import XUserSelector from '@/components/manage/user/user_selector';
import { apiClient } from '@/utils/client';
import { DepartmentDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  department,
  ok,
}: {
  show: boolean;
  hide: () => void;
  department?: DepartmentDto;
  ok: () => void;
}) => {
  const set_department_request = useRequest(
    apiClient.sys.departmentSetAdminDepartments,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          hide();
          ok();
        });
      },
    },
  );

  const add_request = useRequest(apiClient.sys.manageAdminSetAsAdmin, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        const admin = data.data.Data;
        if (admin) {
          set_department_request.run({
            Key: admin.Id,
            Value: [department?.Id || ''].filter((x) => !isStringEmpty(x)),
          });
        } else {
          message.error('添加失败');
        }
      });
    },
  });

  return (
    <>
      <Modal
        title={'搜索用户添加'}
        confirmLoading={add_request.loading || set_department_request.loading}
        open={show}
        onCancel={() => hide()}
        footer={false}
      >
        <XUserSelector
          onFind={(e) => {
            add_request.run({ UserId: e.Id });
          }}
        />
      </Modal>
    </>
  );
};
