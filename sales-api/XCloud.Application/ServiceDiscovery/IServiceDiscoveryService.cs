using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Dto;

namespace XCloud.Application.ServiceDiscovery;

public interface IServiceDiscoveryProvider
{
    string ProviderName { get; }

    int Order { get; }

    Task<ServiceDiscoveryResponseDto> ResolveServiceOrNullAsync(string name);
}

public interface IServiceDiscoveryService
{
    Task<ServiceDiscoveryResponseDto> ResolveServiceAsync(string name);
}

[ExposeServices(typeof(IServiceDiscoveryService))]
public class ServiceDiscoveryService : IServiceDiscoveryService, IScopedDependency
{
    private readonly IServiceDiscoveryProvider[] _providers;

    public ServiceDiscoveryService(IServiceProvider serviceProvider)
    {
        _providers = serviceProvider.GetServices<IServiceDiscoveryProvider>()
            .OrderByDescending(x => x.Order)
            .ToArray();
    }


    public async Task<ServiceDiscoveryResponseDto> ResolveServiceAsync(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        foreach (var provider in _providers)
        {
            var response = await provider.ResolveServiceOrNullAsync(name);
            if (response != null)
            {
                return response;
            }
        }

        var providerNames = this._providers.Select(x => x.ProviderName).ToArray();

        return new ServiceDiscoveryResponseDto().SetError(
            message: $"service:{name} not found",
            details: $"providers:{string.Join(',', providerNames)}");
    }
}