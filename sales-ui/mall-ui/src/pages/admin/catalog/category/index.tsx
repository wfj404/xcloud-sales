import { Badge, Button, Card, message, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import PreviewGroup from '@/components/common/preview_group';
import XTime from '@/components/common/time';
import { AntDesignTreeNodeV2, getFlatNodes } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { CategoryDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty } from '@/utils/utils';

import XForm from './form';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<AntDesignTreeNodeV2<CategoryDto>[]>([]);
  const [expandKeys, _expandKeys] = useState<string[]>([]);

  const [formData, _formData] = useState<
    AntDesignTreeNodeV2<CategoryDto> | undefined
  >(undefined);

  const queryList = () => {
    _loading(true);
    apiClient.mallAdmin
      .categoryQueryAntdTree({})
      .then((res) => {
        handleResponse(res, () => {
          const treeData = res.data.Data || [];
          _data(treeData);
          const keys = treeData
            .flatMap((x) => getFlatNodes(x))
            .map((x) => x.key || '');
          //_expandKeys(keys);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnType<AntDesignTreeNodeV2<CategoryDto>>[] = [
    {
      title: '名称',
      render: (d, x) => x.title,
    },
    {
      title: 'SeoName',
      render: (d, x) => x.raw_data?.SeoName || '--',
    },
    {
      title: '描述',
      render: (d, x) => x.raw_data?.Description,
    },
    {
      title: '图片',
      render: (d, x) => {
        const record = x.raw_data || {};

        if (record.Picture == null) {
          return null;
        }

        return (
          <>
            <PreviewGroup data={[record.Picture]} />
          </>
        );
      },
    },
    {
      title: '排序',
      render: (d, x) => x.raw_data?.DisplayOrder,
    },
    {
      title: '标记',
      render: (d, x) => {
        const data = x.raw_data || {};
        return (
          <>
            <Space direction="vertical">
              {data.Recommend && <Tag color="red">推荐</Tag>}
              {data.ShowOnHomePage && <Tag color="green">首页展示</Tag>}
            </Space>
          </>
        );
      },
    },
    {
      title: '状态',
      render: (d, x) => {
        const data = x.raw_data || {};
        return (
          <>
            {data.Published && <Badge status={'success'} text={'可用'} />}
            {data.Published || <Badge status={'error'} text={'不可用'} />}
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x.raw_data} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <>
            <Space direction="horizontal">
              <Button
                type="link"
                onClick={() => {
                  _formData(x);
                }}
              >
                编辑
              </Button>
              {isArrayEmpty(x.children) && (
                <XDeleteButton
                  action={async () => {
                    const res = await apiClient.mallAdmin.categoryDelete({
                      Id: x.raw_data?.Id,
                    });
                    handleResponse(res, () => {
                      message.success('删除成功');
                      queryList();
                    });
                  }}
                />
              )}
            </Space>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData?.raw_data || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title="产品分类"
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.key || ''}
          expandable={{
            expandedRowKeys: expandKeys,
            defaultExpandAllRows: true,
            showExpandColumn: true,
            onExpandedRowsChange: (expandedKeys) => {
              _expandKeys(expandedKeys as any);
            },
          }}
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Card>
    </>
  );
};
