﻿using System.Threading.Tasks;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Settings;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Settings;

public interface ISettingManageService : IPlatformCrudService<Setting, SettingDto, QuerySettingsPagingInput>
{
    //
}

public class SettingManageService : PlatformCrudService<Setting, SettingDto, QuerySettingsPagingInput>,
    ISettingManageService
{
    public SettingManageService(IPlatformRepository<Setting> repository) : base(repository)
    {
        //
    }

    public override Task<Setting> InsertAsync(SettingDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<Setting> UpdateAsync(SettingDto dto)
    {
        throw new NotSupportedException();
    }

    protected override Task<IQueryable<Setting>> GetPagingFilteredQueryableAsync(DbContext db,
        QuerySettingsPagingInput dto)
    {
        return base.GetPagingFilteredQueryableAsync(db, dto);
    }

    public override Task<PagedResponse<SettingDto>> QueryPagingAsync(QuerySettingsPagingInput dto)
    {
        return base.QueryPagingAsync(dto);
    }
}