﻿using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Search;

public interface IGoodsSearchService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<PagedResponse<GoodsDto>> SearchGoodsAsync([NotNull] QueryGoodsPaging dto);
}

public class GoodsSearchService : SalesAppService, IGoodsSearchService
{
    private readonly ICategoryService _categoryService;
    private readonly ISalesRepository<Goods> _goodsRepository;

    public GoodsSearchService(
        ICategoryService categoryService,
        ISalesRepository<Goods> goodsRepository)
    {
        _categoryService = categoryService;
        _goodsRepository = goodsRepository;
    }

    public virtual async Task<PagedResponse<GoodsDto>> SearchGoodsAsync(QueryGoodsPaging dto)
    {
        var db = await _goodsRepository.GetDbContextAsync();

        var goodsQuery = db.Set<Goods>().AsNoTracking();

        if (dto.IsPublished != null)
            goodsQuery = goodsQuery.Where(x => x.Published == dto.IsPublished.Value);

        if (!string.IsNullOrWhiteSpace(dto.BrandId))
            goodsQuery = goodsQuery.Where(q => q.BrandId == dto.BrandId);

        if (dto.WithoutBrand ?? false)
            goodsQuery = goodsQuery.Where(x => x.BrandId == null || x.BrandId == string.Empty);

        if (dto.WithoutCategory ?? false)
            goodsQuery = goodsQuery.Where(x => x.CategoryId == null || x.CategoryId == string.Empty);

        if (!string.IsNullOrWhiteSpace(dto.CategoryId))
        {
            var ids = await _categoryService.QueryCategoryAndAllChildrenIdsWithCacheAsync(dto.CategoryId,
                this.CacheProvider);

            goodsQuery = goodsQuery.Where(x => ids.Contains(x.CategoryId));
        }

        //tag query
        if (!string.IsNullOrWhiteSpace(dto.TagId))
        {
            var goodsIds = db.Set<TagGoodsMapping>().AsNoTracking()
                .Where(x => x.TagId == dto.TagId)
                .Select(x => x.GoodsId);

            goodsQuery = goodsQuery.Where(x => goodsIds.Contains(x.Id));
        }

        var skuQuery = db.Set<Sku>().AsNoTracking();
        var originSkuQuery = skuQuery;

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
        {
            var skuIds = db.Set<StoreGoodsMapping>().AsNoTracking()
                .Where(x => x.StoreId == dto.StoreId)
                .Select(x => x.SkuId);

            skuQuery = skuQuery.Where(x => skuIds.Contains(x.Id));
        }

        if (!ReferenceEquals(skuQuery, originSkuQuery))
        {
            var goodsIds = skuQuery.Select(x => x.GoodsId);
            if (dto.AlwaysShowFunctionalGoods ?? false)
            {
                goodsQuery = goodsQuery.Where(x =>
                    x.LimitedToContact || x.LimitedToRedirect || x.DisplayAsInformationPage ||
                    goodsIds.Contains(x.Id));
            }
            else
            {
                goodsQuery = goodsQuery.Where(x => goodsIds.Contains(x.Id));
            }
        }

        //keywords query
        if (!string.IsNullOrWhiteSpace(dto.Keywords))
        {
            var tagQuery = from mapping in db.Set<TagGoodsMapping>().AsNoTracking()
                join tag in db.Set<Tag>().AsNoTracking()
                    on mapping.TagId equals tag.Id
                select new { mapping, tag };

            tagQuery = tagQuery.Where(x => x.tag.Name == dto.Keywords);

            //query with like
            goodsQuery = goodsQuery.Where(x =>
                x.Name.Contains(dto.Keywords) ||
                x.Description.Contains(dto.Keywords) ||
                tagQuery.Any(d => d.mapping.GoodsId == x.Id));
        }

        var count = await goodsQuery.CountOrDefaultAsync(dto);

        var items = await goodsQuery
            .OrderByDescending(x => x.StickyTop)
            .ThenByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var datalist = items.MapArrayTo<Goods, GoodsDto>(ObjectMapper);

        return new PagedResponse<GoodsDto>(datalist, dto, count);
    }
}