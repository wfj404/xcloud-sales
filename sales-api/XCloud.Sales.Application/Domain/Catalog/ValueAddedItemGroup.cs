namespace XCloud.Sales.Application.Domain.Catalog;

public class ValueAddedItemGroup : SalesBaseEntity<string>
{
    public string Name { get; set; }

    public string Description { get; set; }

    public string GoodsId { get; set; }

    public bool IsRequired { get; set; }

    public bool MultipleChoice { get; set; }
}