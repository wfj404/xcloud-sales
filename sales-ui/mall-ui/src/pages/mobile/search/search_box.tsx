import React, { useEffect, useState } from 'react';
import { history } from 'umi';

import XSearchBox from '@/components/common/search_box';
import { QueryGoodsPaging } from '@/utils/swagger';
import { ClearTwoTone, WidgetsOutlined } from '@mui/icons-material';
import { Box, IconButton, Tooltip } from '@mui/material';

import XAddon from './search_box_addon';
import { isEmptyGoodsQueryFilter } from './utils';

export default function ({
  finalQuery,
  onSearch,
}: {
  finalQuery: QueryGoodsPaging;
  onSearch: (d: QueryGoodsPaging) => void;
}) {
  const [keywords, _keywords] = useState<string>('');

  useEffect(() => {
    _keywords(finalQuery.Keywords || '');
  }, [finalQuery.Keywords]);

  const renderClearButton = () => {
    if (isEmptyGoodsQueryFilter(finalQuery)) {
      return null;
    }
    return (
      <Tooltip title="清空搜索条件">
        <IconButton
          size="small"
          onClick={() => {
            onSearch({});
          }}
          sx={{
            backgroundColor: 'rgb(240,240,240)',
            scale: 0.8,
            mr: 0.5,
          }}
        >
          <ClearTwoTone fontSize="inherit" />
        </IconButton>
      </Tooltip>
    );
  };

  return (
    <>
      <Box
        sx={{
          mb: 2,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}
      >
        <div style={{ flexGrow: 1 }}>
          <XSearchBox
            //hideSearchIcon={!isEmptyGoodsQueryFilter(finalQuery)}
            hideSearchButton={!isEmptyGoodsQueryFilter(finalQuery)}
            addonAfter={renderClearButton()}
            addonBefore={
              <XAddon
                query={finalQuery}
                onSearch={(e) => {
                  onSearch({ ...e, Page: 1 });
                }}
              />
            }
            keywords={keywords}
            onChange={(e) => {
              _keywords(e);
            }}
            onSearch={(e) => {
              onSearch({
                ...finalQuery,
                Keywords: keywords,
                Page: 1,
              });
            }}
          />
        </div>
        <Tooltip title="品牌">
          <IconButton
            onClick={() => {
              history.push({
                pathname: '/brand',
              });
            }}
          >
            <WidgetsOutlined />
          </IconButton>
        </Tooltip>
      </Box>
    </>
  );
}
