import { Button, Card, Form, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { QueryGiftCardPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';

export default (props: {
  query: QueryGiftCardPagingInput;
  onSearch: (d: QueryGiftCardPagingInput) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QueryGiftCardPagingInput>({});

  const triggerSearch = (e: QueryGiftCardPagingInput) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form({ ...query });
  }, [query]);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form>
        <Form.Item label="是否使用">
          <Switch checked={false} disabled />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            icon={<SearchOutlined />}
            onClick={() => {
              triggerSearch(form);
            }}
          >
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};
