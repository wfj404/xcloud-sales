﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Messenger.Service;

namespace XCloud.Platform.Messenger.Job;

[ExposeServices(typeof(CleanExpiredRegistrationJob))]
public class CleanExpiredRegistrationJob : PlatformAppService, IScopedDependency
{
    private readonly IUserRegistrationService _userRegistrationService;
    private readonly IServerInstanceService _serverInstanceService;

    public CleanExpiredRegistrationJob(IUserRegistrationService userRegistrationService,
        IServerInstanceService serverInstanceService)
    {
        _userRegistrationService = userRegistrationService;
        _serverInstanceService = serverInstanceService;
    }

    public async Task ExecuteAsync()
    {
        await this._userRegistrationService.DeleteExpiredAsync();
        await this._serverInstanceService.DeleteExpiredAsync();
    }
}