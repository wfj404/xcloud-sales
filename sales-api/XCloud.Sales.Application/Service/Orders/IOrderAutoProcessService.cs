using XCloud.Application.DistributedLock;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderAutoProcessService : ISalesAppService
{
    Task AutoProcessOrderAsync(string orderId);

    Task CloseDeadOrdersAsync();
}

public class OrderAutoProcessService : SalesAppService, IOrderAutoProcessService
{
    private readonly OrderUtils _orderUtils;
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly IOrderProcessingService _orderProcessingService;

    public OrderAutoProcessService(OrderUtils orderUtils,
        ISalesRepository<Order> orderRepository,
        IOrderProcessingService orderProcessingService)
    {
        _orderUtils = orderUtils;
        _orderRepository = orderRepository;
        _orderProcessingService = orderProcessingService;
    }

    private bool MarkAsProcessing(Order order)
    {
        if (order.IsProcessing())
            return false;

        var condition1 = order.IsPending() && order.IsToService();
        var condition2 = order.IsToService() && order.IsShipping();

        return condition1 || condition2;
    }

    private bool MarkAsFinished(Order order)
    {
        if (order.IsFinished())
            return false;

        var condition1 = order.IsProcessing() && order.IsPaid() && order.IsShipped();

        return condition1;
    }

    [UnitOfWork(isTransactional: true)]
    public async Task AutoProcessOrderAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, orderId);

        if (this.MarkAsProcessing(order))
        {
            await this._orderProcessingService.MarkAsProcessingAsync(orderId: order.Id);

            await AutoProcessOrderAsync(order.Id);
        }
        else if (MarkAsFinished(order))
        {
            await _orderProcessingService.FinishAsync(new FinishOrderInput { OrderId = order.Id });

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }
    }

    private async Task ClosePendingOrderAsync()
    {
        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(OrderAutoProcessService)}.{nameof(ClosePendingOrderAsync)}",
            expiryTime: TimeSpan.FromMinutes(3));

        var end = Clock.Now.AddMinutes(-10);

        var db = await _orderRepository.GetDbContextAsync();

        while (true)
        {
            var query = db.Set<Order>().AsNoTracking()
                .Where(x => x.CreationTime < end)
                .Where(x => x.OrderStatusId == SalesConstants.OrderStatus.Pending)
                .Where(x => x.PaymentStatusId == SalesConstants.PaymentStatus.Pending);

            query = query.OrderBy(x => x.CreationTime).ThenBy(x => x.Id);

            var items = await query.Take(10).ToArrayAsync();

            if (!items.Any())
            {
                this.Logger.LogInformation($"finished:{nameof(ClosePendingOrderAsync)}");
                break;
            }

            foreach (var m in items)
            {
                await _orderProcessingService.CloseAsync(new CloseOrderInput
                {
                    OrderId = m.Id,
                    Comment = "auto close"
                });
            }

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }
    }

    public async Task CloseDeadOrdersAsync()
    {
        await ClosePendingOrderAsync();
    }
}