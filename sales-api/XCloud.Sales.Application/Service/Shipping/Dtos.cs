using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;

namespace XCloud.Sales.Application.Service.Shipping;

public class TimeRangeDto : IEntityDto<string>
{
    public string Id { get; set; }

    public double StartSecond { get; set; }

    public double EndSecond { get; set; }

    public int TotalTickets { get; set; }

    public DateTime? StartTime { get; set; }

    [Obsolete] public DateTime? Start => this.StartTime;

    public DateTime? EndTime { get; set; }

    [Obsolete] public DateTime? End => this.EndTime;

    public int? Tickets { get; set; }
}

public class ServiceTimeDto : IEntityDto
{
    private CronRule[] _cronRules;

    [NotNull]
    public CronRule[] CronRules
    {
        get => _cronRules ??= Array.Empty<CronRule>();
        set => _cronRules = value;
    }

    private TimeRangeDto[] _timeRanges;

    [NotNull]
    public TimeRangeDto[] TimeRanges
    {
        get => _timeRanges ??= Array.Empty<TimeRangeDto>();
        set => _timeRanges = value;
    }

    private ExceptionTime[] _exceptionTimes;

    [NotNull]
    public ExceptionTime[] ExceptionTimes
    {
        get => _exceptionTimes ??= Array.Empty<ExceptionTime>();
        set => _exceptionTimes = value;
    }
}

public class CronRule : IEntityDto
{
    public string Name { get; set; }

    public string Cron { get; set; }
}

public class ExceptionTime : IEntityDto
{
    public string Name { get; set; }

    public DateTime StartTime { get; set; }

    public DateTime EndTime { get; set; }

    public bool Include { get; set; }
}

public class AvailableServiceTimeRangeDto : IEntityDto
{
    public DateTime Date { get; set; }

    public TimeRangeDto[] TimeRanges { get; set; }
}