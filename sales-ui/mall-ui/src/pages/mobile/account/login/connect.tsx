import { useLocalStorageState, useRequest } from 'ahooks';
import qs from 'qs';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { concatUrl, isWechatBrowser } from '@/utils/utils';
import { WechatOutlined } from '@ant-design/icons';
import {
  Badge,
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Stack,
  Typography,
} from '@mui/material';

export default function () {
  const [openWechat, _openWechat] = useState(false);
  const [usewechatprofile, _usewechatprofile] = useLocalStorageState<boolean>(
    'usewechatprofile',
    {
      serializer(value) {
        return value ? '1' : '0';
      },
      deserializer(value) {
        return value == '1';
      },
    },
  );

  const query_appid_request = useRequest(
    apiClient.platform.wechatMpAuthGetWechatMpOption,
    { manual: true },
  );
  const appid = query_appid_request.data?.data?.Data?.AppId;

  const wechatLogin = isWechatBrowser();

  const startWxMpAuthorize = () => {
    const callback = concatUrl([
      window.location.origin,
      '/store/account/wx-callback',
    ]);
    console.log(callback);
    const p = qs.stringify(
      {
        appid: appid,
        redirect_uri: callback,
        response_type: 'code',
        scope: 'snsapi_userinfo',
        state: 'STATE',
      },
      {
        addQueryPrefix: false,
      },
    );

    let url = 'https://open.weixin.qq.com/connect/oauth2/authorize';
    url = `${url}?${p}#wechat_redirect`;
    console.log(url);
    window.location.href = url;
  };

  useEffect(() => {
    if (wechatLogin) {
      query_appid_request.run({});
    }
  }, []);

  if (!wechatLogin) {
    return null;
  }

  if (!appid) {
    return null;
  }

  return (
    <>
      <Dialog
        open={openWechat}
        onClose={() => {
          _openWechat(false);
        }}
      >
        <DialogTitle>使用微信授权登录</DialogTitle>
        <DialogContent>
          <Typography variant="overline" color={'primary'} component="div">
            即将跳转到微信授权登录！
          </Typography>
          <FormControlLabel
            control={
              <Checkbox
                checked={usewechatprofile}
                onChange={(e) => {
                  _usewechatprofile(e.target.checked);
                }}
              />
            }
            label="使用微信的昵称和头像"
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              _openWechat(false);
            }}
          >
            取消
          </Button>
          <Button
            onClick={() => {
              startWxMpAuthorize();
            }}
          >
            确定
          </Button>
        </DialogActions>
      </Dialog>
      <Box sx={{ py: 3 }}>
        <Stack
          spacing={2}
          direction={'row'}
          alignContent="center"
          justifyContent={'flex-end'}
        >
          {wechatLogin && (
            <Badge badgeContent="推荐" color="error">
              <Button
                onClick={() => {
                  _openWechat(true);
                }}
                variant="outlined"
                startIcon={<WechatOutlined />}
                color="success"
              >
                微信登录
              </Button>
            </Badge>
          )}
        </Stack>
      </Box>
    </>
  );
}
