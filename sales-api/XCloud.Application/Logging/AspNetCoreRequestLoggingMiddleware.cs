﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Volo.Abp;
using Volo.Abp.DependencyInjection;

namespace XCloud.Application.Logging;

public class AspNetCoreRequestLoggingMiddleware : IMiddleware, ITransientDependency
{
    public AspNetCoreRequestLoggingMiddleware()
    {
        //
    }

    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        using (new DisposeAction(() => { }))
        {
            await next.Invoke(context);
        }
    }
}