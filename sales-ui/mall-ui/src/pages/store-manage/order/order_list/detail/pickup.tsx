import { Card } from 'antd';
import React, { useEffect } from 'react';

import { apiClient } from '@/utils/client';
import { OrderDto, PickupRecordDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XPickupList from '../../../shipping/pickup/list';

const App = (props: { model: OrderDto, ok: () => void }) => {
  const { model, ok } = props;

  const [data, _data] = React.useState<PickupRecordDto[]>([]);
  const [loading, _loading] = React.useState<boolean>(false);

  const queryList = () => {
    if (isStringEmpty(model.Id)) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .pickupGetByOrderId({ Id: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList();
  }, [model]);

  return (
    <>
      <Card
        title='自提单'
        loading={loading}
        style={{
          marginBottom: 10,
          borderColor: 'blue',
        }}
      >
        <XPickupList
          data={data}
          pagination={false}
          refresh={() => {
            //queryList();
            ok();
          }}
        />
      </Card>
    </>
  );
};

export default App;
