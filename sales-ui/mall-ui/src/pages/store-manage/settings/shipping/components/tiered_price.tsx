import { InputNumber, Space } from 'antd';

import { TieredPrice } from '@/utils/swagger';

export default ({
  data,
  onChange,
  unit,
}: {
  unit?: string;
  data: TieredPrice;
  onChange: (e: TieredPrice) => void;
}) => {
  if (!data) {
    return null;
  }

  const unitOrDefault = unit || 'kg';

  return (
    <>
      <Space
        direction="horizontal"
        size={'small'}
        style={{
          width: '100%',
        }}
      >
        <InputNumber
          changeOnWheel
          min={0}
          addonAfter={`${unitOrDefault}以内`}
          value={data.StartingAmount}
          onChange={(e) => {
            onChange({
              ...data,
              StartingAmount: e || 0,
            });
          }}
        />
        <InputNumber
          changeOnWheel
          min={0}
          addonAfter="元"
          value={data.StartingPrice}
          onChange={(e) => {
            onChange({
              ...data,
              StartingPrice: e || 0,
            });
          }}
        />
        <InputNumber
          changeOnWheel
          min={0}
          addonBefore="每增加"
          addonAfter={unitOrDefault}
          value={data.AddedUnitAmount}
          onChange={(e) => {
            onChange({
              ...data,
              AddedUnitAmount: e || 0,
            });
          }}
        />
        <InputNumber
          changeOnWheel
          min={0}
          addonBefore="增加"
          addonAfter="元"
          value={data.AddedUnitPrice}
          onChange={(e) => {
            onChange({
              ...data,
              AddedUnitPrice: e || 0,
            });
          }}
        />
      </Space>
    </>
  );
};
