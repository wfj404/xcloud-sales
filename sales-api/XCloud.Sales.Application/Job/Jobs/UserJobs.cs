using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Job.Jobs;

[UnitOfWork]
public class UserJobs : SalesAppService
{
    private readonly IStoreUserService _userService;

    public UserJobs(IStoreUserService userService)
    {
        _userService = userService;
    }
}