using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Service.Mapping;

namespace XCloud.Sales.Application.Mapping.Mapping;

public class CommonEntityNotMappingMap : SalesEntityTypeConfiguration<CommonEntityNotMapping>
{
    public override void Configure(EntityTypeBuilder<CommonEntityNotMapping> builder)
    {
        builder.ToTable("sales_common_entity_not_mapping");

        builder.PreConfigIntPrimaryKey();
        builder.PreConfigEntityMappingEntity();

        base.Configure(builder);
    }
}