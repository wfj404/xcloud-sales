import { BlockItem, BlockItemProps } from '../../utils';

export interface MarkdownBlockProps extends BlockItemProps {
  markdownText?: string;
}

export const markdown_demo: BlockItem<MarkdownBlockProps> = {
  type: 'markdown',
  style: {
    marginBottom: 10,
  },
  body: {
    markdownText: `
      # markdown block
      > put content here
      `,
  },
};
