import { SkuDto } from '@/utils/swagger';
import { Typography } from '@mui/material';
import Box from '@mui/material/Box';
import XPrice from './price';

export default ({ model }: { model: SkuDto }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
      }}
      component='div'
    >
      <Box
        sx={{
          width: '100%',
        }}
      >
        <Typography color='text.secondary' variant='caption'>
          {model.Name || '--'}
        </Typography>
      </Box>
      <Box
        sx={{
          display: 'inline-block',
          ml: 1,
        }}
      >
        <XPrice model={model} />
      </Box>
    </Box>
  );
};
