using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Domain.Grades;
using XCloud.Sales.Application.Domain.ShoppingCart;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Users;

public interface IStoreUserService : ISalesAppService
{
    Task<StoreUserDto> GetByIdAsync(int id);

    Task<StoreUserDto[]> GetByIdsAsync(int[] ids);

    Task<StoreUserDto[]> AttachPlatformUsersAsync(StoreUserDto[] data);

    Task<PagedResponse<StoreUserDto>> QueryStoreUserPagingAsync(QueryUserPagingInput dto);

    Task TrySetLastActivityTimeAsync(int userId);
}

public class StoreUserService : SalesAppService, IStoreUserService
{
    private readonly ISalesRepository<StoreUser> _userRepository;
    private readonly IUserService _userService;
    private readonly IUserMobileService _userMobileService;

    public StoreUserService(ISalesRepository<StoreUser> userRepository,
        IUserService userService, IUserMobileService userMobileService)
    {
        _userRepository = userRepository;
        _userService = userService;
        _userMobileService = userMobileService;
    }

    public async Task<StoreUserDto> GetByIdAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentNullException(nameof(id));

        var entity = await this._userRepository.FirstOrDefaultAsync(x => x.Id == id);

        return entity?.MapTo<StoreUser, StoreUserDto>(this.ObjectMapper);
    }

    public async Task<StoreUserDto[]> GetByIdsAsync(int[] ids)
    {
        if (ValidateHelper.IsEmptyCollection(ids))
            return Array.Empty<StoreUserDto>();

        var datalist = await this._userRepository.GetListAsync(x => ids.Contains(x.Id));

        return datalist.MapArrayTo<StoreUser, StoreUserDto>(this.ObjectMapper);
    }

    public async Task<StoreUserDto[]> AttachPlatformUsersAsync(StoreUserDto[] data)
    {
        if (data == null)
            throw new ArgumentNullException(nameof(data));

        var userIds = data.Select(x => x.UserId).WhereNotEmpty().Distinct().ToArray();

        if (userIds.Any())
        {
            var sysUsers = await _userService.GetByIdsAsync(userIds);
            await _userMobileService.AttachAccountMobileAsync(sysUsers);
            foreach (var m in data)
            {
                m.User = sysUsers.FirstOrDefault(x => x.Id == m.UserId);
            }
        }

        return data;
    }

    public async Task TrySetLastActivityTimeAsync(int userId)
    {
        if (userId <= 0)
        {
            return;
        }

        var key = $"try-set-last-activity-time:{userId}";

        if (!await this.CacheProvider.TrySetEmptyStringAsync(key, TimeSpan.FromMinutes(1)))
        {
            return;
        }

        var user = await _userRepository.FirstOrDefaultAsync(x => x.Id == userId);
        if (user == null)
        {
            var e = new EntityNotFoundException(nameof(userId));
            this.Logger.LogError(message: nameof(TrySetLastActivityTimeAsync), exception: e);
            return;
        }

        user.LastActivityTime = Clock.Now;

        await _userRepository.UpdateAsync(user);
    }

    public virtual async Task<PagedResponse<StoreUserDto>> QueryStoreUserPagingAsync(QueryUserPagingInput dto)
    {
        var db = await _userRepository.GetDbContextAsync();

        var mappingQuery = from mapping in db.Set<StoreUserGradeMapping>().AsNoTracking()
            join grade in db.Set<Grade>().AsNoTracking()
                on mapping.GradeId equals grade.Id
            select new { mapping, grade };

        var now = Clock.Now;
        mappingQuery = mappingQuery.Where(x => x.mapping.StartTime == null || x.mapping.StartTime <= now);
        mappingQuery = mappingQuery.Where(x => x.mapping.EndTime == null || x.mapping.EndTime >= now);

        var query = db.Set<StoreUser>().IgnoreQueryFilters().AsNoTracking();

        if (dto.StartTime.HasValue)
            query = query.Where(c => dto.StartTime.Value <= c.CreationTime);

        if (dto.EndTime.HasValue)
            query = query.Where(c => dto.EndTime.Value >= c.CreationTime);

        if (dto.Active.HasValue)
            query = query.Where(c => c.IsActive == dto.Active.Value);

        if (!string.IsNullOrWhiteSpace(dto.GradeId))
        {
            var userIds = mappingQuery.Where(x => x.grade.Id == dto.GradeId).Select(x => x.mapping.UserId);
            query = query.Where(x => userIds.Contains(x.Id));
        }

        if (ValidateHelper.IsNotEmptyCollection(dto.GlobalUserIds))
            query = query.Where(x => dto.GlobalUserIds.Contains(x.UserId));

        if (dto.OnlyWithShoppingCart)
            query = query.Where(c => db.Set<ShoppingCartItem>().AsNoTracking().Any(x => x.UserId == c.Id));

        var count = await query.CountOrDefaultAsync(dto);

        var items = await query
            .OrderBy(x => x.IsActive)
            .ThenByDescending(x => x.LastActivityTime)
            .ThenByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();

        var storeUsers = items.Select(x => ObjectMapper.Map<StoreUser, StoreUserDto>(x)).ToArray();

        var response = new PagedResponse<StoreUserDto>(storeUsers, dto, count);

        return response;
    }
}