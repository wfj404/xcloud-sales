import { useRequest } from 'ahooks';
import {
  Badge,
  Button,
  Card,
  message,
  Space,
  Table,
  Tag,
  Tooltip,
  Typography,
} from 'antd';
import { ColumnProps } from 'antd/es/table';
import { sumBy } from 'lodash-es';
import React, { useEffect } from 'react';

import RenderTime from '@/components/common/time/render_time';
import { getPaymentChannel } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto, PaymentBillDto } from '@/utils/swagger';
import { formatMoney, handleResponse, isStringEmpty } from '@/utils/utils';

export default ({ model, ok }: { model: OrderDto; ok: () => void }) => {
  const [data, _data] = React.useState<PaymentBillDto[]>([]);
  const [loading, _loading] = React.useState<boolean>(false);
  const [loadingCreate, _loadingCreate] = React.useState<boolean>(false);

  const queryBill = () => {
    if (isStringEmpty(model.Id)) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .paymentBillListOrderBill({ Id: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const createOrderBill = () => {
    if (!confirm('创建支付单？')) {
      return;
    }
    _loadingCreate(true);
    apiClient.mallManager
      .paymentBillCreateOrderBill({ Id: model.Id })
      .then((res) => {
        handleResponse(res, () => {
          message.success('创建成功');
          queryBill();
        });
      })
      .finally(() => _loadingCreate(false));
  };

  const refund_to_balance_request = useRequest(
    apiClient.mallManager.refundBillRefundToBalance,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          message.success('退款成功');
          queryBill();
          ok();
        });
      },
    },
  );

  useEffect(() => {
    queryBill();
  }, [model]);

  const columns: ColumnProps<PaymentBillDto>[] = [
    {
      title: '支付单号',
      width: 200,
      render: (d, x) => {
        const refund_price = sumBy(x.RefundBills || [], (x) => x.Price || 0);

        return (
          <span>
            <Typography.Paragraph copyable>{x.Id}</Typography.Paragraph>
            {refund_price > 0 && (
              <p style={{ color: 'red' }}>退款：{formatMoney(refund_price)}</p>
            )}
          </span>
        );
      },
    },
    {
      title: '金额',
      render: (d, x: PaymentBillDto) => <b>{formatMoney(x.Price || 0)}</b>,
    },
    {
      title: '支付渠道',
      render: (d, x: PaymentBillDto) => getPaymentChannel(x),
    },
    {
      title: '支付状态',
      render: (d, x: PaymentBillDto) => (
        <Badge
          text={x.Paid ? '已支付' : '未支付'}
          status={x.Paid ? 'success' : 'default'}
        />
      ),
    },
    {
      title: '支付时间',
      render: (d, x: PaymentBillDto) => <RenderTime timeStr={x.PayTime} />,
    },
    {
      title: '外部支付号',
      render: (d, x: PaymentBillDto) => (
        <Tooltip title={'第三方支付系统生成的流水号，用于查账'}>
          <Tag>{x.PaymentTransactionId || '--'}</Tag>
        </Tooltip>
      ),
    },
    {
      title: '创建时间',
      render: (d, x: PaymentBillDto) => <RenderTime timeStr={x.CreationTime} />,
    },
    {
      render: (d, x) => {
        const refund_price = sumBy(x.RefundBills || [], (x) => x.Price || 0);
        const refund_available = refund_price < (x.Price || 0);
        return (
          <Space direction="horizontal">
            {refund_available && x.Paid && (
              <Button
                type="text"
                danger
                loading={refund_to_balance_request.loading}
                onClick={() => {
                  if (confirm('确定退款至余额？')) {
                    refund_to_balance_request.run({
                      Id: x.Id,
                    });
                  }
                }}
              >
                退款至余额
              </Button>
            )}
          </Space>
        );
      },
    },
  ];

  return (
    <>
      <Card
        title="支付账单"
        extra={
          <Button
            disabled
            loading={loadingCreate}
            type={'link'}
            onClick={() => {
              createOrderBill();
            }}
          >
            新建账单
          </Button>
        }
        style={{
          marginBottom: 10,
        }}
      >
        <Table
          loading={loading}
          dataSource={data}
          columns={columns}
          rowKey={(x) => x.Id || ''}
          pagination={false}
        />
      </Card>
    </>
  );
};
