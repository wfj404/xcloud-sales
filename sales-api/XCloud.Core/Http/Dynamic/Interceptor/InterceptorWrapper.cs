﻿namespace XCloud.Core.Http.Dynamic.Interceptor;

public class InterceptorWrapper<T> : InterceptorWrapper where T : class, IRequestInterceptor
{
    public Type InterceptorType { get; }

    public InterceptorWrapper(IRequestInterceptor requestInterceptor, Type type) : base(requestInterceptor, type)
    {
        InterceptorType = typeof(T);
    }
}

public class InterceptorWrapper
{
    public int Order { get; }
    public Type TargetType { get; }
    public IRequestInterceptor RequestInterceptor { get; }

    public InterceptorWrapper(IRequestInterceptor requestInterceptor, Type type)
    {
        RequestInterceptor = requestInterceptor ?? throw new ArgumentNullException(nameof(requestInterceptor));
        TargetType = type;
    }

    public bool IsGlobalInterceptor() => TargetType == null;

    public bool IsInterceptorFor<ClientType>() => TargetType == typeof(ClientType);
}