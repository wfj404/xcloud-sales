import { Button, Input, InputNumber, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';

import XSkuSelector from '@/components/goods/sku/sku_selector';
import { OrderItemDto } from '@/utils/swagger';
import { formatMoney, isStringEmpty } from '@/utils/utils';
import { PlusOutlined } from '@ant-design/icons';

import { calculate_total_price } from './utils';

export default ({
  items,
  onChange,
}: {
  items: OrderItemDto[];
  onChange: (e: OrderItemDto[]) => void;
}) => {
  if (!items || !onChange) {
    return <h1>error</h1>;
  }

  const setItemValue = (i: number, e: OrderItemDto) => {
    const datalist: OrderItemDto[] = [...items];
    datalist[i] = e;
    onChange(datalist);
  };

  const columns: ColumnType<OrderItemDto>[] = [
    {
      render: (d, x, i) => <Tag color="blue">{i + 1}</Tag>,
    },
    {
      title: '商品',
      render: (d, x, i) => {
        return (
          <>
            <Space direction="vertical" style={{ width: '100%' }}>
              <XSkuSelector
                selectedSku={x.Sku}
                onChange={(e) => {
                  setItemValue(i, { ...x, Sku: e, SkuId: e?.Id });
                }}
                style={{ width: '100%' }}
              />
              <Input.TextArea
                rows={1}
                placeholder="输入商品备注"
                value={x.Remark || ''}
                onChange={(e) => {
                  setItemValue(i, { ...x, Remark: e.target.value });
                }}
              />
              {isStringEmpty(x.SkuId) && isStringEmpty(x.Remark) && (
                <b style={{ color: 'red' }}>请选择商品或者输入备注</b>
              )}
            </Space>
          </>
        );
      },
    },
    {
      title: '单价',
      render: (d, x, i) => {
        return (
          <>
            <InputNumber
              size="large"
              addonAfter="元"
              min={0}
              status={(x.UnitPrice || 0) <= 0 ? 'error' : undefined}
              value={x.UnitPrice}
              onChange={(e) => {
                setItemValue(i, { ...x, UnitPrice: e || 0 });
              }}
            />
          </>
        );
      },
    },
    {
      title: '数量',
      render: (d, x, i) => {
        return (
          <>
            <InputNumber
              size="large"
              addonAfter="个"
              min={1}
              status={(x.Quantity || 0) <= 0 ? 'error' : undefined}
              value={x.Quantity}
              onChange={(e) => {
                setItemValue(i, { ...x, Quantity: e || 0 });
              }}
            />
          </>
        );
      },
    },
    {
      title: '单品总价',
      render: (d, x, i) => {
        return (
          <Tag color="blue">
            <b>{formatMoney(calculate_total_price([x]))}</b>
          </Tag>
        );
      },
    },
    {
      title: '操作',
      width: 150,
      render: (d, x, i) => {
        return (
          <>
            <Space direction="horizontal">
              <Button
                danger
                type="link"
                onClick={() => {
                  if (confirm('确定删除？')) {
                    onChange(items.filter((_, index) => index !== i));
                  }
                }}
              >
                删除
              </Button>
            </Space>
          </>
        );
      },
    },
  ];

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Table
          title={() => <b>商品列表</b>}
          style={{ border: `1px solid orange` }}
          size="small"
          bordered
          columns={columns}
          dataSource={items}
          pagination={false}
          footer={() => (
            <div>
              <Button
                size="small"
                icon={<PlusOutlined />}
                type="link"
                onClick={() => {
                  onChange([
                    ...items,
                    {
                      Quantity: 1,
                      UnitPrice: 0,
                    },
                  ]);
                }}
              >
                添加商品
              </Button>
            </div>
          )}
        />
      </Space>
    </>
  );
};
