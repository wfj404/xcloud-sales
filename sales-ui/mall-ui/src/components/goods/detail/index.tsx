import { useRequest } from 'ahooks';
import React, { useEffect, useState } from 'react';

import XLoading from '@/components/common/loading/skeleton';
import { apiClient } from '@/utils/client';
import { GoodsDto, SkuDto } from '@/utils/swagger';
import { Box } from '@mui/material';

import XGoodsPublishAlert from '../goods_publish_alert';
import XAddCart from './add_cart';
import XDescription from './description';
import XImages from './images';
import XNotice from './notice';
import XSkuList from './spec';
import XSummary from './summary';

export default function ({ goodsId }: { goodsId?: string }) {
  const get_detail_request = useRequest(apiClient.mall.goodsQueryGoodsDetail, {
    manual: true,
  });
  const model: GoodsDto = get_detail_request.data?.data?.Data || {};

  const [selectedSku, _selectedSku] = useState<SkuDto | undefined>(undefined);

  const renderDetail = () => {
    if (get_detail_request.loading) {
      return <XLoading />;
    }

    if (!model.Published) {
      return <XGoodsPublishAlert model={model} />;
    }

    return (
      <>
        <XImages model={model} skuId={selectedSku?.Id || undefined} />
        <XSummary model={model} />
        <XNotice />
        <XSkuList
          goods={model}
          skuId={selectedSku?.Id || undefined}
          onSkuSelect={(x) => {
            _selectedSku(x);
          }}
        />
        <XAddCart selectedSku={selectedSku} />
        <XDescription model={model} />
      </>
    );
  };

  useEffect(() => {
    if (goodsId) {
      get_detail_request.run({ Id: goodsId });
    } else {
      get_detail_request.mutate();
    }
  }, [goodsId]);

  return (
    <>
      <Box sx={{ margin: 0, pb: 2 }}>{renderDetail()}</Box>
    </>
  );
}
