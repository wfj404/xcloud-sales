using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Sales.Test.Service;

[ExposeServices(typeof(TestUserAccountService))]
public class TestUserAccountService : PlatformAppService
{
    private readonly IUserAccountService _userAccountService;
    private readonly IAdminAccountService _adminAccountService;
    private readonly IUserService _userService;
    
    public TestUserAccountService(IUserAccountService userAccountService,
        IAdminAccountService adminAccountService,
        IUserService userService)
    {
        _userAccountService = userAccountService;
        _adminAccountService = adminAccountService;
        _userService = userService;
    }

    public async Task TestUserAccountAsync()
    {
        var user = await CreateUserAccountAsync();
        var admin = await CreateAdminAccountAsync(user);

        await EnsureRoleAsync(admin);

        await _userAccountService.UpdateStatusAsync(new UpdateUserStatusDto
        {
            Id = user.Id,
            IsActive = false,
            IsDeleted = true
        });

        await _adminAccountService.UpdateStatusAsync(new UpdateAdminStatusDto
        {
            Id = admin.Id,
            IsActive = false,
            IsSuperAdmin = false,
        });
    }

    private async Task EnsureRoleAsync(Admin admin)
    {
        await Task.CompletedTask;
    }

    private async Task<Admin> CreateAdminAccountAsync(User user)
    {
        var admin = await _adminAccountService.GetOrCreateByUserIdAsync(user.Id);

        await _adminAccountService.UpdateStatusAsync(new UpdateAdminStatusDto
        {
            Id = admin.Id,
            IsActive = true,
            IsSuperAdmin = false
        });

        return admin;
    }

    private async Task<User> CreateUserAccountAsync()
    {
        var userName = GuidGenerator.CreateGuidString();
        var userEntity = await _userAccountService.CreateUserAccountV1Async(new IdentityNameDto(userName));

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _userAccountService.SetPasswordAsync(userEntity.Id, "123");

        await _userAccountService.UpdateStatusAsync(new UpdateUserStatusDto
        {
            Id = userEntity.Id,
            IsActive = true,
            IsDeleted = false,
        });

        var user = await _userService.GetByIdAsync(userEntity.Id);
        if (user != null)
        {
            user.NickName = $"test-user-{Clock.Now.Ticks}";
            await _userService.UpdateProfileAsync(user);
        }

        return userEntity;
    }
}