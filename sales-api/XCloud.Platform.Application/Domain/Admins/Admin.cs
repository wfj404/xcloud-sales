﻿namespace XCloud.Platform.Application.Domain.Admins;

public class Admin : PlatformBaseEntity, IHasCreationTime
{
    public string UserId { get; set; }

    public bool IsActive { get; set; }

    public bool IsSuperAdmin { get; set; }

    public DateTime CreationTime { get; set; }
}