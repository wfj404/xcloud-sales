﻿namespace XCloud.Sales.Application.Domain.Stores;

public class StoreRolePermission : SalesBaseEntity<string>, IHasCreationTime
{
    public string PermissionKey { get; set; }

    public string RoleId { get; set; }

    public DateTime CreationTime { get; set; }
}