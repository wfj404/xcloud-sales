﻿using System.Reflection;

namespace XCloud.Core.Http.Dynamic.Definition;

public static class ServiceDefinitionHelper
{
    public static bool IsParameterSame(ParameterInfo a, ParameterInfo b)
    {
        if (a == null || b == null)
        {
            return false;
        }

        if (a.Name != b.Name || a.ParameterType != b.ParameterType || a.Position != b.Position)
        {
            return false;
        }

        return true;
    }

    public static bool IsMethodParameterSame(MethodInfo impl, MethodInfo define)
    {
        var implParameters = impl.GetParameters().OrderBy(x => x.Position).ToArray();
        var defineParameters = define.GetParameters().OrderBy(x => x.Position).ToArray();
        if (implParameters.Length != defineParameters.Length)
        {
            return false;
        }

        for (var i = 0; i < implParameters.Length; ++i)
        {
            var a = implParameters[i];
            var b = defineParameters[i];
            if (!IsParameterSame(a, b))
            {
                return false;
            }
        }

        return true;
    }

    public static bool IsImplementRelation(MethodInfo impl, MethodInfo define)
    {
        return impl.Name == define.Name &&
               impl.IsGenericMethod == define.IsGenericMethod &&
               impl.IsPublic == define.IsPublic &&
               IsMethodParameterSame(impl, define);
    }

    public static Type GetServiceContractInterfaceOrNull(Type type)
    {
        if (type == null)
            throw new ArgumentNullException(nameof(type));

        type.IsNormalPublicClass().Should().BeTrue();

        var contracts = type.GetInterfaces();
        var res = contracts.FirstOrDefault(x => x.GetCustomAttributesList<DynamicHttpClientAttribute>().Any());
        return res;
    }
}