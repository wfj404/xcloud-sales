import { useRequest } from 'ahooks';
import { Spin } from 'antd';
import { ReactNode, useEffect } from 'react';
import { useLocation, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { hasAccessToken } from '@/utils/utils';

export default function({ children }: { children: ReactNode }) {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const query_login_request = useRequest(
    apiClient.mallManager.storeManagerPermissionGetMyPermission,
    {
      manual: true,
      onSuccess(data, params) {
        app.setManagerPermissions(data.data || {});
      },
    },
  );

  useEffect(() => {
    if (hasAccessToken()) {
      query_login_request.run();
    } else {
      app.setStoreManagerAuthResult(undefined);
      app.setManagerPermissions(undefined);
    }
  }, [location.pathname]);

  if (app.managerPermissions == undefined && query_login_request.loading) {
    return (
      <Spin
        spinning={query_login_request.loading}
        size='large'
        fullscreen
        tip={<p>加载权限信息...</p>}
      />
    );
  }

  return <>{children}</>;
}
