import { Button, Space, Tooltip } from 'antd';
import { pullAt } from 'lodash-es';
import { CSSProperties } from 'react';

import { getDeliveryAreaFingerPrint } from '@/utils/biz';
import { DeliveryArea } from '@/utils/swagger';
import { GlobalContainer } from '@/utils/utils';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import { Box } from '@mui/material';

import XAreaSelector from './area_selector';

export default ({
  area_list,
  ok,
  style,
}: {
  area_list: DeliveryArea[];
  ok: (d: DeliveryArea[]) => void;
  style?: CSSProperties;
}) => {
  const area_finger_prints = area_list.map((x) =>
    getDeliveryAreaFingerPrint(x),
  );

  return (
    <>
      <Box
        sx={{
          p: 1,
          '&:hover': {
            border: (t) => `1px dashed ${t.palette.primary.main}`,
          },
        }}
      >
        <Space direction={'vertical'}>
          {area_list?.map((x, i) => {
            return (
              <Space direction={'horizontal'}>
                <XAreaSelector
                  style={style}
                  data={x}
                  onChange={(e) => {
                    const prints = getDeliveryAreaFingerPrint(e);
                    if (area_finger_prints.indexOf(prints) >= 0) {
                      GlobalContainer.message?.error('地区已存在');
                      return;
                    }

                    let areas: DeliveryArea[] = [...area_list];
                    areas[i] = e;
                    ok(areas);
                  }}
                />

                <Tooltip title="删除">
                  <Button
                    size="small"
                    shape={'circle'}
                    icon={<DeleteOutlined />}
                    danger
                    onClick={() => {
                      let areas = [...area_list];
                      pullAt(areas, i);
                      ok(areas);
                    }}
                  ></Button>
                </Tooltip>
              </Space>
            );
          })}
          <Button
            size="small"
            type={'dashed'}
            block
            icon={<PlusOutlined />}
            onClick={() => {
              let areas = [...area_list, {}];
              ok(areas);
            }}
          >
            添加
          </Button>
        </Space>
      </Box>
    </>
  );
};
