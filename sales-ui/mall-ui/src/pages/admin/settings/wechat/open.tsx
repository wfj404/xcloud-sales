import { Form, Input } from 'antd';

import { WechatOpenOption } from '@/utils/swagger';

export default ({
  data,
  onChange,
}: {
  data: WechatOpenOption;
  onChange: (e: WechatOpenOption) => void;
}) => {
  return (
    <>
      <Form labelCol={{ span: 4 }}>
        <Form.Item label="AppId">
          <Input
            value={data.AppId || ''}
            onChange={(e) => {
              onChange({ ...data, AppId: e.target.value });
            }}
          />
        </Form.Item>
        <Form.Item label="AppSecret">
          <Input
            value={data.AppSecret || ''}
            onChange={(e) => {
              onChange({ ...data, AppSecret: e.target.value });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
};
