import { Button, Card, Col, Result, Row } from 'antd';
import { redirectToLogin } from '@/utils/biz';

export default () => (
  <Row
    justify='center'
    style={{
      marginTop: '100px',
    }}
  >
    <Col span={12}>
      <Card title='提示'>
        <Result
          status='warning'
          title='门店账号'
          subTitle='门店账号被禁用'
          extra={
            <Button
              type='primary'
              onClick={() => {
                redirectToLogin();
              }}
            >
              立即登录
            </Button>
          }
        />
      </Card>
    </Col>
  </Row>
);
