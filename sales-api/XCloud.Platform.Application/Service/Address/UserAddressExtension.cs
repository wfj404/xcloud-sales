﻿using JetBrains.Annotations;
using XCloud.Application.Gis;
using XCloud.Platform.Application.Domain.Address;

namespace XCloud.Platform.Application.Service.Address;

public static class UserAddressExtension
{
    public static bool HasGeoLocation(this UserAddress userAddress) => userAddress.GetGeoLocation().IsValid;

    [NotNull]
    public static GeoLocationDto GetGeoLocation(this UserAddress userAddress) => new GeoLocationDto()
    {
        Lat = userAddress.Lat ?? GeoLocationDto.InValidValue,
        Lon = userAddress.Lon ?? GeoLocationDto.InValidValue
    };
}