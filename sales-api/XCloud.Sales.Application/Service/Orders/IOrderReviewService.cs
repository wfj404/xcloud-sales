﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Core.Json;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Exceptions;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderReviewService : ISalesStringCrudAppService<OrderReview, OrderReviewDto, QueryOrderReviewPaging>
{
    Task<OrderReviewDto[]> AttachStoreUserAsync(OrderReviewDto[] data);

    Task<OrderDto[]> AttachOrderReviewAsync(OrderDto[] data);

    Task<OrderReview> EnsureStoreOrderReviewAsync(string reviewId, string storeId);

    Task<OrderReviewDto> GetReviewByOrderIdAsync(string orderId);

    Task ApproveReviewAsync(IdDto dto);

    Task<OrderReviewDto[]> AttachPicturesAsync(OrderReviewDto[] data);

    Task<OrderReviewDto[]> AttachItemsAsync(OrderReviewDto[] data);

    Task UpdateGoodsReviewTotalsAsync(string goodsId);
}

public class OrderReviewService : SalesStringCrudAppService<OrderReview, OrderReviewDto, QueryOrderReviewPaging>,
    IOrderReviewService
{
    private readonly IOrderService _orderService;
    private readonly IStorageMetaService _storageMetaService;

    public OrderReviewService(
        ISalesRepository<OrderReview> goodsReviewRepository,
        IOrderService orderService,
        IStorageMetaService storageMetaService) : base(goodsReviewRepository)
    {
        _orderService = orderService;
        _storageMetaService = storageMetaService;
    }

    public async Task<OrderReview> EnsureStoreOrderReviewAsync(string reviewId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(reviewId))
            throw new ArgumentNullException(nameof(reviewId));

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var db = await Repository.GetDbContextAsync();

        var query = from review in db.Set<OrderReview>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on review.OrderId equals order.Id
            select new { review, order };

        var data = await query.FirstOrDefaultAsync(x => x.review.Id == reviewId);

        if (data == null)
            throw new EntityNotFoundException(nameof(EnsureStoreOrderReviewAsync));

        if (data.order.StoreId != storeId)
            throw new PermissionRequiredException(nameof(EnsureStoreOrderReviewAsync));

        return data.review;
    }

    public override async Task<OrderReview> InsertAsync(OrderReviewDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var order = await _orderService.GetByIdAsync(dto.OrderId);

        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        if (order.OrderStatusId != SalesConstants.OrderStatus.Finished)
            throw new UserFriendlyException("u can't leave a review at current order status");

        var db = await Repository.GetDbContextAsync();

        if (await db.Set<OrderReview>().AnyAsync(x => x.OrderId == order.Id))
            throw new UserFriendlyException("already reviewed");

        var entity = dto.MapTo<OrderReviewDto, OrderReview>(ObjectMapper);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        db.Set<OrderReview>().Add(entity);

        if (dto.Items != null)
        {
            var itemSet = db.Set<OrderReviewItem>();
            foreach (var m in dto.Items)
            {
                var item = m.MapTo<OrderReviewItemDto, OrderReviewItem>(ObjectMapper);
                item.Id = GuidGenerator.CreateGuidString();
                item.ReviewId = entity.Id;
                itemSet.Add(item);
            }
        }

        await db.TrySaveChangesAsync();

        return entity;
    }

    public async Task ApproveReviewAsync(IdDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var entity = await Repository.GetRequiredByIdAsync(dto.Id);

        if (entity.Approved)
        {
            return;
        }

        entity.Approved = true;
        entity.LastModificationTime = Clock.Now;

        await Repository.UpdateAsync(entity);
    }

    public async Task<OrderDto[]> AttachOrderReviewAsync(OrderDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await this.Repository.GetDbContextAsync();

        var query = db.Set<OrderReview>().AsNoTracking();

        var datalist = await query
            .Where(x => ids.Contains(x.OrderId))
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .ToArrayAsync();

        foreach (var m in data)
        {
            var item = datalist
                .OrderByDescending(x => x.CreationTime)
                .FirstOrDefault(x => x.OrderId == m.Id);

            if (item == null)
                continue;

            m.OrderReview = item.MapTo<OrderReview, OrderReviewDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<OrderReviewDto[]> AttachPicturesAsync(OrderReviewDto[] data)
    {
        if (!data.Any())
            return data;

        foreach (var m in data)
        {
            m.MetaIds = this.JsonDataSerializer.DeserializeArrayFromStringOrEmpty<string>(m.PictureMetaIdsJson,
                this.Logger);
        }

        var ids = data.SelectMany(x => x.MetaIds).Distinct().ToArray();

        var datalist = await this._storageMetaService.ByIdsAsync(ids);

        foreach (var m in data)
        {
            m.StorageMeta = datalist.Where(x => m.MetaIds.Contains(x.Id)).ToArray();
        }

        return data;
    }

    public async Task<OrderReviewDto[]> AttachItemsAsync(OrderReviewDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await Repository.GetDbContextAsync();

        var items = db.Set<OrderReviewItem>().Where(x => ids.Contains(x.ReviewId)).ToArray();

        foreach (var m in data)
        {
            m.Items = items.Where(x => x.ReviewId == m.Id)
                .MapArrayTo<OrderReviewItem, OrderReviewItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<OrderReviewDto[]> AttachStoreUserAsync(OrderReviewDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await Repository.GetDbContextAsync();

        var query = from review in db.Set<OrderReview>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on review.OrderId equals order.Id
            join storeUser in db.Set<StoreUser>().AsNoTracking()
                on order.UserId equals storeUser.Id
            select new { review, storeUser };

        query = query.Where(x => ids.Contains(x.review.Id));

        var datalist = await query.OrderByDescending(x => x.review.CreationTime).ToArrayAsync();

        foreach (var m in data)
        {
            var user = datalist.FirstOrDefault(x => x.review.Id == m.Id)?.storeUser;
            if (user == null)
                continue;

            m.StoreUser = user.MapTo<StoreUser, StoreUserDto>(ObjectMapper);
        }

        return data;
    }

    public virtual async Task UpdateGoodsReviewTotalsAsync(string goodsId)
    {
        var db = await Repository.GetDbContextAsync();

        var goods = await db.Set<Goods>().FirstOrDefaultAsync(x => x.Id == goodsId);

        if (goods == null)
            throw new EntityNotFoundException(nameof(UpdateGoodsReviewTotalsAsync));

        var query = from reviewItem in db.Set<OrderReviewItem>().AsNoTracking()
            join orderItem in db.Set<OrderItem>().AsNoTracking()
                on reviewItem.OrderItemId equals orderItem.Id
            join review in db.Set<OrderReview>().AsNoTracking()
                on reviewItem.ReviewId equals review.Id
            join sku in db.Set<Sku>().AsNoTracking()
                on orderItem.SkuId equals sku.Id
            select new { reviewItem, review, sku };

        query = query.Where(x => x.review.Approved);
        query = query.Where(x => x.sku.GoodsId == goods.Id);

        goods.ApprovedTotalReviews = await query.CountAsync();
        if (goods.ApprovedTotalReviews > 0)
        {
            var totalRate = await query.SumAsync(x => x.reviewItem.Rating);
            goods.ApprovedReviewsRates = totalRate / goods.ApprovedTotalReviews;
        }
        else
        {
            goods.ApprovedReviewsRates = default;
        }

        goods.LastModificationTime = Clock.Now;

        await db.TrySaveChangesAsync();
    }

    public async Task<OrderReviewDto> GetReviewByOrderIdAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var db = await this.Repository.GetDbContextAsync();

        var query = db.Set<OrderReview>().AsNoTracking();

        var entity = await query
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .FirstOrDefaultAsync(x => x.OrderId == orderId);

        if (entity == null)
            return null;

        return entity.MapTo<OrderReview, OrderReviewDto>(ObjectMapper);
    }

    public override async Task<PagedResponse<OrderReviewDto>> QueryPagingAsync(QueryOrderReviewPaging dto)
    {
        var db = await Repository.GetDbContextAsync();

        var reviewQuery = db.Set<OrderReview>().AsNoTracking();

        reviewQuery.WhereCreationTimeBetween(dto.StartTime, dto.EndTime);

        if (dto.IsApproved != null)
            reviewQuery = reviewQuery.Where(x => x.Approved == dto.IsApproved.Value);

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
        {
            var query = db.Set<Order>().AsNoTracking().Where(x => x.StoreId == dto.StoreId);

            reviewQuery = reviewQuery.Where(x => query.Any(d => d.Id == x.OrderId));
        }

        if (!string.IsNullOrWhiteSpace(dto.Keywords))
        {
            var query = from reviewItem in db.Set<OrderReviewItem>().AsNoTracking()
                join review in db.Set<OrderReview>()
                    on reviewItem.ReviewId equals review.Id
                select new { reviewItem, review };

            query = query.Where(x =>
                x.reviewItem.ReviewText.Contains(dto.Keywords) || x.review.ReviewText.Contains(dto.Keywords));

            reviewQuery = reviewQuery.Where(x => query.Any(d => d.review.Id == x.Id));
        }

        var total = await reviewQuery.CountOrDefaultAsync(dto);

        reviewQuery = reviewQuery.OrderByDescending(c => c.CreationTime).ThenByDescending(x => x.Id);

        var items = await reviewQuery.PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();

        var datalist = items.MapArrayTo<OrderReview, OrderReviewDto>(ObjectMapper);

        return new PagedResponse<OrderReviewDto>(datalist, dto, total);
    }
}