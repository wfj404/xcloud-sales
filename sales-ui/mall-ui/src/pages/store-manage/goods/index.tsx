import { useOutlet } from 'umi';

import ValidatePermission from '@/components/login/manager/validate_permission';
import { ManagerPermissionKeys } from '@/utils/biz';

export default () => {
  const children = useOutlet();

  return (
    <>
      <ValidatePermission permissionKey={ManagerPermissionKeys.Sku}>
        {children}
      </ValidatePermission>
    </>
  );
};
