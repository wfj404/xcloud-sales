﻿using AutoMapper;
using Volo.Abp.Http;
using XCloud.Core.Dto;

namespace XCloud.AspNetMvc.Mapper;

public class AspNetMvcAutoMapperConfiguration : Profile
{
    public AspNetMvcAutoMapperConfiguration()
    {
        this.CreateMap<RemoteServiceErrorInfo, ErrorInfo>().ReverseMap();
    }
}