import { Alert, Input, Space, Tag } from 'antd';
import { sumBy } from 'lodash-es';

import { OrderGroupDto } from '@/utils/swagger';
import { formatMoney } from '@/utils/utils';

import { calculate_total_price } from './order_item/utils';

export default ({
  group,
  onChange,
}: {
  group: OrderGroupDto;
  onChange: (e: OrderGroupDto) => void;
}) => {
  const orders = group?.GroupData?.Orders || [];

  const all_items = orders.flatMap((x) => x.Items || []);
  const goods_sum = sumBy(all_items, (x) => x.Quantity || 0);
  const all_offset = sumBy(orders, (x) => x.SellerPriceOffset || 0);
  const total_price = calculate_total_price(all_items) + all_offset;

  const all_bills = orders.flatMap((x) => x.PaymentBills || []);

  const paid_price = sumBy(all_bills, (x) => x.Price || 0);

  return (
    <>
      <Alert
        message={
          <>
            <Space direction="vertical" style={{ width: '100%' }}>
              <div>
                <Space direction="horizontal">
                  <span>批次号</span>
                  <Tag>{group.Id || '--'}</Tag>
                </Space>
              </div>
              <Input
                addonBefore={'名称'}
                value={group.Name || ''}
                onChange={(e) => {
                  onChange({ ...group, Name: e.target.value });
                }}
              />
              <Input.TextArea
                rows={3}
                value={group.Description || ''}
                placeholder="输入备注信息"
                onChange={(e) => {
                  onChange({ ...group, Description: e.target.value });
                }}
              />
            </Space>
          </>
        }
        description={
          <>
            <div>
              {orders.length}个订单，{goods_sum}个商品
            </div>
            <div>总计{formatMoney(total_price)}</div>
            <div>收款{formatMoney(paid_price)}</div>
            <div>剩余应收{formatMoney(total_price - paid_price)}</div>
          </>
        }
      />
    </>
  );
};
