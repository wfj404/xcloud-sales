﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Service;

namespace XCloud.Application.Data;

[ExposeServices(typeof(DataSeederExecutor))]
public class DataSeederExecutor : XCloudAppService
{
    private readonly IDataSeeder _dataSeeder;

    public DataSeederExecutor(IDataSeeder dataSeeder)
    {
        _dataSeeder = dataSeeder;
    }

    public async Task Execute()
    {
        try
        {
            Logger.LogInformation("开始执行数据库初始化工作");
            await _dataSeeder.SeedAsync(new DataSeedContext { });
            Logger.LogInformation("初始化数据库完成！");
        }
        catch (Exception e)
        {
            Logger.LogError(message: e.Message, exception: e);
        }
    }
}