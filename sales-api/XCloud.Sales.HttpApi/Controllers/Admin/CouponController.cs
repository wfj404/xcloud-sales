using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Coupons;
using XCloud.Sales.Application.Service.Coupons;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/coupon")]
public class CouponController : ShopBaseController
{
    private readonly ICouponService _couponService;
    private readonly IUserCouponService _userCouponService;
    private readonly IStoreMappingService _storeMappingService;

    public CouponController(ICouponService couponService, IUserCouponService userCouponService,
        IStoreMappingService storeMappingService)
    {
        _couponService = couponService;
        _userCouponService = userCouponService;
        _storeMappingService = storeMappingService;
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<CouponDto>> QueryCouponPagingAsync([FromBody] QueryCouponPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageMarketing);

        var response = await _couponService.QueryPagingAsync(dto);

        await _couponService.AttachStoresAsync(response.Items.ToArray());
        await _couponService.AttachStatisticsAsync(response.Items.ToArray());

        return response;
    }

    [HttpPost("create-user-coupon")]
    public async Task<ApiResponse<object>> CreateUserCouponAsync([FromBody] CouponUserMappingDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageMarketing);

        await _userCouponService.CreateUserCouponAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("user-coupon-paging")]
    public async Task<PagedResponse<CouponUserMappingDto>> QueryUserCouponPagingAsync(
        [FromBody] QueryUserCouponPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageMarketing);

        dto.IsDeleted = null;

        var response = await _userCouponService.QueryUserCouponPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _userCouponService.AttachUserCouponStoreUserAsync(response.Items.ToArray());
        }

        return response;
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> SaveCouponAsync([FromBody] CouponDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageMarketing);

        var entity = default(Coupon);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            entity = await _couponService.InsertAsync(dto);
        }
        else
        {
            throw new NotSupportedException("update is not supported");
        }

        if (dto.Stores != null)
        {
            await _storeMappingService.SaveStoreMappingAsync(entity, dto.Stores.Ids());
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageMarketing);

        await _couponService.DeleteByIdAsync(dto);

        return new ApiResponse<object>();
    }
}