import { ReactNode } from 'react';

import { IHasCreationTime, IHasLastModifiedTime } from '@/utils/biz';

import XRenderTime from './render_time';

export default ({
  model,
  fallback,
}: {
  model?: IHasCreationTime | IHasLastModifiedTime;
  fallback?: ReactNode | any;
}) => {
  if (model == null || model == undefined) {
    return null;
  }

  const creationTime = (model as IHasCreationTime)?.CreationTime;

  const updateTime = (model as IHasLastModifiedTime)?.LastModificationTime;

  return (
    <>
      <div style={{}}>
        <div>
          <XRenderTime
            prefix={<span>创建：</span>}
            onlyDate
            timeStr={creationTime}
            fallback={fallback}
          />
        </div>
        <div>
          <XRenderTime
            prefix={<span>修改：</span>}
            onlyDate
            timeStr={updateTime}
            fallback={fallback}
          />
        </div>
      </div>
    </>
  );
};
