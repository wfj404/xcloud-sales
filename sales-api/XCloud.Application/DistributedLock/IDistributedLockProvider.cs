﻿using System;
using System.Threading.Tasks;

namespace XCloud.Application.DistributedLock;

public interface IDistributedLock : IAsyncDisposable
{
    bool IsAcquired { get; }
}

public interface IDistributedLockProvider
{
    Task<IDistributedLock> CreateLockAsync(string resource, TimeSpan? expiryTime);
}