﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCloud.Core.Dto;
using XCloud.Core.Extension;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Service;
using XCloud.Sales.Qingdao.Service.Stores;

namespace XCloud.Sales.Qingdao.Controllers.User;

[Route("api/mall/qingdao/store")]
public class StoreController : ShopBaseController
{
    private readonly Sh3HClient _sh3HClient;
    private readonly IQingdaoStoreService _qingdaoStoreService;
    private readonly QingdaoLocationBasedStoreSelectorContributor _qingdaoLocationBasedStoreSelectorContributor;

    public StoreController(Sh3HClient sh3HClient, IQingdaoStoreService qingdaoStoreService,
        QingdaoLocationBasedStoreSelectorContributor qingdaoLocationBasedStoreSelectorContributor)
    {
        _sh3HClient = sh3HClient;
        _qingdaoStoreService = qingdaoStoreService;
        _qingdaoLocationBasedStoreSelectorContributor = qingdaoLocationBasedStoreSelectorContributor;
    }

    [HttpPost("stores-list")]
    public async Task<ApiResponse<StoreDto[]>> StoreListAsync()
    {
        var locationBasedStoreIdOrEmpty =
            await this._qingdaoLocationBasedStoreSelectorContributor.GetStoreIdOrEmptyAsync();

        var storeList = new List<StoreDto>();

        var config = this.Configuration.GetRequired3HConfig();

        if (ValidateHelper.IsNotEmptyCollection(config.StoreOrgCodeList))
        {
            foreach (var code in config.StoreOrgCodeList)
            {
                var org = await this._sh3HClient.GetOrganizationByCodeAsync(code);
                if (org == null)
                    continue;

                var store = await this._qingdaoStoreService.PrepareStoreAsync(org);
                store.InServiceArea = default;
                store.Distance = default;
                if (!string.IsNullOrWhiteSpace(locationBasedStoreIdOrEmpty))
                {
                    store.InServiceArea = locationBasedStoreIdOrEmpty == store.Id;
                }

                storeList.Add(store);
            }
        }

        storeList = storeList
            .OrderByDescending(x => (x.InServiceArea ?? false).ToBoolInt())
            .ThenBy(x => x.Distance ?? 0d)
            .ThenByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .ToList();

        return new ApiResponse<StoreDto[]>(storeList.ToArray());
    }
}