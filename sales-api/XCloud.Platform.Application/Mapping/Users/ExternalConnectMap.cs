﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Users;

namespace XCloud.Platform.Application.Mapping.Users;

public class ExternalConnectMap : PlatformEntityTypeConfiguration<ExternalConnect>
{
    public override void Configure(EntityTypeBuilder<ExternalConnect> builder)
    {
        builder.ToTable("sys_external_connect");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        
        builder.Property(x => x.UserId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Platform).IsRequired().HasMaxLength(100);
        builder.Property(x => x.AppId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.OpenId).IsRequired().HasMaxLength(100);

        base.Configure(builder);
    }
}