using XCloud.Core.Dto;
using XCloud.Platform.Application.Exceptions;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Delivery.Express;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/freight-template")]
public class FreightTemplateController : ShopBaseController
{
    private readonly IFreightTemplateService _freightTemplateService;
    private readonly IFreightTemplateGoodsMappingService _freightTemplateGoodsMappingService;

    public FreightTemplateController(IFreightTemplateService freightTemplateService,
        IFreightTemplateGoodsMappingService freightTemplateGoodsMappingService)
    {
        _freightTemplateService = freightTemplateService;
        _freightTemplateGoodsMappingService = freightTemplateGoodsMappingService;
    }

    [HttpPost("save-goods-mapping")]
    public async Task<ApiResponse<object>> SaveGoodsExpressTemplateAsync(
        [FromBody] SaveGoodsStoreExpressTemplateInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        dto.StoreId = manager.StoreId;

        await _freightTemplateGoodsMappingService.SaveGoodsExpressTemplateAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("list")]
    public async Task<ApiResponse<FreightTemplateDto[]>> QueryAllAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var data = await _freightTemplateService.QueryAllAsync(manager.StoreId);

        return new ApiResponse<FreightTemplateDto[]>(data);
    }

    [HttpPost("save")]
    public async Task<ApiResponse<FreightTemplate>> SaveAsync([FromBody] FreightTemplateDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var entity = default(FreightTemplate);

        if (string.IsNullOrWhiteSpace(dto.Id))
        {
            dto.StoreId = manager.StoreId;
            entity = await _freightTemplateService.InsertAsync(dto);
        }
        else
        {
            entity = await _freightTemplateService.UpdateAsync(dto);
        }

        return new ApiResponse<FreightTemplate>(entity);
    }

    [HttpPost("delete-by-id")]
    public async Task<ApiResponse<object>> DeleteByIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var template = await _freightTemplateService.GetByIdAsync(dto.Id);

        if (template == null)
            throw new EntityNotFoundException(nameof(template));

        if (template.StoreId != manager.StoreId)
            throw new PermissionRequiredException();

        await _freightTemplateService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }
}