﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;
using XCloud.Application.Core;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application;
using XCloud.Platform.Application.Domain;
using XCloud.Platform.Messenger;
using XCloud.Platform.WeChat;
using XCloud.Sales.Application;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Domain;
using XCloud.Sales.Warehouses;

namespace XCloud.Sales.Qingdao.Database.EntityFrameworkCore;

[ConnectionStringName(SalesConstants.DbConnectionNames.SqlServer)]
public class ShopSqlServerDbContext : AbpDbContext<ShopSqlServerDbContext>
{
    public ShopSqlServerDbContext(DbContextOptions<ShopSqlServerDbContext> options) : base(options)
    {
        //
    }

    [MethodMetric]
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ConfigEntityMapperFromAssemblies<ISalesBaseEntity>(new[]
        {
            typeof(SalesApplicationModule).Assembly,
            typeof(SalesWarehouseApplicationModule).Assembly,
            typeof(QingdaoSalesApplicationModule).Assembly
        });
        modelBuilder.ConfigEntityMapperFromAssemblies<IPlatformBaseEntity>(new[]
        {
            typeof(PlatformApplicationModule).Assembly,
            typeof(PlatformMessengerModule).Assembly,
            typeof(PlatformWechatModule).Assembly
        });
    }
}