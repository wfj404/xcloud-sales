import DotsLoading from '@/components/common/loading/dots';
import { apiClient } from '@/utils/client';
import { errorMsg, isStringEmpty, setAccessToken, successMsg } from '@/utils/utils';
import { Box, Container, Typography } from '@mui/material';
import qs from 'query-string';
import { useEffect, useState } from 'react';
import { history, useLocation } from 'umi';

export default () => {
  const [loading, _loading] = useState(false);
  const location = useLocation();

  const loginAction = (code: string) => {
    _loading(true);

    const usewechatprofileKey = 'usewechatprofile';
    const updateprofile = localStorage.getItem(usewechatprofileKey) == 'true';
    localStorage.removeItem(usewechatprofileKey);

    apiClient.platform
      .wechatMpAuthWxMpOAuthCodeLogin({
        Code: code,
        UseWechatProfile: updateprofile,
      })
      .then((res) => {
        if (res.data.Error) {
          errorMsg(res.data.Error.Message || '操作未能如期完成');
          return false;
        } else {
          const { AccessToken } = res.data.Data || {};
          if (isStringEmpty(AccessToken)) {
            errorMsg('无法获取token');
            return;
          } else {
            setAccessToken(AccessToken!);
            successMsg('登录成功，即将跳转');
            let next = localStorage.getItem('login_next') || '/';
            setTimeout(() => {
              window.location.href = next;
            }, 500);
          }
          return true;
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    console.log(location);

    const code = qs.parse(location.search)?.code as string;

    if (isStringEmpty(code)) {
      history.push({
        pathname: '/',
      });
    } else {
      loginAction(code);
    }
  }, []);

  return (
    <>
      <Container maxWidth='sm' disableGutters>
        <Box sx={{}}>
          {loading && <DotsLoading />}
          <Box
            sx={{
              py: 4,
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Typography variant='h5'>登录中...</Typography>
          </Box>
        </Box>
      </Container>
    </>
  );
};
