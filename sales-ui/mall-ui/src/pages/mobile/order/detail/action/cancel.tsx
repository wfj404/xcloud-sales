import React, { useState } from 'react';

import { OrderStatus, PaymentStatus, ShippingStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { errorMsg, handleResponse, isStringEmpty } from '@/utils/utils';
import { Close } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Button, TextField } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function XCancelButton(props: {
  model: OrderDto;
  ok: () => void;
}) {
  const { model, ok } = props;

  const [show, _show] = useState(false);
  const [loadingSave, _loadingSave] = React.useState(false);

  const [comment, _comment] = React.useState('');

  const save = () => {
    if (isStringEmpty(comment)) {
      errorMsg('请输入取消理由');
      return;
    }

    _loadingSave(true);
    apiClient.mall
      .orderCancelOrder({
        OrderId: model.Id,
        Comment: comment,
      })
      .then((res) => {
        handleResponse(res, () => {
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  if (
    model.OrderStatusId == OrderStatus.Pending &&
    model.ShippingStatusId == ShippingStatus.NotShipping &&
    [PaymentStatus.Pending].indexOf(model.PaymentStatusId || '') >= 0
  ) {
    //
  } else {
    return null;
  }

  return (
    <>
      <Button
        startIcon={<Close />}
        size={'large'}
        color="error"
        variant={'outlined'}
        onClick={() => {
          _show(true);
        }}
      >
        取消订单
      </Button>
      <Dialog
        open={show}
        onClose={() => {
          _show(false);
        }}
        fullWidth
      >
        <DialogTitle>取消订单</DialogTitle>
        <DialogContent>
          <DialogContentText>确认取消订单吗？</DialogContentText>
          <TextField
            autoFocus
            label="取消理由"
            multiline
            fullWidth
            value={comment}
            onChange={(e) => _comment(e.target.value)}
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              _show(false);
            }}
          >
            取消
          </Button>
          <LoadingButton
            onClick={() => {
              save();
            }}
            loading={loadingSave}
          >
            确认取消
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}
