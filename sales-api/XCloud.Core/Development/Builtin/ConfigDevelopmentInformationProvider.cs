﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Volo.Abp.DependencyInjection;

namespace XCloud.Core.Development.Builtin;

[ExposeServices(typeof(IDevelopmentInformationProvider))]
public class ConfigDevelopmentInformationProvider : IDevelopmentInformationProvider, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public ConfigDevelopmentInformationProvider(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public string ProviderName => "配置";

    public async Task<object> GetInformationAsync()
    {
        await Task.CompletedTask;

        var config = _serviceProvider.GetRequiredService<IConfiguration>();
        var dict = config.ConfigAsKv();
        var pairs = dict
            .OrderBy(x => x.Key)
            .Select(x => $"{x.Key}={x.Value}")
            .ToArray();

        return pairs;
    }
}