﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Security;

namespace XCloud.Platform.Application.Mapping.Security;

public class RolePermissionMap : PlatformEntityTypeConfiguration<RolePermission>
{
    public override void Configure(EntityTypeBuilder<RolePermission> builder)
    {
        builder.ToTable("sys_role_permission");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.RoleId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.PermissionKey).IsRequired().HasMaxLength(100);

        base.Configure(builder);
    }
}