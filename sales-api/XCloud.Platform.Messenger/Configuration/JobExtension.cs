﻿using System;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Platform.Messenger.Job;

namespace XCloud.Platform.Messenger.Configuration;

public static class JobExtension
{
    internal static void StartImWorker(this IServiceProvider provider)
    {
        using var s = provider.CreateScope();

        var manager = s.ServiceProvider.GetRequiredService<IRecurringJobManager>();

        manager.AddOrUpdate<CleanExpiredRegistrationJob>("clean-expired-reg", x => x.ExecuteAsync(), Cron.Minutely());
    }
}