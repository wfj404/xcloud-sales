import 'dayjs/locale/zh-cn';

import dayjs, { Dayjs } from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isToday from 'dayjs/plugin/isToday';
import isTomorrow from 'dayjs/plugin/isTomorrow';
import isYesterday from 'dayjs/plugin/isYesterday';
import localeData from 'dayjs/plugin/localeData';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import _relativeTime from 'dayjs/plugin/relativeTime';
import utc from 'dayjs/plugin/utc';

import { isStringEmpty } from './utils';

dayjs.extend(localeData);
dayjs.extend(_relativeTime);
dayjs.extend(localizedFormat);
dayjs.extend(customParseFormat);
dayjs.extend(isToday);
dayjs.extend(isYesterday);
dayjs.extend(isTomorrow);
dayjs.extend(utc);
dayjs.locale('zh-cn');

export const myDayjs = dayjs;
export const timezoneOffset = 8;
export const hourMinuteFormat = 'HH:mm';
export const dateFormat = 'YYYY-MM-DD';
export const monthDayFormat = 'MM-DD';
export const dateTimeFormat = 'YYYY-MM-DD HH:mm';
export const dateTimeFormatWithSecond = 'YYYY-MM-DD HH:mm:ss';
export const weekFormat = 'dddd';
export const dateTimeUserFriendlyFormat = 'MMMM DD A h:mm';

export const formatRelativeTimeFromNow = (
  dateStr?: string | null,
): string | null | undefined => {
  const djs = parseAsDayjs(dateStr)?.add(timezoneOffset, 'hour');
  if (!djs) {
    return null;
  }

  const now = dayjs.utc().add(timezoneOffset, 'hour');
  const diff = djs.diff(now, 'day');

  if (Math.abs(diff) > 7) {
    return djs.format(dateTimeFormat);
  }

  return djs.fromNow();
};

export const parseAsDayjs = (time: string | null | undefined): Dayjs | null => {
  if (isStringEmpty(time)) {
    return null;
  }
  try {
    return dayjs(time);
  } catch (e) {
    return null;
  }
};

export const isPastDateTime = (
  time_str: string | undefined | null,
): boolean | undefined => {
  const past = parseAsDayjs(time_str)?.isBefore(myDayjs().utc());
  return past;
};
