import {
  BarsOutlined,
  CodeOutlined,
  ContainerOutlined,
  FlagOutlined,
  FolderOpenOutlined,
  FolderViewOutlined,
  SettingOutlined,
  ShopFilled,
  ShopOutlined,
  ShoppingCartOutlined,
  TagOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { MenuDataItem } from '@ant-design/pro-layout';

const routes: Array<MenuDataItem> = [
  {
    name: '商品',
    path: '/admin/catalog/goods',
    key: 'catalog',
    icon: <ShoppingCartOutlined />,
    children: [
      {
        name: '商品管理',
        path: '/admin/catalog/goods',
        icon: <FolderOpenOutlined />,
      },
      {
        name: '规格明细',
        path: '/admin/catalog/sku',
        icon: <FolderViewOutlined />,
      },
      {
        name: '品牌管理',
        path: '/admin/catalog/brand',
        icon: <FlagOutlined />,
      },
      {
        name: '分类管理',
        path: '/admin/catalog/category',
        icon: <BarsOutlined />,
      },
      {
        name: '标签管理',
        path: '/admin/catalog/tag',
        icon: <TagOutlined />,
      },
    ],
  },
  {
    name: '商城',
    path: '/admin/settings/store_user',
    key: 'mall-settings',
    icon: <ShopOutlined />,
    children: [
      {
        name: '商城会员',
        path: '/admin/settings/store_user',
        icon: <UserOutlined />,
      },
      {
        name: '二级单位',
        path: '/admin/settings/store',
        icon: <ShopFilled />,
      },
      {
        name: '商城设置',
        path: '/admin/settings/common',
        icon: <SettingOutlined />,
      },
    ],
  },
  {
    name: '设置',
    path: '/admin/settings/log',
    key: 'settings',
    icon: <SettingOutlined />,
    children: [
      {
        name: '环境变量',
        path: '/admin/settings/env',
        icon: <CodeOutlined />,
      },
      {
        name: '活动日志',
        path: '/admin/settings/log',
        icon: <ContainerOutlined />,
      },
    ],
  },
];

export default routes;
