using XCloud.Application.Mapper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Domain.Common;
using XCloud.Platform.Application.Service.Common;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Search;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Catalog;

public interface IGoodsService : ISalesStringCrudAppService<Goods, GoodsDto, QueryGoodsPaging>
{
    Task<GoodsDto[]> ListByTagNameAsync(ListByTagNameInput dto);

    Task<GoodsDto[]> AttachTagsAsync(GoodsDto[] data);

    Task<GoodsDto[]> AttachSkusAsync(GoodsDto[] data);

    Task<GoodsDto[]> AttachBrandsAsync(GoodsDto[] data);

    Task<GoodsDto[]> AttachCategoriesAsync(GoodsDto[] data);

    Task<GoodsDto[]> AttachAreasAsync(GoodsDto[] data);

    Task SetTagsAsync(SetGoodsTagsInput dto);

    Task<GoodsDto[]> QueryGoodsForSelectionAsync(QueryGoodsForSelection dto);
}

public class GoodsService : SalesStringCrudAppService<Goods, GoodsDto, QueryGoodsPaging>, IGoodsService
{
    private readonly GoodsUtils _goodsUtils;
    private readonly SalesUtils _salesUtils;

    public GoodsService(ISalesRepository<Goods> goodsRepository, GoodsUtils goodsUtils, SalesUtils salesUtils) : base(
        goodsRepository)
    {
        _goodsUtils = goodsUtils;
        _salesUtils = salesUtils;
    }

    public async Task<GoodsDto[]> QueryGoodsForSelectionAsync(QueryGoodsForSelection dto)
    {
        var db = await Repository.GetDbContextAsync();

        var query = db.Set<Goods>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(dto.Keyword))
            query = query.Where(x => x.Name.Contains(dto.Keyword) || x.SeoName.Contains(dto.Keyword));

        var data = await query
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .Take(dto.PageSize).ToArrayAsync();

        return data.MapArrayTo<Goods, GoodsDto>(ObjectMapper);
    }

    public async Task<GoodsDto[]> ListByTagNameAsync(ListByTagNameInput dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        dto.Count ??= 10;

        if (dto.Count.Value > 100)
            throw new ArgumentException(nameof(dto.Count));

        var db = await this.Repository.GetDbContextAsync();

        var mappingQuery = from mapping in db.Set<TagGoodsMapping>().AsNoTracking()
            join tag in db.Set<Tag>().AsNoTracking()
                on mapping.TagId equals tag.Id
            select new { mapping, tag };

        mappingQuery = mappingQuery.Where(x => x.tag.Name == dto.Name);

        var query = db.Set<Goods>().AsNoTracking().Where(x => mappingQuery.Any(d => d.mapping.GoodsId == x.Id));

        var data = await query
            .OrderByDescending(x => x.LastModificationTime)
            .ThenByDescending(x => x.Id)
            .Take(dto.Count.Value).ToArrayAsync();

        return data.MapArrayTo<Goods, GoodsDto>(ObjectMapper);
    }

    public override async Task DeleteByIdAsync(string id)
    {
        await this.Repository.SoftDeleteAsync(id, this.Clock.Now);
    }

    protected override async Task SetFieldsBeforeInsertAsync(Goods entity)
    {
        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        entity.Name = _goodsUtils.NormalizeName(entity.Name);
        entity.SeoName = this._salesUtils.NormalizeSeoName(entity.SeoName);

        if (!string.IsNullOrWhiteSpace(entity.SeoName))
        {
            if (await this.Repository.AnyAsync(x => x.SeoName == entity.SeoName))
                throw new UserFriendlyException("seo name exist");
        }
    }

    public override Task<Goods> InsertAsync(GoodsDto dto)
    {
        if (!CheckGoodsForm(dto, out var error))
            throw new UserFriendlyException(error);

        return base.InsertAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Goods goods, GoodsDto dto)
    {
        await Task.CompletedTask;

        //type
        //goods.GoodsType = dto.GoodsType;
        goods.Name = _goodsUtils.NormalizeName(dto.Name);
        goods.SeoName = _salesUtils.NormalizeSeoName(dto.SeoName);
        goods.Description = dto.Description;
        goods.DetailInformation = dto.DetailInformation;
        goods.Keywords = dto.Keywords;

        goods.LimitedToRedirect = dto.LimitedToRedirect;
        goods.RedirectToUrlJson = dto.RedirectToUrlJson;
        goods.LimitedToContact = dto.LimitedToContact;
        goods.ContactTelephone = dto.ContactTelephone;

        goods.StockDeductStrategy = dto.StockDeductStrategy;
        goods.PayAfterDeliveredSupported = dto.PayAfterDeliveredSupported;

        goods.CategoryId = dto.CategoryId;
        goods.BrandId = dto.BrandId;
        goods.AreaId = dto.AreaId;

        goods.StickyTop = dto.StickyTop;

        goods.AdminComment = dto.AdminComment;

        goods.DisplayAsInformationPage = dto.DisplayAsInformationPage;
        goods.CommentSupported = dto.CommentSupported;
        goods.PlaceOrderSupported = dto.PlaceOrderSupported;
        goods.AfterSalesSupported = dto.AfterSalesSupported;
        goods.PayAfterDeliveredSupported = dto.PayAfterDeliveredSupported;
        goods.Published = dto.Published;

        goods.LastModificationTime = Clock.Now;

        if (!string.IsNullOrWhiteSpace(goods.SeoName))
        {
            if (await this.Repository.AnyAsync(x => x.Id != goods.Id && x.SeoName == goods.SeoName))
                throw new UserFriendlyException("seo name exist");
        }
    }

    public override Task<Goods> UpdateAsync(GoodsDto dto)
    {
        if (!CheckGoodsForm(dto, out var error))
            throw new UserFriendlyException(error);

        return base.UpdateAsync(dto);
    }

    public async Task SetTagsAsync(SetGoodsTagsInput dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id) || dto.TagIds == null)
            throw new ArgumentException(nameof(SetTagsAsync));

        var entities = dto.TagIds.Select(x => new TagGoodsMapping { GoodsId = dto.Id, TagId = x }).ToArray();

        var db = await Repository.GetDbContextAsync();

        var set = db.Set<TagGoodsMapping>();

        var tagsMapping = await set.Where(x => x.GoodsId == dto.Id).ToArrayAsync();

        var toDeleted = tagsMapping.NotInBy(entities, x => x.TagId).ToArray();
        var toAdd = entities.NotInBy(tagsMapping, x => x.TagId).ToArray();

        if (toDeleted.Any())
            set.RemoveRange(toDeleted);

        if (toAdd.Any())
            set.AddRange(toAdd);

        await db.TrySaveChangesAsync();
    }

    private bool CheckGoodsForm(Goods dto, out string error)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        error = string.Empty;
        if (string.IsNullOrWhiteSpace(dto.Name))
        {
            error = "name is required";
            return false;
        }

        return true;
    }

    public async Task<GoodsDto[]> AttachSkusAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var goodsIds = data.Ids();

        var db = await Repository.GetDbContextAsync();

        var goodsSpecs = await db.Set<Sku>().AsNoTracking()
            .Where(x => goodsIds.Contains(x.GoodsId)).ToArrayAsync();

        foreach (var m in data)
        {
            m.Skus = goodsSpecs
                .Where(x => x.GoodsId == m.Id)
                .MapArrayTo<Sku, SkuDto>(this.ObjectMapper)
                .ToArray();
        }

        return data;
    }

    public async Task<GoodsDto[]> AttachTagsAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var goodsIds = data.Select(x => x.Id).Distinct().ToArray();

        var db = await Repository.GetDbContextAsync();

        var query = from tagGoods in db.Set<TagGoodsMapping>().AsNoTracking()
            join tag in db.Set<Tag>().AsNoTracking()
                on tagGoods.TagId equals tag.Id
            select new { tagGoods, tag };
        query = query.Where(x => goodsIds.Contains(x.tagGoods.GoodsId));
        var tags = await query.ToArrayAsync();

        foreach (var m in data)
        {
            m.Tags = tags.Where(x => x.tagGoods.GoodsId == m.Id).Select(x => x.tag)
                .MapArrayTo<Tag, TagDto>(ObjectMapper).ToArray();
        }

        return data;
    }

    public async Task<GoodsDto[]> AttachAreasAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await Repository.GetDbContextAsync();

        var ids = data.Select(x => x.AreaId).WhereNotEmpty().Distinct().ToArray();
        if (ids.Any())
        {
            var datalist = await db.Set<Area>().AsNoTracking().Where(x => ids.Contains(x.Id))
                .ToArrayAsync();

            foreach (var m in data)
            {
                m.Area = datalist.FirstOrDefault(x => x.Id == m.AreaId)
                    ?.MapTo<Area, AreaDto>(ObjectMapper);
            }
        }

        return data;
    }

    public async Task<GoodsDto[]> AttachCategoriesAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await Repository.GetDbContextAsync();

        var categoryIds = data.Select(x => x.CategoryId).WhereNotEmpty().Distinct().ToArray();
        if (categoryIds.Any())
        {
            var categories = await db.Set<Category>().AsNoTracking().Where(x => categoryIds.Contains(x.Id))
                .ToArrayAsync();
            foreach (var m in data)
                m.Category = categories.FirstOrDefault(x => x.Id == m.CategoryId)
                    ?.MapTo<Category, CategoryDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<GoodsDto[]> AttachBrandsAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await Repository.GetDbContextAsync();

        var brandIds = data.Select(x => x.BrandId).WhereNotEmpty().Distinct().ToArray();

        if (brandIds.Any())
        {
            var brands = await db.Set<Brand>().AsNoTracking().Where(x => brandIds.Contains(x.Id))
                .ToArrayAsync();

            foreach (var m in data)
            {
                m.Brand = brands.FirstOrDefault(x => x.Id == m.BrandId)?.MapTo<Brand, BrandDto>(ObjectMapper);
            }
        }

        return data;
    }
}