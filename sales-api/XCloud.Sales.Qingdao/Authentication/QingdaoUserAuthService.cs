using System;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Authentication;

[DisableConventionalRegistration]
public class QingdaoUserAuthService : SalesAppService, IUserAuthService
{
    private readonly Sh3HClient _sh3HClient;
    private readonly IRequestCacheProvider _cacheProvider;
    private readonly IQingdaoUserAccountService _qingdaoUserAccountService;

    public QingdaoUserAuthService(Sh3HClient sh3HClient,
        IRequestCacheProvider cacheProvider,
        IQingdaoUserAccountService qingdaoUserAccountService)
    {
        _sh3HClient = sh3HClient;
        _cacheProvider = cacheProvider;
        _qingdaoUserAccountService = qingdaoUserAccountService;
    }

    private async Task<UserAuthResult> MapAdminUserAsync()
    {
        var response = new UserAuthResult();

        var config = this.Configuration.GetRequired3HConfig();
        var token = this.WebHelper.RequiredHttpContext.GetBearerToken();

        if (string.IsNullOrWhiteSpace(token))
        {
            return response.SetError(nameof(token));
        }

        var wapAdminDto = await this._sh3HClient.GetWapAdminAsync(config, token);

        if (wapAdminDto == null)
        {
            return response.SetError("token is invalid");
        }

        response.SetPropertyAsJson(wapAdminDto, this.JsonDataSerializer);

        var user = await this._qingdaoUserAccountService.PrepareUserAsync(wapAdminDto);

        response.SetData(user);

        return response;
    }

    private async Task<UserAuthResult> MapMcsUserAsync()
    {
        var response = new UserAuthResult();

        var config = this.Configuration.GetRequired3HConfig();

        var token = this.WebHelper.RequiredHttpContext.GetBearerToken();

        if (string.IsNullOrWhiteSpace(token))
        {
            return response.SetError("token is required");
        }

        var mcsUser = await this._sh3HClient.GetMcsUserAsync(config, token);

        if (mcsUser == null)
        {
            return response.SetError("token is invalid");
        }

        response.SetPropertyAsJson(mcsUser, this.JsonDataSerializer);

        var user = await this._qingdaoUserAccountService.PrepareUserAsync(mcsUser);

        response.SetData(user);

        return response;
    }

    public async Task<UserAuthResult> GetAuthUserAsync()
    {
        return await this._cacheProvider.GetOrSetAsync(async () =>
        {
            if (this.WebHelper.RequiredHttpContext.IsMcsToken())
            {
                return await MapMcsUserAsync();
            }
            else if (this.WebHelper.RequiredHttpContext.IsWapToken())
            {
                return await MapAdminUserAsync();
            }
            else
            {
                throw new ArgumentException(nameof(GetAuthUserAsync));
            }
        });
    }
}