import { useRequest } from 'ahooks';
import { Alert, Badge, Button, Card, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XUserAvatar from '@/components/manage/user/avatar';
import { apiClient } from '@/utils/client';
import { AdminDto, DepartmentDto } from '@/utils/swagger';

import XDepartment from './department';
import XInvite from './invite';
import XRoles from './roles';
import XForm from './status';

export default ({
  department,
  ok,
}: {
  department?: DepartmentDto;
  ok: () => void;
}) => {
  const get_department_admin_request = useRequest(
    apiClient.sys.departmentListDepartmentAdmins,
    { manual: true },
  );
  const get_dangling_admins_request = useRequest(
    apiClient.sys.departmentDanglingAdmin,
    {
      manual: true,
    },
  );

  const any_loading =
    get_department_admin_request.loading || get_dangling_admins_request.loading;

  const data: AdminDto[] =
    get_department_admin_request.data?.data?.Data ||
    get_dangling_admins_request.data?.data?.Data ||
    [];

  const [formdata, _formdata] = useState<AdminDto | undefined>(undefined);
  const [show_invite, _show_invite] = useState(false);

  const queryAdmins = () => {
    get_department_admin_request.mutate();
    get_dangling_admins_request.mutate();

    if (department && department.Id) {
      get_department_admin_request.run({ Id: department.Id });
    } else {
      get_dangling_admins_request.run({});
    }
  };

  useEffect(() => {
    queryAdmins();
  }, [department?.Id]);

  const columns: ColumnType<AdminDto>[] = [
    {
      title: '账号主体',
      render: (record: AdminDto) => <XUserAvatar model={record.User} />,
    },
    {
      title: '名称',
      fixed: 'left',
      render: (text, record) => {
        let name = record.User?.IdentityName;
        return <a>{name}</a>;
      },
    },
    {
      title: '所属部门',
      render: (d, x) => {
        return (
          <>
            <XDepartment
              data={x}
              ok={() => {
                queryAdmins();
                ok();
              }}
            />
          </>
        );
      },
    },
    {
      title: '角色',
      render: (d, x) => (
        <XRoles
          model={x}
          ok={() => {
            queryAdmins();
          }}
        />
      ),
    },
    {
      title: '活动状态',
      render: (d, x) => (
        <Badge
          status={x.IsActive ? 'success' : 'default'}
          text={x.IsActive ? '活动' : '冻结'}
        />
      ),
    },
    {
      title: '超级管理员',
      render: (d, x) => (
        <Badge
          status={x.IsSuperAdmin ? 'success' : 'default'}
          text={x.IsSuperAdmin ? '超级管理员' : '普通管理员'}
        />
      ),
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (text, record) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formdata(record);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];
  return (
    <>
      <XForm
        show={formdata != undefined}
        hide={() => _formdata(undefined)}
        ok={() => {
          _formdata(undefined);
          queryAdmins();
        }}
        model={formdata || {}}
      />
      <XInvite
        show={show_invite}
        hide={() => _show_invite(false)}
        ok={() => {
          queryAdmins();
          ok();
        }}
        department={department}
      />
      <Card
        title="部门成员"
        extra={
          <Space direction="horizontal">
            <Button
              type="link"
              onClick={() => {
                _show_invite(true);
              }}
            >
              添加成员
            </Button>
          </Space>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={any_loading}
          columns={columns}
          dataSource={data}
          pagination={false}
          expandable={{
            expandedRowRender: (x) => {
              const permissions =
                x.Roles?.flatMap((d) => d.PermissionKeys || []) || [];
              return (
                <>
                  <h5>权限值</h5>
                  <div>
                    {permissions.map((d, i) => (
                      <Tag key={i} style={{ margin: 5 }}>
                        {d}
                      </Tag>
                    ))}
                    {permissions.length <= 0 && <Alert message="无任何权限" />}
                  </div>
                </>
              );
            },
          }}
        />
      </Card>
    </>
  );
};
