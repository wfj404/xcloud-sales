using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderReviewMap : SalesEntityTypeConfiguration<OrderReview>
{
    public override void Configure(EntityTypeBuilder<OrderReview> builder)
    {
        builder.ToTable("sales_order_review");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.ReviewText).HasMaxLength(1000);

        builder.Property(x => x.Approved).HasDefaultValue(false);
        builder.Property(x => x.OrderId).RequiredId();
        builder.Property(x => x.Rating);
        builder.Property(x => x.PictureMetaIdsJson);
        builder.Property(x => x.ExtraData);
        builder.Property(x => x.IpAddress).HasMaxLength(200);

        base.Configure(builder);
    }
}