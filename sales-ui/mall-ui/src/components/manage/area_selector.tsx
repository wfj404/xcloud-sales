import { useRequest } from 'ahooks';
import { Space, TreeSelect } from 'antd';
import React, { useEffect } from 'react';

import { getFlatNodes, getTreeSelectOptionsV2 } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { resolveUrl } from '@/utils/storage';
import { AntDesignTreeNode1AreaDto, AreaDto } from '@/utils/swagger';
import { getPinyinShortStyle } from '@/utils/utils';

export default ({
  value,
  onChange,
}: {
  value?: string | null;
  onChange?: (e: AreaDto | undefined) => void;
}) => {
  const query_tree_request = useRequest(apiClient.sys.areaLoadTree, {
    manual: true,
  });

  const tree_data = query_tree_request.data?.data?.Data || [];

  const flatNodes = tree_data.flatMap((x) => getFlatNodes(x));

  const allKeys = [...new Set(flatNodes.map((x) => x.key || ''))];

  const options = tree_data.map((x) =>
    getTreeSelectOptionsV2(x, (d) => {
      const url = resolveUrl(
        (d as AntDesignTreeNode1AreaDto).raw_data?.StorageMeta,
        { width: 20 },
      );
      return (
        <Space direction="horizontal">
          {url && <img src={url} alt="" />}
          <span>
            {`${d.title}-${getPinyinShortStyle(d.title || '') || 'N/A'}`}
          </span>
        </Space>
      );
    }),
  );

  useEffect(() => {
    query_tree_request.run({});
  }, []);

  return (
    <>
      <TreeSelect
        loading={query_tree_request.loading}
        value={value}
        onChange={(e) => {
          onChange && onChange(flatNodes.find((x) => x.key == e)?.raw_data);
        }}
        placeholder="输入拼音缩写可以搜索..."
        treeData={options}
        treeExpandedKeys={allKeys}
        treeDefaultExpandAll
        allowClear
        showSearch
        treeNodeFilterProp="label"
        filterTreeNode={undefined}
      />
    </>
  );
};
