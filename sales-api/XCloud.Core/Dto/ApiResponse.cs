﻿using JetBrains.Annotations;
using Volo.Abp.Data;
using Volo.Abp.Http;

namespace XCloud.Core.Dto;

public class ApiResponse<T> : RemoteServiceErrorResponse, IDataContainer<T>, IHasExtraProperties
{
    [CanBeNull] public T Data { get; set; }

    private ExtraPropertyDictionary _extraProperties;

    [NotNull] public ExtraPropertyDictionary ExtraProperties => _extraProperties ??= new ExtraPropertyDictionary();

    /// <summary>
    /// 默认为success，error为null
    /// </summary>
    public ApiResponse() : base(error: null)
    {
        //
    }

    public ApiResponse(T data) : this()
    {
        Data = data;
    }

    public object this[string key]
    {
        get => this.ExtraProperties[key];
        set => this.ExtraProperties[key] = value;
    }

    public static implicit operator bool(ApiResponse<T> data) => data.Error == null;
}