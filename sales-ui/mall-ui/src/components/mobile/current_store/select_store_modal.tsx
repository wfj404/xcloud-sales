import { useRequest } from 'ahooks';
import { useEffect } from 'react';
import { useSnapshot } from 'umi';

import XLoading from '@/components/common/loading/skeleton';
import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { GeoLocationDto } from '@/utils/swagger';
import {
  GlobalContainer,
  handleResponse,
  isArrayEmpty,
  try_get_browser_geo_location,
} from '@/utils/utils';
import { Dialog, DialogContent, DialogTitle } from '@mui/material';

import StoreItem from './store_item';

export default ({ show, close }: { show: boolean; close: () => void }) => {
  const app = useSnapshot(storeState) as StoreType;

  const queryStoresRequest = useRequest(apiClient.mall.storeListStores, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {});
    },
  });

  const stores = queryStoresRequest.data?.data?.Data || [];

  const get_geo_location_request = useRequest(try_get_browser_geo_location, {
    manual: true,
    onSuccess: (res) => {
      console.log('定位结果', res);
      const location: GeoLocationDto = {
        Lat: res.coords.latitude,
        Lon: res.coords.longitude,
      };

      queryStoresRequest.run({
        headers: {
          'x-location': JSON.stringify(location),
        },
      });
    },
    onError: (e) => {
      console.log(e);
      GlobalContainer.message?.error('定位失败，将展示默认门店');
      queryStoresRequest.run({});
    },
  });

  useEffect(() => {
    if (!show) {
      return;
    }

    alert('请允许我们通过您的定位来获取附近的门店');
    get_geo_location_request.run();
  }, [show]);

  const renderContent = () => {
    if (queryStoresRequest.loading || get_geo_location_request.loading) {
      return <XLoading />;
    }

    if (isArrayEmpty(stores)) {
      return <h2>附近没有可选门店</h2>;
    }

    return stores?.map((x, i) => (
      <div key={i}>
        <StoreItem currentId={app.currentStore?.Id} store={x} />
      </div>
    ));
  };

  return (
    <>
      <Dialog
        open={show}
        onClose={() => close && close()}
        scroll={'body'}
        fullWidth
        maxWidth={'sm'}
      >
        <div
          style={{
            backgroundColor: 'rgb(251,251,251)',
          }}
        >
          <DialogTitle>附近门店</DialogTitle>
          <DialogContent>{renderContent()}</DialogContent>
        </div>
      </Dialog>
    </>
  );
};
