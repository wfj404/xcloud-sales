import { Card, Col, Divider, Row } from 'antd';
import { CSSProperties, ReactNode, useState } from 'react';

import XRenderBlock from '@/components/blocks/render_block_item';
import {
  BlockItem,
  BlockItemProps,
  menuToBlockItem,
} from '@/components/blocks/utils';
import { DndDataType } from '@/utils/biz';
import { insertAt } from '@/utils/utils';
import {
  closestCenter,
  DndContext,
  DragEndEvent,
  DragOverlay,
  PointerSensor,
  UniqueIdentifier,
  useSensor,
  useSensors,
} from '@dnd-kit/core';
import {
  arrayMove,
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';

import XMenuItem from '../../editor/menu_item';
import XPlaceHolder from '../placeholder';
import XToolbarLayer from '../toolbox_layer';
import { blocks_droppable_area_id, menu_options } from '../utils';
import XDragItem from './drag';
import DroppableArea from './drop';
import DragSortItem from './sort_item';

export default function ({
  blocks,
  onChange,
  renderExtra,
  onMaskClick,
  selectedIndex,
  onInsert,
  loading,
}: {
  blocks: BlockItem<BlockItemProps>[];
  onChange: (e: BlockItem<BlockItemProps>[]) => void;
  loading?: boolean;
  renderExtra?: () => ReactNode;
  onMaskClick: (i: number) => void;
  selectedIndex?: number;
  onInsert?: (x: number) => void;
}) {
  const sensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: {
        delay: 500,
        tolerance: 30,
      },
    }),
  );

  const [activeId, _activeId] = useState<UniqueIdentifier | undefined>(
    undefined,
  );
  const [overId, _overId] = useState<UniqueIdentifier | undefined>(undefined);

  const blocks_options = blocks.map<DndDataType<BlockItem<BlockItemProps>>>(
    (x, i) => ({
      item: x,
      id: `temp-id-${i}`,
    }),
  );

  const active_menu = menu_options.find((x) => x.id == activeId);
  const active_block = blocks_options.find((x) => x.id == activeId);
  const over_block = blocks_options.find((x) => x.id == overId);

  const tryAddCurrentMenuToBlockSortedList = (
    overId: UniqueIdentifier,
    placeholder?: boolean,
  ): DndDataType<BlockItem<BlockItemProps>>[] => {
    let datalist: DndDataType<BlockItem<BlockItemProps>>[] = [
      ...blocks_options,
    ];

    if (active_menu) {
      const id_prefix = placeholder ? 'placeholder-block' : 'new-block-item';
      const dragged_item: DndDataType<BlockItem<BlockItemProps>> = {
        id: `${id_prefix}-${active_menu.id}`,
        item: menuToBlockItem(active_menu.item),
        placeholder: placeholder,
      };

      if (overId == blocks_droppable_area_id) {
        datalist = [dragged_item, ...datalist];
      } else if (over_block) {
        const index = datalist.map((x) => x.id).indexOf(over_block.id);
        if (index >= 0) {
          datalist = insertAt(datalist, dragged_item, index);
        }
      }
    }

    return datalist;
  };

  const getSortListItemData = (): DndDataType<BlockItem<BlockItemProps>>[] => {
    let datalist: DndDataType<BlockItem<BlockItemProps>>[] = [
      ...blocks_options,
    ];

    if (overId) {
      datalist = tryAddCurrentMenuToBlockSortedList(overId, true);
    }

    return datalist;
  };

  const onDragEnd = (e: DragEndEvent) => {
    console.log('end', e);
    if (!activeId || !overId) {
      return;
    }

    if (activeId == overId) {
      return;
    }

    let datalist: DndDataType<BlockItem<BlockItemProps>>[] = [
      ...blocks_options,
    ];

    if (active_menu) {
      //添加新组件
      datalist = tryAddCurrentMenuToBlockSortedList(overId, false);
    } else if (active_block && over_block) {
      //排序
      const ids = datalist.map((x) => x.id);
      const from = ids.indexOf(activeId);
      const to = ids.indexOf(overId);
      if (from >= 0 && to >= 0) {
        datalist = arrayMove(datalist, from, to);
      }
    }

    console.log(
      'drag end:',
      datalist.map((x) => x.id),
    );
    onChange(datalist.map((x) => x.item));
  };

  const renderSortListItem = (
    x: DndDataType<BlockItem<BlockItemProps>>,
    i: number,
  ) => {
    const styles: CSSProperties = {};
    if (x.id == overId) {
      //styles.border = '3px dashed green';
    }

    if (x.placeholder) {
      return (
        <div style={{ ...styles }}>
          <XPlaceHolder block={x.item} />
        </div>
      );
    }

    return (
      <XToolbarLayer
        data={x.item}
        i={i}
        blocks={blocks}
        selected={i == selectedIndex}
        onBlocksChange={(e) => {
          onChange(e);
        }}
        onMaskClick={() => {
          onMaskClick && onMaskClick(i);
        }}
        onInsert={(i) => {
          onInsert && onInsert(i);
        }}
      >
        <div style={{ ...styles }}>
          <XRenderBlock block={x.item} />
        </div>
      </XToolbarLayer>
    );
  };

  const calculated_blocks = getSortListItemData();

  return (
    <>
      <DndContext
        sensors={sensors}
        collisionDetection={closestCenter}
        onDragStart={(e) => {
          _activeId(e.active.id);
        }}
        onDragOver={(e) => {
          if (!e.over) {
            return;
          }
          const item = calculated_blocks.find((x) => x.id == e.over?.id);
          if (item?.placeholder) {
            //skip placeholder
            return;
          }

          _overId(e.over.id);
        }}
        onDragEnd={(e) => {
          onDragEnd(e);
          _activeId(undefined);
          _overId(undefined);
        }}
      >
        <Row gutter={[10, 10]}>
          <Col span={8}>
            <Card title="组件" size="small">
              <DroppableArea id={'toolbox-drop-area'}>
                <Divider type="horizontal" orientation="center">
                  通用组件
                </Divider>
                <Row gutter={[10, 10]}>
                  {menu_options.map((x, i) => {
                    return (
                      <Col key={i} span={12}>
                        <XDragItem id={x.id}>
                          <XMenuItem x={x.item} />
                        </XDragItem>
                      </Col>
                    );
                  })}
                </Row>
              </DroppableArea>
            </Card>
          </Col>
          <Col span={16}>
            <Card
              loading={loading}
              title="预览"
              size="small"
              extra={renderExtra && renderExtra()}
            >
              <DroppableArea id={blocks_droppable_area_id}>
                <SortableContext
                  items={calculated_blocks}
                  strategy={verticalListSortingStrategy}
                >
                  {calculated_blocks.map((x, i) => {
                    return (
                      <DragSortItem id={x.id} key={x.id}>
                        {renderSortListItem(x, i)}
                      </DragSortItem>
                    );
                  })}
                </SortableContext>
              </DroppableArea>
            </Card>
          </Col>
        </Row>
        <DragOverlay>
          {active_menu && (
            <div style={{ border: '2px dashed orange' }}>
              <XMenuItem x={active_menu.item} />
            </div>
          )}
          {active_block && active_block.item && (
            <div style={{ border: '2px dashed orange' }}>
              <XRenderBlock block={active_block.item} />
            </div>
          )}
        </DragOverlay>
      </DndContext>
    </>
  );
}
