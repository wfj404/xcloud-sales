﻿using System.Threading.Tasks;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Domain.Security;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Permissions;

namespace XCloud.Platform.Application.Service.Roles;

public interface IAdminRoleService : IXCloudAppService
{
    Task<Role[]> QueryByAdminIdAsync(string adminId, CacheStrategy cacheStrategy);

    Task SetAdminRolesAsync(string adminId, string[] roleIds);

    Task<AdminDto[]> AttachRolesAsync(AdminDto[] data);
}

public class AdminRoleService : PlatformAppService, IAdminRoleService
{
    private readonly IPlatformRepository<Role> _roleRepository;

    public AdminRoleService(IPlatformRepository<Role> roleRepository)
    {
        _roleRepository = roleRepository;
    }

    public async Task<AdminDto[]> AttachRolesAsync(AdminDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _roleRepository.GetDbContextAsync();

        var query = from adminRole in db.Set<AdminRole>().AsNoTracking()
            join role in db.Set<Role>().AsNoTracking()
                on adminRole.RoleId equals role.Id
            select new { adminRole, role };

        var ids = data.Ids();

        var datalist = await query
            .Where(x => ids.Contains(x.adminRole.AdminId)).ToArrayAsync();

        foreach (var m in data)
        {
            var roles = datalist.Where(x => x.adminRole.AdminId == m.Id).Select(x => x.role).ToArray();
            m.Roles = roles.Select(x => ObjectMapper.Map<Role, RoleDto>(x)).ToArray();
        }

        return data;
    }

    public async Task<Role[]> QueryByAdminIdAsync(string adminId, CacheStrategy cacheStrategy)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        var key = $"roles.by.admin.id:{adminId}";
        var option = new CacheOption<Role[]>(key, TimeSpan.FromMinutes(5));

        var response =
            await CacheProvider.ExecuteWithPolicyAsync(
                async () =>
                {
                    var db = await _roleRepository.GetDbContextAsync();

                    var assignQuery = db.Set<AdminRole>().AsNoTracking()
                        .Where(x => x.AdminId == adminId);

                    var roleQuery = db.Set<Role>().AsNoTracking();

                    var query = from assign in assignQuery
                        join role in roleQuery
                            on assign.RoleId equals role.Id
                        select new { assign, role };

                    var data = await query.OrderBy(x => x.role.CreationTime).ToArrayAsync();

                    var roles = data.Select(x => x.role).ToArray();

                    return roles;
                },
                option, cacheStrategy);

        response ??= Array.Empty<Role>();

        return response;
    }

    public async Task SetAdminRolesAsync(string adminId, string[] roleIds)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        if (roleIds == null)
            throw new ArgumentNullException(nameof(roleIds));

        var assigns = roleIds.Select(x => new AdminRole
        {
            RoleId = x,
            AdminId = adminId,
        }).ToArray();

        var db = await _roleRepository.GetDbContextAsync();

        var set = db.Set<AdminRole>();

        var originData = await set.Where(x => x.AdminId == adminId).ToArrayAsync();

        static string FingerPrint(AdminRole entity) => $"{entity.RoleId}";

        var toDelete = originData.NotInBy(assigns, FingerPrint).ToArray();
        var toInsert = assigns.NotInBy(originData, FingerPrint).ToArray();

        if (toDelete.Any())
            set.RemoveRange(toDelete);

        if (toInsert.Any())
        {
            var now = Clock.Now;
            foreach (var m in toInsert)
            {
                m.Id = GuidGenerator.CreateGuidString();
                m.CreationTime = now;
                set.Add(m);
            }
        }

        await db.TrySaveChangesAsync();
    }
}