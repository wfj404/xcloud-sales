namespace XCloud.Sales.Application.Domain.Orders;

public class OrderItemGift : SalesBaseEntity<string>
{
    public string OrderItemId { get; set; }
    public string SkuId { get; set; }
    public int Quantity { get; set; }
}