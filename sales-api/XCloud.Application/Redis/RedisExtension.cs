﻿using System;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using XCloud.Core.Exceptions;

namespace XCloud.Application.Redis;

public static class RedisExtension
{
    public static IDatabase SelectDatabase(this IConnectionMultiplexer con, int? db)
    {
        var res = db == null ? con.GetDatabase() : con.GetDatabase(db.Value);

        return res;
    }

    public static RedisCacheOptions GetAbpRedisCacheOptionsOrNull(this IConfiguration configuration)
    {
        var section = configuration.GetSection("Redis");

        if (!section.Exists())
            return null;

        var option = new RedisCacheOptions();

        section.Bind(option);

        return option;
    }

    public static string GetRedisConnectionString(this IConfiguration config)
    {
        var section = config.GetSection("app").GetSection("redis");

        if (section.Exists())
        {
            var host = section["host"];
            var port = section["port"];

            if (string.IsNullOrEmpty(host) || string.IsNullOrEmpty(port))
                throw new ArgumentNullException(nameof(GetRedisConnectionString));

            var connectionString = $"{host}:{port}";

            return connectionString;
        }
        else
        {
            var abpRedisConnectionString = GetAbpRedisCacheOptionsOrNull(config)?.Configuration;

            if (string.IsNullOrWhiteSpace(abpRedisConnectionString))
                throw new ConfigException(nameof(abpRedisConnectionString));

            return abpRedisConnectionString;
        }
    }
}