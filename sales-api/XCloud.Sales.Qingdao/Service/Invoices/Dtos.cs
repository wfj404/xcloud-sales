﻿using System;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Qingdao.Domain.Invoices;

namespace XCloud.Sales.Qingdao.Service.Invoices;

public class CreateInvoiceRequestInput : IEntityDto
{
    public string[] bizIds { get; set; }

    public string userInvoiceId { get; set; }
}

public class McsBizOrderCallbackResult : IEntityDto
{
    public int success { get; set; }
    public int fail { get; set; }
    public McsBizOrderDto[] bills { get; set; }
}

public class McsBizOrderItemDto : IEntityDto
{
    public int productId { get; set; }
    public string productName { get; set; }
    public string introduce { get; set; }
    public decimal money { get; set; }
    public string remark { get; set; }
}

public class McsBizOrderDto : IEntityDto
{
    public string bizId { get; set; }
    public int bizType { get; set; }
    public string title { get; set; }
    public string userId { get; set; }
    public string sourceType { get; set; }
    public decimal totalMoney { get; set; }

    [Obsolete("非字面意义")] public bool canInvoicing { get; set; }

    /// <summary>
    /// !can-invoicing
    /// </summary>
    public bool AvailableForInvoicing => !this.canInvoicing;

    public int outletId { get; set; }
    public string outletTitle { get; set; }
    public string billDate { get; set; }
    public string payTime { get; set; }
    public string chargeDate { get; set; }
    public string remark { get; set; }
    public McsBizOrderItemDto[] details { get; set; }
}

public static class McsInvoiceRequestStatus
{
    public static string Pending => "pending";

    public static string Success => "success";

    public static string Failed => "failed";
}

public class McsInvoiceRequestItemDto : McsInvoiceRequestItem, IEntityDto
{
    public McsInvoiceRequestDto McsInvoiceRequest { get; set; }

    public McsBizOrderDto McsBizOrder { get; set; }
}

public class McsInvoiceRequestDto : McsInvoiceRequest, IEntityDto
{
    public McsInvoiceRequestItemDto[] Items { get; set; }

    public McsUserDto McsUser { get; set; }

    public UserInvoiceDto UserInvoice { get; set; }

    public StorageMetaDto StorageMeta { get; set; }

    public void InitFields()
    {
        Status = McsInvoiceRequestStatus.Pending;
        AttachmentMetaId = default;
        InvoicingTime = default;
        Picked = false;
        PickedTime = default;
        IsDeleted = false;
    }
}

public class QueryMcsBizOrderForInvoicingInput : PagedRequest
{
    public string QueryType { get; set; }
    public string QueryValue { get; set; }
    public string Status { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class QueryMcsInvoiceRequestInput : PagedRequest
{
    public int PickupLocationId { get; set; }

    public string McsUserId { get; set; }

    public string Status { get; set; }

    public bool? Picked { get; set; }

    public string BizType { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public string BizOrderId { get; set; }
}