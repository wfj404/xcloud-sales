import { ReactElement, ReactNode } from 'react';
import { history, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isArrayEmpty } from '@/utils/utils';
import { ChevronLeft } from '@mui/icons-material';
import { Container, IconButton, Stack } from '@mui/material';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export default function ({
  children,
  rightButtons,
  leftButtons,
}: {
  children?: ReactNode;
  leftButtons?: Array<ReactElement>;
  rightButtons?: Array<ReactElement>;
}) {
  const app = useSnapshot(storeState) as StoreType;

  return (
    <>
      <Container disableGutters maxWidth="sm">
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            py: 1,
          }}
        >
          <Stack
            direction="row"
            alignItems="center"
            spacing={{ xs: 0.5, sm: 1.5 }}
          >
            {isArrayEmpty(leftButtons) && (
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  cursor: 'pointer',
                }}
                onClick={() => {
                  if (window.history.length > 1) {
                    history.back();
                  } else {
                    history.push({
                      pathname: '/',
                    });
                  }
                }}
              >
                <IconButton size="large" color="inherit">
                  <ChevronLeft />
                </IconButton>
                <Typography variant="h2" sx={{}}>
                  {window.document.title || app.getAppName()}
                </Typography>
              </Box>
            )}
            {leftButtons?.map((x) => x)}
          </Stack>
          <Stack
            direction="row"
            alignItems="center"
            spacing={{ xs: 0.5, sm: 1.5 }}
          >
            {rightButtons?.map((x) => x)}
          </Stack>
        </Box>

        <Box sx={{ mt: 0 }}>{children}</Box>
      </Container>
    </>
  );
}
