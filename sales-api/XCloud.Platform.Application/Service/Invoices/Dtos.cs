using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Domain.Invoices;

namespace XCloud.Platform.Application.Service.Invoices;

public class UserInvoiceDto : UserInvoice, IEntityDto<string>
{
    //
}

public class QueryUserInvoicePagingInput : PagedRequest
{
    public string UserId { get; set; }
}

[Obsolete]
public class UpdateUserInvoiceStatusInput : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? IsDeleted { get; set; }
}