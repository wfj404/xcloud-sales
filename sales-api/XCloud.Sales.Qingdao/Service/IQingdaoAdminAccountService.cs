﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Service;

namespace XCloud.Sales.Qingdao.Service;

public interface IQingdaoAdminAccountService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<AdminDto> PrepareAdminAsync(UserDto user, WapAdminDto wapAdminDto);
}

public class QingdaoAdminAccountService : SalesAppService, IQingdaoAdminAccountService
{
    private readonly IAdminAccountService _adminAccountService;

    public QingdaoAdminAccountService(IAdminAccountService adminAccountService)
    {
        _adminAccountService = adminAccountService;
    }

    public async Task<AdminDto> PrepareAdminAsync(UserDto user, WapAdminDto wapAdminDto)
    {
        var admin = await _adminAccountService.GetOrCreateByUserIdAsync(user.Id);

        admin = await this.TryUpdateInfoAsync(admin, wapAdminDto);

        return admin;
    }

    private async Task<AdminDto> TryUpdateInfoAsync(AdminDto admin, WapAdminDto wapAdminDto)
    {
        if (admin.IsActive != wapAdminDto.active || !admin.IsSuperAdmin)
        {
            await _adminAccountService.UpdateStatusAsync(new UpdateAdminStatusDto
            {
                Id = admin.Id,
                IsActive = wapAdminDto.active,
                IsSuperAdmin = true
            });

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return admin;
    }
}