﻿using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Application.Service.Token;

public class ValidationCode : IEntityDto
{
    public string Key { get; set; }
    public string Code { get; set; }
    public DateTime CreateTime { get; set; }
}