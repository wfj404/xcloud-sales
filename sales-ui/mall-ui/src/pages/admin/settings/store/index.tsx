import { Tabs } from 'antd';
import { useState } from 'react';
import { useParams } from 'umi';

import {
  BgColorsOutlined,
  SettingOutlined,
  ShopOutlined,
} from '@ant-design/icons';

import XCommon from './common';
import XHomePage from './home_page';
import XStore from './store';

export default () => {
  const param = useParams<{ id?: string }>();

  const [selected, _selected] = useState<string | null | undefined>(param.id);

  return (
    <>
      <Tabs
        destroyInactiveTabPane
        activeKey={selected || 'common'}
        onChange={(e) => _selected(e)}
        items={[
          {
            key: 'common',
            label: '通用设置',
            children: <XCommon />,
            icon: <SettingOutlined />,
          },
          {
            key: 'store',
            label: '门店管理',
            children: <XStore />,
            icon: <ShopOutlined />,
          },
          {
            key: 'home-page-edit',
            label: '首页装修',
            children: <XHomePage />,
            icon: <BgColorsOutlined />,
          },
        ]}
      />
    </>
  );
};
