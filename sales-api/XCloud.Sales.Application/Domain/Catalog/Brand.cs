using XCloud.Application.Utils;

namespace XCloud.Sales.Application.Domain.Catalog;

[TypeIdentityName("brand")]
public class Brand : SalesBaseEntity<string>, IHasDeletionTime, IHasCreationTime
{
    /// <summary>
    /// Gets or sets the name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the description
    /// </summary>
    public string Description { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether show on public  page 
    /// </summary>
    public bool ShowOnPublicPage { get; set; }

    /// <summary>
    /// Gets or sets the meta keywords
    /// </summary>
    public string MetaKeywords { get; set; }

    public string PictureMetaId { get; set; }

    public bool Published { get; set; }

    public DateTime? DeletionTime { get; set; }

    public bool IsDeleted { get; set; }

    public int DisplayOrder { get; set; }

    public DateTime CreationTime { get; set; }
}