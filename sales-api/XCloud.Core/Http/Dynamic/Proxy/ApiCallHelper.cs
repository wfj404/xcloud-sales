﻿using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp;
using XCloud.Core.Configuration;
using XCloud.Core.Http.Dynamic.Definition;
using XCloud.Core.Http.Dynamic.Interceptor;
using XCloud.Core.Json;

namespace XCloud.Core.Http.Dynamic.Proxy;

public static class ApiCallHelper
{
    private static bool FriendErrorMessage(HttpResponseMessage response)
    {
        if (response.Headers.TryGetValues("_friend_error_message_", out var values) && values.Any())
        {
            return true;
        }

        return false;
    }

    private static async Task TryHandleFriendErrorMessage(HttpResponseMessage response, byte[] bs)
    {
        if (!FriendErrorMessage(response))
        {
            return;
        }

        await Task.CompletedTask;
    }

    private static async Task<ReturnType> TryHandleResponseData<ReturnType>(
        AppConfig appConfig,
        IJsonDataSerializer jsonDataSerializer, HttpResponseMessage response, byte[] bs)
    {
        var responseString = appConfig.AppEncoding.GetString(bs);

        if (responseString is ReturnType t)
        {
            return t;
        }

        try
        {
            var responseEntity = jsonDataSerializer.DeserializeFromString<ReturnType>(responseString);

            return responseEntity;
        }
        catch
        {
            await Task.CompletedTask;
            throw new ApiResponseSerializerException(responseString, typeof(ReturnType));
        }
    }

    private static async Task<byte[]> ReadBytes(HttpResponseMessage response)
    {
        var maxSize = 1024 * 1024 * 2;

        if (response.Content.Headers.ContentLength != null)
        {
            if (response.Content.Headers.ContentLength.Value > maxSize)
            {
                throw new UserFriendlyException("返回过大");
            }
        }

        await using var stream = await response.Content.ReadAsStreamAsync();
        using var ms = new MemoryStream();

        var buffer = new byte[1024];
        var streamSize = 0;

        while (true)
        {
            var flag = await stream.ReadAsync(buffer, 0, buffer.Length);
            if (flag <= 0)
            {
                break;
            }

            streamSize += flag;
            if (streamSize > maxSize)
            {
                throw new UserFriendlyException("读取流过大");
            }

            await ms.WriteAsync(buffer, 0, flag);
        }

        var bs = ms.ToArray();

        return bs;
    }

    private static IEnumerable<InterceptorWrapper> ResolveInterceptor<ClientType>(IServiceProvider serviceProvider)
    {
        //所有拦截器
        var allInterceptors = serviceProvider.ResolveAllRequestInterceptor().ToArray();
        //客户端拦截器
        var clientInterceptors = allInterceptors.Where(x => x.IsInterceptorFor<ClientType>()).ToArray();
        //全局拦截器
        var globalInterceptors = allInterceptors.Where(x => x.IsGlobalInterceptor()).ToArray();

        var requestMessageInterceptors = globalInterceptors.Concat(clientInterceptors).OrderBy(x => x.Order).ToArray();

        return requestMessageInterceptors;
    }

    public static async Task<ReturnType> MakeApiRequest<ClientType, ReturnType>(
        IServiceProvider serviceProvider,
        AppConfig appConfig,
        IJsonDataSerializer jsonDataSerializer,
        ApiCallDescriptor descriptor)
    {
        //所有拦截器
        var requestMessageInterceptors = ResolveInterceptor<ClientType>(serviceProvider).ToArray();

        var httpMessageBuilder = serviceProvider.GetRequiredService<HttpMessageBuilder>();
        using var message = httpMessageBuilder.BuildMessage(descriptor);

        foreach (var handler in requestMessageInterceptors)
        {
            handler.RequestInterceptor.InterceptBeforeRequest(message);
        }

        var client = serviceProvider.GetRequiredService<IHttpClientFactory>()
            .CreateClient(typeof(ClientType).FullName);

        using var response = await client.SendAsync(message, cancellationToken: descriptor.CancellationToken);

        foreach (var handler in requestMessageInterceptors)
        {
            handler.RequestInterceptor.InterceptAfterRequest(response);
        }
        //----------------------

        var bs = await ReadBytes(response);

        await TryHandleFriendErrorMessage(response, bs);

        var data = await TryHandleResponseData<ReturnType>(appConfig, jsonDataSerializer, response, bs);

        return data;
    }

    public static void ParseParameters(IDictionary<ParameterDefinition, object> args,
        out IDictionary<ParameterDefinition, object> paramArgs,
        out CancellationToken? token)
    {
        paramArgs = new Dictionary<ParameterDefinition, object>();
        token = null;

        var tokenList = new List<CancellationToken>();

        foreach (var m in args)
        {
            var t = m.Value?.GetType();

            (t == typeof(CancellationToken?)).Should().BeFalse("cancellation token should not be nullable");

            if (m.Value != null && m.Value is CancellationToken tk)
            {
                tokenList.Add(tk);
            }
            else
            {
                paramArgs[m.Key] = m.Value;
            }
        }

        if (tokenList.Any())
        {
            (tokenList.Count > 1).Should().BeFalse("cancellation token");
            token = tokenList.FirstOrDefault();
        }
    }
}