using XCloud.Application.Gis;
using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/store")]
public class StoreController : ShopBaseController
{
    private readonly IStoreService _storeService;

    public StoreController(IStoreService storeService)
    {
        _storeService = storeService;
    }

    [HttpPost("list-stores")]
    public async Task<ApiResponse<StoreDto[]>> ListStoresAsync()
    {
        var stores = await this._storeService.ListAllStoresAsync(openingOnly: true, maxCount: 50);

        var location = this.HttpContext.GetClientGeoLocationOrNull(this.JsonDataSerializer, this.Logger);

        if (location != null && location.IsValid)
        {
            foreach (var m in stores)
            {
                m.Distance = location.TryGetDistanceInMeter(m.GetGeoLocation(), out var error);
                if (!string.IsNullOrWhiteSpace(error))
                {
                    m.Distance = null;
                }
            }
        }

        stores = stores.OrderBy(x => x.Distance ?? double.MaxValue).ToArray();

        return new ApiResponse<StoreDto[]>(stores);
    }

    [HttpPost("most-nearby-store")]
    public async Task<ApiResponse<StoreDto>> MostNearbyStoreAsync()
    {
        var response = await this.ListStoresAsync();

        var store = response?.Data?.FirstOrDefault();

        return new ApiResponse<StoreDto>(store);
    }

    [HttpPost("distance-to-store")]
    public async Task<ApiResponse<double?>> GetDistanceToStoreAsync([FromBody] GeoLocationDto dto)
    {
        if (!dto.IsValid)
        {
            return new ApiResponse<double?>().SetError("invalid location");
        }

        var storeId = await this.CurrentStoreContext.GetStoreIdOrEmptyAsync();

        if (!string.IsNullOrWhiteSpace(storeId))
        {
            var store = await this._storeService.GetRequiredByIdAsync(storeId);

            var distance = store.GetGeoLocation().TryGetDistanceInMeter(dto, out var error);

            if (!string.IsNullOrWhiteSpace(error))
            {
                return new ApiResponse<double?>().SetError(error);
            }

            return new ApiResponse<double?>(distance);
        }

        return new ApiResponse<double?>();
    }

    [HttpPost("current-store")]
    public async Task<ApiResponse<StoreDto>> GetCurrentStoreAsync()
    {
        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var store = await _storeService.GetRequiredByIdAsync(storeId);

        return new ApiResponse<StoreDto>(store);
    }
}