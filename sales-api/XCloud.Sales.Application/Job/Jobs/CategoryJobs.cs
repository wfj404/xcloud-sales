﻿using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Job.Jobs;

[UnitOfWork]
public class CategoryJobs : SalesAppService
{
    private readonly ICategoryService _categoryService;

    public CategoryJobs(ICategoryService categoryService)
    {
        _categoryService = categoryService;
    }

    public virtual async Task TryFixCategoryTreeAsync()
    {
        await _categoryService.TryFixTreeAsync();
    }
}