﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;
using Volo.Abp.Authorization;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Qingdao.Authentication;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Domain.Invoices;
using XCloud.Sales.Qingdao.Service;
using XCloud.Sales.Qingdao.Service.Invoices;

namespace XCloud.Sales.Qingdao.Controllers.Sh3h;

[StoreAuditLog]
[Route("api/mall-3h/invoice-request")]
public class McsInvoiceRequestController : ShopBaseController
{
    private readonly IMcsInvoiceRequestService _mcsInvoiceRequestService;
    private readonly Sh3HClient _sh3HClient;
    private readonly McsClient _mcsClient;
    private readonly IUserInvoiceService _userInvoiceService;

    public McsInvoiceRequestController(IMcsInvoiceRequestService mcsInvoiceRequestService,
        Sh3HClient sh3HClient,
        McsClient mcsClient,
        IUserInvoiceService userInvoiceService)
    {
        _mcsInvoiceRequestService = mcsInvoiceRequestService;
        _sh3HClient = sh3HClient;
        _mcsClient = mcsClient;
        _userInvoiceService = userInvoiceService;
    }

    [NonAction]
    private async Task<McsUserDto> GetRequiredMcsUserAsync()
    {
        if (!this.WebHelper.RequiredHttpContext.IsMcsToken())
            throw new AbpAuthorizationException(message: $"mcs token required");

        var token = this.WebHelper.RequiredHttpContext.GetBearerToken();
        if (string.IsNullOrWhiteSpace(token))
            throw new AbpAuthorizationException(message: "token is required");

        var config = this.Configuration.GetRequired3HConfig();

        var user = await this._sh3HClient.GetMcsUserAsync(config, token);

        if (user == null)
            throw new AbpAuthorizationException(message: "admin is not authed");

        return user;
    }

    [HttpPost("query-mcs-biz-order-for-invoicing")]
    public async Task<PagedResponse<McsBizOrderDto>> QueryMcsBizOrderForInvoicingAsync(
        [FromBody] QueryMcsBizOrderForInvoicingInput dto)
    {
        var mcsUser = await this.GetRequiredMcsUserAsync();

        dto.QueryType = "02";
        dto.QueryValue = mcsUser.mcsUserId;
        dto.Status ??= "0";

        if (dto.EndTime == null || dto.StartTime == null)
        {
            dto.EndTime = this.Clock.Now.AddHours(8);
            dto.StartTime = dto.EndTime.Value.AddYears(-1);
        }

        var datalist = await this._mcsClient.QueryMcsBizOrderForInvoicingAsync(dto);

        return new PagedResponse<McsBizOrderDto>()
        {
            Items = datalist
        };
    }

    [HttpPost("create")]
    public async Task<ApiResponse<McsInvoiceRequest[]>> CreateInvoiceRequestAsync(
        [FromBody] CreateInvoiceRequestInput input)
    {
        if (ValidateHelper.IsEmptyCollection(input.bizIds))
            throw new ArgumentNullException(nameof(input.bizIds));

        if (string.IsNullOrWhiteSpace(input.userInvoiceId))
            throw new ArgumentNullException(nameof(input.userInvoiceId));

        var loginUser = await this.UserAuthService.GetRequiredAuthedUserAsync();
        var mcsUser = await this.GetRequiredMcsUserAsync();

        await this._mcsInvoiceRequestService.EnsureMcsBizOrderAsync(input.bizIds);

        var invoice = await this._userInvoiceService.EnsureUserInvoiceAsync(input.userInvoiceId, loginUser.Id);

        var requests = await this._mcsClient.BuildInvoiceRequestDtoAsync(input.bizIds);

        var responseList = new List<McsInvoiceRequest>();

        foreach (var dto in requests)
        {
            dto.McsUserId = mcsUser.mcsUserId;
            dto.UserInvoice = invoice;
            dto.UserInvoiceId = invoice.Id;
            dto.UserInvoiceData = this.JsonDataSerializer.SerializeToString(invoice);

            var entity = await this._mcsInvoiceRequestService.CreateInvoiceRequestAsync(dto);
            responseList.Add(entity);

            var bizIds = dto.Items.Select(x => x.McsBizOrderId).ToArray();
            var result = await this._mcsClient.NotifyMcsToUpdateInvoicingStatusAsync(bizIds);
            if (result.fail > 0 || result.bills == null || result.bills.Any(x => x.AvailableForInvoicing))
            {
                //rollback
                throw new UserFriendlyException(message: "回写mcs业务单存在失败项目");
            }
        }

        return new ApiResponse<McsInvoiceRequest[]>(responseList.ToArray());
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteByIdAsync([FromBody] IdDto dto)
    {
        var mcsUser = await this.GetRequiredMcsUserAsync();

        await this._mcsInvoiceRequestService.EnsureUserInvoiceRequestAsync(mcsUser.mcsUserId, dto.Id);

        await this._mcsInvoiceRequestService.SoftDeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("query-paging")]
    public async Task<PagedResponse<McsInvoiceRequestDto>> QueryPagingAsync([FromBody] QueryMcsInvoiceRequestInput dto)
    {
        var mcsUser = await this.GetRequiredMcsUserAsync();

        dto.McsUserId = mcsUser.mcsUserId;

        var data = await this._mcsInvoiceRequestService.QueryPagingAsync(dto);

        await this._mcsClient.AttachMcsUsersAsync(data.Items.ToArray());

        await this._mcsInvoiceRequestService.AttachUserInvoiceAsync(data.Items.ToArray());

        await this._mcsInvoiceRequestService.AttachItemsAsync(data.Items.ToArray());

        var items = data.Items.SelectMany(x => x.Items).ToArray();

        await this._mcsClient.AttachMcsBizOrdersAsync(items);

        return data;
    }
}