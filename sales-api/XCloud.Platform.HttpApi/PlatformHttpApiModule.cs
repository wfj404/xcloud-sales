﻿global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Threading.Tasks;
global using Microsoft.AspNetCore.Mvc;
global using Volo.Abp.Domain.Entities;
global using XCloud.Core.Extension;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.AspNetMvc.Extension;
using XCloud.Platform.Application;
using XCloud.Platform.Messenger;

namespace XCloud.Platform.HttpApi;

[DependsOn(
    typeof(PlatformApplicationModule),
    typeof(PlatformMessengerModule),
    typeof(PlatformApplicationModule))]
[MethodMetric]
public class PlatformHttpApiModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.AddMvcPart<PlatformHttpApiModule>();
    }
}