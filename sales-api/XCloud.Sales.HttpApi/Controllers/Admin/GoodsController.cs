﻿using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Search;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/goods")]
public class GoodsController : ShopBaseController
{
    private readonly IGoodsService _goodsService;
    private readonly IGoodsSearchService _goodsSearchService;
    private readonly IGoodsPictureService _goodsPictureService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly ISpecValueCombinationService _specValueCombinationService;

    public GoodsController(IGoodsService goodsService,
        IGoodsSearchService goodsSearchService, IGoodsPictureService goodsPictureService,
        IStoreGoodsMappingService storeGoodsMappingService,
        ISpecValueCombinationService specValueCombinationService)
    {
        _goodsService = goodsService;
        _goodsSearchService = goodsSearchService;
        _goodsPictureService = goodsPictureService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _specValueCombinationService = specValueCombinationService;
    }

    [HttpPost("query-goods-for-selection")]
    public async Task<ApiResponse<GoodsDto[]>> QueryGoodsForSelectionAsync([FromBody] QueryGoodsForSelection dto)
    {
        dto.Page = 1;
        dto.PageSize = 30;
        dto.SkipCalculateTotalCount = true;

        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var goods = await _goodsService.QueryGoodsForSelectionAsync(dto);

        return new ApiResponse<GoodsDto[]>(goods);
    }

    [HttpPost("get-by-id")]
    public async Task<ApiResponse<GoodsDto>> GetById([FromBody] IdDto<string> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var goods = await _goodsService.GetByIdAsync(dto.Id);

        if (goods == null)
            throw new EntityNotFoundException(nameof(GetById));

        var list = new[] { goods };

        await _goodsService.AttachBrandsAsync(list);
        await _goodsService.AttachCategoriesAsync(list);
        await _goodsPictureService.AttachImagesAsync(list);
        await _goodsService.AttachTagsAsync(list);
        await _goodsService.AttachSkusAsync(list);

        return new ApiResponse<GoodsDto>(goods);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<GoodsDto>> QueryGoodsPagingAsync([FromBody] QueryGoodsPaging dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        dto.PageSize = 20;

        var response = await _goodsSearchService.SearchGoodsAsync(dto);

        if (response.IsEmpty)
        {
            return new PagedResponse<GoodsDto>();
        }

        await _goodsPictureService.AttachImagesAsync(response.Items.ToArray());
        await _goodsService.AttachBrandsAsync(response.Items.ToArray());
        await _goodsService.AttachCategoriesAsync(response.Items.ToArray());
        await _goodsService.AttachSkusAsync(response.Items.ToArray());

        var skus = response.Items.SelectMany(x => x.Skus).WhereNotNull().ToArray();

        await _storeGoodsMappingService.AttachActiveStoreMappingsWithStoreAsync(skus);
        await _goodsPictureService.AttachImagesAsync(skus);
        await this._specValueCombinationService.AttachSpecCombinationErrorsAsync(skus);

        response.Items.HideDetail();

        return response;
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<Goods>> SaveGoodsAsync([FromBody] GoodsDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var res = default(Goods);

        if (!dto.IsEmptyId())
        {
            res = await _goodsService.UpdateAsync(dto);
        }
        else
        {
            res = await _goodsService.InsertAsync(dto);

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        if (dto.Tags != null)
        {
            await _goodsService.SetTagsAsync(new SetGoodsTagsInput
            {
                Id = res.Id,
                TagIds = dto.Tags.Ids()
            });
        }

        if (dto.GoodsPictures != null)
        {
            await this._goodsPictureService.SaveGoodsImagesAsync(res.Id, dto.GoodsPictures);
        }

        await SalesEventBusService.NotifyRefreshGoodsInfo(res);

        return new ApiResponse<Goods>(res);
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        await _goodsService.DeleteByIdAsync(dto);

        await SalesEventBusService.NotifyRefreshGoodsInfo(new Goods() { Id = dto.Id });

        return new ApiResponse<object>();
    }
}