﻿using System.Text;
using Newtonsoft.Json;
using XCloud.Core.Configuration;
using XCloud.Core.Helper;

namespace XCloud.Core.Json.NewtonsoftJson;

public class NewtonsoftJsonDataSerializer : IJsonDataSerializer
{
    private readonly JsonSerializerSettings _jsonSerializerSettings;

    public NewtonsoftJsonDataSerializer(
        AppConfig appConfig,
        INewtonsoftJsonOptionAccessor newtonsoftJsonOptionAccessor)
    {
        JsonEncoding = appConfig.AppEncoding;
        _jsonSerializerSettings = newtonsoftJsonOptionAccessor.SerializerSettings;
    }

    private Encoding JsonEncoding { get; }

    public byte[] SerializeToBytes(object item)
    {
        try
        {
            var jsonString = SerializeToString(item);

            var res = JsonEncoding.GetBytes(jsonString);

            return res;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(SerializeToBytes), e);
        }
    }

    public T DeserializeFromBytes<T>(byte[] serializedObject)
    {
        try
        {
            if (ValidateHelper.IsEmptyCollection(serializedObject))
                throw new ArgumentNullException(nameof(serializedObject));

            var json = JsonEncoding.GetString(serializedObject);

            var res = DeserializeFromString<T>(json);

            return res;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromBytes), e);
        }
    }

    public object DeserializeFromBytes(byte[] serializedObject, Type target)
    {
        try
        {
            if (ValidateHelper.IsEmptyCollection(serializedObject))
                throw new ArgumentNullException(nameof(serializedObject));

            var json = JsonEncoding.GetString(serializedObject);

            var obj = DeserializeFromString(json, target);

            return obj;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromBytes), e);
        }
    }

    public string SerializeToString(object obj)
    {
        try
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));

            var res = JsonConvert.SerializeObject(obj,
                settings: _jsonSerializerSettings);
            return res;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(SerializeToString), e);
        }
    }

    public T DeserializeFromString<T>(string serializedObject)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(serializedObject))
                throw new ArgumentNullException(nameof(serializedObject));

            var res = JsonConvert.DeserializeObject<T>(serializedObject,
                settings: _jsonSerializerSettings);
            return res;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromString), e);
        }
    }

    public object DeserializeFromString(string serializedObject, Type target)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(serializedObject))
                throw new ArgumentNullException(nameof(serializedObject));

            var res = JsonConvert.DeserializeObject(serializedObject, type: target,
                settings: _jsonSerializerSettings);
            return res;
        }
        catch (Exception e)
        {
            throw new SerializeException(nameof(DeserializeFromString), e);
        }
    }
}