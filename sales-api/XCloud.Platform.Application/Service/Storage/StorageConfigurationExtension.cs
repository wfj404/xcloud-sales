using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Volo.Abp.BlobStoring;
using Volo.Abp.BlobStoring.FileSystem;
using Volo.Abp.Modularity;
using XCloud.Core.Exceptions;
using XCloud.Platform.Application.Core;
using XCloud.Platform.Application.Service.Storage.AbpFileSystem;
using XCloud.Platform.Application.Service.Storage.Database;

namespace XCloud.Platform.Application.Service.Storage;

public static class StorageConfigurationExtension
{
    internal static void ConfigStorageAll(this ServiceConfigurationContext context)
    {
        context.ConfigDefaultStorage();

        context.Services.RemoveAll<IBlobFilePathCalculator>();
        context.Services.AddSingleton<IBlobFilePathCalculator, FilePathCalculator>();
    }

    [NotNull]
    public static StorageOption GetStorageOption(this IConfiguration configuration)
    {
        var section = configuration.GetRequiredSection("Storage");

        var option = new StorageOption();

        section.Bind(option);

        return option;
    }

    [NotNull]
    public static string GetDefaultStorageProvider(this IConfiguration configuration)
    {
        var storageProvider = configuration.GetStorageOption().Provider;

        if (string.IsNullOrWhiteSpace(storageProvider))
            return StorageProviders.DatabaseBlob;

        return storageProvider;
    }

    public static bool IsThumborEnabled(this IConfiguration configuration)
    {
        return configuration.GetStorageOption().Thumbor.Enabled;
    }

    private static void ConfigDefaultStorage(this ServiceConfigurationContext context)
    {
        var configuration = context.Services.GetConfiguration();

        var providerName = configuration.GetDefaultStorageProvider();

        context.Services.Configure<AbpBlobStoringOptions>(option =>
        {
            //config default storage provider
            option.Containers.ConfigureDefault(config =>
            {
                if (providerName == StorageProviders.DatabaseBlob)
                {
                    config.UseDatabaseStorageProvider();
                }
                else if (providerName == StorageProviders.AbpFsBlob)
                {
                    var savePath = context.GetOrCreateSavePath();
                    config.UseFileSystem(f =>
                    {
                        f.AppendContainerNameToBasePath = false;
                        f.BasePath = savePath;
                    });
                }
                else
                {
                    throw new ConfigException("unsupported storage provider name");
                }
            });
        });
    }
}