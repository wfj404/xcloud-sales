import { AppListProps } from '@ant-design/pro-components';
import admin from '@/assets/app/admin.svg';
import worker from '@/assets/app/worker.svg';

const data: AppListProps = [
  {
    title: '集团管理',
    desc: '终极管理员使用',
    url: '/sales/admin/',
    target: '_blank',
    icon: admin,
  },
  {
    title: '二级单位管理',
    desc: '门店管理员使用',
    url: '/sales/store-manage/',
    target: '_blank',
    icon: worker,
  },
];

export default data;
