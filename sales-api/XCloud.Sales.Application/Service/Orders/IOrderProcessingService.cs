using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Users;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderProcessingService : ISalesAppService
{
    Task DangerouslyUpdateStatusAsync(UpdateOrderStatusInput dto);

    Task CloseAsync(CloseOrderInput input);

    Task FinishAsync(FinishOrderInput input);

    Task MarkAsProcessingAsync(string orderId);
}

public class OrderProcessingService : SalesAppService, IOrderProcessingService
{
    private readonly OrderUtils _orderUtils;
    private readonly IStoreUserPointService _userPointService;
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly IOrderStockQuantityAdjustService _orderStockQuantityAdjustService;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderNoteService _orderNoteService;

    public OrderProcessingService(OrderUtils orderUtils,
        IStoreUserPointService userPointService,
        ISalesRepository<Order> orderRepository,
        IOrderStockQuantityAdjustService orderStockQuantityAdjustService,
        IOrderPaymentBillService orderPaymentBillService,
        IOrderNoteService orderNoteService)
    {
        _orderUtils = orderUtils;
        _orderRepository = orderRepository;
        _orderStockQuantityAdjustService = orderStockQuantityAdjustService;
        _orderPaymentBillService = orderPaymentBillService;
        _orderNoteService = orderNoteService;
        _userPointService = userPointService;
    }

    public virtual async Task FinishAsync(FinishOrderInput input)
    {
        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, input.OrderId);

        if (!order.IsPaid())
            throw new UserFriendlyException("payment is required");

        if (!order.IsShipped())
            throw new UserFriendlyException("shipping is not finished");

        var now = this.Clock.Now;

        order.OrderStatusId = SalesConstants.OrderStatus.Finished;
        order.CompleteTime = now;
        order.LastModificationTime = now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await this._orderStockQuantityAdjustService.DeductOrderInventoryAsync(order.Id);

        await _userPointService.AddPointsHistoryAsync(new PointsHistoryDto
        {
            UserId = order.UserId,
            OrderId = order.Id,
            Points = (int)order.TotalPrice,
            ActionType = (int)PointsActionType.Add,
            Message = "from order",
        });

        await _orderNoteService.InsertAsync(new OrderNoteDto()
        {
            OrderId = order.Id,
            Note = $"{this.L["order.finished.note"]}",
            DisplayToUser = true,
            CreationTime = Clock.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Finished
            }
        });
    }

    public virtual async Task DangerouslyUpdateStatusAsync(UpdateOrderStatusInput dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(DangerouslyUpdateStatusAsync));

        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, dto.Id);

        if (!string.IsNullOrWhiteSpace(dto.OrderStatus))
            order.OrderStatusId = dto.OrderStatus;

        if (!string.IsNullOrWhiteSpace(dto.PaymentStatus))
            order.PaymentStatusId = dto.PaymentStatus;

        if (!string.IsNullOrWhiteSpace(dto.DeliveryStatus))
            order.ShippingStatusId = dto.DeliveryStatus;

        order.LastModificationTime = Clock.Now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);
    }

    public async Task MarkAsProcessingAsync(string orderId)
    {
        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, orderId);

        if (!order.IsToService())
            throw new UserFriendlyException("order status error when mark as processing");

        order.OrderStatusId = SalesConstants.OrderStatus.Processing;
        order.LastModificationTime = Clock.Now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);

        await this._orderNoteService.InsertAsync(new OrderNoteDto()
        {
            OrderId = order.Id,
            Note = $"{this.L["order.processing.note"]}",
            DisplayToUser = true,
            CreationTime = Clock.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Processing
            }
        });
    }

    public virtual async Task CloseAsync(CloseOrderInput input)
    {
        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, input.OrderId);

        if (!order.IsPending())
            throw new UserFriendlyException("order is not in pending");

        if (order.IsPaid())
            throw new UserFriendlyException("order payment is not in pending");

        var bills = await this._orderPaymentBillService.GetPaidBillsAsync(order.Id);

        var paidMoney = bills.Sum(x => x.Price);

        if (paidMoney > decimal.Zero)
        {
            var reason = this.L["order.bill.refund.pending"];
            throw new UserFriendlyException(message: this.L["order.close.error.with.reason.placeholder", reason]);
        }

        order.OrderStatusId = SalesConstants.OrderStatus.Closed;
        order.LastModificationTime = Clock.Now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await this._orderStockQuantityAdjustService.ReturnBackOrderInventoryAsync(order.Id);

        await _orderNoteService.InsertAsync(new OrderNoteDto()
        {
            OrderId = order.Id,
            Note = $"{this.L["order.closed.note"]}:{input.Comment}",
            DisplayToUser = true,
            CreationTime = Clock.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Closed,
                Data = new Dictionary<string, string>()
                {
                    [nameof(input.Comment)] = input.Comment
                }
            }
        });
    }
}