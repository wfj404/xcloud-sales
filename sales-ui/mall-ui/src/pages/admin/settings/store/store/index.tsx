import { Badge, Button, Card, Col, Row, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';
import { useSnapshot } from 'umi';

import XTime from '@/components/common/time';
import { storeState, StoreType } from '@/store';
import { StoreDto } from '@/utils/swagger';
import { serializeThenDeserialize } from '@/utils/utils';

import StoreForm from './form';
import XMap from './map';
import XManager from './store_manager';

export default () => {
  const [formData, _formData] = useState<StoreDto | undefined>(undefined);

  const [storeId, _storeId] = useState<string | undefined>(undefined);

  const [selected, _selected] = useState<StoreDto | undefined>(undefined);

  const [loading, _loading] = useState(false);
  const app = useSnapshot(storeState) as StoreType;
  const datalist = serializeThenDeserialize(app.stores || []);

  const queryList = () => {
    _loading(true);
    app.queryStores().finally(() => _loading(false));
  };

  const columns: ColumnType<StoreDto>[] = [
    {
      title: '名称',
      render: (d, x: StoreDto) => (
        <div>
          <div>
            <Button
              type="link"
              onClick={() => {
                _selected(x);
              }}
            >
              {x.Name || '--'}
            </Button>
          </div>
          <Space wrap direction={'horizontal'}>
            <Badge
              status={x.Opening ? 'success' : 'default'}
              text={x.Opening ? '营业中' : '闭店中'}
            />
            {x.InvoiceSupported && <Tag>开发票</Tag>}
          </Space>
        </div>
      ),
    },
    {
      title: '描述',
      render: (d, x) => (
        <>
          <div>{x.Description || '--'}</div>
          <div>{x.Telephone || '--'}</div>
        </>
      ),
    },
    {
      title: '时间',
      render: (d, x: StoreDto) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: '200px',
      render: (d, record: StoreDto) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
            <Button
              type={'link'}
              onClick={() => {
                _storeId(record.Id || '');
              }}
            >
              成员
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <XManager
        storeId={storeId}
        hide={() => {
          _storeId(undefined);
        }}
      />
      <StoreForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title={'门店'}
        loading={loading}
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Row gutter={[10, 10]}>
          <Col span={12}>
            <Table
              rowKey={(x) => x.Id || ''}
              columns={columns}
              dataSource={datalist}
              pagination={false}
            />
          </Col>
          <Col span={12}>
            <div style={{ height: 600 }}>
              <XMap
                stores={datalist}
                onClick={(e) => {
                  _selected(e);
                }}
                selectedId={selected?.Id || undefined}
              />
            </div>
          </Col>
        </Row>
      </Card>
    </>
  );
};
