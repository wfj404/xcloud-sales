﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Data;

namespace XCloud.Core.Dto;

public interface IFinder
{
    //
}

public interface IDbTableFinder : IFinder
{
    //
}

public interface IDataContainer<T> : IEntityDto
{
    T Data { get; set; }
}

public class MultipleErrorsDto<T, ErrorType> : IDataContainer<T>, IHasExtraProperties, IEntityDto
{
    public MultipleErrorsDto()
    {
        //
    }

    private ExtraPropertyDictionary _extraProperties;

    [NotNull] public ExtraPropertyDictionary ExtraProperties => _extraProperties ??= new ExtraPropertyDictionary();

    [CanBeNull] public T Data { get; set; }

    private IList<ErrorType> _errorList;

    [NotNull]
    public IList<ErrorType> ErrorList
    {
        get { return _errorList ??= new List<ErrorType>(); }
        set => _errorList = value;
    }
}

public class IdDto<T> : IEntityDto<T>
{
    public IdDto()
    {
        //
    }

    public IdDto(T id) : this()
    {
        Id = id;
    }

    public T Id { get; set; }

    public static implicit operator IdDto<T>(T id) => new IdDto<T>() { Id = id };
}

public class IdDto : IdDto<string>
{
    public IdDto()
    {
        //
    }

    public IdDto(string id)
    {
        Id = id;
    }

    public static implicit operator IdDto(string id) => new IdDto(id);

    public static implicit operator string(IdDto dto) => dto?.Id;
}

public class TreeNode : IEntityDto
{
    public string Name { get; set; }

    public TreeNode[] Children { get; set; }
}