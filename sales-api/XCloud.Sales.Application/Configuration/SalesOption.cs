using Volo.Abp.Application.Dtos;

namespace XCloud.Sales.Application.Configuration;

public class SalesOption : IEntityDto
{
    public SalesOption()
    {
        //
    }

    public bool AutoCreateDatabase { get; set; } = false;
    public bool AutoStartJob { get; set; } = true;
}