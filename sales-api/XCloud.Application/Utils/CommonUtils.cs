using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Validation;
using XCloud.Core.Extension;
using XCloud.Core.Json.NewtonsoftJson;

namespace XCloud.Application.Utils;

[ExposeServices(typeof(CommonUtils))]
public class CommonUtils : ITransientDependency
{
    private readonly IMemoryCache _memoryCache;
    private readonly INewtonsoftJsonOptionAccessor _newtonsoftJsonOptionAccessor;

    public CommonUtils(IMemoryCache memoryCache, INewtonsoftJsonOptionAccessor newtonsoftJsonOptionAccessor)
    {
        _memoryCache = memoryCache;
        _newtonsoftJsonOptionAccessor = newtonsoftJsonOptionAccessor;
    }

    public void EnsureMax(int value, int max, string errorMessage = null)
    {
        if (value > max)
        {
            var sb = new StringBuilder();
            sb.AppendLine(nameof(EnsureMax));
            sb.AppendLine($"max value:{max},but your value is {value}");
            if (!string.IsNullOrWhiteSpace(errorMessage))
            {
                sb.AppendLine(errorMessage);
            }

            throw new AbpValidationException(message: sb.ToString());
        }
    }

    public IAsyncPolicy BuildGenericHttpCallExecutionPolicy()
    {
        var breaker =
            Policy.Handle<Exception>().AdvancedCircuitBreakerAsync(
                failureThreshold: 0.1,
                samplingDuration: TimeSpan.FromSeconds(30),
                minimumThroughput: 100,
                durationOfBreak: TimeSpan.FromSeconds(10));

        var retry = Policy.Handle<TimeoutException>()
            .WaitAndRetryAsync(3, (i) => TimeSpan.FromMilliseconds(100 * i));

        var timeout = Policy.TimeoutAsync(TimeSpan.FromSeconds(3));

        return Policy.WrapAsync(breaker, retry, timeout);
    }

    private string GetTokenString(JToken token)
    {
        using var sw = new StringWriter();
        using var jw = new JsonTextWriter(sw);
        token.WriteTo(jw);
        return sw.ToString();
    }

    private JToken Sort(JToken token)
    {
        if (token == null)
            return null;

        if (token.Type == JTokenType.Array)
        {
            var items = token.ToObject<JArray>()
                //.OrderBy(x => GetTokenString(x).Length)
                .ToList();

            var arr = new JArray();

            items.ForEach(x => arr.Add(Sort(x)));

            return arr;
        }
        else if (token.Type == JTokenType.Object)
        {
            var items = token.ToObject<JObject>().Properties()
                .Where(x => x.Value.Type != JTokenType.Null)
                .Where(x => x.Value.Type != JTokenType.None)
                .OrderBy(x => x.Name)
                .ToList();

            var obj = new JObject();

            items.ForEach(x => obj.Add(x.Name, Sort(x.Value)));

            return obj;
        }
        else if (token.Type == JTokenType.Raw)
        {
            throw new NotSupportedException();
        }

        return token;
    }

    public string SerializeWithSortedKeys(object obj)
    {
        if (obj == null)
            throw new ArgumentNullException(nameof(obj));

        var settings = this._newtonsoftJsonOptionAccessor.SerializerSettings;

        var sortedToken = Sort(JToken.FromObject(obj));
        var json = JsonConvert.SerializeObject(sortedToken, settings);

        return json;
    }

    public Dictionary<string, string> ObjectToStringKeyValue(object obj)
    {
        if (obj == null)
            throw new ArgumentNullException(nameof(obj));

        var dict = new Dictionary<string, string>();

        var notSupportedType = new JTokenType[]
        {
            JTokenType.None,
            JTokenType.Array,
            JTokenType.Object,
        };

        var properties = JObject.FromObject(obj).Properties().ToArray();
        foreach (var p in properties)
        {
            if (notSupportedType.Contains(p.Value.Type))
                throw new NotSupportedException($"not supported property type:{p.Value.GetType().FullName}");

            dict[p.Name] = p.Value.Value<string>();
        }

        return dict;
    }

    public string GetTypeIdentityName<T>() where T : class
    {
        var t = typeof(T);

        var name = _memoryCache.GetOrCreate(t, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);

            var attr = t.GetCustomAttributesList<TypeIdentityNameAttribute>().FirstOrDefault();

            return attr?.Name ?? t.FullName;
        });

        return name;
    }
}