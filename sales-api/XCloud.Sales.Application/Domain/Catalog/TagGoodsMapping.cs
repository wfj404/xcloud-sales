﻿namespace XCloud.Sales.Application.Domain.Catalog;

public class TagGoodsMapping : SalesBaseEntity
{
    public string TagId { get; set; }
    public string GoodsId { get; set; }
}