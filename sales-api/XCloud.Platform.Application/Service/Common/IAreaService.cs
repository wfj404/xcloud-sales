﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Common;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Storage;

namespace XCloud.Platform.Application.Service.Common;

public interface IAreaService : IXCloudAppService
{
    Task AdjustParentIdAsync(string id, string parentId);

    Task<AreaDto[]> GetAllChildrenAsync(string catId);

    Task<AreaDto[]> QueryNodePathAsync(Area category);

    Task UpdateNodePathAsync(string id);

    Task<Area> InsertAsync(AreaDto dto);

    Task<Area> UpdateAsync(AreaDto dto);

    [NotNull]
    [ItemCanBeNull]
    Task<AreaDto> GetByIdAsync([NotNull] string id);

    Task DeleteByIdAsync([NotNull] string id);

    Task<AreaDto[]> GetAllAsync();

    Task<AreaDto[]> AttachPictureMetaAsync(AreaDto[] data);

    Task<AreaDto[]> AttachNodePathAsync(AreaDto[] data);

    Task<AreaDto[]> GetByParentIdAsync([CanBeNull] string parentId = default);
}

public class AreaService : PlatformAppService, IAreaService
{
    private readonly IPlatformRepository<Area> _repository;

    public AreaService(IPlatformRepository<Area> repository)
    {
        _repository = repository;
    }

    public async Task<AreaDto[]> QueryNodePathAsync(Area category)
    {
        var nodePath = await this._repository.GetNodePathAsync(category);

        return nodePath.MapArrayTo<Area, AreaDto>(this.ObjectMapper);
    }

    public async Task UpdateNodePathAsync(string id)
    {
        var entity = await this._repository.GetRequiredByIdAsync(id);

        var nodePath = await this.QueryNodePathAsync(entity);

        entity.NodePathJson = this.JsonDataSerializer.SerializeToString(nodePath.Ids());

        await this._repository.UpdateAsync(entity);
    }

    private async Task<bool> CheckNameExistAsync(string name, string exceptId = default)
    {
        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<Area>().AsNoTracking();

        if (!string.IsNullOrWhiteSpace(exceptId))
        {
            query = query.Where(x => x.Id != exceptId);
        }

        return await query.AnyAsync(x => x.Name == name);
    }

    private string NormalizeName(string name) => name?.Trim();

    public async Task<Area> InsertAsync(AreaDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
        {
            throw new ArgumentNullException(nameof(dto.Name));
        }

        dto.Name = NormalizeName(dto.Name);

        if (await this.CheckNameExistAsync(dto.Name))
        {
            throw new UserFriendlyException(message: "name exist");
        }

        var entity = dto.MapTo<AreaDto, Area>(this.ObjectMapper);

        entity.Id = this.GuidGenerator.CreateGuidString();
        entity.CreationTime = this.Clock.Now;

        var all = await this.GetAllAsync();
        all.Append(entity).EnsureTreeNodes();

        await this._repository.InsertAsync(entity);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await this.UpdateNodePathAsync(entity.Id);

        return entity;
    }

    public async Task<Area> UpdateAsync(AreaDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        if (string.IsNullOrWhiteSpace(dto.Name))
        {
            throw new ArgumentNullException(nameof(dto.Name));
        }

        dto.Name = NormalizeName(dto.Name);

        if (await this.CheckNameExistAsync(dto.Name, entity.Id))
        {
            throw new UserFriendlyException(message: "name exist");
        }

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.MetaId = dto.MetaId;
        entity.AreaType = dto.AreaType;

        var all = await this.GetAllAsync();
        all.Where(x => x.Id != entity.Id).Append(entity).EnsureTreeNodes();

        await this._repository.UpdateAsync(entity);

        return entity;
    }

    public async Task AdjustParentIdAsync(string id, string parentId)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        if (string.IsNullOrWhiteSpace(parentId))
            throw new ArgumentNullException(nameof(parentId));

        if (id == parentId)
            throw new ArgumentException("id & parent id should not be equal");

        var allChildren = await this.GetAllChildrenAsync(id);
        var ids = allChildren.Ids().Append(id).Distinct().ToArray();

        var entity = await _repository.GetRequiredByIdAsync(id);

        if (string.IsNullOrWhiteSpace(parentId))
        {
            entity.ParentId = string.Empty;
        }
        else
        {
            if (ids.Contains(parentId))
            {
                throw new UserFriendlyException("loop reference");
            }

            var parent = await _repository.GetRequiredByIdAsync(parentId);
            entity.ParentId = parent.Id;
        }

        var all = await this.GetAllAsync();
        all.Where(x => x.Id != entity.Id).Append(entity).EnsureTreeNodes();

        await _repository.UpdateAsync(entity);

        await UpdateNodePathAsync(entity.Id);

        foreach (var m in ids)
        {
            await UpdateNodePathAsync(m);
        }
    }

    public async Task<AreaDto[]> GetAllChildrenAsync(string catId)
    {
        var datalist = await this.GetAllAsync();

        var node = datalist.FirstOrDefault(x => x.Id == catId) ?? throw new ArgumentNullException(nameof(catId));

        var children = datalist.FindNodeChildrenRecursively(node);

        return children.MapArrayTo<Area, AreaDto>(this.ObjectMapper);
    }

    public async Task<AreaDto> GetByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await this._repository.FirstOrDefaultAsync(x => x.Id == id);

        return entity?.MapTo<Area, AreaDto>(this.ObjectMapper);
    }

    public async Task DeleteByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var children = await this.GetByParentIdAsync(id);

        if (children.Any())
            throw new UserFriendlyException("children exist");

        await this._repository.DeleteAsync(x => x.Id == id);
    }

    public async Task<AreaDto[]> AttachPictureMetaAsync(AreaDto[] data)
    {
        var ids = data.Select(x => x.MetaId).WhereNotEmpty().Distinct().ToArray();

        if (ids.Any())
        {
            var db = await this._repository.GetDbContextAsync();

            var datalist = await db.Set<StorageMeta>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

            foreach (var m in data)
            {
                if (string.IsNullOrWhiteSpace(m.MetaId))
                    continue;

                m.StorageMeta = datalist.FirstOrDefault(x => x.Id == m.MetaId)
                    ?.MapTo<StorageMeta, StorageMetaDto>(this.ObjectMapper);
            }
        }

        return data;
    }

    public async Task<AreaDto[]> AttachNodePathAsync(AreaDto[] data)
    {
        if (data.Any())
        {
            var datalist = await this.GetAllAsync();

            foreach (var m in data)
            {
                m.PathList = datalist.GetNodePath(
                    idSelector: x => x.Id,
                    parentIdSelector: x => x.ParentId,
                    nodeId: m.Id);
            }
        }

        return data;
    }

    public async Task<AreaDto[]> GetAllAsync()
    {
        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<Area>().AsNoTracking();

        var data = await query.OrderBy(x => x.CreationTime).ToArrayAsync();

        return data.MapArrayTo<Area, AreaDto>(this.ObjectMapper);
    }

    public async Task<AreaDto[]> GetByParentIdAsync(string parentId)
    {
        if (string.IsNullOrWhiteSpace(parentId))
            throw new ArgumentNullException(nameof(parentId));

        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<Area>().AsNoTracking();

        if (string.IsNullOrWhiteSpace(parentId))
        {
            query = query.Where(x => x.ParentId == string.Empty || x.ParentId == null);
        }
        else
        {
            query = query.Where(x => x.ParentId == parentId);
        }

        var data = await query.OrderBy(x => x.CreationTime).ToArrayAsync();

        return data.MapArrayTo<Area, AreaDto>(this.ObjectMapper);
    }
}