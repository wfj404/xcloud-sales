using System;

namespace XCloud.Application.Utils;

[AttributeUsage(AttributeTargets.Class)]
public class TypeIdentityNameAttribute : Attribute
{
    public TypeIdentityNameAttribute(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        Name = name;
    }

    public string Name { get; }
}