import { AddComponentsMenuItems, BlockMenuItem } from '@/components/blocks/utils';
import { DndDataType } from '@/utils/biz';
import { UniqueIdentifier } from '@dnd-kit/core';

export const menu_options = AddComponentsMenuItems.map<DndDataType<BlockMenuItem>>((x) => ({
  item: x,
  id: `toolbox-item-${x.type}`,
}));

export const blocks_droppable_area_id: UniqueIdentifier = 'blocks_droppable_area_id';
