﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Messenger.Abstraction.Core;

namespace XCloud.Platform.Messenger.Abstraction.MessageHandler.Impl;

[ExposeServices(typeof(IMessageHandler))]
public class UserMessageHandler : IMessageHandler, IScopedDependency
{
    private readonly ILogger _logger;

    public UserMessageHandler(ILogger<UserMessageHandler> logger)
    {
        _logger = logger;
    }

    public string MessageType => MessageTypeConst.UserToUser;

    public int Sort => 1;

    public async Task HandleMessageFromTransportAsync(TransportMessageContext context)
    {
        var payload =
            context.MessengerServer.MessageSerializer.DeserializeFromString<SendToUserMessageBody>(context.Message.Data);

        var find = false;

        foreach (var connection in context.MessengerServer.ConnectionManager.AsReadOnlyList())
        {
            if (connection.ClientIdentity.SubjectId == payload.Receiver)
            {
                find = true;
                await connection.SendMessageToClientAsync(context.Message);
            }
        }

        if (!find)
        {
            //消息路由过来但是本地没有对应的连接
            _logger.LogInformation($"destination not found");
        }
    }

    public async Task HandleMessageFromClientAsync(ClientMessageContext context)
    {
        var payload =
            context.Connection.Server.MessageSerializer.DeserializeFromString<SendToUserMessageBody>(
                context.Message.Data);

        payload.Sender = context.Connection.ClientIdentity.SubjectId;
        payload.SendTime = default;

        await context.MessengerServer.MessageClusterRouter.RouteToUserAsync(payload.Receiver, context.Message);
    }
}