﻿using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Application.Service.SerialNos;

public class CreateSerialNoDto : IEntityDto
{
    public CreateSerialNoDto()
    {
    }

    public CreateSerialNoDto(string category)
    {
        Category = category;
    }

    public string Category { get; set; }

    public int? Step { get; set; }

    public int? MinId { get; set; }

    public static implicit operator CreateSerialNoDto(string category) =>
        new CreateSerialNoDto(category);
}