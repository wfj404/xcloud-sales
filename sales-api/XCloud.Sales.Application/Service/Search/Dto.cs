using XCloud.Application.Model;

namespace XCloud.Sales.Application.Service.Search;

public class QueryGoodsPaging : PagedRequest
{
    public string StoreId { get; set; }
    public bool? WithoutBrand { get; set; }
    public bool? WithoutCategory { get; set; }
    public string TagId { get; set; }
    public string CategoryId { get; set; }
    public string BrandId { get; set; }
    public string Keywords { get; set; }
    public bool? IsPublished { get; set; }
    public bool? AlwaysShowFunctionalGoods { get; set; }
}

public class QuerySkuPaging : PagedRequest
{
    public string StoreId { get; set; }

    public string Sku { get; set; }

    public string Keywords { get; set; }

    public bool? IsActive { get; set; }
}