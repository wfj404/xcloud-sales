﻿using Volo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.Guids;
using Volo.Abp.Modularity;

namespace XCloud.EntityFrameworkCore.MySql;

[DependsOn(
    typeof(XCloudEntityFrameworkCoreModule),
    typeof(AbpEntityFrameworkCoreMySQLModule)
)]
public class XCloudEntityFrameworkCoreMySqlModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpSequentialGuidGeneratorOptions>(option =>
        {
            //适配mysql
            option.DefaultSequentialGuidType = SequentialGuidType.SequentialAsString;
        });
    }
}