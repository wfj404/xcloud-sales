﻿using System.Linq;
using Nest;
using XCloud.Elasticsearch.Core;

namespace XCloud.Elasticsearch.Extension;

public static class ElasticsearchExtension
{
    /// <summary>
    /// 如果有错误就抛出异常
    /// </summary>
    public static T ThrowIfException<T>(this T response, string data = null) where T : IResponse
    {
        if (!response.IsValid)
        {
            var inner = response.OriginalException ??
                        new EsResponseException($"{nameof(response.OriginalException)} is empty");
            throw new EsResponseException(nameof(ThrowIfException), inner)
            {
                Data =
                {
                    ["data"] = data,
                    ["debug"] = response.DebugInformation,
                    ["error"] = response.ServerError,
                }
            };
        }

        return response;
    }

    /// <summary>
    /// 如果有错误就抛出异常
    /// </summary>
    public static T BulkResponseThrowIfException<T>(this T response, int includeErrorCount = 30) where T : BulkResponse
    {
        if (response.ItemsWithErrors.Any())
        {
            var items = response.ItemsWithErrors.Take(includeErrorCount).ToArray();
            throw new EsBulkException($"部分数据未能成功更新到ES")
            {
                Data =
                {
                    ["items"] = items
                }
            };
        }

        return response;
    }

    public static bool IsValidLatitude(this double lat) => GeoLocation.IsValidLatitude(lat);

    public static bool IsValidLongitude(this double lng) => GeoLocation.IsValidLongitude(lng);

    /// <summary>
    /// 唯一ID
    /// </summary>
    public static DocumentPath<T> Id<T>(this IElasticClient client, string indexName, string uid)
        where T : class, IEsIndex
        => DocumentPath<T>.Id(uid).Index(indexName);
}