﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Notifications;

namespace XCloud.Sales.Application.Mapping.Notifications;

public class NotificationMap : SalesEntityTypeConfiguration<Notification>
{
    public override void Configure(EntityTypeBuilder<Notification> builder)
    {
        builder.ToTable("sales_notification");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.Title).HasMaxLength(100);
        builder.Property(x => x.Content).HasMaxLength(1000);

        builder.Property(x => x.UserId);
        builder.Property(x => x.Read);
        builder.Property(x => x.ReadTime);
        builder.Property(x => x.ActionType).HasMaxLength(100);
        builder.Property(x => x.Data);

        builder.Property(x => x.BusinessId).HasMaxLength(100);
        builder.Property(x => x.App).HasMaxLength(100);
        builder.Property(x => x.SenderId).HasMaxLength(100);
        builder.Property(x => x.SenderType).HasMaxLength(50);

        builder.Property(x => x.LastModificationTime);
        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}