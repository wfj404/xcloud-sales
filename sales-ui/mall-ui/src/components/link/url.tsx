import { Input, Space } from 'antd';

import { formatGoodsRedirectDto, LinkDto } from '@/utils/biz';

export default ({
  link,
  onChange,
}: {
  link: LinkDto;
  onChange: (x: LinkDto) => void;
}) => {
  if (!link) {
    return null;
  }

  return (
    <>
      <Space
        direction={'vertical'}
        style={{
          width: '100%',
        }}
      >
        <Input
          placeholder={'wx.navigateTo(some-url-scheme)'}
          addonBefore={'微信小程序'}
          value={link.WxMiniProgramUrl || ''}
          onChange={(e) => {
            let config = link;
            config = {
              ...config,
              WxMiniProgramUrl: e.target.value,
            };
            config = formatGoodsRedirectDto(config);
            onChange(config);
          }}
        />
        <Input
          addonBefore={'支付宝小程序'}
          value={link.AlipayMiniProgramUrl || ''}
          onChange={(e) => {
            let config = link;
            config = {
              ...config,
              AlipayMiniProgramUrl: e.target.value,
            };
            config = formatGoodsRedirectDto(config);
            onChange(config);
          }}
        />
        <Input
          addonBefore={'APP'}
          value={link.AppUrl || ''}
          onChange={(e) => {
            let config = link;
            config = {
              ...config,
              AppUrl: e.target.value,
            };
            config = formatGoodsRedirectDto(config);
            onChange(config);
          }}
        />
        <Input
          addonBefore={'H5'}
          value={link.H5Url || ''}
          onChange={(e) => {
            let config = link;
            config = {
              ...config,
              H5Url: e.target.value,
            };
            config = formatGoodsRedirectDto(config);
            onChange(config);
          }}
        />
      </Space>
    </>
  );
};
