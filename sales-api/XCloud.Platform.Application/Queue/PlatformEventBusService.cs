﻿using XCloud.Application.Service;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events.Distributed;
using XCloud.Application.Queue;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Queue;

public static class PlatformEventTopics
{
    public const string UserUpdated = "user.updated";
}

[ExposeServices(typeof(PlatformEventBusService))]
public class PlatformEventBusService : XCloudAppService
{
    private readonly ICapPublisher _capPublisher;

    public PlatformEventBusService(ICapPublisher capPublisher)
    {
        _capPublisher = capPublisher;
    }

    public async Task NotifyUserUpdatedAsync(User user)
    {
        if (user == null)
            throw new ArgumentNullException(nameof(user));

        await this._capPublisher.PublishEntityUpdatedAsync(PlatformEventTopics.UserUpdated, user);
    }
}

[UnitOfWork]
public class PlatformEventHandler : PlatformAppService, ICapSubscribe
{
    private readonly IUserService _userService;
    private readonly IUserMobileService _userMobileService;

    public PlatformEventHandler(IUserMobileService userMobileService, IUserService userService)
    {
        _userMobileService = userMobileService;
        _userService = userService;
    }

    [CapSubscribe(PlatformEventTopics.UserUpdated)]
    public async Task HandleUserUpdatedAsync(EntityUpdatedEto<User> args)
    {
        var dto = args.EnsureNotNull();

        await this._userService.GetByIdAsync(dto.Id);
        await _userMobileService.GetMobileByUserIdAsync(dto.Id);
    }
}