﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Validation;
using System.Linq;
using System.Reflection;
using XCloud.Core.Extension;

namespace XCloud.Application.Validation;

[ExposeServices(typeof(IObjectValidationContributor))]
public class XCloudObjectValidationContributor : IObjectValidationContributor, ITransientDependency
{
    private readonly IMemoryCache _memoryCache;

    public XCloudObjectValidationContributor(IMemoryCache memoryCache)
    {
        _memoryCache = memoryCache;
    }

    public async Task AddErrorsAsync(ObjectValidationContext context)
    {
        var obj = context.ValidatingObject;

        if (obj == null)
        {
            context.Errors.Add(new ValidationResult($"object is null"));
            return;
        }

        var type = obj.GetType();

        var properties = this._memoryCache.GetOrCreate(type, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1);
            return GetProperties(type);
        });

        foreach (var m in properties)
        {
            var value = m.Key.GetValue(obj);

            foreach (var validator in m.Value)
            {
                if (!validator.IsValid(value))
                {
                    //list.Add(validator.ErrorMessage);
                    context.Errors.Add(new ValidationResult(
                        errorMessage: validator.ErrorMessage,
                        memberNames: new string[] { }));
                }
            }
        }

        await Task.CompletedTask;
    }

    private KeyValuePair<PropertyInfo, ValidationAttribute[]>[] GetProperties(Type t)
    {
        return t
            .GetProperties()
            .Select(Build)
            .ToArray();

        KeyValuePair<PropertyInfo, ValidationAttribute[]> Build(PropertyInfo x)
        {
            var attrs = x.GetCustomAttributesList<ValidationAttribute>().ToArray();

            return new KeyValuePair<PropertyInfo, ValidationAttribute[]>(x, attrs);
        }
    }
}