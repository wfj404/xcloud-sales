﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Authorization;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Exceptions;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Qingdao.Configuration;
using XCloud.Sales.Qingdao.Service;
using XCloud.Sales.Qingdao.Service.Invoices;

namespace XCloud.Sales.Qingdao.Controllers.Sh3h;

[StoreAuditLog]
[Route("api/mall-3h/admin/invoice-request")]
public class McsInvoiceRequestAdminController : ShopBaseController
{
    private readonly IMcsInvoiceRequestService _mcsInvoiceRequestService;
    private readonly Sh3HClient _sh3HClient;
    private readonly McsClient _mcsClient;

    public McsInvoiceRequestAdminController(IMcsInvoiceRequestService mcsInvoiceRequestService,
        Sh3HClient sh3HClient,
        McsClient mcsClient)
    {
        _mcsInvoiceRequestService = mcsInvoiceRequestService;
        _sh3HClient = sh3HClient;
        _mcsClient = mcsClient;
    }

    [NonAction]
    private async Task<WapAdminDto> GetRequiredAdminAsync(string requiredPermission = default)
    {
        var token = this.WebHelper.RequiredHttpContext.GetBearerToken();
        if (string.IsNullOrWhiteSpace(token))
            throw new AbpAuthorizationException(message: "token is required");

        var config = this.Configuration.GetRequired3HConfig();

        var admin = await this._sh3HClient.GetWapAdminAsync(config, token);

        if (admin == null)
            throw new AbpAuthorizationException(message: "admin is not authed");

        if (!string.IsNullOrWhiteSpace(requiredPermission))
        {
            var permissionList = await this._sh3HClient.GetAdminPermissionsAsync(token);
            if (!permissionList.Contains(requiredPermission))
                throw new PermissionRequiredException(msg: $"permission:{requiredPermission}");
        }

        return admin;
    }

    [HttpPost("query-paging")]
    public async Task<PagedResponse<McsInvoiceRequestDto>> QueryPagingAsync(
        [FromBody] QueryMcsInvoiceRequestInput dto)
    {
        var admin = await this.GetRequiredAdminAsync();

        var data = await this._mcsInvoiceRequestService.QueryPagingAsync(dto);

        await this._mcsClient.AttachMcsUsersAsync(data.Items.ToArray());

        await this._mcsInvoiceRequestService.AttachUserInvoiceAsync(data.Items.ToArray());

        await this._mcsInvoiceRequestService.AttachItemsAsync(data.Items.ToArray());

        var items = data.Items.SelectMany(x => x.Items).ToArray();

        await this._mcsClient.AttachMcsBizOrdersAsync(items);

        return data;
    }

    [HttpPost("mark-as-success")]
    public async Task<ApiResponse<object>> MarkAsSuccessAsync([FromBody] IdDto dto)
    {
        var admin = await this.GetRequiredAdminAsync();

        await this._mcsInvoiceRequestService.MarkAsSuccessAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("mark-as-failed")]
    public async Task<ApiResponse<object>> MarkAsFailedAsync([FromBody] IdDto dto)
    {
        var admin = await this.GetRequiredAdminAsync();

        await this._mcsInvoiceRequestService.MarkAsFailedAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("mark-as-picked")]
    public async Task<ApiResponse<object>> MarkAsPickedAsync([FromBody] IdDto dto)
    {
        var admin = await this.GetRequiredAdminAsync();

        await this._mcsInvoiceRequestService.MarkAsPickedAsync(dto.Id);

        return new ApiResponse<object>();
    }
}