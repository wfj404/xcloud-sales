﻿using System.Net;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Http;

namespace XCloud.Core.Dto;

public class ErrorInfo : RemoteServiceErrorInfo, IEntityDto
{
    public ErrorInfo()
    {
        //
    }

    public ErrorInfo(Exception e)
    {
        this.Message = e.Message;
        this.Details = e.StackTrace;
        this.Data = e.Data;
    }

    public object this[string key]
    {
        get => this.Data?[key];
        set
        {
            this.Data ??= new Dictionary<string, object>();
            this.Data[key] = value;
        }
    }

    public HttpStatusCode? HttpStatusCode { get; set; }

    public static implicit operator ErrorInfo(string message) => new ErrorInfo() { Message = message };

    public static implicit operator string(ErrorInfo error) => error?.Message;
}