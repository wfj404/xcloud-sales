﻿using FreeSql;
using Volo.Abp.Domain.Entities;

namespace XCloud.FreeSql;

public interface IFreeSqlStringKeyRepository<T> : IBaseRepository<T, string> where T : class, IEntity<string>
{
    //
}

public class FreeSqlStringKeyRepository<T> : DefaultRepository<T, string>, IFreeSqlStringKeyRepository<T>
    where T : class, IEntity<string>
{
    public FreeSqlStringKeyRepository(UnitOfWorkManager unitOfWorkManager) : base(unitOfWorkManager.Orm,
        unitOfWorkManager)
    {
        //
    }
}