using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.ShoppingCart;

namespace XCloud.Sales.Application.Mapping.ShoppingCart;

public class ShoppingCartValueAddedItemMap : SalesEntityTypeConfiguration<ShoppingCartValueAddedItem>
{
    public override void Configure(EntityTypeBuilder<ShoppingCartValueAddedItem> builder)
    {
        builder.ToTable("sales_shopping_cart_value_added_item");

        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.ShoppingCartItemId);
        builder.Property(x => x.ValueAddedItemId);

        base.Configure(builder);
    }
}