﻿using System.Reflection;
using Microsoft.AspNetCore.Mvc.Controllers;

namespace XCloud.AspNetMvc.DynamicApi;

public class DynamicApiControllerFeatureProvider : ControllerFeatureProvider
{
    protected override bool IsController(TypeInfo typeInfo)
    {
        return DynamicApiControllerHelper.IsDynamicApiController(typeInfo);
    }
}