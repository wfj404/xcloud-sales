﻿using XCloud.Application.DistributedLock;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores.Context.Contributors;

[ExposeServices(typeof(GetOrCreateDefaultStoreSelectorContributor))]
public class GetOrCreateDefaultStoreSelectorContributor : SalesAppService, ICurrentStoreSelectorContributor
{
    private readonly ISalesRepository<Store> _repository;
    private readonly IStoreService _storeService;

    public GetOrCreateDefaultStoreSelectorContributor(ISalesRepository<Store> repository, IStoreService storeService)
    {
        _repository = repository;
        _storeService = storeService;
    }

    private async Task<Store> GetFirstStoreAsync()
    {
        var db = await _repository.GetDbContextAsync();

        var query = db.Set<Store>().AsNoTracking().IgnoreQueryFilters();

        var store = await query
            .OrderBy(x => x.CreationTime)
            .ThenBy(x => x.Id)
            .FirstOrDefaultAsync();

        return store;
    }

    private async Task<Store> GetOrCreateDefaultStoreAsync()
    {
        var store = await GetFirstStoreAsync();

        if (store == null)
        {
            await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
                resource: $"{nameof(GetOrCreateDefaultStoreSelectorContributor)}.{nameof(GetOrCreateDefaultStoreAsync)}",
                expiryTime: TimeSpan.FromSeconds(5));

            store = await GetFirstStoreAsync();

            if (store == null)
            {
                store = await _storeService.InsertAsync(new StoreDto
                {
                    Id = default,
                    Name = "default store",
                    Description = "created by system",
                    AddressDetail = "jiangsu province...",
                    Telephone = "400-123456-90",
                    InvoiceSupported = true,
                    ExternalId = string.Empty,
                    Opening = true,
                    IsDeleted = false,
                });

                await RequiredCurrentUnitOfWork.SaveChangesAsync();
            }
        }

        return store;
    }

    public async Task<string> GetStoreIdOrEmptyAsync()
    {
        var key = $"default.first.store.id@{nameof(GetOrCreateDefaultStoreSelectorContributor)}";

        var dto = await CacheProvider.GetOrSetAsync(
            GetOrCreateDefaultStoreAsync,
            new CacheOption<Store>(key, TimeSpan.FromSeconds(60)));

        return dto?.Id;
    }
}