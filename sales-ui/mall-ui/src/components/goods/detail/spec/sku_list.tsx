import { sortBy } from 'lodash-es';
import React, { useEffect } from 'react';

import XSkuPrice from '@/components/goods/sku/price';
import { skuHasStock } from '@/utils/biz';
import { parseAsDayjs } from '@/utils/dayjs';
import { SkuDto, SpecValueDto } from '@/utils/swagger';
import { errorMsg, isStringEmpty } from '@/utils/utils';
import { Alert, Box, Paper, Typography } from '@mui/material';

import { get_spec_combination_description } from './utils';

export default function({
                          skus_list,
                          skuId,
                          onSkuSelect,
                          loading,
                          spec_values,
                        }: {
  skus_list: SkuDto[];
  skuId?: string;
  onSkuSelect: (d?: SkuDto) => void;
  loading?: boolean;
  spec_values?: SpecValueDto[];
}) {
  const available_skus: SkuDto[] = skus_list.filter((x) => skuHasStock(x));

  const selected_sku = available_skus.find((x) => x.Id === skuId);

  const renderSkuItem = (x: SkuDto) => {
    const available = skuHasStock(x);
    const selected = available && x.Id === skuId;

    const spec_desc = get_spec_combination_description(
      x.SpecItemList || [],
      spec_values || [],
    );

    return (
      <Paper
        elevation={0}
        sx={{
          border: (theme) =>
            selected
              ? `2px solid ${theme.palette.grey[700]}`
              : `2px solid ${theme.palette.grey[200]}`,
          borderRadius: '10px',
          cursor: 'pointer',
          overflow: 'hidden',
          boxShadow: 'none',
        }}
        onClick={() => {
          if (available) {
            onSkuSelect(x);
          } else {
            errorMsg('库存不足');
          }
        }}
      >
        <Box
          sx={{
            p: 1,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <Box sx={{ width: '100%' }}>
            <Typography variant='h5'>{x.Name}</Typography>
            {isStringEmpty(x.Description) || (
              <Typography variant='overline' color='text.disabled'>
                {x.Description}
              </Typography>
            )}
            {isStringEmpty(spec_desc) || (
              <Typography
                variant='caption'
                color='text.disabled'
                display='block'
              >
                {spec_desc}
              </Typography>
            )}
            {available || (
              <Typography
                variant='caption'
                color='text.disabled'
                display='block'
              >
                无库存
              </Typography>
            )}
          </Box>
          <Box
            sx={{
              width: '150px',
            }}
          >
            <XSkuPrice model={x} />
          </Box>
        </Box>
      </Paper>
    );
  };

  useEffect(() => {
    if (selected_sku != undefined) {
      return;
    }
    const deft_sku = available_skus.at(0);

    if (deft_sku == undefined || deft_sku.Id == skuId) {
      return;
    }

    onSkuSelect(deft_sku);
  }, [available_skus]);

  if (loading) {
  } else {
    if (skus_list.length <= 0) {
      return <Alert severity='info'>暂无可售</Alert>;
    }
  }

  const sorted_skus = sortBy(
    skus_list,
    (x) => (skuHasStock(x) ? 0 : 1),
    (x) => parseAsDayjs(x.CreationTime)?.unix() || 0,
  );

  return sorted_skus.map((x, i) => {
    return (
      <Box sx={{ my: 0.5 }} key={i}>
        {renderSkuItem(x)}
      </Box>
    );
  });
}
