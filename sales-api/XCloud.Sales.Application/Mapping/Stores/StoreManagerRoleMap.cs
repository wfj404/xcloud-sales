﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Mapping.Stores;

public class StoreManagerRoleMap : SalesEntityTypeConfiguration<StoreManagerRole>
{
    public override void Configure(EntityTypeBuilder<StoreManagerRole> builder)
    {
        builder.ToTable("sales_store_manager_role");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.ManagerId).RequiredId();
        builder.Property(x => x.RoleId).RequiredId();

        base.Configure(builder);
    }
}