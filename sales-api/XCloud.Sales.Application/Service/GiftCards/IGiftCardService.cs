﻿using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.GiftCards;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.GiftCards;

public interface IGiftCardService : ISalesAppService
{
    Task<GiftCardDto[]> AttachStoreUsersAsync(GiftCardDto[] data);

    Task UseGiftCardAsync(UseGiftCardInput dto);

    Task<decimal> QueryUnusedAmountTotalAsync();

    Task<GiftCardDto> QueryByIdAsync(string cardId);

    Task<GiftCard> CreateGiftCardAsync(GiftCardDto dto);

    Task UpdateStatusAsync(UpdateGiftCardStatusInput dto);

    Task<PagedResponse<GiftCardDto>> QueryPagingAsync(QueryGiftCardPagingInput dto);
}

public class GiftCardService : SalesAppService, IGiftCardService
{
    private readonly ISalesRepository<GiftCard> _salesRepository;
    private readonly IStoreUserBalanceService _userBalanceService;

    public GiftCardService(ISalesRepository<GiftCard> salesRepository,
        IStoreUserBalanceService userBalanceService)
    {
        _salesRepository = salesRepository;
        _userBalanceService = userBalanceService;
    }

    public async Task<GiftCardDto[]> AttachStoreUsersAsync(GiftCardDto[] data)
    {
        if (data == null)
            throw new ArgumentNullException(nameof(data));

        var ids = data.Select(x => x.UserId).Where(x => x > 0).Distinct().ToArray();

        if (!ids.Any())
            return data;

        var db = await this._salesRepository.GetDbContextAsync();

        var users = await db.Set<StoreUser>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            m.StoreUser = users.FirstOrDefault(x => x.Id == m.UserId)?.MapTo<StoreUser, StoreUserDto>(ObjectMapper);
        }

        return data;
    }

    public async Task UseGiftCardAsync(UseGiftCardInput dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.CardId) || dto.UserId <= 0)
            throw new ArgumentNullException(nameof(UseGiftCardAsync));

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(GiftCardService)}.{nameof(UseGiftCardAsync)}.{dto.CardId}",
            expiryTime: TimeSpan.FromSeconds(5));

        using var uow = UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

        try
        {
            var db = await _salesRepository.GetDbContextAsync();

            var now = Clock.Now;

            var card = await db.Set<GiftCard>().FirstOrDefaultAsync(x => x.Id == dto.CardId);
            if (card == null)
                throw new UserFriendlyException("card is not exist");

            if (!card.IsActive)
                throw new UserFriendlyException("gift card is inactive");

            if (card.Used)
                throw new UserFriendlyException("card is used");

            if (card.EndTime != null && card.EndTime.Value < now)
                throw new UserFriendlyException("card is expired");

            var user = await db.Set<StoreUser>().FirstOrDefaultAsync(x => x.Id == dto.UserId);
            if (user == null)
                throw new UserFriendlyException("use is not exist");

            card.UserId = user.Id;
            card.UsedTime = now;
            card.Used = true;

            await _userBalanceService.ChangeUserBalanceAsync(
                user.Id, card.Amount, BalanceActionType.Add,
                "charge from prepaid card");

            await db.TrySaveChangesAsync();

            await uow.CompleteAsync();
        }
        catch
        {
            await uow.RollbackAsync();
            throw;
        }
    }

    public async Task<decimal> QueryUnusedAmountTotalAsync()
    {
        var db = await _salesRepository.GetDbContextAsync();
        var query = db.Set<GiftCard>().AsNoTracking();

        var total = await query.Where(x => !x.Used).SumAsync(x => x.Amount);
        return total;
    }

    public async Task<PagedResponse<GiftCardDto>> QueryPagingAsync(QueryGiftCardPagingInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _salesRepository.GetDbContextAsync();
        var query = db.Set<GiftCard>().AsNoTracking();

        if (dto.Used != null)
            query = query.Where(x => x.Used == dto.Used.Value);

        if (dto.UserId != null)
            query = query.Where(x => x.UserId == dto.UserId.Value);

        var count = await query.CountOrDefaultAsync(dto);

        var items = await query
            .OrderBy(x => x.Used).ThenByDescending(x => x.CreationTime)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var dtos = items.Select(x => ObjectMapper.Map<GiftCard, GiftCardDto>(x)).ToArray();

        return new PagedResponse<GiftCardDto>(dtos, dto, count);
    }

    public async Task UpdateStatusAsync(UpdateGiftCardStatusInput dto)
    {
        if (dto == null || string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(UpdateStatusAsync));

        var db = await _salesRepository.GetDbContextAsync();

        var entity = await db.Set<GiftCard>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(UpdateStatusAsync));

        if (dto.IsActive != null)
            entity.IsActive = dto.IsActive.Value;

        if (dto.IsDeleted != null)
            entity.IsDeleted = dto.IsDeleted.Value;

        await db.TrySaveChangesAsync();
    }

    public async Task<GiftCard> CreateGiftCardAsync(GiftCardDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(CreateGiftCardAsync));
        if (dto.Amount <= 0)
            throw new ArgumentException(nameof(dto.Amount));

        var db = await _salesRepository.GetDbContextAsync();

        var entity = ObjectMapper.Map<GiftCardDto, GiftCard>(dto);
        entity.Used = false;
        entity.UserId = default;
        entity.UsedTime = null;
        entity.IsDeleted = false;
        entity.IsActive = true;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;

        db.Set<GiftCard>().Add(entity);

        await db.SaveChangesAsync();

        return entity;
    }

    public async Task<GiftCardDto> QueryByIdAsync(string cardId)
    {
        if (string.IsNullOrWhiteSpace(cardId))
            throw new ArgumentNullException(nameof(QueryByIdAsync));

        var db = await _salesRepository.GetDbContextAsync();

        var entity = await db.Set<GiftCard>().AsNoTracking().FirstOrDefaultAsync(x => x.Id == cardId);

        if (entity == null)
            return null;

        return ObjectMapper.Map<GiftCard, GiftCardDto>(entity);
    }
}