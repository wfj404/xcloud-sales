﻿namespace XCloud.Platform.Application.Domain.Storage;

/// <summary>
/// 系统文件池，只增不减，可以由迁移工具或者定时任务迁移
/// </summary>
public class StorageMeta : PlatformBaseEntity, IHasModificationTime, IHasCreationTime
{
    public StorageMeta()
    {
        //
    }

    public StorageMeta(string provider) : this()
    {
        StorageProvider = provider;
    }

    public string FileExtension { get; set; }

    public string ContentType { get; set; }

    public long ResourceSize { get; set; }

    public string ResourceKey { get; set; }

    public string ResourceHash { get; set; }

    public string HashType { get; set; }

    public string ExtraData { get; set; }

    public string StorageProvider { get; set; }

    public int ReferenceCount { get; set; }

    public int UploadTimes { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }
}