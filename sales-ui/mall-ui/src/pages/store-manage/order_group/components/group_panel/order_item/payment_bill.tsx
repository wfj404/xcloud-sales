import { Button, InputNumber, Select, Space, Table, Tag } from 'antd';
import { DefaultOptionType } from 'antd/es/select';
import { ColumnType } from 'antd/es/table';

import { PaymentChannel, PaymentChannelList } from '@/utils/biz';
import { OrderItemDto, PaymentBillDto } from '@/utils/swagger';
import { formatMoney } from '@/utils/utils';
import { PlusOutlined } from '@ant-design/icons';

export default ({
  bills,
  items,
  onChange,
}: {
  bills: PaymentBillDto[];
  items: OrderItemDto[];
  onChange: (e: PaymentBillDto[]) => void;
}) => {
  if (!bills || !onChange) {
    return <h1>error</h1>;
  }

  const setItemValue = (i: number, e: PaymentBillDto) => {
    const datalist: PaymentBillDto[] = [...bills];
    datalist[i] = e;
    onChange(datalist);
  };

  const columns: ColumnType<PaymentBillDto>[] = [
    {
      render: (d, x, i) => <Tag color="blue">{i + 1}</Tag>,
    },
    {
      title: '支付渠道',
      render: (d, x, i) => {
        return (
          <Select
            style={{ minWidth: 150 }}
            size="large"
            allowClear
            value={x.PaymentChannel}
            options={PaymentChannelList.map<DefaultOptionType>((x) => ({
              value: x.id,
              label: x.name,
            }))}
            onChange={(e) => {
              setItemValue(i, { ...x, PaymentChannel: e });
            }}
          />
        );
      },
    },
    {
      title: '金额',
      render: (d, x, i) => {
        return (
          <>
            <InputNumber
              size="large"
              addonAfter="元"
              min={0}
              status={(x.Price || 0) <= 0 ? 'error' : undefined}
              value={x.Price}
              onChange={(e) => {
                setItemValue(i, { ...x, Price: e || 0 });
              }}
            />
          </>
        );
      },
    },
    {
      render: (d, x, i) => {
        return (
          <Tag color="blue">
            <b>{formatMoney(x.Price || 0)}</b>
          </Tag>
        );
      },
    },
    {
      title: '操作',
      width: 150,
      render: (d, x, i) => {
        return (
          <>
            <Space direction="horizontal">
              <Button
                danger
                type="link"
                onClick={() => {
                  if (confirm('确定删除？')) {
                    onChange(bills.filter((_, index) => index !== i));
                  }
                }}
              >
                删除
              </Button>
            </Space>
          </>
        );
      },
    },
  ];

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <Table
          title={() => <b>账单列表</b>}
          style={{ border: `1px solid green` }}
          size="small"
          bordered
          columns={columns}
          dataSource={bills}
          pagination={false}
          footer={() => (
            <div>
              <Button
                size="small"
                icon={<PlusOutlined />}
                type="link"
                onClick={() => {
                  onChange([
                    ...bills,
                    {
                      PaymentChannel: PaymentChannel.Balance,
                      Price: undefined,
                    },
                  ]);
                }}
              >
                添加账单
              </Button>
            </div>
          )}
        />
      </Space>
    </>
  );
};
