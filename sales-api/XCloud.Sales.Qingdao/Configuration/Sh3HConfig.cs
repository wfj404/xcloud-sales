﻿using System;
using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;

namespace XCloud.Sales.Qingdao.Configuration;

public class OrgRedirection : IEntityDto
{
    public string From { get; set; }
    public string To { get; set; }
}

public class Sh3HConfig : IEntityDto
{
    public string SalesWapAppName { get; set; }

    private OrgRedirection[] _psRedirection;

    [NotNull]
    public OrgRedirection[] PsRedirection
    {
        get => this._psRedirection ??= Array.Empty<OrgRedirection>();
        set => this._psRedirection = value;
    }

    private string[] _storeOrgCodeList;

    [NotNull]
    public string[] StoreOrgCodeList
    {
        get => this._storeOrgCodeList ??= Array.Empty<string>();
        set => this._storeOrgCodeList = value;
    }

    public string PsDefaultStation { get; set; }

    public string GsDefaultStation { get; set; }

    public string GisUrl { get; set; }

    public string GisUserName { get; set; }

    public string PlatformUrl { get; set; }

    public string McsOpenApiUrl { get; set; }

    public string McsMfApiUrl { get; set; }

    public string McsBmApiUrl { get; set; }

    public string McsChannel { get; set; }

    public void Validate()
    {
        if (string.IsNullOrWhiteSpace(this.SalesWapAppName))
            throw new ArgumentNullException(nameof(this.SalesWapAppName));

        if (string.IsNullOrWhiteSpace(PlatformUrl))
            throw new ArgumentNullException(nameof(PlatformUrl));

        if (string.IsNullOrWhiteSpace(McsOpenApiUrl))
            throw new ArgumentNullException(nameof(McsOpenApiUrl));

        if (string.IsNullOrWhiteSpace(this.McsChannel))
            throw new ArgumentNullException(nameof(McsChannel));
    }
}