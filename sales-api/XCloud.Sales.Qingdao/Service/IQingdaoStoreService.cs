﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Qingdao.Utils;

namespace XCloud.Sales.Qingdao.Service;

public interface IQingdaoStoreService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<StoreDto> PrepareStoreAsync(WapOrganization organization);
}

public class QingdaoStoreService : SalesAppService, IQingdaoStoreService
{
    private readonly IStoreService _storeService;
    private readonly QingdaoUtils _qingdaoUtils;

    public QingdaoStoreService(IStoreService storeService, QingdaoUtils qingdaoUtils)
    {
        _storeService = storeService;
        _qingdaoUtils = qingdaoUtils;
    }

    public async Task<StoreDto> PrepareStoreAsync(WapOrganization organization)
    {
        var store = await this._storeService.GetOrCreateByExternalIdAsync(
            this._qingdaoUtils.BuildStoreExternalId(organization),
            organization.organizationName);

        store = await this.TryUpdateStoreInfoAsync(store, organization);

        return store;
    }

    private async Task<StoreDto> TryUpdateStoreInfoAsync(StoreDto store, WapOrganization organization)
    {
        if (store.Name != organization.organizationName && !string.IsNullOrWhiteSpace(organization.organizationName) ||
            !store.Opening ||
            !store.InvoiceSupported)
        {
            store.Name = organization.organizationName;
            store.Opening = true;
            store.InvoiceSupported = true;

            await this._storeService.UpdateAsync(store);

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return store;
    }
}