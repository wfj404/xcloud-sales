using System;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Elasticsearch.Core;
using XCloud.Sales.Application.Service;

namespace XCloud.Sales.ElasticSearch.Job;

[ExposeServices(typeof(EnsureIndexCreatedJob))]
public class EnsureIndexCreatedJob : SalesAppService, ITransientDependency
{
    private readonly ElasticSearchClient _salesElasticSearchClient;

    public EnsureIndexCreatedJob(ElasticSearchClient salesElasticSearchClient)
    {
        _salesElasticSearchClient = salesElasticSearchClient;
    }

    public virtual async Task EnsureIndexCreatedAsync()
    {
        await Task.CompletedTask;

        await _salesElasticSearchClient.Value.Cat.IndicesAsync();

        throw new NotImplementedException();
    }
}