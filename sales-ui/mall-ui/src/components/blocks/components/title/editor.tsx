import { Form, Input } from 'antd';

import XLinkEditor from '@/components/link';

import { TitleBlockProps } from './utils';

export default function({ data, onChange }: {
  data: TitleBlockProps,
  onChange: (d: TitleBlockProps) => void
}) {

  if (!data || !onChange) {
    return null;
  }

  return <>
    <Form labelCol={{ span: 3 }}>
      <Form.Item label={'标题'}>
        <Input value={data?.title} onChange={e => {
          onChange && onChange({ ...data, title: e.target.value });
        }} />
      </Form.Item>
      <Form.Item label={'更多按钮-文字'}>
        <Input value={data?.moreText} onChange={e => {
          onChange && onChange({ ...data, moreText: e.target.value });
        }} />
      </Form.Item>
      <Form.Item label={'更多按钮-点击事件'}>
        <XLinkEditor link={data.moreLink || {}} onChange={e => {
          onChange({
            ...data,
            moreLink: e,
          });
        }} />
      </Form.Item>
    </Form>
  </>;
}
