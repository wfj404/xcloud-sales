namespace XCloud.Sales.Application.Domain.Shipping;

public class DeliveryRecordTrack : SalesBaseEntity, IHasCreationTime
{
    public string DeliveryId { get; set; }

    public double Lon { get; set; }

    public double Lat { get; set; }

    public DateTime CreationTime { get; set; }
}