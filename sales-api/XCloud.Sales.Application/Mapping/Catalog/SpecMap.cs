using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class SpecMap : SalesEntityTypeConfiguration<Spec>
{
    public override void Configure(EntityTypeBuilder<Spec> builder)
    {
        builder.ToTable("sales_spec");

        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigIntPrimaryKey();

        builder.Property(pa => pa.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.GoodsId).RequiredId();

        base.Configure(builder);
    }
}