import { apiClient } from '@/utils/client';
import { GiftCardDto } from '@/utils/swagger';
import { DatePicker, Form, InputNumber, message, Modal, Spin } from 'antd';
import { useEffect, useState } from 'react';
import { dateTimeFormat, parseAsDayjs, timezoneOffset } from '@/utils/dayjs';

export default (props: {
  show: boolean;
  hide: () => void;
  data: GiftCardDto;
  ok: () => void;
}) => {
  const { show, hide, data, ok } = props;
  const [loading, _loading] = useState(false);

  const [form, _form] = useState<GiftCardDto>({});

  const save = (row: GiftCardDto) => {
    _loading(true);

    apiClient.mallAdmin
      .giftCardCreate(row)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _form({
      ...data,
    });
  }, [data]);

  return (
    <>
      <Modal open={show} onCancel={() => hide()} onOk={() => save(form)}>
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign='right'
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label='卡面金额'>
              <InputNumber value={form.Amount || 0} onChange={e => {
                _form({
                  ...form,
                  Amount: e || 0,
                });
              }} />
            </Form.Item>
            <Form.Item label='过期时间' name='EndTime'>
              <DatePicker allowClear
                          value={parseAsDayjs(form.EndTime)?.add(timezoneOffset, 'hour')}
                          onChange={e => {
                            _form({
                              ...form,
                              EndTime: e?.add(-timezoneOffset, 'hour').format(dateTimeFormat),
                            });
                          }} />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
