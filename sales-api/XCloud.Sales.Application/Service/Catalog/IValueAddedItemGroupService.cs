using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface IValueAddedItemGroupService : ISalesAppService
{
    Task<ValueAddedItemGroupDto[]> AttachItemsAsync(ValueAddedItemGroupDto[] data);

    Task<ValueAddedItemGroupDto[]> QueryByGoodsIdAsync(string goodsId);

    Task InsertAsync(ValueAddedItemGroupDto dto);

    Task UpdateAsync(ValueAddedItemGroupDto dto);

    Task DeleteAsync(string id);
}

public class ValueAddedItemGroupService : SalesAppService, IValueAddedItemGroupService
{
    private readonly ISalesRepository<ValueAddedItemGroup> _repository;

    public ValueAddedItemGroupService(ISalesRepository<ValueAddedItemGroup> repository)
    {
        _repository = repository;
    }

    public async Task<ValueAddedItemGroupDto[]> AttachItemsAsync(ValueAddedItemGroupDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var list = await db.Set<ValueAddedItem>().AsNoTracking().Where(x => ids.Contains(x.GroupId)).ToArrayAsync();

        foreach (var m in data)
        {
            var items = list.Where(x => x.GroupId == m.Id).ToArray();

            m.Items = items.MapArrayTo<ValueAddedItem, ValueAddedItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<ValueAddedItemGroupDto[]> QueryByGoodsIdAsync(string goodsId)
    {
        if (string.IsNullOrWhiteSpace(goodsId))
            throw new ArgumentNullException(nameof(goodsId));

        var data = await _repository.QueryManyAsync(x => x.GoodsId == goodsId);

        return data.MapArrayTo<ValueAddedItemGroup, ValueAddedItemGroupDto>(ObjectMapper);
    }

    private async Task<bool> CheckNameExistAsync(string goodsId, string name, string exceptId = null)
    {
        var db = await _repository.GetDbContextAsync();

        var query = db.Set<ValueAddedItemGroup>().AsNoTracking();

        query = query.Where(x => x.GoodsId == goodsId && x.Name == name);

        if (!string.IsNullOrWhiteSpace(exceptId))
            query = query.Where(x => x.Id != exceptId);

        return await query.AnyAsync();
    }

    public async Task InsertAsync(ValueAddedItemGroupDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.GoodsId))
            throw new ArgumentNullException(nameof(dto.GoodsId));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        var db = await _repository.GetDbContextAsync();

        if (await CheckNameExistAsync(dto.GoodsId, dto.Name))
            throw new UserFriendlyException("group name already exist");

        var entity = dto.MapTo<ValueAddedItemGroupDto, ValueAddedItemGroup>(ObjectMapper);

        entity.Id = GuidGenerator.CreateGuidString();

        await _repository.InsertAsync(entity);
    }

    public async Task UpdateAsync(ValueAddedItemGroupDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        var entity = await _repository.FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.IsRequired = dto.IsRequired;
        entity.MultipleChoice = dto.MultipleChoice;

        if (await CheckNameExistAsync(entity.GoodsId, entity.Name, entity.Id))
            throw new UserFriendlyException("group name already exist");

        await _repository.UpdateAsync(entity);
    }

    public async Task DeleteAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var db = await _repository.GetDbContextAsync();

        if (await db.Set<ValueAddedItem>().AsNoTracking().AnyAsync(x => x.GroupId == id))
            throw new UserFriendlyException("pls delete group item first");

        await _repository.DeleteAsync(x => x.Id == id);
    }
}