import { useRequest } from 'ahooks';
import { TreeSelect } from 'antd';
import React, { CSSProperties, useEffect } from 'react';

import { getFlatNodes, getTreeSelectOptionsV2 } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { DepartmentDto } from '@/utils/swagger';
import { getPinyinShortStyle } from '@/utils/utils';

export default ({
  value,
  onChange,
  style,
}: {
  value?: string | null;
  onChange?: (e: DepartmentDto | undefined) => void;
  style?: CSSProperties;
}) => {
  const query_tree_request = useRequest(
    apiClient.sys.departmentDepartmentTree,
    {
      manual: true,
    },
  );

  const tree_data = query_tree_request.data?.data?.Data || [];

  const flatNodes = tree_data.flatMap((x) => getFlatNodes(x));

  const allKeys = [...new Set(flatNodes.map((x) => x.key || ''))];

  const options = tree_data.map((x) =>
    getTreeSelectOptionsV2(
      x,
      (d) => `${d.title}-${getPinyinShortStyle(d.title || '') || 'N/A'}`,
    ),
  );

  useEffect(() => {
    query_tree_request.run({});
  }, []);

  return (
    <>
      <TreeSelect
        style={{
          ...style,
        }}
        loading={query_tree_request.loading}
        value={value}
        onChange={(e) => {
          onChange && onChange(flatNodes.find((x) => x.key == e)?.raw_data);
        }}
        placeholder="输入拼音缩写可以搜索..."
        treeData={options}
        treeExpandedKeys={allKeys}
        treeDefaultExpandAll
        allowClear
        showSearch
        treeNodeFilterProp="label"
        filterTreeNode={undefined}
      />
    </>
  );
};
