namespace XCloud.Platform.Application.Domain.Invoices;

public class UserInvoice : PlatformBaseEntity, IHasCreationTime, IHasDeletionTime
{
    public string UserId { get; set; }

    public string Head { get; set; }

    public string Code { get; set; }

    public int InvoiceType { get; set; }

    public string Address { get; set; }

    public string PhoneNumber { get; set; }

    public string BankName { get; set; }

    public string BankCode { get; set; }

    public string Content { get; set; }

    public DateTime CreationTime { get; set; }
    
    public bool IsDeleted { get; set; }
    
    public DateTime? DeletionTime { get; set; }
}