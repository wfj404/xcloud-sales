﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Job.Jobs;

[UnitOfWork]
[ExposeServices(typeof(AutoImportStorageJob))]
public class AutoImportStorageJob : PlatformAppService
{
    private readonly IStorageService _storageService;
    private readonly StorageUtils _storageUtils;

    public AutoImportStorageJob(IStorageService storageService, StorageUtils storageUtils)
    {
        _storageService = storageService;
        _storageUtils = storageUtils;
    }

    private async Task ImportFileAsync(string filePath)
    {
        this.Logger.LogInformation($"{nameof(ImportFileAsync)}-handle file:{filePath}");

        var file = new FileInfo(filePath);

        if (file.Length > 1024 * 1024 * 5)
        {
            this.Logger.LogInformation($"file is to large,skip file: {filePath}");
            return;
        }

        var bs = await File.ReadAllBytesAsync(filePath);

        if (ValidateHelper.IsEmptyCollection(bs))
        {
            this.Logger.LogInformation($"skip file:{filePath}");
            return;
        }

        await this._storageService.UploadBytesAsync(new FileUploadBytesArgs()
        {
            FileName = file.Name,
            ContentType = _storageUtils.ResolveContentTypeOrNull(file.Name),
            Bytes = bs,
        });
    }

    private IEnumerable<string> WalkDir(string path)
    {
        var files = Directory.GetFiles(path);

        foreach (var f in files)
        {
            yield return f;
        }

        var dirs = Directory.GetDirectories(path);

        foreach (var d in dirs)
        {
            foreach (var f in WalkDir(d))
            {
                yield return f;
            }
        }
    }

    public async Task RunImportAsync()
    {
        var path = this.Configuration["app:config:file_path_to_import"];
        if (string.IsNullOrWhiteSpace(path) || !Directory.Exists(path))
        {
            this.Logger.LogInformation($"directory not exist:{path},skip:{nameof(AutoImportStorageJob)}");
            return;
        }

        this.Logger.LogInformation($"{nameof(AutoImportStorageJob)} started");

        try
        {
            foreach (var f in WalkDir(path))
            {
                using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

                try
                {
                    await ImportFileAsync(f);
                    await Task.Delay(TimeSpan.FromMilliseconds(500));

                    await uow.CompleteAsync();
                }
                catch
                {
                    await uow.RollbackAsync();
                    throw;
                }
            }
        }
        catch (Exception e)
        {
            this.Logger.LogError(message: $"{nameof(AutoImportStorageJob)}", exception: e);
            throw;
        }
        finally
        {
            this.Logger.LogInformation($"{nameof(AutoImportStorageJob)} finished");
        }
    }
}