import { Form, InputNumber } from 'antd';

import XPictureListEditor from '@/components/blocks/components/block_picture_list_editor';
import { CheckCard } from '@ant-design/pro-components';

import { GalleryBlockProps, GalleryStyleType } from './utils';

interface StyleOption {
  title: string;
  value: GalleryStyleType;
}

const style_options: StyleOption[] = [
  {
    title: '一栏',
    value: '1-column',
  },
  {
    title: '二栏',
    value: '2-columns',
  },
  {
    title: '三栏',
    value: '3-columns',
  },
  {
    title: '四宫格',
    value: '4-grids',
  },
  {
    title: '样式一',
    value: 'style-1',
  },
];

export default ({
  data,
  onChange,
}: {
  data: GalleryBlockProps;
  onChange: (x: GalleryBlockProps) => void;
}) => {
  if (!data) {
    return null;
  }

  return (
    <>
      <Form labelCol={{ span: 3 }}>
        <Form.Item label="样式">
          <CheckCard.Group
            multiple={false}
            value={data.style}
            onChange={(e) => {
              if (!e) {
                return;
              }
              onChange({
                ...data,
                style: e?.valueOf() as any,
              });
            }}
            options={style_options}
          ></CheckCard.Group>
        </Form.Item>
        <Form.Item label="网格间隙">
          <InputNumber
            value={data.gutter}
            onChange={(e) => {
              onChange({ ...data, gutter: e || 0 });
            }}
          />
        </Form.Item>
        <Form.Item label="图片">
          <XPictureListEditor
            data={data?.pictureList || []}
            onChange={(x) => {
              onChange({
                ...data,
                pictureList: x,
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
};
