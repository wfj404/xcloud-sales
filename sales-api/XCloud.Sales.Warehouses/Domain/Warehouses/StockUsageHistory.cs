using System;
using Volo.Abp.Auditing;
using XCloud.Sales.Application.Domain;

namespace XCloud.Sales.Warehouses.Domain.Warehouses;

public class StockUsageHistory : SalesBaseEntity<string>, IHasCreationTime
{
    public string StoreId { get; set; }
    public string StockItemId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreationTime { get; set; }
}