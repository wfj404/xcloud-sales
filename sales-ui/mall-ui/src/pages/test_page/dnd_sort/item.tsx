import { CSSProperties, ReactNode } from 'react';

import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

export function SortableItem(props: { id: string; children: ReactNode }) {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    rect,
    isOver,
    over,
  } = useSortable({ id: props.id, disabled: props.id == 'drop-here' });

  const style: CSSProperties = {
    transform: CSS.Transform.toString(transform),
    transition,
    border: isOver ? '1px solid red' : 'none',
  };

  return (
    <div ref={setNodeRef} style={style} {...attributes} {...listeners}>
      {props.children}
      {JSON.stringify(rect)}
    </div>
  );
}
