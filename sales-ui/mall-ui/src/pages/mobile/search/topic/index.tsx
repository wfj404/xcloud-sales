import { useLocalStorageState } from 'ahooks';
import { useEffect, useState } from 'react';

import XLoading from '@/components/common/loading/dots';
import { apiClient } from '@/utils/client';
import { QueryGoodsPaging, SearchView } from '@/utils/swagger';
import { handleResponse, parseJsonOrEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

import XBrand from './brand';
import XSearch from './search';
import XTag from './tag';

export default ({
  query,
  onChange,
}: {
  query: QueryGoodsPaging;
  onChange: (e: QueryGoodsPaging) => void;
}) => {
  const [cachedData, _cachedData] = useLocalStorageState<SearchView>(
    'search.page.data',
    {
      serializer: (x) => JSON.stringify(x || {}),
      deserializer: (x) => parseJsonOrEmpty(x),
      defaultValue: undefined,
    },
  );
  const data = cachedData || {};

  const [loading, _loading] = useState(false);
  const finalLoading = cachedData == undefined && loading;

  const queryView = () => {
    _loading(true);
    apiClient.mall
      .searchSearchView({})
      .then((res) => {
        handleResponse(res, () => {
          _cachedData(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryView();
  }, []);

  return (
    <>
      <Box sx={{}}>
        {finalLoading && <XLoading />}
        <XSearch model={data} query={query} onChange={onChange} />
        <XBrand model={data} query={query} onChange={onChange} />
        <XTag model={data} query={query} onChange={onChange} />
      </Box>
    </>
  );
};
