﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Extension;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Configuration;

namespace XCloud.Sales.Qingdao.Service.Invoices;

[ExposeServices(typeof(McsClient))]
public class McsClient : SalesAppService
{
    private readonly HttpClient _httpClient;

    public McsClient(IHttpClientFactory httpClientFactory)
    {
        this._httpClient = httpClientFactory.CreateClient(nameof(McsClient));
    }

    public async Task<McsBizOrderDto[]> QueryMcsBizOrderForInvoicingAsync(
        QueryMcsBizOrderForInvoicingInput dto)
    {
        if (dto.StartTime == null || dto.EndTime == null)
            throw new ArgumentNullException(paramName: nameof(dto.StartTime));

        var sb = new StringBuilder();
        sb.AppendLine(nameof(QueryMcsBizOrderForInvoicingAsync));

        try
        {
            var config = this.Configuration.GetRequired3HConfig();

            var url = new string[] { config.McsMfApiUrl, "/mcs-biz-api/v3/mf/e-invoice" }.ConcatUrl();

            sb.AppendLine(url);

            var dict = new Dictionary<string, string>()
            {
                ["qt"] = dto.QueryType,
                ["qv"] = dto.QueryValue,
                ["s"] = dto.Status,
                ["st"] = dto.StartTime.Value.ToString("yyyy-MM-dd"),
                ["et"] = dto.EndTime.Value.ToString("yyyy-MM-dd"),
            };

            var queryString = dict.ToUrlQueryString().TrimStart('?');

            if (!string.IsNullOrWhiteSpace(queryString))
            {
                url = $"{url}?{queryString}";

                sb.AppendLine("with query string:");
                sb.AppendLine(url);
            }

            using var res = await this._httpClient.GetAsync(url);

            var json = await res.Content.ReadAsStringAsync();

            sb.AppendLine("response data:");
            sb.AppendLine(json);

            var response = this.JsonDataSerializer.DeserializeFromString<McsResponse<McsBizOrderDto[]>>(json);

            return response.data ?? Array.Empty<McsBizOrderDto>();
        }
        catch (Exception e)
        {
            sb.AppendLine(e.Message);
            throw;
        }
        finally
        {
            this.Logger.LogInformation(message: sb.ToString());
        }
    }

    public async Task<McsBizOrderCallbackResult> NotifyMcsToUpdateInvoicingStatusAsync(string[] bizIds)
    {
        if (ValidateHelper.IsEmptyCollection(bizIds))
            throw new ArgumentNullException(nameof(bizIds));

        var sb = new StringBuilder();
        sb.AppendLine(nameof(GetByIdsAsync));

        try
        {
            var config = this.Configuration.GetRequired3HConfig();

            var url = new string[] { config.McsMfApiUrl, "/mcs-biz-api/v3/mf/e-invoice/apply" }.ConcatUrl();

            sb.AppendLine(url);
            sb.AppendLine("ids:");
            sb.AppendLine(this.JsonDataSerializer.SerializeToString(bizIds));

            using var res = await this._httpClient.PostAsJsonAsync(url, new { bizIds = bizIds });

            var json = await res.Content.ReadAsStringAsync();

            sb.AppendLine("response data:");
            sb.AppendLine(json);

            var response = this.JsonDataSerializer.DeserializeFromString<McsResponse<McsBizOrderCallbackResult>>(json);

            var result = response.data ?? new McsBizOrderCallbackResult();

            result.bills ??= Array.Empty<McsBizOrderDto>();

            return result;
        }
        catch (Exception e)
        {
            sb.AppendLine(e.Message);
            throw;
        }
        finally
        {
            this.Logger.LogInformation(message: sb.ToString());
        }
    }

    public async Task<McsInvoiceRequestItemDto[]> AttachMcsBizOrdersAsync(McsInvoiceRequestItemDto[] data)
    {
        if (data.Any())
        {
            var ids = data.Select(x => x.McsBizOrderId).Distinct().ToArray();

            var datalist = await this.GetByIdsAsync(ids);

            foreach (var m in data)
            {
                m.McsBizOrder = datalist.FirstOrDefault(x => x.bizId == m.McsBizOrderId);
            }
        }

        return data;
    }

    public async Task<McsInvoiceRequestDto[]> AttachMcsUsersAsync(McsInvoiceRequestDto[] data)
    {
        if (data.Any())
        {
            var ids = data.Select(x => x.McsUserId).Distinct().ToArray();

            var datalist = await this.GetMcsUserByIdsAsync(ids);

            foreach (var m in data)
            {
                m.McsUser = datalist.FirstOrDefault(x => x.mcsUserId == m.McsUserId);
            }
        }

        return data;
    }

    public async Task<McsUserDto[]> GetMcsUserByIdsAsync(string[] mcsUserIds)
    {
        this.Logger.LogWarning("未实现，返回空集合");

        await Task.CompletedTask;
        return Array.Empty<McsUserDto>();
    }

    public async Task<McsBizOrderDto[]> GetByIdsAsync(string[] bizIds)
    {
        if (ValidateHelper.IsEmptyCollection(bizIds))
            return Array.Empty<McsBizOrderDto>();

        var sb = new StringBuilder();
        sb.AppendLine(nameof(GetByIdsAsync));

        try
        {
            var config = this.Configuration.GetRequired3HConfig();

            var url = new string[] { config.McsMfApiUrl, "/mcs-biz-api/v3/mf/e-invoice/multi" }.ConcatUrl();

            sb.AppendLine(url);
            sb.AppendLine("ids:");
            sb.AppendLine(this.JsonDataSerializer.SerializeToString(bizIds));

            using var res = await this._httpClient.PostAsJsonAsync(url, new { bizIds = bizIds });

            var json = await res.Content.ReadAsStringAsync();

            sb.AppendLine("response data:");
            sb.AppendLine(json);

            var response = this.JsonDataSerializer.DeserializeFromString<McsResponse<McsBizOrderDto[]>>(json);

            return response.data ?? Array.Empty<McsBizOrderDto>();
        }
        catch (Exception e)
        {
            sb.AppendLine(e.Message);
            throw;
        }
        finally
        {
            this.Logger.LogInformation(message: sb.ToString());
        }
    }

    private string FingerPrint(string[] ids) => string.Join(',', ids.OrderBy(x => x).ToArray());

    private McsInvoiceRequestItemDto BuildInvoiceRequestItem(McsBizOrderDto bizOrder)
    {
        bizOrder.details ??= Array.Empty<McsBizOrderItemDto>();

        return new McsInvoiceRequestItemDto()
        {
            McsBizOrder = bizOrder,
            McsBizOrderId = bizOrder.bizId,
            Price = bizOrder.totalMoney,
            Description = string.Join(',', bizOrder.details.Select(x => x.productName))
        };
    }

    private McsInvoiceRequestDto BuildInvoiceRequest(string bizType, int pickupLocationId, McsBizOrderDto[] group)
    {
        var first = group.First()!;

        var request = new McsInvoiceRequestDto
        {
            BizType = bizType,
            PickupLocationId = first.outletId,
            PickupLocationName = first.outletTitle,
            Items = group.Select(BuildInvoiceRequestItem).ToArray()
        };

        request.InitFields();
        request.TotalAmount = request.Items.Sum(x => x.Price);

        return request;
    }

    public async Task<McsInvoiceRequestDto[]> BuildInvoiceRequestDtoAsync(string[] bizIds)
    {
        if (ValidateHelper.IsEmptyCollection(bizIds))
            throw new ArgumentNullException(nameof(bizIds));

        var datalist = await this.GetByIdsAsync(bizIds);

        if (FingerPrint(datalist.Select(x => x.bizId).ToArray()) != FingerPrint(bizIds))
        {
            throw new ArgumentException(message: "业务单不存在");
        }

        if (datalist.Any(x => !x.AvailableForInvoicing))
        {
            throw new ArgumentException(message: $"部分业务单无法开票：mcs-can-invoicing");
        }

        var list = new List<McsInvoiceRequestDto>();

        var grouped = datalist
            .GroupBy(x => new { x.outletId, x.bizType })
            .Select(x => new
            {
                x.Key.outletId,
                x.Key.bizType,
                Items = x.ToArray()
            })
            .ToArray();

        foreach (var group in grouped)
        {
            var first = group.Items.First()!;

            var request = BuildInvoiceRequest(first.bizType.ToString(), first.outletId, group.Items);

            list.Add(request);
        }

        return list.ToArray();
    }
}