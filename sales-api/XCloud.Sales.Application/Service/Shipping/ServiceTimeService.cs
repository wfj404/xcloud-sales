﻿using Cronos;
using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Core.Helper;

namespace XCloud.Sales.Application.Service.Shipping;

[ExposeServices(typeof(ServiceTimeService))]
public class ServiceTimeService : SalesAppService, ISingletonDependency
{
    public IEnumerable<AvailableServiceTimeRangeDto> ParseServiceTime([NotNull] ServiceTimeDto dto, int days,
        int timezoneOffset = default)
    {
        var datalist = ParseServiceTimeV2(dto, days, timezoneOffset).ToArray();

        var grouped = datalist
            .GroupBy(x => x.StartTime!.Value.AddHours(timezoneOffset).Date)
            .Select(x => new AvailableServiceTimeRangeDto()
            {
                Date = x.Key,
                TimeRanges = x.ToArray()
            });

        return grouped.OrderBy(x => x.Date);
    }

    public IEnumerable<TimeRangeDto> ParseServiceTimeV2([NotNull] ServiceTimeDto dto, int days,
        int timezoneOffset = default)
    {
        foreach (var date in ParseServiceDate(dto, days, timezoneOffset))
        {
            var datalist = dto.TimeRanges.MapArrayTo<TimeRangeDto, TimeRangeDto>(this.ObjectMapper);

            foreach (var range in datalist)
            {
                range.StartTime = date.AddSeconds(range.StartSecond).AddHours(-timezoneOffset);
                range.EndTime = date.AddSeconds(range.EndSecond).AddHours(-timezoneOffset);

                yield return range;
            }
        }
    }

    private ExceptionTime WithTimeZone(ExceptionTime x, int timezoneOffset)
    {
        var time = x.MapTo<ExceptionTime, ExceptionTime>(this.ObjectMapper);

        time.StartTime = x.StartTime.AddHours(timezoneOffset);
        time.EndTime = x.EndTime.AddHours(timezoneOffset);

        return time;
    }

    private IEnumerable<DateTime> ParseServiceDate([NotNull] ServiceTimeDto dto, int days, int timezoneOffset = default)
    {
        var cronDate = ParseServiceCronTimeTrigger(dto, days, timezoneOffset)
            .Select(x => x.Date)
            .Distinct()
            .OrderBy(x => x.Ticks)
            .ToArray();

        var includedTimeRange =
            dto.ExceptionTimes.Where(x => x.Include)
                .Select(x => WithTimeZone(x, timezoneOffset))
                .ToArray();

        var excludedTimeRange =
            dto.ExceptionTimes.Where(x => !x.Include)
                .Select(x => WithTimeZone(x, timezoneOffset))
                .ToArray();

        var start = Clock.Now.AddHours(timezoneOffset).Date;
        var end = start.AddDays(days).Date;

        for (var date = start; date < end; date = date.AddDays(1))
        {
            //extra work
            if (includedTimeRange.IncludeTime(date))
            {
                yield return date;
                continue;
            }

            //a little break
            if (excludedTimeRange.IncludeTime(date))
            {
                continue;
            }

            //normal cron
            if (cronDate.Contains(date))
            {
                yield return date;
                continue;
            }
        }
    }

    private IEnumerable<DateTime> ParseServiceCronTimeTrigger([NotNull] ServiceTimeDto dto, int days,
        int timezoneOffset = default)
    {
        ArgumentNullException.ThrowIfNull(dto);

        var cronList = dto.CronRules.Select(x => x.Cron).WhereNotEmpty().ToArray();

        if (ValidateHelper.IsEmptyCollection(cronList))
            yield break;

        var start = Clock.Now;
        var end = start.AddDays(days);
        var maxCount = 24 * 6 * days;

        foreach (var cron in cronList)
        {
            var expression = CronExpression.Parse(cron);

            var nextList = expression.GetOccurrences(fromUtc: start, toUtc: end,
                zone: TimeZoneInfo.Utc,
                fromInclusive: true,
                toInclusive: true);

            var i = 0;
            foreach (var next in nextList)
            {
                if (++i > maxCount)
                    break;

                yield return next.AddHours(timezoneOffset);
            }
        }
    }
}