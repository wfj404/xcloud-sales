﻿using Volo.Abp;

namespace XCloud.EntityFrameworkCore.Exceptions;

public class UnSubmitChangesException : BusinessException
{
    public UnSubmitChangesException(string msg) : base(message: msg)
    {
        //
    }
}