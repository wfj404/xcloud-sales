import { ImageViewer, Swiper, SwiperRef } from 'antd-mobile';
import { SlidesRef } from 'antd-mobile/es/components/image-viewer/slides';
import { useEffect, useMemo, useRef, useState } from 'react';

import XEmptyImage from '@/assets/empty.svg';
import XImage from '@/components/common/image';
import { getPictureSrc, PictureSrc, sortGoodsPictures } from '@/utils/biz';
import { GoodsDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

export default ({ model, skuId }: { model: GoodsDto; skuId?: string }) => {
  const [sliderIndex, setSliderIndex] = useState<number>(0);
  const [previewShow, _previewShow] = useState<boolean>(false);
  const previewer = useRef<SlidesRef>(null);
  const swiper = useRef<SwiperRef>(null);

  const imageUrlList = useMemo(() => {
    const sortedPictures = sortGoodsPictures(model.GoodsPictures || [], skuId);

    const pictureList = sortedPictures.map((x) => x.StorageMeta || {});

    return pictureList.map<PictureSrc>((x) => getPictureSrc(x));
  }, [model.GoodsPictures, skuId]);

  useEffect(() => {
    setSliderIndex(0);
  }, [imageUrlList]);

  const renderSwiper = () => {
    if (isArrayEmpty(imageUrlList)) {
      return <img src={XEmptyImage} style={{ width: '100%' }} alt="" />;
    }

    return (
      <>
        <Swiper
          ref={swiper}
          slideSize={100}
          tabIndex={sliderIndex}
          onIndexChange={(e) => {
            if (e == sliderIndex) {
              return;
            }
            setSliderIndex(e);
            previewer.current?.swipeTo(e);
          }}
        >
          {imageUrlList.map((x, index) => (
            <Swiper.Item key={index}>
              <Box
                onClick={() => {
                  _previewShow(true);
                }}
              >
                <XImage
                  rate={3 / 4}
                  alt=""
                  lazy={true}
                  src={x.medium || ''}
                  fit="cover"
                />
              </Box>
            </Swiper.Item>
          ))}
        </Swiper>
      </>
    );
  };

  const renderPreview = () => {
    if (isArrayEmpty(imageUrlList)) {
      return null;
    }

    return (
      <>
        <ImageViewer.Multi
          ref={previewer}
          visible={previewShow}
          images={imageUrlList.map((x) => x.large || '')}
          onIndexChange={(e) => {
            setSliderIndex(e);
            //swiper.current?.swipeTo(e);
          }}
          onClose={() => {
            _previewShow(false);
          }}
        />
      </>
    );
  };

  return (
    <>
      {renderPreview()}
      <Box
        sx={{
          minHeight: '200px',
          mb: 2,
        }}
      >
        {renderSwiper()}
      </Box>
    </>
  );
};
