import { useLocalStorageState, useRequest, useTitle } from 'ahooks';
import { useEffect } from 'react';
import { useSnapshot } from 'umi';

import XBlocks from '@/components/blocks';
import { BlockItem, BlockItemProps } from '@/components/blocks/utils';
import XLoading from '@/components/common/loading/dots';
import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { parseJsonOrEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

import XBottom from './bottom';
import XContainer from './container';
import XNotice from './notice';

export default function () {
  const app = useSnapshot(storeState) as StoreType;

  useTitle(`${app.getAppName(true)}-首页`);

  const [cachedData, _cachedData] = useLocalStorageState<
    BlockItem<BlockItemProps>[]
  >('home.page.data.blocks', {
    serializer: (x) => JSON.stringify(x || []),
    deserializer: (x) => parseJsonOrEmpty(x),
    defaultValue: undefined,
  });

  const blocks = cachedData || [];

  const queryHomeBlocksRequest = useRequest(apiClient.mall.homeHomePageBlocks, {
    manual: true,
    onSuccess(data, params) {
      const blocks_data: BlockItem<BlockItemProps>[] = parseJsonOrEmpty(
        data.data?.Data?.Blocks,
      );
      _cachedData(blocks_data);
    },
  });

  useEffect(() => {
    queryHomeBlocksRequest.run();
  }, []);

  return (
    <>
      <XContainer>
        <Box sx={{ p: 0, m: 0, minHeight: 1000 }}>
          {queryHomeBlocksRequest.loading && <XLoading />}
          <XNotice />
          <XBlocks showHidden blocks={blocks} />
          <XBottom />
        </Box>
      </XContainer>
    </>
  );
}
