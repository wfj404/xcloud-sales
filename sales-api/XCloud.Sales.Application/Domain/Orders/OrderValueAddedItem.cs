namespace XCloud.Sales.Application.Domain.Orders;

public class OrderValueAddedItem : SalesBaseEntity<string>
{
    public string OrderItemId { get; set; }
    
    public int ValueAddedItemId { get; set; }

    public decimal PriceOffset { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }
}