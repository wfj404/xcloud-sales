using XCloud.EntityFrameworkCore.Repository;
using XCloud.Sales.Application.Domain;

namespace XCloud.Sales.Application.Repository;

/// <summary>
/// Represents an entity repository
/// </summary>
/// <typeparam name="TEntity">Entity type</typeparam>
public interface ISalesRepository<TEntity> : IEfRepository<TEntity>, IScopedDependency where TEntity : class, ISalesBaseEntity
{
    //
}