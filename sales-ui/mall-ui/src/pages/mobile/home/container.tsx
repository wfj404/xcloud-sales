import { ReactElement, useMemo } from 'react';
import { Link, useSnapshot } from 'umi';

import XStore from '@/components/mobile/current_store/store';
import { storeState, StoreType } from '@/store';
import { config } from '@/utils/config';
import { resolveUrl } from '@/utils/storage';
import { isStringEmpty } from '@/utils/utils';
import { alpha, AppBar, Box, Container, Stack, Toolbar } from '@mui/material';

const XTopLogo = function () {
  const app = useSnapshot(storeState) as StoreType;

  const settings = app.mallSettings || {};

  const logo_url =
    useMemo(() => {
      const urls = [
        settings.FullLogoStorageData,
        settings.SimpleLogoStorageData,
      ].map((x) => resolveUrl(x, { height: 100 }));
      return urls.filter((x) => !isStringEmpty(x)).at(0);
    }, [settings]) || config.app.logo.normal;

  return (
    <>
      <Box sx={{}}>
        <Link
          to={{
            pathname: '/',
          }}
        >
          <Box sx={{}}>
            <img
              src={logo_url}
              alt={'logo'}
              style={{ height: '30px', width: 'auto' }}
            />
          </Box>
        </Link>
      </Box>
    </>
  );
};

export default function ({ children }: { children: ReactElement }) {
  return (
    <>
      <Container maxWidth="sm" disableGutters>
        <AppBar
          position="static"
          sx={(theme) => ({
            boxShadow: 'none',
            backdropFilter: 'blur(6px)',
            WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
            backgroundColor: alpha('rgb(254,254,254)', 0.72),
          })}
        >
          <Container disableGutters maxWidth="sm">
            <Toolbar
              sx={(theme) => ({
                color: 'black',
              })}
            >
              <XTopLogo />
              <Box sx={{ flexGrow: 1 }} />
              <Stack
                direction="row"
                alignItems="center"
                spacing={{ xs: 0.5, sm: 1.5 }}
              >
                <XStore />
              </Stack>
            </Toolbar>
          </Container>
        </AppBar>
        <Box
          component={'div'}
          sx={(theme) => ({
            paddingBottom: 1,
          })}
        >
          {children}
        </Box>
      </Container>
    </>
  );
}
