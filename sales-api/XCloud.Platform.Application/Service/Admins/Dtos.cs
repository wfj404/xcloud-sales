﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Service.Dept;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Service.Admins;

public class AdminAuthResult : ApiResponse<AdminDto>
{
    public AdminDto Admin => Data;
}

public class SetAsAdminDto : IEntityDto
{
    public string UserId { get; set; }
}

public class UpdateAdminStatusDto : IdDto
{
    public bool? IsActive { get; set; }
    public bool? IsSuperAdmin { get; set; }
}

public class QueryAdminPaging : PagedRequest
{
    public string Keyword { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
}

public class AdminDto : Admin, IEntityDto<string>
{
    public string[] PermissionKeys { get; set; }

    public RoleDto[] Roles { get; set; }

    public DepartmentDto[] Departments { get; set; }

    public UserDto User { get; set; }

    public static implicit operator string(AdminDto dto) => dto?.Id;
}