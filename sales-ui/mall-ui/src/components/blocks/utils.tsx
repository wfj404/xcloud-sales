import { CSSProperties, ReactElement, ReactNode } from 'react';

import { LinkDto } from '@/utils/biz';
import { StorageMetaDto } from '@/utils/swagger';
import {
  FileTextOutlined,
  FontSizeOutlined,
  InfoCircleOutlined,
  LikeOutlined,
  OrderedListOutlined,
  PicLeftOutlined,
  PictureOutlined,
  ReconciliationOutlined,
  SearchOutlined,
  SoundOutlined,
  YoutubeOutlined,
} from '@ant-design/icons';

import { card_demo } from './components/card/utils';
import { category_goods_demo } from './components/category_goods/utils';
import { coupon_demo } from './components/coupon/utils';
import {
  columns_1_demo,
  columns_2_demo,
  columns_style_demo,
} from './components/gallery/utils';
import { hello_demo } from './components/hello/utils';
import { hot_goods_demo } from './components/hot_goods/utils';
import { markdown_demo } from './components/markdown/utils';
import { search_demo } from './components/search/utils';
import { slider_demo } from './components/slider/utils';
import { title_demo } from './components/title/utils';
import { video_demo } from './components/video/utils';

export type BlockItemType =
  | 'slider'
  | 'hello'
  | 'gallery'
  | 'search'
  | 'video'
  | 'card'
  | 'coupon'
  | 'hot-goods'
  | 'category-goods'
  | 'markdown'
  | 'title';

export interface BlockItem<T extends BlockItemProps> {
  id?: string;
  label?: string;
  type: BlockItemType;
  body: T;
  hide?: boolean;
  style?: CSSProperties;
}

export interface BlockItemProps {}

export interface BlockPictureProps {
  id?: string;
  storage?: StorageMetaDto | string;
  caption?: string;
  width?: number;
  height?: number;
  rate?: number;
  link?: LinkDto;
}

export interface BlockLinkText {
  text?: string;
  link?: LinkDto;
}

export type GoodsDisplayType = 'grid' | 'item' | 'slider';

export interface GoodsDisplayTypeOptionsType {
  label: string;
  value: GoodsDisplayType;
}

export const GoodsDisplayTypeOptions: GoodsDisplayTypeOptionsType[] = [
  {
    label: 'grid',
    value: 'grid',
  },
  {
    label: 'item',
    value: 'item',
  },
  {
    label: 'slider',
    value: 'slider',
  },
];

export const getTemplateBlocks = async (): Promise<
  BlockItem<BlockItemProps>[]
> => {
  return [
    search_demo,
    slider_demo,
    hello_demo,
    video_demo,
    coupon_demo,
    card_demo,
    columns_1_demo,
    columns_2_demo,
    columns_style_demo,
    hot_goods_demo,
    category_goods_demo,
    title_demo,
    markdown_demo,
  ];
};

export interface BlockMenuItem extends BlockItem<BlockItemProps> {
  icon?: ReactElement;
}

export const menuToBlockItem = (
  x: BlockMenuItem,
): BlockItem<BlockItemProps> => {
  const item: BlockMenuItem = { ...x, icon: undefined, label: undefined };

  return item;
};

export interface BlockMenuGroup {
  title: ReactNode;
  menus: BlockMenuItem[];
}

export const GroupedComponentsMenuItems: BlockMenuGroup[] = [
  {
    title: '通用组件',
    menus: [
      {
        ...title_demo,
        label: '标题',
        icon: <FontSizeOutlined />,
      },
      {
        ...slider_demo,
        label: '轮播图',
        icon: <PictureOutlined />,
      },
      {
        ...video_demo,
        label: '视频',
        icon: <YoutubeOutlined />,
      },
      {
        ...hello_demo,
        label: 'Say Hi',
        icon: <InfoCircleOutlined />,
      },
      {
        ...columns_1_demo,
        label: '画廊',
        icon: <PicLeftOutlined />,
      },
      {
        ...search_demo,
        label: '搜索框',
        icon: <SearchOutlined />,
      },
      {
        ...hot_goods_demo,
        label: '热门商品',
        icon: <LikeOutlined />,
      },
      {
        ...category_goods_demo,
        label: '商品类目',
        icon: <OrderedListOutlined />,
      },
      {
        ...coupon_demo,
        label: '领券中心',
        icon: <SoundOutlined />,
      },
      {
        ...markdown_demo,
        label: '图文',
        icon: <FileTextOutlined />,
      },
      {
        ...card_demo,
        label: '卡片',
        icon: <ReconciliationOutlined />,
      },
    ],
  },
];

export const AddComponentsMenuItems: BlockMenuItem[] =
  GroupedComponentsMenuItems.flatMap((x) => x.menus);
