import {
    Button, Card, Collapse, CollapseProps, Divider, notification, Space, Tag, Tooltip
} from 'antd';
import { sumBy } from 'lodash-es';
import { useEffect, useState } from 'react';

import { OrderGroupDataDto, OrderGroupOrderDto, UserAddressDto } from '@/utils/swagger';
import { formatMoney, isStringEmpty, simpleString } from '@/utils/utils';
import { Box } from '@mui/material';

import XOrderEditor from './order_item';
import { calculate_total_price, has_error_in_order } from './order_item/utils';

export const get_order_card_id = (x: OrderGroupOrderDto, i: number) =>
  `x_order_group_card_id_index_${i}`;

const address_to_text = (x: UserAddressDto, subject?: string) => {
  const text_list: string[] = [
    `${subject || '收件人'}：${x.Name || '--'}`,
    `联系电话：${x.Tel || '--'}`,
    `邮寄地址：${x.AddressDetail || '--'}`,
  ];

  return text_list.join('\n');
};

export default ({
  group_data,
  onChange,
}: {
  group_data: OrderGroupDataDto;
  onChange: (e: OrderGroupDataDto) => void;
}) => {
  const orders = group_data?.Orders || [];

  const [selectedIds, _selectedIds] = useState<string[]>([]);

  useEffect(() => {
    console.log(selectedIds);
  }, [selectedIds]);

  const renderTitle = (x: OrderGroupOrderDto, i: number) => {
    const total_price = calculate_total_price(x.Items || []);
    const total_paid = sumBy(x.PaymentBills || [], (x) => x.Price || 0);
    const offset = total_price - total_paid;

    return (
      <Space direction="horizontal" split={<Divider type="vertical" />}>
        <Tag color="orange">#{i + 1}</Tag>
        <h3
          style={{
            display: 'inline-block',
            backgroundColor: 'orange',
            paddingLeft: 3,
            paddingRight: 3,
          }}
        >
          <pre>{x.UserAddress?.Name || '???'}</pre>
        </h3>
        <Space direction="horizontal">
          <Tooltip title="应收">
            <Tag color="error">{formatMoney(total_price)}</Tag>
          </Tooltip>
          <b>-</b>
          <Tooltip title="已收">
            <Tag color="blue">{formatMoney(total_paid)}</Tag>
          </Tooltip>
          <b>=</b>
          <Tooltip title="剩余应收">
            <Tag
              color={offset > 0 ? 'orange' : offset < 0 ? 'error' : 'default'}
            >
              {formatMoney(offset)}
            </Tag>
          </Tooltip>
        </Space>
        {x.SpecificSender && (
          <span style={{ color: 'gray' }}>
            发件人:{x.Sender?.Name || '???'}
          </span>
        )}
        {isStringEmpty(x.Remark) || (
          <Tooltip title={<div>{`订单备注：${x.Remark}`}</div>}>
            <span>{simpleString(x.Remark || '', 30)}</span>
          </Tooltip>
        )}
      </Space>
    );
  };

  const renderExtra = (x: OrderGroupOrderDto, i: number) => {
    return (
      <Space direction="horizontal">
        <Button
          type="dashed"
          size="small"
          onClick={() => {
            const address =
              (x.SpecificSender ? x.Sender : group_data.SharedSender) || {};
            const text = address_to_text(address, '发件人');
            navigator.clipboard.writeText(text);
            notification.success({
              message: '复制成功',
              description: <pre>{text}</pre>,
            });
          }}
        >
          复制发件人
        </Button>
        <Button
          type="dashed"
          size="small"
          onClick={() => {
            const text = address_to_text(x.UserAddress || {}, '收件人');
            navigator.clipboard.writeText(text);
            notification.success({
              message: '复制成功',
              description: <pre>{text}</pre>,
            });
          }}
        >
          复制收件人
        </Button>
        <Button
          type="dashed"
          size="small"
          onClick={() => {
            const address =
              (x.SpecificSender ? x.Sender : group_data.SharedSender) || {};

            const goods = x.Items?.map(
              (x) => `${x.Sku?.Name || x.Remark || '--'}---X${x.Quantity || 0}`,
            );

            const text_list: string[] = [
              address_to_text(address, '发件人'),
              `-----------------------------------`,
              address_to_text(x.UserAddress || {}, '收件人'),
              `-----------------------------------`,
              ...(goods || []),
            ];

            const text = text_list.join('\n');
            navigator.clipboard.writeText(text);
            notification.success({
              message: '复制成功',
              description: <pre>{text}</pre>,
            });
          }}
        >
          复制订单文本
        </Button>
        <Button
          danger
          type="text"
          size="small"
          onClick={() => {
            if (confirm('确定删除订单？')) {
              const datalist: OrderGroupOrderDto[] = [...orders].filter(
                (x, index) => index !== i,
              );
              onChange({ ...group_data, Orders: datalist });
            }
          }}
        >
          删除订单
        </Button>
      </Space>
    );
  };

  const renderOrderEditorCard = (
    selected: boolean,
    x: OrderGroupOrderDto,
    i: number,
  ) => {
    return (
      <Box
        sx={{
          border: `3px dashed blue`,
          borderColor: selected ? 'blue' : 'transparent',
        }}
      >
        <Card size="small" extra={renderExtra(x, i)}>
          <XOrderEditor
            order={x || {}}
            onChange={(e) => {
              const datalist: OrderGroupOrderDto[] = [...orders];
              datalist[i] = e;
              onChange({ ...group_data, Orders: datalist });
            }}
          />
        </Card>
      </Box>
    );
  };

  const items: CollapseProps['items'] = orders.map((x, i) => {
    const id = get_order_card_id(x, i);
    const selected = selectedIds.indexOf(id) >= 0;
    return {
      key: id,
      label: <div id={id}>{renderTitle(x, i)}</div>,
      extra: has_error_in_order(x) && (
        <Tooltip title="信息不完整">
          <span style={{ color: 'red' }}>请补全</span>
        </Tooltip>
      ),
      children: (
        <div style={{ minHeight: 200 }}>
          {selected && renderOrderEditorCard(selected, x, i)}
        </div>
      ),
      forceRender: false,
    };
  });

  return (
    <>
      <Collapse
        size="small"
        accordion
        bordered
        destroyInactivePanel
        items={items}
        activeKey={selectedIds.at(0)}
        onChange={(e) => {
          _selectedIds((e as string[]) || []);
        }}
      />
    </>
  );
};
