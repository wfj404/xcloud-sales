import XTime from '@/components/common/time';
import { apiClient } from '@/utils/client';
import { PagedResponse } from '@/utils/biz';
import { QueryRolePaging, RoleDto } from '@/utils/swagger';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import XForm from './form';
import XPermission from './permission';
import { handleResponse } from '@/utils/utils';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<RoleDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QueryRolePaging>({
    Page: 1,
  });

  const [formData, _formData] = useState<RoleDto | undefined>(undefined);

  const queryList = () => {
    _loading(true);
    apiClient.sys
      .manageRoleQueryPaging({ ...query })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnProps<RoleDto>[] = [
    {
      title: '名称',
      render: (x: RoleDto) => x.Name,
    },
    {
      title: '描述',
      render: (x: RoleDto) => x.Description || '--',
    },
    {
      title: '权限',
      render: (x: RoleDto) => (
        <XPermission
          model={x}
          ok={() => {
            queryList();
          }}
        />
      ),
    },
    {
      title: '时间',
      render: (x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (record: RoleDto) => {
        return (
          <Button.Group>
            <Button
              type='link'
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, [query]);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title='角色'
        extra={
          <Button
            type='primary'
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
