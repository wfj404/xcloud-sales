import { Space } from 'antd-mobile';
import { useState } from 'react';

import { SpecDto, SpecItemDto, SpecValueDto } from '@/utils/swagger';
import { LoadingButton } from '@mui/lab';
import { Box, Tooltip, Typography } from '@mui/material';

export default ({
  specs,
  selectedSpecItems,
  onSpecItemsSelect,
  loading,
}: {
  specs: SpecDto[];
  selectedSpecItems: SpecItemDto[];
  onSpecItemsSelect: (e: SpecItemDto[]) => void;
  loading?: boolean;
}) => {
  const [click_value_id, _click_value_id] = useState<number | undefined | null>(
    undefined,
  );

  const renderButton = (spec: SpecDto, value: SpecValueDto) => {
    const active = value.Selected || value.Available;
    const others: SpecItemDto[] = selectedSpecItems.filter(
      (x) => x.SpecId != spec.Id,
    );

    return (
      <>
        <Tooltip title={value.Available ? undefined : '无库存'}>
          <LoadingButton
            loading={loading && click_value_id == value.Id}
            size="small"
            variant={value.Selected ? 'contained' : 'outlined'}
            color="primary"
            disabled={!active}
            onClick={(e) => {
              _click_value_id(value.Id);
              if (value.Selected) {
                onSpecItemsSelect(others);
              } else {
                onSpecItemsSelect([
                  ...others,
                  {
                    SpecId: spec.Id,
                    SpecValueId: value.Id,
                  },
                ]);
              }
            }}
          >
            {value.Name}
          </LoadingButton>
        </Tooltip>
      </>
    );
  };

  const renderSpec = (spec: SpecDto) => {
    const values: SpecValueDto[] = spec.Values || [];
    return (
      <>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <Typography
            variant="caption"
            color="text.disabled"
            component={'span'}
          >
            {spec.Name || '--'}
          </Typography>
          <div style={{ flexGrow: 1, marginLeft: 10 }}>
            <Space direction="horizontal" wrap>
              {values.map((x, i) => {
                return <span key={i}>{renderButton(spec, x)}</span>;
              })}
            </Space>
          </div>
        </div>
      </>
    );
  };

  const renderMenu = () => {
    if (specs.length <= 0) {
      return null;
    }

    return (
      <Box sx={{ p: 1, my: 1 }}>
        <Space direction="vertical">
          {specs.map((x, i) => {
            return <div key={i}>{renderSpec(x)}</div>;
          })}
        </Space>
      </Box>
    );
  };

  return <>{renderMenu()}</>;
};
