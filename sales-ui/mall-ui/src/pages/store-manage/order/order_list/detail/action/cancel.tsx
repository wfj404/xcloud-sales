import { Button, Input, Modal } from 'antd';
import React, { useState } from 'react';

import { OrderStatus, PaymentStatus, ShippingStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';
import { CloseOutlined } from '@ant-design/icons';

export default function (props: { model: OrderDto; ok: () => void }) {
  const { model, ok } = props;

  const [show, _show] = useState<boolean>(false);

  const hide = () => _show(false);

  const [loadingSave, _loadingSave] = React.useState(false);

  const [comment, _comment] = React.useState('');

  const save = () => {
    _loadingSave(true);
    apiClient.mallManager
      .orderCancelOrder({
        OrderId: model.Id,
        Comment: comment,
      })
      .then((res) => {
        handleResponse(res, () => {
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  return (
    <>
      <Button.Group>
        <Button
          disabled={
            model.OrderStatusId != OrderStatus.Pending ||
            model.ShippingStatusId != ShippingStatus.NotShipping ||
            model.PaymentStatusId != PaymentStatus.Pending
          }
          icon={<CloseOutlined />}
          type={'primary'}
          danger
          onClick={() => {
            _show(true);
          }}
        >
          取消订单
        </Button>
      </Button.Group>
      <Modal
        title="取消订单"
        open={show}
        onCancel={() => {
          hide && hide();
        }}
        onOk={() => {
          save();
        }}
        confirmLoading={loadingSave}
      >
        <h6
          style={{
            marginBottom: 10,
            display: 'block',
          }}
        >
          确认取消订单吗？
        </h6>
        <Input.TextArea
          autoFocus
          rows={3}
          placeholder="请输入取消理由"
          value={comment}
          onChange={(e) => _comment(e.target.value)}
        />
      </Modal>
    </>
  );
}
