﻿using System.Data;
using System.Threading.Tasks;
using Polly;
using Volo.Abp.Data;
using XCloud.Application.DistributedLock;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.SerialNos;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.SerialNos;

/// <summary>
/// 使用分布式锁只能锁住当前代码块，不能防止其他方式访问并修改数据库。
/// 所以这里配合数据库乐观锁来使用。
/// </summary>
public interface ISerialNoService : IXCloudAppService
{
    Task<int> CreateSerialNoAsync(CreateSerialNoDto dto);
}

public class SerialNoService : PlatformAppService, ISerialNoService
{
    private readonly IPlatformRepository<SerialNo> _sequenceEntities;

    public SerialNoService(IPlatformRepository<SerialNo> sequenceEntities)
    {
        _sequenceEntities = sequenceEntities;
    }

    private void CheckCreateNoByCategoryDto(CreateSerialNoDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.Category))
            throw new ArgumentNullException(nameof(dto.Category));
    }

    private void TrySetMinId(SerialNo entity, CreateSerialNoDto dto)
    {
        if (dto.MinId != null)
        {
            if (entity.NextId < dto.MinId.Value)
            {
                entity.NextId = dto.MinId.Value;
            }
        }
    }

    private async Task<int> CreateWithOptimisticLockAsync(CreateSerialNoDto dto)
    {
        var step = dto.Step ?? 1;
        step = Math.Max(step, 1);

        using var uow = UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);
        try
        {
            var db = await _sequenceEntities.GetDbContextAsync();
            var set = db.Set<SerialNo>();

            var entity = await set.AsTracking().FirstOrDefaultAsync(x => x.Category == dto.Category);
            if (entity == null)
            {
                //create new record
                entity = new SerialNo
                {
                    Id = GuidGenerator.CreateGuidString(),
                    CreationTime = Clock.Now,
                    Category = dto.Category,
                    NextId = default(int) + step
                };

                TrySetMinId(entity, dto);

                set.Add(entity);
            }
            else
            {
                //modify the record
                entity.NextId += step;
                entity.LastModificationTime = Clock.Now;

                TrySetMinId(entity, dto);
            }

            await db.SaveChangesAsync();

            await uow.CompleteAsync();

            return entity.NextId;
        }
        catch (Exception e) when (IsDbConcurrencyException(e))
        {
            await uow.RollbackAsync();
            throw;
        }
        catch
        {
            await uow.RollbackAsync();
            throw;
        }
    }

    private bool IsDbConcurrencyException(Exception e) =>
        e is DBConcurrencyException ||
        e is DbUpdateConcurrencyException ||
        e is AbpDbConcurrencyException;

    private async Task<int> CreateWithOptimisticLockAndRetryAsync(CreateSerialNoDto dto)
    {
        var retryPolicy = Policy.Handle<Exception>(exceptionPredicate: IsDbConcurrencyException)
            .WaitAndRetryAsync(retryCount: 3, i => TimeSpan.FromMilliseconds(100 * i));

        var nextId = await retryPolicy.ExecuteAsync(() => CreateWithOptimisticLockAsync(dto));

        return nextId;
    }

    public async Task<int> CreateSerialNoAsync(CreateSerialNoDto dto)
    {
        CheckCreateNoByCategoryDto(dto);

        var resourceKey =
            $"{nameof(SerialNoService)}.{nameof(CreateSerialNoAsync)}.category={dto.Category}";

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: resourceKey,
            expiryTime: TimeSpan.FromSeconds(30));

        return await CreateWithOptimisticLockAndRetryAsync(dto);
    }
}