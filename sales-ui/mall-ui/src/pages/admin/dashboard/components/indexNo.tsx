import { Tag } from 'antd';

interface ItemColor {
  index: number;
  color: string;
}

const colors: ItemColor[] = [
  {
    index: 0,
    color: 'success',
  },
  {
    index: 1,
    color: 'error',
  },
  {
    index: 2,
    color: 'warning',
  },
];

interface IIndexNoProps {
  index: number;
}

export default (props: IIndexNoProps) => {
  const { index } = props;

  let color = colors.find((x) => x.index == index)?.color;
  color = color || 'default';

  return <Tag color={color}>#{index + 1}</Tag>;
};
