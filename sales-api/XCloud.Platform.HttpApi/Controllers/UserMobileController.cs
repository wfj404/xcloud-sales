﻿using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/user-mobile")]
public class UserMobileController : PlatformBaseController
{
    private readonly IUserMobileService _userMobileService;

    public UserMobileController(IUserMobileService userMobileService)
    {
        _userMobileService = userMobileService;
    }
}