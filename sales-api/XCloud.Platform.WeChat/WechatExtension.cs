﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using SKIT.FlurlHttpClient;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Settings;
using XCloud.Core.Cache;
using XCloud.Core.Exceptions;
using XCloud.Core.Extension;
using XCloud.Core.Json;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Platform.WeChat;

public static class WechatExtension
{
    public static string NormalizeWechatBillsField(this string str)
    {
        return str?.TrimStart('`').Trim();
    }

    public static void ThrowIfNotSuccess(this ICommonResponse response, IJsonDataSerializer jsonDataSerializer,
        string message = default)
    {
        if (!response.IsSuccessful())
        {
            message ??= response.GetType().FullName;

            var sb = new StringBuilder();

            sb.AppendLine(message);
            sb.AppendLine(jsonDataSerializer.SerializeToString(response));

            throw new WechatException(message: sb.ToString());
        }
    }

    public static T EnsureWechatOptionNotEmpty<T>(this T option)
        where T : WechatOptionBase
    {
        if (string.IsNullOrWhiteSpace(option.AppId))
            throw new ConfigException(nameof(option.AppId));

        if (string.IsNullOrWhiteSpace(option.AppSecret))
            throw new ConfigException(nameof(option.AppSecret));

        return option;
    }

    public static WechatPaymentOption EnsurePaymentOptionNotEmpty(this WechatPaymentOption option)
    {
        if (string.IsNullOrWhiteSpace(option.MchId))
            throw new ConfigException(nameof(option.MchId));

        return option;
    }

    [NotNull]
    [ItemNotNull]
    public static async Task<WechatMpOption> GetWechatMpOptionAsync(this ISettingService settingService)
    {
        var option = await settingService.GetSettingsAsync<WechatMpOption>(new CacheStrategy { Cache = true });

        return option;
    }

    [NotNull]
    [ItemNotNull]
    public static async Task<WechatOpenOption> GetWechatOpenOptionAsync(this ISettingService settingService)
    {
        var option = await settingService.GetSettingsAsync<WechatOpenOption>(new CacheStrategy { Cache = true });

        return option;
    }

    [NotNull]
    [ItemNotNull]
    public static async Task<WechatPaymentOption> GetWechatPaymentOptionAsync(this ISettingService settingService)
    {
        var option = await settingService.GetSettingsAsync<WechatPaymentOption>(new CacheStrategy { Cache = true });

        return option;
    }

    public static void SetCertificates(this ICertificateManager certificateManager,
        IEnumerable<CertificateEntry> datalist)
    {
        certificateManager.AllEntries().Select(x => x.SerialNumber).ToList()
            .ForEach(x => certificateManager.RemoveEntry(serialNumber: x));

        foreach (var m in datalist)
        {
            certificateManager.AddEntry(m);
        }
    }

    public static IEnumerable<CertificateEntry> CombineCertificateEntries(
        this IEnumerable<CertificateEntry> certificateList,
        CertificateEntry[] datalist)
    {
        var combineList = certificateList.AppendManyItems(datalist).Where(x => x.IsAvailable()).ToArray();

        var grouping = combineList.GroupBy(x => x.SerialNumber)
            .Select(x => new { x.Key, Items = x.ToArray() })
            .ToArray();

        foreach (var group in grouping)
        {
            var m = group.Items.MinBy(x => x.EffectiveTime);
            yield return m;
        }
    }
}