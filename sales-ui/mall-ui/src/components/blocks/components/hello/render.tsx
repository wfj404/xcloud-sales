import confetti from 'canvas-confetti';
import { useSnapshot } from 'umi';

import illustration_avatar from '@/assets/static/illustrations/illustration_avatar.png';
import { storeState, StoreType } from '@/store';
import { isStringEmpty } from '@/utils/utils';
import { Box, Paper, Typography } from '@mui/material';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';

import { HelloBlockProps } from './utils';

const StyledButton = styled(Button)(({ theme }) => ({
  backgroundColor: 'white',
  borderRadius: 100,
  paddingLeft: 32,
  paddingRight: 32,
  color: 'black',
  textTransform: 'none',
  border: `2px solid ${theme.palette.error.main}`,
  '&:hover': {
    backgroundColor: 'rgba(250,250,250)',
  },
}));

export default function({ data }: { data: HelloBlockProps }) {

  const app = useSnapshot(storeState) as StoreType;

  return (
    <Paper
      sx={{
        p: '3px',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
      elevation={0}
    >
      <Box
        sx={{
          flexGrow: 1,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <img
          alt=''
          src={illustration_avatar}
          style={{
            width: '100px',
          }}
        />
      </Box>
      <Box sx={{ flexGrow: 1 }}>
        <Typography
          variant='overline'
          color='text.disabled'
          gutterBottom
          component={'div'}
        >{`${app.getAppName()}`}</Typography>
        <Typography variant={'h6'} gutterBottom component={'div'}>
          {isStringEmpty(data?.helloText) ? `🎉新店开业` : data?.helloText}
        </Typography>
        <StyledButton
          size='small'
          onClick={() => {
            confetti({});
          }}
        >
          散花 ~
        </StyledButton>
      </Box>
    </Paper>
  );
}
