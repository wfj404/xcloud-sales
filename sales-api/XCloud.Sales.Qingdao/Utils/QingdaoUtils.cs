using System.Linq;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Extension;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Utils;

[ExposeServices(typeof(QingdaoUtils))]
public class QingdaoUtils : ISingletonDependency
{
    public bool TryParseAdminId(string externalId, out int id)
    {
        id = default;

        if (string.IsNullOrWhiteSpace(externalId))
        {
            return false;
        }

        var arr = externalId.Split('@').WhereNotEmpty().ToArray();

        if (arr.Length == 2 && arr[1] == "3h-admin")
        {
            if (int.TryParse(arr[0], out id))
            {
                return true;
            }
        }

        return false;
    }

    public string BuildAdminExternalId(WapAdminDto admin)
    {
        return $"{admin.userId}@3h-admin";
    }

    public string BuildStoreExternalId(WapOrganization org)
    {
        return $"{org.organizationId}@3h-admin-org";
    }

    public string BuildMcsUserExternalId(McsUserDto mcsUser)
    {
        return $"{mcsUser.mcsUserId}@3h-user";
    }
}