import { useRequest } from 'ahooks';
import { Alert, Button, Card, Descriptions, DescriptionsProps, message, Space } from 'antd';
import { useState } from 'react';

import { apiClient } from '@/utils/client';
import { AntDesignTreeNode1DepartmentDto, DepartmentDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty, isStringEmpty } from '@/utils/utils';

import XForm from './form';

export default ({
  node,
  ok,
}: {
  node?: AntDesignTreeNode1DepartmentDto;
  ok: () => void;
}) => {
  const department: DepartmentDto | undefined = node?.raw_data;
  const [formdata, _formdata] = useState<DepartmentDto | undefined>(undefined);

  const delete_request = useRequest(apiClient.sys.departmentDeleteById, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        message.success('删除成功');
        ok();
      });
    },
  });

  const renderDepartment = () => {
    if (!department) {
      return <Alert message="未选择部门" />;
    }

    const items: DescriptionsProps['items'] = [
      {
        label: '描述',
        children: <span>{department.Description}</span>,
      },
    ];

    return <Descriptions title={department.Name} items={items}></Descriptions>;
  };

  const renderButtons = () => {
    return (
      <Space direction="horizontal">
        <Button
          type="link"
          onClick={() => {
            _formdata({
              ParentId: department?.Id,
            });
          }}
        >
          添加子部门
        </Button>
        <Button
          type="link"
          disabled={isStringEmpty(department?.Id)}
          onClick={() => {
            _formdata({ ...department });
          }}
        >
          编辑
        </Button>
        <Button
          type="link"
          danger
          disabled={
            isStringEmpty(department?.Id) || !isArrayEmpty(node?.children)
          }
          loading={delete_request.loading}
          onClick={() => {
            if (confirm('确定要删除该部门吗？')) {
              delete_request.run({ Id: department?.Id });
            }
          }}
        >
          删除
        </Button>
      </Space>
    );
  };

  return (
    <>
      <XForm
        data={formdata || {}}
        show={formdata != undefined}
        ok={() => {
          _formdata(undefined);
          ok && ok();
        }}
        hide={() => _formdata(undefined)}
      />
      <Card style={{ marginBottom: 10 }} extra={renderButtons()}>
        {renderDepartment()}
      </Card>
    </>
  );
};
