using XCloud.Application.Service;
using XCloud.AspNetMvc.Core;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Service;

public abstract class PlatformAppService : XCloudAppService
{
    protected IWebHelper WebHelper => LazyServiceProvider.LazyGetRequiredService<IWebHelper>();

    protected AccountUtils AccountUtils =>
        LazyServiceProvider.LazyGetRequiredService<AccountUtils>();
}