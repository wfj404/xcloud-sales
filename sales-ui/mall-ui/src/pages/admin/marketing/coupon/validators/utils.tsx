

import { LabeledValueItem } from '@/utils/biz';

export type OrderValidatorType =
  | 'order-price'
  | 'store'
  | 'goods-category'
  | 'goods-brand'
  | 'user-grade';

export const OrderValidatorTypeOptions: LabeledValueItem[] = [
  {
    label: '订单满减',
    key: 'order-price',
  },
  {
    label: '特定门店',
    key: 'store',
  },
  {
    label: '特定商品分类',
    key: 'goods-category',
  },
  {
    label: '特定商品品牌',
    key: 'goods-brand',
  },
  {
    label: '特定用户等级',
    key: 'user-grade',
    disabled: true,
  },
];
