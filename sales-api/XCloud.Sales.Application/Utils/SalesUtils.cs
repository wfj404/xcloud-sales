using JetBrains.Annotations;
using XCloud.Sales.Application.Service;

namespace XCloud.Sales.Application.Utils;

[ExposeServices(typeof(SalesUtils))]
public class SalesUtils : SalesAppService
{
    public bool StatusEqual(string a, string b)
    {
        return string.Equals(a?.Trim(), b?.Trim(), StringComparison.OrdinalIgnoreCase);
    }

    [NotNull]
    public string NormalizeSeoName([CanBeNull] string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            return string.Empty;

        var ignoreChars = new List<char>
        {
            '/', '\\', '@', '#', '$', '|', '&',
            '+', '-', '*', '%', '^', '!',
            ':', ',', '[', ']', '{', '}', ';',
            ' '
        };

        var charArray = name.Where(x => ignoreChars.IndexOf(x) < 0).ToArray();

        name = string.Join(String.Empty, charArray);

        return name.ToLower();
    }
}