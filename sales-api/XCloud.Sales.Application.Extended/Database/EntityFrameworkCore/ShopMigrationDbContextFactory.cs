using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Steeltoe.Extensions.Configuration.Placeholder;
using XCloud.EntityFrameworkCore.MySql;
using XCloud.Sales.Application.Core;

namespace XCloud.Sales.Application.Extended.Database.EntityFrameworkCore;

public class ShopMigrationDbContextFactory : IDesignTimeDbContextFactory<ShopMySqlDbContext>
{
    public ShopMySqlDbContext CreateDbContext(string[] args)
    {
        var optionBuilder = new DbContextOptionsBuilder<ShopMySqlDbContext>();
        //optionBuilder.UseSqlite("./xx.db");
        optionBuilder.UseMySqlProvider(BuildConfiguration(), SalesConstants.DbConnectionNames.MySql);

        return new ShopMySqlDbContext(optionBuilder.Options);
    }

    private IConfigurationRoot BuildConfiguration()
    {
        var configBuilder = new ConfigurationBuilder();

        configBuilder.AddJsonFile(
            Path.Combine(Directory.GetCurrentDirectory(), "../XCloud.Sales.WebApi/appsettings.json"),
            optional: false);

        configBuilder.AddPlaceholderResolver();

        return configBuilder.Build();
    }
}