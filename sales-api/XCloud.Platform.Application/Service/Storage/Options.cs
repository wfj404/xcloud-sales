﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Application.Service.Storage;

public class ThumborOption : IEntityDto
{
    public bool Enabled { get; set; } = false;
}

public class DiskStorageOption : IEntityDto
{
    public string BasePath { get; set; }
}

public class StorageOption : IEntityDto
{
    public string Provider { get; set; }

    private ThumborOption _thumborOption;

    [NotNull]
    public ThumborOption Thumbor
    {
        get => this._thumborOption ??= new ThumborOption();
        set => this._thumborOption = value;
    }

    private DiskStorageOption _diskStorageOption;

    [NotNull]
    public DiskStorageOption Disk
    {
        get => _diskStorageOption ??= new DiskStorageOption();
        set => _diskStorageOption = value;
    }
}