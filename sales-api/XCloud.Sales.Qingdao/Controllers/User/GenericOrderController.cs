﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCloud.Application.Model;
using XCloud.Core.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Service.GenericOrders;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.Qingdao.Controllers.User;

[Route("api/mall/qingdao/generic-order")]
public class GenericOrderController : ShopBaseController
{
    private readonly IGenericOrderService _genericOrderService;
    private readonly OrderViewService _orderViewService;

    public GenericOrderController(IGenericOrderService genericOrderService, OrderViewService orderViewService)
    {
        _genericOrderService = genericOrderService;
        _orderViewService = orderViewService;
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<GenericOrderDto>> QueryPagingAsync([FromBody] QueryGenericOrderInput dto)
    {
        var loginUser = await this.StoreUserAuthService.GetRequiredStoreUserAsync();

        dto.UserId = loginUser.Id;

        var response = await this._genericOrderService.QueryPagingAsync(dto);

        await this._genericOrderService.AttachItemsAsync(response.Items.ToArray());
        await this._genericOrderService.AttachStoreUsersAsync(response.Items.ToArray());
        await this._genericOrderService.AttachXCloudSalesOrdersAsync(response.Items.ToArray());

        var orders = response.Items.Select(x => x.Order).WhereNotNull().ToArray();

        await this._orderViewService.AttachOrderDetailDataAsync(orders);

        return response;
    }
}