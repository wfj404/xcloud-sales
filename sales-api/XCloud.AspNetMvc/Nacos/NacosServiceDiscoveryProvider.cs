using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Nacos.V2;
using Volo.Abp.DependencyInjection;
using XCloud.Application.ServiceDiscovery;

namespace XCloud.AspNetMvc.Nacos;

[ExposeServices(typeof(IServiceDiscoveryProvider))]
public class NacosServiceDiscoveryProvider : IServiceDiscoveryProvider, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public NacosServiceDiscoveryProvider(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    public string ProviderName => nameof(NacosServiceDiscoveryProvider);

    public int Order => -1;

    public async Task<ServiceDiscoveryResponseDto> ResolveServiceOrNullAsync(string name)
    {
        var nacosNamingService = _serviceProvider.GetService<INacosNamingService>();
        if (nacosNamingService == null)
            return null;

        var instance = await nacosNamingService.SelectOneHealthyInstance(name, "DEFAULT_GROUP");

        if (instance == null || string.IsNullOrWhiteSpace(instance.Ip))
            return null;
        
        var host = $"{instance.Ip}:{instance.Port}";

        var baseUrl = instance.Metadata.TryGetValue("secure", out _)
            ? $"https://{host}"
            : $"http://{host}";

        return new ServiceDiscoveryResponseDto(baseUrl);
    }
}