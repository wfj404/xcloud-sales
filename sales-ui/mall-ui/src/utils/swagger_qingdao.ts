/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ActivityLog {
  /** @format int32 */
  ActivityLogTypeId?: number;
  AdministratorId?: string | null;
  BrowserType?: string | null;
  Comment?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Data?: string | null;
  Device?: string | null;
  GeoCity?: string | null;
  GeoCountry?: string | null;
  /** @format int32 */
  Id?: number;
  IpAddress?: string | null;
  /** @format double */
  Lat?: number | null;
  /** @format double */
  Lng?: number | null;
  ManagerId?: string | null;
  RequestPath?: string | null;
  /** @format int32 */
  StoreUserId?: number;
  SubjectId?: string | null;
  /** @format int32 */
  SubjectIntId?: number;
  SubjectType?: string | null;
  UrlReferrer?: string | null;
  UserAgent?: string | null;
  UserId?: string | null;
  Value?: string | null;
}

export interface ActivityLogDto {
  /** @format int32 */
  ActivityLogTypeId?: number;
  Admin?: AdminDto;
  AdministratorId?: string | null;
  BrowserType?: string | null;
  Comment?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Data?: string | null;
  Device?: string | null;
  GeoCity?: string | null;
  GeoCountry?: string | null;
  /** @format int32 */
  Id?: number;
  IpAddress?: string | null;
  /** @format double */
  Lat?: number | null;
  /** @format double */
  Lng?: number | null;
  ManagerId?: string | null;
  RequestPath?: string | null;
  StoreUser?: StoreUserDto;
  /** @format int32 */
  StoreUserId?: number;
  SubjectId?: string | null;
  /** @format int32 */
  SubjectIntId?: number;
  SubjectType?: string | null;
  UrlReferrer?: string | null;
  UserAgent?: string | null;
  UserId?: string | null;
  Value?: string | null;
}

export interface AddOrUpdateShoppingCartInput {
  /** @format int32 */
  Quantity?: number;
  SkuId?: string | null;
  /** @format int32 */
  UserId?: number;
  ValueAddedItemIds?: number[] | null;
}

export interface Admin {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsActive?: boolean;
  IsSuperAdmin?: boolean;
  UserId?: string | null;
}

export interface AdminAuthResult {
  Admin?: AdminDto;
  Data?: AdminDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface AdminDto {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsActive?: boolean;
  IsSuperAdmin?: boolean;
  PermissionKeys?: string[] | null;
  Roles?: RoleDto[] | null;
  User?: UserDto;
  UserId?: string | null;
}

export interface AdminGrantedPermissionResponse {
  Data?: AdminDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  HasAllPermission?: boolean;
  Permissions?: string[] | null;
}

export interface AfterSalesCommentDto {
  AfterSaleId?: string | null;
  AfterSales?: AfterSalesDto;
  Content?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsAdmin?: boolean;
  PictureJson?: string | null;
  Pictures?: StorageMetaDto[] | null;
}

export interface AfterSalesDto {
  AfterSalesStatusId?: string | null;
  /** @format date-time */
  CreationTime?: string;
  HideForCustomer?: boolean;
  Id?: string | null;
  Images?: string | null;
  Items?: AfterSalesItemDto[] | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  Order?: OrderDto;
  OrderId?: string | null;
  ReasonForReturn?: string | null;
  RequestedAction?: string | null;
  StaffNotes?: string | null;
  UserComments?: string | null;
}

export interface AfterSalesItemDto {
  AfterSalesId?: string | null;
  Id?: string | null;
  OrderId?: string | null;
  OrderItem?: OrderItemDto;
  OrderItemId?: string | null;
  /** @format int32 */
  Quantity?: number;
}

export interface AfterSalesSettings {
  AfterSaleDisabled?: boolean;
}

export interface AggregateRouteConfig {
  JsonPath?: string | null;
  Parameter?: string | null;
  RouteKey?: string | null;
}

export interface AntDesignTreeNode1CategoryDto {
  children?: AntDesignTreeNode1CategoryDto[] | null;
  key?: string | null;
  /** @format int32 */
  node_level?: number | null;
  raw_data?: CategoryDto;
  title?: string | null;
}

export interface ApiResponse1Admin {
  Data?: Admin;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1AfterSalesDto {
  Data?: AfterSalesDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1AntDesignTreeNode1Array1 {
  Data?: AntDesignTreeNode1CategoryDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1AuthTokenDto {
  Data?: AuthTokenDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1AvailableServiceTimeRangeDtoArray1 {
  Data?: AvailableServiceTimeRangeDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1BrandDtoArray1 {
  Data?: BrandDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1CategoryDto {
  Data?: CategoryDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1CategoryDtoArray1 {
  Data?: CategoryDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1CategoryPageDto {
  Data?: CategoryPageDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1DashboardCountView {
  Data?: DashboardCountView;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1Decimal {
  /** @format double */
  Data?: number;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1DeliveryRecord {
  Data?: DeliveryRecord;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1DeliveryRecordDto {
  Data?: DeliveryRecordDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1DeliveryRecordDtoArray1 {
  Data?: DeliveryRecordDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1DeliveryRecordItemArray1 {
  Data?: DeliveryRecordItem[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1FreightTemplate {
  Data?: FreightTemplate;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1FreightTemplateDtoArray1 {
  Data?: FreightTemplateDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1GiftCard {
  Data?: GiftCard;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1GiftCardDto {
  Data?: GiftCardDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1Goods {
  Data?: Goods;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1GoodsDto {
  Data?: GoodsDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1GoodsDtoArray1 {
  Data?: GoodsDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1GoodsGradePriceDtoArray1 {
  Data?: GoodsGradePriceDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1GradeArray1 {
  Data?: Grade[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1HomePageBlocksDto {
  Data?: HomePageBlocksDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1HomePageDto {
  Data?: HomePageDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1Int32 {
  /** @format int32 */
  Data?: number;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1MallSettingsDto {
  Data?: MallSettingsDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1McsInvoiceRequestArray1 {
  Data?: McsInvoiceRequest[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1Object {
  Data?: any;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1OrderDto {
  Data?: OrderDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1OrderNoteDtoArray1 {
  Data?: OrderNoteDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1OrderReview {
  Data?: OrderReview;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1OrderReviewDto {
  Data?: OrderReviewDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1OrderSumByDateResponseArray1 {
  Data?: OrderSumByDateResponse[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1PaymentBillDto {
  Data?: PaymentBillDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1PaymentBillDtoArray1 {
  Data?: PaymentBillDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1PickupRecord {
  Data?: PickupRecord;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1PickupRecordDto {
  Data?: PickupRecordDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1PickupRecordDtoArray1 {
  Data?: PickupRecordDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1QueryGoodsVisitReportResponseArray1 {
  Data?: QueryGoodsVisitReportResponse[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1QuerySearchKeywordsReportResponseArray1 {
  Data?: QuerySearchKeywordsReportResponse[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SearchOptionsDto {
  Data?: SearchOptionsDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SearchView {
  Data?: SearchView;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1ShoppingCartItemDtoArray1 {
  Data?: ShoppingCartItemDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1Sku {
  Data?: Sku;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SkuDto {
  Data?: SkuDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SkuDtoArray1 {
  Data?: SkuDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SpecDtoArray1 {
  Data?: SpecDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SpecGroupSelectResponse {
  Data?: SpecGroupSelectResponse;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StorageMetaDtoArray1 {
  Data?: StorageMetaDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreDto {
  Data?: StoreDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreDtoArray1 {
  Data?: StoreDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreExpressSettings {
  Data?: StoreExpressSettings;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreManager {
  Data?: StoreManager;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreManagerDtoArray1 {
  Data?: StoreManagerDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StorePickupSettings {
  Data?: StorePickupSettings;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreRoleDtoArray1 {
  Data?: StoreRoleDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreShortDistanceDeliverySettings {
  Data?: StoreShortDistanceDeliverySettings;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1StoreUser {
  Data?: StoreUser;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1SupplierDtoArray1 {
  Data?: SupplierDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1Tag {
  Data?: Tag;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TagDtoArray1 {
  Data?: TagDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TopAfterSaleMallUsersResponseArray1 {
  Data?: TopAfterSaleMallUsersResponse[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TopBrandListArray1 {
  Data?: TopBrandList[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TopCategoryListArray1 {
  Data?: TopCategoryList[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TopCustomersListArray1 {
  Data?: TopCustomersList[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TopSellersListArray1 {
  Data?: TopSellersList[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1TopSkuListArray1 {
  Data?: TopSkuList[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1User {
  Data?: User;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserActivityGroupByGeoLocationResponseArray1 {
  Data?: UserActivityGroupByGeoLocationResponse[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserActivityGroupByHourResponseArray1 {
  Data?: UserActivityGroupByHourResponse[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserAddress {
  Data?: UserAddress;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserAddressArray1 {
  Data?: UserAddress[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserDto {
  Data?: UserDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserDtoArray1 {
  Data?: UserDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserIdentity {
  Data?: UserIdentity;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1UserInvoiceArray1 {
  Data?: UserInvoice[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1ValueAddedItemGroupDtoArray1 {
  Data?: ValueAddedItemGroupDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1WapOrganization {
  Data?: WapOrganization;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApiResponse1WarehouseDtoArray1 {
  Data?: WarehouseDto[] | null;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
}

export interface ApproveAfterSaleDto {
  Comment?: string | null;
  Id?: string | null;
}

export interface AuthTokenDto {
  AccessToken?: string | null;
  /** @format date-time */
  ExpiredTime?: string | null;
  RefreshToken?: string | null;
}

export interface AvailableServiceTimeRangeDto {
  /** @format date-time */
  Date?: string;
  TimeRanges?: ServiceDateTimeRange[] | null;
}

export interface BalanceHistoryDto {
  /** @format int32 */
  ActionType?: number;
  /** @format double */
  Balance?: number;
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  /** @format double */
  LatestBalance?: number;
  Message?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface BrandDto {
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Description?: string | null;
  /** @format int32 */
  DisplayOrder?: number;
  Id?: string | null;
  IsDeleted?: boolean;
  MetaKeywords?: string | null;
  Name?: string | null;
  Picture?: StorageMetaDto;
  PictureMetaId?: string | null;
  Published?: boolean;
  ShowOnPublicPage?: boolean;
}

export interface CancelAfterSaleDto {
  Comment?: string | null;
  Id?: string | null;
}

export interface Category {
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Description?: string | null;
  /** @format int32 */
  DisplayOrder?: number;
  Id?: string | null;
  IsDeleted?: boolean;
  Name?: string | null;
  NodePath?: string | null;
  ParentCategoryId?: string | null;
  PictureMetaId?: string | null;
  Published?: boolean;
  Recommend?: boolean;
  RootId?: string | null;
  SeoName?: string | null;
  ShowOnHomePage?: boolean;
}

export interface CategoryDto {
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Description?: string | null;
  /** @format int32 */
  DisplayOrder?: number;
  Id?: string | null;
  IsDeleted?: boolean;
  Name?: string | null;
  NodePath?: string | null;
  ParentCategoryId?: string | null;
  ParentNodes?: Category[] | null;
  ParentNodesIds?: string[] | null;
  Picture?: StorageMetaDto;
  PictureMetaId?: string | null;
  Published?: boolean;
  Recommend?: boolean;
  RootId?: string | null;
  SeoName?: string | null;
  ShowOnHomePage?: boolean;
}

export interface CategoryPageDto {
  Root?: CategoryDto[] | null;
}

export interface CloseOrderInput {
  Comment?: string | null;
  OrderId?: string | null;
}

export interface CompleteAfterSaleDto {
  Comment?: string | null;
  Id?: string | null;
}

export interface CouponDto {
  /** @format int32 */
  AccountIssuedLimitCount?: number | null;
  /** @format int32 */
  Amount?: number;
  CanBeIssued?: boolean | null;
  /** @format int32 */
  CouponType?: number;
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  ExpiredInDays?: number | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  /** @format int32 */
  IssuedAmount?: number;
  LimitedToStore?: boolean;
  Name?: string | null;
  OrderConditionRulesJson?: string | null;
  /** @format double */
  Price?: number;
  /** @format date-time */
  StartTime?: string | null;
  Stores?: StoreDto[] | null;
  /** @format int32 */
  UsedAmount?: number;
}

export interface CouponUserMappingDto {
  Coupon?: CouponDto;
  CouponId?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  ExpiredAt?: string | null;
  /** @format int32 */
  Id?: number;
  IsUsed?: boolean;
  StoreUser?: StoreUserDto;
  /** @format date-time */
  UsedTime?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface CreateInvoiceRequestInput {
  bizIds?: string[] | null;
  userInvoiceId?: string | null;
}

export interface CronRule {
  Cron?: string | null;
  Name?: string | null;
}

export interface DangerouslyUpdateAfterSalesStatusDto {
  Id?: string | null;
  Status?: string | null;
}

export interface DashboardCountView {
  /** @format int32 */
  ActiveUser?: number;
  /** @format int32 */
  AfterSale?: number;
  /** @format double */
  Balance?: number;
  /** @format int32 */
  Brand?: number;
  /** @format int32 */
  Category?: number;
  /** @format int32 */
  Coupon?: number;
  /** @format int32 */
  Goods?: number;
  /** @format int32 */
  Orders?: number;
  /** @format double */
  PrepaidCard?: number;
  /** @format int32 */
  Promotion?: number;
  /** @format int32 */
  Tag?: number;
}

export interface DeliveryArea {
  AreaId?: string | null;
  CityId?: string | null;
  ProvinceId?: string | null;
}

export interface DeliveryRecord {
  /** @format date-time */
  CreationTime?: string;
  Delivered?: boolean;
  /** @format date-time */
  DeliveredTime?: string | null;
  Delivering?: boolean;
  /** @format date-time */
  DeliveringTime?: string | null;
  DeliveryType?: string | null;
  ExpressName?: string | null;
  Id?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  OrderId?: string | null;
  /** @format date-time */
  PreferDeliveryEndTime?: string | null;
  /** @format date-time */
  PreferDeliveryStartTime?: string | null;
  TrackingNumber?: string | null;
}

export interface DeliveryRecordDto {
  /** @format date-time */
  CreationTime?: string;
  Delivered?: boolean;
  /** @format date-time */
  DeliveredTime?: string | null;
  Delivering?: boolean;
  /** @format date-time */
  DeliveringTime?: string | null;
  DeliveryType?: string | null;
  ExpressName?: string | null;
  Id?: string | null;
  Items?: DeliveryRecordItemDto[] | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  Order?: OrderDto;
  OrderId?: string | null;
  /** @format date-time */
  PreferDeliveryEndTime?: string | null;
  /** @format date-time */
  PreferDeliveryStartTime?: string | null;
  TrackingNumber?: string | null;
}

export interface DeliveryRecordItem {
  DeliveryId?: string | null;
  Id?: string | null;
  OrderItemId?: string | null;
  /** @format int32 */
  Quantity?: number;
}

export interface DeliveryRecordItemDto {
  DeliveryId?: string | null;
  Id?: string | null;
  OrderItem?: OrderItemDto;
  OrderItemId?: string | null;
  /** @format int32 */
  Quantity?: number;
}

export interface DeliveryRule {
  /** @format int32 */
  AddedUnitAmount?: number;
  /** @format double */
  AddedUnitPrice?: number;
  Areas?: DeliveryArea[] | null;
  IsDefault?: boolean;
  /** @format int32 */
  StartingAmount?: number;
  /** @format double */
  StartingPrice?: number;
}

export interface DeliverySettings {
  /** @format int32 */
  MultipleDeliveryTemplateCalculateType?: number;
  ShippingFeeFree?: boolean;
}

export interface Discount {
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  /** @format double */
  DiscountPercentage?: number;
  /** @format date-time */
  EndTime?: string | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  LimitedToSku?: boolean;
  LimitedToStore?: boolean;
  Name?: string | null;
  /** @format int32 */
  Sort?: number;
  /** @format date-time */
  StartTime?: string | null;
}

export interface DiscountDto {
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  /** @format double */
  DiscountPercentage?: number;
  /** @format date-time */
  EndTime?: string | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  LimitedToSku?: boolean;
  LimitedToStore?: boolean;
  Name?: string | null;
  Skus?: SkuDto[] | null;
  /** @format int32 */
  Sort?: number;
  /** @format date-time */
  StartTime?: string | null;
  Stores?: StoreDto[] | null;
}

export interface ExceptionTime {
  /** @format date-time */
  EndTime?: string;
  Include?: boolean;
  Name?: string | null;
  /** @format date-time */
  StartTime?: string;
}

export interface FileAggregateRoute {
  Aggregator?: string | null;
  /** @format int32 */
  Priority?: number;
  RouteIsCaseSensitive?: boolean;
  RouteKeys?: string[] | null;
  RouteKeysConfig?: AggregateRouteConfig[] | null;
  UpstreamHost?: string | null;
  UpstreamHttpMethod?: string[] | null;
  UpstreamPathTemplate?: string | null;
}

export interface FileAuthenticationOptions {
  AllowedScopes?: string[] | null;
  AuthenticationProviderKey?: string | null;
}

export interface FileCacheOptions {
  Region?: string | null;
  /** @format int32 */
  TtlSeconds?: number;
}

export interface FileConfiguration {
  Aggregates?: FileAggregateRoute[] | null;
  DynamicRoutes?: FileDynamicRoute[] | null;
  GlobalConfiguration?: FileGlobalConfiguration;
  Routes?: FileRoute[] | null;
}

export interface FileDynamicRoute {
  DownstreamHttpVersion?: string | null;
  RateLimitRule?: FileRateLimitRule;
  ServiceName?: string | null;
}

export interface FileGlobalConfiguration {
  BaseUrl?: string | null;
  DownstreamHttpVersion?: string | null;
  DownstreamScheme?: string | null;
  HttpHandlerOptions?: FileHttpHandlerOptions;
  LoadBalancerOptions?: FileLoadBalancerOptions;
  QoSOptions?: FileQoSOptions;
  RateLimitOptions?: FileRateLimitOptions;
  RequestIdKey?: string | null;
  ServiceDiscoveryProvider?: FileServiceDiscoveryProvider;
}

export interface FileHostAndPort {
  Host?: string | null;
  /** @format int32 */
  Port?: number;
}

export interface FileHttpHandlerOptions {
  AllowAutoRedirect?: boolean;
  /** @format int32 */
  MaxConnectionsPerServer?: number;
  UseCookieContainer?: boolean;
  UseProxy?: boolean;
  UseTracing?: boolean;
}

export interface FileLoadBalancerOptions {
  /** @format int32 */
  Expiry?: number;
  Key?: string | null;
  Type?: string | null;
}

export interface FileQoSOptions {
  /** @format int32 */
  DurationOfBreak?: number;
  /** @format int32 */
  ExceptionsAllowedBeforeBreaking?: number;
  /** @format int32 */
  TimeoutValue?: number;
}

export interface FileRateLimitOptions {
  ClientIdHeader?: string | null;
  DisableRateLimitHeaders?: boolean;
  /** @format int32 */
  HttpStatusCode?: number;
  QuotaExceededMessage?: string | null;
  RateLimitCounterPrefix?: string | null;
}

export interface FileRateLimitRule {
  ClientWhitelist?: string[] | null;
  EnableRateLimiting?: boolean;
  /** @format int64 */
  Limit?: number;
  Period?: string | null;
  /** @format double */
  PeriodTimespan?: number;
}

export interface FileRoute {
  AddClaimsToRequest?: Record<string, string | null>;
  AddHeadersToRequest?: Record<string, string | null>;
  AddQueriesToRequest?: Record<string, string | null>;
  AuthenticationOptions?: FileAuthenticationOptions;
  ChangeDownstreamPathTemplate?: Record<string, string | null>;
  DangerousAcceptAnyServerCertificateValidator?: boolean;
  DelegatingHandlers?: string[] | null;
  DownstreamHeaderTransform?: Record<string, string | null>;
  DownstreamHostAndPorts?: FileHostAndPort[] | null;
  DownstreamHttpMethod?: string | null;
  DownstreamHttpVersion?: string | null;
  DownstreamPathTemplate?: string | null;
  DownstreamScheme?: string | null;
  FileCacheOptions?: FileCacheOptions;
  HttpHandlerOptions?: FileHttpHandlerOptions;
  Key?: string | null;
  LoadBalancerOptions?: FileLoadBalancerOptions;
  /** @format int32 */
  Priority?: number;
  QoSOptions?: FileQoSOptions;
  RateLimitOptions?: FileRateLimitRule;
  RequestIdKey?: string | null;
  RouteClaimsRequirement?: Record<string, string | null>;
  RouteIsCaseSensitive?: boolean;
  SecurityOptions?: FileSecurityOptions;
  ServiceName?: string | null;
  ServiceNamespace?: string | null;
  /** @format int32 */
  Timeout?: number;
  UpstreamHeaderTransform?: Record<string, string | null>;
  UpstreamHost?: string | null;
  UpstreamHttpMethod?: string[] | null;
  UpstreamPathTemplate?: string | null;
}

export interface FileSecurityOptions {
  IPAllowedList?: string[] | null;
  IPBlockedList?: string[] | null;
}

export interface FileServiceDiscoveryProvider {
  ConfigurationKey?: string | null;
  Host?: string | null;
  Namespace?: string | null;
  /** @format int32 */
  PollingInterval?: number;
  /** @format int32 */
  Port?: number;
  Scheme?: string | null;
  Token?: string | null;
  Type?: string | null;
}

export interface FinishOrderInput {
  Comment?: string | null;
  OrderId?: string | null;
}

export interface FreightTemplate {
  /** @format int32 */
  CalculateType?: number;
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  ExcludedAreaJson?: string | null;
  /** @format double */
  FreeShippingMinOrderTotalPrice?: number;
  Id?: string | null;
  Name?: string | null;
  RulesJson?: string | null;
  /** @format int32 */
  ShipInHours?: number | null;
  /** @format int32 */
  Sort?: number;
  StoreId?: string | null;
}

export interface FreightTemplateDto {
  /** @format int32 */
  CalculateType?: number;
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  ExcludedAreaJson?: string | null;
  ExcludedAreas?: DeliveryArea[] | null;
  /** @format double */
  FreeShippingMinOrderTotalPrice?: number;
  Id?: string | null;
  Name?: string | null;
  Rules?: DeliveryRule[] | null;
  RulesJson?: string | null;
  /** @format int32 */
  ShipInHours?: number | null;
  /** @format int32 */
  Sort?: number;
  StoreId?: string | null;
}

export interface GenericOrderDto {
  Closed?: boolean;
  /** @format date-time */
  ClosedTime?: string | null;
  /** @format date-time */
  CreationTime?: string;
  ExtraDataJson?: string | null;
  Finished?: boolean;
  /** @format date-time */
  FinishedTime?: string | null;
  Id?: string | null;
  Items?: GenericOrderItemDto[] | null;
  ItemsJson?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  Order?: OrderDto;
  OrderId?: string | null;
  OrderType?: string | null;
  Paid?: boolean;
  Reviewed?: boolean;
  Serviced?: boolean;
  StoreUser?: StoreUserDto;
  /** @format int32 */
  StoreUserId?: number;
  ToService?: boolean;
}

export interface GenericOrderItemDto {
  GoodsName?: string | null;
  Pictures?: StorageMetaDto[] | null;
  /** @format double */
  Price?: number | null;
  /** @format int32 */
  Quantity?: number | null;
}

export interface GeoLocationDto {
  /** @format double */
  Lat?: number;
  /** @format double */
  Lon?: number;
}

export interface GetOrgByLocationInput {
  BizType?: string | null;
  Location?: GeoLocationDto;
}

export interface GiftCard {
  /** @format double */
  Amount?: number;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  EndTime?: string | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  Used?: boolean;
  /** @format date-time */
  UsedTime?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface GiftCardDto {
  /** @format double */
  Amount?: number;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  EndTime?: string | null;
  Expired?: boolean | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  StoreUser?: StoreUserDto;
  Used?: boolean;
  /** @format date-time */
  UsedTime?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface Goods {
  AdminComment?: string | null;
  AfterSalesSupported?: boolean;
  /** @format double */
  ApprovedReviewsRates?: number;
  /** @format int32 */
  ApprovedTotalReviews?: number;
  BrandId?: string | null;
  CategoryId?: string | null;
  CommentSupported?: boolean;
  ContactTelephone?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  DetailInformation?: string | null;
  DisplayAsInformationPage?: boolean;
  /** @format int32 */
  GoodsType?: number;
  Id?: string | null;
  IsDeleted?: boolean;
  Keywords?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  LimitedToContact?: boolean;
  LimitedToRedirect?: boolean;
  Name?: string | null;
  PayAfterDeliveredSupported?: boolean;
  PlaceOrderSupported?: boolean;
  Published?: boolean;
  RedirectToUrlJson?: string | null;
  SeoName?: string | null;
  StickyTop?: boolean;
  StockDeductStrategy?: string | null;
}

export interface GoodsDto {
  AdminComment?: string | null;
  AfterSalesSupported?: boolean;
  /** @format double */
  ApprovedReviewsRates?: number;
  /** @format int32 */
  ApprovedTotalReviews?: number;
  Brand?: BrandDto;
  BrandId?: string | null;
  Category?: CategoryDto;
  CategoryId?: string | null;
  CommentSupported?: boolean;
  ContactTelephone?: string | null;
  /** @format date-time */
  CreationTime?: string;
  CurrentFreightTemplate?: FreightTemplateDto;
  Description?: string | null;
  DetailInformation?: string | null;
  DisplayAsInformationPage?: boolean;
  GoodsPictures?: GoodsPictureDto[] | null;
  /** @format int32 */
  GoodsType?: number;
  Id?: string | null;
  IsDeleted?: boolean;
  Keywords?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  LimitedToContact?: boolean;
  LimitedToRedirect?: boolean;
  Name?: string | null;
  PayAfterDeliveredSupported?: boolean;
  PlaceOrderSupported?: boolean;
  Published?: boolean;
  RedirectToUrl?: GoodsRedirectToUrlDto;
  RedirectToUrlJson?: string | null;
  SeoName?: string | null;
  Skus?: SkuDto[] | null;
  StickyTop?: boolean;
  StockDeductStrategy?: string | null;
  Tags?: TagDto[] | null;
  ValueAddedItems?: ValueAddedItemDto[] | null;
}

export interface GoodsGradePriceDto {
  /** @format date-time */
  CreationTime?: string;
  Grade?: Grade;
  GradeId?: string | null;
  Id?: string | null;
  /** @format double */
  PriceOffset?: number;
  SkuId?: string | null;
}

export interface GoodsPictureDto {
  /** @format int32 */
  DisplayOrder?: number;
  FileName?: string | null;
  GoodsId?: string | null;
  /** @format int32 */
  Id?: number;
  PictureMetaId?: string | null;
  SkuId?: string | null;
  StorageMeta?: StorageMetaDto;
}

export interface GoodsRedirectToUrlDto {
  H5Url?: string | null;
  AlipayMiniProgramUrl?: string | null;
  AppUrl?: string | null;
  WebUrl?: string | null;
  WxMiniProgramUrl?: string | null;
}

export interface GoodsSettings {
  DisplayPriceForGuest?: boolean;
}

export interface Grade {
  Description?: string | null;
  Id?: string | null;
  IsDeleted?: boolean;
  Name?: string | null;
  /** @format int32 */
  Sort?: number;
  /** @format int32 */
  UserCount?: number;
}

export interface GradeDto {
  Description?: string | null;
  Id?: string | null;
  IsDeleted?: boolean;
  Name?: string | null;
  /** @format int32 */
  Sort?: number;
  /** @format int32 */
  UserCount?: number;
}

export interface HomePageBlocksDto {
  Blocks?: string | null;
}

export interface HomePageCategoryAndGoodsDto {
  Category?: CategoryDto;
  Goods?: GoodsDto[] | null;
  /** @format int32 */
  Order?: number;
}

export interface HomePageDto {
  CategoryAndGoods?: HomePageCategoryAndGoodsDto[] | null;
  HotGoods?: GoodsDto[] | null;
}

export interface IdDto {
  Id?: string | null;
}

export interface IdDto1Int32 {
  /** @format int32 */
  Id?: number;
}

export interface IdDto1String {
  Id?: string | null;
}

export interface IdentityNameDto {
  IdentityName?: string | null;
}

export interface IssueCouponInput {
  CouponId?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface KeyValuePair2StringBoolean {
  Key?: string | null;
  Value?: boolean;
}

export interface KeyValuePair2StringNullable1Decimal {
  Key?: string | null;
  /** @format double */
  Value?: number | null;
}

export interface KeyValuePair2StringStringArray1 {
  Key?: string | null;
  Value?: string[] | null;
}

export interface ListByTagNameInput {
  /** @format int32 */
  Count?: number | null;
  Name?: string | null;
}

export interface MallSettingsDto {
  AfterSalesSettings?: AfterSalesSettings;
  DeliverySettings?: DeliverySettings;
  FilingNumber?: string | null;
  FooterSlogan?: string | null;
  FullLogoStorageData?: string | null;
  GoodsDetailNotice?: string | null;
  GoodsSettings?: GoodsSettings;
  /** @deprecated */
  HomePageCategorySeoNames?: string | null;
  HomePageNotice?: string | null;
  HomeSliderImages?: string | null;
  LoginNotice?: string | null;
  OrderSettings?: OrderSettings;
  PaymentSettings?: PaymentSettings;
  PlaceOrderNotice?: string | null;
  RegisterNotice?: string | null;
  ShoppingCartSettings?: ShoppingCartSettings;
  SimpleLogoStorageData?: string | null;
  StoreEnglishName?: string | null;
  StoreNickName?: string | null;
  StoreOfficialEnglishName?: string | null;
  StoreOfficialName?: string | null;
}

export interface McsBizOrderDto {
  AvailableForInvoicing?: boolean;
  billDate?: string | null;
  bizId?: string | null;
  /** @format int32 */
  bizType?: number;
  /** @deprecated */
  canInvoicing?: boolean;
  chargeDate?: string | null;
  details?: McsBizOrderItemDto[] | null;
  /** @format int32 */
  outletId?: number;
  outletTitle?: string | null;
  payTime?: string | null;
  remark?: string | null;
  sourceType?: string | null;
  title?: string | null;
  /** @format double */
  totalMoney?: number;
  userId?: string | null;
}

export interface McsBizOrderItemDto {
  introduce?: string | null;
  /** @format double */
  money?: number;
  /** @format int32 */
  productId?: number;
  productName?: string | null;
  remark?: string | null;
}

export interface McsInvoiceRequest {
  AttachmentMetaId?: string | null;
  BizType?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Email?: string | null;
  Id?: string | null;
  /** @format date-time */
  InvoicingTime?: string | null;
  IsDeleted?: boolean;
  McsUserId?: string | null;
  Picked?: boolean;
  /** @format date-time */
  PickedTime?: string | null;
  /** @format int32 */
  PickupLocationId?: number;
  PickupLocationName?: string | null;
  Status?: string | null;
  /** @format double */
  TotalAmount?: number;
  UserInvoiceData?: string | null;
  UserInvoiceId?: string | null;
}

export interface McsInvoiceRequestDto {
  AttachmentMetaId?: string | null;
  BizType?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Email?: string | null;
  Id?: string | null;
  /** @format date-time */
  InvoicingTime?: string | null;
  IsDeleted?: boolean;
  Items?: McsInvoiceRequestItemDto[] | null;
  McsUser?: McsUserDto;
  McsUserId?: string | null;
  Picked?: boolean;
  /** @format date-time */
  PickedTime?: string | null;
  /** @format int32 */
  PickupLocationId?: number;
  PickupLocationName?: string | null;
  Status?: string | null;
  StorageMeta?: StorageMetaDto;
  /** @format double */
  TotalAmount?: number;
  UserInvoice?: UserInvoiceDto;
  UserInvoiceData?: string | null;
  UserInvoiceId?: string | null;
}

export interface McsInvoiceRequestItemDto {
  Description?: string | null;
  Id?: string | null;
  InvoiceRequestId?: string | null;
  McsBizOrder?: McsBizOrderDto;
  McsBizOrderId?: string | null;
  McsInvoiceRequest?: McsInvoiceRequestDto;
  /** @format double */
  Price?: number;
}

export interface McsUserDto {
  headImgUrl?: string | null;
  mcsUserId?: string | null;
  nickName?: string | null;
}

export interface NotificationDto {
  ActionType?: string | null;
  App?: string | null;
  BusinessId?: string | null;
  Content?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Data?: string | null;
  Id?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  Read?: boolean;
  /** @format date-time */
  ReadTime?: string | null;
  SenderId?: string | null;
  SenderType?: string | null;
  Title?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface OAuthCodeDto {
  Code?: string | null;
  UseWechatProfile?: boolean | null;
}

export interface OrderActions {
  ToPay?: boolean | null;
  ToReview?: boolean | null;
  ToService?: boolean | null;
}

export interface OrderCouponRecordDto {
  Coupon?: CouponDto;
  CouponUserMapping?: CouponUserMappingDto;
  Id?: string | null;
  OrderId?: string | null;
  /** @format int32 */
  UserCouponId?: number;
}

export interface OrderDto {
  Actions?: OrderActions;
  /** @format int32 */
  AffiliateId?: number;
  AfterSales?: AfterSalesDto;
  /** @format date-time */
  CompleteTime?: string | null;
  /** @format double */
  CouponPrice?: number;
  /** @format date-time */
  CreationTime?: string;
  DeliveryRecord?: DeliveryRecordDto;
  /** @format double */
  ExchangePointsAmount?: number;
  /** @format int32 */
  GoodsType?: number;
  GradeId?: string | null;
  HideForCustomer?: boolean;
  Id?: string | null;
  IsAfterSales?: boolean;
  Items?: OrderItemDto[] | null;
  /** @format double */
  ItemsTotalPrice?: number;
  /** @format date-time */
  LastModificationTime?: string | null;
  OrderCouponRecords?: OrderCouponRecordDto[] | null;
  OrderIp?: string | null;
  OrderReview?: OrderReviewDto;
  OrderSn?: string | null;
  OrderStatusId?: string | null;
  /** @format double */
  PackageFee?: number;
  /** @format double */
  PaidTotalPrice?: number;
  PaymentMethodId?: string | null;
  PaymentStatusId?: string | null;
  PickupRecord?: PickupRecordDto;
  PlatformId?: string | null;
  PromotionId?: string | null;
  /** @format double */
  PromotionPriceOffset?: number;
  /** @format double */
  RefundedAmount?: number;
  Remark?: string | null;
  /** @format int32 */
  RewardPointsHistoryId?: number | null;
  /** @format double */
  SellerPriceOffset?: number;
  /** @format date-time */
  ServiceEndTime?: string | null;
  /** @format date-time */
  ServiceStartTime?: string | null;
  /** @format double */
  ShippingFee?: number;
  ShippingMethodId?: string | null;
  ShippingStatusId?: string | null;
  Store?: StoreDto;
  StoreId?: string | null;
  StoreUser?: StoreUserDto;
  /** @format double */
  TotalPrice?: number;
  UserAddress?: UserAddressDto;
  UserAddressId?: string | null;
  /** @format int32 */
  UserId?: number;
  UserInvoice?: UserInvoiceDto;
  UserInvoiceId?: string | null;
}

export interface OrderItemDto {
  /** @format double */
  BasePrice?: number;
  /** @format double */
  DiscountRate?: number;
  GoodsName?: string | null;
  /** @format double */
  GradePriceOffset?: number;
  Id?: string | null;
  OrderId?: string | null;
  OrderItemGifts?: OrderItemGiftDto[] | null;
  OrderValueAddedItems?: OrderValueAddedItemDto[] | null;
  /** @format int32 */
  Quantity?: number;
  Remark?: string | null;
  Sku?: SkuDto;
  SkuDescription?: string | null;
  SkuId?: string | null;
  SkuName?: string | null;
  SpecItems?: SpecItemDto[] | null;
  StockDeductStrategy?: string | null;
  /** @format double */
  TotalPrice?: number;
  /** @format double */
  UnitPrice?: number;
  /** @format double */
  ValueAddedPriceOffset?: number;
  /** @format int32 */
  Weight?: number;
}

export interface OrderItemGiftDto {
  Id?: string | null;
  OrderItemId?: string | null;
  /** @format int32 */
  Quantity?: number;
  SkuId?: string | null;
}

export interface OrderNoteDto {
  /** @format date-time */
  CreationTime?: string;
  DisplayToUser?: boolean;
  ExtendedJson?: string | null;
  /** @format int32 */
  Id?: number;
  Note?: string | null;
  OrderId?: string | null;
  OrderNoteExtended?: OrderNoteExtended;
}

export interface OrderNoteExtended {
  Data?: Record<string, string | null>;
  NoteType?: string | null;
}

export interface OrderReview {
  Approved?: boolean;
  /** @format date-time */
  CreationTime?: string;
  ExtraData?: string | null;
  Id?: string | null;
  IpAddress?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  OrderId?: string | null;
  PictureMetaIdsJson?: string | null;
  /** @format double */
  Rating?: number;
  ReviewText?: string | null;
}

export interface OrderReviewDto {
  Approved?: boolean;
  /** @format date-time */
  CreationTime?: string;
  ExtraData?: string | null;
  Id?: string | null;
  IpAddress?: string | null;
  Items?: OrderReviewItemDto[] | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  MetaIds?: string[] | null;
  OrderId?: string | null;
  PictureMetaIdsJson?: string | null;
  /** @format double */
  Rating?: number;
  ReviewText?: string | null;
  StorageMeta?: StorageMetaDto[] | null;
  StoreUser?: StoreUserDto;
}

export interface OrderReviewItemDto {
  Id?: string | null;
  MetaIds?: string[] | null;
  OrderItem?: OrderItemDto;
  OrderItemId?: string | null;
  OrderReview?: OrderReviewDto;
  PictureMetaIdsJson?: string | null;
  /** @format double */
  Rating?: number;
  ReviewId?: string | null;
  ReviewText?: string | null;
  StorageMeta?: StorageMetaDto[] | null;
}

export interface OrderSettings {
  PlaceOrderDisabled?: boolean;
  /** @format int32 */
  ReviewEntryExpiredInDays?: number | null;
}

export interface OrderSumByDateInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface OrderSumByDateResponse {
  /** @format double */
  Amount?: number;
  /** @format date-time */
  Date?: string;
  /** @format int32 */
  Total?: number;
}

export interface OrderValueAddedItemDto {
  Description?: string | null;
  Id?: string | null;
  Name?: string | null;
  OrderItemId?: string | null;
  /** @format double */
  PriceOffset?: number;
  ValueAddedItem?: ValueAddedItemDto;
  /** @format int32 */
  ValueAddedItemId?: number;
}

export interface PagedRequest {
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface PagedResponse1ActivityLogDto {
  Data?: ActivityLogDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: ActivityLogDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1AdminDto {
  Data?: AdminDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: AdminDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1AfterSalesCommentDto {
  Data?: AfterSalesCommentDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: AfterSalesCommentDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1AfterSalesDto {
  Data?: AfterSalesDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: AfterSalesDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1BalanceHistoryDto {
  Data?: BalanceHistoryDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: BalanceHistoryDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1BrandDto {
  Data?: BrandDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: BrandDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1CouponDto {
  Data?: CouponDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: CouponDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1CouponUserMappingDto {
  Data?: CouponUserMappingDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: CouponUserMappingDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1DeliveryRecordDto {
  Data?: DeliveryRecordDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: DeliveryRecordDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1DiscountDto {
  Data?: DiscountDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: DiscountDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1GenericOrderDto {
  Data?: GenericOrderDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: GenericOrderDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1GiftCardDto {
  Data?: GiftCardDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: GiftCardDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1GoodsDto {
  Data?: GoodsDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: GoodsDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1McsBizOrderDto {
  Data?: McsBizOrderDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: McsBizOrderDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1McsInvoiceRequestDto {
  Data?: McsInvoiceRequestDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: McsInvoiceRequestDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1NotificationDto {
  Data?: NotificationDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: NotificationDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1OrderDto {
  Data?: OrderDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: OrderDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1OrderReviewDto {
  Data?: OrderReviewDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: OrderReviewDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1OrderReviewItemDto {
  Data?: OrderReviewItemDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: OrderReviewItemDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1PaymentBillDto {
  Data?: PaymentBillDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: PaymentBillDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1PickupRecordDto {
  Data?: PickupRecordDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: PickupRecordDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1PointsHistoryDto {
  Data?: PointsHistoryDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: PointsHistoryDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1RoleDto {
  Data?: RoleDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: RoleDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1SettingDto {
  Data?: SettingDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: SettingDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1SkuDto {
  Data?: SkuDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: SkuDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1StockDto {
  Data?: StockDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: StockDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1StockItemDto {
  Data?: StockItemDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: StockItemDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1StoreGoodsMappingDto {
  Data?: StoreGoodsMappingDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: StoreGoodsMappingDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1StoreUserDto {
  Data?: StoreUserDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: StoreUserDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1SupplierDto {
  Data?: SupplierDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: SupplierDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PagedResponse1UserDto {
  Data?: UserDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  IsEmpty?: boolean;
  IsNotEmpty?: boolean;
  Items?: UserDto[] | null;
  /** @format int32 */
  PageIndex?: number;
  /** @format int32 */
  PageSize?: number;
  /** @format int64 */
  TotalCount?: number;
  /** @format int32 */
  TotalPages?: number;
}

export interface PasswordLoginDto {
  IdentityName?: string | null;
  Password?: string | null;
}

export interface PaymentBillDto {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsDeleted?: boolean;
  NotifyData?: string | null;
  Order?: OrderDto;
  OrderId?: string | null;
  Paid?: boolean;
  /** @format date-time */
  PayTime?: string | null;
  PaymentChannel?: string | null;
  PaymentTransactionId?: string | null;
  /** @format double */
  Price?: number;
}

export type PaymentSettings = object;

export interface PickupRecord {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  OrderId?: string | null;
  Picked?: boolean;
  /** @format date-time */
  PickedTime?: string | null;
  /** @format date-time */
  PreferPickupEndTime?: string | null;
  /** @format date-time */
  PreferPickupStartTime?: string | null;
  Prepared?: boolean;
  /** @format date-time */
  PreparedTime?: string | null;
  ReservationApproved?: boolean | null;
  /** @format date-time */
  ReservationApprovedTime?: string | null;
  SecurityCode?: string | null;
}

export interface PickupRecordDto {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  Order?: OrderDto;
  OrderId?: string | null;
  Picked?: boolean;
  /** @format date-time */
  PickedTime?: string | null;
  /** @format date-time */
  PreferPickupEndTime?: string | null;
  /** @format date-time */
  PreferPickupStartTime?: string | null;
  Prepared?: boolean;
  /** @format date-time */
  PreparedTime?: string | null;
  ReservationApproved?: boolean | null;
  /** @format date-time */
  ReservationApprovedTime?: string | null;
  SecurityCode?: string | null;
}

export interface PlaceOrderRequestDto {
  AddressId?: string | null;
  DeliveryType?: string | null;
  InvoiceId?: string | null;
  Items?: PlaceOrderRequestItemDto[] | null;
  PaymentMethodId?: string | null;
  PlatformId?: string | null;
  /** @format date-time */
  PreferDeliveryEndTime?: string | null;
  /** @format date-time */
  PreferDeliveryStartTime?: string | null;
  Remark?: string | null;
  ShippingMethodId?: string | null;
  StoreId?: string | null;
  /** @format int32 */
  UserCouponId?: number;
  /** @format int32 */
  UserId?: number;
}

export interface PlaceOrderRequestItemDto {
  /** @format int32 */
  Quantity?: number;
  Remark?: string | null;
  SkuId?: string | null;
  ValueAddedItemIds?: number[] | null;
}

export interface PlaceOrderResult {
  CouponErrors?: string[] | null;
  Data?: OrderDto;
  Error?: RemoteServiceErrorInfo;
  Errors?: string[] | null;
  ExtraProperties?: Record<string, any>;
  PaymentErrors?: string[] | null;
  ShippingErrors?: string[] | null;
  SkuErrors?: PlaceOrderSkuError[] | null;
}

export interface PlaceOrderSkuError {
  Data?: SkuDto;
  ErrorList?: string[] | null;
  ExtraProperties?: Record<string, any>;
  Id?: string | null;
}

export interface PointsHistoryDto {
  /** @format int32 */
  ActionType?: number;
  /** @format date-time */
  CreationTime?: string;
  /** @format int32 */
  Id?: number;
  Message?: string | null;
  OrderId?: string | null;
  /** @format int32 */
  Points?: number;
  /** @format int32 */
  PointsBalance?: number;
  /**
   * @deprecated
   * @format double
   */
  UsedAmount?: number;
  /** @format int32 */
  UserId?: number;
}

export interface PriceModel {
  /** @format double */
  BasePrice?: number;
  DiscountModel?: DiscountDto;
  Empty?: boolean;
  ExtraProperties?: Record<string, any>;
  /** @format double */
  FinalPrice?: number;
  GradePriceModel?: GoodsGradePriceDto;
}

export interface QueryActivityLogPagingInput {
  /** @format int32 */
  ActivityLogTypeId?: number | null;
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QueryAdminPaging {
  IsActive?: boolean | null;
  IsDeleted?: boolean | null;
  Keyword?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryAfterSalePaging {
  /** @format date-time */
  EndTime?: string | null;
  IsAfterSalesPending?: boolean | null;
  IsDeleted?: boolean | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  Status?: string[] | null;
  StoreId?: string | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QueryAfterSalesCommentPaging {
  AfterSalesId?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryBalancePagingInput {
  /** @format int32 */
  ActionType?: number | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format int32 */
  UserId?: number;
}

export interface QueryBrandPaging {
  Name?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  Published?: boolean;
  ShowOnHomePage?: boolean;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryCouponPagingInput {
  /** @format date-time */
  AvailableFor?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  StoreId?: string | null;
}

export interface QueryDeliveryRecordPagingInput {
  OrderId?: string | null;
  OrderToService?: boolean | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  StoreId?: string | null;
}

export interface QueryDiscountPagingInput {
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  StoreId?: string | null;
  /** @format date-time */
  Time?: string | null;
}

export interface QueryGenericOrderInput {
  Closed?: boolean | null;
  /** @format date-time */
  EndTime?: string | null;
  Finished?: boolean | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  Paid?: boolean | null;
  Reviewed?: boolean | null;
  Serviced?: boolean | null;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  ToService?: boolean | null;
  /** @format int32 */
  UserId?: number;
}

export interface QueryGiftCardPagingInput {
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  Used?: boolean | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QueryGoodsForSelection {
  Keyword?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryGoodsPaging {
  AlwaysShowFunctionalGoods?: boolean | null;
  BrandId?: string | null;
  CategoryId?: string | null;
  IsPublished?: boolean | null;
  Keywords?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  StoreId?: string | null;
  TagId?: string | null;
  WithoutBrand?: boolean | null;
  WithoutCategory?: boolean | null;
}

export interface QueryGoodsVisitReportInput {
  /** @format int32 */
  Count?: number | null;
  /** @format date-time */
  EndTime?: string | null;
  GoodsId?: string | null;
  /** @format date-time */
  StartTime?: string | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QueryGoodsVisitReportResponse {
  Goods?: GoodsDto;
  GoodsId?: string | null;
  /** @format int32 */
  VisitedCount?: number;
}

export interface QueryMcsBizOrderForInvoicingInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  QueryType?: string | null;
  QueryValue?: string | null;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  Status?: string | null;
}

export interface QueryMcsInvoiceRequestInput {
  BizOrderId?: string | null;
  BizType?: string | null;
  /** @format date-time */
  EndTime?: string | null;
  McsUserId?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  Picked?: boolean | null;
  /** @format int32 */
  PickupLocationId?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  Status?: string | null;
}

export interface QueryNotificationPagingInput {
  AppKey?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format int32 */
  UserId?: number;
}

export interface QueryOrderBillPagingInput {
  OrderId?: string | null;
  OrderNo?: string | null;
  OutTradeNo?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  Paid?: boolean | null;
  PaymentMethod?: string | null;
  PaymentTransactionId?: string | null;
  RefundTransactionId?: string | null;
  Refunded?: boolean | null;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryOrderNotesInput {
  /** @format int32 */
  MaxCount?: number | null;
  OnlyForUser?: boolean;
  OrderId?: string | null;
}

export interface QueryOrderPagingInput {
  /** @format int32 */
  AffiliateId?: number | null;
  AfterSalesStatus?: string[] | null;
  DeliveryStatus?: string[] | null;
  /** @format date-time */
  EndTime?: string | null;
  HideForCustomer?: boolean | null;
  IsAfterSales?: boolean | null;
  IsAfterSalesPending?: boolean | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  PaymentMethodId?: string[] | null;
  PaymentStatus?: string[] | null;
  ShippingMethodId?: string[] | null;
  SkipCalculateTotalCount?: boolean | null;
  Sn?: string | null;
  /** @format date-time */
  StartTime?: string | null;
  Status?: string[] | null;
  StoreId?: string | null;
  ToReview?: boolean | null;
  ToService?: boolean | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QueryOrderReviewItemPaging {
  /** @format date-time */
  EndTime?: string | null;
  GoodsId?: string | null;
  IsApproved?: boolean | null;
  Keywords?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  StoreId?: string | null;
}

export interface QueryOrderReviewPaging {
  /** @format date-time */
  EndTime?: string | null;
  IsApproved?: boolean | null;
  Keywords?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  StoreId?: string | null;
}

export interface QueryPickupPagingInput {
  Approved?: boolean | null;
  OrderToService?: boolean | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  StoreId?: string | null;
}

export interface QueryPointsPagingInput {
  /** @format int32 */
  ActionType?: number | null;
  OrderId?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format int32 */
  UserId?: number;
}

export interface QueryRolePaging {
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QuerySearchKeywordsReportInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QuerySearchKeywordsReportResponse {
  /** @format int32 */
  Count?: number;
  Keywords?: string | null;
}

export interface QuerySettingsPagingInput {
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QuerySkuBySelectedItemsDto {
  GoodsId?: string | null;
  Specs?: SpecItemDto[] | null;
}

export interface QuerySkuForSelectionInput {
  ExcludedSkuIds?: string[] | null;
  Keywords?: string | null;
  /** @format int32 */
  Take?: number;
}

export interface QuerySkuPaging {
  IsActive?: boolean | null;
  Keywords?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  Sku?: string | null;
  StoreId?: string | null;
}

export interface QueryStoreGoodsMappingPagingInput {
  Active?: boolean | null;
  Keywords?: string | null;
  /** @format double */
  MaxPrice?: number | null;
  /** @format double */
  MinPrice?: number | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  Sku?: string | null;
  StoreId?: string | null;
}

export interface QuerySupplierPagingInput {
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryTopBrandListInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface QueryTopCategoryListInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface QueryTopCustomerInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface QueryTopSellerInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface QueryTopSkuListInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface QueryUserCouponPagingInput {
  /** @format date-time */
  AvailableFor?: string | null;
  IsDeleted?: boolean | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  Used?: boolean | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface QueryUserPaging {
  IsActive?: boolean | null;
  Keyword?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
}

export interface QueryUserPagingInput {
  Active?: boolean | null;
  /** @format date-time */
  EndTime?: string | null;
  GlobalUserIds?: string[] | null;
  GradeId?: string | null;
  OnlyWithShoppingCart?: boolean;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface QueryWarehouseStockItemInput {
  Approved?: boolean | null;
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  RunningOut?: boolean | null;
  SerialNo?: string | null;
  SkipCalculateTotalCount?: boolean | null;
  SkuId?: string | null;
  /** @format date-time */
  StartTime?: string | null;
  SupplierId?: string | null;
  WarehouseId?: string | null;
}

export interface QueryWarehouseStockPagingInput {
  Approved?: boolean | null;
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  Page?: number;
  /** @format int32 */
  PageSize?: number;
  SerialNo?: string | null;
  SkipCalculateTotalCount?: boolean | null;
  /** @format date-time */
  StartTime?: string | null;
  SupplierId?: string | null;
  WarehouseId?: string | null;
}

export interface RefreshViewCacheInput {
  Dashboard?: boolean;
  Home?: boolean;
  MallSettings?: boolean;
  RootCategory?: boolean;
  Search?: boolean;
}

export interface RejectAfterSaleInput {
  Comment?: string | null;
  Id?: string | null;
}

export interface RemoteServiceErrorInfo {
  Code?: string | null;
  Data?: Record<string, any>;
  Details?: string | null;
  Message?: string | null;
  ValidationErrors?: RemoteServiceValidationErrorInfo[] | null;
}

export interface RemoteServiceValidationErrorInfo {
  Members?: string[] | null;
  Message?: string | null;
}

export interface RoleDto {
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  Id?: string | null;
  Name?: string | null;
  PermissionKeys?: string[] | null;
}

export interface SaveGoodsStoreExpressTemplateInput {
  GoodsId?: string | null;
  StoreId?: string | null;
  TemplateIds?: string[] | null;
}

export interface SaveStoreMappingInput1Discount {
  Entity?: Discount;
  StoreIds?: string[] | null;
}

export interface SearchOptionsDto {
  Brand?: BrandDto;
  Category?: CategoryDto;
  Tag?: Tag;
}

export interface SearchUserAccountDto {
  AccountIdentity?: string | null;
}

export interface SearchView {
  Brands?: BrandDto[] | null;
  Keywords?: string[] | null;
  Tags?: TagDto[] | null;
}

export interface SendSmsDto {
  CaptchaCode?: string | null;
  Mobile?: string | null;
}

export interface ServiceDateTimeRange {
  /** @format date-time */
  End?: string;
  /** @format date-time */
  Start?: string;
}

export interface ServiceTimeDto {
  CronRules?: CronRule[] | null;
  ExceptionTimes?: ExceptionTime[] | null;
  TimeRanges?: TimeRangeDto[] | null;
}

export interface SetAdminRolesDto {
  Id?: string | null;
  RoleIds?: string[] | null;
}

export interface SetAsAdminDto {
  UserId?: string | null;
}

export interface SetPermissionToRoleDto {
  Id?: string | null;
  PermissionKeys?: string[] | null;
}

export interface SetUserMobileDto {
  Id?: string | null;
  Mobile?: string | null;
}

export interface SetUserPasswordDto {
  Id?: string | null;
  Password?: string | null;
}

export interface SettingDto {
  Id?: string | null;
  Name?: string | null;
  Value?: string | null;
}

export interface ShoppingCartItemDto {
  /** @format date-time */
  CreationTime?: string;
  /** @format int32 */
  Id?: number;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format int32 */
  Quantity?: number;
  Sku?: SkuDto;
  SkuId?: string | null;
  Specs?: SpecItemDto[] | null;
  /** @format int32 */
  UserId?: number;
  ValueAddedItems?: ValueAddedItemDto[] | null;
  Waring?: string[] | null;
}

export type ShoppingCartSettings = object;

export interface Sku {
  Color?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Description?: string | null;
  GoodsId?: string | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format int32 */
  MaxAmountInOnePurchase?: number;
  /** @format int32 */
  MinAmountInOnePurchase?: number;
  Name?: string | null;
  /** @format double */
  Price?: number;
  SkuCode?: string | null;
  SpecCombinationJson?: string | null;
  /** @format int32 */
  Weight?: number;
}

export interface SkuDto {
  Color?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Description?: string | null;
  Discounts?: DiscountDto[] | null;
  Goods?: GoodsDto;
  GoodsId?: string | null;
  GoodsPictures?: GoodsPictureDto[] | null;
  GradePrices?: GoodsGradePriceDto[] | null;
  Id?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format int32 */
  MaxAmountInOnePurchase?: number;
  /** @format int32 */
  MinAmountInOnePurchase?: number;
  Name?: string | null;
  /** @format double */
  Price?: number;
  PriceModel?: PriceModel;
  /** @format int32 */
  ShoppingCartQuantity?: number | null;
  SkuCode?: string | null;
  SpecCombinationErrors?: string[] | null;
  SpecCombinationJson?: string | null;
  SpecItemList?: SpecItemDto[] | null;
  StockModel?: StockModel;
  StoreGoodsMapping?: StoreGoodsMappingDto[] | null;
  /** @format int32 */
  Weight?: number;
}

export interface SmsLoginDto {
  Mobile?: string | null;
  SmsCode?: string | null;
}

export interface SpecDto {
  GoodsId?: string | null;
  /** @format int32 */
  Id?: number;
  IsDeleted?: boolean;
  Name?: string | null;
  Values?: SpecValue[] | null;
}

export interface SpecGroup {
  /** @format int32 */
  Id?: number;
  Items?: SpecGroupItem[] | null;
  Name?: string | null;
}

export interface SpecGroupItem {
  /** @format int32 */
  Id?: number;
  IsActive?: boolean;
  IsSelected?: boolean;
  Name?: string | null;
}

export interface SpecGroupSelectResponse {
  FilteredSkus?: SkuDto[] | null;
  SpecGroup?: SpecGroup[] | null;
}

export interface SpecItemDto {
  Spec?: SpecDto;
  /** @format int32 */
  SpecId?: number;
  SpecValue?: SpecValueDto;
  /** @format int32 */
  SpecValueId?: number;
}

export interface SpecValue {
  /** @format int32 */
  AssociatedSkuId?: number;
  /** @format int32 */
  Id?: number;
  IsDeleted?: boolean;
  Name?: string | null;
  /** @format double */
  PriceOffset?: number;
  /** @format int32 */
  SpecId?: number;
}

export interface SpecValueDto {
  /** @format int32 */
  AssociatedSkuId?: number;
  /** @format int32 */
  Id?: number;
  IsDeleted?: boolean;
  Name?: string | null;
  /** @format double */
  PriceOffset?: number;
  Spec?: SpecDto;
  /** @format int32 */
  SpecId?: number;
}

export interface StockDto {
  Approved?: boolean;
  ApprovedByUserId?: string | null;
  /** @format date-time */
  ApprovedTime?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  Items?: StockItemDto[] | null;
  No?: string | null;
  Remark?: string | null;
  Supplier?: SupplierDto;
  SupplierId?: string | null;
  Warehouse?: WarehouseDto;
  WarehouseId?: string | null;
}

export interface StockItemDto {
  /** @format int32 */
  DeductQuantity?: number;
  Goods?: GoodsDto;
  Id?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format double */
  Price?: number;
  /** @format int32 */
  Quantity?: number;
  Sku?: SkuDto;
  SkuId?: string | null;
  Stock?: StockDto;
  StockId?: string | null;
}

export interface StockModel {
  Description?: string | null;
  ExtraProperties?: Record<string, any>;
  Provider?: string | null;
  /** @format int32 */
  StockQuantity?: number;
  StoreId?: string | null;
}

export interface StorageMetaDto {
  ContentType?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Error?: string | null;
  ExtraData?: string | null;
  FileExtension?: string | null;
  HashType?: string | null;
  Id?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format int32 */
  ReferenceCount?: number;
  ResourceHash?: string | null;
  ResourceKey?: string | null;
  /** @format int64 */
  ResourceSize?: number;
  StorageProvider?: string | null;
  /** @format int32 */
  UploadTimes?: number;
  Url?: string | null;
}

export interface StoreDto {
  AddressDetail?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  /** @format double */
  Distance?: number | null;
  ExternalId?: string | null;
  Id?: string | null;
  InServiceArea?: boolean | null;
  InvoiceSupported?: boolean;
  IsDeleted?: boolean;
  /** @format double */
  Lat?: number;
  /** @format double */
  Lon?: number;
  Name?: string | null;
  Opening?: boolean;
  Picture?: StorageMetaDto;
  Telephone?: string | null;
}

export interface StoreExpressSettings {
  Enabled?: boolean;
  PayAfterShippedSupported?: boolean;
}

export interface StoreGoodsMappingDto {
  Active?: boolean;
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  /** @format date-time */
  LastModificationTime?: string | null;
  OverridePrice?: boolean;
  /** @format double */
  Price?: number;
  Sku?: SkuDto;
  SkuId?: string | null;
  /** @format int32 */
  StockQuantity?: number;
  Store?: StoreDto;
  StoreId?: string | null;
}

export interface StoreManager {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsActive?: boolean;
  IsSuperManager?: boolean;
  /** @format int32 */
  Role?: number;
  StoreId?: string | null;
  UserId?: string | null;
}

export interface StoreManagerAuthResult {
  Data?: StoreManagerDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  StoreManager?: StoreManagerDto;
}

export interface StoreManagerDto {
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsActive?: boolean;
  IsSuperManager?: boolean;
  /** @format int32 */
  Role?: number;
  Store?: StoreDto;
  StoreId?: string | null;
  StoreRoles?: StoreRoleDto[] | null;
  User?: UserDto;
  UserId?: string | null;
}

export interface StoreManagerGrantedPermissionResponse {
  Data?: StoreManagerDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  HasAllPermission?: boolean;
  Permissions?: string[] | null;
}

export interface StorePickupSettings {
  Enabled?: boolean;
  PayAfterShippedSupported?: boolean;
  ServiceTime?: ServiceTimeDto;
}

export interface StoreRoleDto {
  /** @format date-time */
  CreationTime?: string;
  Description?: string | null;
  Id?: string | null;
  Name?: string | null;
  PermissionKeys?: string[] | null;
  StoreId?: string | null;
}

export interface StoreShortDistanceDeliverySettings {
  Areas?: DeliveryArea[] | null;
  Enabled?: boolean;
  PayAfterShippedSupported?: boolean;
  ServiceTime?: ServiceTimeDto;
}

export interface StoreUser {
  /** @format double */
  Balance?: number;
  /** @format date-time */
  CreationTime?: string;
  /** @format int32 */
  HistoryPoints?: number;
  /** @format int32 */
  Id?: number;
  IsActive?: boolean;
  /** @format date-time */
  LastActivityTime?: string;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format int32 */
  Points?: number;
  UserId?: string | null;
}

export interface StoreUserAuthResult {
  Data?: StoreUserDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  StoreUser?: StoreUserDto;
}

export interface StoreUserDto {
  /** @format double */
  Balance?: number;
  /** @format date-time */
  CreationTime?: string;
  Grade?: Grade;
  /** @format int32 */
  HistoryPoints?: number;
  /** @format int32 */
  Id?: number;
  IsActive?: boolean;
  /** @format date-time */
  LastActivityTime?: string;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format int32 */
  Points?: number;
  User?: UserDto;
  UserId?: string | null;
}

export interface StoreUserGradeMappingDto {
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  EndTime?: string | null;
  GradeId?: string | null;
  Id?: string | null;
  /** @format date-time */
  StartTime?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface SupplierDto {
  Address?: string | null;
  ContactName?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsDeleted?: boolean;
  Name?: string | null;
  Telephone?: string | null;
}

export interface Tag {
  Alert?: string | null;
  Description?: string | null;
  Id?: string | null;
  IsDeleted?: boolean;
  Link?: string | null;
  Name?: string | null;
}

export interface TagDto {
  Alert?: string | null;
  Description?: string | null;
  Id?: string | null;
  IsDeleted?: boolean;
  Link?: string | null;
  Name?: string | null;
}

export interface TimeRangeDto {
  /** @format double */
  EndSecond?: number;
  /** @format double */
  StartSecond?: number;
}

export interface TopAfterSaleMallUsersInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
}

export interface TopAfterSaleMallUsersResponse {
  /** @format double */
  Amount?: number;
  /** @format int32 */
  Count?: number;
  StoreUser?: StoreUserDto;
  /** @format int32 */
  UserId?: number;
}

export interface TopBrandList {
  BrandId?: string | null;
  BrandName?: string | null;
  /** @format double */
  TotalPrice?: number;
  /** @format int32 */
  TotalQuantity?: number;
}

export interface TopCategoryList {
  CategoryId?: string | null;
  CategoryName?: string | null;
  RootCategoryId?: string | null;
  RootCategoryName?: string | null;
  /** @format double */
  TotalPrice?: number;
  /** @format int32 */
  TotalQuantity?: number;
}

export interface TopCustomersList {
  /** @format int32 */
  CustomerId?: number;
  CustomerName?: string | null;
  GlobalUserId?: string | null;
  /** @format double */
  TotalPrice?: number;
  /** @format int32 */
  TotalQuantity?: number;
}

export interface TopSellersList {
  GlobalUserId?: string | null;
  /** @format int32 */
  SellerId?: number;
  SellerName?: string | null;
  /** @format double */
  TotalPrice?: number;
  /** @format int32 */
  TotalQuantity?: number;
}

export interface TopSkuList {
  Name?: string | null;
  SkuId?: string | null;
  /** @format double */
  TotalPrice?: number;
  /** @format int32 */
  TotalQuantity?: number;
}

export interface UpdateAdminStatusDto {
  Id?: string | null;
  IsActive?: boolean | null;
  IsSuperAdmin?: boolean | null;
}

export interface UpdateAfterSaleStatusDto {
  Id?: string | null;
  IsDeleted?: boolean | null;
}

export interface UpdateGiftCardStatusInput {
  Id?: string | null;
  IsActive?: boolean | null;
  IsDeleted?: boolean | null;
}

export interface UpdateGradeStatusInput {
  Id?: string | null;
  IsDeleted?: boolean | null;
}

export interface UpdateNotificationStatusInput {
  Id?: string | null;
  Read?: boolean | null;
}

export interface UpdateOrderStatusInput {
  DeliveryStatus?: string | null;
  Id?: string | null;
  OrderStatus?: string | null;
  PaymentStatus?: string | null;
}

export interface UpdateShoppingCartQuantityInput {
  /** @format int32 */
  Id?: number;
  /** @format int32 */
  Quantity?: number;
}

export interface UpdateSupplierStatusInput {
  Id?: string | null;
  IsDeleted?: boolean | null;
}

export interface UpdateUserInvoiceStatusInput {
  Id?: string | null;
  IsDeleted?: boolean | null;
}

export interface UpdateUserStatusDto {
  Id?: string | null;
  IsActive?: boolean | null;
  IsDeleted?: boolean | null;
}

export interface UpdateUserStatusInput {
  /** @format int32 */
  Id?: number;
  IsActive?: boolean | null;
}

export interface UpdateWarehouseStatusInput {
  Id?: string | null;
  IsDeleted?: boolean | null;
}

export interface UseGiftCardInput {
  CardId?: string | null;
  /** @format int32 */
  UserId?: number;
}

export interface User {
  Avatar?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  ExternalId?: string | null;
  /** @format int32 */
  Gender?: number;
  Id?: string | null;
  IdentityName?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format date-time */
  LastPasswordUpdateTime?: string | null;
  NickName?: string | null;
  OriginIdentityName?: string | null;
  PassWord?: string | null;
}

export interface UserActivityGroupByGeoLocationInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format int32 */
  MaxCount?: number | null;
  /** @format date-time */
  StartTime?: string | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface UserActivityGroupByGeoLocationResponse {
  City?: string | null;
  /** @format int32 */
  Count?: number;
  Country?: string | null;
}

export interface UserActivityGroupByHourInput {
  /** @format date-time */
  EndTime?: string | null;
  /** @format date-time */
  StartTime?: string | null;
  /** @format int32 */
  UserId?: number | null;
}

export interface UserActivityGroupByHourResponse {
  /** @format int32 */
  ActivityType?: number;
  /** @format int32 */
  Count?: number;
  /** @format int32 */
  Hour?: number;
}

export interface UserAddress {
  AddressDescription?: string | null;
  AddressDetail?: string | null;
  Area?: string | null;
  AreaCode?: string | null;
  City?: string | null;
  CityCode?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  ExtendedJson?: string | null;
  Id?: string | null;
  IsDefault?: boolean;
  IsDeleted?: boolean;
  /** @format double */
  Lat?: number | null;
  /** @format double */
  Lon?: number | null;
  Name?: string | null;
  Nation?: string | null;
  NationCode?: string | null;
  PostalCode?: string | null;
  Province?: string | null;
  ProvinceCode?: string | null;
  Tel?: string | null;
  UserId?: string | null;
}

export interface UserAddressDto {
  AddressDescription?: string | null;
  AddressDetail?: string | null;
  Area?: string | null;
  AreaCode?: string | null;
  City?: string | null;
  CityCode?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  ExtendedJson?: string | null;
  Id?: string | null;
  IsDefault?: boolean;
  IsDeleted?: boolean;
  /** @format double */
  Lat?: number | null;
  /** @format double */
  Lon?: number | null;
  Name?: string | null;
  Nation?: string | null;
  NationCode?: string | null;
  PostalCode?: string | null;
  Province?: string | null;
  ProvinceCode?: string | null;
  Tel?: string | null;
  UserId?: string | null;
}

export interface UserAuthResult {
  Data?: UserDto;
  Error?: RemoteServiceErrorInfo;
  ExtraProperties?: Record<string, any>;
  TokenIsExpired?: boolean | null;
  User?: UserDto;
}

export interface UserDto {
  AdminIdentity?: AdminDto;
  Avatar?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  ExternalId?: string | null;
  /** @format int32 */
  Gender?: number;
  Id?: string | null;
  IdentityName?: string | null;
  IsActive?: boolean;
  IsDeleted?: boolean;
  /** @format date-time */
  LastModificationTime?: string | null;
  /** @format date-time */
  LastPasswordUpdateTime?: string | null;
  NickName?: string | null;
  OriginIdentityName?: string | null;
  UserEmails?: UserEmailDto[] | null;
  UserMobile?: UserMobileDto;
  UserRealName?: UserRealNameDto;
}

export interface UserEmailDto {
  Confirmed?: boolean | null;
  Email?: string | null;
  Id?: string | null;
}

export interface UserIdentity {
  /** @format date-time */
  CreationTime?: string;
  Data?: string | null;
  Email?: string | null;
  EmailConfirmed?: boolean | null;
  /** @format date-time */
  EmailConfirmedTimeUtc?: string | null;
  Id?: string | null;
  Identity?: string | null;
  /** @format int32 */
  IdentityType?: number;
  MobileAreaCode?: string | null;
  MobileConfirmed?: boolean | null;
  /** @format date-time */
  MobileConfirmedTimeUtc?: string | null;
  MobilePhone?: string | null;
  UserId?: string | null;
}

export interface UserInvoice {
  Address?: string | null;
  BankCode?: string | null;
  BankName?: string | null;
  Code?: string | null;
  Content?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Head?: string | null;
  Id?: string | null;
  /** @format int32 */
  InvoiceType?: number;
  IsDeleted?: boolean;
  PhoneNumber?: string | null;
  UserId?: string | null;
}

export interface UserInvoiceDto {
  Address?: string | null;
  BankCode?: string | null;
  BankName?: string | null;
  Code?: string | null;
  Content?: string | null;
  /** @format date-time */
  CreationTime?: string;
  /** @format date-time */
  DeletionTime?: string | null;
  Head?: string | null;
  Id?: string | null;
  /** @format int32 */
  InvoiceType?: number;
  IsDeleted?: boolean;
  PhoneNumber?: string | null;
  UserId?: string | null;
}

export interface UserMobileDto {
  Confirmed?: boolean | null;
  Id?: string | null;
  MobilePhone?: string | null;
}

export interface UserRealNameDto {
  /** @format date-time */
  CreationTime?: string;
  Data?: string | null;
  /** @format date-time */
  EndTimeUtc?: string | null;
  Id?: string | null;
  IdCard?: string | null;
  IdCardBackUrl?: string | null;
  IdCardConfirmed?: boolean | null;
  /** @format date-time */
  IdCardConfirmedTimeUtc?: string | null;
  IdCardFrontUrl?: string | null;
  IdCardHandUrl?: string | null;
  IdCardIdentity?: string | null;
  IdCardRealName?: string | null;
  /** @format int32 */
  IdCardStatus?: number;
  /** @format int32 */
  IdCardType?: number;
  /** @format date-time */
  StartTimeUtc?: string | null;
  UserId?: string | null;
}

export interface ValueAddedItemDto {
  Description?: string | null;
  GroupId?: string | null;
  /** @format int32 */
  Id?: number;
  Name?: string | null;
  /** @format double */
  PriceOffset?: number;
}

export interface ValueAddedItemGroupDto {
  Description?: string | null;
  GoodsId?: string | null;
  Id?: string | null;
  IsRequired?: boolean;
  Items?: ValueAddedItemDto[] | null;
  MultipleChoice?: boolean;
  Name?: string | null;
}

export interface WapOrganization {
  organizationCode?: string | null;
  /** @format int32 */
  organizationId?: number;
  organizationKey?: string | null;
  organizationName?: string | null;
  organizationType?: string | null;
  /** @format int32 */
  parentOrganizationId?: number;
}

export interface WarehouseDto {
  Address?: string | null;
  /** @format date-time */
  CreationTime?: string;
  Id?: string | null;
  IsDeleted?: boolean;
  /** @format double */
  Lat?: number | null;
  /** @format double */
  Lng?: number | null;
  Name?: string | null;
}

export interface WechatMpOption {
  AppId?: string | null;
  AppSecret?: string | null;
  Payment?: WechatPaymentOption;
}

export interface WechatOpenOption {
  AppId?: string | null;
  AppSecret?: string | null;
  Payment?: WechatPaymentOption;
}

export interface WechatPaymentOption {
  ApiV3Key?: string | null;
  ApiKey?: string | null;
  ApiPrivateKey?: string | null;
  Certificate?: string | null;
  CertificatePassword?: string | null;
  MchId?: string | null;
  PaymentNotifyUrl?: string | null;
  RefundNotifyUrl?: string | null;
  RsaPublicKey?: string | null;
  SubAppId?: string | null;
  SubMchId?: string | null;
}

import type {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  HeadersDefaults,
  ResponseType,
} from 'axios';
import axios from 'axios';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams
  extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<
  FullRequestParams,
  'body' | 'method' | 'query' | 'path'
>;

export interface ApiConfig<SecurityDataType = unknown>
  extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
  securityWorker?: (
    securityData: SecurityDataType | null,
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
  Text = 'text/plain',
}

export class HttpClient<SecurityDataType = unknown> {
  public instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({
    securityWorker,
    secure,
    format,
    ...axiosConfig
  }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({
      ...axiosConfig,
      baseURL: axiosConfig.baseURL || '',
    });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected mergeRequestParams(
    params1: AxiosRequestConfig,
    params2?: AxiosRequestConfig,
  ): AxiosRequestConfig {
    const method = params1.method || (params2 && params2.method);

    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...((method &&
          this.instance.defaults.headers[
            method.toLowerCase() as keyof HeadersDefaults
          ]) ||
          {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected stringifyFormItem(formItem: unknown) {
    if (typeof formItem === 'object' && formItem !== null) {
      return JSON.stringify(formItem);
    } else {
      return `${formItem}`;
    }
  }

  protected createFormData(input: Record<string, unknown>): FormData {
    return Object.keys(input || {}).reduce((formData, key) => {
      const property = input[key];
      const propertyContent: any[] =
        property instanceof Array ? property : [property];

      for (const formItem of propertyContent) {
        const isFileType = formItem instanceof Blob || formItem instanceof File;
        formData.append(
          key,
          isFileType ? formItem : this.stringifyFormItem(formItem),
        );
      }

      return formData;
    }, new FormData());
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = format || this.format || undefined;

    if (
      type === ContentType.FormData &&
      body &&
      body !== null &&
      typeof body === 'object'
    ) {
      body = this.createFormData(body as Record<string, unknown>);
    }

    if (
      type === ContentType.Text &&
      body &&
      body !== null &&
      typeof body !== 'string'
    ) {
      body = JSON.stringify(body);
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData
          ? { 'Content-Type': type }
          : {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
    });
  };
}

/**
 * @title mall
 */
export class Api<
  SecurityDataType extends unknown,
> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags FileConfiguration
   * @name FileConfigurationGet
   * @request GET:/configuration
   * @secure
   */
  fileConfigurationGet = (params: RequestParams = {}) =>
    this.request<void, any>({
      path: `/configuration`,
      method: 'GET',
      secure: true,
      ...params,
    });

  /**
   * No description
   *
   * @tags FileConfiguration
   * @name FileConfigurationPost
   * @request POST:/configuration
   * @secure
   */
  fileConfigurationPost = (
    data: FileConfiguration,
    params: RequestParams = {},
  ) =>
    this.request<void, any>({
      path: `/configuration`,
      method: 'POST',
      body: data,
      secure: true,
      type: ContentType.Json,
      ...params,
    });

  mallAdmin = {
    /**
     * No description
     *
     * @tags ActivityLog
     * @name ActivityLogQueryActivityLogPaging
     * @request POST:/api/mall-admin/activity-log/log-paging
     * @secure
     */
    activityLogQueryActivityLogPaging: (
      data: QueryActivityLogPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1ActivityLogDto, any>({
        path: `/api/mall-admin/activity-log/log-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ActivityLog
     * @name ActivityLogDeleteActivityLog
     * @request POST:/api/mall-admin/activity-log/delete-log
     * @secure
     */
    activityLogDeleteActivityLog: (
      data: IdDto1Int32,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/activity-log/delete-log`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Balance
     * @name BalanceUpdateUserBalance
     * @request POST:/api/mall-admin/balance/update-user-balance
     * @secure
     */
    balanceUpdateUserBalance: (
      data: BalanceHistoryDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/balance/update-user-balance`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandListBrand
     * @request POST:/api/mall-admin/brand/list
     * @secure
     */
    brandListBrand: (data: QueryBrandPaging, params: RequestParams = {}) =>
      this.request<ApiResponse1BrandDtoArray1, any>({
        path: `/api/mall-admin/brand/list`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandQueryPaging
     * @request POST:/api/mall-admin/brand/paging
     * @secure
     */
    brandQueryPaging: (data: QueryBrandPaging, params: RequestParams = {}) =>
      this.request<PagedResponse1BrandDto, any>({
        path: `/api/mall-admin/brand/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandSaveBrand
     * @request POST:/api/mall-admin/brand/save
     * @secure
     */
    brandSaveBrand: (data: BrandDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/brand/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandDelete
     * @request POST:/api/mall-admin/brand/delete
     * @secure
     */
    brandDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/brand/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Category
     * @name CategoryQueryAntdTree
     * @request POST:/api/mall-admin/category/tree
     * @secure
     */
    categoryQueryAntdTree: (params: RequestParams = {}) =>
      this.request<ApiResponse1AntDesignTreeNode1Array1, any>({
        path: `/api/mall-admin/category/tree`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Category
     * @name CategorySaveCategory
     * @request POST:/api/mall-admin/category/save
     * @secure
     */
    categorySaveCategory: (data: Category, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/category/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Category
     * @name CategoryDelete
     * @request POST:/api/mall-admin/category/delete
     * @secure
     */
    categoryDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/category/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponQueryCouponPaging
     * @request POST:/api/mall-admin/coupon/paging
     * @secure
     */
    couponQueryCouponPaging: (
      data: QueryCouponPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1CouponDto, any>({
        path: `/api/mall-admin/coupon/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponCreateUserCoupon
     * @request POST:/api/mall-admin/coupon/create-user-coupon
     * @secure
     */
    couponCreateUserCoupon: (
      data: CouponUserMappingDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/coupon/create-user-coupon`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponQueryUserCouponPaging
     * @request POST:/api/mall-admin/coupon/user-coupon-paging
     * @secure
     */
    couponQueryUserCouponPaging: (
      data: QueryUserCouponPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1CouponUserMappingDto, any>({
        path: `/api/mall-admin/coupon/user-coupon-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponSaveCoupon
     * @request POST:/api/mall-admin/coupon/save
     * @secure
     */
    couponSaveCoupon: (data: CouponDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/coupon/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponDelete
     * @request POST:/api/mall-admin/coupon/delete
     * @secure
     */
    couponDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/coupon/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Discount
     * @name DiscountSaveStoreMapping
     * @request POST:/api/mall-admin/discount/save-discount-store-mapping
     * @secure
     */
    discountSaveStoreMapping: (
      data: SaveStoreMappingInput1Discount,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/discount/save-discount-store-mapping`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Discount
     * @name DiscountQueryPaging
     * @request POST:/api/mall-admin/discount/paging
     * @secure
     */
    discountQueryPaging: (
      data: QueryDiscountPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1DiscountDto, any>({
        path: `/api/mall-admin/discount/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Discount
     * @name DiscountSave
     * @request POST:/api/mall-admin/discount/save
     * @secure
     */
    discountSave: (data: DiscountDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/discount/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Discount
     * @name DiscountDelete
     * @request POST:/api/mall-admin/discount/delete
     * @secure
     */
    discountDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/discount/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GiftCard
     * @name GiftCardQueryUnusedTotal
     * @request POST:/api/mall-admin/gift-card/unused-total
     * @secure
     */
    giftCardQueryUnusedTotal: (params: RequestParams = {}) =>
      this.request<ApiResponse1Decimal, any>({
        path: `/api/mall-admin/gift-card/unused-total`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GiftCard
     * @name GiftCardQueryPaging
     * @request POST:/api/mall-admin/gift-card/paging
     * @secure
     */
    giftCardQueryPaging: (
      data: QueryGiftCardPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1GiftCardDto, any>({
        path: `/api/mall-admin/gift-card/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GiftCard
     * @name GiftCardCreate
     * @request POST:/api/mall-admin/gift-card/create
     * @secure
     */
    giftCardCreate: (data: GiftCardDto, params: RequestParams = {}) =>
      this.request<ApiResponse1GiftCard, any>({
        path: `/api/mall-admin/gift-card/create`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GiftCard
     * @name GiftCardUpdateStatus
     * @request POST:/api/mall-admin/gift-card/update-status
     * @secure
     */
    giftCardUpdateStatus: (
      data: UpdateGiftCardStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/gift-card/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Goods
     * @name GoodsQueryGoodsForSelection
     * @request POST:/api/mall-admin/goods/query-goods-for-selection
     * @secure
     */
    goodsQueryGoodsForSelection: (
      data: QueryGoodsForSelection,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1GoodsDtoArray1, any>({
        path: `/api/mall-admin/goods/query-goods-for-selection`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Goods
     * @name GoodsGetById
     * @request POST:/api/mall-admin/goods/get-by-id
     * @secure
     */
    goodsGetById: (data: IdDto1String, params: RequestParams = {}) =>
      this.request<ApiResponse1GoodsDto, any>({
        path: `/api/mall-admin/goods/get-by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Goods
     * @name GoodsQueryGoodsPaging
     * @request POST:/api/mall-admin/goods/paging
     * @secure
     */
    goodsQueryGoodsPaging: (
      data: QueryGoodsPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1GoodsDto, any>({
        path: `/api/mall-admin/goods/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Goods
     * @name GoodsSaveGoods
     * @request POST:/api/mall-admin/goods/save
     * @secure
     */
    goodsSaveGoods: (data: GoodsDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Goods, any>({
        path: `/api/mall-admin/goods/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Goods
     * @name GoodsDelete
     * @request POST:/api/mall-admin/goods/delete
     * @secure
     */
    goodsDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/goods/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Grade
     * @name GradeSetUserGrade
     * @request POST:/api/mall-admin/grade/set-user-grade
     * @secure
     */
    gradeSetUserGrade: (
      data: StoreUserGradeMappingDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/grade/set-user-grade`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Grade
     * @name GradeListGrades
     * @request POST:/api/mall-admin/grade/list
     * @secure
     */
    gradeListGrades: (params: RequestParams = {}) =>
      this.request<ApiResponse1GradeArray1, any>({
        path: `/api/mall-admin/grade/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Grade
     * @name GradeSave
     * @request POST:/api/mall-admin/grade/save
     * @secure
     */
    gradeSave: (data: GradeDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/grade/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Grade
     * @name GradeUpdateStatus
     * @request POST:/api/mall-admin/grade/update-status
     * @secure
     */
    gradeUpdateStatus: (
      data: UpdateGradeStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/grade/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GradePrice
     * @name GradePriceListSkuGradePrice
     * @request POST:/api/mall-admin/grade-price/list-sku-grade-price
     * @secure
     */
    gradePriceListSkuGradePrice: (
      data: IdDto1String,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1GoodsGradePriceDtoArray1, any>({
        path: `/api/mall-admin/grade-price/list-sku-grade-price`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportDashBoardCounter
     * @request POST:/api/mall-admin/report/dashboard-counter
     * @secure
     */
    reportDashBoardCounter: (params: RequestParams = {}) =>
      this.request<ApiResponse1DashboardCountView, any>({
        path: `/api/mall-admin/report/dashboard-counter`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopAfterSalesMallUsers
     * @request POST:/api/mall-admin/report/top-after-sales-users
     * @secure
     */
    reportTopAfterSalesMallUsers: (
      data: TopAfterSaleMallUsersInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1TopAfterSaleMallUsersResponseArray1, any>({
        path: `/api/mall-admin/report/top-after-sales-users`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportGroupByGeoLocation
     * @request POST:/api/mall-admin/report/user-activity-by-geo-location
     * @secure
     */
    reportGroupByGeoLocation: (
      data: UserActivityGroupByGeoLocationInput,
      params: RequestParams = {},
    ) =>
      this.request<
        ApiResponse1UserActivityGroupByGeoLocationResponseArray1,
        any
      >({
        path: `/api/mall-admin/report/user-activity-by-geo-location`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportUserActivityGroupByHour
     * @request POST:/api/mall-admin/report/user-activity-by-hour
     * @secure
     */
    reportUserActivityGroupByHour: (
      data: UserActivityGroupByHourInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1UserActivityGroupByHourResponseArray1, any>({
        path: `/api/mall-admin/report/user-activity-by-hour`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportQuerySearchKeywordsReport
     * @request POST:/api/mall-admin/report/grouped-keywords
     * @secure
     */
    reportQuerySearchKeywordsReport: (
      data: QuerySearchKeywordsReportInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1QuerySearchKeywordsReportResponseArray1, any>({
        path: `/api/mall-admin/report/grouped-keywords`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopVisitedGoods
     * @request POST:/api/mall-admin/report/top-visited-goods
     * @secure
     */
    reportTopVisitedGoods: (
      data: QueryGoodsVisitReportInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1QueryGoodsVisitReportResponseArray1, any>({
        path: `/api/mall-admin/report/top-visited-goods`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopCustomers
     * @request POST:/api/mall-admin/report/top-customers
     * @secure
     */
    reportTopCustomers: (
      data: QueryTopCustomerInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1TopCustomersListArray1, any>({
        path: `/api/mall-admin/report/top-customers`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopSellers
     * @request POST:/api/mall-admin/report/top-sellers
     * @secure
     */
    reportTopSellers: (data: QueryTopSellerInput, params: RequestParams = {}) =>
      this.request<ApiResponse1TopSellersListArray1, any>({
        path: `/api/mall-admin/report/top-sellers`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopBrands
     * @request POST:/api/mall-admin/report/top-brands
     * @secure
     */
    reportTopBrands: (
      data: QueryTopBrandListInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1TopBrandListArray1, any>({
        path: `/api/mall-admin/report/top-brands`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopCategories
     * @request POST:/api/mall-admin/report/top-category
     * @secure
     */
    reportTopCategories: (
      data: QueryTopCategoryListInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1TopCategoryListArray1, any>({
        path: `/api/mall-admin/report/top-category`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportTopSkus
     * @request POST:/api/mall-admin/report/top-skus
     * @secure
     */
    reportTopSkus: (data: QueryTopSkuListInput, params: RequestParams = {}) =>
      this.request<ApiResponse1TopSkuListArray1, any>({
        path: `/api/mall-admin/report/top-skus`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Report
     * @name ReportOrderSumByDate
     * @request POST:/api/mall-admin/report/order-sum-by-date
     * @secure
     */
    reportOrderSumByDate: (
      data: OrderSumByDateInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1OrderSumByDateResponseArray1, any>({
        path: `/api/mall-admin/report/order-sum-by-date`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Setting
     * @name SettingRefreshViewCache
     * @request POST:/api/mall-admin/settings/refresh-view-cache
     * @secure
     */
    settingRefreshViewCache: (
      data: RefreshViewCacheInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/settings/refresh-view-cache`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Setting
     * @name SettingHomePageBlocks
     * @request POST:/api/mall-admin/settings/home-page-blocks
     * @secure
     */
    settingHomePageBlocks: (params: RequestParams = {}) =>
      this.request<ApiResponse1HomePageBlocksDto, any>({
        path: `/api/mall-admin/settings/home-page-blocks`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Setting
     * @name SettingSaveHomePageBlocks
     * @request POST:/api/mall-admin/settings/save-home-page-blocks
     * @secure
     */
    settingSaveHomePageBlocks: (
      data: HomePageBlocksDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/settings/save-home-page-blocks`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Setting
     * @name SettingGetMallSettings
     * @request POST:/api/mall-admin/settings/mall-settings
     * @secure
     */
    settingGetMallSettings: (params: RequestParams = {}) =>
      this.request<ApiResponse1MallSettingsDto, any>({
        path: `/api/mall-admin/settings/mall-settings`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Setting
     * @name SettingSaveMallSettings
     * @request POST:/api/mall-admin/settings/save-mall-settings
     * @secure
     */
    settingSaveMallSettings: (
      data: MallSettingsDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/settings/save-mall-settings`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuQueryByPageOrderByCreationTime
     * @request POST:/api/mall-admin/sku/query-by-page-order-by-creation-time
     * @secure
     */
    skuQueryByPageOrderByCreationTime: (
      data: PagedRequest,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1SkuDtoArray1, any>({
        path: `/api/mall-admin/sku/query-by-page-order-by-creation-time`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuUpdateSkuPrice
     * @request POST:/api/mall-admin/sku/update-sku-price
     * @secure
     */
    skuUpdateSkuPrice: (data: SkuDto, params: RequestParams = {}) =>
      this.request<ApiResponse1SkuDto, any>({
        path: `/api/mall-admin/sku/update-sku-price`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuQuerySkuForSelection
     * @request POST:/api/mall-admin/sku/query-sku-for-selection
     * @secure
     */
    skuQuerySkuForSelection: (
      data: QuerySkuForSelectionInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1SkuDtoArray1, any>({
        path: `/api/mall-admin/sku/query-sku-for-selection`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuGetById
     * @request POST:/api/mall-admin/sku/get-by-id
     * @secure
     */
    skuGetById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1SkuDto, any>({
        path: `/api/mall-admin/sku/get-by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuQuerySkuPagination
     * @request POST:/api/mall-admin/sku/query-paging
     * @secure
     */
    skuQuerySkuPagination: (data: QuerySkuPaging, params: RequestParams = {}) =>
      this.request<PagedResponse1SkuDto, any>({
        path: `/api/mall-admin/sku/query-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuSetSpecCombination
     * @request POST:/api/mall-admin/sku/update-spec-combination
     * @secure
     */
    skuSetSpecCombination: (data: SkuDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/sku/update-spec-combination`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuDelete
     * @request POST:/api/mall-admin/sku/delete
     * @secure
     */
    skuDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/sku/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuSaveSkus
     * @request POST:/api/mall-admin/sku/save-v1
     * @secure
     */
    skuSaveSkus: (data: SkuDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Sku, any>({
        path: `/api/mall-admin/sku/save-v1`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Spec
     * @name SpecListJson
     * @request POST:/api/mall-admin/spec/list
     * @secure
     */
    specListJson: (data: IdDto1String, params: RequestParams = {}) =>
      this.request<ApiResponse1SpecDtoArray1, any>({
        path: `/api/mall-admin/spec/list`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Spec
     * @name SpecEdit
     * @request POST:/api/mall-admin/spec/save
     * @secure
     */
    specEdit: (data: SpecDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/spec/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Spec
     * @name SpecDelete
     * @request POST:/api/mall-admin/spec/delete
     * @secure
     */
    specDelete: (data: IdDto1Int32, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/spec/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Spec
     * @name SpecValueEdit
     * @request POST:/api/mall-admin/spec/save-value
     * @secure
     */
    specValueEdit: (data: SpecValueDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/spec/save-value`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Spec
     * @name SpecValueDelete
     * @request POST:/api/mall-admin/spec/delete-value
     * @secure
     */
    specValueDelete: (data: IdDto1Int32, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/spec/delete-value`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Stock
     * @name StockStockPaging
     * @request POST:/api/mall-admin/stock/paging
     * @secure
     */
    stockStockPaging: (
      data: QueryWarehouseStockPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1StockDto, any>({
        path: `/api/mall-admin/stock/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Stock
     * @name StockApproveStock
     * @request POST:/api/mall-admin/stock/approve
     * @secure
     */
    stockApproveStock: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/stock/approve`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Stock
     * @name StockDeleteUnApprovedStock
     * @request POST:/api/mall-admin/stock/delete-unapproved-stock
     * @secure
     */
    stockDeleteUnApprovedStock: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/stock/delete-unapproved-stock`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Stock
     * @name StockInsertStock
     * @request POST:/api/mall-admin/stock/insert-stock
     * @secure
     */
    stockInsertStock: (data: StockDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/stock/insert-stock`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StockItem
     * @name StockItemStockItemPaging
     * @request POST:/api/mall-admin/stock-item/item-paging
     * @secure
     */
    stockItemStockItemPaging: (
      data: QueryWarehouseStockItemInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1StockItemDto, any>({
        path: `/api/mall-admin/stock-item/item-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreQueryStores
     * @request POST:/api/mall-admin/store/list
     * @secure
     */
    storeQueryStores: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreDtoArray1, any>({
        path: `/api/mall-admin/store/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreCreateStore
     * @request POST:/api/mall-admin/store/save
     * @secure
     */
    storeCreateStore: (data: StoreDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/store/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreDeleteStore
     * @request POST:/api/mall-admin/store/delete
     * @secure
     */
    storeDeleteStore: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/store/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreQueryStoreManager
     * @request POST:/api/mall-admin/store/manager-list
     * @secure
     */
    storeQueryStoreManager: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1StoreManagerDtoArray1, any>({
        path: `/api/mall-admin/store/manager-list`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreSaveStoreManager
     * @request POST:/api/mall-admin/store/save-manager
     * @deprecated
     * @secure
     */
    storeSaveStoreManager: (
      data: StoreManagerDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1StoreManager, any>({
        path: `/api/mall-admin/store/save-manager`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreDeleteStoreManager
     * @request POST:/api/mall-admin/store/delete-manager
     * @secure
     */
    storeDeleteStoreManager: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/store/delete-manager`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Supplier
     * @name SupplierSave
     * @request POST:/api/mall-admin/supplier/save
     * @secure
     */
    supplierSave: (data: SupplierDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/supplier/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Supplier
     * @name SupplierUpdateStatus
     * @request POST:/api/mall-admin/supplier/update-status
     * @secure
     */
    supplierUpdateStatus: (
      data: UpdateSupplierStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/supplier/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Supplier
     * @name SupplierQueryPaging
     * @request POST:/api/mall-admin/supplier/paging
     * @secure
     */
    supplierQueryPaging: (
      data: QuerySupplierPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1SupplierDto, any>({
        path: `/api/mall-admin/supplier/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Supplier
     * @name SupplierQueryAll
     * @request POST:/api/mall-admin/supplier/all
     * @secure
     */
    supplierQueryAll: (params: RequestParams = {}) =>
      this.request<ApiResponse1SupplierDtoArray1, any>({
        path: `/api/mall-admin/supplier/all`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Tag
     * @name TagListTags
     * @request POST:/api/mall-admin/tag/list-tags
     * @secure
     */
    tagListTags: (params: RequestParams = {}) =>
      this.request<ApiResponse1TagDtoArray1, any>({
        path: `/api/mall-admin/tag/list-tags`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Tag
     * @name TagSaveTag
     * @request POST:/api/mall-admin/tag/save-tag
     * @secure
     */
    tagSaveTag: (data: TagDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/tag/save-tag`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Tag
     * @name TagDelete
     * @request POST:/api/mall-admin/tag/delete
     * @secure
     */
    tagDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/tag/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserUpdateStatus
     * @request POST:/api/mall-admin/user/update-status
     * @secure
     */
    userUpdateStatus: (
      data: UpdateUserStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/user/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserQueryPaging
     * @request POST:/api/mall-admin/user/paging
     * @secure
     */
    userQueryPaging: (data: QueryUserPagingInput, params: RequestParams = {}) =>
      this.request<PagedResponse1StoreUserDto, any>({
        path: `/api/mall-admin/user/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ValueAdded
     * @name ValueAddedQueryGroupWithItemsByGoodsId
     * @request POST:/api/mall-admin/value-added/query-group-with-items-by-goods-id
     * @secure
     */
    valueAddedQueryGroupWithItemsByGoodsId: (
      data: IdDto1String,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1ValueAddedItemGroupDtoArray1, any>({
        path: `/api/mall-admin/value-added/query-group-with-items-by-goods-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ValueAdded
     * @name ValueAddedSaveGroup
     * @request POST:/api/mall-admin/value-added/save-group
     * @secure
     */
    valueAddedSaveGroup: (
      data: ValueAddedItemGroupDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/value-added/save-group`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ValueAdded
     * @name ValueAddedUpdateGroup
     * @request POST:/api/mall-admin/value-added/delete-group
     * @secure
     */
    valueAddedUpdateGroup: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/value-added/delete-group`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ValueAdded
     * @name ValueAddedSaveGroupItem
     * @request POST:/api/mall-admin/value-added/save-group-item
     * @secure
     */
    valueAddedSaveGroupItem: (
      data: ValueAddedItemDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/value-added/save-group-item`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ValueAdded
     * @name ValueAddedUpdateGroupItem
     * @request POST:/api/mall-admin/value-added/delete-group-item
     * @secure
     */
    valueAddedUpdateGroupItem: (
      data: IdDto1Int32,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/value-added/delete-group-item`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Warehouse
     * @name WarehouseSave
     * @request POST:/api/mall-admin/warehouse/save
     * @secure
     */
    warehouseSave: (data: WarehouseDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/warehouse/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Warehouse
     * @name WarehouseUpdateStatus
     * @request POST:/api/mall-admin/warehouse/update-status
     * @secure
     */
    warehouseUpdateStatus: (
      data: UpdateWarehouseStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-admin/warehouse/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Warehouse
     * @name WarehouseQueryAll
     * @request POST:/api/mall-admin/warehouse/all
     * @secure
     */
    warehouseQueryAll: (params: RequestParams = {}) =>
      this.request<ApiResponse1WarehouseDtoArray1, any>({
        path: `/api/mall-admin/warehouse/all`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  sys = {
    /**
     * No description
     *
     * @tags AdminAuth
     * @name AdminAuthAdminAuth
     * @request POST:/api/sys/admin-auth/auth
     * @secure
     */
    adminAuthAdminAuth: (params: RequestParams = {}) =>
      this.request<AdminAuthResult, any>({
        path: `/api/sys/admin-auth/auth`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageAdmin
     * @name ManageAdminQueryAdminPermissions
     * @request POST:/api/sys/manage-admin/permissions
     * @secure
     */
    manageAdminQueryAdminPermissions: (params: RequestParams = {}) =>
      this.request<AdminGrantedPermissionResponse, any>({
        path: `/api/sys/manage-admin/permissions`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageAdmin
     * @name ManageAdminQueryAdminPaging
     * @request POST:/api/sys/manage-admin/paging
     * @secure
     */
    manageAdminQueryAdminPaging: (
      data: QueryAdminPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1AdminDto, any>({
        path: `/api/sys/manage-admin/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageAdmin
     * @name ManageAdminSetAsAdmin
     * @request POST:/api/sys/manage-admin/set-as-admin
     * @secure
     */
    manageAdminSetAsAdmin: (data: SetAsAdminDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Admin, any>({
        path: `/api/sys/manage-admin/set-as-admin`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageAdmin
     * @name ManageAdminUpdateStatus
     * @request POST:/api/sys/manage-admin/update-status
     * @secure
     */
    manageAdminUpdateStatus: (
      data: UpdateAdminStatusDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-admin/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageRole
     * @name ManageRoleQueryPaging
     * @request POST:/api/sys/manage-role/paging
     * @secure
     */
    manageRoleQueryPaging: (
      data: QueryRolePaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1RoleDto, any>({
        path: `/api/sys/manage-role/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageRole
     * @name ManageRoleDelete
     * @request POST:/api/sys/manage-role/delete
     * @secure
     */
    manageRoleDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-role/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageRole
     * @name ManageRoleSave
     * @request POST:/api/sys/manage-role/save
     * @secure
     */
    manageRoleSave: (data: RoleDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-role/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageRole
     * @name ManageRoleSetAdminRoles
     * @request POST:/api/sys/manage-role/set-admin-roles
     * @secure
     */
    manageRoleSetAdminRoles: (
      data: SetAdminRolesDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-role/set-admin-roles`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageRole
     * @name ManageRoleSetRolePermissions
     * @request POST:/api/sys/manage-role/set-role-permissions
     * @secure
     */
    manageRoleSetRolePermissions: (
      data: SetPermissionToRoleDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-role/set-role-permissions`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageSetting
     * @name ManageSettingQueryPaging
     * @request POST:/api/sys/manage-setting/paging
     * @secure
     */
    manageSettingQueryPaging: (
      data: QuerySettingsPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1SettingDto, any>({
        path: `/api/sys/manage-setting/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserGetProfileById
     * @request POST:/api/sys/manage-user/get-profile-by-id
     * @secure
     */
    manageUserGetProfileById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1UserDto, any>({
        path: `/api/sys/manage-user/get-profile-by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserSearchUserAccount
     * @request POST:/api/sys/manage-user/search-user-account
     * @secure
     */
    manageUserSearchUserAccount: (
      data: SearchUserAccountDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1UserDto, any>({
        path: `/api/sys/manage-user/search-user-account`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserQueryUserByIds
     * @request POST:/api/sys/manage-user/user-by-ids
     * @secure
     */
    manageUserQueryUserByIds: (data: string[], params: RequestParams = {}) =>
      this.request<ApiResponse1UserDtoArray1, any>({
        path: `/api/sys/manage-user/user-by-ids`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserQueryUserPagination
     * @request POST:/api/sys/manage-user/paging
     * @secure
     */
    manageUserQueryUserPagination: (
      data: QueryUserPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1UserDto, any>({
        path: `/api/sys/manage-user/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserSetPassword
     * @request POST:/api/sys/manage-user/set-password
     * @secure
     */
    manageUserSetPassword: (
      data: SetUserPasswordDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-user/set-password`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserSetMobile
     * @request POST:/api/sys/manage-user/set-mobile
     * @secure
     */
    manageUserSetMobile: (data: SetUserMobileDto, params: RequestParams = {}) =>
      this.request<ApiResponse1UserIdentity, any>({
        path: `/api/sys/manage-user/set-mobile`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserRemoveMobiles
     * @request POST:/api/sys/manage-user/remove-mobiles
     * @secure
     */
    manageUserRemoveMobiles: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-user/remove-mobiles`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserCreateUser
     * @request POST:/api/sys/manage-user/create
     * @secure
     */
    manageUserCreateUser: (data: IdentityNameDto, params: RequestParams = {}) =>
      this.request<ApiResponse1User, any>({
        path: `/api/sys/manage-user/create`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ManageUser
     * @name ManageUserUpdateStatus
     * @request POST:/api/sys/manage-user/update-status
     * @secure
     */
    manageUserUpdateStatus: (
      data: UpdateUserStatusDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/manage-user/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags WechatSettings
     * @name WechatSettingsSaveMpSettings
     * @request POST:/api/sys/wechat-settings/save-mp-settings
     * @secure
     */
    wechatSettingsSaveMpSettings: (
      data: WechatMpOption,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/wechat-settings/save-mp-settings`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags WechatSettings
     * @name WechatSettingsSaveOpenSettings
     * @request POST:/api/sys/wechat-settings/save-open-settings
     * @secure
     */
    wechatSettingsSaveOpenSettings: (
      data: WechatOpenOption,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/sys/wechat-settings/save-open-settings`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  mall = {
    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleInsertComment
     * @request POST:/api/mall/aftersale/add-comment
     * @secure
     */
    afterSaleInsertComment: (
      data: AfterSalesCommentDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/aftersale/add-comment`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleQueryCommentPaging
     * @request POST:/api/mall/aftersale/comment-paging
     * @secure
     */
    afterSaleQueryCommentPaging: (
      data: QueryAfterSalesCommentPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1AfterSalesCommentDto, any>({
        path: `/api/mall/aftersale/comment-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleAfterSalePendingCount
     * @request POST:/api/mall/aftersale/pending-count
     * @secure
     */
    afterSaleAfterSalePendingCount: (params: RequestParams = {}) =>
      this.request<ApiResponse1Int32, any>({
        path: `/api/mall/aftersale/pending-count`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleCancel
     * @request POST:/api/mall/aftersale/cancel
     * @secure
     */
    afterSaleCancel: (data: CancelAfterSaleDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/aftersale/cancel`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleCreateAfterSales
     * @request POST:/api/mall/aftersale/create
     * @secure
     */
    afterSaleCreateAfterSales: (
      data: AfterSalesDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1AfterSalesDto, any>({
        path: `/api/mall/aftersale/create`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleGetByOrderId
     * @request POST:/api/mall/aftersale/get-by-order-id
     * @secure
     */
    afterSaleGetByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1AfterSalesDto, any>({
        path: `/api/mall/aftersale/get-by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleQueryById
     * @request POST:/api/mall/aftersale/by-id
     * @secure
     */
    afterSaleQueryById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1AfterSalesDto, any>({
        path: `/api/mall/aftersale/by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Balance
     * @name BalancePayWithBalance
     * @request POST:/api/mall/balance/create-order-payment
     * @secure
     */
    balancePayWithBalance: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/balance/create-order-payment`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandAll
     * @request POST:/api/mall/brand/all
     * @secure
     */
    brandAll: (params: RequestParams = {}) =>
      this.request<ApiResponse1BrandDtoArray1, any>({
        path: `/api/mall/brand/all`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Brand
     * @name BrandList
     * @request POST:/api/mall/brand/list
     * @deprecated
     * @secure
     */
    brandList: (data: QueryBrandPaging, params: RequestParams = {}) =>
      this.request<ApiResponse1BrandDtoArray1, any>({
        path: `/api/mall/brand/list`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Category
     * @name CategoryCategoryPage
     * @request POST:/api/mall/category/view
     * @secure
     */
    categoryCategoryPage: (params: RequestParams = {}) =>
      this.request<ApiResponse1CategoryPageDto, any>({
        path: `/api/mall/category/view`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Category
     * @name CategoryByParent
     * @request POST:/api/mall/category/by-parent
     * @secure
     */
    categoryByParent: (data: IdDto1String, params: RequestParams = {}) =>
      this.request<ApiResponse1CategoryDtoArray1, any>({
        path: `/api/mall/category/by-parent`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Category
     * @name CategoryById
     * @request POST:/api/mall/category/by-id
     * @secure
     */
    categoryById: (data: IdDto1String, params: RequestParams = {}) =>
      this.request<ApiResponse1CategoryDto, any>({
        path: `/api/mall/category/by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Common
     * @name CommonListTags
     * @request POST:/api/mall/common/list-tags
     * @secure
     */
    commonListTags: (params: RequestParams = {}) =>
      this.request<ApiResponse1TagDtoArray1, any>({
        path: `/api/mall/common/list-tags`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Common
     * @name CommonQueryTagById
     * @request POST:/api/mall/common/query-tag-by-id
     * @secure
     */
    commonQueryTagById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Tag, any>({
        path: `/api/mall/common/query-tag-by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Common
     * @name CommonSaveActivityLog
     * @request POST:/api/mall/common/save-activity-log
     * @secure
     */
    commonSaveActivityLog: (data: ActivityLog, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/common/save-activity-log`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponQueryCouponPaging
     * @request POST:/api/mall/coupon/paging
     * @secure
     */
    couponQueryCouponPaging: (
      data: QueryCouponPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1CouponDto, any>({
        path: `/api/mall/coupon/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponQueryUserCouponPaging
     * @request POST:/api/mall/coupon/user-coupon-paging
     * @secure
     */
    couponQueryUserCouponPaging: (
      data: QueryUserCouponPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1CouponUserMappingDto, any>({
        path: `/api/mall/coupon/user-coupon-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Coupon
     * @name CouponIssueCoupon
     * @request POST:/api/mall/coupon/issue-coupon
     * @secure
     */
    couponIssueCoupon: (data: IssueCouponInput, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/coupon/issue-coupon`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryGetServiceTime
     * @request POST:/api/mall/delivery/get-service-time
     * @secure
     */
    deliveryGetServiceTime: (params: RequestParams = {}) =>
      this.request<ApiResponse1AvailableServiceTimeRangeDtoArray1, any>({
        path: `/api/mall/delivery/get-service-time`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryGetByOrderId
     * @request POST:/api/mall/delivery/by-order-id
     * @secure
     */
    deliveryGetByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1DeliveryRecordDtoArray1, any>({
        path: `/api/mall/delivery/by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryMarkAsDelivered
     * @request POST:/api/mall/delivery/mark-as-delivered
     * @secure
     */
    deliveryMarkAsDelivered: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/delivery/mark-as-delivered`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GenericOrder
     * @name GenericOrderQueryPaging
     * @request POST:/api/mall/qingdao/generic-order/paging
     * @secure
     */
    genericOrderQueryPaging: (
      data: QueryGenericOrderInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1GenericOrderDto, any>({
        path: `/api/mall/qingdao/generic-order/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GiftCard
     * @name GiftCardQueryById
     * @request POST:/api/mall/gift-card/by-id
     * @secure
     */
    giftCardQueryById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1GiftCardDto, any>({
        path: `/api/mall/gift-card/by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags GiftCard
     * @name GiftCardUseCard
     * @request POST:/api/mall/gift-card/use-card
     * @secure
     */
    giftCardUseCard: (data: UseGiftCardInput, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/gift-card/use-card`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Gis
     * @name GisGetOrgByLocation
     * @request POST:/api/mall/qingdao/gis/get-org-by-location
     * @secure
     */
    gisGetOrgByLocation: (
      data: GetOrgByLocationInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1WapOrganization, any>({
        path: `/api/mall/qingdao/gis/get-org-by-location`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Gis
     * @name GisCurrentStore
     * @request POST:/api/mall/qingdao/gis/current-store
     * @secure
     */
    gisCurrentStore: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreDto, any>({
        path: `/api/mall/qingdao/gis/current-store`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Goods
     * @name GoodsQueryGoodsDetail
     * @request POST:/api/mall/goods/detail
     * @secure
     */
    goodsQueryGoodsDetail: (data: IdDto1String, params: RequestParams = {}) =>
      this.request<ApiResponse1GoodsDto, any>({
        path: `/api/mall/goods/detail`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Home
     * @name HomeHomePageBlocks
     * @request POST:/api/mall/home/home-page-blocks
     * @secure
     */
    homeHomePageBlocks: (params: RequestParams = {}) =>
      this.request<ApiResponse1HomePageBlocksDto, any>({
        path: `/api/mall/home/home-page-blocks`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Home
     * @name HomeHomePage
     * @request POST:/api/mall/home/view
     * @deprecated
     * @secure
     */
    homeHomePage: (params: RequestParams = {}) =>
      this.request<ApiResponse1HomePageDto, any>({
        path: `/api/mall/home/view`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Notification
     * @name NotificationUpdateStatus
     * @request POST:/api/mall/notification/update-status
     * @secure
     */
    notificationUpdateStatus: (
      data: UpdateNotificationStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/notification/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Notification
     * @name NotificationPaging
     * @request POST:/api/mall/notification/paging
     * @secure
     */
    notificationPaging: (
      data: QueryNotificationPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1NotificationDto, any>({
        path: `/api/mall/notification/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Notification
     * @name NotificationDeleteNotification
     * @request POST:/api/mall/notification/delete
     * @secure
     */
    notificationDeleteNotification: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/notification/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Notification
     * @name NotificationDeleteAll
     * @request POST:/api/mall/notification/delete-all
     * @secure
     */
    notificationDeleteAll: (params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/notification/delete-all`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Notification
     * @name NotificationUnReadCount
     * @request POST:/api/mall/notification/unread-count
     * @secure
     */
    notificationUnReadCount: (params: RequestParams = {}) =>
      this.request<ApiResponse1Int32, any>({
        path: `/api/mall/notification/unread-count`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderPendingCount
     * @request POST:/api/mall/order/pending-count
     * @secure
     */
    orderPendingCount: (params: RequestParams = {}) =>
      this.request<ApiResponse1Int32, any>({
        path: `/api/mall/order/pending-count`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderPrePlaceOrderWithoutSaving
     * @request POST:/api/mall/order/pre-place-order
     * @secure
     */
    orderPrePlaceOrderWithoutSaving: (
      data: PlaceOrderRequestDto,
      params: RequestParams = {},
    ) =>
      this.request<PlaceOrderResult, any>({
        path: `/api/mall/order/pre-place-order`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderPlaceOrder
     * @request POST:/api/mall/order/place-order
     * @secure
     */
    orderPlaceOrder: (data: PlaceOrderRequestDto, params: RequestParams = {}) =>
      this.request<PlaceOrderResult, any>({
        path: `/api/mall/order/place-order`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderQueryOrderList
     * @request POST:/api/mall/order/paging
     * @secure
     */
    orderQueryOrderList: (
      data: QueryOrderPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1OrderDto, any>({
        path: `/api/mall/order/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderDetail
     * @request POST:/api/mall/order/detail
     * @secure
     */
    orderDetail: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1OrderDto, any>({
        path: `/api/mall/order/detail`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderListOrderBill
     * @request POST:/api/mall/order/list-order-bill
     * @secure
     */
    orderListOrderBill: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1PaymentBillDtoArray1, any>({
        path: `/api/mall/order/list-order-bill`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderQueryOrderNotes
     * @request POST:/api/mall/order/list-order-notes
     * @secure
     */
    orderQueryOrderNotes: (
      data: QueryOrderNotesInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1OrderNoteDtoArray1, any>({
        path: `/api/mall/order/list-order-notes`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderComplete
     * @request POST:/api/mall/order/complete
     * @secure
     */
    orderComplete: (data: FinishOrderInput, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/order/complete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderCancelOrder
     * @request POST:/api/mall/order/cancel
     * @secure
     */
    orderCancelOrder: (data: CloseOrderInput, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/order/cancel`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderDelete
     * @request POST:/api/mall/order/delete
     * @secure
     */
    orderDelete: (data: IdDto1String, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/order/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderReview
     * @name OrderReviewQueryItemsPaging
     * @request POST:/api/mall/order-review/goods-review
     * @secure
     */
    orderReviewQueryItemsPaging: (
      data: QueryOrderReviewItemPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1OrderReviewItemDto, any>({
        path: `/api/mall/order-review/goods-review`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderReview
     * @name OrderReviewInsert
     * @request POST:/api/mall/order-review/insert
     * @secure
     */
    orderReviewInsert: (data: OrderReviewDto, params: RequestParams = {}) =>
      this.request<ApiResponse1OrderReview, any>({
        path: `/api/mall/order-review/insert`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderReview
     * @name OrderReviewQueryOrderReviewByOrderId
     * @request POST:/api/mall/order-review/get-review-by-order-id
     * @secure
     */
    orderReviewQueryOrderReviewByOrderId: (
      data: IdDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1OrderReviewDto, any>({
        path: `/api/mall/order-review/get-review-by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupGetByOrderId
     * @request POST:/api/mall/pickup/by-order-id
     * @secure
     */
    pickupGetByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1PickupRecordDtoArray1, any>({
        path: `/api/mall/pickup/by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupMarkAsPicked
     * @request POST:/api/mall/pickup/mark-as-picked
     * @secure
     */
    pickupMarkAsPicked: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/pickup/mark-as-picked`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupGetServiceTime
     * @request POST:/api/mall/pickup/get-service-time
     * @secure
     */
    pickupGetServiceTime: (params: RequestParams = {}) =>
      this.request<ApiResponse1AvailableServiceTimeRangeDtoArray1, any>({
        path: `/api/mall/pickup/get-service-time`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Search
     * @name SearchSearchView
     * @request POST:/api/mall/search/search-view
     * @secure
     */
    searchSearchView: (params: RequestParams = {}) =>
      this.request<ApiResponse1SearchView, any>({
        path: `/api/mall/search/search-view`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Search
     * @name SearchSearchOptions
     * @request POST:/api/mall/search/search-options
     * @secure
     */
    searchSearchOptions: (data: QueryGoodsPaging, params: RequestParams = {}) =>
      this.request<ApiResponse1SearchOptionsDto, any>({
        path: `/api/mall/search/search-options`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Search
     * @name SearchListByTagName
     * @request POST:/api/mall/search/list-by-tag-name
     * @secure
     */
    searchListByTagName: (
      data: ListByTagNameInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1GoodsDtoArray1, any>({
        path: `/api/mall/search/list-by-tag-name`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Search
     * @name SearchSearchSkuPaging
     * @request POST:/api/mall/search/sku
     * @secure
     */
    searchSearchSkuPaging: (data: QuerySkuPaging, params: RequestParams = {}) =>
      this.request<PagedResponse1SkuDto, any>({
        path: `/api/mall/search/sku`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Search
     * @name SearchSearchGoodsList
     * @request POST:/api/mall/search/goods
     * @secure
     */
    searchSearchGoodsList: (
      data: QueryGoodsPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1GoodsDto, any>({
        path: `/api/mall/search/goods`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Setting
     * @name SettingGetMallSettings
     * @request POST:/api/mall/setting/mall-settings
     * @secure
     */
    settingGetMallSettings: (params: RequestParams = {}) =>
      this.request<ApiResponse1MallSettingsDto, any>({
        path: `/api/mall/setting/mall-settings`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ShoppingCart
     * @name ShoppingCartCount
     * @request POST:/api/mall/shoppingcart/count
     * @secure
     */
    shoppingCartCount: (params: RequestParams = {}) =>
      this.request<ApiResponse1Int32, any>({
        path: `/api/mall/shoppingcart/count`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ShoppingCart
     * @name ShoppingCartListShoppingCarts
     * @request POST:/api/mall/shoppingcart/list
     * @secure
     */
    shoppingCartListShoppingCarts: (params: RequestParams = {}) =>
      this.request<ApiResponse1ShoppingCartItemDtoArray1, any>({
        path: `/api/mall/shoppingcart/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ShoppingCart
     * @name ShoppingCartGoodsToCartV1
     * @request POST:/api/mall/shoppingcart/add-v1
     * @secure
     */
    shoppingCartGoodsToCartV1: (
      data: AddOrUpdateShoppingCartInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/shoppingcart/add-v1`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ShoppingCart
     * @name ShoppingCartUpdateCartItem
     * @request POST:/api/mall/shoppingcart/update
     * @secure
     */
    shoppingCartUpdateCartItem: (
      data: UpdateShoppingCartQuantityInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/shoppingcart/update`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags ShoppingCart
     * @name ShoppingCartRemoveItem
     * @request POST:/api/mall/shoppingcart/delete
     * @secure
     */
    shoppingCartRemoveItem: (data: IdDto1Int32, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/shoppingcart/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuListSpecGroup
     * @request POST:/api/mall/sku/list-spec-group
     * @secure
     */
    skuListSpecGroup: (
      data: QuerySkuBySelectedItemsDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1SpecGroupSelectResponse, any>({
        path: `/api/mall/sku/list-spec-group`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuListBy
     * @request POST:/api/mall/sku/list-by
     * @secure
     */
    skuListBy: (data: QuerySkuBySelectedItemsDto, params: RequestParams = {}) =>
      this.request<ApiResponse1SkuDtoArray1, any>({
        path: `/api/mall/sku/list-by`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreListStores
     * @request POST:/api/mall/store/list-stores
     * @secure
     */
    storeListStores: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreDtoArray1, any>({
        path: `/api/mall/store/list-stores`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreMostNearbyStore
     * @request POST:/api/mall/store/most-nearby-store
     * @secure
     */
    storeMostNearbyStore: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreDto, any>({
        path: `/api/mall/store/most-nearby-store`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreGetCurrentStore
     * @request POST:/api/mall/store/current-store
     * @secure
     */
    storeGetCurrentStore: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreDto, any>({
        path: `/api/mall/store/current-store`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Store
     * @name StoreStoreList
     * @request POST:/api/mall/qingdao/store/stores-list
     * @secure
     */
    storeStoreList: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreDtoArray1, any>({
        path: `/api/mall/qingdao/store/stores-list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserUserBalanceAndPoints
     * @request POST:/api/mall/user/balance-and-points
     * @secure
     */
    userUserBalanceAndPoints: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreUser, any>({
        path: `/api/mall/user/balance-and-points`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserQueryPointsPaging
     * @request POST:/api/mall/user/points-history
     * @secure
     */
    userQueryPointsPaging: (
      data: QueryPointsPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1PointsHistoryDto, any>({
        path: `/api/mall/user/points-history`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserQueryBalancePaging
     * @request POST:/api/mall/user/balance-history
     * @secure
     */
    userQueryBalancePaging: (
      data: QueryBalancePagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1BalanceHistoryDto, any>({
        path: `/api/mall/user/balance-history`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserAuth
     * @name UserAuthQueryUserProfile
     * @request POST:/api/mall/user-auth/auth
     * @secure
     */
    userAuthQueryUserProfile: (params: RequestParams = {}) =>
      this.request<StoreUserAuthResult, any>({
        path: `/api/mall/user-auth/auth`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags WechatMpPayment
     * @name WechatMpPaymentTest
     * @request POST:/api/mall/wechat-mp-payment/test
     * @secure
     */
    wechatMpPaymentTest: (params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall/wechat-mp-payment/test`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  mallManager = {
    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleInsertComment
     * @request POST:/api/mall-manager/after-sale/add-comment
     * @secure
     */
    afterSaleInsertComment: (
      data: AfterSalesCommentDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/add-comment`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleQueryCommentPaging
     * @request POST:/api/mall-manager/after-sale/comment-paging
     * @secure
     */
    afterSaleQueryCommentPaging: (
      data: QueryAfterSalesCommentPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1AfterSalesCommentDto, any>({
        path: `/api/mall-manager/after-sale/comment-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleUpdateStatus
     * @request POST:/api/mall-manager/after-sale/update-status
     * @secure
     */
    afterSaleUpdateStatus: (
      data: UpdateAfterSaleStatusDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleUpdateAfterSalesStatus
     * @request POST:/api/mall-manager/after-sale/dangerously-update-status
     * @secure
     */
    afterSaleUpdateAfterSalesStatus: (
      data: DangerouslyUpdateAfterSalesStatusDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/dangerously-update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleCancel
     * @request POST:/api/mall-manager/after-sale/cancel
     * @secure
     */
    afterSaleCancel: (data: CancelAfterSaleDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/cancel`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleComplete
     * @request POST:/api/mall-manager/after-sale/complete
     * @secure
     */
    afterSaleComplete: (
      data: CompleteAfterSaleDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/complete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleApprove
     * @request POST:/api/mall-manager/after-sale/approve
     * @secure
     */
    afterSaleApprove: (data: ApproveAfterSaleDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/approve`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleReject
     * @request POST:/api/mall-manager/after-sale/reject
     * @secure
     */
    afterSaleReject: (data: RejectAfterSaleInput, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/after-sale/reject`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleGetByOrderId
     * @request POST:/api/mall-manager/after-sale/get-by-order-id
     * @secure
     */
    afterSaleGetByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1AfterSalesDto, any>({
        path: `/api/mall-manager/after-sale/get-by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleQueryById
     * @request POST:/api/mall-manager/after-sale/by-id
     * @secure
     */
    afterSaleQueryById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1AfterSalesDto, any>({
        path: `/api/mall-manager/after-sale/by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags AfterSale
     * @name AfterSaleQueryPaging
     * @request POST:/api/mall-manager/after-sale/paging
     * @secure
     */
    afterSaleQueryPaging: (
      data: QueryAfterSalePaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1AfterSalesDto, any>({
        path: `/api/mall-manager/after-sale/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryQueryById
     * @request POST:/api/mall-manager/delivery/by-id
     * @secure
     */
    deliveryQueryById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1DeliveryRecordDto, any>({
        path: `/api/mall-manager/delivery/by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryQueryPaging
     * @request POST:/api/mall-manager/delivery/paging
     * @secure
     */
    deliveryQueryPaging: (
      data: QueryDeliveryRecordPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1DeliveryRecordDto, any>({
        path: `/api/mall-manager/delivery/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryGetOrderDeliveryItems
     * @request POST:/api/mall-manager/delivery/get-delivery-item-by-order-id
     * @secure
     */
    deliveryGetOrderDeliveryItems: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1DeliveryRecordItemArray1, any>({
        path: `/api/mall-manager/delivery/get-delivery-item-by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryByOrderId
     * @request POST:/api/mall-manager/delivery/by-order-id
     * @secure
     */
    deliveryByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1DeliveryRecordDtoArray1, any>({
        path: `/api/mall-manager/delivery/by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryMarkAsDelivering
     * @request POST:/api/mall-manager/delivery/mark-as-delivering
     * @secure
     */
    deliveryMarkAsDelivering: (
      data: DeliveryRecordDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/delivery/mark-as-delivering`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryMarkAsDelivered
     * @request POST:/api/mall-manager/delivery/mark-as-delivered
     * @secure
     */
    deliveryMarkAsDelivered: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/delivery/mark-as-delivered`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Delivery
     * @name DeliveryCreateDelivery
     * @request POST:/api/mall-manager/delivery/create-delivery
     * @secure
     */
    deliveryCreateDelivery: (
      data: DeliveryRecordDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1DeliveryRecord, any>({
        path: `/api/mall-manager/delivery/create-delivery`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags DeliverySettings
     * @name DeliverySettingsGetShortDistanceDeliverySettings
     * @request POST:/api/mall-manager/delivery-settings/get-short-distance-delivery-settings
     * @secure
     */
    deliverySettingsGetShortDistanceDeliverySettings: (
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1StoreShortDistanceDeliverySettings, any>({
        path: `/api/mall-manager/delivery-settings/get-short-distance-delivery-settings`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags DeliverySettings
     * @name DeliverySettingsSaveShortDistanceDeliverySettings
     * @request POST:/api/mall-manager/delivery-settings/save-short-distance-delivery-settings
     * @secure
     */
    deliverySettingsSaveShortDistanceDeliverySettings: (
      data: StoreShortDistanceDeliverySettings,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/delivery-settings/save-short-distance-delivery-settings`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags DeliverySettings
     * @name DeliverySettingsGetExpressSettings
     * @request POST:/api/mall-manager/delivery-settings/get-express-settings
     * @secure
     */
    deliverySettingsGetExpressSettings: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreExpressSettings, any>({
        path: `/api/mall-manager/delivery-settings/get-express-settings`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags DeliverySettings
     * @name DeliverySettingsSaveExpressSettings
     * @request POST:/api/mall-manager/delivery-settings/save-express-settings
     * @secure
     */
    deliverySettingsSaveExpressSettings: (
      data: StoreExpressSettings,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/delivery-settings/save-express-settings`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags FreightTemplate
     * @name FreightTemplateSaveGoodsExpressTemplate
     * @request POST:/api/mall-manager/freight-template/save-goods-mapping
     * @secure
     */
    freightTemplateSaveGoodsExpressTemplate: (
      data: SaveGoodsStoreExpressTemplateInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/freight-template/save-goods-mapping`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags FreightTemplate
     * @name FreightTemplateQueryAll
     * @request POST:/api/mall-manager/freight-template/list
     * @secure
     */
    freightTemplateQueryAll: (params: RequestParams = {}) =>
      this.request<ApiResponse1FreightTemplateDtoArray1, any>({
        path: `/api/mall-manager/freight-template/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags FreightTemplate
     * @name FreightTemplateSave
     * @request POST:/api/mall-manager/freight-template/save
     * @secure
     */
    freightTemplateSave: (
      data: FreightTemplateDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1FreightTemplate, any>({
        path: `/api/mall-manager/freight-template/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags FreightTemplate
     * @name FreightTemplateDeleteById
     * @request POST:/api/mall-manager/freight-template/delete-by-id
     * @secure
     */
    freightTemplateDeleteById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/freight-template/delete-by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderRefreshStatus
     * @request POST:/api/mall-manager/order/refresh-status
     * @secure
     */
    orderRefreshStatus: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order/refresh-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderDangerouslyUpdateStatus
     * @request POST:/api/mall-manager/order/dangerously-update-status
     * @secure
     */
    orderDangerouslyUpdateStatus: (
      data: UpdateOrderStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order/dangerously-update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderQueryOrderPaging
     * @request POST:/api/mall-manager/order/paging
     * @secure
     */
    orderQueryOrderPaging: (
      data: QueryOrderPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1OrderDto, any>({
        path: `/api/mall-manager/order/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderDetail
     * @request POST:/api/mall-manager/order/detail
     * @secure
     */
    orderDetail: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1OrderDto, any>({
        path: `/api/mall-manager/order/detail`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Order
     * @name OrderCancelOrder
     * @request POST:/api/mall-manager/order/cancel
     * @secure
     */
    orderCancelOrder: (data: CloseOrderInput, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order/cancel`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderNote
     * @name OrderNoteQueryOrderNotes
     * @request POST:/api/mall-manager/order-note/list-order-notes
     * @secure
     */
    orderNoteQueryOrderNotes: (
      data: QueryOrderNotesInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1OrderNoteDtoArray1, any>({
        path: `/api/mall-manager/order-note/list-order-notes`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderNote
     * @name OrderNoteAddOrderNote
     * @request POST:/api/mall-manager/order-note/add-order-note
     * @secure
     */
    orderNoteAddOrderNote: (data: OrderNoteDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order-note/add-order-note`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderNote
     * @name OrderNoteDelete
     * @request POST:/api/mall-manager/order-note/delete
     * @secure
     */
    orderNoteDelete: (data: IdDto1Int32, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order-note/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderPaymentBill
     * @name OrderPaymentBillCreateOrderBill
     * @request POST:/api/mall-manager/order-payment-bill/create-order-bill
     * @secure
     */
    orderPaymentBillCreateOrderBill: (
      data: IdDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1PaymentBillDto, any>({
        path: `/api/mall-manager/order-payment-bill/create-order-bill`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderPaymentBill
     * @name OrderPaymentBillCreateOfflinePayment
     * @request POST:/api/mall-manager/order-payment-bill/create-offline-payment
     * @secure
     */
    orderPaymentBillCreateOfflinePayment: (
      data: KeyValuePair2StringNullable1Decimal,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order-payment-bill/create-offline-payment`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderPaymentBill
     * @name OrderPaymentBillListOrderBill
     * @request POST:/api/mall-manager/order-payment-bill/list-order-bill
     * @secure
     */
    orderPaymentBillListOrderBill: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1PaymentBillDtoArray1, any>({
        path: `/api/mall-manager/order-payment-bill/list-order-bill`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderPaymentBill
     * @name OrderPaymentBillQueryPaging
     * @request POST:/api/mall-manager/order-payment-bill/paging
     * @secure
     */
    orderPaymentBillQueryPaging: (
      data: QueryOrderBillPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1PaymentBillDto, any>({
        path: `/api/mall-manager/order-payment-bill/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderReview
     * @name OrderReviewApproveReview
     * @request POST:/api/mall-manager/order-review/approve-review
     * @secure
     */
    orderReviewApproveReview: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/order-review/approve-review`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderReview
     * @name OrderReviewByOrderId
     * @request POST:/api/mall-manager/order-review/by-order-id
     * @secure
     */
    orderReviewByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1OrderReviewDto, any>({
        path: `/api/mall-manager/order-review/by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags OrderReview
     * @name OrderReviewQueryOrderReviewPaging
     * @request POST:/api/mall-manager/order-review/paging
     * @secure
     */
    orderReviewQueryOrderReviewPaging: (
      data: QueryOrderReviewPaging,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1OrderReviewDto, any>({
        path: `/api/mall-manager/order-review/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupCreatePickup
     * @request POST:/api/mall-manager/pickup/create-pickup
     * @secure
     */
    pickupCreatePickup: (data: PickupRecordDto, params: RequestParams = {}) =>
      this.request<ApiResponse1PickupRecord, any>({
        path: `/api/mall-manager/pickup/create-pickup`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupQueryById
     * @request POST:/api/mall-manager/pickup/by-id
     * @secure
     */
    pickupQueryById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1PickupRecordDto, any>({
        path: `/api/mall-manager/pickup/by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupQueryPaging
     * @request POST:/api/mall-manager/pickup/paging
     * @secure
     */
    pickupQueryPaging: (
      data: QueryPickupPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1PickupRecordDto, any>({
        path: `/api/mall-manager/pickup/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupApproveOrNot
     * @request POST:/api/mall-manager/pickup/approve-or-not
     * @secure
     */
    pickupApproveOrNot: (
      data: KeyValuePair2StringBoolean,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/pickup/approve-or-not`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupSetPrepared
     * @request POST:/api/mall-manager/pickup/set-prepared
     * @secure
     */
    pickupSetPrepared: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/pickup/set-prepared`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupSetPicked
     * @request POST:/api/mall-manager/pickup/set-picked
     * @secure
     */
    pickupSetPicked: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/pickup/set-picked`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupGetSettings
     * @request POST:/api/mall-manager/pickup/get-settings
     * @secure
     */
    pickupGetSettings: (params: RequestParams = {}) =>
      this.request<ApiResponse1StorePickupSettings, any>({
        path: `/api/mall-manager/pickup/get-settings`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupSaveSettings
     * @request POST:/api/mall-manager/pickup/save-settings
     * @secure
     */
    pickupSaveSettings: (
      data: StorePickupSettings,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/pickup/save-settings`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Pickup
     * @name PickupGetByOrderId
     * @request POST:/api/mall-manager/pickup/get-by-order-id
     * @secure
     */
    pickupGetByOrderId: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1PickupRecordDtoArray1, any>({
        path: `/api/mall-manager/pickup/get-by-order-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuUpdateQuantity
     * @request POST:/api/mall-manager/sku/update-quantity
     * @secure
     */
    skuUpdateQuantity: (
      data: StoreGoodsMappingDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/sku/update-quantity`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Sku
     * @name SkuQueryPaging
     * @request POST:/api/mall-manager/sku/paging
     * @secure
     */
    skuQueryPaging: (
      data: QueryStoreGoodsMappingPagingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1StoreGoodsMappingDto, any>({
        path: `/api/mall-manager/sku/paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManager
     * @name StoreManagerListStoreManager
     * @request POST:/api/mall-manager/store-manager/list
     * @secure
     */
    storeManagerListStoreManager: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreManagerDtoArray1, any>({
        path: `/api/mall-manager/store-manager/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerAuth
     * @name StoreManagerAuthAuth
     * @request POST:/api/mall-manager/store-manager-auth/auth
     * @secure
     */
    storeManagerAuthAuth: (params: RequestParams = {}) =>
      this.request<StoreManagerAuthResult, any>({
        path: `/api/mall-manager/store-manager-auth/auth`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerAuth
     * @name StoreManagerAuthStoreManagerList
     * @request POST:/api/mall-manager/store-manager-auth/store-manager-list
     * @secure
     */
    storeManagerAuthStoreManagerList: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreManagerDtoArray1, any>({
        path: `/api/mall-manager/store-manager-auth/store-manager-list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerPermission
     * @name StoreManagerPermissionListRoles
     * @request POST:/api/mall-manager/store-manager-permission/list-roles
     * @secure
     */
    storeManagerPermissionListRoles: (params: RequestParams = {}) =>
      this.request<ApiResponse1StoreRoleDtoArray1, any>({
        path: `/api/mall-manager/store-manager-permission/list-roles`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerPermission
     * @name StoreManagerPermissionSaveRole
     * @request POST:/api/mall-manager/store-manager-permission/save-role
     * @secure
     */
    storeManagerPermissionSaveRole: (
      data: StoreRoleDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/store-manager-permission/save-role`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerPermission
     * @name StoreManagerPermissionSetRolePermissions
     * @request POST:/api/mall-manager/store-manager-permission/set-role-permissions
     * @secure
     */
    storeManagerPermissionSetRolePermissions: (
      data: KeyValuePair2StringStringArray1,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/store-manager-permission/set-role-permissions`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerPermission
     * @name StoreManagerPermissionSetManagerRoles
     * @request POST:/api/mall-manager/store-manager-permission/set-manager-roles
     * @secure
     */
    storeManagerPermissionSetManagerRoles: (
      data: KeyValuePair2StringStringArray1,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/store-manager-permission/set-manager-roles`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerPermission
     * @name StoreManagerPermissionDeleteRole
     * @request POST:/api/mall-manager/store-manager-permission/delete-role
     * @secure
     */
    storeManagerPermissionDeleteRole: (
      data: IdDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-manager/store-manager-permission/delete-role`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags StoreManagerPermission
     * @name StoreManagerPermissionGetMyPermission
     * @request POST:/api/mall-manager/store-manager-permission/my-permissions
     * @secure
     */
    storeManagerPermissionGetMyPermission: (params: RequestParams = {}) =>
      this.request<StoreManagerGrantedPermissionResponse, any>({
        path: `/api/mall-manager/store-manager-permission/my-permissions`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),
  };
  platform = {
    /**
     * No description
     *
     * @tags Auth
     * @name AuthUserAuth
     * @request POST:/api/platform/auth/auth
     * @secure
     */
    authUserAuth: (params: RequestParams = {}) =>
      this.request<UserAuthResult, any>({
        path: `/api/platform/auth/auth`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name AuthPasswordLogin
     * @request POST:/api/platform/auth/password-login
     * @secure
     */
    authPasswordLogin: (data: PasswordLoginDto, params: RequestParams = {}) =>
      this.request<ApiResponse1AuthTokenDto, any>({
        path: `/api/platform/auth/password-login`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name AuthTestJwtService
     * @request POST:/api/platform/auth/test-jwt-service
     * @secure
     */
    authTestJwtService: (params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/api/platform/auth/test-jwt-service`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Auth
     * @name AuthAuthorizationTest
     * @request POST:/api/platform/auth/authorization_test
     * @deprecated
     * @secure
     */
    authAuthorizationTest: (params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/api/platform/auth/authorization_test`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags InternalCommon
     * @name InternalCommonTest
     * @request POST:/internal-api/platform/common/test
     * @secure
     */
    internalCommonTest: (params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/internal-api/platform/common/test`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags MobileAuth
     * @name MobileAuthSendSmsCode
     * @request POST:/api/platform/mobile-auth/send-sms-code
     * @secure
     */
    mobileAuthSendSmsCode: (data: SendSmsDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/mobile-auth/send-sms-code`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags MobileAuth
     * @name MobileAuthSmsLogin
     * @request POST:/api/platform/mobile-auth/sms-login
     * @secure
     */
    mobileAuthSmsLogin: (data: SmsLoginDto, params: RequestParams = {}) =>
      this.request<ApiResponse1AuthTokenDto, any>({
        path: `/api/platform/mobile-auth/sms-login`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Storage
     * @name StorageUpload
     * @request POST:/api/platform/storage/upload
     * @secure
     */
    storageUpload: (
      data: {
        formFileCollection?: File[];
      },
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1StorageMetaDtoArray1, any>({
        path: `/api/platform/storage/upload`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.FormData,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags Storage
     * @name StorageOriginFile
     * @request GET:/api/platform/storage/file/origin/{key}
     * @secure
     */
    storageOriginFile: (key: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/api/platform/storage/file/origin/${key}`,
        method: 'GET',
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Storage
     * @name StorageResizedFile
     * @request GET:/api/platform/storage/file/{w}x{h}/{key}
     * @secure
     */
    storageResizedFile: (
      key: string,
      h: number,
      w: number,
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/api/platform/storage/file/{w}x{h}/${key}`,
        method: 'GET',
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags Storage
     * @name StorageResizedUrl
     * @request GET:/api/platform/storage/sales-url-resize/unsafe/{url}/{w}x{h}/resize.png
     * @secure
     */
    storageResizedUrl: (
      url: string,
      h: number,
      w: number,
      params: RequestParams = {},
    ) =>
      this.request<void, any>({
        path: `/api/platform/storage/sales-url-resize/unsafe/${url}/{w}x{h}/resize.png`,
        method: 'GET',
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserRandomAvatars
     * @request GET:/api/platform/user/random-avatar
     * @secure
     */
    userRandomAvatars: (params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/api/platform/user/random-avatar`,
        method: 'GET',
        secure: true,
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserUserProfile
     * @request POST:/api/platform/user/profile
     * @secure
     */
    userUserProfile: (params: RequestParams = {}) =>
      this.request<ApiResponse1UserDto, any>({
        path: `/api/platform/user/profile`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags User
     * @name UserUpdateProfile
     * @request POST:/api/platform/user/update-profile
     * @secure
     */
    userUpdateProfile: (data: UserDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/user/update-profile`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserAddress
     * @name UserAddressList
     * @request POST:/api/platform/user-address/list
     * @secure
     */
    userAddressList: (params: RequestParams = {}) =>
      this.request<ApiResponse1UserAddressArray1, any>({
        path: `/api/platform/user-address/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserAddress
     * @name UserAddressGetById
     * @request POST:/api/platform/user-address/get-by-id
     * @secure
     */
    userAddressGetById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1UserAddress, any>({
        path: `/api/platform/user-address/get-by-id`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserAddress
     * @name UserAddressSave
     * @request POST:/api/platform/user-address/save
     * @secure
     */
    userAddressSave: (data: UserAddress, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/user-address/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserAddress
     * @name UserAddressSetDefault
     * @request POST:/api/platform/user-address/set-default
     * @secure
     */
    userAddressSetDefault: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/user-address/set-default`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserAddress
     * @name UserAddressDelete
     * @request POST:/api/platform/user-address/delete
     * @secure
     */
    userAddressDelete: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/user-address/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserInvoice
     * @name UserInvoiceList
     * @request POST:/api/platform/user-invoice/list
     * @secure
     */
    userInvoiceList: (params: RequestParams = {}) =>
      this.request<ApiResponse1UserInvoiceArray1, any>({
        path: `/api/platform/user-invoice/list`,
        method: 'POST',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserInvoice
     * @name UserInvoiceUpdateStatus
     * @request POST:/api/platform/user-invoice/update-status
     * @secure
     */
    userInvoiceUpdateStatus: (
      data: UpdateUserInvoiceStatusInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/user-invoice/update-status`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags UserInvoice
     * @name UserInvoiceSave
     * @request POST:/api/platform/user-invoice/save
     * @secure
     */
    userInvoiceSave: (data: UserInvoiceDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/platform/user-invoice/save`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags WechatMpAuth
     * @name WechatMpAuthWxMpOAuthCodeLogin
     * @request POST:/api/platform/wechat-mp-auth/oauth-code-login
     * @secure
     */
    wechatMpAuthWxMpOAuthCodeLogin: (
      data: OAuthCodeDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1AuthTokenDto, any>({
        path: `/api/platform/wechat-mp-auth/oauth-code-login`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  mall3H = {
    /**
     * No description
     *
     * @tags McsInvoiceRequest
     * @name McsInvoiceRequestQueryMcsBizOrderForInvoicing
     * @request POST:/api/mall-3h/invoice-request/query-mcs-biz-order-for-invoicing
     * @secure
     */
    mcsInvoiceRequestQueryMcsBizOrderForInvoicing: (
      data: QueryMcsBizOrderForInvoicingInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1McsBizOrderDto, any>({
        path: `/api/mall-3h/invoice-request/query-mcs-biz-order-for-invoicing`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequest
     * @name McsInvoiceRequestCreateInvoiceRequest
     * @request POST:/api/mall-3h/invoice-request/create
     * @secure
     */
    mcsInvoiceRequestCreateInvoiceRequest: (
      data: CreateInvoiceRequestInput,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1McsInvoiceRequestArray1, any>({
        path: `/api/mall-3h/invoice-request/create`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequest
     * @name McsInvoiceRequestDeleteById
     * @request POST:/api/mall-3h/invoice-request/delete
     * @secure
     */
    mcsInvoiceRequestDeleteById: (data: IdDto, params: RequestParams = {}) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-3h/invoice-request/delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequest
     * @name McsInvoiceRequestQueryPaging
     * @request POST:/api/mall-3h/invoice-request/query-paging
     * @secure
     */
    mcsInvoiceRequestQueryPaging: (
      data: QueryMcsInvoiceRequestInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1McsInvoiceRequestDto, any>({
        path: `/api/mall-3h/invoice-request/query-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequestAdmin
     * @name McsInvoiceRequestAdminQueryPaging
     * @request POST:/api/mall-3h/admin/invoice-request/query-paging
     * @secure
     */
    mcsInvoiceRequestAdminQueryPaging: (
      data: QueryMcsInvoiceRequestInput,
      params: RequestParams = {},
    ) =>
      this.request<PagedResponse1McsInvoiceRequestDto, any>({
        path: `/api/mall-3h/admin/invoice-request/query-paging`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequestAdmin
     * @name McsInvoiceRequestAdminMarkAsSuccess
     * @request POST:/api/mall-3h/admin/invoice-request/mark-as-success
     * @secure
     */
    mcsInvoiceRequestAdminMarkAsSuccess: (
      data: IdDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-3h/admin/invoice-request/mark-as-success`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequestAdmin
     * @name McsInvoiceRequestAdminMarkAsFailed
     * @request POST:/api/mall-3h/admin/invoice-request/mark-as-failed
     * @secure
     */
    mcsInvoiceRequestAdminMarkAsFailed: (
      data: IdDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-3h/admin/invoice-request/mark-as-failed`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags McsInvoiceRequestAdmin
     * @name McsInvoiceRequestAdminMarkAsPicked
     * @request POST:/api/mall-3h/admin/invoice-request/mark-as-picked
     * @secure
     */
    mcsInvoiceRequestAdminMarkAsPicked: (
      data: IdDto,
      params: RequestParams = {},
    ) =>
      this.request<ApiResponse1Object, any>({
        path: `/api/mall-3h/admin/invoice-request/mark-as-picked`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  region = {
    /**
     * No description
     *
     * @tags OutputCache
     * @name OutputCacheDelete
     * @request DELETE:/outputcache/{region}
     * @secure
     */
    outputCacheDelete: (region: string, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/outputcache/${region}`,
        method: 'DELETE',
        secure: true,
        ...params,
      }),
  };
}
