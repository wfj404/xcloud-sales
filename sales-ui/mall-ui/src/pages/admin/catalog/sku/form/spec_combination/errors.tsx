import { SkuDto } from '@/utils/swagger';

export default ({ model }: { model: SkuDto }) => {
  const errors = model.SpecCombinationErrors || [];

  return (
    <>
      <div style={{}}>
        {errors.map((x, i) => (
          <div
            style={{ marginBottom: 5, color: 'red', fontWeight: 'bold' }}
            key={i}
          >
            {x || '--'}
          </div>
        ))}
      </div>
    </>
  );
};
