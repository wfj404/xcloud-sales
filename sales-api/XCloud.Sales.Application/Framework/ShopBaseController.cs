﻿using XCloud.Platform.Application.Framework;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Queue;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Framework;

public abstract class ShopBaseController : PlatformBaseController
{
    protected SalesUtils SalesUtils => LazyServiceProvider.LazyGetRequiredService<SalesUtils>();
    
    protected IStoreUserAuthService StoreUserAuthService =>
        LazyServiceProvider.LazyGetRequiredService<IStoreUserAuthService>();

    protected IStoreManagerAuthService StoreManagerAuthService =>
        LazyServiceProvider.LazyGetRequiredService<IStoreManagerAuthService>();

    protected IStoreManagerPermissionService StoreManagerPermissionService =>
        LazyServiceProvider.LazyGetRequiredService<IStoreManagerPermissionService>();

    protected SalesEventBusService SalesEventBusService =>
        LazyServiceProvider.LazyGetRequiredService<SalesEventBusService>();

    protected ICurrentStoreContext CurrentStoreContext =>
        LazyServiceProvider.LazyGetRequiredService<ICurrentStoreContext>();
}