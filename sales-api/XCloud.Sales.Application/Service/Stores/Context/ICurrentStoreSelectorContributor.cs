﻿using JetBrains.Annotations;

namespace XCloud.Sales.Application.Service.Stores.Context;

public interface ICurrentStoreSelectorContributor
{
    [NotNull]
    [ItemCanBeNull]
    Task<string> GetStoreIdOrEmptyAsync();
}