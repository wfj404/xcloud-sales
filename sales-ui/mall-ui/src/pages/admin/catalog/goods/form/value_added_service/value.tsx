import {
  Button,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Space,
  Spin,
  Tag,
  Tooltip,
} from 'antd';
import { useState } from 'react';

import { apiClient } from '@/utils/client';
import { ValueAddedItemDto, ValueAddedItemGroupDto } from '@/utils/swagger';
import { formatMoney, isNumberEmpty } from '@/utils/utils';
import { DeleteOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';

export default ({
  model,
  ok,
}: {
  model: ValueAddedItemGroupDto;
  ok: () => void;
}) => {
  const data = model?.Items || [];

  const [loadingSave, _loadingSave] = useState(false);
  const [form, _form] = useState<ValueAddedItemDto | undefined>(undefined);
  const f: ValueAddedItemDto = form || {};

  const save = (row: ValueAddedItemDto) => {
    _loadingSave(true);
    apiClient.mallAdmin
      .valueAddedSaveGroupItem({
        ...row,
        GroupId: model.Id,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          _form(undefined);
          ok && ok();
        }
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const deleteGroupItem = (record: ValueAddedItemDto) => {
    if (!confirm('确定删除？')) {
      return;
    }
    _loadingSave(true);
    apiClient.mallAdmin
      .valueAddedDeleteGroupItem({ Id: record.Id })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          _form(undefined);
          ok && ok();
        }
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const renderPriceOffset = (x: ValueAddedItemDto) => {
    const price = x.PriceOffset || 0;
    if (price > 0) {
      return <span>{`+${formatMoney(price)}`}</span>;
    } else if (price < 0) {
      return <span>{`-${formatMoney(price)}`}</span>;
    } else {
      return null;
    }
  };

  return (
    <>
      <Modal
        title={'增值服务'}
        open={form != undefined}
        onCancel={() => _form(undefined)}
        footer={
          <Space size={2}>
            {isNumberEmpty(f.Id) || (
              <Button
                type="primary"
                danger
                icon={<DeleteOutlined />}
                onClick={() => {
                  deleteGroupItem(f);
                }}
              >
                删除
              </Button>
            )}
            <Button
              type="primary"
              icon={<SaveOutlined />}
              onClick={() => {
                save(f);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loadingSave}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="名称">
              <Tag color="processing">{model.Name || '--'}</Tag>
            </Form.Item>
            <Form.Item
              label="名称"
              rules={[
                {
                  required: true,
                },
                {
                  max: 20,
                },
              ]}
            >
              <Input
                value={f.Name || ''}
                onChange={(e) => {
                  _form({
                    ...form,
                    Name: e.target.value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item label="差价">
              <InputNumber
                defaultValue={f.PriceOffset || undefined}
                onChange={(e) => {
                  _form({
                    ...form,
                    PriceOffset: e || undefined,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
      <Space wrap direction="horizontal" size={2}>
        {data.map((x, index) => {
          return (
            <Tag
              key={index}
              color={'blue'}
              style={{
                cursor: 'pointer',
              }}
              onClick={() => {
                _form(x);
              }}
              closable={false}
            >
              <Tooltip title={undefined}>
                <Space direction="horizontal">
                  <span>{x.Name}</span>
                  {renderPriceOffset(x)}
                </Space>
              </Tooltip>
            </Tag>
          );
        })}
        <Tooltip title="添加服务明细">
          <Button
            icon={<PlusOutlined />}
            onClick={() => {
              _form({});
            }}
          ></Button>
        </Tooltip>
      </Space>
    </>
  );
};
