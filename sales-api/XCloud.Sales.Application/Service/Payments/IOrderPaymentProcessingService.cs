using XCloud.Application.DistributedLock;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Payments;

public interface IOrderPaymentProcessingService : ISalesAppService
{
    Task TryUpdatePaymentStatusAsync(IdDto input);
}

public class OrderPaymentProcessingService : SalesAppService, IOrderPaymentProcessingService
{
    private readonly OrderUtils _orderUtils;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderStockQuantityAdjustService _orderStockQuantityAdjustService;
    private readonly ISalesRepository<Order> _orderRepository;
    private readonly IOrderNoteService _orderNoteService;

    public OrderPaymentProcessingService(OrderUtils orderUtils,
        IOrderPaymentBillService orderPaymentBillService,
        ISalesRepository<Order> orderRepository,
        IOrderStockQuantityAdjustService orderStockQuantityAdjustService,
        IOrderNoteService orderNoteService)
    {
        _orderUtils = orderUtils;
        _orderPaymentBillService = orderPaymentBillService;
        _orderRepository = orderRepository;
        _orderStockQuantityAdjustService = orderStockQuantityAdjustService;
        _orderNoteService = orderNoteService;
    }

    private async Task MarkAsPaidAsync(string orderId)
    {
        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, orderId);

        if (order.IsPaid())
        {
            this.Logger.LogInformation($"skip:{nameof(MarkAsPaidAsync)}");
            return;
        }

        order.PaymentStatusId = SalesConstants.PaymentStatus.Paid;
        order.LastModificationTime = Clock.Now;

        await this._orderRepository.UpdateOrderAndTriggerChangedEventAsync(order, this.SalesEventBusService);

        await this._orderStockQuantityAdjustService.DeductOrderInventoryAfterPaymentAsync(order.Id);

        await _orderNoteService.InsertAsync(new OrderNoteDto()
        {
            OrderId = order.Id,
            Note = $"{this.L["order.paid.note"]}",
            DisplayToUser = true,
            CreationTime = Clock.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Paid,
                Data = new Dictionary<string, string>()
                {
                    ["Tips"] = $""
                }
            }
        });
    }

    public virtual async Task TryUpdatePaymentStatusAsync(IdDto input)
    {
        var resourceKey = $"{nameof(TryUpdatePaymentStatusAsync)}.order.id.{input.Id}";

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: resourceKey,
            expiryTime: TimeSpan.FromSeconds(5));

        var order = await _orderUtils.GetRequiredOrderForProcessAsync(this._orderRepository, input.Id);

        if (order.IsPaid())
        {
            Logger.LogInformation("payment status is paid already");
            return;
        }

        var bills = await _orderPaymentBillService.GetPaidBillsAsync(order.Id);

        order.PaidTotalPrice = bills.Sum(x => x.Price);
        order.PaymentStatusId = SalesConstants.PaymentStatus.Pending;

        await this._orderRepository.UpdateAsync(order);

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        if (_orderUtils.IsMoneyEqual(order.PaidTotalPrice, order.TotalPrice) ||
            order.PaidTotalPrice > order.TotalPrice ||
            order.TotalPrice <= decimal.Zero)
        {
            await this.MarkAsPaidAsync(order.Id);
        }
        else
        {
            if (order.PaidTotalPrice > 0)
            {
                this.Logger.LogInformation("部分支付");
            }
        }
    }
}