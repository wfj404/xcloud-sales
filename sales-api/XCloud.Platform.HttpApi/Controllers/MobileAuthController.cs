using System.Security.Claims;
using Volo.Abp;
using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Token;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/mobile-auth")]
public class MobileAuthController : PlatformBaseController
{
    private readonly IUserMobileService _userMobileService;
    private readonly IUserAccountService _userAccountService;
    private readonly IValidationTokenService _validationTokenService;
    private readonly ITokenService _tokenService;

    public MobileAuthController(IUserAccountService userAccountService,
        IValidationTokenService validationTokenService,
        IUserMobileService userMobileService, ITokenService tokenService)
    {
        _userMobileService = userMobileService;
        _userAccountService = userAccountService;
        _validationTokenService = validationTokenService;
        _tokenService = tokenService;
    }

    private string SmsLoginValidationCodeGroup => "sms-login";

    [HttpPost("send-sms-code")]
    public async Task<ApiResponse<object>> SendSmsCodeAsync([FromBody] SendSmsDto dto)
    {
        dto.Mobile = _userMobileService.NormalizeMobilePhone(dto.Mobile);

        var user = await _userMobileService.GetUserByPhoneAsync(dto.Mobile);

        if (user == null)
        {
            if (await _userMobileService.IsPhoneExistAsync(dto.Mobile))
                throw new UserFriendlyException("the account associated to this phone number is not available");

            user = await _userAccountService.CreateUserAccountV1Async(new IdentityNameDto
                { IdentityName = GuidGenerator.CreateGuidString() });

            var setMobileResponse =
                await _userMobileService.SetUserMobilePhoneAsync(user.Id, dto.Mobile);
            setMobileResponse.ThrowIfErrorOccured();

            await _userMobileService.ConfirmMobileAsync(setMobileResponse.Data.Id);
        }

        var rand = new Random((int)Clock.Now.Ticks);
        var charPool = "123456789".ToArray();

        var code = string.Join(string.Empty, Com.Range(0, 4).Select(x => rand.Choice(charPool)).ToArray());

        await _userMobileService.SendSmsAsync(new UserPhoneBindSmsMessage { Phone = dto.Mobile, Code = code });
        await _validationTokenService.AddValidationCodeAsync(SmsLoginValidationCodeGroup, user.Id, code);

        return new ApiResponse<object>();
    }

    [HttpPost("sms-login")]
    public async Task<ApiResponse<AuthTokenDto>> SmsLoginAsync([FromBody] SmsLoginDto dto)
    {
        dto.Mobile = _userMobileService.NormalizeMobilePhone(dto.Mobile);

        var user = await _userMobileService.GetUserByPhoneAsync(dto.Mobile);

        if (user == null)
            throw new UserFriendlyException("the account is not available");

        var codeResponse =
            await _validationTokenService.GetValidationCodeAsync(SmsLoginValidationCodeGroup, user.Id);

        if (codeResponse == null || codeResponse.Code != dto.SmsCode ||
            codeResponse.CreateTime < Clock.Now.AddMinutes(-5))
            throw new UserFriendlyException("wrong sms code");

        var identity = new ClaimsIdentity().SetSubjectId(user.Id)
            .SetCreationTime(Clock.Now);

        var token = await _tokenService.CreateTokenAsync(identity.Claims.ToArray());

        return new ApiResponse<AuthTokenDto>(new AuthTokenDto
        {
            AccessToken = token
        });
    }
}