using XCloud.Application.Model;
using XCloud.Application.Utils;
using XCloud.Sales.Application.Domain.Mapping;

namespace XCloud.Sales.Application.Domain.Discounts;

[TypeIdentityName("discount")]
public class Discount : SalesBaseEntity<string>, IHasCreationTime, ISoftDelete, IStoreMappingSupported,
    IMayHaveTimeRange
{
    public string Name { get; set; }

    public string Description { get; set; }

    public double DiscountPercentage { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public bool IsActive { get; set; }

    public DateTime CreationTime { get; set; }

    public int Sort { get; set; }

    public bool IsDeleted { get; set; }

    public bool LimitedToStore { get; set; }

    public bool LimitedToSku { get; set; }
}