using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.GiftCards;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.GiftCards;

public class QueryGiftCardPagingInput : PagedRequest
{
    public int? UserId { get; set; }
    public bool? Used { get; set; }
}

public class UpdateGiftCardStatusInput : IEntityDto<string>
{
    public string Id { get; set; }
    public bool? IsActive { get; set; }
    public bool? IsDeleted { get; set; }
}

public class GiftCardDto : GiftCard, IEntityDto
{
    public StoreUserDto StoreUser { get; set; }

    public bool? Expired { get; set; }
}

public class UseGiftCardInput : IEntityDto
{
    public string CardId { get; set; }
    public int UserId { get; set; }
}