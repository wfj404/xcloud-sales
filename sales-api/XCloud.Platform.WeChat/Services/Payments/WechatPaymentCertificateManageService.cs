﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.Logging;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Models;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Settings;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Service;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Settings;

namespace XCloud.Platform.WeChat.Services.Payments;

[TypeIdentityName("wechat.mch.certificate.entry.list")]
public class MchCertificateEntryGroup : ISettings
{
    private CertificateEntry[] _entries;

    [NotNull]
    public CertificateEntry[] Entries
    {
        get => this._entries ??= [];
        set => this._entries = value;
    }
}

[ExposeServices(typeof(WechatPaymentCertificateManageService))]
public class WechatPaymentCertificateManageService : XCloudAppService
{
    private readonly IKeyValueService _keyValueService;

    public WechatPaymentCertificateManageService(IKeyValueService keyValueService)
    {
        _keyValueService = keyValueService;
    }

    private string GetSettingsKey(string mchId)
    {
        var typeName = this.CommonUtils.GetTypeIdentityName<MchCertificateEntryGroup>();

        return $"{typeName}@{mchId}";
    }

    public async Task<CertificateEntry[]> GetCertificateEntriesAsync(string mchId)
    {
        if (string.IsNullOrWhiteSpace(mchId))
            throw new ArgumentNullException(nameof(mchId));

        var key = GetSettingsKey(mchId);
        var data = await this._keyValueService.GetObjectOrDefaultAsync<MchCertificateEntryGroup>(key);
        data ??= new MchCertificateEntryGroup();

        return data.Entries;
    }

    private const string AlgorithmType = "RSA";

    public async Task UpdateMchCertificateEntriesAsync(WechatTenpayClient client)
    {
        var request = new QueryCertificatesRequest() { AlgorithmType = AlgorithmType };
        var response = await client.ExecuteQueryCertificatesAsync(request);

        if (response.IsSuccessful())
        {
            var responseCertificates = response.CertificateList
                .Select(x => CertificateEntry.Parse(AlgorithmType, x)).ToArray();

            var certificateList = await this.GetCertificateEntriesAsync(client.Credentials.MerchantId);
            
            certificateList = certificateList.CombineCertificateEntries(responseCertificates)
                .ToArray();

            var key = GetSettingsKey(client.Credentials.MerchantId);

            await this._keyValueService.SaveObjectAsync(key,
                new MchCertificateEntryGroup() { Entries = certificateList });

            this.Logger.LogInformation("刷新微信商户平台证书成功。");
        }
        else
        {
            this.Logger.LogWarning(
                "刷新微信商户平台证书失败（状态码：{0}，错误代码：{1}，错误描述：{2}）。",
                response.GetRawStatus(), response.ErrorCode, response.ErrorMessage
            );
        }
    }
}