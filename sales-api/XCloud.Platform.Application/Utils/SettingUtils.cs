using Volo.Abp.DependencyInjection;
using XCloud.Application.Extension;
using XCloud.Application.Utils;

namespace XCloud.Platform.Application.Utils;

[ExposeServices(typeof(SettingUtils))]
public class SettingUtils : ISingletonDependency
{
    private readonly CommonUtils _commonUtils;

    public SettingUtils(CommonUtils commonUtils)
    {
        _commonUtils = commonUtils;
    }

    public string GetSettingsRequiredTypeIdentityName<T>(string storeId = null) where T : class
    {
        var name = _commonUtils.GetRequiredTypeIdentityName<T>();

        name = $"settings.{name}";

        if (!string.IsNullOrWhiteSpace(storeId))
        {
            name = $"{name}@store:{storeId}";
        }

        return name;
    }
}