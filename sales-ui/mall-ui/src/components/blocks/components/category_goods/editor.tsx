import { Button, Form, InputNumber, Segmented, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import PreviewGroup from '@/components/common/preview_group';
import { AntDesignTreeNodeV2, getFlatNodes } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { CategoryDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import { GoodsDisplayType, GoodsDisplayTypeOptions } from '../../utils';
import { CategoryGoodsBlockProps } from './utils';

export default ({
  data,
  onChange,
}: {
  data: CategoryGoodsBlockProps;
  onChange: (x: CategoryGoodsBlockProps) => void;
}) => {
  const [loading, _loading] = useState(true);
  const [category_datalist, _category_datalist] = useState<
    AntDesignTreeNodeV2<CategoryDto>[]
  >([]);
  const [expandKeys, _expandKeys] = useState<any[]>([]);
  const autoExpand = false;

  const queryList = () => {
    _loading(true);
    apiClient.mallAdmin
      .categoryQueryAntdTree({})
      .then((res) => {
        handleResponse(res, () => {
          let treeData = res.data.Data || [];
          _category_datalist(treeData);
          if (autoExpand) {
            const keys = treeData
              .flatMap((x) => getFlatNodes(x))
              .map((x) => x.key || '');
            _expandKeys(keys);
          }
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnType<AntDesignTreeNodeV2<CategoryDto>>[] = [
    {
      title: '名称',
      render: (record: AntDesignTreeNodeV2<CategoryDto>) => record.title,
    },
    {
      title: 'SeoName',
      render: (x: AntDesignTreeNodeV2<CategoryDto>) =>
        x.raw_data?.SeoName || '--',
    },
    {
      title: '描述',
      render: (x: AntDesignTreeNodeV2<CategoryDto>) => x.raw_data?.Description,
    },
    {
      title: '图片',
      render: (x: AntDesignTreeNodeV2<CategoryDto>) => {
        const record = x.raw_data || {};

        if (record.Picture == null) {
          return null;
        }

        return (
          <>
            <PreviewGroup data={[record.Picture]} />
          </>
        );
      },
    },
    {
      title: '选择',
      render: (d, x) => {
        if (!x.raw_data) {
          return <p>error raw data</p>;
        }
        if (
          data?.category?.Id == x.raw_data?.Id &&
          !isStringEmpty(data?.category?.Id)
        ) {
          return <Tag color={'green'}>当前选择</Tag>;
        }
        return (
          <>
            <Button
              type={'dashed'}
              size={'small'}
              onClick={() => {
                const selected: CategoryGoodsBlockProps = {
                  ...data,
                  category: x.raw_data,
                };
                onChange(selected);
              }}
            >
              选择
            </Button>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <Space direction={'vertical'} style={{ width: '100%' }}>
        <Form
          labelCol={{
            span: 3,
          }}
        >
          <Form.Item label={'当前选择分类'}>
            <Tag color={'green'}>{data?.category?.Name || '--'}</Tag>
          </Form.Item>
          <Form.Item label={'显示数量'}>
            <InputNumber
              min={1}
              value={data?.count}
              onChange={(e) => {
                onChange && onChange({ ...data, count: e || undefined });
              }}
            />
          </Form.Item>
          <Form.Item label={'展示方式'}>
            <Segmented
              required
              defaultValue={'grid' as GoodsDisplayType}
              options={GoodsDisplayTypeOptions}
              value={data?.display_type}
              onChange={(e) => {
                if (!e) {
                  return;
                }
                onChange({
                  ...data,
                  display_type: e as GoodsDisplayType,
                });
              }}
            />
          </Form.Item>
        </Form>
        <Table
          rowKey={(x) => x.key || ''}
          expandable={{
            expandedRowKeys: expandKeys,
            onExpandedRowsChange: (expandedKeys: any) => {
              _expandKeys(expandedKeys);
            },
          }}
          loading={loading}
          columns={columns}
          dataSource={category_datalist}
          pagination={false}
        />
      </Space>
    </>
  );
};
