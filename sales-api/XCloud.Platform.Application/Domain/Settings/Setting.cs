﻿namespace XCloud.Platform.Application.Domain.Settings;

public class Setting : PlatformBaseEntity
{
    public string Name { get; set; }
    
    public string Value { get; set; }
}