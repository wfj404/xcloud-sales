import XEmptyImage from '@/assets/static/illustrations/illustration_register.png';
import { Alert } from '@mui/material';

export default function() {
  return (
    <>
      <div style={{ margin: 20 }}>
        <div style={{ fontSize: 100, textAlign: 'center' }}>
          <img src={XEmptyImage} alt='' />
        </div>
        <Alert severity='info'>暂无数据</Alert>
      </div>
    </>
  );
}
