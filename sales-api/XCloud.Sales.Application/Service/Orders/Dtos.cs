﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Address;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Service.AfterSale;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Coupons;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Shipping.Delivery;
using XCloud.Sales.Application.Service.Shipping.Pickup;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Orders;

public class OrderReviewDto : OrderReview, IEntityDto<string>
{
    public OrderReviewItemDto[] Items { get; set; }

    public StorageMetaDto[] StorageMeta { get; set; }

    public StoreUserDto StoreUser { get; set; }

    public string[] MetaIds { get; set; }
}

public class OrderReviewItemDto : OrderReviewItem, IEntityDto<string>
{
    public StorageMetaDto[] StorageMeta { get; set; }

    public string[] MetaIds { get; set; }

    public OrderReviewDto OrderReview { get; set; }

    public OrderItemDto OrderItem { get; set; }
}

public class QueryOrderReviewItemPaging : PagedRequest
{
    public string Keywords { get; set; }
    public string StoreId { get; set; }
    public string GoodsId { get; set; }
    public bool? IsApproved { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class QueryOrderReviewPaging : PagedRequest
{
    public string Keywords { get; set; }
    public string StoreId { get; set; }
    public bool? IsApproved { get; set; }
    public DateTime? StartTime { get; set; }
    public DateTime? EndTime { get; set; }
}

public class OrderCouponRecordDto : OrderCouponRecord, IEntityDto<string>
{
    public CouponUserMappingDto CouponUserMapping { get; set; }

    public CouponDto Coupon { get; set; }
}

public class CreateOrderPayBillInput : IEntityDto
{
    public string OrderId { get; set; }
    public decimal? PriceOrNull { get; set; }
    public string Description { get; set; }
}

public class QueryPendingCountInput : IEntityDto
{
    public int? UserId { get; set; }
}

public class QueryAfterSalePendingCountInput : IEntityDto
{
    public int? UserId { get; set; }
}

public class UpdateOrderStatusInput : IEntityDto<string>
{
    public string Id { get; set; }

    public string OrderStatus { get; set; }

    public string PaymentStatus { get; set; }

    public string DeliveryStatus { get; set; }
}

public class CloseOrderInput : IEntityDto
{
    public string OrderId { get; set; }

    public string Comment { get; set; }
}

public class FinishOrderInput : IEntityDto
{
    public string OrderId { get; set; }

    public string Comment { get; set; }
}

public class OrderItemGiftDto : OrderItemGift, IEntityDto<string>
{
    //
}

public class OrderItemDto : OrderItem, IEntityDto
{
    public SkuDto Sku { get; set; }

    public SpecItemDto[] SpecItems { get; set; }

    public OrderItemGiftDto[] OrderItemGifts { get; set; }

    public OrderValueAddedItemDto[] OrderValueAddedItems { get; set; }
}

public class OrderValueAddedItemDto : OrderValueAddedItem, IEntityDto
{
    public ValueAddedItemDto ValueAddedItem { get; set; }
}

public class OrderNoteDto : OrderNote, IEntityDto<int>
{
    public OrderNoteExtended OrderNoteExtended { get; set; }
}

public class QueryOrderNotesInput : IEntityDto
{
    public string OrderId { get; set; }

    public bool OnlyForUser { get; set; }

    public int? MaxCount { get; set; }
}

public class OrderDto : Order, IEntityDto<string>
{
    [Obsolete] public bool IsAfterSales => this.OrderStatusId == SalesConstants.OrderStatus.AfterSales;

    public UserInvoiceDto UserInvoice { get; set; }

    public UserAddressDto UserAddress { get; set; }

    public DeliveryRecordDto DeliveryRecord { get; set; }

    public PickupRecordDto PickupRecord { get; set; }

    public StoreDto Store { get; set; }

    public StoreUserDto StoreUser { get; set; }

    public OrderItemDto[] Items { get; set; }

    public PaymentBillDto[] PaymentBills { get; set; }

    public RefundBillDto[] RefundBills { get; set; }

    public OrderCouponRecordDto[] OrderCouponRecords { get; set; }

    public AfterSalesDto AfterSales { get; set; }

    public OrderReviewDto OrderReview { get; set; }
}

public class QueryOrderPagingInput : PagedRequest
{
    public string Sn { get; set; }

    public int? AffiliateId { get; set; }

    public string StoreId { get; set; }

    public string[] ShippingMethodId { get; set; }

    public string[] PaymentMethodId { get; set; }

    public bool? IsAfterSales { get; set; }

    public bool? IsAfterSalesPending { get; set; }

    public string[] AfterSalesStatus { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public string[] Status { get; set; }

    public string[] PaymentStatus { get; set; }

    public string[] DeliveryStatus { get; set; }

    public bool? ToService { get; set; }

    public bool? ToReview { get; set; }

    public int? UserId { get; set; }

    public bool? HideForCustomer { get; set; }
}