﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Mapping.Stores;

public class StoreRoleMap : SalesEntityTypeConfiguration<StoreRole>
{
    public override void Configure(EntityTypeBuilder<StoreRole> builder)
    {
        builder.ToTable("sales_store_role");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.StoreId).RequiredId();
        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(100);

        base.Configure(builder);
    }
}