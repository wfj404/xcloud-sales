using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderValueAddedItemMap : SalesEntityTypeConfiguration<OrderValueAddedItem>
{
    public override void Configure(EntityTypeBuilder<OrderValueAddedItem> builder)
    {
        builder.ToTable("sales_order_value_added_item");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.OrderItemId).RequiredId();
        builder.Property(x => x.ValueAddedItemId);
        builder.Property(x => x.PriceOffset).PricePrecision();
        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);

        base.Configure(builder);
    }
}