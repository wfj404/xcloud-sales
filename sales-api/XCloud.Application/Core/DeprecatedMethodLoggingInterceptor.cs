﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using Volo.Abp.DynamicProxy;

namespace XCloud.Application.Core;

[ExposeServices(typeof(DeprecatedMethodLoggingInterceptor), typeof(IAbpInterceptor))]
public class DeprecatedMethodLoggingInterceptor : IAbpInterceptor, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public DeprecatedMethodLoggingInterceptor(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task InterceptAsync(IAbpMethodInvocation invocation)
    {
        var env = _serviceProvider.GetRequiredService<IWebHostEnvironment>();

        if (env.IsStaging() || env.IsDevelopment())
        {
            var logger =
                _serviceProvider.GetRequiredService<ILogger<DeprecatedMethodLoggingInterceptor>>();
            try
            {
                var method = invocation.Method;
                var attr = method.GetCustomAttributes<ObsoleteAttribute>().FirstOrDefault();
                if (attr != null)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine(nameof(DeprecatedMethodLoggingInterceptor));
                    sb.AppendLine(attr.Message ?? "null-attr-message");
                    sb.AppendLine(method.DeclaringType?.FullName ?? "null-type-name");
                    sb.AppendLine(method.Name);

                    logger.LogWarning(message: sb.ToString());
                }
            }
            catch (Exception e)
            {
                logger.LogError(message: e.Message, exception: e);
            }
        }

        await invocation.ProceedAsync();
    }
}