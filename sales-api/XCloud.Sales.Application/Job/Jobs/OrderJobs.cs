﻿using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Job.Jobs;

[UnitOfWork]
public class OrderJobs : SalesAppService
{
    private readonly IOrderAutoProcessService _orderAutoProcessService;

    public OrderJobs(IOrderAutoProcessService orderAutoProcessService)
    {
        _orderAutoProcessService = orderAutoProcessService;
    }

    public virtual async Task CloseDeadOrdersAsync()
    {
        await _orderAutoProcessService.CloseDeadOrdersAsync();
    }
}