using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Discounts;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Discounts;

public class DiscountDto : Discount, IEntityDto<string>
{
    public SkuDto[] Skus { get; set; }

    public StoreDto[] Stores { get; set; }
}

public class QueryDiscountPagingInput : PagedRequest
{
    public string StoreId { get; set; }

    public DateTime? Time { get; set; }
}