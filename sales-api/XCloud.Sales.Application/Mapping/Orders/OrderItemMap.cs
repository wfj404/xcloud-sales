using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderItemMap : SalesEntityTypeConfiguration<OrderItem>
{
    public override void Configure(EntityTypeBuilder<OrderItem> builder)
    {
        builder.ToTable("sales_order_item");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.OrderId).RequiredId();

        builder.Property(x => x.Quantity);

        builder.Property(x => x.GoodsName).HasMaxLength(500);
        builder.Property(x => x.SkuId).RequiredId();
        builder.Property(x => x.SkuName).HasMaxLength(500);
        builder.Property(x => x.SkuDescription).HasMaxLength(500);
        builder.Property(x => x.Weight);
        builder.Property(x => x.StockDeductStrategy).RequiredId();

        builder.Property(x => x.BasePrice).HasPrecision(18, 2);
        builder.Property(x => x.DiscountRate);
        builder.Property(o => o.GradePriceOffset).HasPrecision(18, 2);
        builder.Property(x => x.ValueAddedPriceOffset).HasPrecision(18, 2);
        builder.Property(x => x.UnitPrice).HasPrecision(18, 2);
        builder.Property(x => x.TotalPrice).HasPrecision(18, 2);

        builder.Property(x => x.Remark).HasMaxLength(100);

        base.Configure(builder);
    }
}