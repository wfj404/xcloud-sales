import { message, Modal, Spin } from 'antd';
import L from 'leaflet';
import { useEffect, useState } from 'react';

import XStoreForm from '@/components/common/store_form';
import { apiClient } from '@/utils/client';
import { StoreDto } from '@/utils/swagger';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: StoreDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [model, _model] = useState<StoreDto>({});

  const save = (row: StoreDto) => {
    _loading(true);

    apiClient.mallAdmin
      .storeCreateStore({ ...row })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...data,
    });
  }, [data]);

  const getLatLng = (): L.LatLngLiteral | undefined => {
    if (model.Lat == undefined || model.Lon == undefined) {
      return undefined;
    }

    return {
      lat: model.Lat,
      lng: model.Lon,
    };
  };

  return (
    <>
      <Modal
        title={'门店'}
        open={show}
        onCancel={() => hide()}
        onOk={() => save(model)}
        width={'80%'}
        forceRender
        destroyOnClose
      >
        <Spin spinning={loading}>
          <XStoreForm
            model={model}
            onChange={(e) => {
              _model({ ...e });
            }}
          />
        </Spin>
      </Modal>
    </>
  );
};
