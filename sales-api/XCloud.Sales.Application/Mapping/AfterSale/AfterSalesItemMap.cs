﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.AfterSale;

namespace XCloud.Sales.Application.Mapping.AfterSale;

public class AfterSalesItemMap : SalesEntityTypeConfiguration<AfterSalesItem>
{
    public override void Configure(EntityTypeBuilder<AfterSalesItem> builder)
    {
        builder.ToTable("sales_after_sales_item");
        
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.AfterSalesId).RequiredId();
        builder.Property(x => x.OrderId).RequiredId();
        builder.Property(x => x.OrderItemId).RequiredId();
        builder.Property(x => x.Quantity);

        base.Configure(builder);
    }
}