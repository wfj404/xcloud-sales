﻿using FreeSql;
using Volo.Abp.Domain.Entities;

namespace XCloud.FreeSql;

public interface IFreeSqlIntKeyRepository<T> : IBaseRepository<T, int> where T : class, IEntity<int>
{
    //
}

public class FreeSqlIntKeyRepository<T> : DefaultRepository<T, int>, IFreeSqlIntKeyRepository<T>
    where T : class, IEntity<int>
{
    public FreeSqlIntKeyRepository(UnitOfWorkManager unitOfWorkManager) : base(unitOfWorkManager.Orm, unitOfWorkManager)
    {
        //
    }
}