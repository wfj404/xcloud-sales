﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Primitives;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.Spa;

public class FallToIndexFileProvider : IFileProvider
{
    private readonly string _rootDir;

    public FallToIndexFileProvider(string rootDir)
    {
        if (string.IsNullOrWhiteSpace(rootDir))
            throw new ArgumentNullException(nameof(rootDir));

        _rootDir = rootDir;
    }

    public IFileInfo GetFileInfo(string subpath)
    {
        var fallBackList = new[] { subpath, "index.html" }
            .Select(x => x?.Trim('/', '\\'))
            .WhereNotEmpty()
            .Distinct()
            .ToArray();

        foreach (var p in fallBackList)
        {
            var filePath = Path.Combine(this._rootDir, p);

            var info = new FileInfo(filePath);

            if (info.Exists)
            {
                return new PhysicalFileInfo(info);
            }
        }

        return new NotFoundFileInfo(subpath ?? nameof(FallToIndexFileProvider));
    }

    public IDirectoryContents GetDirectoryContents(string subpath)
    {
        return NotFoundDirectoryContents.Singleton;
    }

    public IChangeToken Watch(string filter)
    {
        return NullChangeToken.Singleton;
    }
}