import React, { useEffect } from 'react';
import { history, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isStoreUserLogin } from '@/utils/biz';
import { StoreUserDto } from '@/utils/swagger';
import NotificationsIcon from '@mui/icons-material/Notifications';
import { Badge, IconButton } from '@mui/material';

export default function () {
  const app = useSnapshot(storeState) as StoreType;

  const queryNotificationCount = () => {
    if (!isStoreUserLogin(app.storeUser as StoreUserDto)) {
      return;
    }
    app.queryNotificationCount();
  };

  useEffect(() => {
    queryNotificationCount();
  }, []);

  useEffect(() => {
    if (!isStoreUserLogin(app.storeUser as StoreUserDto)) {
      app.setNotificationCount(0);
    }
  }, [app.storeUser]);

  const count = app.notificationCount || 0;

  return (
    <>
      <IconButton
        size="large"
        color="inherit"
        onClick={() => {
          history.push('/inbox');
        }}
        sx={{}}
      >
        <Badge
          variant="dot"
          badgeContent={count}
          invisible={count <= 0}
          color="error"
        >
          <NotificationsIcon />
        </Badge>
      </IconButton>
    </>
  );
}
