﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using Volo.Abp.DependencyInjection;

namespace XCloud.Application.Utils;

[ExposeServices(typeof(GZipUtils))]
public class GZipUtils : ITransientDependency
{
    static Encoding Encoding => Encoding.UTF8;

    /// <summary>
    /// 压缩
    /// </summary>
    /// <param name="text">文本</param>
    public string Compress(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return text;
        }

        var buffer = Encoding.GetBytes(text);
        return Convert.ToBase64String(Compress(buffer));
    }

    /// <summary>
    /// 解压缩
    /// </summary>
    /// <param name="text">文本</param>
    public string Decompress(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
        {
            return text;
        }

        var buffer = Convert.FromBase64String(text);
        using var ms = new MemoryStream(buffer);
        using var zip = new GZipStream(ms, CompressionMode.Decompress);
        using var reader = new StreamReader(zip);
        return reader.ReadToEnd();
    }

    /// <summary>
    /// 压缩
    /// </summary>
    /// <param name="buffer">字节流</param>
    public byte[] Compress(byte[] buffer)
    {
        if (buffer == null)
            return null;
        using var ms = new MemoryStream();
        using (var zip = new GZipStream(ms, CompressionMode.Compress, true))
        {
            zip.Write(buffer, 0, buffer.Length);
        }

        return ms.ToArray();
    }

    /// <summary>
    /// 解压缩
    /// </summary>
    /// <param name="buffer">字节流</param>
    public byte[] Decompress(byte[] buffer)
    {
        if (buffer == null)
            return null;

        using var ms = new MemoryStream(buffer);
        using var zip = new GZipStream(ms, CompressionMode.Decompress);
        using var reader = new StreamReader(zip);
        return Encoding.GetBytes(reader.ReadToEnd());
    }
}