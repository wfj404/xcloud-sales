import { List } from 'antd-mobile';

import { ShippingMethod } from '@/utils/biz';
import { PlaceOrderDataHolder, PlaceOrderRequestDto } from '@/utils/swagger';
import { formatMoney, isStringEmpty } from '@/utils/utils';
import { Typography } from '@mui/material';

export default ({
  model,
  data,
}: {
  model: PlaceOrderDataHolder;
  data: PlaceOrderRequestDto;
}) => {
  const order = model.Order;

  if (!order) {
    return null;
  }

  const promotion_price = order.PromotionPriceOffset || 0;
  const coupon_price = order.CouponPrice || 0;
  const shipping_fee = order.ShippingFee || 0;

  const coupon_names = order.OrderCouponRecords?.map(
    (x) => x.Coupon?.Name || '',
  )
    .filter((x) => !isStringEmpty(x))
    .join(',');

  return (
    <>
      <List header="订单金额" mode="card">
        {promotion_price != 0 && (
          <List.Item extra={formatMoney(promotion_price)}>活动价</List.Item>
        )}
        {coupon_price != 0 && (
          <List.Item
            description={coupon_names || undefined}
            extra={formatMoney(coupon_price)}
          >
            优惠券
          </List.Item>
        )}
        {data.ShippingMethodId == ShippingMethod.Pickup || (
          <List.Item extra={formatMoney(shipping_fee)}>配送费</List.Item>
        )}
        <List.Item
          extra={
            <Typography variant="overline" color="error">
              {formatMoney(order.TotalPrice || 0)}
            </Typography>
          }
        >
          最终价格
        </List.Item>
      </List>
    </>
  );
};
