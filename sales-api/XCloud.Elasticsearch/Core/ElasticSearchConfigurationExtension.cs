﻿using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using XCloud.Core.Exceptions;

namespace XCloud.Elasticsearch.Core;

public static class ElasticSearchConfigurationExtension
{
    internal static void AddElasticSearchClient(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        var option = config.GetRequiredElasticSearchOption();

        var client = new ElasticSearchClient(option.Hosts);

        context.Services.AddSingleton(client);
    }

    [NotNull]
    public static IConfigurationSection GetEsConfigurationSectionOrEmpty(this IConfiguration configuration)
    {
        return configuration.GetRequiredSection("es");
    }

    [NotNull]
    public static ElasticSearchOption GetRequiredElasticSearchOption(this IConfiguration configuration)
    {
        var option = new ElasticSearchOption();

        var section = GetEsConfigurationSectionOrEmpty(configuration);

        if (!section.Exists())
            throw new ConfigException(nameof(GetEsConfigurationSectionOrEmpty));

        section.Bind(option);

        return option;
    }
}