using Volo.Abp.BlobStoring;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Application.Service.Storage.Temp;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Service.Storage;

[ExposeServices(typeof(StorageToolbox))]
public class StorageToolbox : ITransientDependency
{
    private readonly IAbpLazyServiceProvider _abpLazyServiceProvider;

    public StorageToolbox(IAbpLazyServiceProvider abpLazyServiceProvider)
    {
        _abpLazyServiceProvider = abpLazyServiceProvider;
    }

    public IStorageMetaService StorageMetaService =>
        _abpLazyServiceProvider.LazyGetRequiredService<IStorageMetaService>();

    public IFileHashProvider FileHashProvider =>
        _abpLazyServiceProvider.LazyGetRequiredService<IFileHashProvider>();

    public ITempFileService TempFileService =>
        _abpLazyServiceProvider.LazyGetRequiredService<ITempFileService>();

    public StorageUtils StorageUtils =>
        _abpLazyServiceProvider.LazyGetRequiredService<StorageUtils>();

    public IBlobContainerFactory BlobContainerFactory =>
        _abpLazyServiceProvider.LazyGetRequiredService<IBlobContainerFactory>();
}