import { MessageInstance } from 'antd/es/message/interface';
import { AxiosResponse } from 'axios';
import currency from 'currency.js';
import { trim } from 'lodash-es';
import pinyin from 'pinyin';

import { StoreType } from '@/store';
import { ApiResponse } from '@/utils/biz';

export interface GlobalContainerType {
  message?: MessageInstance;
  store?: StoreType;
  setGoodsDetailId?: (x: string | undefined | null) => void;
  sendWs?: (e: string) => void;
}

export const GlobalContainer: GlobalContainerType = {
  message: undefined,
  store: undefined,
  setGoodsDetailId: undefined,
};

export const isInIframe = (): boolean => {
  return (
    self != top ||
    window.frames.length != parent.frames.length ||
    self.frameElement?.tagName?.toLowerCase() == 'iframe'
  );
};

export const isMobileBrowser = () => {
  return /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i.test(
    navigator.userAgent,
  );
};

export const swapArrayPosition = <T>(
  arr: T[],
  index1: number,
  index2: number,
): T[] => {
  const temp = arr.at(index1)!;
  arr[index1] = arr.at(index2)!;
  arr[index2] = temp;

  return arr;
};

export const insertAt = <T>(arr: T[], item: T, index: number): T[] => {
  let datalist = [...arr];
  if (index > arr.length - 1 || index < 0) {
    datalist.push(item);
  } else {
    //insert
    //swapArrayPosition(datalist, index, datalist.length - 1);
    datalist = [
      ...datalist.slice(0, index),
      item,
      ...datalist.slice(index, undefined),
    ];
  }

  return datalist;
};

export const isStringEmpty = (str?: string | null): boolean =>
  !str || str.length <= 0;

export const isArrayEmpty = <T>(arr?: T[] | null): boolean =>
  !arr || arr.length <= 0;

export const isNumberEmpty = (num?: number | null) => !num || num <= 0;

export const getPinyinFirstLetter = (
  name?: string | null,
): string | undefined => {
  if (!name || name.length <= 0) {
    return undefined;
  }

  const py = getPinyinShortStyle(name);

  if (isStringEmpty(py)) {
    return undefined;
  }

  return py?.substring(0, 1);
};

export const getPinyinShortStyle = (name: string): string => {
  let py = pinyin(name || '', {
    style: pinyin.STYLE_FIRST_LETTER,
  });
  return py
    .flatMap((x) => x.flatMap((d) => d.split(' ')))
    .filter((x) => !isStringEmpty(x))
    .join('');
};

export const getPinyinNormalStyle = (name: string): string => {
  let py = pinyin(name || '', {
    style: pinyin.STYLE_NORMAL,
  });

  return py
    .flatMap((x) => x.flatMap((d) => d.split(' ')))
    .filter((x) => !isStringEmpty(x))
    .join('');
};

export const getPinyinSeoNameStyle = (name: string): string => {
  let py = pinyin(name || '', {
    style: pinyin.STYLE_NORMAL,
  });

  return py
    .flatMap((x) => x.flatMap((d) => d.split(' ')))
    .filter((x) => !isStringEmpty(x))
    .join('-')
    .toLowerCase();
};

export const handleResponse = (
  res: AxiosResponse<ApiResponse<any>>,
  callback?: () => void,
): boolean => {
  if (res.data.Error?.Message) {
    errorMsg(res.data.Error.Message || '操作未能如期完成');
    return false;
  } else {
    callback && callback();
    return true;
  }
};

export const concatUrl = (paths: string[]): string => {
  return (
    paths
      .map((x) => x || '')
      .map((x) => trim(x, '/'))
      .map((x) => trim(x, '\\'))
      //.flatMap(x => x.split('/'))
      //.flatMap(x => x.split('\\'))
      .filter((x) => x.length > 0)
      .join('/')
  );
};

export const normalizePath = (path: string): string => {
  path = path.toLowerCase();
  path = trim(path, '/');
  return path;
};

export const pathEqual = (path1: string, path2: string): boolean => {
  path1 = path1 || '';
  path2 = path2 || '';
  return normalizePath(path1) === normalizePath(path2);
};

const access_token_key = 'access_token';

export const setAccessToken = (token: string) =>
  localStorage.setItem(access_token_key, token);

export const getAccessToken = (): string | undefined | null =>
  localStorage.getItem(access_token_key);

export const hasAccessToken = () => !isStringEmpty(getAccessToken());

const selected_store_key = 'sales-selected-store-id';

export const setSelectedStoreId = (storeId: string) => {
  localStorage.setItem(selected_store_key, storeId);
};
export const getSelectedStoreId = (): string | null | undefined => {
  return localStorage.getItem(selected_store_key);
};

export const getFileExtension = (
  filename: string,
): string | null | undefined => {
  let arr = filename?.split('.') || [];
  if (arr.length <= 0) {
    return undefined;
  }

  return arr.at(arr.length - 1);
};

export const isAlphabet = (str: string): boolean => {
  return !isStringEmpty(str) && /^[a-zA-Z]+$/.test(str);
};

export const isBase64 = (str: string): boolean => {
  return (
    !isStringEmpty(str) &&
    /^\s*data:(?:[a-z]+\/[a-z0-9-+.]+(?:;[a-z-]+=[a-z0-9-]+)?)?(?:;base64)?,([a-z0-9!$&',()*+;=\-._~:@/?%\s]*?)\s*$/i.test(
      str,
    )
  );
};

export function isWechatBrowser() {
  return /MicroMessenger/i.test(window.navigator.userAgent);
}

export const isMobile = (mobile: string): boolean => {
  return !isStringEmpty(mobile) && /^(?:(?:\+|00)86)?1[3-9]\d{9}$/.test(mobile);
};

export const findMobile = (text: string): string[] | undefined => {
  if (!text) {
    return undefined;
  }

  return text.match(/\b1\d{10}\b/g) || undefined;
};

export const isEmail = (email: string): boolean => {
  return (
    !isStringEmpty(email) &&
    /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(email)
  );
};

export const isIdentityCard = (idno: string): boolean => {
  return (
    !isStringEmpty(idno) &&
    /^\d{6}((((((19|20)\d{2})(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(((19|20)\d{2})(0[13578]|1[02])31)|((19|20)\d{2})02(0[1-9]|1\d|2[0-8])|((((19|20)([13579][26]|[2468][048]|0[48]))|(2000))0229))\d{3})|((((\d{2})(0[13-9]|1[012])(0[1-9]|[12]\d|30))|((\d{2})(0[13578]|1[02])31)|((\d{2})02(0[1-9]|1\d|2[0-8]))|(([13579][26]|[2468][048]|0[048])0229))\d{2}))(\d|X|x)$/.test(
      idno,
    )
  );
};

export const isUrl = (str: string): boolean => {
  return (
    !isStringEmpty(str) &&
    /^(((ht|f)tps?):\/\/)?[\w-]+(\.[\w-]+)+([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?$/.test(
      str,
    )
  );
};

export const isImageUrl = (str: string): boolean => {
  return (
    !isStringEmpty(str) &&
    /^https?:\/\/(.+\/)+.+(\.(gif|png|jpg|jpeg|webp|svg|psd|bmp|tif))$/i.test(
      str,
    )
  );
};

export const stringOrUndefined = (str?: string | null): string | undefined => {
  if (str && !isStringEmpty(str)) {
    return str;
  }
  return undefined;
};

export const firstNotEmpty = (
  data: (string | null | undefined)[],
): string | undefined => {
  const item = data.filter((x) => !isStringEmpty(x)).at(0);
  return item || undefined;
};

export const simpleString = (value: string, len: number): string => {
  if (value.length <= len) {
    return value;
  }
  return `${value.substring(0, len)}...`;
};

export const errorMsg = (msg: string): void => {
  if (GlobalContainer.message == undefined) {
    alert(msg);
    return;
  }
  GlobalContainer.message?.error(msg);
  //toast.error(msg);
};

export const successMsg = (msg: string): void => {
  if (GlobalContainer.message == undefined) {
    alert(msg);
    return;
  }
  GlobalContainer.message?.success(msg);
  //toast.success(msg);
};

export const formatMoney = (price: number): string => {
  return currency(price, {
    separator: ',',
    symbol: '￥',
    precision: 2,
  }).format();
};

export const formatNumber = (num: number) => {
  return num.toFixed(2);
};

export const serializeThenDeserialize = function <T>(data: T): T {
  return JSON.parse(JSON.stringify(data)) as T;
};

export const parseJsonOrEmpty = (
  str: string | null | undefined,
): any | null => {
  try {
    if (str) {
      return JSON.parse(str);
    }
  } catch (e) {
    console.log(e);
  }
  return null;
};

export const try_get_browser_geo_location =
  (): Promise<GeolocationPosition> => {
    return new Promise<GeolocationPosition>((resolve, reject) => {
      try {
        navigator.geolocation.getCurrentPosition(
          (location) => {
            resolve(location);
          },
          (e) => {
            reject(e);
          },
        );
      } catch (e) {
        reject(e);
      }
    });
  };
