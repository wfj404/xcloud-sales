﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.ThirdParty.Services.KuaiMai;

namespace XCloud.Sales.ThirdParty.Controllers.StoreManager;

[StoreAuditLog]
[Route("api/mall-manager/kuaimai")]
public class KuaiMaiController : ShopBaseController
{
    private readonly IStoreSettingService _storeSettingService;

    public KuaiMaiController(IStoreSettingService storeSettingService)
    {
        _storeSettingService = storeSettingService;
    }

    [HttpPost("get-settings")]
    public async Task<ApiResponse<KuaiMaiConfigDto>> GetSettingsAsync()
    {
        var storeManager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var settings = await this._storeSettingService.GetSettingsAsync<KuaiMaiConfigDto>(storeManager.StoreId);

        return new ApiResponse<KuaiMaiConfigDto>(settings);
    }

    [HttpPost("save-settings")]
    public async Task<ApiResponse<object>> SaveSettingsAsync([FromBody] KuaiMaiConfigDto dto)
    {
        var storeManager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeSettingService.SaveSettingsAsync(storeManager.StoreId, dto);

        return new ApiResponse<object>();
    }
}