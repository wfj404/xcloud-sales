﻿using XCloud.Application.Model;
using XCloud.Application.Utils;
using XCloud.Sales.Application.Domain.Mapping;
using XCloud.Sales.Application.Service.Coupons;

namespace XCloud.Sales.Application.Domain.Coupons;

[TypeIdentityName("coupon")]
public class Coupon : SalesBaseEntity<string>, IHasCreationTime, ISoftDelete, IStoreMappingSupported, IMayHaveTimeRange
{
    public string Name { get; set; }

    public string Description { get; set; }

    public int CouponType { get; set; } = CouponTypes.Normal;

    public decimal Price { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public string OrderConditionRulesJson { get; set; }

    public int? ExpiredInDays { get; set; }

    public int Amount { get; set; }

    public int? AccountIssuedLimitCount { get; set; }

    public bool LimitedToStore { get; set; }

    public DateTime CreationTime { get; set; }

    public bool IsActive { get; set; }

    public bool IsDeleted { get; set; }
}