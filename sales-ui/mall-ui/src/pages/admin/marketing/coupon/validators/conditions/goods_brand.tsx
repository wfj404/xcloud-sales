import { useRequest } from 'ahooks';
import { Button, Form, Modal, Select } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { GoodsBrandValidatorParameter } from '@/utils/swagger';

export default ({
  rule,
  ok,
}: {
  rule: GoodsBrandValidatorParameter;
  ok: (d: GoodsBrandValidatorParameter) => void;
}) => {
  const [settings, _settings] = useState<GoodsBrandValidatorParameter>({});
  const [show, _show] = useState(false);

  const queryBrand = useRequest(apiClient.mallAdmin.brandListBrand, {
    manual: true,
  });
  const dataList = queryBrand.data?.data?.Data || [];

  useEffect(() => {
    _settings({ ...rule });
  }, [rule]);

  useEffect(() => {
    if (show) {
      queryBrand.run({});
    }
  }, [show]);

  return (
    <>
      <Button
        block
        onClick={() => {
          _show(true);
        }}
      >{`限定品牌(${
        rule.SupportedBrands?.map((x) => x.Name).join(',') || '--'
      })`}</Button>

      <Modal
        title={'限定品牌可用'}
        open={show}
        onCancel={() => {
          _show(false);
        }}
        onOk={() => {
          ok({
            ...settings,
          });
          _show(false);
        }}
      >
        <Form>
          <Form.Item label={'品牌'}>
            <Select
              mode={'multiple'}
              maxTagCount={100}
              loading={queryBrand.loading}
              value={settings.SupportedBrands?.map((x) => x.Id)}
              onChange={(e) => {
                _settings({
                  ...settings,
                  SupportedBrands: dataList.filter((x) => e.indexOf(x.Id) >= 0),
                });
              }}
              options={dataList.map((x) => ({
                label: x.Name,
                value: x.Id || '',
              }))}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
