import { useRequest } from 'ahooks';
import { useEffect, useState } from 'react';
import { history, useSnapshot } from 'umi';

import XModal from '@/components/common/modal';
import XStore from '@/components/mobile/current_store/store';
import { storeState, StoreType } from '@/store';
import { DeliveryType, PaymentMethod, ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { config } from '@/utils/config';
import {
  PlaceOrderRequestDto,
  PlaceOrderRequestItemDto,
  ShoppingCartItemDto,
} from '@/utils/swagger';
import {
  handleResponse,
  isArrayEmpty,
  isStringEmpty,
  successMsg,
} from '@/utils/utils';
import { ArrowCircleRightOutlined, CheckOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Alert, Box, Button, Divider, Stack, Typography } from '@mui/material';

import XAddition from './addition';
import XCoupon from './coupon';
import XOrderItems from './goods';
import XPayment from './payment';
import XPrice from './price';
import XServiceTime from './service_time';
import XShipping from './shipping';

const default_place_order_request: PlaceOrderRequestDto = {
  ShippingMethodId: ShippingMethod.Delivery,
  DeliveryType: DeliveryType.Express,
  PaymentMethodId: PaymentMethod.Online,
};

export default ({
  selectedItems,
}: {
  selectedItems: ShoppingCartItemDto[];
}) => {
  const app = useSnapshot(storeState) as StoreType;

  const [open, _open] = useState(false);

  const [formdata, _formdata] = useState<PlaceOrderRequestDto>(
    default_place_order_request,
  );

  useEffect(() => {
    if (open) {
      _formdata({ ...default_place_order_request });
    }
  }, [open]);

  const items = selectedItems.map<PlaceOrderRequestItemDto>((x) => {
    const requestItem = formdata.Items?.find((d) => d.SkuId == x.SkuId);

    return {
      SkuId: x.SkuId,
      Quantity: x.Quantity,
      Remark: requestItem?.Remark || undefined,
      ValueAddedItemIds: requestItem?.ValueAddedItemIds || undefined,
    };
  });

  const prePlaceOrderRequest = useRequest(
    apiClient.mall.orderPrePlaceOrderWithoutSaving,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {});
      },
      onError: (e) => {
        console.log(e);
        prePlaceOrderRequest.mutate(undefined);
      },
    },
  );
  const prePlaceOrderDataHolder = prePlaceOrderRequest.data?.data?.Data || {};
  const prePlaceOrderErrors = [
    ...(prePlaceOrderDataHolder.SkuErrors || []),
    ...(prePlaceOrderDataHolder.CouponErrors || []),
    ...(prePlaceOrderDataHolder.PaymentErrors || []),
    ...(prePlaceOrderDataHolder.ShippingErrors || []),
    ...(prePlaceOrderDataHolder.ErrorList || []),
  ];

  const prePlaceOrderWithoutCouponRequest = useRequest(
    apiClient.mall.orderPrePlaceOrderWithoutSaving,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {});
      },
      onError: (e) => {
        console.log(e);
        prePlaceOrderWithoutCouponRequest.mutate(undefined);
      },
    },
  );

  const placeOrderImpl = async () => {
    if (isArrayEmpty(items)) {
      alert('请选择商品');
      return;
    }
    return await apiClient.mall.orderPlaceOrder({
      ...formdata,
      Items: items,
      PlatformId: 'H5',
    });
  };

  const placeOrderRequest = useRequest(placeOrderImpl, {
    manual: true,
    onSuccess: (res) => {
      if (!res) {
        return;
      }
      handleResponse(res, () => {
        successMsg('下单成功');
        console.log('下单成功', res.data);
        //goto order detail page
        setTimeout(() => {
          app.queryShoppingCartCount();
          //let detailUrl = `/order/detail?id=${order.Id}`;
          history.push({
            pathname: `/order`,
          });
        }, 1000);
      });
    },
  });

  const any_loading =
    placeOrderRequest.loading ||
    prePlaceOrderRequest.loading ||
    prePlaceOrderWithoutCouponRequest.loading;

  const renderErrors = (errors?: string[] | null) => {
    if (any_loading) {
      return null;
    }

    if (isArrayEmpty(errors)) {
      return null;
    }
    return (
      <Box
        sx={{
          width: '100%',
          p: 1,
          py: 0.5,
          border: (t) => `1px dashed ${t.palette.error.main}`,
        }}
      >
        {errors?.map((x, i) => {
          return <div key={i}>{`${i + 1}. ${x}`}</div>;
        })}
      </Box>
    );
  };

  useEffect(() => {
    if (!open) {
      return;
    }
    if (items.length <= 0) {
      return;
    }

    if (
      isStringEmpty(formdata.AddressId) &&
      formdata.ShippingMethodId == ShippingMethod.Delivery
    ) {
      return;
    }

    const request: PlaceOrderRequestDto = {
      ...formdata,
      Items: items,
      PlatformId: 'H5',
    };

    prePlaceOrderRequest.run(request);
    prePlaceOrderWithoutCouponRequest.run({
      ...request,
      UserCouponId: undefined,
    });
  }, [
    formdata.AddressId,
    formdata.DeliveryType,
    formdata.UserCouponId,
    formdata.ShippingMethodId,
    selectedItems,
    open,
  ]);

  if (app.mallSettings.OrderSettings?.PlaceOrderDisabled) {
    return <Alert>管理员关闭了下单功能</Alert>;
  }

  const renderForm = () => {
    return (
      <>
        <Stack
          spacing={1}
          direction="column"
          sx={{
            width: '100%',
            mb: 3,
            pt: 2,
          }}
        >
          {isStringEmpty(app.mallSettings.PlaceOrderNotice) || (
            <Box sx={{}}>
              <Alert>{app.mallSettings.PlaceOrderNotice}</Alert>
            </Box>
          )}
          <Stack
            direction="row"
            spacing={1}
            alignItems={'center'}
            justifyContent={'flex-start'}
            sx={{ pb: 2 }}
          >
            <Typography variant="caption" color="text.disabled">
              下单门店
            </Typography>
            <XStore />
          </Stack>
          <XShipping
            model={formdata}
            onChange={(e) => {
              _formdata({
                ...e,
              });
            }}
          />
          {renderErrors(prePlaceOrderDataHolder.ShippingErrors)}
          <Divider orientation="horizontal" />
          <XOrderItems
            loading={prePlaceOrderRequest.loading}
            holder={prePlaceOrderDataHolder}
            model={formdata}
            onChange={(e) => {
              console.log(e);
              _formdata({ ...e });
            }}
          />
          <XAddition model={formdata} onChange={(e) => _formdata({ ...e })} />
          <XCoupon
            order={prePlaceOrderWithoutCouponRequest.data?.data?.Data?.Order}
            value={formdata.UserCouponId}
            onSelected={(x) => {
              _formdata({
                ...formdata,
                UserCouponId: x?.Id || undefined,
              });
            }}
          />
          {renderErrors(prePlaceOrderDataHolder.CouponErrors)}
          <XServiceTime
            model={formdata}
            onChange={(e) => {
              _formdata({
                ...e,
              });
            }}
          />
          <XPayment
            model={formdata}
            onChange={(e) => {
              _formdata({
                ...e,
              });
            }}
          />
          {renderErrors(prePlaceOrderDataHolder.PaymentErrors)}
          <Divider orientation="horizontal" />
          <XPrice model={prePlaceOrderDataHolder} data={formdata} />
        </Stack>
      </>
    );
  };

  return (
    <>
      <Button
        color="warning"
        variant="contained"
        endIcon={<ArrowCircleRightOutlined />}
        disabled={isArrayEmpty(items)}
        onClick={() => {
          _open(true);
        }}
      >
        立即下单
      </Button>
      <XModal
        showCloseButton
        title={'购物车下单'}
        open={open}
        onClose={() => {
          _open(false);
        }}
        bodyStyle={{
          backgroundColor: config.color.gray,
        }}
      >
        {renderForm()}
        <Divider orientation="horizontal" />
        <Stack
          direction={'column'}
          alignItems={'center'}
          justifyContent={'center'}
          spacing={1}
          sx={{
            width: '100%',
            my: 1,
          }}
        >
          {prePlaceOrderErrors.length > 0 && (
            <Typography variant="caption" color="error">
              请先修正提交错误
            </Typography>
          )}
          {renderErrors(prePlaceOrderDataHolder.ErrorList)}
          <LoadingButton
            loading={any_loading}
            variant="contained"
            fullWidth
            color="warning"
            startIcon={<CheckOutlined />}
            disabled={prePlaceOrderErrors.length > 0}
            onClick={() => {
              placeOrderRequest.run();
            }}
          >
            确认下单
          </LoadingButton>
        </Stack>
      </XModal>
    </>
  );
};
