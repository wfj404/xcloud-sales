import { Descriptions } from 'antd';

import XRenderTime from '@/components/common/time/render_time';
import { PaymentChannelList } from '@/utils/biz';
import { PaymentBillDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';

import XRefundList from './refund_list';

export default ({ model }: { model: PaymentBillDto }) => {
  const renderPayment = () => {
    if (!model.Paid) {
      return null;
    }
    return (
      <Descriptions title="支付信息" bordered style={{ marginBottom: 10 }}>
        <Descriptions.Item label="交易单号">
          {model.PaymentTransactionId || '--'}
        </Descriptions.Item>
        <Descriptions.Item label="支付方式">
          {PaymentChannelList.find((x) => x.id == model.PaymentChannel)?.name ||
            '--'}
        </Descriptions.Item>
        <Descriptions.Item label="支付时间">
          {<XRenderTime timeStr={model.PayTime} />}
        </Descriptions.Item>
        <Descriptions.Item label="回调信息" span={2}>
          {model.NotifyData || '--'}
        </Descriptions.Item>
      </Descriptions>
    );
  };

  const renderRefund = () => {
    if (isArrayEmpty(model.RefundBills)) {
      return null;
    }
    return <XRefundList datalist={model.RefundBills || []} />;
  };

  return (
    <>
      {renderPayment()}
      {renderRefund()}
    </>
  );
};
