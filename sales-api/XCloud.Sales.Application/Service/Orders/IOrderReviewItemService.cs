﻿using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Json;
using XCloud.Platform.Application.Service.Storage;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderReviewItemService : ISalesAppService
{
    Task<OrderReviewItemDto[]> AttachOrderItemAsync(OrderReviewItemDto[] data);

    Task<OrderReviewItemDto[]> AttachMainReviewAsync(OrderReviewItemDto[] data);

    Task<OrderReviewItemDto[]> AttachPicturesAsync(OrderReviewItemDto[] data);

    Task<PagedResponse<OrderReviewItemDto>> QueryItemsPagingAsync(QueryOrderReviewItemPaging dto);
}

public class OrderReviewItemService : SalesAppService, IOrderReviewItemService
{
    private readonly ISalesRepository<OrderReviewItem> _repository;
    private readonly IStorageMetaService _storageMetaService;

    public OrderReviewItemService(ISalesRepository<OrderReviewItem> repository, IStorageMetaService storageMetaService)
    {
        _repository = repository;
        _storageMetaService = storageMetaService;
    }

    public async Task<OrderReviewItemDto[]> AttachPicturesAsync(OrderReviewItemDto[] data)
    {
        if (!data.Any())
            return data;

        foreach (var m in data)
        {
            m.MetaIds = this.JsonDataSerializer.DeserializeArrayFromStringOrEmpty<string>(m.PictureMetaIdsJson,
                this.Logger);
        }

        var ids = data.SelectMany(x => x.MetaIds).Distinct().ToArray();

        var datalist = await this._storageMetaService.ByIdsAsync(ids);

        foreach (var m in data)
        {
            m.StorageMeta = datalist.Where(x => m.MetaIds.Contains(x.Id)).ToArray();
        }

        return data;
    }

    public async Task<OrderReviewItemDto[]> AttachOrderItemAsync(OrderReviewItemDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.OrderItemId).WhereNotEmpty().Distinct().ToArray();

        var db = await this._repository.GetDbContextAsync();

        var datalist = await db.Set<OrderItem>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            m.OrderItem = datalist.FirstOrDefault(x => x.Id == m.OrderItemId)
                ?.MapTo<OrderItem, OrderItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<OrderReviewItemDto[]> AttachMainReviewAsync(OrderReviewItemDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await this._repository.GetDbContextAsync();

        var ids = data.Select(x => x.ReviewId).WhereNotEmpty().Distinct().ToArray();

        var datalist = await db.Set<OrderReview>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            m.OrderReview = datalist.FirstOrDefault(x => x.Id == m.ReviewId)
                ?.MapTo<OrderReview, OrderReviewDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<PagedResponse<OrderReviewItemDto>> QueryItemsPagingAsync(QueryOrderReviewItemPaging dto)
    {
        var db = await this._repository.GetDbContextAsync();

        var itemQuery = db.Set<OrderReviewItem>().AsNoTracking();

        var reviewQuery = db.Set<OrderReview>().AsNoTracking();

        reviewQuery = reviewQuery.WhereCreationTimeBetween(dto.StartTime, dto.EndTime);

        if (dto.IsApproved != null)
        {
            reviewQuery = reviewQuery.Where(x => x.Approved == dto.IsApproved.Value);
        }

        var query = from reviewItem in itemQuery
            join orderItem in db.Set<OrderItem>().AsNoTracking()
                on reviewItem.OrderItemId equals orderItem.Id
            join review in reviewQuery
                on reviewItem.ReviewId equals review.Id
            join sku in db.Set<Sku>().AsNoTracking()
                on orderItem.SkuId equals sku.Id
            join order in db.Set<Order>().AsNoTracking()
                on orderItem.OrderId equals order.Id
            select new { reviewItem, review, sku, order };

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
        {
            query = query.Where(x => x.order.StoreId == dto.StoreId);
        }

        if (!string.IsNullOrWhiteSpace(dto.GoodsId))
        {
            query = query.Where(x => x.sku.GoodsId == dto.GoodsId);
        }

        if (!string.IsNullOrWhiteSpace(dto.Keywords))
        {
            query = query.Where(x =>
                x.reviewItem.ReviewText.Contains(dto.Keywords) || x.review.ReviewText.Contains(dto.Keywords));
        }

        var count = await query.CountOrDefaultAsync(dto);

        var items = await query
            .OrderByDescending(x => x.review.CreationTime)
            .Select(x => x.reviewItem)
            .PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();


        var datalist = items.MapArrayTo<OrderReviewItem, OrderReviewItemDto>(ObjectMapper);

        return new PagedResponse<OrderReviewItemDto>(datalist, dto, count);
    }
}