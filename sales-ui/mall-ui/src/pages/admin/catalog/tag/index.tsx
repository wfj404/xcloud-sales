import { useRequest } from 'ahooks';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import DeleteWrapper from '@/components/common/delete_wrapper';
import { apiClient } from '@/utils/client';
import { TagDto } from '@/utils/swagger';

import XForm from './form';

export default () => {
  const [formData, _formData] = useState<TagDto | undefined>(undefined);

  const query_list_request = useRequest(apiClient.mallAdmin.tagListTags);
  const data = query_list_request.data?.data?.Data || [];

  const queryList = () => {
    query_list_request.run({});
  };

  const columns: ColumnProps<TagDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name || '--',
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      title: '提示',
      render: (d, x) => x.Alert || '--',
    },
    {
      title: '链接',
      render: (d, x) => x.Link || '--',
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <DeleteWrapper model={x}>
            <Button.Group>
              <Button
                type="link"
                onClick={() => {
                  _formData(x);
                }}
              >
                编辑
              </Button>
            </Button.Group>
          </DeleteWrapper>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title="标签"
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={query_list_request.loading}
          columns={columns}
          dataSource={data || []}
          pagination={false}
        />
      </Card>
    </>
  );
};
