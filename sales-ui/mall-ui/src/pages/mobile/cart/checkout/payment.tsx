import { List, Space } from 'antd-mobile';

import { PaymentMethod, ShippingMethod } from '@/utils/biz';
import { PlaceOrderRequestDto } from '@/utils/swagger';
import { CheckOutlined } from '@mui/icons-material';

export default ({
  model,
  onChange,
}: {
  model: PlaceOrderRequestDto;
  onChange: (e: PlaceOrderRequestDto) => void;
}) => {
  const online = model.PaymentMethodId == PaymentMethod.Online;
  const after_shipped = model.PaymentMethodId == PaymentMethod.Offline;

  const after_shipped_payment_allowed =
    model.ShippingMethodId == ShippingMethod.Pickup;

  const checked_button = <CheckOutlined color="success" fontSize="inherit" />;

  return (
    <>
      <Space direction="vertical" style={{ width: '100%' }}>
        <List
          mode="card"
          header="请选择支付方式"
          style={{
            margin: 0,
          }}
        >
          <List.Item
            arrow={false}
            extra={online && checked_button}
            onClick={() => {
              onChange({
                ...model,
                PaymentMethodId: PaymentMethod.Online,
              });
            }}
          >
            在线支付
          </List.Item>
          <List.Item
            disabled={!after_shipped_payment_allowed}
            arrow={false}
            extra={after_shipped && checked_button}
            description={
              after_shipped_payment_allowed || '当前配送方式不支持货到付款'
            }
            onClick={() => {
              onChange({
                ...model,
                PaymentMethodId: PaymentMethod.Offline,
              });
            }}
          >
            货到付款
          </List.Item>
        </List>
      </Space>
    </>
  );
};
