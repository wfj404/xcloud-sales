using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Service;
using XCloud.Platform.Application.Authentication.Extension;

namespace XCloud.Platform.Application.Authentication;

public interface ITokenService : IXCloudAppService
{
    Task<string> CreateTokenAsync(Claim[] claims);
    
    Task<ClaimsPrincipal> ParseTokenAsync(string token);
}

[ExposeServices(typeof(ITokenService))]
public class JwtTokenService : XCloudAppService, ITokenService
{
    public async Task<string> CreateTokenAsync(Claim[] claims)
    {
        await Task.CompletedTask;

        var option = Configuration.GetOAuthServerOption();

        if (string.IsNullOrWhiteSpace(option.JwtSecretKey))
            throw new ArgumentNullException(nameof(option.JwtSecretKey));

        if (string.IsNullOrWhiteSpace(option.Server))
            throw new ArgumentNullException(nameof(option.Server));

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(option.JwtSecretKey));
        var sign = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        var token = new JwtSecurityToken(
            issuer: option.Server,
            audience: option.Server,
            claims: claims,
            expires: Clock.Now.AddDays(30),
            signingCredentials: sign);

        var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
        return jwtToken;
    }

    public async Task<ClaimsPrincipal> ParseTokenAsync(string token)
    {
        await Task.CompletedTask;

        if (string.IsNullOrWhiteSpace(token))
            throw new ArgumentNullException(nameof(token));

        var option = Configuration.GetOAuthServerOption();

        if (string.IsNullOrWhiteSpace(option.JwtSecretKey))
            throw new ArgumentNullException(nameof(option.JwtSecretKey));

        if (string.IsNullOrWhiteSpace(option.Server))
            throw new ArgumentNullException(nameof(option.Server));

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(option.JwtSecretKey));

        var parameter = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = key,
            ValidIssuer = option.Server,
            ValidAudience = option.Server
        };

        var claimsPrincipal =
            new JwtSecurityTokenHandler().ValidateToken(token, parameter,
                out var securityToken);

        return claimsPrincipal;
    }
}