import { CascaderOption, CascaderView, Space } from 'antd-mobile';
import React from 'react';

import XModal from '@/components/common/modal';
import { convertToAntdData } from '@/utils/biz';
import { LocationOnOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Typography } from '@mui/material';
import { useCascaderAreaData } from '@vant/area-data';

export default function ({
  value,
  onChange,
  autoClose,
}: {
  value?: CascaderOption[];
  onChange: (e: (CascaderOption | undefined)[]) => void;
  autoClose?: boolean;
}) {
  const [open, _open] = React.useState(false);

  const vantAreaData = useCascaderAreaData();
  const provinceList = vantAreaData.map((x) => convertToAntdData(x));
  const cityList = provinceList.flatMap((x) => x.children || []);
  const areaList = cityList.flatMap((x) => x.children || []);

  const address = [...(value || [])]
    .map((x) => x.label)
    .filter((x) => x && x.length > 0);

  const selectedOk = address.length >= 3;

  return (
    <>
      <LoadingButton
        variant="outlined"
        color={selectedOk ? 'primary' : 'error'}
        onClick={() => _open(true)}
        startIcon={<LocationOnOutlined />}
      >
        <Space direction="horizontal">
          <div>{address.length > 0 ? `${address.join('-')}` : `选择地址`}</div>
          {selectedOk || (
            <Typography variant="caption" color="text.disabled">
              {`请完善地址到'省市区'`}
            </Typography>
          )}
        </Space>
      </LoadingButton>
      <XModal
        title={'选择地址'}
        open={open}
        onClose={() => {
          _open(false);
        }}
        actionRender={() => (
          <LoadingButton
            color="primary"
            onClick={() => {
              _open(false);
            }}
          >
            确定
          </LoadingButton>
        )}
      >
        <CascaderView
          style={{
            '--height': '300px',
            border: '1px dashed black',
          }}
          options={provinceList}
          value={value?.map((x) => x.value || '')}
          onChange={(e) => {
            const province = provinceList.find((x) => x.value == e.at(0));
            const city = cityList.find((x) => x.value == e.at(1));
            const area = areaList.find((x) => x.value == e.at(2));

            onChange && onChange([province, city, area]);

            if (area != undefined && autoClose) {
              _open(false);
            }
          }}
        />
      </XModal>
    </>
  );
}
