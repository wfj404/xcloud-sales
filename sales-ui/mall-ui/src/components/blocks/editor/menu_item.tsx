import { Space } from 'antd';

import { BlockMenuItem, menuToBlockItem } from '@/components/blocks/utils';
import { Box } from '@mui/material';

export default ({ x }: { x: BlockMenuItem }) => {
  return (
    <Box
      onClick={() => {
        const item = menuToBlockItem(x);
      }}
      sx={{
        p: 2,
        cursor: 'grab',
        backgroundColor: 'rgb(250,250,250)',
        '&:hover': {
          backgroundColor: 'rgb(240,240,240)',
        },
      }}
    >
      <Space direction={'vertical'}>
        {x.icon != undefined && <span>{x.icon}</span>}
        <span>{x.label || x.type}</span>
      </Space>
    </Box>
  );
};
