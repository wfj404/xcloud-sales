﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Gis;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores.Context;

namespace XCloud.Sales.Qingdao.Service.Stores;

[DisableConventionalRegistration]
public class QingdaoLocationBasedStoreSelectorContributor : SalesAppService, ICurrentStoreSelectorContributor
{
    private readonly GisClient _gisClient;
    private readonly Sh3HClient _sh3HClient;
    private readonly IQingdaoStoreService _qingdaoStoreService;

    public QingdaoLocationBasedStoreSelectorContributor(
        GisClient gisClient, Sh3HClient sh3HClient, IQingdaoStoreService qingdaoStoreService)
    {
        _gisClient = gisClient;
        _sh3HClient = sh3HClient;
        _qingdaoStoreService = qingdaoStoreService;
    }

    private async Task<WapOrganization> GetStoreFromLocationOrNullAsync(GeoLocationDto location)
    {
        var point = await this._gisClient.TransformCoordinatesToPipelineTypeAsync(location);

        if (point != null)
        {
            var authId = await this._gisClient.GetAuthCodeAsync();

            if (!string.IsNullOrWhiteSpace(authId))
            {
                var area = await this._gisClient.GetGisAreaAsync(authId, "GONGSHUI", point);

                if (area != null && !string.IsNullOrWhiteSpace(area.ECode))
                {
                    var org = await this._sh3HClient.ResolveStoreOrganizationByCodeWithCacheAsync(area.ECode);

                    return org;
                }
            }
        }

        return null;
    }

    public async Task<string> GetStoreIdOrEmptyAsync()
    {
        var location =
            this.WebHelper.RequiredHttpContext.GetClientGeoLocationOrNull(this.JsonDataSerializer, this.Logger);

        if (location != null && location.IsValid)
        {
            var org = await this.GetStoreFromLocationOrNullAsync(location);

            if (org != null)
            {
                var store = await this._qingdaoStoreService.PrepareStoreAsync(org);

                return store.Id;
            }
        }

        return default;
    }
}