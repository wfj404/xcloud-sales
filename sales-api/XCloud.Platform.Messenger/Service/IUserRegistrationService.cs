﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Core.Cache;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Messenger.Domain.Messenger;

namespace XCloud.Platform.Messenger.Service;

public interface IUserRegistrationService : IXCloudAppService
{
    Task PingAsync(OnlineStatusDto info);

    Task RemoveUserRegistrationAsync(string userId, string device);

    Task<ServerInstanceDto[]> GetUserServerInstancesAsync(string userId);

    Task DeleteExpiredAsync();
}

[ExposeServices(typeof(IUserRegistrationService))]
[UnitOfWork]
public class DatabaseUserRegistrationService : PlatformAppService, IUserRegistrationService
{
    private readonly IPlatformRepository<OnlineStatus> _repository;

    public DatabaseUserRegistrationService(IPlatformRepository<OnlineStatus> repository)
    {
        _repository = repository;
    }

    private int ExpiredMinutes => 3;

    public async Task PingAsync(OnlineStatusDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.UserId))
            throw new ArgumentNullException(nameof(dto.UserId));

        if (string.IsNullOrWhiteSpace(dto.DeviceId))
            throw new ArgumentNullException(nameof(dto.DeviceId));

        if (string.IsNullOrWhiteSpace(dto.ServerInstanceId))
            throw new ArgumentNullException(nameof(dto.ServerInstanceId));

        var key = $"im.user.reg.{dto.UserId}.{dto.DeviceId}.{dto.ServerInstanceId}";

        if (!await this.CacheProvider.TrySetEmptyStringAsync(key, TimeSpan.FromSeconds(30)))
        {
            return;
        }

        var entity = await _repository.FirstOrDefaultAsync(x =>
            x.UserId == dto.UserId &&
            x.DeviceId == dto.DeviceId &&
            x.ServerInstanceId == dto.ServerInstanceId);

        var now = this.Clock.Now;

        if (entity == null)
        {
            entity = ObjectMapper.Map<OnlineStatusDto, OnlineStatus>(dto);
            entity.Id = GuidGenerator.CreateGuidString();
            entity.PingTime = now;
            entity.CreationTime = now;

            await _repository.InsertAsync(entity);
        }
        else
        {
            entity.PingTime = now;

            await _repository.UpdateAsync(entity);
        }
    }

    public async Task<ServerInstanceDto[]> GetUserServerInstancesAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var expired = this.Clock.Now.AddMinutes(-this.ExpiredMinutes);

        var db = await _repository.GetDbContextAsync();

        var userRegistrationQuery = db.Set<OnlineStatus>().AsNoTracking();

        userRegistrationQuery = userRegistrationQuery.Where(x => x.PingTime >= expired);
        userRegistrationQuery = userRegistrationQuery.Where(x => x.UserId == userId);

        var query = from reg in userRegistrationQuery
            join server in db.Set<ServerInstance>().AsNoTracking()
                on reg.ServerInstanceId equals server.InstanceId
            select new { reg, server };

        var datalist = await query
            .OrderByDescending(x => x.reg.PingTime)
            .ToArrayAsync();

        return datalist.Select(x => x.server).MapArrayTo<ServerInstance, ServerInstanceDto>(this.ObjectMapper);
    }

    public async Task RemoveUserRegistrationAsync(string userId, string device)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        if (string.IsNullOrWhiteSpace(device))
            throw new ArgumentNullException(nameof(device));

        await _repository.DeleteAsync(x => x.UserId == userId && x.DeviceId == device);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();
    }

    public async Task DeleteExpiredAsync()
    {
        var expired = Clock.Now.AddMinutes(-this.ExpiredMinutes);

        await this._repository.DeleteAsync(x => x.PingTime < expired);
    }
}