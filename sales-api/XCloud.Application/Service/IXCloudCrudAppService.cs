using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Repository;

namespace XCloud.Application.Service;

public interface
    IXCloudCrudAppService<TEntity, TEntityDto, in TPagingRequest, in TKey> : IXCloudAppService
    where TEntity : class, IEntity<TKey>
    where TEntityDto : class, IEntityDto<TKey>
    where TPagingRequest : PagedRequest
{
    Task<int> CountAsync();

    Task<TEntityDto> GetByIdAsync(TKey id);

    Task<TEntityDto[]> GetByIdsAsync(TKey[] ids);

    Task<TEntity> InsertAsync(TEntityDto dto);

    Task<TEntity> UpdateAsync(TEntityDto dto);

    Task DeleteByIdAsync(TKey id);

    Task<PagedResponse<TEntityDto>> QueryPagingAsync(TPagingRequest dto);
}

public abstract class
    XCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, TKey> :
    XCloudAppService,
    IXCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, TKey>
    where TEntity : class, IEntity<TKey>
    where TEntityDto : class, IEntityDto<TKey>
    where TPagingRequest : PagedRequest
{
    protected XCloudCrudAppService(IEfRepository<TEntity> repository)
    {
        Repository = repository;
        LocalizationResource = null;
    }

    protected IEfRepository<TEntity> Repository { get; }

    public virtual async Task<int> CountAsync()
    {
        var query = await Repository.GetQueryableAsync();

        return await query.CountAsync();
    }

    public virtual async Task<TEntityDto> GetByIdAsync(TKey id)
    {
        if (IsEmptyKey(id))
            throw new ArgumentNullException(nameof(id));

        var query = await Repository.GetQueryableAsync();

        var entity = await query.FirstOrDefaultAsync(BuildPrimaryKeyEqualExpression(id));

        if (entity == null)
            return null;

        return entity.MapTo<TEntity, TEntityDto>(ObjectMapper);
    }

    public virtual async Task<TEntityDto[]> GetByIdsAsync(TKey[] ids)
    {
        if (ids == null)
            throw new ArgumentNullException(nameof(ids));

        if (!ids.Any())
            return Array.Empty<TEntityDto>();

        foreach (var id in ids)
        {
            if (IsEmptyKey(id))
                throw new ArgumentNullException(nameof(ids));
        }

        var query = await Repository.GetQueryableAsync();

        var datalist = await query.Where(x => ids.Contains(x.Id)).ToArrayAsync();

        return datalist.MapArrayTo<TEntity, TEntityDto>(ObjectMapper);
    }

    public virtual async Task DeleteByIdAsync(TKey id)
    {
        if (IsEmptyKey(id))
            throw new ArgumentNullException(nameof(id));

        var query = await Repository.GetQueryableAsync();

        query = query.AsTracking().IgnoreQueryFilters();

        var entity = await query.FirstOrDefaultAsync(BuildPrimaryKeyEqualExpression(id));

        if (entity == null)
            return;

        await Repository.DeleteAsync(entity);
    }

    public virtual async Task<TEntity> InsertAsync(TEntityDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (!IsEmptyKey(dto.Id))
            throw new ArgumentException($"{nameof(dto.Id)} should be empty");

        var entity = dto.MapTo<TEntityDto, TEntity>(ObjectMapper);

        await SetFieldsBeforeInsertAsync(entity);

        await Repository.InsertAsync(entity);

        return entity;
    }

    public virtual async Task<TEntity> UpdateAsync(TEntityDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (IsEmptyKey(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        var entity = await Repository.FirstOrDefaultAsync(BuildPrimaryKeyEqualExpression(dto.Id));

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        await SetFieldsBeforeUpdateAsync(entity, dto);

        await Repository.UpdateAsync(entity);

        return entity;
    }

    protected virtual bool IsEmptyKey(TKey id)
    {
        Logger.LogWarning(message: $"please override {nameof(IsEmptyKey)} to speed up your code");

        var keyType = typeof(TKey);

        if (keyType == typeof(string))
        {
            var notEmpty = id is string strId && !string.IsNullOrWhiteSpace(strId);
            return !notEmpty;
        }
        else if (keyType == typeof(int))
        {
            var notEmpty = id is int intId && intId > 0;
            return !notEmpty;
        }
        else if (keyType == typeof(long))
        {
            var notEmpty = id is long longId && longId > 0;
            return !notEmpty;
        }
        else if (keyType == typeof(double))
        {
            var notEmpty = id is double doubleId && doubleId > 0;
            return !notEmpty;
        }

        throw new NotSupportedException(nameof(IsEmptyKey));
    }

    protected virtual void TrySetPrimaryKey(TEntity entity)
    {
        var properties = typeof(TEntity).GetProperties();

        var primaryProperty = properties.FirstOrDefault(x => x.Name == nameof(IEntity<TKey>.Id) && x.CanWrite);

        if (primaryProperty != null)
        {
            if (primaryProperty.PropertyType == typeof(string))
                primaryProperty.SetValue(entity, GuidGenerator.CreateGuidString());
            else if (primaryProperty.PropertyType == typeof(int))
                primaryProperty.SetValue(entity, default(int));
            else if (primaryProperty.PropertyType == typeof(long))
                primaryProperty.SetValue(entity, default(long));
            else
                throw new NotSupportedException(nameof(primaryProperty));
        }
    }

    protected virtual void TrySetCreationTime(TEntity entity)
    {
        var properties = typeof(TEntity).GetProperties();

        var creationTimeProperty =
            properties.FirstOrDefault(x => x.Name == nameof(IHasCreationTime.CreationTime) && x.CanWrite);

        if (creationTimeProperty != null) creationTimeProperty.SetValue(entity, Clock.Now);
    }

    protected virtual async Task SetFieldsBeforeInsertAsync(TEntity entity)
    {
        Logger.LogWarning($"please override {nameof(SetFieldsBeforeInsertAsync)} to speed up your code");

        await Task.CompletedTask;

        TrySetPrimaryKey(entity);
        TrySetCreationTime(entity);
    }

    protected virtual Expression<Func<TEntity, bool>> BuildPrimaryKeyEqualExpression(TKey id)
    {
        Expression<Func<TEntity, bool>> expression = x => x.Id.Equals(id);

        return expression;
    }

    protected virtual async Task SetFieldsBeforeUpdateAsync(TEntity entity, TEntityDto dto)
    {
        Logger.LogWarning("you are using default entity fields modification method");

        await Task.CompletedTask;

        var ignoreProperty = new[]
        {
            nameof(IEntity<TKey>.Id),
            nameof(IHasCreationTime.CreationTime)
        };

        entity.SetEntityFields(dto, x => ignoreProperty.Contains(x.Name));

        if (entity is IHasModificationTime)
        {
            entity.SetPropertyValue(nameof(IHasModificationTime.LastModificationTime), Clock.Now);
        }
    }

    public virtual async Task<PagedResponse<TEntityDto>> QueryPagingAsync(TPagingRequest dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await Repository.GetDbContextAsync();

        var query = await GetPagingFilteredQueryableAsync(db, dto);

        var count = await query.CountOrDefaultAsync(dto);

        var orderedQuery = await GetPagingOrderedQueryableAsync(query, dto);

        var datalist = await orderedQuery.PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();

        var items = datalist.MapArrayTo<TEntity, TEntityDto>(ObjectMapper);

        return new PagedResponse<TEntityDto>(items, dto, count);
    }

    protected virtual async Task<IOrderedQueryable<TEntity>> GetPagingOrderedQueryableAsync(IQueryable<TEntity> query,
        TPagingRequest dto)
    {
        Logger.LogWarning($"please override {nameof(GetPagingOrderedQueryableAsync)} to speed up your code");

        await Task.CompletedTask;

        var entityType = typeof(TEntity);
        if (entityType.IsAssignableTo<IHasCreationTime>())
        {
            //x
            var parameterExpr = Expression.Parameter(entityType, "x");

            var creationTimeProperty = entityType.GetProperties()
                .FirstOrDefault(x => x.Name == nameof(IHasCreationTime.CreationTime));

            if (creationTimeProperty == null)
                throw new AbpException(nameof(creationTimeProperty));

            if (creationTimeProperty.PropertyType != typeof(DateTime))
                throw new AbpException("wrong creation time type");

            //x.Id
            var fieldSelectorExpr =
                Expression.Property(parameterExpr, creationTimeProperty);

            var expression =
                Expression.Lambda<Func<TEntity, DateTime>>(fieldSelectorExpr, parameterExpr);

            return query.OrderByDescending(expression).ThenByDescending(x => x.Id);
        }

        return query.OrderByDescending(x => x.Id);
    }

    protected virtual async Task<IQueryable<TEntity>> GetPagingFilteredQueryableAsync(DbContext db, TPagingRequest dto)
    {
        await Task.CompletedTask;
        return db.Set<TEntity>().AsNoTracking();
    }
}