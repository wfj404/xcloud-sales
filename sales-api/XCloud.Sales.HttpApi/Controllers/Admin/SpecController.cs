﻿using XCloud.Application.Extension;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/spec")]
public class SpecController : ShopBaseController
{
    private readonly ISpecService _goodsSpecService;
    private readonly ISpecValueService _specValueService;
    private readonly ISkuService _skuService;

    public SpecController(ISpecService goodsSpecService, ISpecValueService specValueService, ISkuService skuService)
    {
        _goodsSpecService = goodsSpecService;
        _specValueService = specValueService;
        _skuService = skuService;
    }

    [HttpPost("list")]
    public async Task<ApiResponse<SpecDto[]>> ListJson([FromBody] IdDto<string> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var specs = await _goodsSpecService.QuerySpecByGoodsIdAsync(dto.Id);

        specs = await _specValueService.AttachValuesAsync(specs);

        var values = specs.SelectMany(x => x.Values).ToArray();

        await this._specValueService.AttachSkusAsync(values);

        var skus = values.Select(x => x.AssociatedSku).WhereNotNull().ToArray();

        await this._skuService.AttachGoodsAsync(skus);

        return new ApiResponse<SpecDto[]>(specs);
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> Edit([FromBody] SpecDto model)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        if (model.IsEmptyId())
        {
            await _goodsSpecService.InsertAsync(model);
        }
        else
        {
            await _goodsSpecService.UpdateAsync(model);
        }

        return new ApiResponse<object>();
    }

    //delete
    [HttpPost("delete")]
    public virtual async Task<ApiResponse<object>> Delete([FromBody] IdDto<int> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        await _goodsSpecService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("save-value")]
    public virtual async Task<ApiResponse<object>> ValueEdit([FromBody] SpecValueDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        if (dto.IsEmptyId())
        {
            await _specValueService.InsertAsync(dto);
        }
        else
        {
            await _specValueService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete-value")]
    public virtual async Task<ApiResponse<object>> ValueDelete([FromBody] IdDto<int> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        await _specValueService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }
}