﻿using System;
using Volo.Abp;

namespace XCloud.Sales.Qingdao.Exceptions;

public class WapException : BusinessException
{
    public WapException()
    {
        //
    }

    public WapException(string message) : base(message: message)
    {
        //
    }

    public WapException(string message, Exception e) : base(message: message, innerException: e)
    {
        //
    }
}

public class McsException : BusinessException
{
    public McsException(string message) : base(message: message)
    {
        //
    }

    public McsException(string message, Exception e) : base(message: message, innerException: e)
    {
        //
    }
}