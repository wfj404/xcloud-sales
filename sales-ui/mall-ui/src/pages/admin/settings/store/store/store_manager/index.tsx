import { useRequest } from 'ahooks';
import { Badge, Button, message, Modal, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import UserAvatar from '@/components/manage/user/avatar';
import XUserSelector from '@/components/manage/user/user_selector';
import { apiClient } from '@/utils/client';
import { StoreManagerDto, UserDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XForm from './form';

export default ({ storeId, hide }: { storeId?: string; hide: () => void }) => {
  const queryManager = useRequest(apiClient.mallAdmin.storeQueryStoreManager, {
    manual: true,
  });
  const data = queryManager.data?.data.Data || [];
  const show = !isStringEmpty(storeId);

  const add_manager_request = useRequest(
    apiClient.mallAdmin.storeSaveStoreManager,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          message.success('添加成功');
          queryList();
        });
      },
    },
  );

  const [formData, _formData] = useState<StoreManagerDto | undefined>(
    undefined,
  );

  const addManager = (user: UserDto) => {
    add_manager_request.run({
      UserId: user.Id,
      StoreId: storeId,
      IsActive: true,
    });
  };

  const queryList = () => {
    queryManager.run({ Id: storeId });
  };

  const columns: ColumnType<StoreManagerDto>[] = [
    {
      render: (x: StoreManagerDto) => {
        return <UserAvatar model={x.User} />;
      },
    },
    {
      title: '名称',
      render: (x: StoreManagerDto) =>
        x.User?.NickName || x.User?.IdentityName || '--',
    },
    {
      title: '是否激活',
      render: (x: StoreManagerDto) =>
        x.IsActive ? (
          <Badge status={'success'} text={'激活'} />
        ) : (
          <Badge status={'error'} text={'未激活'} />
        ),
    },
    {
      title: '时间',
      render: (x: StoreManagerDto) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (x: StoreManagerDto) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(x);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    if (isStringEmpty(storeId)) {
      queryManager.mutate(undefined);
    } else {
      queryList();
    }
  }, [storeId]);

  return (
    <>
      <Modal
        title={'成员'}
        open={show}
        onCancel={() => {
          hide();
        }}
        onOk={() => {
          hide();
        }}
      >
        <XUserSelector
          loading={add_manager_request.loading}
          onFind={(e) => {
            addManager(e);
          }}
        />
        <XForm
          show={formData != undefined}
          hide={() => {
            _formData(undefined);
          }}
          data={formData || {}}
          ok={() => {
            _formData(undefined);
            queryList();
          }}
        />
        <Table
          rowKey={(x) => x.Id || ''}
          loading={queryManager.loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Modal>
    </>
  );
};
