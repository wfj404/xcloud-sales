namespace XCloud.Sales.Application.Domain.Orders;

public class OrderCouponRecord : SalesBaseEntity<string>
{
    public string OrderId { get; set; }
    
    public int UserCouponId { get; set; }
}