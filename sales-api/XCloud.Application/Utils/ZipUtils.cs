﻿using Volo.Abp.DependencyInjection;

namespace XCloud.Application.Utils;

/// <summary>
/// https://learn.microsoft.com/zh-cn/dotnet/standard/io/how-to-compress-and-extract-files
/// </summary>
[ExposeServices(typeof(ZipUtils))]
public class ZipUtils : ITransientDependency
{
    //
}