using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Discounts;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Catalog;

public interface IPriceCalculationService : ISalesAppService
{
    void CalculateAndUpdateOrderPrice(OrderDto order);

    Task<SkuDto[]> AttachPriceModelAsync(SkuDto[] data, string storeId, int? storeUserId);
}

public class PriceCalculationService : SalesAppService, IPriceCalculationService
{
    private readonly ISalesRepository<StoreGoodsMapping> _repository;
    private readonly IGradeService _userGradeService;
    private readonly IGradeGoodsPriceService _goodsPriceService;
    private readonly IDiscountMappingService _discountMappingService;

    public PriceCalculationService(ISalesRepository<StoreGoodsMapping> repository,
        IGradeService userGradeService,
        IGradeGoodsPriceService goodsPriceService,
        IDiscountMappingService discountMappingService)
    {
        _repository = repository;
        _userGradeService = userGradeService;
        _goodsPriceService = goodsPriceService;
        _discountMappingService = discountMappingService;
    }

    private decimal CalculatePrice(PriceCalculateInput dto)
    {
        var price = dto.BasePrice;

        if (dto.DiscountRate is > 0 and < 1)
        {
            price *= (decimal)dto.DiscountRate;
        }

        price += dto.GradePriceOffset;
        price += dto.ValueAddedPriceOffset;

        return Math.Max(price, Decimal.Zero);
    }

    private void CalculateOrderItemPriceAsync(OrderItem item)
    {
        item.UnitPrice = CalculatePrice(new PriceCalculateInput(item));
        item.TotalPrice = item.UnitPrice * item.Quantity;
    }

    public void CalculateAndUpdateOrderPrice(OrderDto order)
    {
        foreach (var item in order.Items)
        {
            this.CalculateOrderItemPriceAsync(item);
        }

        order.ItemsTotalPrice = order.Items.Sum(x => x.TotalPrice);
        //total
        order.TotalPrice = order.ItemsTotalPrice;
        order.TotalPrice += order.ShippingFee;
        order.TotalPrice += order.PackageFee;
        order.TotalPrice += order.PromotionPriceOffset;
        order.TotalPrice += order.SellerPriceOffset;
        order.TotalPrice -= order.CouponPrice;
    }

    public async Task<SkuDto[]> AttachPriceModelAsync(SkuDto[] data, string storeId, int? storeUserId)
    {
        if (!data.Any())
            return data;

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        await AttachStorePriceOrDefaultAsync(data, storeId);

        await _discountMappingService.AttachDiscountModelAsync(data, storeId);

        await TryAttachGradeOffsetAsync(data, storeUserId);

        foreach (var m in data)
        {
            m.PriceModel.FinalPrice = CalculatePrice(new PriceCalculateInput(m.PriceModel));
            m.PriceModel.Empty = false;
        }

        return data;
    }

    private async Task TryAttachGradeOffsetAsync(SkuDto[] skus, int? storeUserId)
    {
        if (storeUserId == null || storeUserId.Value <= 0)
            return;

        var grade = await _userGradeService.GetGradeByUserIdAsync(storeUserId.Value);

        if (grade == null)
            return;

        await _goodsPriceService.AttachGradePriceModelAsync(skus, grade.Id);
        
        foreach (var m in skus)
        {
            m.PriceModel.GradePriceModel ??= new GoodsGradePriceDto();
            m.PriceModel.GradePriceModel.Grade = grade;
        }
    }

    private async Task AttachStorePriceOrDefaultAsync(SkuDto[] data, string storeId)
    {
        if (!data.Any())
            return;

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var query = db.Set<StoreGoodsMapping>().AsNoTracking()
            .Where(x => x.StoreId == storeId);

        var mappings = await query
            .Where(x => ids.Contains(x.SkuId))
            .OrderByDescending(x => x.CreationTime)
            .ToArrayAsync();

        foreach (var m in data)
        {
            var storePrice = mappings.FirstOrDefault(x => x.SkuId == m.Id);

            if (storePrice is { OverridePrice: true })
            {
                m.PriceModel = new PriceModel
                {
                    BasePrice = storePrice.Price,
                    ExtraProperties =
                    {
                        ["source"] = "store-price"
                    },
                };
            }
            else
            {
                m.PriceModel = new PriceModel
                {
                    BasePrice = m.Price,
                    ExtraProperties =
                    {
                        ["source"] = "goods-base-price"
                    },
                };
            }
        }
    }
}