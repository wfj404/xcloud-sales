﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Threading;

namespace XCloud.AspNetMvc.ModelBinder;

public class CancellationTokenBinder : IModelBinder
{
    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        await Task.CompletedTask;

        var tokenProvider = bindingContext.HttpContext.RequestServices.GetService<ICancellationTokenProvider>();

        if (tokenProvider != null)
        {
            bindingContext.Result = ModelBindingResult.Success(tokenProvider.Token);
        }
        else
        {
            var token = bindingContext.HttpContext.RequestAborted;

            bindingContext.Result = ModelBindingResult.Success(token);
        }
    }
}