import { Badge } from 'antd';

import { OrderStatus, OrderStatusList } from '@/utils/biz';
import { OrderDto } from '@/utils/swagger';

export default function (props: { model: OrderDto }) {
  const { model } = props;

  const name =
    OrderStatusList.find((x) => x.id == model.OrderStatusId)?.name || '--';

  if (model.OrderStatusId == OrderStatus.Processing) {
    return <Badge text={name} status={'processing'} />;
  }

  if (model.OrderStatusId == OrderStatus.Closed) {
    return <Badge text={name} status={'error'} />;
  }

  if (model.OrderStatusId == OrderStatus.Finished) {
    return <Badge text={name} status={'success'} />;
  }

  if (model.OrderStatusId == OrderStatus.AfterSales) {
    return <Badge text={name} status={'warning'} />;
  }

  return <Badge text={name} status={'default'} />;
}
