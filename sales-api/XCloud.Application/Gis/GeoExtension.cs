﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using XCloud.Core.Json;

namespace XCloud.Application.Gis;

public static class GeoExtension
{
    [CanBeNull]
    public static GeoLocationDto GetClientGeoLocationOrNull(this HttpContext httpContext,
        IJsonDataSerializer jsonDataSerializer,
        ILogger logger)
    {
        if (httpContext.Request.Headers.TryGetValue("x-location", out var value))
        {
            var location =
                jsonDataSerializer.DeserializeFromStringOrDefault<GeoLocationDto>(value,
                    logger: logger);

            if (location != null && location.IsValid)
            {
                return location;
            }
        }

        return null;
    }
}