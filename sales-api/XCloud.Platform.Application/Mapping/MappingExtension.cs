using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Application.Mapping;

public static class MappingExtension
{
    public static void PreConfigIdentityNameEntity<T>(this EntityTypeBuilder<T> builder)
        where T : class, IHasIdentityNameFields
    {
        builder.Property(x => x.OriginIdentityName).HasMaxLength(100);

        builder.Property(x => x.IdentityName).IsRequired().HasMaxLength(100);
        builder.HasIndex(x => x.IdentityName).IsUnique();
    }

    public static void PreConfigExternalBinding<T>(this EntityTypeBuilder<T> builder)
        where T : class, IHasExternalBinding
    {
        builder.Property(x => x.ExternalId).HasMaxLength(100);
    }
}