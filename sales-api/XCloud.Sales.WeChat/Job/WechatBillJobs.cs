﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Uow;
using XCloud.Application.Service;
using XCloud.Sales.WeChat.Service.Payments;

namespace XCloud.Sales.WeChat.Job;

[UnitOfWork]
public class WechatBillJobs : XCloudAppService
{
    private readonly IServiceProvider _serviceProvider;

    public WechatBillJobs(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task VerifyPaymentBillsAsync()
    {
        using var s = this._serviceProvider.CreateScope();

        var beijingDate = this.Clock.Now.AddHours(8).Date.AddDays(-1);

        var verifyService = s.ServiceProvider.GetRequiredService<WechatBillVerifyService>();

        await verifyService.VerifyPaymentBillsAsync(beijingDate);
    }
}