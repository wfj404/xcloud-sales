﻿using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Common;

namespace XCloud.Platform.Application.Domain.Common;

public class Area : PlatformBaseEntity, IHasCreationTime, IHasParent
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string MetaId { get; set; }

    public string AreaType { get; set; } = AreaTypeEnum.Area;

    public string NodePathJson { get; set; }

    public string ParentId { get; set; } = string.Empty;
    public DateTime CreationTime { get; set; }
}