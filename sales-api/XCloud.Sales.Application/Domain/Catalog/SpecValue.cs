namespace XCloud.Sales.Application.Domain.Catalog;

public class SpecValue : SalesBaseEntity, ISoftDelete
{
    public int SpecId { get; set; }

    public string Name { get; set; }

    public decimal PriceOffset { get; set; }

    public string AssociatedSkuId { get; set; }

    public int AssociatedSkuCount { get; set; }

    public bool IsDeleted { get; set; }
}