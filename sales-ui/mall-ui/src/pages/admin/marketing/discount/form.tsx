import {
  Button,
  Checkbox,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Modal,
  Space,
  Spin,
  Switch,
} from 'antd';
import { useEffect, useState } from 'react';
import { useSnapshot } from 'umi';

import XDeleteButton from '@/components/common/delete_button';
import MultipleSkuSelector from '@/components/goods/sku/mutiple_sku_selector';
import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';
import { dateTimeFormat, parseAsDayjs, timezoneOffset } from '@/utils/dayjs';
import { DiscountDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default (props: {
  show: boolean;
  hide: () => void;
  data: DiscountDto;
  ok: () => void;
}) => {
  const { show, hide, data, ok } = props;
  const [loading, _loading] = useState(false);
  const [model, _model] = useState<DiscountDto>({});

  const app = useSnapshot(storeState) as StoreType;

  const save = (row: DiscountDto) => {
    _loading(true);

    apiClient.mallAdmin
      .discountSave({ ...row })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...(data || {}),
    });
  }, [data]);

  useEffect(() => {
    show && app.queryStores();
  }, [show]);

  return (
    <>
      <Modal
        width={'60%'}
        title={'折扣'}
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                let res = await apiClient.mallAdmin.discountDelete({
                  Id: data.Id,
                });
                handleResponse(res, () => {
                  hide();
                  ok();
                });
              }}
              hide={isStringEmpty(data.Id)}
            />
            <Button
              type={'primary'}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="名称" tooltip="61儿童节">
              <Input
                maxLength={30}
                required
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="描述">
              <Input.TextArea
                maxLength={30}
                required
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="折扣">
              <InputNumber
                min={0}
                max={1}
                required
                value={model.DiscountPercentage || 0}
                onChange={(e) => {
                  _model({
                    ...model,
                    DiscountPercentage: e || 0,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="指定门店可用">
              <Space direction="horizontal">
                <Switch
                  checked={model.LimitedToStore}
                  onChange={(e) => {
                    _model({
                      ...model,
                      LimitedToStore: e,
                    });
                  }}
                />

                {model.LimitedToStore || <b>当前所有门店可用</b>}
              </Space>
            </Form.Item>

            {model.LimitedToStore && (
              <Form.Item label={'适用门店'}>
                <Checkbox.Group
                  onChange={(e) => {
                    _model({
                      ...model,
                      Stores: app.stores.filter(
                        (x) => e.indexOf(x.Id || '') >= 0,
                      ),
                    });
                  }}
                  value={model.Stores?.map((x) => x.Id || '') || []}
                  options={app.stores.map((x) => ({
                    label: x.Name,
                    value: x.Id || '',
                  }))}
                ></Checkbox.Group>
              </Form.Item>
            )}

            <Form.Item label={'特定商品'}>
              <Switch
                checked={model.LimitedToSku}
                onChange={(e) => {
                  _model({
                    ...model,
                    LimitedToSku: e,
                  });
                }}
              />
            </Form.Item>

            {model.LimitedToSku && (
              <Form.Item label={'参与商品'}>
                <MultipleSkuSelector
                  sku_list={model.Skus || []}
                  onChange={(e) => {
                    _model({
                      ...model,
                      Skus: e || [],
                    });
                  }}
                />
              </Form.Item>
            )}

            <Form.Item label="时间范围">
              <DatePicker.RangePicker
                allowClear
                value={[
                  parseAsDayjs(model.StartTime)?.add(timezoneOffset, 'hour') ||
                    null,
                  parseAsDayjs(model.EndTime)?.add(timezoneOffset, 'hour') ||
                    null,
                ]}
                onChange={(e) => {
                  _model({
                    ...model,
                    StartTime: e
                      ?.at(0)
                      ?.add(-timezoneOffset, 'hours')
                      .format(dateTimeFormat),
                    EndTime: e
                      ?.at(1)
                      ?.add(-timezoneOffset, 'hours')
                      .format(dateTimeFormat),
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="状态可用">
              <Switch
                checked={model.IsActive}
                onChange={(e) => {
                  _model({
                    ...model,
                    IsActive: e,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="排序">
              <InputNumber
                value={model.Sort || 0}
                onChange={(e) => {
                  _model({
                    ...model,
                    Sort: e || 0,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
