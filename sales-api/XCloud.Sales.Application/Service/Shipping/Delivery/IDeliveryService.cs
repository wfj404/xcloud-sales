using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Exceptions;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.Application.Service.Shipping.Delivery;

public interface
    IDeliveryService : ISalesStringCrudAppService<DeliveryRecord, DeliveryRecordDto, QueryDeliveryRecordPagingInput>
{
    Task TryUpdateOrderDeliveryStatusAsync(string orderId);

    Task MarkAsDeliveringAsync(DeliveryRecordDto dto);

    Task MarkAsDeliveredAsync(IdDto dto);

    Task<DeliveryRecordItem[]> GetOrderRestItemsToShipAsync(string orderId);

    Task<DeliveryRecordItemDto[]> AttachOrderItemsAsync(DeliveryRecordItemDto[] data);

    Task<DeliveryRecordDto[]> AttachOrdersAsync(DeliveryRecordDto[] data);

    Task<DeliveryRecordItem[]> GetOrderDeliveryItemsAsync(string orderId);

    Task<DeliveryRecordDto[]> AttachItemsAsync(DeliveryRecordDto[] data);

    Task<DeliveryRecord> CreateDeliveryAsync(DeliveryRecordDto dto);

    Task<DeliveryRecordDto> EnsureStoreDeliveryAsync(string deliveryId, string storeId);

    Task<DeliveryRecordDto> EnsureUserDeliveryAsync(string deliveryId, int userId);

    Task<DeliveryRecordDto[]> QueryByOrderIdAsync(string orderId);
}

public class DeliveryService :
    SalesStringCrudAppService<DeliveryRecord, DeliveryRecordDto, QueryDeliveryRecordPagingInput>,
    IDeliveryService
{
    private readonly ISalesRepository<DeliveryRecord> _repository;
    private readonly ISalesRepository<DeliveryRecordItem> _deliveryItemRepository;
    private readonly IOrderService _orderService;
    private readonly IOrderShippingProcessService _orderShippingProcessService;

    public DeliveryService(ISalesRepository<DeliveryRecord> repository,
        IOrderService orderService,
        ISalesRepository<DeliveryRecordItem> deliveryItemRepository,
        IOrderShippingProcessService orderShippingProcessService) : base(repository)
    {
        _repository = repository;
        _orderService = orderService;
        _deliveryItemRepository = deliveryItemRepository;
        _orderShippingProcessService = orderShippingProcessService;
    }

    public async Task TryUpdateOrderDeliveryStatusAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            return;

        var db = await _repository.GetDbContextAsync();

        var order = await db.Set<Order>().FirstOrDefaultAsync(x => x.Id == orderId);

        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        if (!order.IsDelivery())
            return;

        var deliveryList = await QueryByOrderIdAsync(orderId);
        var rest = await GetOrderRestItemsToShipAsync(orderId);

        order.ShippingStatusId = SalesConstants.ShippingStatus.NotShipping;
        if (rest.Any())
        {
            if (deliveryList.Any(x => x.Delivering))
            {
                await this._orderShippingProcessService.MarkAsShippingAsync(order.Id);
            }
        }
        else
        {
            if (deliveryList.All(x => x.Delivered))
            {
                await this._orderShippingProcessService.MarkAsShippedAsync(order.Id);
            }
            else if (deliveryList.Any(x => x.Delivering))
            {
                await this._orderShippingProcessService.MarkAsShippingAsync(order.Id);
            }
        }
    }

    public async Task MarkAsDeliveringAsync(DeliveryRecordDto dto)
    {
        var entity = await _repository.GetRequiredByIdAsync(dto.Id);

        if (entity.Delivering)
            return;

        await this._orderService.EnsureOrderIsToServiceAsync(entity.OrderId);

        entity.Delivering = true;
        entity.DeliveringTime = Clock.Now;

        entity.ExpressName = dto.ExpressName;
        entity.TrackingNumber = dto.TrackingNumber;

        await _repository.UpdateAsync(entity);
    }

    public async Task MarkAsDeliveredAsync(IdDto dto)
    {
        var entity = await _repository.GetRequiredByIdAsync(dto.Id);

        if (entity.Delivered)
            return;

        var order = await this._orderService.EnsureOrderIsToServiceAsync(entity.OrderId);

        if (!order.IsPaid())
        {
            throw new UserFriendlyException(message: this.L["order.payment.is.required"]);
        }

        entity.Delivered = true;
        entity.DeliveredTime = Clock.Now;

        await _repository.UpdateAsync(entity);
    }

    public async Task<DeliveryRecordItem[]> GetOrderDeliveryItemsAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var db = await _repository.GetDbContextAsync();

        var query = from item in db.Set<DeliveryRecordItem>().AsNoTracking()
            join orderItem in db.Set<OrderItem>().AsNoTracking()
                on item.OrderItemId equals orderItem.Id
            select new { item, orderItem };

        query = query.Where(x => x.orderItem.OrderId == orderId);

        var data = await query
            .OrderByDescending(x => x.item.Id)
            .Select(x => x.item)
            .ToArrayAsync();

        return data;
    }

    public async Task<DeliveryRecordDto[]> AttachOrdersAsync(DeliveryRecordDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.OrderId).WhereNotEmpty().Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var datalist = await db.Set<Order>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            m.Order = datalist.FirstOrDefault(x => x.Id == m.OrderId)?
                .MapTo<Order, OrderDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<DeliveryRecordDto[]> AttachItemsAsync(DeliveryRecordDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var datalist = await db.Set<DeliveryRecordItem>().AsNoTracking()
            .Where(x => ids.Contains(x.DeliveryId))
            .ToArrayAsync();

        foreach (var m in data)
        {
            m.Items = datalist.Where(x => x.DeliveryId == m.Id)
                .MapArrayTo<DeliveryRecordItem, DeliveryRecordItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<DeliveryRecordItemDto[]> AttachOrderItemsAsync(DeliveryRecordItemDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.OrderItemId).WhereNotEmpty().Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var datalist = await db.Set<OrderItem>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

        foreach (var m in data)
        {
            m.OrderItem = datalist.FirstOrDefault(x => x.Id == m.OrderItemId)?
                .MapTo<OrderItem, OrderItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<DeliveryRecordDto> EnsureStoreDeliveryAsync(string deliveryId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(deliveryId))
            throw new ArgumentNullException(nameof(deliveryId));

        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var db = await _repository.GetDbContextAsync();

        var query = from delivery in db.Set<DeliveryRecord>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on delivery.OrderId equals order.Id
            select new { delivery, order };

        var data = await query.FirstOrDefaultAsync(x => x.delivery.Id == deliveryId);

        if (data == null)
            throw new EntityNotFoundException(nameof(EnsureStoreDeliveryAsync));

        if (data.order.StoreId != storeId)
            throw new PermissionRequiredException(nameof(EnsureStoreDeliveryAsync));

        return data.delivery.MapTo<DeliveryRecord, DeliveryRecordDto>(ObjectMapper);
    }

    public async Task<DeliveryRecordDto> EnsureUserDeliveryAsync(string deliveryId, int userId)
    {
        if (string.IsNullOrWhiteSpace(deliveryId))
            throw new ArgumentNullException(nameof(deliveryId));

        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        var db = await _repository.GetDbContextAsync();

        var query = from delivery in db.Set<DeliveryRecord>().AsNoTracking()
            join order in db.Set<Order>().AsNoTracking()
                on delivery.OrderId equals order.Id
            select new { delivery, order };

        var data = await query.FirstOrDefaultAsync(x => x.delivery.Id == deliveryId);

        if (data == null)
            throw new EntityNotFoundException(nameof(EnsureStoreDeliveryAsync));

        if (data.order.UserId != userId)
            throw new PermissionRequiredException(nameof(EnsureStoreDeliveryAsync));

        return data.delivery.MapTo<DeliveryRecord, DeliveryRecordDto>(ObjectMapper);
    }

    protected override async Task SetFieldsBeforeInsertAsync(DeliveryRecord entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
        entity.DeliveredTime = entity.CreationTime;

        entity.Delivered = false;
        entity.Delivering = false;
        entity.DeliveredTime = default;
        entity.DeliveringTime = default;
    }

    public async Task<DeliveryRecordItem[]> GetOrderRestItemsToShipAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var order = await _orderService.GetByIdAsync(orderId);
        if (order == null)
            throw new EntityNotFoundException(nameof(order));

        await _orderService.AttachOrderItemsAsync(new[] { order });

        var deliveryItems = await GetOrderDeliveryItemsAsync(order.Id);

        var items = new List<DeliveryRecordItem>();

        foreach (var m in order.Items)
        {
            var shippedQuantity = deliveryItems.Where(x => x.OrderItemId == m.Id).Sum(x => x.Quantity);
            if (shippedQuantity >= m.Quantity)
                continue;

            items.Add(new DeliveryRecordItem
            {
                DeliveryId = default,
                OrderItemId = m.Id,
                Quantity = m.Quantity - shippedQuantity
            });
        }

        return items.ToArray();
    }

    public async Task<DeliveryRecord> CreateDeliveryAsync(DeliveryRecordDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.OrderId))
            throw new ArgumentNullException(nameof(dto.OrderId));

        if (string.IsNullOrWhiteSpace(dto.DeliveryType))
            throw new ArgumentNullException(nameof(dto.DeliveryType));

        dto.Items ??= Array.Empty<DeliveryRecordItemDto>();
        if (dto.Items.GroupBy(x => x.OrderItemId).Any(x => x.Count() > 1))
            throw new ArgumentException(nameof(dto.Items));

        var db = await _repository.GetDbContextAsync();

        await CheckOrderDeliveryAsync(db, dto);

        var entity = dto.MapTo<DeliveryRecordDto, DeliveryRecord>(ObjectMapper);

        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
        entity.DeliveredTime = entity.CreationTime;

        entity.Delivered = false;
        entity.Delivering = false;
        entity.DeliveredTime = default;
        entity.DeliveringTime = default;

        var deliveryItems = await GetOrderRestItemsToShipAsync(entity.OrderId);

        foreach (var item in dto.Items)
        {
            if (item.Quantity <= 0)
                throw new ArgumentNullException(nameof(item.Quantity));

            var restTotal = deliveryItems.FirstOrDefault(x => x.OrderItemId == item.OrderItemId)?.Quantity ?? default;

            if (item.Quantity > restTotal)
                throw new UserFriendlyException("over shipped");

            var m = item.MapTo<DeliveryRecordItemDto, DeliveryRecordItem>(ObjectMapper);
            m.Id = GuidGenerator.CreateGuidString();
            m.DeliveryId = entity.Id;

            await _deliveryItemRepository.InsertAsync(m);
        }

        await _repository.InsertAsync(entity);

        return entity;
    }

    public override Task<DeliveryRecord> InsertAsync(DeliveryRecordDto dto)
    {
        throw new NotSupportedException();
    }

    public override Task<DeliveryRecord> UpdateAsync(DeliveryRecordDto dto)
    {
        throw new NotSupportedException();
    }

    private async Task CheckOrderDeliveryAsync(DbContext db, DeliveryRecordDto recordDto)
    {
        var query = db.Set<DeliveryRecord>().AsNoTracking();
        if (await query.AnyAsync(x => x.OrderId == recordDto.OrderId))
            throw new UserFriendlyException("delivery already exist");
    }

    public async Task<DeliveryRecordDto[]> QueryByOrderIdAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var db = await _repository.GetDbContextAsync();

        var list = await db.Set<DeliveryRecord>().AsNoTracking()
            .Where(x => x.OrderId == orderId)
            .OrderByDescending(x => x.CreationTime)
            .TakeUpTo5000().ToArrayAsync();

        var response = list.Select(x => ObjectMapper.Map<DeliveryRecord, DeliveryRecordDto>(x)).ToArray();

        return response;
    }

    public override async Task<PagedResponse<DeliveryRecordDto>> QueryPagingAsync(QueryDeliveryRecordPagingInput dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var db = await _repository.GetDbContextAsync();

        var orderQuery = db.Set<Order>().AsNoTracking();

        if (dto.OrderToService ?? false)
        {
            orderQuery = orderQuery.WhereToService();
        }

        if (!string.IsNullOrWhiteSpace(dto.OrderId))
            orderQuery = orderQuery.Where(x => x.Id == dto.OrderId);

        if (!string.IsNullOrWhiteSpace(dto.StoreId))
            orderQuery = orderQuery.Where(x => x.StoreId == dto.StoreId);

        var query = db.Set<DeliveryRecord>().AsNoTracking().Where(x => orderQuery.Any(d => d.Id == x.OrderId));

        var count = await query.CountOrDefaultAsync(dto);

        var datalist = await query.OrderByDescending(x => x.CreationTime).PageBy(dto.ToAbpPagedRequest())
            .ToArrayAsync();

        var response = datalist.MapArrayTo<DeliveryRecord, DeliveryRecordDto>(ObjectMapper);

        return new PagedResponse<DeliveryRecordDto>(response, dto, count);
    }
}