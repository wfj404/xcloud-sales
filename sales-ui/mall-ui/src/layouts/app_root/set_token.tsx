import { parse } from 'query-string';
import { ReactElement, useEffect, useState } from 'react';
import { useLocation } from 'umi';

import { updateQueryString } from '@/utils/biz';
import { config } from '@/utils/config';
import { firstNotEmpty, isStringEmpty, setAccessToken } from '@/utils/utils';

export default ({ children }: { children: ReactElement }) => {
  const location = useLocation();
  const [finished, setFinished] = useState(false);

  const external_token_key =
    firstNotEmpty([config.external_token_key]) || 'external_token';

  const handleExternalToken = () => {
    const q = parse(location.search);
    const token = q[external_token_key] as string;

    if (isStringEmpty(token)) {
      setFinished(true);
    } else {
      setAccessToken(token);
      //successMsg('使用外部token');
      setTimeout(() => {
        setFinished(true);
      }, 200);

      //remove external token from url
      q[external_token_key] = null;

      updateQueryString({ ...q, use_external_token: true });
    }
  };

  useEffect(() => {
    handleExternalToken();
  }, []);

  if (!finished) {
    return null;
  }

  return <>{children}</>;
};
