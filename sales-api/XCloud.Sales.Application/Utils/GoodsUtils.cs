using JetBrains.Annotations;

namespace XCloud.Sales.Application.Utils;

[ExposeServices(typeof(GoodsUtils))]
public class GoodsUtils : ISingletonDependency
{
    [CanBeNull]
    public string NormalizeName([CanBeNull] string name) => name?.Trim();

    [CanBeNull]
    public string NormalizeSku([CanBeNull] string sku) => sku?.Trim().RemoveWhitespace() ?? string.Empty;
}