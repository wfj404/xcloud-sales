﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Timing;
using XCloud.Core.Json;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Users;
using XCloud.Platform.Messenger.Abstraction.Core;
using XCloud.Platform.Messenger.Service;

namespace XCloud.Platform.Messenger.Abstraction.MessageHandler.Impl;

[ExposeServices(typeof(IMessageHandler))]
public class PingHandler : IMessageHandler, IScopedDependency
{
    private readonly IClock _clock;
    private readonly ILogger _logger;
    private readonly ITokenService _tokenService;
    private readonly IUserService _userAccountService;

    public PingHandler(IClock clock, ILogger<PingHandler> logger, ITokenService tokenService,
        IUserService userAccountService)
    {
        _clock = clock;
        this._logger = logger;
        _tokenService = tokenService;
        _userAccountService = userAccountService;
    }

    public string MessageType => MessageTypeConst.Ping;

    public int Sort => 1;

    private async Task<UserDto> GetAuthedUserOrNullAsync(ClientMessageContext context)
    {
        var json = context.Message.Data;

        var body =
            context.MessengerServer.MessageSerializer.DeserializeFromStringOrDefault<PingMessageBody>(json,
                this._logger) ?? new PingMessageBody();

        if (!string.IsNullOrWhiteSpace(body.Token))
        {
            var principal = await this._tokenService.ParseTokenAsync(body.Token);
            if (principal.IsAuthenticated(out var claims))
            {
                var userId = claims.GetSubjectId();
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    var user = await this._userAccountService.GetByIdAsync(userId);

                    return user;
                }
            }
        }

        return default;
    }

    private async Task TryRegUserAsync(ClientMessageContext context)
    {
        var user = await this.GetAuthedUserOrNullAsync(context);
        if (user != null)
        {
            context.Connection.ClientIdentity.SubjectId = user.Id;
            context.Connection.ClientIdentity.PingTime = this._clock.Now;

            var res = context.Connection.ToOnlineStatusDto();

            var userRegistrationService =
                context.ScopedServiceProvider.GetRequiredService<IUserRegistrationService>();
            await userRegistrationService.PingAsync(res);
        }
        else
        {
            context.Connection.ClientIdentity.SubjectId = default;
        }
    }

    public async Task HandleMessageFromClientAsync(ClientMessageContext context)
    {
        await this.TryRegUserAsync(context);

        context.Connection.ClientIdentity.PingTime = this._clock.Now;

        await context.Connection.SendMessageToClientAsync(new MessageDto
        {
            MessageType = MessageTypeConst.Ping,
            Data = "success"
        });
    }

    public Task HandleMessageFromTransportAsync(TransportMessageContext context)
    {
        throw new NotImplementedException();
    }
}