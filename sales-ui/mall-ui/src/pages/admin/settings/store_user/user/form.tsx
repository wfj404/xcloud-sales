import { apiClient } from '@/utils/client';
import { UserDto } from '@/utils/swagger';
import { SaveOutlined } from '@ant-design/icons';
import { Button, Form, Input, message, Modal } from 'antd';
import { useEffect, useState } from 'react';

export default (props: {
  show: boolean;
  hide: () => void;
  ok: () => void;
}) => {
  const { show, hide, ok } = props;
  const [loading, _loading] = useState(false);
  const [form, _form] = useState<UserDto>({});

  const save = (e: UserDto) => {
    _loading(true);
    apiClient.sys
      .manageUserCreateUser(e)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok && ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    show && _form({});
  }, [show]);

  return (
    <>
      <Modal
        title='新增用户'
        confirmLoading={loading}
        open={show}
        onCancel={() => {
          hide && hide();
        }}
        footer={
          <>
            <Button
              type='primary'
              onClick={() => {
                save(form);
              }}
              icon={<SaveOutlined />}
            >
              保存
            </Button>
          </>
        }
      >
        <Form>
          <Form.Item
            label='用户名'
            rules={[
              {
                required: true,
                message: '请输入用户名',
              },
              {
                pattern: new RegExp('^[a-zA-Z0-9_]{3,20}$'),
                message: '用户名必须是3-16位字母、数字或下划线',
              },
            ]}
          >
            <Input value={form.IdentityName || ''}
                   onChange={e => {
                     _form(x => ({ ...x, IdentityName: e.target.value }));
                   }}
            />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
