using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.Coupons;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Coupons;

public interface
    IUserCouponService : ISalesAppService
{
    Task<CouponUserMappingDto> GetByIdAsync(int id);

    Task MarkUserCouponAsUsedAsync(int userCouponId);

    Task<PagedResponse<CouponUserMappingDto>> QueryUserCouponPagingAsync(QueryUserCouponPagingInput dto);

    Task CreateUserCouponAsync(CouponUserMappingDto dto);

    Task ReturnBackUserCouponAsync(int userCouponId);

    Task<CouponUserMappingDto[]> AttachCouponAsync(CouponUserMappingDto[] data);

    Task<CouponUserMappingDto[]> AttachUserCouponStoreUserAsync(CouponUserMappingDto[] data);

    Task<CouponDto[]> CheckCanIssueAsync(CouponDto[] data, int userId);
}

public class UserCouponService : SalesAppService, IUserCouponService
{
    private readonly ICouponService _couponService;
    private readonly ISalesRepository<CouponUserMapping> _couponUserRepository;

    public UserCouponService(ISalesRepository<CouponUserMapping> couponUserRepository,
        ICouponService couponService)
    {
        _couponUserRepository = couponUserRepository;
        _couponService = couponService;
    }

    public async Task<CouponUserMappingDto> GetByIdAsync(int id)
    {
        var entity = await this._couponUserRepository.FirstOrDefaultAsync(x => x.Id == id);

        return entity?.MapTo<CouponUserMapping, CouponUserMappingDto>(this.ObjectMapper);
    }

    public async Task<CouponUserMappingDto[]> AttachUserCouponStoreUserAsync(CouponUserMappingDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _couponUserRepository.GetDbContextAsync();

        var userIds = data.Select(x => x.UserId).Distinct().ToArray();
        var users = await db.Set<StoreUser>().AsNoTracking().WhereIdIn(userIds).ToArrayAsync();
        foreach (var m in data)
        {
            var u = users.FirstOrDefault(x => x.Id == m.UserId);
            if (u == null)
                continue;

            m.StoreUser = ObjectMapper.Map<StoreUser, StoreUserDto>(u);
        }

        return data;
    }

    public async Task<CouponUserMappingDto[]> AttachCouponAsync(CouponUserMappingDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _couponUserRepository.GetDbContextAsync();

        var couponIds = data.Select(x => x.CouponId).Distinct().ToArray();
        var coupons = await db.Set<Coupon>().AsNoTracking().WhereIdIn(couponIds).ToArrayAsync();
        foreach (var m in data)
        {
            var u = coupons.FirstOrDefault(x => x.Id == m.CouponId);
            if (u == null)
                continue;

            m.Coupon = ObjectMapper.Map<Coupon, CouponDto>(u);
        }

        return data;
    }

    public async Task<CouponDto[]> CheckCanIssueAsync(CouponDto[] data, int userId)
    {
        if (!data.Any())
            return data;

        var db = await _couponUserRepository.GetDbContextAsync();
        var ids = data.Ids();

        var query = db.Set<CouponUserMapping>().AsNoTracking()
            .Where(x => x.UserId == userId)
            .Where(x => ids.Contains(x.CouponId));

        var groupedQuery = query
            .GroupBy(x => new { x.CouponId, x.IsUsed })
            .Select(x => new
            {
                x.Key.CouponId,
                x.Key.IsUsed,
                Count = x.Count()
            });

        var groupedData = await groupedQuery.ToArrayAsync();

        var now = this.Clock.Now;

        foreach (var m in data)
        {
            //user can issue by default
            m.CanBeIssued = true;

            if (!m.IsValid(now, out var errorMessage))
            {
                m.CanBeIssued = false;
                m.UnableToIssueReason = errorMessage;
                continue;
            }

            if (m.Amount <= 0)
            {
                m.CanBeIssued = false;
                m.UnableToIssueReason = "coupon is running out";
                continue;
            }

            var couponGroupedData = groupedData.Where(x => x.CouponId == m.Id).ToArray();

            //coupon in user pocket
            if (couponGroupedData.Any(x => !x.IsUsed))
            {
                m.CanBeIssued = false;
                m.UnableToIssueReason = "unused coupon exist in account";
                continue;
            }

            //if store administrator set account limitation
            if (m.AccountLimitationIsSet(out var limitation))
            {
                var count = couponGroupedData.Sum(x => x.Count);
                //check if reach the limitation
                m.CanBeIssued = count < limitation;
                m.UnableToIssueReason = "account limitation reached";
                continue;
            }
        }

        return data;
    }

    public async Task MarkUserCouponAsUsedAsync(int userCouponId)
    {
        var db = await _couponUserRepository.GetDbContextAsync();

        var userCoupon = await db.Set<CouponUserMapping>().FirstOrDefaultAsync(x => x.Id == userCouponId);
        if (userCoupon == null)
            throw new EntityNotFoundException(nameof(MarkUserCouponAsUsedAsync));

        var now = Clock.Now;

        if (!userCoupon.IsValid(now, out var error))
        {
            throw new UserFriendlyException(error);
        }

        userCoupon.IsUsed = true;
        userCoupon.UsedTime = now;

        await db.SaveChangesAsync();
    }

    public async Task ReturnBackUserCouponAsync(int userCouponId)
    {
        var db = await _couponUserRepository.GetDbContextAsync();

        var userCoupon = await db.Set<CouponUserMapping>().FirstOrDefaultAsync(x => x.Id == userCouponId);
        if (userCoupon == null)
            throw new EntityNotFoundException(nameof(MarkUserCouponAsUsedAsync));

        if (!userCoupon.IsUsed)
            throw new UserFriendlyException("优惠券未被使用");

        userCoupon.IsUsed = false;
        userCoupon.UsedTime = null;

        await db.SaveChangesAsync();
    }

    public async Task CreateUserCouponAsync(CouponUserMappingDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.CouponId) || dto.UserId <= 0)
            throw new ArgumentNullException(nameof(CreateUserCouponAsync));

        var db = await _couponUserRepository.GetDbContextAsync();

        var coupon = await this._couponService.GetByIdAsync(dto.CouponId);
        if (coupon == null)
            throw new EntityNotFoundException(nameof(CreateUserCouponAsync));

        var now = this.Clock.Now;

        if (!coupon.IsValid(now, out var errorMessage))
        {
            throw new UserFriendlyException(errorMessage);
        }

        if (coupon.Amount <= 0)
        {
            throw new UserFriendlyException("coupon is running out");
        }

        if (coupon.AccountLimitationIsSet(out var limitation))
        {
            var userCouponCount =
                await _couponUserRepository.CountAsync(x => x.UserId == dto.UserId && x.CouponId == coupon.Id);
            if (userCouponCount >= limitation)
                throw new UserFriendlyException("you reach the limitation for get this coupon");
        }

        var userCoupon = new CouponUserMapping
        {
            Id = default,
            CouponId = coupon.Id,
            UserId = dto.UserId,
            IsUsed = false,
            UsedTime = null,
            ExpiredAt = null,
            CreationTime = now
        };
        if (coupon.ExpiredInDays != null)
        {
            userCoupon.ExpiredAt = now.AddDays(coupon.ExpiredInDays.Value);
        }

        db.Set<CouponUserMapping>().Add(userCoupon);

        coupon.Amount -= 1;

        await db.SaveChangesAsync();
    }


    public async Task<PagedResponse<CouponUserMappingDto>> QueryUserCouponPagingAsync(QueryUserCouponPagingInput dto)
    {
        var db = await _couponUserRepository.GetDbContextAsync();

        var query = db.Set<CouponUserMapping>().IgnoreQueryFilters().AsNoTracking();

        if (dto.UserId != null)
            query = query.Where(x => x.UserId == dto.UserId);

        if (dto.Used != null)
            query = query.Where(x => x.IsUsed == dto.Used.Value);

        if (dto.AvailableFor != null)
            query = query.Where(x => x.ExpiredAt == null || x.ExpiredAt.Value > dto.AvailableFor.Value);

        var count = await query.CountOrDefaultAsync(dto);

        query = query.OrderBy(x => x.IsUsed).ThenByDescending(x => x.CreationTime);

        var data = await query.PageBy(dto.ToAbpPagedRequest()).ToArrayAsync();

        var response = data.Select(x => ObjectMapper.Map<CouponUserMapping, CouponUserMappingDto>(x)).ToArray();

        return new PagedResponse<CouponUserMappingDto>(response, dto, count);
    }
}