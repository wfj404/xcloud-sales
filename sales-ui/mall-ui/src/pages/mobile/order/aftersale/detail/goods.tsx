import { getSkuName } from '@/utils/biz';
import { AfterSalesDto } from '@/utils/swagger';
import { Box, Typography } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

export default (props: { model: AfterSalesDto }) => {
  const { model } = props;

  return (
    <Box sx={{ my: 1 }}>
      <Typography variant="subtitle1" gutterBottom>
        售后商品
      </Typography>
      <Table sx={{ width: '100%', my: 1 }} size="small">
        <TableBody>
          {model.Items?.map((x, index) => {
            return (
              <TableRow key={index} hover>
                <TableCell>
                  <Typography variant="body1">
                    {getSkuName(x?.OrderItem?.Sku || {})}
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography
                    variant="overline"
                    color="primary"
                    sx={{ display: 'inline' }}
                  >
                    {`x${x.Quantity}`}
                  </Typography>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Box>
  );
};
