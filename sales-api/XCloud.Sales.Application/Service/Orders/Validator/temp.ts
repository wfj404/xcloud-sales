//this is a template of config structure
const data = [
    {
        //match all orders
        ValidatorType: "*",
        RuleJson: JSON.stringify({
            let_this_json_empty: true
        })
    },
    {
        //only order total amount greater than 99
        ValidatorType: "order-price",
        RuleJson: JSON.stringify({
            LimitedOrderAmount: 99
        })
    },
    {
        ValidatorType: "",
        RuleJson: JSON.stringify({})
    }
]

console.log(JSON.stringify(data));