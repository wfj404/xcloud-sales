﻿namespace XCloud.Sales.Application.Domain.AfterSale;

public class AfterSalesItem : SalesBaseEntity<string>
{
    public string AfterSalesId { get; set; }

    public string OrderId { get; set; }

    public string OrderItemId { get; set; }

    public int Quantity { get; set; }
}