import { Avatar, Button, ConfigProvider } from 'antd';
import { useEffect, useMemo } from 'react';
import { history, Link, useLocation, useOutlet, useSnapshot } from 'umi';

import AdminLoginRequired from '@/components/login/admin';
import UserLoginRequired from '@/components/login/user';
import XAction from '@/layouts/admin/action';
import default_menu from '@/layouts/admin/menu';
import XAppList from '@/layouts/app_list';
import { storeState, StoreType } from '@/store';
import { adminGetFilteredMenuData, getUserName, isSh3H } from '@/utils/biz';
import { config } from '@/utils/config';
import { resolveAvatar, resolveUrl } from '@/utils/storage';
import { AdminGrantedPermissionResponse, UserDto } from '@/utils/swagger';
import { isInIframe, isStringEmpty, simpleString } from '@/utils/utils';
import { PageContainer, ProLayout } from '@ant-design/pro-layout';

import sh3h_menu from '../sh3h/admin/menu';
import sh3h_app_list from '../sh3h/app_list';

export default function () {
  const children = useOutlet();
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();
  const menus = isSh3H() ? sh3h_menu : default_menu;
  const filteredMenus = adminGetFilteredMenuData(
    menus,
    (app.adminPermissions as AdminGrantedPermissionResponse) || {},
  );

  useEffect(() => {
    console.log('菜单数据', filteredMenus);
  }, [filteredMenus]);

  const nick_name = simpleString(
    getUserName(app.admin?.User as UserDto) || '--',
    8,
  );

  const settings = app.mallSettings || {};

  const logo_url =
    useMemo(() => {
      const urls = [
        settings.FullLogoStorageData,
        settings.SimpleLogoStorageData,
      ].map((x) => resolveUrl(x, { height: 100 }));
      return urls.filter((x) => !isStringEmpty(x)).at(0);
    }, [settings]) || config.app.logo.normal;

  const avatar_url = useMemo(
    () => resolveAvatar(app.admin?.User?.Avatar),
    [app.storeManager?.User?.Avatar],
  );

  return (
    <ConfigProvider
      //componentSize="small"
      theme={{
        token: {
          colorPrimary: '722ED1',
          //borderRadius: 0,
        },
      }}
    >
      <UserLoginRequired>
        <AdminLoginRequired>
          {isInIframe() && <div style={{ display: 'none' }}>in iframe</div>}
          <ProLayout
            pure={isInIframe()}
            location={location}
            appList={isSh3H() ? sh3h_app_list : XAppList}
            theme="light"
            navTheme={'light'}
            layout="mix"
            fixedHeader
            splitMenus
            logo={
              <img
                style={{}}
                alt=""
                src={logo_url}
                onClick={() => {
                  history.push({
                    pathname: '/admin',
                  });
                }}
              />
            }
            title={'管理端'}
            menu={{
              request: async () => filteredMenus,
              params: filteredMenus,
            }}
            menuItemRender={(x, dom) => {
              return <Link to={x.path || '/'}>{dom}</Link>;
            }}
            actionsRender={() => [
              isSh3H() || (
                <Button
                  type="dashed"
                  size="small"
                  onClick={() => {
                    history.push({
                      pathname: '/admin/catalog/external_skus',
                    });
                  }}
                >
                  导入商品
                </Button>
              ),
              <XAction />,
            ]}
            waterMarkProps={{
              content: `管理端：${nick_name}`,
            }}
            avatarProps={{
              icon: <Avatar src={avatar_url}>{nick_name}</Avatar>,
              size: 'small',
              title: nick_name,
              onClick: (e) => {
                console.log(e);
                history.push({
                  pathname: '/admin/ucenter',
                });
              },
            }}
            token={{}}
          >
            <PageContainer>{children}</PageContainer>
          </ProLayout>
        </AdminLoginRequired>
      </UserLoginRequired>
    </ConfigProvider>
  );
}
