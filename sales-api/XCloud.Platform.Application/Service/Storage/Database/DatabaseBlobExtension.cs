﻿using Volo.Abp.BlobStoring;

namespace XCloud.Platform.Application.Service.Storage.Database;

public static class DatabaseBlobExtension
{
    public static void UseDatabaseStorageProvider(this BlobContainerConfiguration blobContainerConfiguration)
    {
        blobContainerConfiguration.ProviderType = typeof(DatabaseBlobProvider);
    }
}