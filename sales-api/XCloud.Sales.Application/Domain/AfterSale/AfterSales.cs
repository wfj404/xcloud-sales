namespace XCloud.Sales.Application.Domain.AfterSale;

public class AfterSales : SalesBaseEntity<string>, IHasModificationTime, IHasCreationTime
{
    public string OrderId { get; set; }

    public string ReasonForReturn { get; set; }

    public string RequestedAction { get; set; }

    public string UserComments { get; set; }

    public string StaffNotes { get; set; }

    public string Images { get; set; }

    public string AfterSalesStatusId { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public bool HideForCustomer { get; set; }
}