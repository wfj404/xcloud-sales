import { redirectToLinkObject } from '@/utils/biz';
import { isStringEmpty } from '@/utils/utils';
import { Box, Button, Typography } from '@mui/material';

import { TitleBlockProps } from './utils';

export default ({ data }: { data: TitleBlockProps }) => {
  if (!data || isStringEmpty(data.title)) {
    return null;
  }

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          px: 1,
        }}
      >
        <Box>
          <Typography variant="h4" sx={{}}>
            {data.title}
          </Typography>
        </Box>
        {isStringEmpty(data.moreText) || (
          <Button
            size="small"
            onClick={() => {
              data.moreLink && redirectToLinkObject(data.moreLink);
            }}
          >
            {data.moreText}
          </Button>
        )}
      </Box>
    </>
  );
};
