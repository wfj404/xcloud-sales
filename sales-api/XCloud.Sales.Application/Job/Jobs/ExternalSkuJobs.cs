﻿using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Common;

namespace XCloud.Sales.Application.Job.Jobs;

[ExposeServices(typeof(ExternalSkuJobs))]
[UnitOfWork]
public class ExternalSkuJobs : SalesAppService
{
    private readonly IExternalSkuService _externalSkuService;

    public ExternalSkuJobs(IExternalSkuService externalSkuService)
    {
        _externalSkuService = externalSkuService;
    }

    public async Task DeleteExpiredAsync()
    {
        var expiredTime = this.Clock.Now.AddDays(-100);

        await this._externalSkuService.DeleteExpiredAsync(expiredTime);
    }
}