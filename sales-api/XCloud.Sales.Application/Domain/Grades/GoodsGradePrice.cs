namespace XCloud.Sales.Application.Domain.Grades;

public class GoodsGradePrice : SalesBaseEntity<string>, IHasCreationTime
{
    public string SkuId { get; set; }
        
    public string GradeId { get; set; }
        
    public decimal PriceOffset { get; set; }

    public DateTime CreationTime { get; set; }
}