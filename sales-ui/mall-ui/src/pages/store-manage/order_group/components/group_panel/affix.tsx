import { Affix } from 'antd';
import { ReactNode, useEffect, useRef, useState } from 'react';

export default ({ children }: { children: ReactNode }) => {
  const ref = useRef<HTMLDivElement>(null);
  const [affix_height, _affix_height] = useState<number>(0);

  useEffect(() => {
    const calculate_affix_height = () => {
      _affix_height(
        window.innerHeight -
          (ref.current?.getBoundingClientRect().top || 0) -
          10,
      );
    };

    const id = setInterval(() => calculate_affix_height(), 1000);
    window.onresize = () => calculate_affix_height();
    window.onscroll = () => calculate_affix_height();

    calculate_affix_height();

    return () => {
      id && clearInterval(id);
      window.onresize = null;
      window.onscroll = null;
    };
  }, []);

  return (
    <>
      <Affix offsetTop={80}>
        <div
          ref={ref}
          style={{
            overflowY: 'scroll',
            height: affix_height,
          }}
        >
          {children}
        </div>
      </Affix>
    </>
  );
};
