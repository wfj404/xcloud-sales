﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotNetCore.CAP.Internal;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

// ReSharper disable All

namespace XCloud.Application.Queue;

public interface ICapConsumerServiceSelectorContributor
{
    IEnumerable<ConsumerExecutorDescriptor> FindCapConsumers();
}

public class MyCapConsumerServiceSelector : ConsumerServiceSelector
{
    public MyCapConsumerServiceSelector([NotNull] IServiceProvider serviceProvider) : base(serviceProvider)
    {
        //
    }

    protected override IEnumerable<ConsumerExecutorDescriptor> FindConsumersFromInterfaceTypes(
        IServiceProvider provider)
    {
        var logger = provider.GetRequiredService<ILogger<MyCapConsumerServiceSelector>>();

        logger.LogWarning(message: nameof(FindConsumersFromInterfaceTypes));

        var descriptorFromInterfaces = base.FindConsumersFromInterfaceTypes(provider).ToArray();
        foreach (var m in descriptorFromInterfaces)
        {
            yield return m;
        }

        //addon
        var contributorList = provider.GetServices<ICapConsumerServiceSelectorContributor>();

        foreach (var contributor in contributorList)
        {
            logger.LogInformation(
                message: contributor.GetType().FullName ?? nameof(ICapConsumerServiceSelectorContributor));

            var descriptors = contributor.FindCapConsumers().ToArray();

            foreach (var m in descriptors)
            {
                yield return m;
            }
        }
    }
}