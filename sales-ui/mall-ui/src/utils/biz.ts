import { TreeDataNode } from 'antd';
import { CascaderOption } from 'antd-mobile';
import { DefaultOptionType } from 'antd/es/select';
import { sortBy, trimEnd, trimStart } from 'lodash-es';
import { stringify } from 'qs';
import qs from 'query-string';
import { CSSProperties, ReactNode } from 'react';
import { history } from 'umi';

import { config } from '@/utils/config';
import { resolveUrl, ResolveUrlOption } from '@/utils/storage';
import {
    AdminDto, AdminGrantedPermissionResponse, AntDesignTreeNode, CouponDto, DeliveryArea,
    DeliveryRecordDto, GiftCardDto, GoodsDto, GoodsPictureDto, GoodsRedirectToUrlDto, OrderDto,
    PaymentBillDto, SkuDto, StorageMetaDto, StoreDto, StoreManagerDto,
    StoreManagerGrantedPermissionResponse, StoreUserDto, UserAddress, UserDto
} from '@/utils/swagger';
import {
    firstNotEmpty, GlobalContainer, isArrayEmpty, isInIframe, isNumberEmpty, isStringEmpty, isUrl,
    stringOrUndefined
} from '@/utils/utils';
import { MenuDataItem } from '@ant-design/pro-layout';
import { UniqueIdentifier } from '@dnd-kit/core';

import { isPastDateTime } from './dayjs';
import { QueryGoodsPaging } from './swagger_qingdao';

export const getTreeSelectOptionsV2 = (
  root_node: AntDesignTreeNode,
  title_callback?: (d: AntDesignTreeNode) => ReactNode,
): DefaultOptionType => {
  const modify_title = title_callback || ((x) => x.title || '');

  const title = modify_title(root_node);
  const children = root_node.children?.map<DefaultOptionType>((d) =>
    getTreeSelectOptionsV2(d, modify_title),
  );

  return {
    value: root_node.key || '',
    label: title,
    children: children,
  };
};

export const getAntDesignTreeNodeData = (
  root_node: AntDesignTreeNode,
  title_callback?: (d: AntDesignTreeNode) => ReactNode,
): TreeDataNode => {
  const modify_title = title_callback || ((x) => x.title || '');

  const title = modify_title(root_node);
  const children = root_node.children?.map<TreeDataNode>((d) =>
    getAntDesignTreeNodeData(d, modify_title),
  );

  return {
    key: root_node.key || '',
    title: title,
    children: children,
  };
};

export const getFlatNodes = (
  root_node: AntDesignTreeNode,
): AntDesignTreeNode[] => {
  const flated_children =
    root_node.children?.flatMap((d) => getFlatNodes(d)) || [];
  return [
    {
      ...root_node,
      children: undefined,
    },
    ...flated_children,
  ];
};

export const isGiftCardExpired = (x: GiftCardDto): boolean => {
  const past = isPastDateTime(x.EndTime) || true;
  return !isStringEmpty(x.EndTime) && past;
};

export const parseQueryString = (
  searchString: string,
): qs.ParsedQuery<string> => {
  try {
    return qs.parse(searchString) || {};
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const updateQueryString = <T extends Record<string, any>>(q: T) => {
  const pathname = trimEnd(window.location.pathname, '/');
  const query_string = trimStart(stringify({ ...q }, { skipNulls: true }), '/');

  const url = [pathname, query_string]
    .filter((x) => !isStringEmpty(x))
    .join('?');

  window.history.replaceState({}, '', url);
};

export const getDeliveryAreaFingerPrint = (x: DeliveryArea): string => {
  return `${stringOrUndefined(x.ProvinceId)}-${stringOrUndefined(
    x.CityId,
  )}-${stringOrUndefined(x.AreaId)}`;
};

export interface DndDataType<T> {
  id: UniqueIdentifier;
  item: T;
  placeholder?: boolean;
}

//-------------------------------------------------------------
export interface PictureSrc {
  small?: string | null;
  medium?: string | null;
  large?: string | null;
  origin?: string | null;
}

export interface IHasDeletion {
  IsDeleted?: boolean;
}

export interface IHasCreationTime {
  CreationTime?: string;
}

export interface IHasLastModifiedTime {
  LastModificationTime?: string;
}

export interface AntDesignTreeNodeV2<T> {
  children?: AntDesignTreeNodeV2<T>[] | null;
  key?: string | null;
  /** @format int32 */
  node_level?: number | null;
  raw_data?: T;
  title?: string | null;
}

export interface PermissionDto {
  name: string;
  key: string;
}

export interface PermissionGroupDto {
  name: string;
  permissions: PermissionDto[];
}

export interface ApiResponse<T> {
  Error?: {
    Code?: string | null;
    Data?: Record<string, any>;
    Details?: string | null;
    Message?: string | null;
  };
  Data?: T;
}

export interface PagedResponse<T> extends ApiResponse<T> {
  Items?: T[] | undefined | null;
  TotalPages?: number;
  PageSize?: number;
  PageIndex?: number;
  TotalCount?: number;
  IsNotEmpty?: boolean;
  IsEmpty?: boolean;
  ExtraProperties?: Record<any, any>;
}

export const redirectToGoodsSearch = (q: QueryGoodsPaging) => {
  history.push({
    pathname: '/search',
    search: qs.stringify(q || {}),
  });
};

export type LinkType = 'link' | 'goods' | 'coupon';

export interface LinkDto extends GoodsRedirectToUrlDto {
  type?: LinkType;
  goods?: GoodsDto;
  coupon?: CouponDto;
}

export const redirectToLinkObject = (link: LinkDto): void => {
  console.log('redirect to link obj:', link);

  if (link.type == 'goods') {
    if (!link.goods) {
      return;
    }
    const set_detail_func = GlobalContainer.setGoodsDetailId;

    if (set_detail_func) {
      set_detail_func(link.goods?.Id);
    } else {
      history.push({
        pathname: `/goods/${link.goods.Id}`,
      });
    }
    return;
  }

  if (link.type == 'coupon') {
    if (link.coupon) {
      alert('todo');
      return;
    }
  }

  const url = firstNotEmpty([link.H5Url, link.WebUrl]);
  if (url && !isStringEmpty(url)) {
    if (isUrl(url)) {
      window.location.href = url;
    } else {
      history.push({
        pathname: url,
      });
    }
  } else {
    //
  }
};

export const redirectToLogin = (): void => {
  const currentPath = window.location.href;

  const external_3h_login_url: string = config.external_3h_login_url || '';

  if (isStringEmpty(external_3h_login_url)) {
    history.push({
      pathname: '/account/login',
      search: qs.stringify({
        next: currentPath,
        qs: history.location.search,
      }),
    });
  } else {
    if (isInIframe()) {
      GlobalContainer.message?.error('登录已过期，请刷新页面').then((res) => {
        console.log(res);
      });
      return;
    } else {
      const q = qs.stringify(
        {
          app: 'sales_app',
          ref: currentPath,
        },
        {
          encode: true,
        },
      );
      console.log('跳转到外部登录', external_3h_login_url, q);
      window.location.href = `${external_3h_login_url}?${trimStart(q, '?')}`;
    }
  }
};

export type VantCascaderOption = {
  text: string;
  value: string;
  children?: VantCascaderOption[];
};

export const convertToAntdData = (item: VantCascaderOption): CascaderOption => {
  return {
    value: item.value,
    label: item.text,
    children: item.children?.map((d) => convertToAntdData(d)),
  };
};

export const sortGoodsPictures = (
  data: GoodsPictureDto[],
  skuId?: string | null,
): GoodsPictureDto[] => {
  const sortSku = (x: GoodsPictureDto) => {
    if (isStringEmpty(skuId)) {
      return 0;
    }

    return x.SkuId == skuId ? -1 : 0;
  };

  return sortBy(
    data,
    (x) => sortSku(x),
    (x) => x.DisplayOrder || 0,
  );
};

export const getSkuName = (sku?: SkuDto | null): string | undefined | null => {
  if (!sku) {
    return null;
  }
  let names = [sku.Goods?.Name, sku.Name].filter((x) => !isStringEmpty(x));

  if (isArrayEmpty(names)) {
    return undefined;
  }

  return names.join('-');
};

export const IsGoodsAvailable = (goods?: GoodsDto | null): boolean => {
  if (goods == null) {
    return false;
  }

  if (goods.IsDeleted || !goods.Published) {
    return false;
  }

  return true;
};

export const IsSkuAvailable = (sku?: SkuDto | null): boolean => {
  if (sku == null) {
    return false;
  }

  if (sku.IsDeleted || !sku.IsActive) {
    return false;
  }

  return true;
};

export const isSkuAndGoodsAvailable = (sku?: SkuDto | null): boolean => {
  return IsSkuAvailable(sku) && IsGoodsAvailable(sku?.Goods);
};

export const getPictureSrc = (
  meta: StorageMetaDto,
  origin?: boolean,
): PictureSrc => {
  return {
    small: resolveUrl(meta, {
      width: 300,
    }),
    medium: resolveUrl(meta, {
      width: 700,
    }),
    large: resolveUrl(meta, {
      width: 1000,
    }),
    origin: origin ? resolveUrl(meta) : undefined,
  };
};

export const getGoodsMainPictureUrl = (
  goods: GoodsDto,
  option?: ResolveUrlOption,
): string | null | undefined => {
  let pictures = goods.GoodsPictures || [];
  let meta = sortGoodsPictures(pictures)
    .map<StorageMetaDto>((x) => x.StorageMeta || {})
    .at(0);
  return resolveUrl(meta, option);
};

export const getSkuMainPictureUrl = (
  sku: SkuDto,
  option?: ResolveUrlOption,
): string | null | undefined => {
  let pictures = sku?.Goods?.GoodsPictures || [];
  let meta = sortGoodsPictures(pictures, sku.Id)
    .map<StorageMetaDto>((x) => x.StorageMeta || {})
    .at(0);
  return resolveUrl(meta, option);
};

//最小下单数量
const minQuantity = 1;
const infiniteMaxQuantity = 1000000;

//sku最多可以买多少
export const getSkuMaxAmountInOnePurchase = (sku?: SkuDto | null) => {
  if (sku) {
    if (sku.MaxAmountInOnePurchase && sku.MaxAmountInOnePurchase > 0) {
      return sku.MaxAmountInOnePurchase;
    }
  }

  return infiniteMaxQuantity;
};

//sku最少可以买多少
export const getSkuMinAmountInOnePurchase = (sku?: SkuDto | null) => {
  if (sku) {
    if (sku.MinAmountInOnePurchase && sku.MinAmountInOnePurchase > 0) {
      return sku.MinAmountInOnePurchase;
    }
  }

  return minQuantity;
};

export const isUserLogin = (user?: UserDto | null): boolean =>
  !isStringEmpty(user?.Id);

export const isAdminLogin = (admin?: AdminDto | null): boolean =>
  !isStringEmpty(admin?.Id);

export const isStoreUserLogin = (storeUser?: StoreUserDto | null): boolean =>
  !isNumberEmpty(storeUser?.Id);

export const isManagerLogin = (
  storeManager?: StoreManagerDto | null,
): boolean => !isStringEmpty(storeManager?.Id);

export const adminHasPermission = (
  permissions?: AdminGrantedPermissionResponse,
  permissionKey?: string,
): boolean => {
  if (!permissionKey) {
    return false;
  }

  if (permissions?.Data?.IsSuperAdmin) {
    return true;
  }

  let permission_list = permissions?.Permissions || [];

  return (
    permission_list.indexOf('*') >= 0 ||
    permission_list.indexOf(permissionKey) >= 0
  );
};

export const managerHasPermission = (
  permissions?: StoreManagerGrantedPermissionResponse,
  permissionKey?: string,
): boolean => {
  if (!permissionKey) {
    return false;
  }

  if (permissions?.Data?.IsSuperManager) {
    return true;
  }

  let permission_list = permissions?.Permissions || [];

  return (
    permission_list.indexOf('*') >= 0 ||
    permission_list.indexOf(permissionKey) >= 0
  );
};

export const getFilteredMenuDataOrigin = (
  data: MenuDataItem[],
  check: (d: string) => boolean,
): MenuDataItem[] => {
  function walk(item: MenuDataItem[]): MenuDataItem[] {
    item = item.filter((x) => isStringEmpty(x.key) || check(x.key || ''));

    item.forEach((x) => {
      if (x.children) {
        x.children = walk(x.children);
      }
    });

    return item;
  }

  data = walk(data);

  return data;
};

export const adminGetFilteredMenuData = (
  data: MenuDataItem[],
  permissions: AdminGrantedPermissionResponse,
): MenuDataItem[] => {
  return getFilteredMenuDataOrigin(data, (x) =>
    adminHasPermission(permissions, x),
  );
};

export const getUserName = (
  user?: UserDto | null,
): string | null | undefined => {
  if (!user) {
    return null;
  }
  return [user.NickName, user.IdentityName]
    .filter((x) => !isStringEmpty(x))
    .at(0);
};

export const formatGoodsRedirectDto = (
  dto: GoodsRedirectToUrlDto,
): GoodsRedirectToUrlDto => {
  dto.AlipayMiniProgramUrl = stringOrUndefined(dto.AlipayMiniProgramUrl);
  dto.WxMiniProgramUrl = stringOrUndefined(dto.WxMiniProgramUrl);
  dto.AppUrl = stringOrUndefined(dto.AppUrl);
  dto.H5Url = stringOrUndefined(dto.H5Url);
  dto.WebUrl = stringOrUndefined(dto.WebUrl);

  return dto;
};

export interface Status<T> {
  name: string;
  desc?: string;
  id: T | string | number;
  color?: string;
  icon?: any;
}

export interface LabeledValueItem {
  label: ReactNode;
  key: any;
  icon?: ReactNode;
  disabled?: boolean;
}

export const OrderStockDeductStrategy = {
  AfterPlaceOrder: 'after-place-order',
  AfterPayment: 'after-payment',
};

export const OrderStockDeductStrategyList: Status<any>[] = [
  {
    id: OrderStockDeductStrategy.AfterPlaceOrder,
    name: '下单后',
  },
  {
    id: OrderStockDeductStrategy.AfterPayment,
    name: '支付后',
  },
];

export const PaymentChannel = {
  Manual: 'manual',
  Balance: 'balance',
  WechatMp: 'wechat-mp',
  WechatOpen: 'wechat-open',
};

export const PaymentMethod = {
  Online: 'online',
  Offline: 'offline',
};

export const PaymentMethodList: Status<any>[] = [
  {
    name: '在线支付',
    id: PaymentMethod.Online,
  },
  {
    name: '线下支付',
    id: PaymentMethod.Offline,
  },
];

export const getPaymentMethod = (x: OrderDto) =>
  PaymentMethodList.find((d) => d.id == x.PaymentMethodId)?.name;

export const GoodsType = {
  Normal: 0,
  Virtual: 1,
  Service: 2,
};

export const ShippingMethod = {
  Delivery: 'delivery',
  Pickup: 'pickup',
};

type test_key_type = keyof typeof ShippingMethod;

export const DeliveryType = {
  Express: 'express',
  ShortDistanceDelivery: 'short-distance-delivery',
};

export const DeliveryTypeList: Status<any>[] = [
  {
    name: '快递配送',
    id: DeliveryType.Express,
  },
  {
    name: '同城配送',
    id: DeliveryType.ShortDistanceDelivery,
  },
];

export const getDeliveryType = (x: DeliveryRecordDto) =>
  DeliveryTypeList.find((d) => d.id == x.DeliveryType)?.name;

export const ShippingMethodList: Status<any>[] = [
  {
    name: '配送',
    id: ShippingMethod.Delivery,
  },
  {
    name: '自提',
    id: ShippingMethod.Pickup,
  },
];

export const getShippingMethod = (x: OrderDto) =>
  ShippingMethodList.find((d) => d.id == x.ShippingMethodId)?.name;

export const PaymentChannelList: Status<any>[] = [
  {
    id: PaymentChannel.Manual,
    name: '线下支付',
  },
  {
    id: PaymentChannel.Balance,
    name: '余额支付',
  },
  {
    id: PaymentChannel.WechatMp,
    name: '微信公众号支付',
  },
  {
    id: PaymentChannel.WechatOpen,
    name: '微信小程序支付',
  },
];

export const getPaymentChannel = (x: PaymentBillDto) =>
  PaymentChannelList.find((d) => d.id == x.PaymentChannel)?.name;

export const PaymentStatus = {
  Pending: 'pending',
  Paid: 'paid',
};

export const PaymentStatusList: Status<any>[] = [
  {
    id: PaymentStatus.Pending,
    name: '未支付',
  },
  {
    id: PaymentStatus.Paid,
    name: '已支付',
    color: 'success',
  },
];

export const OrderStatus = {
  Pending: 'pending',
  Processing: 'processing',
  Finished: 'finished',
  AfterSales: 'aftersales',
  Closed: 'closed',
};

export const OrderStatusList: Status<any>[] = [
  {
    id: OrderStatus.Pending,
    name: '进行中',
  },
  {
    id: OrderStatus.Processing,
    name: '处理中',
  },
  {
    id: OrderStatus.Finished,
    name: '已完成',
    color: 'success',
  },
  {
    id: OrderStatus.Closed,
    name: '已取消',
    color: 'error',
  },
  {
    id: OrderStatus.AfterSales,
    name: '售后',
  },
];

export const AfterSalesStatus = {
  Processing: 'processing',
  Approved: 'approved',
  Rejected: 'rejected',
  Complete: 'finished',
  Cancelled: 'closed',
};

export const AfterSalesStatusList: Status<any>[] = [
  {
    id: AfterSalesStatus.Processing,
    name: '处理中',
  },
  {
    id: AfterSalesStatus.Approved,
    name: '已批准',
  },
  {
    id: AfterSalesStatus.Rejected,
    name: '被拒绝',
    color: 'warning',
  },
  {
    id: AfterSalesStatus.Complete,
    name: '完成',
    color: 'success',
  },
  {
    id: AfterSalesStatus.Cancelled,
    name: '取消',
    color: 'error',
  },
];

export const ShippingStatus = {
  NotShipping: 'not-yet',
  Shipping: 'shipping',
  Shipped: 'shipped',
};

export const ShippingStatusList: Status<any>[] = [
  {
    name: '未交付',
    id: ShippingStatus.NotShipping,
  },
  {
    name: '交付中',
    id: ShippingStatus.Shipping,
  },
  {
    name: '已交付',
    id: ShippingStatus.Shipped,
  },
];

export const AdminPermissionKeys = {
  Super: '*',
  Settings: 'settings',
  Catalog: 'catalog',
  Market: 'market',
  Dashboard: 'dashboard',
  Warehouse: 'warehouse',
};

export const AdminPermissionWithGroup: PermissionGroupDto[] = [
  {
    name: '超级管理员权限',
    permissions: [
      {
        name: '超级管理员权限',
        key: AdminPermissionKeys.Super,
      },
    ],
  },
  {
    name: '商品',
    permissions: [
      {
        name: '商品',
        key: AdminPermissionKeys.Catalog,
      },
    ],
  },
  {
    name: '市场',
    permissions: [
      {
        name: '营销',
        key: AdminPermissionKeys.Market,
      },
    ],
  },
  {
    name: '系统设置',
    permissions: [
      {
        name: '商城设置',
        key: AdminPermissionKeys.Settings,
      },
    ],
  },
  {
    name: '报表',
    permissions: [
      {
        name: '全部',
        key: AdminPermissionKeys.Dashboard,
      },
    ],
  },
  {
    name: '仓库',
    permissions: [
      {
        name: '详细库存',
        key: AdminPermissionKeys.Warehouse,
      },
    ],
  },
];

export const AdminPermissionList: PermissionDto[] =
  AdminPermissionWithGroup.flatMap((x) => x.permissions);

export const ManagerPermissionKeys = {
  Super: '*',
  Sku: 'store-sku',
  Order: 'store-order',
  Shipping: 'store-shipping',
  Bill: 'store-bill',
  Settings: 'store-settings',
};

export const ManagerPermissionWithGroup: PermissionGroupDto[] = [
  {
    name: '超级管理员权限',
    permissions: [
      {
        name: '超级管理员权限',
        key: ManagerPermissionKeys.Super,
      },
    ],
  },
  {
    name: '系统设置',
    permissions: [
      {
        name: '商城设置',
        key: ManagerPermissionKeys.Settings,
      },
    ],
  },
  {
    name: '商品',
    permissions: [
      {
        name: '商品管理',
        key: ManagerPermissionKeys.Sku,
      },
    ],
  },
  {
    name: '订单和售后',
    permissions: [
      {
        name: '订单管理',
        key: ManagerPermissionKeys.Order,
      },
      {
        name: '订单交付',
        key: ManagerPermissionKeys.Shipping,
      },
    ],
  },
  {
    name: '财务',
    permissions: [
      {
        name: '支付查询',
        key: ManagerPermissionKeys.Bill,
      },
    ],
  },
];

export const ManagerPermissionList: PermissionDto[] =
  ManagerPermissionWithGroup.flatMap((x) => x.permissions);

export const isOrderToService = (order?: OrderDto | null): boolean => {
  if (!order) {
    return false;
  }

  const paymentOk =
    order.PaymentStatusId == PaymentStatus.Paid ||
    order.PaymentMethodId == PaymentMethod.Offline;

  return paymentOk && order.ShippingStatusId != ShippingStatus.Shipped;
};

export const isSh3H = (): boolean => config.UMI_ENV == 'sh3h'; // || true;

export const flex_row: CSSProperties = {
  display: 'flex',
  flexDirection: 'row',
};

export const flex_row_start: CSSProperties = {
  ...flex_row,
  alignItems: 'flex-start',
  justifyContent: 'flex-start',
};

export const flex_row_inline: CSSProperties = {
  ...flex_row,
  alignItems: 'center',
  justifyContent: 'flex-start',
};

export const isLocationEmpty = (address: UserAddress) => {
  const lat = address.Lat || 0;
  const lon = address.Lon || 0;
  return lat == 0 || lon == 0;
};

export const getStoreLatLng = (
  model: StoreDto,
): L.LatLngLiteral | undefined => {
  if (model.Lat == undefined || model.Lon == undefined) {
    return undefined;
  }

  return {
    lat: model.Lat,
    lng: model.Lon,
  };
};

export const skuHasStock = (x: SkuDto): boolean =>
  (x.StockModel?.StockQuantity || 0) > 0;
