﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using XCloud.Core.Extension;
using XCloud.Core.Helper;
using XCloud.EntityFrameworkCore.Exceptions;
using XCloud.EntityFrameworkCore.Mapping;

namespace XCloud.EntityFrameworkCore.Extensions;

public static class EntityFrameworkExtension
{
    public static void AddRangeIfNotEmpty<T>(this DbSet<T> set, IEnumerable<T> data) where T : class
    {
        if (data == null)
            return;

        var dataArray = data.ToArray();

        if (dataArray.Any())
            set.AddRange(dataArray);
    }

    public static Type[] FindEntityTypes(this DbContext db)
    {
        var res = db
            .GetType()
            .GetProperties()
            .Select(x => x.PropertyType)
            .Where(x => x.IsEqualToGenericType(typeof(DbSet<>)))
            .Select(x => x.GenericTypeArguments.FirstOrDefault())
            .WhereNotNull()
            .ToArray();

        return res;
    }

    public static async Task<int> TrySaveChangesAsync(this DbContext db)
    {
        if (db.ChangeTracker.HasChanges())
        {
            var effected = await db.SaveChangesAsync();
            return effected;
        }

        return default;
    }

    /// <summary>
    /// 如果存在未提交的更改就抛出异常
    /// 为了防止不可预测的提交
    /// </summary>
    public static void ThrowIfHasChanges(this DbContext context)
    {
        if (context.ChangeTracker.HasChanges())
        {
            throw new UnSubmitChangesException($"{context.GetType().FullName}存在未提交的更改");
        }
    }

    public static void RollbackEntityChanges(this DbContext context)
    {
        if (context.ChangeTracker.HasChanges())
        {
            var changedState = new[]
            {
                //EntityState.Unchanged,
                //EntityState.Detached,
                EntityState.Added,
                EntityState.Modified,
                EntityState.Deleted
            };

            var entries = context.ChangeTracker.Entries().Where(e => changedState.Contains(e.State)).ToArray();

            foreach (var m in entries)
            {
                m.State = EntityState.Unchanged;
            }
        }
    }

    public static EntityEntry<T> AttachEntityIfNot<T>(this DbContext db, T entity) where T : class, IEntity
    {
        var set = db.Set<T>();

        var entry = db.ChangeTracker.Entries<T>()
            .FirstOrDefault(x => x.Entity.GetKeys().SequenceEqual(entity.GetKeys()));

        entry ??= set.Attach(entity);

        return entry;
    }

    public static void ConfigEntityMapperFromAssemblies(this ModelBuilder modelBuilder,
        params Assembly[] assemblies) => modelBuilder.ConfigEntityMapperFromAssemblies<IEntity>(assemblies);

    public static void ConfigEntityMapperFromAssemblies<TableType>(this ModelBuilder modelBuilder,
        params Assembly[] assemblies)
    {
        if (modelBuilder == null)
            throw new ArgumentNullException(nameof(modelBuilder));

        if (ValidateHelper.IsEmptyCollection(assemblies))
            throw new ArgumentNullException(nameof(assemblies));

        var allTypes = assemblies.GetAllTypes()
            .Where(x => x.IsNormalPublicClass() && !x.IsGenericType)
            .Where(x => x.IsAssignableToType<IMappingConfiguration>())
            .ToArray();

        foreach (var cls in allTypes)
        {
            var configType = cls.GetInterfaces()
                .FirstOrDefault(x => x.IsEqualToGenericType(typeof(IEntityTypeConfiguration<>)));
            if (configType == null)
                continue;

            var entityType = configType.GetGenericArguments().FirstOrDefault();
            if (entityType == null || !entityType.IsAssignableToType<TableType>())
                continue;

            var configuration = (IMappingConfiguration)Activator.CreateInstance(cls);
            if (configuration == null)
                throw new BusinessException(message: $"failed to create instance:{cls.FullName}");

            configuration.ApplyConfiguration(modelBuilder);
        }
    }
}