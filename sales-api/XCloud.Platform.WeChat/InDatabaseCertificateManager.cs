﻿using System;
using System.Collections.Generic;
using System.Linq;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Settings;
using XCloud.Platform.WeChat.Services.Payments;

namespace XCloud.Platform.WeChat;

[Obsolete]
public class InDatabaseCertificateManager : ICertificateManager
{
    private readonly WechatPaymentCertificateManageService _wechatPaymentCertificateManageService;

    public InDatabaseCertificateManager(WechatPaymentCertificateManageService wechatPaymentCertificateManageService)
    {
        _wechatPaymentCertificateManageService = wechatPaymentCertificateManageService;
    }

    public IEnumerable<CertificateEntry> AllEntries()
    {
        throw new System.NotImplementedException();
    }

    public void AddEntry(CertificateEntry entry)
    {
        var datalist = this.AllEntries().Where(x => x.SerialNumber != entry.SerialNumber).ToArray();
        datalist = datalist.Append(entry).ToArray();

        throw new System.NotImplementedException();
    }

    public CertificateEntry? GetEntry(string serialNumber)
    {
        return this.AllEntries().FirstOrDefault(x => x.SerialNumber == serialNumber);
    }

    public bool RemoveEntry(string serialNumber)
    {
        var datalist = this.AllEntries().Where(x => x.SerialNumber != serialNumber).ToArray();

        throw new NotImplementedException();
    }
}