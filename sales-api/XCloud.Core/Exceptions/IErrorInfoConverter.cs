﻿using Microsoft.AspNetCore.Http;
using XCloud.Core.Dto;

namespace XCloud.Core.Exceptions;

public interface IErrorInfoConverter
{
    int Order { get; }

    ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e);
}