﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.Extensions.Caching.Memory;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Service;
using XCloud.Platform.Application.Utils;

namespace XCloud.Platform.Application.Service.Settings;

public interface ISettings : IEntityDto
{
    //
}

public interface ISettingService : IXCloudAppService
{
    Task SaveSettingsAsync<T>(T dto) where T : class, ISettings, new();

    [NotNull]
    Task<T> GetSettingsAsync<T>(CacheStrategy cacheStrategy) where T : class, ISettings, new();
}

public class SettingService : XCloudAppService, ISettingService
{
    private readonly IKeyValueService _keyValueService;
    private readonly SettingUtils _settingUtils;

    public SettingService(IKeyValueService keyValueService, SettingUtils settingUtils)
    {
        _keyValueService = keyValueService;
        _settingUtils = settingUtils;
    }

    private string CacheKey(string name)
    {
        var key = $"sales.settings.dto.by.name={name}";
        return key;
    }

    public async Task SaveSettingsAsync<T>(T dto)
        where T : class, ISettings, new()
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var key = _settingUtils.GetSettingsRequiredTypeIdentityName<T>();

        await _keyValueService.SaveObjectAsync(key, dto);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await GetSettingsAsync<T>(new CacheStrategy { Refresh = true });
    }

    private async Task<T> GetSettingsAsync<T>()
        where T : class, ISettings, new()
    {
        var key = _settingUtils.GetSettingsRequiredTypeIdentityName<T>();

        var settings = await _keyValueService.GetObjectOrDefaultAsync<T>(key);

        if (settings != null)
            return settings;

        return new T();
    }

    public async Task<T> GetSettingsAsync<T>(CacheStrategy cacheStrategy)
        where T : class, ISettings, new()
    {
        var key = _settingUtils.GetSettingsRequiredTypeIdentityName<T>();

        var option = new CacheOption<T>(key: CacheKey(key), TimeSpan.FromMinutes(10));

        if (cacheStrategy.RemoveCache ?? false || (cacheStrategy.Refresh ?? false))
            MemoryCache.Remove(option.Key);

        var settings = await MemoryCache.GetOrCreateAsync(option.Key, async x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30);
            return await CacheProvider.ExecuteWithPolicyAsync(GetSettingsAsync<T>, option, cacheStrategy);
        });

        return settings ?? new T();
    }
}