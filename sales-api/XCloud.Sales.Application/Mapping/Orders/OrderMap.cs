using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderMap : SalesEntityTypeConfiguration<Order>
{
    public override void Configure(EntityTypeBuilder<Order> builder)
    {
        builder.ToTable("sales_order");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.GoodsType);
        builder.Property(x => x.UserId);
        builder.Property(x => x.GradeId).HasMaxLength(100);
        builder.Property(x => x.StoreId).RequiredId();

        builder.Property(x => x.ServiceStartTime);
        builder.Property(x => x.ServiceEndTime);
        builder.Property(x => x.CompleteTime);

        builder.Property(x => x.OrderSn).HasMaxLength(100);
        builder.Property(x => x.Remark).HasMaxLength(1000);
        builder.Property(x => x.OrderIp).HasMaxLength(100);
        builder.Property(x => x.PlatformId).HasMaxLength(100);
        builder.Property(x => x.AffiliateId);
        builder.Property(x => x.UserInvoiceId).HasMaxLength(100);

        builder.Property(x => x.ShippingMethodId).HasMaxLength(100);
        builder.Property(x => x.UserAddressId).HasMaxLength(100);

        builder.Property(o => o.RefundedAmount).PricePrecision();

        builder.Property(o => o.TotalPrice).PricePrecision();
        builder.Property(o => o.ItemsTotalPrice).PricePrecision();
        builder.Property(o => o.ShippingFee).PricePrecision();
        builder.Property(o => o.PackageFee).PricePrecision();
        builder.Property(o => o.SellerPriceOffset).PricePrecision();

        builder.Property(x => x.CouponPrice).PricePrecision();

        builder.Property(x => x.PromotionId).HasMaxLength(100);
        builder.Property(x => x.PromotionPriceOffset).PricePrecision();

        builder.Property(x => x.PaidTotalPrice).PricePrecision();
        builder.Property(x => x.PaymentStatusId).RequiredId();
        builder.Property(x => x.PaymentMethodId).HasMaxLength(100);
        builder.Property(x => x.OrderStatusId).RequiredId();
        builder.Property(x => x.ShippingStatusId).RequiredId();

        builder.Property(x => x.RewardPointsHistoryId);
        builder.Property(x => x.ExchangePointsAmount).PricePrecision();

        builder.Property(x => x.LastModificationTime);
        builder.Property(x => x.HideForCustomer);

        base.Configure(builder);
    }
}