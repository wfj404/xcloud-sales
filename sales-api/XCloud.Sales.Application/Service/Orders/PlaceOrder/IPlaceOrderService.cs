using System.Text;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.AspNetMvc.Core;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Exceptions;
using XCloud.Platform.Application.Service.Invoices;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Extension;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Shipping;
using XCloud.Sales.Application.Service.ShoppingCart;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.Application.Service.Orders.PlaceOrder;

public interface IPlaceOrderService : ISalesAppService
{
    Task<PlaceOrderResult> PrePlaceOrderWithoutSavingAsync(PlaceOrderRequestDto dto);

    Task SaveOrderToDatabaseAsync(PlaceOrderDataHolder holder);

    Task AfterPlacedOrderAsync(PlaceOrderDataHolder holder);
}

public class PlaceOrderService : SalesAppService, IPlaceOrderService
{
    private readonly OrderUtils _orderUtils;
    private readonly IWebHelper _webHelper;
    private readonly IStoreUserService _userService;
    private readonly ISalesRepository<Order> _repository;
    private readonly ISettingService _settingService;
    private readonly IGradeService _userGradeService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly IStoreService _storeService;
    private readonly IPriceCalculationService _priceCalculationService;
    private readonly IValueAddedItemService _valueAddedItemService;
    private readonly IShoppingCartService _shoppingCartService;
    private readonly IOrderItemService _orderItemService;
    private readonly IOrderStockQuantityAdjustService _orderStockQuantityAdjustService;
    private readonly ISpecValueCombinationService _specValueCombinationService;
    private readonly IUserInvoiceService _userInvoiceService;
    private readonly ISkuService _skuService;
    private readonly IOrderNoteService _orderNoteService;

    public PlaceOrderService(
        IGradeService userGradeService,
        ISettingService settingService,
        OrderUtils orderUtils,
        IWebHelper webHelper,
        IStoreUserService userService,
        ISalesRepository<Order> orderRepository,
        IPriceCalculationService priceCalculationService,
        IStoreGoodsMappingService storeGoodsMappingService,
        IValueAddedItemService valueAddedItemService,
        IStoreService storeService,
        IShoppingCartService shoppingCartService,
        IOrderStockQuantityAdjustService orderStockQuantityAdjustService,
        IUserInvoiceService userInvoiceService,
        ISkuService skuService, IOrderItemService orderItemService,
        IOrderNoteService orderNoteService,
        ISpecValueCombinationService specValueCombinationService)
    {
        _userGradeService = userGradeService;
        _orderUtils = orderUtils;
        _settingService = settingService;
        _repository = orderRepository;
        _priceCalculationService = priceCalculationService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _valueAddedItemService = valueAddedItemService;
        _storeService = storeService;
        _shoppingCartService = shoppingCartService;
        _orderStockQuantityAdjustService = orderStockQuantityAdjustService;
        _userInvoiceService = userInvoiceService;
        _skuService = skuService;
        _orderItemService = orderItemService;
        _orderNoteService = orderNoteService;
        _specValueCombinationService = specValueCombinationService;
        _webHelper = webHelper;
        _userService = userService;
    }

    private bool CheckAndPreparePlaceOrderRequest(PlaceOrderRequestDto dto, out string errorMessage)
    {
        if (dto.UserId <= 0)
        {
            errorMessage = "user not found";
            return false;
        }

        if (string.IsNullOrWhiteSpace(dto.StoreId))
        {
            errorMessage = "store is required";
            return false;
        }

        if (ValidateHelper.IsEmptyCollection(dto.Items))
        {
            errorMessage = "empty specs";
            return false;
        }

        foreach (var m in dto.Items)
        {
            if (string.IsNullOrWhiteSpace(m.SkuId))
            {
                errorMessage = "empty sku id";
                return false;
            }

            if (m.Quantity < 1)
            {
                errorMessage = "the min quantity is 1";
                return false;
            }
        }

        if (dto.Items.GroupBy(x => x.SkuId).Any(x => x.Count() > 1))
        {
            errorMessage = "duplicate input";
            return false;
        }

        errorMessage = string.Empty;
        return true;
    }

    private async Task BuildOrderValueAddedItemsAsync(PlaceOrderDataHolder holder)
    {
        var orderItemList = holder.Order.Items;
        var skuList = orderItemList.Select(x => x.Sku).WhereNotNull().ToArray();
        var goodsList = skuList.Select(x => x.Goods).WhereNotNull().ToArray();

        await _valueAddedItemService.AttachValueAddedItemsAsync(goodsList);

        foreach (var m in holder.Request.Items)
        {
            if (ValidateHelper.IsEmptyCollection(m.ValueAddedItemIds))
                continue;

            var itemList = new List<OrderValueAddedItem>();

            var orderItem = orderItemList.FirstOrDefault(x => x.SkuId == m.SkuId);
            if (orderItem == null)
                throw new UserFriendlyException(nameof(orderItem));

            var goodsValueAddedItems = skuList
                .FirstOrDefault(x => x.Id == m.SkuId)?.Goods?.ValueAddedItems;

            if (goodsValueAddedItems == null)
                throw new UserFriendlyException(nameof(goodsValueAddedItems));

            foreach (var valueAddedItemId in m.ValueAddedItemIds)
            {
                var valueAddedItem = goodsValueAddedItems.FirstOrDefault(x => x.Id == valueAddedItemId);
                if (valueAddedItem == null)
                    throw new UserFriendlyException("value added item not found");

                var entity = new OrderValueAddedItem
                {
                    Id = default,
                    OrderItemId = orderItem.Id,
                    ValueAddedItemId = valueAddedItem.Id,
                    PriceOffset = valueAddedItem.PriceOffset,
                    Name = valueAddedItem.Name,
                    Description = valueAddedItem.Description,
                };

                itemList.Add(entity);
            }

            orderItem.ValueAddedPriceOffset = itemList.Sum(x => x.PriceOffset);
            orderItem.OrderValueAddedItems =
                itemList.MapArrayTo<OrderValueAddedItem, OrderValueAddedItemDto>(ObjectMapper);
        }
    }

    private IEnumerable<CheckSkuForSaleInput> GetAssociatedSkusForCheck(SkuDto[] skus)
    {
        foreach (var m in skus)
        {
            foreach (var item in m.SpecItemList)
            {
                var value = item.SpecValue;

                if (value == null)
                    continue;

                if (string.IsNullOrWhiteSpace(value.AssociatedSkuId))
                    continue;

                yield return new CheckSkuForSaleInput()
                {
                    Id = value.AssociatedSkuId,
                    Quantity = value.AssociatedSkuCount
                };
            }
        }
    }

    private async Task CheckSkuForSaleAsync(PlaceOrderDataHolder holder)
    {
        var skus = holder.Order.Items.Select(x => x.Sku).WhereNotNull().ToArray();
        this._specValueCombinationService.AttachSpecItemList(skus);

        var specItems = skus.SelectMany(x => x.SpecItemList).ToArray();
        await this._specValueCombinationService.AttachSpecAndValuesAsync(specItems);

        //
        var orderItemSkusCheckInput = holder.Order.Items
            .Select(x => new CheckSkuForSaleInput() { Id = x.SkuId, Quantity = x.Quantity }).ToArray();

        var errorList =
            await this._storeGoodsMappingService.CheckSkuForSaleAsync(holder.Request.StoreId, orderItemSkusCheckInput);

        if (errorList.Any())
        {
            foreach (var e in errorList)
            {
                holder.GetOrCreateSkuError(e.Id, e.Sku).ErrorList.Add(e.Error);
            }
        }

        foreach (var orderItem in holder.Order.Items)
        {
            var checkInput = GetAssociatedSkusForCheck(new[] { orderItem.Sku }).ToArray();

            if (!checkInput.Any())
            {
                continue;
            }

            foreach (var m in checkInput)
            {
                m.Quantity *= orderItem.Quantity;
            }

            checkInput = checkInput.GroupBy(x => x.Id)
                .Select(x => new CheckSkuForSaleInput()
                {
                    Id = x.Key,
                    Quantity = x.Sum(d => d.Quantity)
                }).ToArray();

            var associatedSkusErrorList =
                await this._storeGoodsMappingService.CheckSkuForSaleAsync(holder.Request.StoreId, checkInput);

            if (associatedSkusErrorList.Any())
            {
                var errorStringList = associatedSkusErrorList.Select(x => x.Error).ToArray();
                var errorDescription = string.Join(',', errorStringList);
                holder.GetOrCreateSkuError(orderItem.SkuId, default).ErrorList.Add($"缺少物料：{errorDescription}");
            }
        }
    }

    private async Task BuildOrderItemsAsync(PlaceOrderDataHolder holder)
    {
        var dto = holder.Request;
        var order = holder.Order;

        var orderItems = new List<OrderItemDto>();

        foreach (var item in dto.Items)
        {
            var orderItem = new OrderItemDto
            {
                OrderId = order.Id,
                SkuId = item.SkuId,
                Quantity = item.Quantity,
                Remark = item.Remark,
            };

            orderItems.Add(orderItem);
        }

        holder.Order.Items = orderItems.ToArray();

        await _orderItemService.AttachOrderItemSkuAsync(holder.Order.Items);

        var skus = holder.Order.Items.Select(x => x.Sku).WhereNotNull().ToArray();

        await _skuService.AttachGoodsAsync(skus);

        await _priceCalculationService.AttachPriceModelAsync(skus, holder.Request.StoreId, holder.Order.StoreUser.Id);

        foreach (var orderItem in holder.Order.Items)
        {
            var sku = orderItem.Sku;
            var goods = sku?.Goods;

            var error = holder.GetOrCreateSkuError(orderItem.SkuId, sku);

            if (sku == null || goods == null)
            {
                error.ErrorList.Add("goods not found");
            }
            else
            {
                if (goods.GoodsType != holder.Order.GoodsType)
                    error.ErrorList.Add("wrong goods type, pls reselect");

                if (sku.MaxAmountInOnePurchase > 0 && orderItem.Quantity > sku.MaxAmountInOnePurchase)
                    error.ErrorList.Add("max amount in one purchase");

                if (sku.MinAmountInOnePurchase > 0 && orderItem.Quantity < sku.MinAmountInOnePurchase)
                    error.ErrorList.Add("min amount in one purchase");

                orderItem.GoodsName = goods.Name;
                orderItem.SkuName = sku.Name;
                orderItem.SkuDescription = sku.Description;
                orderItem.Weight = sku.Weight;
                orderItem.StockDeductStrategy = goods.StockDeductStrategy;

                orderItem.BasePrice = sku.PriceModel.BasePrice;
                if (orderItem.BasePrice < Decimal.Zero)
                {
                    error.ErrorList.Add("结算单价不能小于0");
                }

                //grade price
                orderItem.GradePriceOffset = sku.PriceModel.GradePriceModel?.PriceOffset ?? Decimal.Zero;

                //discount
                if (sku.PriceModel.DiscountModel != null)
                {
                    orderItem.DiscountRate = sku.PriceModel.DiscountModel.DiscountPercentage;
                    if (orderItem.DiscountRate <= 0 || orderItem.DiscountRate >= 1)
                    {
                        error.ErrorList.Add(nameof(sku.PriceModel.DiscountModel.DiscountPercentage));
                    }
                }
            }
        }
    }

    private async Task FinalCheckOrderInformationAsync(PlaceOrderDataHolder holder)
    {
        if (holder.ErrorList.Any())
            throw new UserFriendlyException("common errors");

        if (holder.SkuErrors.Any())
            throw new UserFriendlyException("sku errors");

        if (holder.ShippingErrors.Any())
            throw new UserFriendlyException("shipping errors");

        if (holder.PaymentErrors.Any())
            throw new UserFriendlyException("payment errors");

        if (holder.CouponErrors.Any())
            throw new UserFriendlyException("payment errors");

        var order = holder.Order;

        if (string.IsNullOrWhiteSpace(order.ShippingMethodId))
            throw new UserFriendlyException("shipping method is required");

        if (order.IsDelivery())
        {
            if (string.IsNullOrWhiteSpace(order.UserAddressId))
                throw new UserFriendlyException("pls select shipping address");
        }

        if (string.IsNullOrWhiteSpace(order.PaymentMethodId))
            throw new UserFriendlyException(nameof(order.PaymentMethodId));

        if (order.TotalPrice < decimal.Zero)
            throw new UserFriendlyException("订单金额不能小于0");

        if (!order.Items.Any())
            throw new UserFriendlyException("order item is required");

        if (order.Items.Any(x => x.TotalPrice < decimal.Zero))
            throw new UserFriendlyException("order item price should be greater than zero");

        await Task.CompletedTask;
    }

    private async Task BuildEmptyOrderEntityAsync(PlaceOrderDataHolder holder)
    {
        await Task.CompletedTask;

        var order = new OrderDto
        {
            GoodsType = GoodsTypeEnum.Goods,

            Id = default,
            OrderSn = string.Empty,
            StoreId = holder.Request.StoreId,
            OrderIp = _webHelper.GetCurrentIpAddressOrNull(),
            Remark = holder.Request.Remark,
            PlatformId = holder.Request.PlatformId,
            CreationTime = holder.Now,
            LastModificationTime = default,
            ServiceStartTime = holder.Request.PreferDeliveryStartTime,
            ServiceEndTime = holder.Request.PreferDeliveryEndTime,
            //default status
            OrderStatusId = SalesConstants.OrderStatus.Pending
        };

        holder.Order = order;
    }

    private async Task SetPaymentInfoAsync(PlaceOrderDataHolder holder)
    {
        var order = holder.Order;

        order.PaidTotalPrice = default;
        order.RefundedAmount = default;
        order.PaymentMethodId = holder.Request.PaymentMethodId;
        order.PaymentStatusId = SalesConstants.PaymentStatus.Pending;

        if (order.IsOfflinePayment())
        {
            foreach (var m in order.Items)
            {
                var goods = m.Sku?.Goods;
                if (goods == null)
                    continue;

                if (!goods.PayAfterDeliveredSupported)
                {
                    holder.GetOrCreateSkuError(m.SkuId, m.Sku).ErrorList
                        .Add($"{goods.Name} doesn't support pay after shipped");
                }
            }
        }

        await Task.CompletedTask;
    }

    private async Task SetInvoiceInfoAsync(PlaceOrderDataHolder holder)
    {
        if (string.IsNullOrWhiteSpace(holder.Request.InvoiceId))
            return;

        if (!holder.Order.Store.InvoiceSupported)
        {
            throw new UserFriendlyException("invoice is not supported by current store");
        }

        var invoice = await _userInvoiceService.GetByIdAsync(holder.Request.InvoiceId);
        if (invoice == null)
        {
            throw new UserFriendlyException("user invoice not exist");
        }

        if (invoice.UserId != holder.Order.StoreUser.UserId)
        {
            throw new PermissionRequiredException("invoice data error access");
        }

        holder.Order.UserInvoiceId = invoice.Id;
    }

    private async Task SetCustomerInfoAsync(PlaceOrderDataHolder holder)
    {
        if (holder.Request.UserId <= 0)
            throw new ArgumentNullException(nameof(holder.Request.UserId));

        var user = await _userService.GetByIdAsync(holder.Request.UserId);
        if (user == null)
        {
            holder.ErrorList.Add("user not found");
        }
        else
        {
            if (!user.IsActive)
                holder.ErrorList.Add("user is inactive");
        }

        if (holder.ErrorList.Any())
        {
            throw new UserFriendlyException(nameof(SetCustomerInfoAsync));
        }

        holder.Order.StoreUser = ObjectMapper.Map<StoreUser, StoreUserDto>(user);
        holder.Order.StoreUser.Grade = await _userGradeService.GetGradeByUserIdAsync(holder.Order.StoreUser.Id);

        holder.Order.UserId = holder.Order.StoreUser.Id;
        holder.Order.GradeId = holder.Order.StoreUser.Grade?.Id;
    }

    private async Task SetStoreInformationAsync(PlaceOrderDataHolder holder)
    {
        var store = await _storeService.GetByIdAsync(holder.Request.StoreId);

        if (store == null)
        {
            holder.ErrorList.Add("store is required");
        }
        else
        {
            if (!store.Opening)
            {
                holder.ErrorList.Add("store is closed");
            }
        }

        if (holder.ErrorList.Any())
        {
            throw new UserFriendlyException(nameof(SetStoreInformationAsync));
        }

        holder.Order.Store = store.MapTo<Store, StoreDto>(ObjectMapper);
    }

    private async Task SetExchangePointsAmountAsync(PlaceOrderDataHolder holder)
    {
        await Task.CompletedTask;

        //todo rate
        holder.Order.ExchangePointsAmount = holder.Order.TotalPrice;
    }

    private async Task SetIdsAsync(PlaceOrderDataHolder holder)
    {
        holder.Order.Id = GuidGenerator.CreateGuidString();
        holder.Order.OrderSn = await _orderUtils.GenerateOrderSnAsync();

        if (holder.Order.PickupRecord != null)
        {
            holder.Order.PickupRecord.OrderId = holder.Order.Id;
        }

        if (holder.Order.DeliveryRecord != null)
        {
            holder.Order.DeliveryRecord.OrderId = holder.Order.Id;
        }

        if (holder.Order.OrderCouponRecords != null)
        {
            foreach (var coupon in holder.Order.OrderCouponRecords)
            {
                coupon.Id = GuidGenerator.CreateGuidString();
                coupon.OrderId = holder.Order.Id;
            }
        }

        foreach (var orderItem in holder.Order.Items)
        {
            orderItem.Id = GuidGenerator.CreateGuidString();
            orderItem.OrderId = holder.Order.Id;

            if (orderItem.OrderItemGifts != null)
            {
                foreach (var gift in orderItem.OrderItemGifts)
                {
                    gift.Id = GuidGenerator.CreateGuidString();
                    gift.OrderItemId = orderItem.Id;
                }
            }

            if (orderItem.OrderValueAddedItems != null)
            {
                foreach (var valueAddedItem in orderItem.OrderValueAddedItems)
                {
                    valueAddedItem.Id = GuidGenerator.CreateGuidString();
                    valueAddedItem.OrderItemId = orderItem.Id;
                }
            }
        }
    }

    public async Task SaveOrderToDatabaseAsync(PlaceOrderDataHolder holder)
    {
        //set primary key & bind relation
        await SetIdsAsync(holder);

        await this.FinalCheckOrderInformationAsync(holder);

        //get database object
        var db = await _repository.GetDbContextAsync();

        //order
        db.Set<Order>().Add(holder.Order.MapTo<OrderDto, Order>(ObjectMapper));

        //order items
        db.Set<OrderItem>()
            .AddRangeIfNotEmpty(holder.Order.Items.MapArrayTo<OrderItemDto, OrderItem>(ObjectMapper));

        //coupon records
        db.Set<OrderCouponRecord>()
            .AddRangeIfNotEmpty(
                holder.Order.OrderCouponRecords?.MapArrayTo<OrderCouponRecordDto, OrderCouponRecord>(ObjectMapper));

        //added items
        db.Set<OrderValueAddedItem>().AddRangeIfNotEmpty(
            holder.Order.Items
                .Where(x => x.OrderValueAddedItems != null)
                .SelectMany(x => x.OrderValueAddedItems)
                .MapArrayTo<OrderValueAddedItemDto, OrderValueAddedItem>(ObjectMapper));

        //order item gifts
        db.Set<OrderItemGift>().AddRangeIfNotEmpty(
            holder.Order.Items
                .Where(x => x.OrderItemGifts != null)
                .SelectMany(x => x.OrderItemGifts)
                .MapArrayTo<OrderItemGiftDto, OrderItemGift>(ObjectMapper));

        //flush to db
        await db.TrySaveChangesAsync();
    }

    public async Task<PlaceOrderResult> PrePlaceOrderWithoutSavingAsync(PlaceOrderRequestDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        var settings = await _settingService.GetCachedMallSettingsAsync();
        if (settings.OrderSettings.PlaceOrderDisabled)
            throw new UserFriendlyException("admin disabled placing order");

        var key = $"place.order.user.id:{dto.UserId}";

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: key,
            expiryTime: TimeSpan.FromSeconds(5));

        var result = new PlaceOrderResult();

        var holder = new PlaceOrderDataHolder
        {
            Request = dto,
            Now = Clock.Now
        };

        try
        {
            //check goods info
            if (!CheckAndPreparePlaceOrderRequest(holder.Request, out var goodsErrorMessage))
            {
                throw new UserFriendlyException(goodsErrorMessage);
            }

            //prebuild order entities
            await BuildEmptyOrderEntityAsync(holder);
            await SetCustomerInfoAsync(holder);
            await SetStoreInformationAsync(holder);

            //build order details
            await BuildOrderItemsAsync(holder);
            await BuildOrderValueAddedItemsAsync(holder);

            //check for sales
            await CheckSkuForSaleAsync(holder);

            //build other information
            await SetInvoiceInfoAsync(holder);
            await SetExchangePointsAmountAsync(holder);
            //calculate total
            _priceCalculationService.CalculateAndUpdateOrderPrice(holder.Order);
            await SetPaymentInfoAsync(holder);

            //ignore empty error object
            holder.SkuErrors = holder.SkuErrors.Where(x => x.ErrorList.Any()).ToArray();

            //-----------------------------------------------------------------------
            //set data and return
            result.SetData(holder);

            return result;
        }
        catch (UserFriendlyException e)
        {
            return result.SetError(e.Message);
        }
        catch (Exception e)
        {
            LoggingException(e, holder);
            throw;
        }
    }

    private void LoggingException(Exception e, PlaceOrderDataHolder holder)
    {
        var sb = new StringBuilder();
        sb.AppendLine(e.Message);
        sb.AppendLine("data holder:");
        sb.AppendLine(JsonDataSerializer.SerializeToString(holder));
        var info = sb.ToString();

        Logger.LogError(message: info, exception: e);
    }

    public async Task AfterPlacedOrderAsync(PlaceOrderDataHolder holder)
    {
        await this._orderNoteService.InsertAsync(new OrderNoteDto()
        {
            Note = this.L["order.created.note"],
            OrderId = holder.Order.Id,
            DisplayToUser = true,
            CreationTime = holder.Now,
            OrderNoteExtended = new OrderNoteExtended()
            {
                NoteType = OrderNoteExtendedType.Created
            }
        });

        await _orderStockQuantityAdjustService.DeductOrderInventoryAfterPlaceOrderAsync(holder.Order.Id);

        await _shoppingCartService.DeleteBySkuIdsAsync(new RemoveCartBySpecs
        {
            UserId = holder.Request.UserId,
            SkuId = holder.Order.Items.Select(x => x.SkuId).ToArray()
        });
    }
}