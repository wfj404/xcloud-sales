import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isStringEmpty } from '@/utils/utils';
import { Box, Link, Typography } from '@mui/material';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  if (isStringEmpty(app.mallSettings?.FilingNumber)) {
    return null;
  }

  return (
    <>
      <Box
        sx={{
          py: 4,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Link
          href="https://beian.miit.gov.cn/"
          target="_blank"
          underline="none"
          sx={{
            fontSize: 10,
          }}
        >
          <Typography variant="overline" color={'text.disabled'}>
            {app.mallSettings.FilingNumber}
          </Typography>
        </Link>
      </Box>
    </>
  );
};
