﻿using System;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Application.Repository.Admin;

namespace XCloud.Sales.Application.Extended.Database.Repository.Admin;

[ExposeServices(typeof(IAdminAccountRepository))]
public class AdminAccountRepository : PlatformMySqlRepository<Platform.Application.Domain.Admins.Admin>,
    IAdminAccountRepository, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public AdminAccountRepository(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
}