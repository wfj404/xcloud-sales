import { useOutlet } from 'umi';

import HideFor3H from '@/layouts/sh3h/hide';

export default function () {
  const children = useOutlet();

  return (
    <>
      <HideFor3H>{children}</HideFor3H>
    </>
  );
}
