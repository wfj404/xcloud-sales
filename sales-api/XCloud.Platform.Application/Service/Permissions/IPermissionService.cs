using System.Threading.Tasks;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Permissions;

public interface
    IPermissionService : IPlatformCrudService<Domain.Security.Permission, PermissionDto, QueryPermissionPaging>
{
    //
}

public class PermissionService : PlatformCrudService<Domain.Security.Permission, PermissionDto, QueryPermissionPaging>,
    IPermissionService
{
    public PermissionService(IPlatformRepository<Domain.Security.Permission> repository) : base(repository)
    {
        //
    }

    private async Task<bool> CheckNameIsExistAsync(DbContext db,
        string permissionKey, string exceptIdOrEmpty = null)
    {
        if (string.IsNullOrWhiteSpace(permissionKey))
            throw new ArgumentNullException(nameof(permissionKey));

        var query = db.Set<Domain.Security.Permission>().AsNoTracking();
        if (!string.IsNullOrWhiteSpace(exceptIdOrEmpty))
            query = query.Where(x => x.Id != exceptIdOrEmpty);

        var exist = await query.AnyAsync(x => x.PermissionKey == permissionKey);

        return exist;
    }

    public override async Task<Domain.Security.Permission> InsertAsync(PermissionDto dto)
    {
        var db = await Repository.GetDbContextAsync();

        if (await CheckNameIsExistAsync(db, dto.PermissionKey))
            throw new UserFriendlyException("permission key exist");
        
        return await base.InsertAsync(dto);
    }

    public override async Task<Domain.Security.Permission> UpdateAsync(PermissionDto dto)
    {
        var db = await Repository.GetDbContextAsync();

        if (await CheckNameIsExistAsync(db, dto.PermissionKey, exceptIdOrEmpty: dto.Id))
            throw new UserFriendlyException("permission key exist");
        
        return await base.UpdateAsync(dto);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Domain.Security.Permission entity, PermissionDto dto)
    {
        await Task.CompletedTask;
        entity.Description = dto.Description;
    }
}