using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Sales.Application.Mapping;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Mapping.Warehouses;

public class StockUsageHistoryMap : SalesEntityTypeConfiguration<StockUsageHistory>
{
    public override void Configure(EntityTypeBuilder<StockUsageHistory> builder)
    {
        builder.ToTable("sales_stock_usage_history");
        
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.StoreId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.StockItemId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Quantity);
        builder.Property(x => x.CreationTime);
        
        base.Configure(builder);
    }
}