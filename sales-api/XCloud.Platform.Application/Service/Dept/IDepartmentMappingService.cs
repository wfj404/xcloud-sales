﻿using System.Threading.Tasks;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Admins;
using XCloud.Platform.Application.Domain.Dept;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service.Admins;

namespace XCloud.Platform.Application.Service.Dept;

public interface IDepartmentMappingService : IXCloudAppService
{
    Task<DepartmentDto[]> AttachMemberCountAsync(DepartmentDto[] data);

    Task<AdminDto[]> AttachDepartmentsAsync(AdminDto[] data);

    Task<AdminDto[]> GetDanglingAdminsAsync();

    Task<DepartmentDto[]> GetAdminDepartmentsAsync(string adminId);

    Task<AdminDto[]> GetDepartmentAdminsAsync(string departmentId);

    Task SetAdminDepartmentsAsync(string adminId, string[] departmentIds);

    Task AddToDepartmentAsync(string adminId, string departmentId);

    Task RemoveFromDepartmentAsync(string adminId, string departmentId);
}

public class DepartmentMappingService : XCloudAppService, IDepartmentMappingService
{
    private readonly IPlatformRepository<DepartmentMapping> _repository;
    private readonly IDepartmentService _departmentService;

    public DepartmentMappingService(IPlatformRepository<DepartmentMapping> repository,
        IDepartmentService departmentService)
    {
        _repository = repository;
        _departmentService = departmentService;
    }

    public async Task<DepartmentDto[]> AttachMemberCountAsync(DepartmentDto[] data)
    {
        if (data.Any())
        {
            var ids = data.Ids();

            var db = await this._repository.GetDbContextAsync();

            var query = from mapping in db.Set<DepartmentMapping>().AsNoTracking()
                join admin in db.Set<Admin>().AsNoTracking()
                    on mapping.AdminId equals admin.Id
                select new { mapping, admin };

            query = query.Where(x => ids.Contains(x.mapping.DepartmentId));

            var grouping = await query
                .GroupBy(x => x.mapping.DepartmentId)
                .Select(x => new
                {
                    x.Key,
                    Count = x.Select(d => d.mapping.AdminId).Distinct().Count()
                })
                .ToArrayAsync();

            foreach (var m in data)
            {
                m.MemberCount = grouping.FirstOrDefault(x => x.Key == m.Id)?.Count;
            }
        }

        return data;
    }

    public async Task<AdminDto[]> AttachDepartmentsAsync(AdminDto[] data)
    {
        if (data.Any())
        {
            var ids = data.Ids();

            var db = await this._repository.GetDbContextAsync();

            var query = from mapping in db.Set<DepartmentMapping>().AsNoTracking()
                join dept in db.Set<Department>().AsNoTracking()
                    on mapping.DepartmentId equals dept.Id
                select new { mapping, dept };

            query = query.Where(x => ids.Contains(x.mapping.AdminId));

            var datalist = await query.OrderBy(x => x.mapping.CreationTime).ToArrayAsync();

            foreach (var m in data)
            {
                m.Departments = datalist
                    .Where(x => x.mapping.AdminId == m.Id).Select(x => x.dept)
                    .MapArrayTo<Department, DepartmentDto>(this.ObjectMapper);
            }
        }

        return data;
    }

    public async Task SetAdminDepartmentsAsync(string adminId, string[] departmentIds)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        if (departmentIds == null)
            throw new ArgumentNullException(nameof(departmentIds));

        var mappings = departmentIds.Select(x => new DepartmentMapping()
        {
            AdminId = adminId,
            DepartmentId = x
        }).ToArray();

        var db = await this._repository.GetDbContextAsync();

        var set = db.Set<DepartmentMapping>();

        var entities = await set.Where(x => x.AdminId == adminId).ToArrayAsync();

        var toDelete = entities.NotInBy(mappings, x => x.DepartmentId).ToArray();
        var toInsert = mappings.NotInBy(entities, x => x.DepartmentId).ToArray();

        if (toDelete.Any())
        {
            set.RemoveRange(toDelete);
        }

        if (toInsert.Any())
        {
            var now = this.Clock.Now;
            foreach (var m in toInsert)
            {
                m.Id = this.GuidGenerator.CreateGuidString();
                m.CreationTime = now;
            }

            set.AddRange(toInsert);
        }

        await db.SaveChangesAsync();
    }

    public async Task<DepartmentDto[]> GetAdminDepartmentsAsync(string adminId)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        var db = await this._repository.GetDbContextAsync();

        var query = from mapping in db.Set<DepartmentMapping>().AsNoTracking()
            join dept in db.Set<Department>().AsNoTracking()
                on mapping.DepartmentId equals dept.Id
            select new { mapping, dept };

        query = query.Where(x => x.mapping.AdminId == adminId);

        var datalist = await query
            .OrderBy(x => x.mapping.CreationTime)
            .Select(x => x.dept)
            .TakeUpTo5000()
            .ToArrayAsync();

        return datalist.MapArrayTo<Department, DepartmentDto>(this.ObjectMapper);
    }

    public async Task<AdminDto[]> GetDepartmentAdminsAsync(string departmentId)
    {
        if (string.IsNullOrWhiteSpace(departmentId))
            throw new ArgumentNullException(nameof(departmentId));

        var db = await this._repository.GetDbContextAsync();

        var query = from mapping in db.Set<DepartmentMapping>().AsNoTracking()
            join admin in db.Set<Admin>().AsNoTracking()
                on mapping.AdminId equals admin.Id
            select new { mapping, admin };

        query = query.Where(x => x.mapping.DepartmentId == departmentId);

        var datalist = await query
            .OrderBy(x => x.mapping.CreationTime)
            .Select(x => x.admin)
            .TakeUpTo5000()
            .ToArrayAsync();

        return datalist.MapArrayTo<Admin, AdminDto>(this.ObjectMapper);
    }

    public async Task AddToDepartmentAsync(string adminId, string departmentId)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        if (string.IsNullOrWhiteSpace(departmentId))
            throw new ArgumentNullException(nameof(departmentId));

        var departments = await this.GetAdminDepartmentsAsync(adminId);
        var ids = departments.Ids().Append(departmentId).Distinct().ToArray();

        await this.SetAdminDepartmentsAsync(adminId, ids);
    }

    public async Task RemoveFromDepartmentAsync(string adminId, string departmentId)
    {
        if (string.IsNullOrWhiteSpace(adminId))
            throw new ArgumentNullException(nameof(adminId));

        if (string.IsNullOrWhiteSpace(departmentId))
            throw new ArgumentNullException(nameof(departmentId));

        var departments = await this.GetAdminDepartmentsAsync(adminId);
        var ids = departments.Ids().Where(x => x != departmentId).ToArray();

        await this.SetAdminDepartmentsAsync(adminId, ids);
    }

    public async Task<AdminDto[]> GetDanglingAdminsAsync()
    {
        var db = await this._repository.GetDbContextAsync();

        var query = from mapping in db.Set<DepartmentMapping>().AsNoTracking()
            join dept in db.Set<Department>().AsNoTracking()
                on mapping.DepartmentId equals dept.Id
            select new { mapping };

        var validAdminQuery = query.Select(x => x.mapping.AdminId);

        var adminQuery = db.Set<Admin>().AsNoTracking();

        var datalist = await adminQuery
            .Where(x => !validAdminQuery.Contains(x.Id))
            .OrderBy(x => x.CreationTime)
            .TakeUpTo5000().ToArrayAsync();

        return datalist.MapArrayTo<Admin, AdminDto>(this.ObjectMapper);
    }
}