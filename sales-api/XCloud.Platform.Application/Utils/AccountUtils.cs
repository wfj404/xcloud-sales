﻿using Volo.Abp.DependencyInjection;
using XCloud.Core.Configuration;
using XCloud.Core.Helper;
using XCloud.Core.Security.Hash;
using XCloud.Platform.Application.Domain;

namespace XCloud.Platform.Application.Utils;

/// <summary>
/// 把不同格式的电话号码变成统一格式
/// 方便数据库比对查找
/// </summary>
[ExposeServices(typeof(AccountUtils))]
public class AccountUtils : IScopedDependency
{
    private readonly AppConfig _appConfig;

    public AccountUtils(AppConfig appConfig)
    {
        _appConfig = appConfig;
    }

    public string IdCardIdentity(int cardType, string cardNo) => $"{cardNo}@{cardType}";

    public string EncryptPassword(string raw, string salt = null)
    {
        if (string.IsNullOrWhiteSpace(raw))
            throw new ArgumentNullException(nameof(raw));

        salt ??= string.Empty;

        var data = Md5Helper.Encrypt($"{raw}.{salt}", _appConfig.AppEncoding);

        data = data.Replace("-", string.Empty).Trim().ToUpper();

        return data;
    }

    public string NormalizeMobilePhone(string phone)
    {
        if (string.IsNullOrWhiteSpace(phone))
            throw new ArgumentNullException($"{nameof(NormalizeMobilePhone)},phone:{phone}");

        phone = phone.Replace("-", string.Empty).RemoveWhitespace();
        phone = phone.RemovePreFix("+86");
        phone = phone.Trim();

        return phone;
    }

    public void NormalizeIdentityName(IHasIdentityNameFields entity, string originIdentityName)
    {
        if (string.IsNullOrWhiteSpace(originIdentityName))
            throw new ArgumentNullException(nameof(originIdentityName));

        var normalizedIdentityName = NormalizeIdentityName(originIdentityName);
        
        entity.IdentityName = normalizedIdentityName;
        entity.OriginIdentityName = originIdentityName;
    }

    public string NormalizeIdentityName(string identityName)
    {
        var normalized = identityName.RemoveWhitespace().ToLower();
        return normalized;
    }

    public bool ValidateIdentityName(string name, out string msg)
    {
        msg = string.Empty;

        if (string.IsNullOrWhiteSpace(name))
        {
            msg = "identity name is empty";
            return false;
        }

        var chars = Com.Range('a', 'z').Concat(Com.Range('A', 'Z')).Select(x => (char)x).ToList();
        chars.AddRange(Com.Range(0, 10).Select(x => x.ToString()[0]));
        chars.AddRange(new[] { '_', '-', '@' });

        if (!name.All(x => chars.Contains(x)))
        {
            msg = "用户名包含非法字符";
            return false;
        }

        return true;
    }
}