﻿using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderItemService : ISalesAppService
{
    Task<OrderItemDto> GetByIdAsync(string id);

    Task<OrderItemDto[]> ListByOrderIdAsync(string orderId);

    Task<OrderItemDto[]> AttachOrderItemSkuAsync(OrderItemDto[] data);
}

public class OrderItemService : SalesAppService, IOrderItemService
{
    private readonly ISalesRepository<OrderItem> _repository;

    public OrderItemService(ISalesRepository<OrderItem> repository)
    {
        _repository = repository;
    }

    public async Task<OrderItemDto> GetByIdAsync(string id)
    {
        var item = await this._repository.FindAsync(x => x.Id == id);

        return item?.MapTo<OrderItem, OrderItemDto>(ObjectMapper);
    }

    public async Task<OrderItemDto[]> ListByOrderIdAsync(string orderId)
    {
        if (string.IsNullOrWhiteSpace(orderId))
            throw new ArgumentNullException(nameof(orderId));

        var items = await this._repository.QueryManyAsync(x => x.OrderId == orderId);

        return items.MapArrayTo<OrderItem, OrderItemDto>(ObjectMapper);
    }

    public async Task<OrderItemDto[]> AttachOrderItemSkuAsync(OrderItemDto[] data)
    {
        if (!data.Any())
            return Array.Empty<OrderItemDto>();

        var skuIds = data.Select(x => x.SkuId).WhereNotEmpty().Distinct().ToArray();

        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<Sku>().AsNoTracking().IgnoreQueryFilters();

        query = query.WhereIdIn(skuIds);

        var items = await query.ToArrayAsync();

        foreach (var m in data)
        {
            var sku = items.FirstOrDefault(x => x.Id == m.SkuId);
            if (sku == null)
                continue;

            m.Sku = ObjectMapper.Map<Sku, SkuDto>(sku);
        }

        return data;
    }
}