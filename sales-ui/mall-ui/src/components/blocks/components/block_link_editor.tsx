import { Form, Input } from 'antd';

import XLinkEditor from '@/components/link';

import { BlockLinkText } from '../utils';

export default ({
  data,
  onChange,
}: {
  data: BlockLinkText;
  onChange: (x: BlockLinkText) => void;
}) => {
  if (data == undefined) {
    return <div>data error</div>;
  }

  return (
    <>
      <Form>
        <Form.Item label="文字">
          <Input
            value={data.text}
            onChange={(e) => {
              onChange({
                ...data,
                text: e.target.value,
              });
            }}
          />
        </Form.Item>
        <Form.Item label="文字">
          <XLinkEditor
            link={data.link || {}}
            onChange={(e) => {
              onChange({
                ...data,
                link: e,
              });
            }}
          />
        </Form.Item>
      </Form>
    </>
  );
};
