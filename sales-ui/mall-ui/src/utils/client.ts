import { httpClient } from './http';
import { ActivityLogDto, Api } from './swagger';
import { Api as QingdaoApi } from './swagger_qingdao';

export const apiClient: Api<any> = (() => {
  let c = new Api();
  c.instance = httpClient;
  return c;
})();

export const qingdaoApiClient: QingdaoApi<any> = (() => {
  let c = new QingdaoApi();
  c.instance = httpClient;
  return c;
})();

export const sendActivityLog = async (entity: ActivityLogDto) => {
  if (!entity) {
    return;
  }

  return await apiClient.mall.commonSaveActivityLog({
    ...entity,
    Id: undefined,
  });
};
