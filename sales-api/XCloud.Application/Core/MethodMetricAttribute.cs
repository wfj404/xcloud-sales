﻿using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging;
using Rougamo;
using Rougamo.Context;
using XCloud.Application.Logging;

namespace XCloud.Application.Core;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
public class MethodMetricAttribute : MoAttribute
{
    private ILogger _logger;
    private Stopwatch _stopwatch;

    public override void OnEntry(MethodContext context)
    {
        _logger = StaticLoggerFactoryHolder.LoggerFactory?.CreateLogger(context.Method.Name);

        _stopwatch = new Stopwatch();
        _stopwatch.Start();

        base.OnEntry(context);
    }

    public override void OnExit(MethodContext context)
    {
        if (_logger == null)
        {
            Debug.WriteLine("logger is null");
            return;
        }

        _stopwatch.Stop();

        var className = context.Method.DeclaringType?.Name ?? "--";
        var message = $"{className}:{context.Method.Name}";

        message = message.PadRight(80, '-');
        message = $"{message}--耗时：{_stopwatch.Elapsed.TotalSeconds}秒";

        _logger.LogInformation(message: message);

        base.OnExit(context);
    }

    public override void OnException(MethodContext context)
    {
        _stopwatch?.Stop();
    }
}