﻿using Volo.Abp.Application.Dtos;
using XCloud.Sales.Application.Domain.ShoppingCart;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.ShoppingCart;

public class RemoveCartBySpecs : IEntityDto
{
    public int UserId { get; set; }

    public string[] SkuId { get; set; }
}

public class AddOrUpdateShoppingCartInput : IEntityDto
{
    public int UserId { get; set; }
    public string SkuId { get; set; }
    public int Quantity { get; set; }
    public int[] ValueAddedItemIds { get; set; }
}

public class UpdateShoppingCartQuantityInput : IEntityDto<int>
{
    public int Id { get; set; }

    public int Quantity { get; set; }
}

public class ShoppingCartItemDto : ShoppingCartItem, IEntityDto
{
    public SkuDto Sku { get; set; }

    public SpecItemDto[] Specs { get; set; }

    public ValueAddedItemDto[] ValueAddedItems { get; set; }

    public string[] Waring { get; set; }
}