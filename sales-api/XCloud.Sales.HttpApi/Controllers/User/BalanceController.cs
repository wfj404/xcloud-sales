﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/balance")]
public class BalanceController : ShopBaseController
{
    private readonly IStoreUserBalanceService _userBalanceService;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderService _orderService;
    private readonly IOrderPaymentProcessingService _orderPaymentProcessingService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;

    /// <summary>
    /// 构造器
    /// </summary>
    public BalanceController(
        IOrderService orderService,
        IOrderPaymentBillService orderPaymentBillService,
        IStoreUserBalanceService userBalanceService,
        IOrderPaymentProcessingService orderPaymentProcessingService,
        IOrderAutoProcessService orderAutoProcessService)
    {
        _orderService = orderService;
        _orderPaymentBillService = orderPaymentBillService;
        _userBalanceService = userBalanceService;
        _orderPaymentProcessingService = orderPaymentProcessingService;
        _orderAutoProcessService = orderAutoProcessService;
    }

    [HttpPost("create-order-payment")]
    public async Task<ApiResponse<object>> PayWithBalanceAsync([FromBody] IdDto orderId)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await _orderService.EnsureUserOrderAsync(orderId.Id, loginUser.Id);

        var bill = await _orderPaymentBillService.CreateOrderPayBillAsync(new CreateOrderPayBillInput
            { OrderId = order.Id });

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _userBalanceService.UpdateUserBalanceAsync(new BalanceHistoryDto
        {
            UserId = loginUser.Id,
            Balance = bill.Price,
            ActionType = (int)BalanceActionType.Use,
            Message = "used by order"
        });

        await _orderPaymentBillService.MarkAsPaidAsync(new MarkBillAsPayedInput
        {
            Id = bill.Id,
            PaymentMethod = SalesConstants.PaymentChannel.Balance
        });

        await _orderPaymentProcessingService.TryUpdatePaymentStatusAsync(new IdDto(order.Id));

        await this.RequiredCurrentUnitOfWork.SaveChangesAsync();

        await this._orderAutoProcessService.AutoProcessOrderAsync(order.Id);

        return new ApiResponse<object>();
    }
}