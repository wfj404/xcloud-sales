import XAfterSalesStatus from '@/components/status/aftersale';
import { AfterSalesDto } from '@/utils/swagger';
import { Descriptions } from 'antd';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';

const App = (props: { model: AfterSalesDto }) => {
  const { model } = props;

  return (
    <>
      <Descriptions bordered>
        <Descriptions.Item label='状态'>
          <XAfterSalesStatus model={model} />
        </Descriptions.Item>
        <Descriptions.Item label='售后诉求'>
          {model.RequestedAction || '--'}
        </Descriptions.Item>
        <Descriptions.Item label='退货理由'>
          {model.ReasonForReturn || '--'}
        </Descriptions.Item>
        <Descriptions.Item label='备注'>
          {model.UserComments || '--'}
        </Descriptions.Item>
        <Descriptions.Item label='创建时间'>
          {formatRelativeTimeFromNow(model.CreationTime)}
        </Descriptions.Item>
        <Descriptions.Item label='最后变更时间'>
          {formatRelativeTimeFromNow(model.LastModificationTime)}
        </Descriptions.Item>
      </Descriptions>
    </>
  );
};

export default App;
