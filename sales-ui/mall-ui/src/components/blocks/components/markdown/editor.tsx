import XMarkdownView from '@/components/markdown/editor';
import { isStringEmpty } from '@/utils/utils';

import { MarkdownBlockProps } from './utils';

export default ({ data, onChange }: {
  data: MarkdownBlockProps,
  onChange: (d: MarkdownBlockProps) => void
}) => {

  const markdownText = data?.markdownText;

  if (!markdownText || isStringEmpty(markdownText)) {
    return null;
  }

  return <>
    <XMarkdownView
      value={markdownText}
      onChange={e => {
        onChange && onChange({
          ...data,
          markdownText: e,
        });
      }} />
  </>;
};
