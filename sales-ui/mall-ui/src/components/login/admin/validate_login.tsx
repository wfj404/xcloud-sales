import { useRequest } from 'ahooks';
import { Spin } from 'antd';
import React, { ReactNode } from 'react';
import { useLocation, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isAdminLogin } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { AdminAuthResult } from '@/utils/swagger';
import { hasAccessToken } from '@/utils/utils';

import AdminDisabled from './disabled';
import AdminNotLogin from './not_login';

export default function({ children }: { children: ReactNode }) {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const model = app.adminAuthResult?.Admin || {};

  const query_login_request = useRequest(apiClient.sys.adminAuthAdminAuth, {
    manual: true,
    onSuccess(data, params) {
      const result: AdminAuthResult = data.data || {};
      app.setAdminAuthResult(result);
    },
  });

  React.useEffect(() => {
    if (hasAccessToken()) {
      query_login_request.run();
    } else {
      app.setAdminAuthResult(undefined);
      app.setAdminPermissions(undefined);
    }
  }, [location.pathname]);

  if (!isAdminLogin(model) && query_login_request.loading) {
    return (
      <Spin
        spinning={query_login_request.loading}
        size='large'
        fullscreen
        tip={<p>加载登录信息...</p>}
      />
    );
  }

  if (!isAdminLogin(model)) {
    return (
      <>
        <AdminNotLogin />
      </>
    );
  }

  if (!model.IsActive) {
    return (
      <>
        <AdminDisabled />
      </>
    );
  }

  return <>{children}</>;
}
