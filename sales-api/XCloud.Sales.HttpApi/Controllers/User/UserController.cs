﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/user")]
public class UserController : ShopBaseController
{
    private readonly IStoreUserService _userService;
    private readonly IStoreUserPointService _userPointService;
    private readonly IStoreUserBalanceService _userBalanceService;
    
    public UserController(
        IStoreUserService userService,
        IStoreUserPointService userPointService,
        IStoreUserBalanceService userBalanceService)
    {
        _userBalanceService = userBalanceService;
        _userService = userService;
        _userPointService = userPointService;
    }

    [HttpPost("balance-and-points")]
    public async Task<ApiResponse<StoreUser>> UserBalanceAndPointsAsync()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var user = await _userService.GetByIdAsync(loginUser.Id);

        if (user == null)
            throw new EntityNotFoundException();

        return new ApiResponse<StoreUser>(user);
    }

    [HttpPost("points-history")]
    public async Task<PagedResponse<PointsHistoryDto>> QueryPointsPagingAsync([FromBody] QueryPointsPagingInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();
        dto.UserId = loginUser.Id;

        var response = await _userPointService.QueryPagingAsync(dto);

        return response;
    }

    [HttpPost("balance-history")]
    public async Task<PagedResponse<BalanceHistoryDto>> QueryBalancePagingAsync([FromBody] QueryBalancePagingInput dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();
        dto.UserId = loginUser.Id;

        var response = await _userBalanceService.QueryPagingAsync(dto);

        return response;
    }
}