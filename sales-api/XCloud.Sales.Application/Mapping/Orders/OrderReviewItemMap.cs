﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Mapping.Orders;

public class OrderReviewItemMap : SalesEntityTypeConfiguration<OrderReviewItem>
{
    public override void Configure(EntityTypeBuilder<OrderReviewItem> builder)
    {
        builder.ToTable("sales_order_review_item");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.ReviewId).RequiredId();
        builder.Property(x => x.OrderItemId).RequiredId();
        builder.Property(x => x.Rating);
        builder.Property(x => x.ReviewText).HasMaxLength(1000);
        builder.Property(x => x.PictureMetaIdsJson);

        base.Configure(builder);
    }
}