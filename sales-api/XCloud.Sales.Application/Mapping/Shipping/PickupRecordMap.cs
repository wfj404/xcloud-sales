using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Mapping.Shipping;

public class PickupRecordMap : SalesEntityTypeConfiguration<PickupRecord>
{
    public override void Configure(EntityTypeBuilder<PickupRecord> builder)
    {
        builder.ToTable("sales_pickup_record");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.OrderId).RequiredId();
        builder.Property(x => x.Prepared);
        builder.Property(x => x.PreparedTime);
        builder.Property(x => x.ReservationApproved);
        builder.Property(x => x.ReservationApprovedTime);
        builder.Property(x => x.PreferPickupEndTime);
        builder.Property(x => x.PreferPickupStartTime);
        builder.Property(x => x.Picked);
        builder.Property(x => x.PickedTime);
        builder.Property(x => x.SecurityCode).HasMaxLength(100);

        base.Configure(builder);
    }
}