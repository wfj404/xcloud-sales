using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;

namespace XCloud.Platform.Application.Configuration;

public static class PlatformOptionExtension
{
    [NotNull]
    public static PlatformOption GetPlatformOptionOrDefault(this IConfiguration configuration)
    {
        var option = new PlatformOption();

        var section = configuration.GetSection("Platform");

        if (section.Exists())
        {
            section.Bind(option);
        }

        return option;
    }
}