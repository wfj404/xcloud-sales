import { useState } from 'react';

import XEmpty from '@/components/common/empty';
import XItem from '@/components/goods/card/box';
import XDetailPreview from '@/components/goods/detail/detailPreview';
import { GoodsDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box, Grid } from '@mui/material';

export default ({ goods }: {
  goods: GoodsDto[];
}) => {
  const [detailId, _detailId] = useState<string | undefined>(undefined);

  if (isArrayEmpty(goods)) {
    return <XEmpty />;
  }

  return (
    <>
      <XDetailPreview
        detailId={detailId}
        onClose={() => {
          _detailId('');
        }}
      />

      <Grid
        container
        spacing={{
          xs: 1,
          sm: 1,
          md: 2,
        }}
      >
        {goods?.map((x, index) => (
          <Grid item xs={6} sm={4} md={4} key={index}>
            <Box onClick={() => {
              x.Id && _detailId(x.Id);
            }}
            >
              <XItem
                model={x}
                count={1}
              />
            </Box>
          </Grid>
        ))}
      </Grid>
    </>
  );
};
