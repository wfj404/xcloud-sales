﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Payments;

namespace XCloud.Sales.Application.Mapping.Payments;

public class PaymentBillMap : SalesEntityTypeConfiguration<PaymentBill>
{
    public override void Configure(EntityTypeBuilder<PaymentBill> builder)
    {
        builder.ToTable("sales_payment_bill");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.Description).HasMaxLength(500);

        builder.Property(x => x.OrderId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Price).HasPrecision(18, 2);

        builder.Property(x => x.PaymentChannel).RequiredId();

        builder.Property(x => x.Paid);
        builder.Property(x => x.PaymentTransactionId).HasMaxLength(200);
        builder.Property(x => x.PayTime);
        builder.Property(x => x.NotifyData);

        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}