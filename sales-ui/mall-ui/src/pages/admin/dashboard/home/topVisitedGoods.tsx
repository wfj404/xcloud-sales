import { useRequest } from 'ahooks';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { dateTimeFormat, myDayjs } from '@/utils/dayjs';
import {
  QueryGoodsVisitReportInput,
  QueryGoodsVisitReportResponse,
} from '@/utils/swagger';

import XIndexNo from '../components/indexNo';

export default () => {
  const [finalQuery, _finalQuery] = useState<QueryGoodsVisitReportInput>({});

  const request = useRequest(apiClient.mallAdmin.reportTopVisitedGoods, {
    manual: true,
  });

  const data = request.data?.data?.Data || [];

  useEffect(() => {
    let now = myDayjs.utc();
    let monthAgo = now.add(-1, 'month');
    const p: QueryGoodsVisitReportInput = {
      StartTime: monthAgo.format(dateTimeFormat),
      EndTime: now.format(dateTimeFormat),
      Count: 20,
    };
    _finalQuery(p);
    request.run(p);
  }, []);

  const columns: ColumnProps<QueryGoodsVisitReportResponse>[] = [
    {
      render: (x, record, index) => <XIndexNo index={index} />,
    },
    {
      title: '商品',
      render: (d, x) => {
        const { Goods } = x;
        return Goods?.Name || '--';
      },
    },
    {
      title: '数量',
      render: (d, x) => x.VisitedCount,
    },
  ];

  return (
    <>
      <Card
        title="浏览最多的商品"
        extra={<Button type="link">更多</Button>}
        loading={request.loading}
        style={{ marginBottom: 10 }}
      >
        <Table columns={columns} dataSource={data} pagination={false} />
      </Card>
    </>
  );
};
