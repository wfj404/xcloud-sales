import { useRequest } from 'ahooks';
import { ReactNode, useEffect } from 'react';
import { useSnapshot } from 'umi';

import XLoading from '@/components/common/loading';
import { storeState, StoreType } from '@/store';
import { apiClient } from '@/utils/client';

export default ({ children }: { children: ReactNode }) => {
  const app = useSnapshot(storeState) as StoreType;

  const load_settings_request = useRequest(
    apiClient.mall.settingGetMallSettings,
    {
      manual: true,
      onSuccess(data, params) {
        app.setMallSettings(data.data.Data || {});
      },
    },
  );

  useEffect(() => {
    load_settings_request.run();
  }, []);

  if (load_settings_request.loading) {
    return (
      <>
        <XLoading />
      </>
    );
  }

  return <>{children}</>;
};
