using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Dto;

namespace XCloud.Sales.Application.Extension;

public static class SettingExtension
{
    public static async Task<MallSettingsDto> GetCachedMallSettingsAsync(this ISettingService settingService)
    {
        return await settingService.GetSettingsAsync<MallSettingsDto>(new CacheStrategy { Cache = true });
    }
}