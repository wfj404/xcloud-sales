import { Space } from 'antd';
import { SegmentedLabeledOption } from 'antd/es/segmented';
import { isArray, isObject, isString, toPairs } from 'lodash-es';

import {
  OrderStatus,
  PaymentStatus,
  ShippingMethod,
  ShippingStatus,
} from '@/utils/biz';
import { QueryOrderPagingInput } from '@/utils/swagger';
import { isArrayEmpty, isStringEmpty } from '@/utils/utils';
import {
  CarOutlined,
  CheckOutlined,
  CloseOutlined,
  HomeOutlined,
  MehOutlined,
  PayCircleOutlined,
  ShopOutlined,
  StarTwoTone,
} from '@ant-design/icons';

export type OrderFilterPresetOption<T> = {
  filter: QueryOrderPagingInput;
  hide?: boolean;
} & SegmentedLabeledOption<T>;

//this function is used to normalize query parameter
export const order_filter_finger_print = (q: QueryOrderPagingInput) => {
  const filter: QueryOrderPagingInput = {
    ...q,
    Sn: undefined,
    StartTime: undefined,
    EndTime: undefined,
    Page: undefined,
    PageSize: undefined,
    SkipCalculateTotalCount: undefined,
  };

  const get_string = (obj: any, index: number): string | null => {
    if (index > 8) {
      return null;
    }

    ++index;

    if (obj == undefined || obj == null) {
      return null;
    }

    if (isString(obj)) {
      return obj;
    }

    if (isArray(obj)) {
      const arr = obj as any[];
      if (isArrayEmpty(arr)) {
        return null;
      }
      return arr
        .map((x) => get_string(x, index))
        .filter((x) => !isStringEmpty(x))
        .sort((a, b) => a?.localeCompare(b || '') || 0)
        .join(',');
    }

    if (isObject(obj)) {
      return toPairs(obj)
        .map<{ key: string; value: string }>((x) => ({
          key: x[0],
          value: get_string(x[1], index) || '',
        }))
        .filter((x) => !isStringEmpty(x.value))
        .sort((a, b) => a.key.localeCompare(b.key))
        .map((x) => `${x.key}=${x.value}`)
        .join('&');
    }

    return obj.toString();
  };

  return get_string(filter, 0);
};

//['全部订单', '待付款', '待配送', '待自提', '已完成', '已关闭']
export const order_preset_filters: OrderFilterPresetOption<string>[] = [
  {
    value: 'process-required',
    label: (
      <Space direction="horizontal" split={<b>&</b>}>
        <span>待配送</span>
        <span>待自提</span>
      </Space>
    ),
    icon: <StarTwoTone twoToneColor="orange" />,
    filter: {
      ToService: true,
      ShippingMethodId: [ShippingMethod.Delivery, ShippingMethod.Pickup],
      DeliveryStatus: [ShippingStatus.NotShipping, ShippingStatus.Shipping],
    },
  },
  {
    value: 'shipping-required',
    label: '待配送',
    icon: <CarOutlined />,
    filter: {
      ToService: true,
      ShippingMethodId: [ShippingMethod.Delivery],
      DeliveryStatus: [ShippingStatus.NotShipping, ShippingStatus.Shipping],
    },
  },
  {
    value: 'pickup-required',
    label: '待自提',
    icon: <ShopOutlined />,
    filter: {
      ToService: true,
      ShippingMethodId: [ShippingMethod.Pickup],
      DeliveryStatus: [ShippingStatus.NotShipping, ShippingStatus.Shipping],
    },
  },
  {
    value: 'payment-required',
    label: '待付款',
    icon: <PayCircleOutlined />,
    filter: {
      PaymentStatus: [PaymentStatus.Pending],
      Status: [OrderStatus.Pending, OrderStatus.Processing],
    },
  },
  {
    value: 'finished',
    label: '已完成',
    icon: <CheckOutlined />,
    filter: {
      Status: [OrderStatus.Finished],
    },
    hide: true,
  },
  {
    value: 'closed',
    label: '已关闭',
    icon: <CloseOutlined />,
    filter: {
      Status: [OrderStatus.Closed],
    },
    hide: true,
  },
  {
    value: 'aftersales',
    label: '售后中',
    icon: <MehOutlined />,
    filter: {
      IsAfterSales: true,
      IsAfterSalesPending: true,
    },
  },
  {
    value: 'all',
    label: '全部订单',
    icon: <HomeOutlined />,
    filter: {
      //let this empty
    },
  },
];
