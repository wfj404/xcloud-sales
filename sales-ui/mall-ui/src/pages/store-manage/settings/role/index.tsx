import XTime from '@/components/common/time';
import { apiClient } from '@/utils/client';
import { PagedRequest, StoreRoleDto } from '@/utils/swagger';
import { Button, Card, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import XForm from './form';
import XPermission from './permission';
import { handleResponse } from '@/utils/utils';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<StoreRoleDto[]>([]);
  const [query, _query] = useState<PagedRequest>({
    Page: 1,
  });

  const [formData, _formData] = useState<StoreRoleDto | undefined>(undefined);

  const queryList = () => {
    _loading(true);
    apiClient.mallManager.storeManagerPermissionListRoles({})
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnProps<StoreRoleDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name,
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      title: '权限',
      render: (d, x) => (
        <XPermission
          model={x}
          ok={() => {
            queryList();
          }}
        />
      ),
    },
    {
      title: '时间',
      render: (x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type='link'
              onClick={() => {
                _formData(x);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, [query]);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title='角色'
        extra={
          <Button
            type='primary'
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Card>
    </>
  );
};
