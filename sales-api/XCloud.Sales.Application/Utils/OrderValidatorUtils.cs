using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Orders.Validator;

namespace XCloud.Sales.Application.Utils;

[ExposeServices(typeof(OrderValidatorUtils))]
public class OrderValidatorUtils : SalesAppService
{
    private IReadOnlyCollection<IOrderValidator> ConditionValidators { get; }

    public OrderValidatorUtils(IServiceProvider serviceProvider)
    {
        //resolve implements
        ConditionValidators = serviceProvider
            .GetServices<IOrderValidator>()
            .OrderByDescending(x => x.Order)
            .ToArray();
    }

    public async IAsyncEnumerable<OrderConditionValidationResponse> ValidateOrderConditionsAsync(
        OrderConditionCheckDto dto,
        OrderValidatorRule[] conditions)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (dto.Order == null)
            throw new ArgumentNullException(nameof(dto.Order));

        foreach (var condition in conditions)
        {
            foreach (var m in ConditionValidators)
            {
                if (await m.IsConditionTypeSupportedAsync(condition))
                {
                    var result = await m.IsConditionMatchedAsync(dto, condition);
                    yield return result;
                }
            }
        }
    }
}