﻿using Hangfire;
using XCloud.Sales.Application.Job.Jobs;

namespace XCloud.Sales.Application.Job;

public static class JobConfigurationExtension
{
    internal static async Task StartSalesJobsAsync(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var recurringJobManager = s.ServiceProvider.GetRequiredService<IRecurringJobManager>();

        recurringJobManager.AddOrUpdate<OrderJobs>("close-dead-order", x => x.CloseDeadOrdersAsync(),
            Cron.Hourly());

        recurringJobManager.AddOrUpdate<CategoryJobs>("category-try-fix-tree",
            x => x.TryFixCategoryTreeAsync(), Cron.Daily());

        recurringJobManager.AddOrUpdate<LoggingJobs>("trigger-clean-expired-logs",
            x => x.TriggerCleanExpiredLogsAsync(),
            Cron.Daily());

        recurringJobManager.AddOrUpdate<PaymentBillJobs>("delete-unpaid-expired-bills",
            x => x.DeleteUnpaidExpiredBillsAsync(),
            Cron.Daily());

        recurringJobManager.AddOrUpdate<ExternalSkuJobs>("delete-expired-external-skus",
            x => x.DeleteExpiredAsync(),
            Cron.Daily);

        await Task.CompletedTask;
    }
}