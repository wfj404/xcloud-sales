﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.AfterSale;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Orders;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/aftersale")]
public class AfterSaleController : ShopBaseController
{
    private readonly IOrderService _orderService;
    private readonly IOrderItemService _orderItemService;
    private readonly IAfterSaleService _afterSaleService;
    private readonly IAfterSaleProcessingService _afterSaleProcessingService;
    private readonly IAfterSaleCommentService _afterSaleCommentService;
    private readonly ISkuService _skuService;
    private readonly IGoodsPictureService _goodsPictureService;

    public AfterSaleController(IOrderService orderService,
        IAfterSaleService afterSaleService,
        IAfterSaleCommentService afterSaleCommentService,
        IAfterSaleProcessingService afterSaleProcessingService, ISkuService skuService,
        IGoodsPictureService goodsPictureService, IOrderItemService orderItemService)
    {
        _afterSaleService = afterSaleService;
        _orderService = orderService;
        _afterSaleCommentService = afterSaleCommentService;
        _afterSaleProcessingService = afterSaleProcessingService;
        _skuService = skuService;
        _goodsPictureService = goodsPictureService;
        _orderItemService = orderItemService;
    }

    [HttpPost("add-comment")]
    public async Task<ApiResponse<object>> InsertCommentAsync([FromBody] AfterSalesCommentDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var aftersale = await this._afterSaleService.EnsureUserAfterSalesAsync(dto.AfterSaleId, loginUser.Id);

        dto.IsAdmin = false;

        await _afterSaleCommentService.InsertAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("comment-paging")]
    public async Task<PagedResponse<AfterSalesCommentDto>> QueryCommentPagingAsync(
        [FromBody] QueryAfterSalesCommentPaging dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var aftersale = await this._afterSaleService.EnsureUserAfterSalesAsync(dto.AfterSalesId, loginUser.Id);

        dto.SkipCalculateTotalCount = true;
        dto.PageSize = 10;

        var response = await _afterSaleCommentService.QueryPagingAsync(dto);

        return response;
    }

    [HttpPost("pending-count")]
    public async Task<ApiResponse<int>> AfterSalePendingCountAsync()
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var count = await _afterSaleService.QueryPendingCountAsync(new QueryAfterSalePendingCountInput
            { UserId = loginUser.Id });

        return new ApiResponse<int>(count);
    }

    [HttpPost("cancel")]
    public async Task<ApiResponse<object>> CancelAsync([FromBody] CancelAfterSaleDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var aftersale = await this._afterSaleService.EnsureUserAfterSalesAsync(dto.Id, loginUser.Id);

        await _afterSaleProcessingService.CancelAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("create")]
    public async Task<ApiResponse<AfterSalesDto>> CreateAfterSalesAsync([FromBody] AfterSalesDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        await this._orderService.EnsureUserOrderAsync(dto.OrderId, loginUser.Id);

        var response = await _afterSaleProcessingService.CreateAsync(dto);

        return response;
    }

    [NonAction]
    private async Task AttachDataAsync(AfterSalesDto[] data)
    {
        if (!data.Any())
            return;

        await _afterSaleService.AttachOrderAsync(data);
        await _afterSaleService.AttachItemsAsync(data);

        var items = data.SelectMany(x => x.Items).ToArray();

        if (!items.Any())
            return;

        await _afterSaleService.AttachOrderItemsAsync(items);

        var orderItems = items.Select(x => x.OrderItem).WhereNotNull().ToArray();

        if (!orderItems.Any())
            return;

        await _orderItemService.AttachOrderItemSkuAsync(orderItems);
        var skus = orderItems.Select(x => x.Sku).WhereNotNull().ToArray();
        await _skuService.AttachGoodsAsync(skus);

        var goods = skus.Select(x => x.Goods).WhereNotNull().ToArray();

        if (!goods.Any())
            return;

        await _goodsPictureService.AttachImagesAsync(goods);
    }

    [HttpPost("get-by-order-id")]
    public async Task<ApiResponse<AfterSalesDto>> GetByOrderIdAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var order = await this._orderService.EnsureUserOrderAsync(dto.Id, loginUser.Id);

        var afterSalesDto = await _afterSaleService.QueryByOrderIdAsync(order.Id);

        if (afterSalesDto == null)
            throw new EntityNotFoundException();

        await AttachDataAsync(new[] { afterSalesDto });

        return new ApiResponse<AfterSalesDto>(afterSalesDto);
    }

    [HttpPost("by-id")]
    public async Task<ApiResponse<AfterSalesDto>> QueryByIdAsync([FromBody] IdDto dto)
    {
        var loginUser = await StoreUserAuthService.GetRequiredStoreUserAsync();

        var aftersale = await this._afterSaleService.EnsureUserAfterSalesAsync(dto.Id, loginUser.Id);

        await AttachDataAsync(new[] { aftersale });

        return new ApiResponse<AfterSalesDto>(aftersale);
    }
}