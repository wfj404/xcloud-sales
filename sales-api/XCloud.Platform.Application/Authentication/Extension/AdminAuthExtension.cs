﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Authorization;
using XCloud.Application.Extension;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Authentication.Authorization;
using XCloud.Platform.Application.Service.Admins;

namespace XCloud.Platform.Application.Authentication.Extension;

public static class AdminAuthExtension
{
    [NotNull]
    [ItemNotNull]
    public static async Task<AdminDto> GetRequiredAuthedAdminAsync(this IAdminAuthService adminAuthService)
    {
        var res = await adminAuthService.GetAuthAdminAsync();

        res.ThrowIfErrorOccured(x => new AbpAuthorizationException(x.Message ?? nameof(GetRequiredAuthedAdminAsync)));

        var admin = res.Admin;

        if (admin == null || admin.IsEmptyId())
            throw new AbpAuthorizationException(message: nameof(admin));

        if (!admin.IsActive)
            throw new AbpAuthorizationException(message: nameof(admin.IsActive));

        return admin;
    }

    public static bool IsEmpty(this AdminPermissionRequirement dto)
    {
        IEnumerable<bool> HasRequirements()
        {
            yield return ValidateHelper.IsNotEmptyCollection(dto.Permissions);
        }

        return HasRequirements().Any(x => x == true);
    }
}