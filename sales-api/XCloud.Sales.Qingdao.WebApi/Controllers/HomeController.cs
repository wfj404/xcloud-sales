using Microsoft.AspNetCore.Mvc;
using XCloud.Sales.Application.Framework;

namespace XCloud.Sales.Qingdao.WebApi.Controllers;

//[Route("[controller]/[action]")]
public class HomeController : ShopBaseController
{
    [HttpGet]
    public ActionResult Index() => View();

    [HttpGet]
    public ActionResult Swagger() => Redirect("/swagger/index.html");
}