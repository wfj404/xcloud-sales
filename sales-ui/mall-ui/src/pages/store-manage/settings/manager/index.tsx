import { Badge, Button, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XUserAvatar from '@/components/manage/user/avatar';
import { apiClient } from '@/utils/client';
import { StoreManagerDto } from '@/utils/swagger';

import XForm from './form';
import XRole from './roles';

export default () => {
  const [loading, _loading] = useState<boolean>(false);
  const [data, _data] = useState<StoreManagerDto[]>([]);

  const [formData, _formData] = useState<StoreManagerDto | undefined>(
    undefined,
  );

  const queryList = () => {
    _loading(true);
    apiClient.mallManager
      .storeManagerListStoreManager({})
      .then((res) => {
        _data(res.data.Data || []);
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ColumnType<StoreManagerDto>[] = [
    {
      title: '成员',
      render: (x: StoreManagerDto) => {
        return <XUserAvatar model={x.User} />;
      },
    },
    {
      title: '角色',
      render: (d, x) => {
        return (
          <div>
            <XRole
              model={x}
              ok={() => {
                queryList();
              }}
            />
          </div>
        );
      },
    },
    {
      title: '是否激活',
      render: (x: StoreManagerDto) =>
        x.IsActive ? (
          <Badge status={'success'} text={'激活'} />
        ) : (
          <Badge status={'error'} text={'未激活'} />
        ),
    },
    {
      title: '超级管理员',
      render: (d, x) => (
        <Badge
          text={x.IsSuperManager ? '超级管理员' : '普通成员'}
          status={x.IsSuperManager ? 'success' : 'default'}
        />
      ),
    },
    {
      title: '时间',
      render: (x: StoreManagerDto) => <XTime model={x} />,
    },
    {
      title: '操作',
      render: (x: StoreManagerDto) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(x);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => {
          _formData(undefined);
        }}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Table
        rowKey={(x) => x.Id || ''}
        loading={loading}
        columns={columns}
        dataSource={data}
        pagination={false}
      />
    </>
  );
};
