using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class SpecValueMap : SalesEntityTypeConfiguration<SpecValue>
{
    public override void Configure(EntityTypeBuilder<SpecValue> builder)
    {
        builder.ToTable("sales_spec_value");

        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.SpecId).IsRequired();
        builder.Property(x => x.Name).IsRequired().HasMaxLength(400);
        builder.Property(x => x.PriceOffset).HasPrecision(18, 2);
        builder.Property(x => x.AssociatedSkuId).OptionalId();
        builder.Property(x => x.AssociatedSkuCount);
        builder.Property(x => x.IsDeleted);

        base.Configure(builder);
    }
}