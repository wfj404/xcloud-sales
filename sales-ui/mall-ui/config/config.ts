// https://umijs.org/config/
import { defineConfig } from 'umi';

import routes from './routes';

const APP_PUBLIC_PATH = process.env.public_path || '/sales/';
const API_ADDRESS_FROM_ENV = process.env.api_gateway;

console.log('API网关地址', API_ADDRESS_FROM_ENV || '默认值');

export default defineConfig({
  hash: true,
  base: APP_PUBLIC_PATH,
  publicPath: APP_PUBLIC_PATH,
  routes: routes,
  model: false,
  manifest: {
    basePath: '/',
  },
  define: {
    'process.env.UMI_ENV': process.env.UMI_ENV,
    'process.env.app_dev': true,
    'process.env.app_base': APP_PUBLIC_PATH,
    'process.env.api_gateway': API_ADDRESS_FROM_ENV,
    'process.env.app_domain': process.env.app_domain,
    'process.env.external_token_key': process.env.external_token_key,
    'process.env.external_3h_login_url': process.env.external_3h_login_url,
    'process.env.ws_endpoint': process.env.ws_endpoint,
  },
  proxy: {
    '/api': {
      target: 'http://localhost:6001',
      changeOrigin: true,
    },
  },
  headScripts: [
    {
      // 解决首次加载时白屏的问题
      src: '/sales/scripts/loading.js',
      async: true,
    },
  ],
  antd: false,
  request: false,
  esbuildMinifyIIFE: true,
  valtio: {},
  //tailwindcss: {},
  deadCode: {},
});
