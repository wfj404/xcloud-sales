﻿using Volo.Abp;

namespace XCloud.Elasticsearch.Core;

public class EsResponseException : BusinessException
{
    public EsResponseException(string msg, System.Exception inner = null) : base(message: msg, innerException: inner)
    {
        //
    }
}

public class EsBulkException : BusinessException
{
    public EsBulkException(string msg) : base(msg)
    {
        //
    }
}