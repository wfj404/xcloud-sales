import 'react-markdown-editor-lite/lib/index.css';

import { message } from 'antd';
import React, { useState } from 'react';
import ReactMarkdown from 'react-markdown';
import MdEditor from 'react-markdown-editor-lite';

import DotsLoading from '@/components/common/loading/dots';
import { config } from '@/utils/config';
import { resolveUrl } from '@/utils/storage';
import { compressImageV2, uploadFileV2 } from '@/utils/upload_file';

export default (props: {
  onChange: (x: string) => void;
  value: string;
  style?: React.CSSProperties;
}) => {
  const { onChange, value, style } = props;
  const [uploading, _uploading] = useState(false);

  return (
    <>
      {uploading && <DotsLoading />}
      <MdEditor
        style={{ height: '500px', ...(style || {}) }}
        value={value}
        renderHTML={(text) => (
          <ReactMarkdown remarkPlugins={[]}>{text}</ReactMarkdown>
        )}
        onChange={({ text, html }) => {
          onChange && onChange(text);
        }}
        allowPasteImage
        onImageUpload={async (file: File) => {
          try {
            _uploading(true);
            file = await compressImageV2(file, config.upload.maxSize);
            if (file.size > config.upload.maxSize) {
              message.error('图片大小不能超过1MB');
              throw new Error('图片大小不能超过1MB');
            }

            let response = await uploadFileV2(file);

            let url = resolveUrl(response, {
              width: 500,
              //height: 500,
            });
            if (url) {
              return url;
            } else {
              message.error('上传失败');
              throw new Error('上传失败');
            }
          } finally {
            _uploading(false);
          }
        }}
      />
    </>
  );
};
