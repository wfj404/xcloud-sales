import { Button, Card, message, Space, Switch, Table, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XRenderTime from '@/components/common/time/render_time';
import UserAvatar from '@/components/manage/user/avatar';
import { isGiftCardExpired, PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { GiftCardDto, QueryGiftCardPagingInput } from '@/utils/swagger';
import { formatMoney, handleResponse } from '@/utils/utils';

import XCard from './card';
import XForm from './form';
import XSearchForm from './search_form';

export default () => {
  const [loading, _loading] = useState(true);
  const [expandedKey, _expandedKey] = useState<string | undefined | null>(
    undefined,
  );
  const [data, _data] = useState<PagedResponse<GiftCardDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QueryGiftCardPagingInput>({
    Page: 1,
  });

  const [formData, _formData] = useState<GiftCardDto | undefined>(undefined);

  const [loadingId, _loadingId] = useState<string | undefined | null>(
    undefined,
  );

  const queryList = (q: QueryGiftCardPagingInput) => {
    _loading(true);
    apiClient.mallAdmin
      .giftCardQueryPaging(q)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data || {});
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const deleteRow = (row: GiftCardDto) => {
    if (!confirm('删除充值卡？')) {
      return;
    }
    _loadingId(row.Id || '');
    apiClient.mallAdmin
      .giftCardUpdateStatus({
        Id: row.Id,
        IsDeleted: true,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          queryList(query);
        }
      })
      .finally(() => {
        _loadingId('');
      });
  };

  const columns: ColumnProps<GiftCardDto>[] = [
    {
      title: '金额',
      render: (d, x) => <Tag color="warning">{formatMoney(x.Amount || 0)}</Tag>,
    },
    {
      title: '截至日期',
      render: (d, x) => {
        return (
          <>
            <Space direction="vertical">
              <XRenderTime timeStr={x.EndTime} />
              {isGiftCardExpired(x) && <Tag color="red">已过期</Tag>}
            </Space>
          </>
        );
      },
    },
    {
      title: '使用人',
      render: (d, x) => {
        if ((x.UserId || 0) <= 0) {
          return <Tag color={'success'}>未使用</Tag>;
        }

        return <UserAvatar model={x.StoreUser?.User} />;
      },
    },
    {
      title: '激活状态',
      render: (d, x) => {
        if (x.Used) {
          return null;
        }
        return (
          <>
            <Switch
              loading={loadingId == x.Id}
              checked={x.IsActive}
              onChange={(e) => {
                if (!confirm(`确定修改状态为：${e ? '激活' : '冻结'}`)) {
                  return;
                }
                _loadingId(x.Id);
                apiClient.mallAdmin
                  .giftCardUpdateStatus({
                    Id: x.Id,
                    IsActive: e,
                  })
                  .then((res) => {
                    handleResponse(res, () => {
                      queryList(query);
                    });
                  })
                  .finally(() => _loadingId(undefined));
              }}
            />
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (text, record) => {
        if (record.Used) {
          return null;
        }

        return (
          <Button.Group>
            <Button
              loading={loadingId == record.Id}
              type="link"
              danger
              onClick={() => {
                deleteRow(record);
              }}
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList(query);
        }}
      />
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card
        title="充值卡"
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          expandable={{
            expandedRowRender: (x) => (
              <div style={{ padding: 10, border: '5px dashed orange' }}>
                <XCard model={x} />
              </div>
            ),
            expandedRowKeys: expandedKey ? [expandedKey] : [],
            onExpand(expanded, record) {
              if (expanded) {
                _expandedKey(record.Id);
              } else {
                _expandedKey(undefined);
              }
            },
          }}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              const q: QueryGiftCardPagingInput = {
                ...query,
                Page: e,
              };
              _query(q);
              queryList(q);
            },
          }}
        />
      </Card>
    </>
  );
};
