import { ErrorBlock, Tabs } from 'antd-mobile';
import { Dayjs } from 'dayjs';
import { groupBy } from 'lodash-es';

import {
  dateFormat,
  hourMinuteFormat,
  myDayjs,
  parseAsDayjs,
  timezoneOffset,
  weekFormat,
} from '@/utils/dayjs';
import { AvailableServiceTimeRangeDto, TimeRangeDto } from '@/utils/swagger';
import { Box, Button, Typography } from '@mui/material';

interface TimeRangeExtendedDto extends TimeRangeDto {
  start?: Dayjs;
  end?: Dayjs;
}

export default ({
  datalist,
  onSelect,
}: {
  datalist: AvailableServiceTimeRangeDto[];
  onSelect: (start: Dayjs, end: Dayjs) => void;
}) => {
  const all_time_ranges = datalist
    .flatMap((x) => x.TimeRanges || [])
    .map<TimeRangeExtendedDto>((x) => ({
      ...x,
      start:
        parseAsDayjs(x.StartTime)?.add(timezoneOffset, 'hour') || undefined,
      end: parseAsDayjs(x.EndTime)?.add(timezoneOffset, 'hour') || undefined,
    }));

  const invalid_date = myDayjs().add(1000, 'year');

  const grouped_date = groupBy(all_time_ranges, (x) =>
    (x.start || invalid_date).format(dateFormat),
  );

  const sorted_dates = Object.keys(grouped_date).sort(
    (a, b) => (parseAsDayjs(a)?.unix() || 0) - (parseAsDayjs(b)?.unix() || 0),
  );

  const renderButton = (x: TimeRangeExtendedDto) => {
    if (x.start && x.end) {
      return (
        <Box sx={{ mb: 1 }}>
          <Button
            variant="outlined"
            fullWidth
            onClick={() => {
              x.start && x.end && onSelect(x.start, x.end);
            }}
          >
            {x.start.format(hourMinuteFormat)} ~{' '}
            {x.end.format(hourMinuteFormat)}
          </Button>
        </Box>
      );
    }

    return null;
  };

  const renderDateGroup = (x: string) => {
    const group = grouped_date[x];
    if (!group || group.length <= 0) {
      return <ErrorBlock title="暂无数据" />;
    }
    return group.map((x, i) => {
      return <div key={i}>{renderButton(x)}</div>;
    });
  };

  return (
    <>
      <Tabs style={{}} defaultActiveKey={'0'}>
        {[1, 2, 3, 4, 5, 6, 7].map((x, i) => {
          const date = myDayjs()
            .utc()
            .add(timezoneOffset, 'hour')
            .add(x, 'day');
          const date_format_str = date.format(dateFormat);
          return (
            <Tabs.Tab
              title={
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Typography variant="overline">{date_format_str}</Typography>
                  <Typography variant="caption" color="text.disabled">
                    {date.format(weekFormat)}
                  </Typography>
                </div>
              }
              key={`${i}`}
            >
              {renderDateGroup(date.format(dateFormat))}
            </Tabs.Tab>
          );
        })}
      </Tabs>
    </>
  );
};
