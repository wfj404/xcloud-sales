﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/store-manager")]
public class StoreManagerController : ShopBaseController
{
    private readonly IStoreManagerService _storeManagerService;
    private readonly IStoreManagerAccountService _storeManagerAccountService;
    private readonly IStoreRoleService _storeRoleService;

    public StoreManagerController(IStoreManagerService storeManagerService,
        IStoreRoleService storeRoleService,
        IStoreManagerAccountService storeManagerAccountService)
    {
        _storeManagerService = storeManagerService;
        _storeRoleService = storeRoleService;
        _storeManagerAccountService = storeManagerAccountService;
    }

    [HttpPost("list")]
    public async Task<ApiResponse<StoreManagerDto[]>> ListStoreManagerAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var datalist = await _storeManagerService.QueryByStoreIdAsync(manager.StoreId);

        await _storeManagerService.AttachUsersAsync(datalist);
        await _storeRoleService.AttachStoreRolesAsync(datalist);

        return new ApiResponse<StoreManagerDto[]>(datalist);
    }

    [HttpPost("update-manager")]
    public async Task<ApiResponse<StoreManagerDto>> UpdateManagerAsync([FromBody] StoreManagerDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredSuperManagerAsync(manager);

        var data = await this._storeManagerService.EnsureStoreManagerAsync(dto.Id, manager.StoreId);

        await this._storeManagerAccountService.UpdateStatusAsync(new UpdateStoreManagerStatusInput()
        {
            ManagerId = data.Id,
            IsActive = dto.IsActive,
            IsSuperManager = dto.IsSuperManager
        });

        return new ApiResponse<StoreManagerDto>();
    }
}