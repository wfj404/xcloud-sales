﻿using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Data;

public class PlatformDataSeedContributor : IDataSeedContributor, ITransientDependency
{
    private readonly EnsureSuperUserCreatedService _ensureSuperUserCreatedService;

    public PlatformDataSeedContributor(EnsureSuperUserCreatedService ensureSuperUserCreatedService)
    {
        _ensureSuperUserCreatedService = ensureSuperUserCreatedService;
    }

    public async Task SeedAsync(DataSeedContext context)
    {
        await _ensureSuperUserCreatedService.EnsureSuperUserCreatedAsync();
    }
}