import { Alert, Button, Card, Col, Row, Space } from 'antd';
import { ReactNode } from 'react';

import { PaymentChannel } from '@/utils/biz';
import { OrderGroupDataDto, OrderGroupDto, OrderGroupOrderDto } from '@/utils/swagger';
import { PlusOutlined } from '@ant-design/icons';

import XAffix from './affix';
import XList from './list';
import XAddress from './order_item/address';
import XSummary from './summary';

export default ({
  group,
  onChange,
  actionRender,
}: {
  group: OrderGroupDto;
  onChange: (e: OrderGroupDto) => void;
  actionRender?: () => ReactNode;
}) => {
  const group_data = group?.GroupData || {};
  const orders = group_data.Orders || [];

  return (
    <>
      <Row gutter={[10, 10]}>
        <Col span={6}>
          <Space direction="vertical" style={{ width: '100%' }}>
            <Alert message="同一个批次订单不支持多人同时编辑！" type="error" />
            <XAffix>
              <>
                <Space direction="vertical" style={{ width: '100%' }}>
                  <XSummary
                    group={group || {}}
                    onChange={(e) => onChange(e || {})}
                  />
                  {actionRender && actionRender()}
                </Space>
              </>
            </XAffix>
          </Space>
        </Col>
        <Col span={18}>
          <Card
            title="发件人"
            size="small"
            bordered
            extra={
              <span style={{ color: 'gray' }}>
                如果订单未指定发件人，将使用此处配置的发件人信息
              </span>
            }
            style={{ border: `3px dashed orange`, marginBottom: 30 }}
          >
            <XAddress
              nameLabel={'发件人'}
              address={group_data?.SharedSender || {}}
              onChange={(e) => {
                onChange({
                  ...group,
                  GroupData: {
                    ...group_data,
                    SharedSender: e,
                  },
                });
              }}
            />
          </Card>
          <XList
            group_data={group.GroupData || {}}
            onChange={(e) => {
              onChange({
                ...group,
                GroupData: e,
              });
            }}
          />
          <div style={{ padding: 10 }}>
            <Button
              type="primary"
              block
              size="large"
              icon={<PlusOutlined />}
              onClick={() => {
                const order: OrderGroupOrderDto = {
                  SpecificSender: false,
                  Sender: {},
                  UserAddress: {},
                  Items: [
                    {
                      Quantity: 1,
                      UnitPrice: 0,
                    },
                  ],
                  PaymentBills: [
                    {
                      PaymentChannel: PaymentChannel.Balance,
                    },
                  ],
                };

                const order_group_data: OrderGroupDataDto = {
                  ...group_data,
                  Orders: [...orders, order],
                };

                onChange({
                  ...group,
                  GroupData: order_group_data,
                });
              }}
            >
              添加订单
            </Button>
          </div>
        </Col>
      </Row>
    </>
  );
};
