﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/brand")]
public class BrandController : ShopBaseController
{
    private readonly IBrandService _brandService;

    public BrandController(IBrandService brandService)
    {
        _brandService = brandService;
    }

    [HttpPost("list")]
    public async Task<ApiResponse<BrandDto[]>> ListBrandAsync([FromBody] QueryBrandPaging dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        dto.Page = 1;
        dto.PageSize = 1000;
        dto.SkipCalculateTotalCount = true;

        var response = await _brandService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await _brandService.AttachPicturesAsync(response.Items.ToArray());
        }

        return new ApiResponse<BrandDto[]>(response.Items.ToArray());
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<BrandDto>> QueryPagingAsync([FromBody] QueryBrandPaging model)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        model.PageSize = 20;

        var brands = await _brandService.QueryPagingAsync(model);

        if (brands.IsNotEmpty)
        {
            await _brandService.AttachPicturesAsync(brands.Items.ToArray());
        }

        return brands;
    }

    [HttpPost("save")]
    public virtual async Task<ApiResponse<object>> SaveBrandAsync([FromBody] BrandDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        if (!string.IsNullOrWhiteSpace(dto.Id))
        {
            var brand = await _brandService.GetByIdAsync(dto.Id);

            if (brand == null)
                throw new EntityNotFoundException(nameof(brand));

            await _brandService.UpdateAsync(dto);
        }
        else
        {
            await _brandService.InsertAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public virtual async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        await _brandService.DeleteByIdAsync(dto);

        return new ApiResponse<object>();
    }
}