﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace XCloud.AspNetMvc.ModelBinder;

[AttributeUsage(AttributeTargets.Parameter)]
public class InjectCancellationTokenAttribute : ModelBinderAttribute
{
    public InjectCancellationTokenAttribute() : base(typeof(CancellationTokenBinder))
    {
        //BindingSource = Microsoft.AspNetCore.Mvc.ModelBinding.BindingSource.Custom;
    }

    public override BindingSource BindingSource { get; protected set; }
}