﻿using Volo.Abp.DependencyInjection;
using Volo.Abp.Json;

namespace XCloud.Core.Json;

public interface IJsonDataSerializer
{
    string SerializeToString(object item);

    T DeserializeFromString<T>(string serializedObject);

    object DeserializeFromString(string serializedObject, Type target);
}

[DisableConventionalRegistration]
public class AbpJsonDataSerializerProvider : IJsonDataSerializer
{
    private readonly IJsonSerializer _jsonSerializer;

    public AbpJsonDataSerializerProvider(IJsonSerializer jsonSerializer)
    {
        _jsonSerializer = jsonSerializer;
    }

    public string SerializeToString(object item)
    {
        return this._jsonSerializer.Serialize(item, camelCase: false);
    }

    public T DeserializeFromString<T>(string serializedObject)
    {
        return this._jsonSerializer.Deserialize<T>(serializedObject, camelCase: false);
    }

    public object DeserializeFromString(string serializedObject, Type target)
    {
        return this._jsonSerializer.Deserialize(target, serializedObject, camelCase: false);
    }
}