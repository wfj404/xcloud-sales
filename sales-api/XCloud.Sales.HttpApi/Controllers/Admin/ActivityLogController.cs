using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Logging;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/activity-log")]
public class ActivityLogController : ShopBaseController
{
    private readonly IActivityLogService _activityLogService;
    private readonly IAdminService _adminService;
    private readonly IStoreUserService _storeUserService;

    public ActivityLogController(IActivityLogService activityLogService, IAdminService adminService,
        IStoreUserService storeUserService)
    {
        _activityLogService = activityLogService;
        _adminService = adminService;
        _storeUserService = storeUserService;
    }

    [HttpPost("log-paging")]
    public async Task<PagedResponse<ActivityLogDto>> QueryActivityLogPagingAsync(
        [FromBody] QueryActivityLogPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        var response = await _activityLogService.QueryPagingAsync(dto);

        if (response.IsEmpty)
            return response;

        await this._activityLogService.AttachAdminAsync(response.Items.ToArray());
        await this._activityLogService.AttachStoreUserAsync(response.Items.ToArray());

        var admins = response.Items.Select(x => x.Admin).WhereNotNull().ToArray();
        await this._adminService.AttachUsersAsync(admins);

        var storeUsers = response.Items.Select(x => x.StoreUser).WhereNotNull().ToArray();
        await this._storeUserService.AttachPlatformUsersAsync(storeUsers);

        return response;
    }

    [HttpPost("delete-log")]
    public async Task<ApiResponse<object>> DeleteActivityLogAsync([FromBody] IdDto<int> dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageSettings);

        await _activityLogService.DeleteAsync(dto.Id);

        return new ApiResponse<object>();
    }
}