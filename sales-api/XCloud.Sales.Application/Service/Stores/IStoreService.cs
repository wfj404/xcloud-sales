﻿using JetBrains.Annotations;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreService : ISalesStringCrudAppService<Store, StoreDto, QueryStorePagingInput>
{
    [NotNull]
    Task<StoreDto> GetOrCreateByExternalIdAsync(string externalId, string name = null);

    Task<StoreDto[]> ListAllStoresAsync(bool? openingOnly, int? maxCount);
}

public class StoreService : SalesStringCrudAppService<Store, StoreDto, QueryStorePagingInput>, IStoreService
{
    public StoreService(ISalesRepository<Store> storeRepository) : base(storeRepository)
    {
        //
    }

    public async Task<StoreDto[]> ListAllStoresAsync(bool? openingOnly, int? maxCount)
    {
        maxCount ??= 100;

        this.CommonUtils.EnsureMax(maxCount.Value, 5000);

        var db = await this.Repository.GetDbContextAsync();

        var query = db.Set<Store>().AsNoTracking();

        if (openingOnly ?? false)
        {
            query = query.Where(x => x.Opening);
        }

        var datalist = await query
            .OrderByDescending(x => x.CreationTime)
            .Take(maxCount.Value)
            .ToArrayAsync();

        return datalist.MapArrayTo<Store, StoreDto>(ObjectMapper);
    }

    public override async Task DeleteByIdAsync(string id)
    {
        await this.Repository.SoftDeleteAsync(id, this.Clock.Now);
    }

    public async Task<StoreDto> GetOrCreateByExternalIdAsync(string externalId, string name)
    {
        if (string.IsNullOrWhiteSpace(externalId))
            throw new ArgumentNullException(nameof(externalId));

        var db = await this.Repository.GetDbContextAsync();

        var store = await db.Set<Store>().IgnoreQueryFilters().AsNoTracking()
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync(x => x.ExternalId == externalId);

        if (store == null)
        {
            await using var _ = await this.DistributedLockProvider.CreateRequiredLockAsync(
                resource: $"get.or.create.store.by.external.id.{externalId}",
                expiryTime: TimeSpan.FromSeconds(5));

            store = new Store()
            {
                Name = name ?? this.GuidGenerator.CreateGuidString(),
                InvoiceSupported = true,
                IsDeleted = false,
                Opening = false,
                ExternalId = externalId
            };

            store = await this.InsertAsync(store.MapTo<Store, StoreDto>(ObjectMapper));

            await this.RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return store.MapTo<Store, StoreDto>(ObjectMapper);
    }

    protected override async Task SetFieldsBeforeUpdateAsync(Store store, StoreDto dto)
    {
        await Task.CompletedTask;

        store.Name = dto.Name;
        store.Description = dto.Description;
        store.Telephone = dto.Telephone;
        store.Opening = dto.Opening;
        store.InvoiceSupported = dto.InvoiceSupported;
        store.Lat = dto.Lat;
        store.Lon = dto.Lon;
        store.AddressDetail = dto.AddressDetail;
    }

    protected override async Task SetFieldsBeforeInsertAsync(Store store)
    {
        await Task.CompletedTask;

        store.Id = GuidGenerator.CreateGuidString();
        store.CreationTime = Clock.Now;
    }

    public override Task<Store> InsertAsync(StoreDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException("store name is required");

        return base.InsertAsync(dto);
    }

    public override Task<Store> UpdateAsync(StoreDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new UserFriendlyException("store name is required");

        return base.UpdateAsync(dto);
    }
}