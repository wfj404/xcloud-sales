import { apiClient } from '@/utils/client';
import { WarehouseDto } from '@/utils/swagger';
import { Button, Card, message, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import XForm from './form';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<WarehouseDto[]>([]);

  const [formData, _formData] = useState<WarehouseDto | undefined>(undefined);

  const [loadingId, _loadingId] = useState('');

  const queryList = () => {
    _loading(true);
    apiClient.mallAdmin
      .warehouseQueryAll({})
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data.Data || []);
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const deleteBrand = (row: WarehouseDto) => {
    if (!confirm('删除品牌？')) {
      return;
    }
    _loadingId(row.Id || '');
    apiClient.mallAdmin
      .warehouseUpdateStatus({
        Id: row.Id,
        IsDeleted: true,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          queryList();
        }
      })
      .finally(() => {
        _loadingId('');
      });
  };

  const columns: ColumnProps<any>[] = [
    {
      title: '名称',
      render: (x) => x.Name,
    },
    {
      title: '地址',
      render: (x) => x.Address || '--',
    },
    {
      title: '操作',
      width: 200,
      render: (record: WarehouseDto) => {
        return (
          <Button.Group>
            <Button
              type='link'
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
            <Button
              loading={loadingId == record.Id}
              type='link'
              danger
              onClick={() => {
                deleteBrand(record);
              }}
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title='仓库'
        extra={
          <Button
            type='primary'
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id}
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Card>
    </>
  );
};
