import { Button, Col, ConfigProvider, Row, Space } from 'antd';
import { useEffect, useState } from 'react';

import XBlocksDesigner from '@/components/blocks/design';
import { BlockItem, BlockItemProps, getTemplateBlocks } from '@/components/blocks/utils';
import { apiClient } from '@/utils/client';
import { GlobalContainer, handleResponse, parseJsonOrEmpty } from '@/utils/utils';

import XPreview from './preview';

export default function () {
  const [loading, _loading] = useState(false);
  const [loadingSave, _loadingSave] = useState(false);
  const [blocksCopy, _blocksCopy] = useState<BlockItem<BlockItemProps>[]>([]);
  const [blocks, _blocks] = useState<BlockItem<BlockItemProps>[]>([]);

  const loadTemplate = () => {
    getTemplateBlocks().then((res) => _blocks(res));
  };

  const saveHomePageBlocks = () => {
    _loadingSave(true);
    apiClient.mallAdmin
      .settingSaveHomePageBlocks({ Blocks: JSON.stringify(blocks) })
      .then((res) => {
        handleResponse(res, () => {
          GlobalContainer.message?.success('保存成功');
          queryHomePageBlocks();
        });
      })
      .finally(() => _loadingSave(false));
  };

  const queryHomePageBlocks = () => {
    _loading(true);
    apiClient.mallAdmin
      .settingHomePageBlocks()
      .then((res) => {
        const blocks_data: BlockItem<BlockItemProps>[] = parseJsonOrEmpty(
          res.data?.Data?.Blocks,
        );
        _blocksCopy(blocks_data);
        _blocks(blocks_data);
      })
      .finally(() => _loading(false));
  };

  useEffect(() => {
    queryHomePageBlocks();
  }, []);

  const renderSaveButtons = () => {
    return (
      <Space direction={'horizontal'}>
        <XPreview blocks={blocks} />
        <Button
          size={'small'}
          type={'link'}
          onClick={() => {
            loadTemplate();
          }}
        >
          加载模板
        </Button>
        <Button
          size={'small'}
          type={'link'}
          danger
          onClick={() => {
            if (confirm('确定撤销所有修改？')) {
              _blocks(blocksCopy || []);
            }
          }}
        >
          回滚
        </Button>
        <Button
          size={'small'}
          type={'primary'}
          loading={loadingSave}
          onClick={() => {
            saveHomePageBlocks();
          }}
        >
          保存并生效
        </Button>
      </Space>
    );
  };

  return (
    <>
      <ConfigProvider componentSize="small">
        <Row gutter={[10, 10]}>
          <Col span={12}>
            <XBlocksDesigner
              loading={loading}
              blocks={blocks}
              onBlocksChange={(e) => {
                _blocks(e);
              }}
              renderSaveButtons={() => renderSaveButtons()}
            />
          </Col>
          <Col span={12}></Col>
        </Row>
      </ConfigProvider>
    </>
  );
}
