import { useRequest } from 'ahooks';
import { Button, Dropdown, message, notification, Space, Tooltip } from 'antd';

import { apiClient } from '@/utils/client';
import { myDayjs, timezoneOffset } from '@/utils/dayjs';
import { OrderGroupDataDto, OrderGroupDto } from '@/utils/swagger';
import { getPinyinShortStyle, handleResponse, isStringEmpty } from '@/utils/utils';
import {
    ExportOutlined, OrderedListOutlined, ReloadOutlined, SaveOutlined
} from '@ant-design/icons';

export default ({
  group,
  ok,
  onSaved,
  onChange,
  loadingData,
}: {
  group: OrderGroupDto;
  onChange: (e: OrderGroupDto) => void;
  ok: () => void;
  onSaved: () => void;
  loadingData?: boolean;
}) => {
  const save_request = useRequest(apiClient.mallManager.orderGroupSave, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        notification.success({
          message: '保存成功',
          description: '你可以继续编辑',
        });
        onSaved();
        ok();
      });
    },
  });

  const export_excel = useRequest(apiClient.mallManager.orderGroupExportExcel, {
    manual: true,
    onSuccess(data, params) {
      const time = myDayjs
        .utc()
        .add(timezoneOffset, 'hour')
        .format('YYYY-MM-DD-HH-mm-ss');

      const blob = new Blob([data.data as any]);
      const elink = document.createElement('a'); // 创建a标签
      elink.download = `${group.Name || '--'}-${time}.xlsx`;
      elink.style.display = 'none';
      // 创建一个指向blob的url，这里就是点击可以下载文件的根结
      elink.href = URL.createObjectURL(blob);
      document.body.appendChild(elink);
      elink.click();
      URL.revokeObjectURL(elink.href); //移除链接
      document.body.removeChild(elink); //移除a标签
    },
  });

  const comparePinyin = (
    a: string | null | undefined,
    b: string | null | undefined,
  ) => {
    return getPinyinShortStyle(a || 'zzzzzzzzzzzz').localeCompare(
      getPinyinShortStyle(b || 'zzzzzzzzzzzzzzz'),
    );
  };

  const sort_request = useRequest(
    () =>
      new Promise<void>((resolve, reject) => {
        try {
          const orders = group.GroupData?.Orders || [];
          const sorted_orders = orders.sort((a, b) =>
            comparePinyin(a?.UserAddress?.Name, b?.UserAddress?.Name),
          );
          const group_data: OrderGroupDataDto = {
            ...group.GroupData,
            Orders: sorted_orders,
          };
          onChange({
            ...group,
            GroupData: group_data,
          });

          setTimeout(() => resolve(), 1000);
        } catch (e) {
          reject(e);
        }
      }),
    {
      manual: true,
    },
  );

  return (
    <>
      <Space direction="horizontal" wrap style={{ marginBottom: 10 }}>
        <Tooltip title={`保存至服务器，当前版本号：${group.RowVersion}`}>
          <Button
            type="primary"
            loading={save_request.loading}
            icon={<SaveOutlined />}
            onClick={() => {
              if (isStringEmpty(group.Id)) {
                message.error('批次id不存在');
                return;
              }

              save_request.run(group || {});
            }}
          >
            保存
          </Button>
        </Tooltip>
        <Tooltip title="加载最新数据，本地未保存更改将被覆盖">
          <Button
            loading={loadingData}
            type="dashed"
            danger
            icon={<ReloadOutlined />}
            onClick={() => {
              if (confirm('确定重新加载数据？')) {
                ok();
              }
            }}
          ></Button>
        </Tooltip>
        <Tooltip title="按照买家姓名排序">
          <Button
            type="dashed"
            icon={<OrderedListOutlined />}
            loading={sort_request.loading}
            onClick={() => {
              sort_request.run();
            }}
          >
            按姓名排序
          </Button>
        </Tooltip>
        <Tooltip title="复制数据">
          <Dropdown.Button
            trigger={['hover']}
            loading={export_excel.loading}
            menu={{
              items: [
                {
                  key: 'export-excel',
                  label: '导出表格',
                  icon: <ExportOutlined />,
                  danger: true,
                  onClick: () => {
                    export_excel.run(group || {}, {
                      format: 'blob',
                    });
                  },
                },
              ],
            }}
            type="dashed"
            onClick={() => {
              navigator.clipboard.writeText(JSON.stringify(group || {}));
              message.success('复制成功');
            }}
          >
            复制数据
          </Dropdown.Button>
        </Tooltip>
      </Space>
    </>
  );
};
