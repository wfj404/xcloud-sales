﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Data;
using XCloud.Application.DistributedLock;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Data;

public class SalesData : IEntityDto
{
    public string[] Brand { get; set; }
    public string[] Category { get; set; }
    public string[] Tag { get; set; }
}

public class SalesDataSeederContributor : SalesAppService, IDataSeedContributor, ITransientDependency
{
    private readonly IBrandService _brandService;
    private readonly ICategoryService _categoryService;
    private readonly ITagService _tagService;
    private readonly ISalesRepository<Brand> _repository;
    private readonly IWebHostEnvironment _webHostEnvironment;

    public SalesDataSeederContributor(
        IBrandService brandService,
        ISalesRepository<Brand> repository,
        ICategoryService categoryService,
        ITagService tagService,
        IWebHostEnvironment webHostEnvironment)
    {
        _brandService = brandService;
        _repository = repository;
        _categoryService = categoryService;
        _tagService = tagService;
        _webHostEnvironment = webHostEnvironment;
    }

    public async Task SeedAsync(DataSeedContext context)
    {
        var dataPath = Path.Combine(_webHostEnvironment.ContentRootPath, "data", "sales.json");
        if (!File.Exists(dataPath))
        {
            Logger.LogInformation("sales data not exist , skip restore");
            return;
        }

        var json = await File.ReadAllTextAsync(dataPath);

        var data = JsonDataSerializer.DeserializeFromString<SalesData>(json);
        if (data == null)
            throw new AbpException($"failed to parse sales data");

        await TryCreateBrandsAsync(data);
        await TryCreateCategoryAsync(data);
        await TryCreateTagsAsync(data);
    }

    private async Task TryCreateBrandsAsync(SalesData data)
    {
        if (ValidateHelper.IsEmptyCollection(data.Brand))
            return;

        await using var dlock = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"try-create-preset-brands",
            expiryTime: TimeSpan.FromSeconds(30));

        var db = await _repository.GetDbContextAsync();
        var query = db.Set<Brand>().IgnoreQueryFilters().AsNoTracking();

        if (await query.AnyAsync())
            return;

        foreach (var m in data.Brand)
        {
            await _brandService.InsertAsync(new BrandDto
            {
                Name = m,
                IsDeleted = false,
                Published = true,
            });
        }
    }

    private async Task TryCreateCategoryAsync(SalesData data)
    {
        if (ValidateHelper.IsEmptyCollection(data.Category))
            return;

        await using var dlock = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"try-create-preset-categories",
            expiryTime: TimeSpan.FromSeconds(30));

        var db = await _repository.GetDbContextAsync();
        var query = db.Set<Category>().IgnoreQueryFilters().AsNoTracking();

        if (await query.AnyAsync())
            return;

        foreach (var m in data.Category)
        {
            await _categoryService.InsertAsync(new Category
            {
                ParentId = default,
                Name = m,
                IsDeleted = false,
                Published = true,
            });
        }
    }

    private async Task TryCreateTagsAsync(SalesData data)
    {
        if (ValidateHelper.IsEmptyCollection(data.Tag))
            return;

        await using var dlock = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"try-create-preset-tags",
            expiryTime: TimeSpan.FromSeconds(30));

        var db = await _repository.GetDbContextAsync();
        var query = db.Set<Tag>().IgnoreQueryFilters().AsNoTracking();

        if (await query.AnyAsync())
            return;

        foreach (var m in data.Tag)
        {
            await _tagService.InsertAsync(new TagDto
            {
                Name = m,
                IsDeleted = false,
            });
        }
    }
}