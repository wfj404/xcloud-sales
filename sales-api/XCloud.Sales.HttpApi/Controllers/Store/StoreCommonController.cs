﻿using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/common")]
public class StoreCommonController : ShopBaseController
{
    private readonly IUserAccountService _userAccountService;
    private readonly IStoreUserAccountService _storeUserAccountService;

    public StoreCommonController(IUserAccountService userAccountService,
        IStoreUserAccountService storeUserAccountService)
    {
        _userAccountService = userAccountService;
        _storeUserAccountService = storeUserAccountService;
    }

    [HttpPost("search-store-user-account")]
    public async Task<ApiResponse<StoreUserDto>> SearchStoreUserAccountAsync([FromBody] SearchUserAccountDto dto)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var account = await _userAccountService.GetByAccountIdentityAsync(dto.AccountIdentity);

        if (account != null)
        {
            var storeUser =
                await this._storeUserAccountService.GetStoreUserAuthValidationDataAsync(account.Id,
                    new CacheStrategy() { Source = true });

            if (storeUser != null)
            {
                storeUser.User = account;
                return new ApiResponse<StoreUserDto>(storeUser);
            }
        }

        return new ApiResponse<StoreUserDto>();
    }
}