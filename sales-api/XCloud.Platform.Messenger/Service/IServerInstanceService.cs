using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Core.Cache;
using XCloud.Platform.Application.Repository;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Messenger.Domain.Messenger;

namespace XCloud.Platform.Messenger.Service;

public interface IServerInstanceService : IXCloudAppService
{
    Task PingAsync(ServerInstanceDto dto);

    Task<ServerInstanceDto[]> QueryAllAsync();

    Task DeleteExpiredAsync();
}

[ExposeServices(typeof(IServerInstanceService))]
[UnitOfWork]
public class ServerInstanceService : PlatformAppService, IServerInstanceService
{
    private readonly IPlatformRepository<ServerInstance> _repository;

    public ServerInstanceService(IPlatformRepository<ServerInstance> repository)
    {
        _repository = repository;
    }

    private int ExpiredMinutes => 3;

    public async Task PingAsync(ServerInstanceDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.InstanceId))
            throw new ArgumentNullException(nameof(dto.InstanceId));

        var key = $"im.server.instance.reg.{dto.InstanceId}";

        if (!await this.CacheProvider.TrySetEmptyStringAsync(key, TimeSpan.FromMinutes(1)))
        {
            return;
        }

        var entity = await _repository.FirstOrDefaultAsync(x => x.InstanceId == dto.InstanceId);

        if (entity == null)
        {
            entity = ObjectMapper.Map<ServerInstanceDto, ServerInstance>(dto);

            entity.Id = GuidGenerator.CreateGuidString();
            entity.CreationTime = Clock.Now;

            await _repository.InsertAsync(entity);
        }
        else
        {
            entity.ConnectionCount = dto.ConnectionCount;
            entity.PingTime = dto.PingTime;

            await _repository.UpdateAsync(entity);
        }

        await RequiredCurrentUnitOfWork.SaveChangesAsync();
    }

    public async Task<ServerInstanceDto[]> QueryAllAsync()
    {
        var db = await _repository.GetDbContextAsync();

        var query = db.Set<ServerInstance>().AsNoTracking();

        var expired = Clock.Now.AddMinutes(-this.ExpiredMinutes);

        query = query.Where(x => x.PingTime > expired);

        var data = await query.OrderByDescending(x => x.PingTime).ToArrayAsync();

        return data.MapArrayTo<ServerInstance, ServerInstanceDto>(this.ObjectMapper);
    }

    public async Task DeleteExpiredAsync()
    {
        var expired = Clock.Now.AddMinutes(-this.ExpiredMinutes);

        await this._repository.DeleteAsync(x => x.PingTime < expired);
    }
}