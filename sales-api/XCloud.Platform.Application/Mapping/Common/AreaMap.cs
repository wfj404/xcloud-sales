﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Common;

namespace XCloud.Platform.Application.Mapping.Common;

public class AreaMap : PlatformEntityTypeConfiguration<Area>
{
    public override void Configure(EntityTypeBuilder<Area> builder)
    {
        builder.ToTable("sys_area");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigTreeEntity();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.MetaId).HasMaxLength(100);
        builder.Property(x => x.AreaType).HasMaxLength(100);
        builder.Property(x => x.NodePathJson);
        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}