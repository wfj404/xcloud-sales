﻿using System.Text.RegularExpressions;
using Castle.DynamicProxy;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Configuration;
using XCloud.Core.Http.Dynamic.Proxy;

namespace XCloud.Core.Http.Dynamic;

public static class DynamicHttpClientExtension
{
    public static IXCloudBuilder AddDynamicHttpClient<T>(this IXCloudBuilder builder)
        where T : class, IApplicationService
    {
        builder.Services.RemoveAll<T>();
        builder.Services.AddTransient<T>(CreateProxyClient<T>);

        return builder;
    }

    private static T CreateProxyClient<T>(IServiceProvider provider) where T : class, IApplicationService
    {
        var proxy = provider.GetRequiredService<DynamicHttpClientProxy>();

        var generator = provider.GetRequiredService<IObjectAccessor<ProxyGenerator>>().Value; //new ProxyGenerator();

        var client = generator.CreateInterfaceProxyWithoutTarget<T>(interceptors: proxy);

        //client = generator.CreateInterfaceProxyWithTarget<T>(target: client, new PolicyProxy(provider));

        return client;
    }

    private static readonly Regex Re = new Regex(@"\{([^\}]+)\}", RegexOptions.Compiled);

    internal static string ReplacePathArgs(this string path, IDictionary<string, object> args)
    {
        if (string.IsNullOrWhiteSpace(path))
            throw new ArgumentNullException(nameof(path));

        if (args == null)
            throw new ArgumentException(nameof(args));

        var res = Re.Replace(path, x => args[x.Groups[1].Value]?.ToString());
        return res;
    }
}