import { useEffect, useState } from 'react';

import { ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';
import { DeliveryRecordDto, OrderDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';

export default function(props: { model: OrderDto }) {
  const { model } = props;

  const [data, _data] = useState<DeliveryRecordDto[]>([]);
  const [loading, _loading] = useState(false);

  const queryList = (id: string) => {
    if (isStringEmpty(id)) {
      _data([]);
      return;
    }
    _loading(true);
    apiClient.mall
      .deliveryGetByOrderId({ Id: id })
      .then((res) => {
        _data(res.data.Data || []);
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    model && queryList(model.Id || '');
  }, [model]);

  const address = model.UserAddress || {};

  if (model.ShippingMethodId == ShippingMethod.Pickup) {
    return null;
  }

  return (
    <Box sx={{ p: 2, my: 1 }}>
      <Typography variant='h6' gutterBottom>
        配送信息
      </Typography>
      <TableContainer component={Paper}>
        <Table sx={{}} size='small'>
          <TableBody>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                收货人
              </TableCell>
              <TableCell align='right'>{address.Name || '--'}</TableCell>
            </TableRow>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                联系电话
              </TableCell>
              <TableCell align='right'>{address.Tel || '--'}</TableCell>
            </TableRow>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                配送地址
              </TableCell>
              <TableCell align='right'>
                {address.AddressDetail || '--'}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>

      <TableContainer component={Paper}>
        <Table sx={{}} size='small' title={'配送单'}>
          <TableBody>
            {data.map((x, i) => {
              return (
                <TableRow
                  key={i}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell>{x.ExpressName || '--'}</TableCell>
                  <TableCell>{x.TrackingNumber || '--'}</TableCell>
                  <TableCell>
                    {formatRelativeTimeFromNow(x.CreationTime) || '--'}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}
