﻿using XCloud.Sales.Application.Service.Orders.PlaceOrder;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Service.Shipping.Pickup;

public interface IOrderPickupService : ISalesAppService
{
    Task SetPickupAsync(PlaceOrderDataHolder holder);
}

public class OrderPickupService : SalesAppService, IOrderPickupService
{
    private readonly IStoreSettingService _storeSettingService;

    public OrderPickupService(IStoreSettingService storeSettingService)
    {
        _storeSettingService = storeSettingService;
    }

    public async Task SetPickupAsync(PlaceOrderDataHolder holder)
    {
        await Task.CompletedTask;

        if (holder.Request.ShippingMethodId != SalesConstants.ShippingMethodType.Pickup)
            return;

        var settings = await _storeSettingService.GetSettingsAsync<StorePickupSettings>(holder.Order.StoreId);
        if (!settings.Enabled)
        {
            holder.ShippingErrors.Add("pick up is not enabled");
        }

        if (!settings.PayAfterShippedSupported &&
            holder.Request.PaymentMethodId == SalesConstants.PaymentMethod.Offline)
        {
            holder.ShippingErrors.Add("after shipped payment is not supported");
        }

        if (holder.ShippingErrors.Any())
            return;

        holder.Order.ShippingMethodId = SalesConstants.ShippingMethodType.Pickup;
        holder.Order.ShippingFee = decimal.Zero;
        holder.Order.PickupRecord = new PickupRecordDto
        {
            PreferPickupStartTime = holder.Request.PreferDeliveryStartTime,
            PreferPickupEndTime = holder.Request.PreferDeliveryEndTime,
        };
        Logger.LogInformation("pick up in store");
    }
}