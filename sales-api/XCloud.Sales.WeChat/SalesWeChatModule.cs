﻿using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.AspNetMvc.Extension;
using XCloud.Platform.WeChat;
using XCloud.Sales.Application;
using XCloud.Sales.WeChat.Job;

namespace XCloud.Sales.WeChat;

[DependsOn(
    typeof(SalesApplicationModule),
    //wechat
    typeof(PlatformWechatModule))]
[MethodMetric]
public class SalesWeChatModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.AddMvcPart<SalesWeChatModule>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        context.ServiceProvider.StartSalesWechatJobs();
    }
}