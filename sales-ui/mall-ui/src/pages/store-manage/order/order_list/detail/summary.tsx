import { Alert, Descriptions, Space, Tag } from 'antd';

import XTime from '@/components/common/time';
import XUserAvatar from '@/components/manage/user/avatar';
import XOrderDeliveryTag from '@/components/status/delivery';
import XOrderStatusTag from '@/components/status/order';
import XOrderPaymentTag from '@/components/status/payment';
import { getPaymentMethod, getShippingMethod } from '@/utils/biz';
import { OrderDto } from '@/utils/swagger';
import { formatMoney, isStringEmpty } from '@/utils/utils';

export default ({ model }: { model: OrderDto }) => {
  return (
    <>
      <Descriptions
        bordered
        size={'small'}
        column={3}
        style={{ marginBottom: 10 }}
      >
        <Descriptions.Item label={'买家'}>
          <XUserAvatar model={model.StoreUser?.User} />
        </Descriptions.Item>
        <Descriptions.Item label="订单号">
          <div>
            <p>
              <b>{model.OrderSn || '--'}</b>
            </p>
            <Space direction={'horizontal'}>
              <Tag color={'blue'}>{getShippingMethod(model)}</Tag>
              <Tag color={'red'}>{getPaymentMethod(model)}</Tag>
            </Space>
          </div>
        </Descriptions.Item>
        <Descriptions.Item label="时间">
          <XTime model={model} />
        </Descriptions.Item>
        <Descriptions.Item label="订单状态">
          <XOrderStatusTag model={model} />
        </Descriptions.Item>
        <Descriptions.Item label="支付状态">
          <XOrderPaymentTag model={model} />
          {(model.PaidTotalPrice || 0) > 0 && (
            <p style={{ color: 'gray' }}>
              已支付{formatMoney(model.PaidTotalPrice || 0)}
            </p>
          )}
          {(model.RefundedAmount || 0) > 0 && (
            <p style={{ color: 'red' }}>
              已退款{formatMoney(model.RefundedAmount || 0)}
            </p>
          )}
        </Descriptions.Item>
        <Descriptions.Item label="配送状态">
          <XOrderDeliveryTag model={model} />
        </Descriptions.Item>
        <Descriptions.Item label="备注" span={3}>
          {isStringEmpty(model.Remark) && <p>--</p>}
          {isStringEmpty(model.Remark) || (
            <Alert message={`${model.Remark || '--'}`} type={'info'} />
          )}
        </Descriptions.Item>
        <Descriptions.Item label="收件人">
          {model.UserAddress?.Name || '--'}
        </Descriptions.Item>
        <Descriptions.Item label="收件人电话">
          {model.UserAddress?.Tel || '--'}
        </Descriptions.Item>
        <Descriptions.Item label="地址类型">
          {model.UserAddress?.AddressDescription || '--'}
        </Descriptions.Item>
        <Descriptions.Item label="收件地址" span={3}>
          {model.UserAddress?.AddressDetail || '--'}
        </Descriptions.Item>
      </Descriptions>
    </>
  );
};
