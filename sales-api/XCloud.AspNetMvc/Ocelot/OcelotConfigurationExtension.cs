﻿using Microsoft.Extensions.DependencyInjection;
using Ocelot.DependencyInjection;
using Ocelot.Provider.Nacos;
using Volo.Abp.Modularity;
using XCloud.AspNetMvc.Nacos;

namespace XCloud.AspNetMvc.Ocelot;

public static class OcelotConfigurationExtension
{
    public static void ConfigOcelotGateway(this ServiceConfigurationContext context)
    {
        //config
        var config = context.Services.GetConfiguration();

        //add ocelot
        var builder = context.Services.AddOcelot(config);

        //try enable nacos naming service
        var nacosConfiguration = config.GetNacosConfiguration(NacosDefaults.DefaultSectionName);

        if (nacosConfiguration.Enabled)
        {
            if (nacosConfiguration.ServiceExist)
            {
                builder.AddNacosDiscovery(section: nacosConfiguration.Service.Path);
            }
        }
    }
}