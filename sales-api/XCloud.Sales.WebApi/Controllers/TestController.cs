using System.Linq;
using Microsoft.Extensions.Localization;
using Volo.Abp.Http;
using XCloud.Application.Model;
using XCloud.AspNetMvc.Filters;
using XCloud.Sales.Application.Dto;
using XCloud.Sales.Application.Localization;

namespace XCloud.Sales.WebApi.Controllers;

//[Route("api/mall/test")]
[NonActionLogging]
[Route("[controller]/[action]")]
public class TestController : ShopBaseController
{
    public TestController()
    {
        LocalizationResource = typeof(SalesResource);
    }

    [ValidateSign]
    [HttpGet]
    public string TestSign()
    {
        return string.Empty;
    }

    [Obsolete("test-deprecated-logging-filter")]
    [HttpGet]
    public LocalizedString[] LangTest()
    {
        return this.L.GetAllStrings(includeParentCultures: true).ToArray();
    }

    [HttpGet]
    public RefDto GetRefDto()
    {
        return new RefDto();
    }

    [HttpGet]
    public IActionResult SortedJson()
    {
        var response = new PagedResponse<Platform.Application.Domain.Users.User>
        {
            PageIndex = 1,
            PageSize = 2,
            TotalCount = 99,
            Items = new[]
            {
                new Platform.Application.Domain.Users.User()
                    { IdentityName = "long name", NickName = "such a long name" },
                new Platform.Application.Domain.Users.User() { NickName = "1" },
            },
            ExtraProperties =
            {
                ["z"] = "z",
                ["a"] = "a"
            }
        };

        var json = this.CommonUtils.SerializeWithSortedKeys(response);

        return Content(json, contentType: MimeTypes.Application.Json);
    }
}