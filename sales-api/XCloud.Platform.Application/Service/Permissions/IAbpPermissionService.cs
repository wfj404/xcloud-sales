using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Authorization.Permissions;
using XCloud.Application.Model;
using XCloud.Application.Service;
using XCloud.Platform.Application.Localization;

namespace XCloud.Platform.Application.Service.Permissions;

public interface IAbpPermissionService : IXCloudAppService
{
    Task<string[]> QueryAllPermissionsAsync();

    Task<AntDesignTreeNode<object>[]> BuildAntDesignTreeAsync();
}

public class AbpPermissionService : PlatformAppService, IAbpPermissionService
{
    private readonly IPermissionDefinitionManager _permissionManager;

    public AbpPermissionService(IPermissionDefinitionManager permissionService)
    {
        _permissionManager = permissionService;
    }

    public async Task<string[]> QueryAllPermissionsAsync()
    {
        await Task.CompletedTask;

        var res = (await _permissionManager.GetPermissionsAsync()).Select(x => x.Name).ToArray();

        return res;
    }

    private AntDesignTreeNode<object> PermissionDefinition(PermissionDefinition m, IList<string> list)
    {
        list.AddOnceOrThrow(m.Name);

        return new AntDesignTreeNode<object>
        {
            key = m.Name,
            title = L[m.Name]?.Value ?? m.Name,
            raw_data = new { },
            children = m.Children.Select(x => PermissionDefinition(x, list)).ToArray()
        };
    }

    private AntDesignTreeNode<object> GroupDefinition(PermissionGroupDefinition x, IList<string> list)
    {
        list.AddOnceOrThrow(x.Name);

        return new AntDesignTreeNode<object>
        {
            key = x.Name,
            title = L[x.Name]?.Value ?? x.Name,
            raw_data = new { },
            children = x.Permissions.Select(x => PermissionDefinition(x, list)).ToArray()
        };
    }

    public async Task<AntDesignTreeNode<object>[]> BuildAntDesignTreeAsync()
    {
        await Task.CompletedTask;

        LocalizationResource = typeof(PlatformResource);

        var list = new List<string>();

        var groups = await _permissionManager.GetGroupsAsync();

        var res = groups.Select(x => GroupDefinition(x, list)).ToArray();

        return res;
    }
}