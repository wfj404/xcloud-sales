﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Volo.Abp.Uow;
using XCloud.Application.Service;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Platform.WeChat.Services.Payments;

namespace XCloud.Platform.WeChat.Job;

[UnitOfWork]
public class WechatPaymentJobs : XCloudAppService
{
    private readonly ISettingService _settingService;
    private readonly WechatPaymentCertificateManageService _wechatPaymentCertificateManageService;
    private readonly WechatClientFactory _wechatClientFactory;

    public WechatPaymentJobs(ISettingService settingService,
        WechatPaymentCertificateManageService wechatPaymentCertificateManageService,
        WechatClientFactory wechatClientFactory)
    {
        _settingService = settingService;
        _wechatPaymentCertificateManageService = wechatPaymentCertificateManageService;
        _wechatClientFactory = wechatClientFactory;
    }

    public async Task UpdateMchCertificateAsync()
    {
        var option = await this._settingService.GetWechatPaymentOptionAsync();
        if (option.Empty)
        {
            this.Logger.LogInformation("微信支付配置为空，跳过更新商户平台证书。");
            return;
        }

        var client = await this._wechatClientFactory.BuildTenPayClientAsync();

        await this._wechatPaymentCertificateManageService.UpdateMchCertificateEntriesAsync(client);
    }
}