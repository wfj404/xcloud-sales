﻿using Volo.Abp;

namespace XCloud.Core.DependencyInjection;

public static class IocExtension
{
    public static IDisposable SetEngineContextRootServiceProvider(this IServiceProvider provider)
    {
        if (provider == null)
            throw new ArgumentNullException(nameof(provider));

        if (EngineContext.Current.ServiceProvider != null)
            throw new NotSupportedException(nameof(SetEngineContextRootServiceProvider));

        EngineContext.Current.SetRootContainer(provider);

        return new DisposeAction(() => EngineContext.Current.Dispose());
    }

    //-------------------------------------------------------------------------

    public static IServiceCollection RemoveByFilter(this IServiceCollection collection,
        Func<ServiceDescriptor, bool> where)
    {
        var query = collection.AsEnumerable().Where(where);
        var removeList = query.ToArray();

        //这里最好tolist，防止query中的值被修改
        foreach (var m in removeList)
        {
            collection.Remove(m);
        }

        return collection;
    }
}