﻿using JetBrains.Annotations;

namespace XCloud.Core;

public class EngineContext : IDisposable
{
    private static EngineContext _context;

    [NotNull]
    public static EngineContext Current
    {
        get => _context ??= new EngineContext();
        set => _context = value;
    }

    [CanBeNull]
    public IServiceProvider ServiceProvider { get; private set; }

    [NotNull]
    public IServiceProvider RequiredServiceProvider =>
        ServiceProvider ?? throw new ArgumentNullException(nameof(ServiceProvider));

    public void SetRootContainer(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
    }

    /// <summary>
    /// 销毁所有组件
    /// </summary>
    public void Dispose()
    {
        //
    }
}