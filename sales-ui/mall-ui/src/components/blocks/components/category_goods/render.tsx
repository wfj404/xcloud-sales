import { take } from 'lodash-es';
import qs from 'query-string';
import { useEffect, useState } from 'react';
import { history } from 'umi';

import GoodsGrid from '@/components/blocks/components/goods_grid';
import GoodsItem from '@/components/blocks/components/goods_item';
import XLoading from '@/components/common/loading/skeleton';
import { apiClient } from '@/utils/client';
import { CategoryDto, GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Button, Typography } from '@mui/material';

import GoodsSlider from '../goods_slider';
import { CategoryGoodsBlockProps } from './utils';

export default ({ data }: { data: CategoryGoodsBlockProps }) => {
  const [category, _category] = useState<CategoryDto | undefined>(undefined);
  const [goods, _goods] = useState<GoodsDto[]>([]);
  const [loading, _loading] = useState(false);

  const finalCategory = category || data?.category;

  const queryCategoryGoods = (categoryId: string) => {
    if (isStringEmpty(categoryId)) {
      return;
    }

    _loading(true);
    apiClient.mall
      .searchSearchGoodsList({
        IsPublished: true,
        CategoryId: categoryId,
        PageSize: data?.count || 10,
        Page: 1,
        SkipCalculateTotalCount: true,
      })
      .then((res) => {
        _goods(res.data.Items || []);
      })
      .finally(() => _loading(false));
  };

  const queryCategory = (categoryId: string) => {
    //
  };

  const id = data?.category?.Id;
  useEffect(() => {
    if (id && !isStringEmpty(id)) {
      queryCategory(id);
      queryCategoryGoods(id);
    }
  }, [id]);

  if (finalCategory == null) {
    return null;
  }

  const renderGoods = () => {
    if (loading) {
      return <XLoading />;
    }
    const goods_list = take(goods, data.count || 1000);

    if (data.display_type == 'slider') {
      return <GoodsSlider goods={goods_list} />;
    } else if (data.display_type == 'grid') {
      return <GoodsGrid goods={goods_list} />;
    } else if (data.display_type == 'item') {
      return <GoodsItem goods={goods_list} />;
    }
    return <GoodsGrid goods={goods_list} />;
  };

  return (
    <>
      <Box
        sx={{
          py: 2,
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            px: 1,
          }}
        >
          <Box>
            <Typography variant="h4" sx={{}}>
              {finalCategory.Name || '--'}
            </Typography>
            {isStringEmpty(finalCategory.Description) || (
              <Typography variant="caption" color={'text.disabled'} sx={{}}>
                {finalCategory.Description}
              </Typography>
            )}
          </Box>
          <Button
            onClick={() => {
              finalCategory &&
                finalCategory.Id &&
                history.push({
                  pathname: '/category',
                  search: qs.stringify({ cat: `${finalCategory.Id}` }),
                });
            }}
          >
            更多
          </Button>
        </Box>
        {renderGoods()}
      </Box>
    </>
  );
};
