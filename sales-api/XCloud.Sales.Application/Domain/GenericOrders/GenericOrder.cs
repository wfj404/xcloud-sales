﻿namespace XCloud.Sales.Application.Domain.GenericOrders;

public class GenericOrder : SalesBaseEntity<string>, IHasCreationTime, IHasModificationTime
{
    public string OrderType { get; set; }

    public string OrderId { get; set; }

    public bool Paid { get; set; }

    public bool Serviced { get; set; }

    public bool ToService { get; set; }

    public bool Reviewed { get; set; }

    public bool Finished { get; set; }

    public DateTime? FinishedTime { get; set; }

    public bool Closed { get; set; }

    public DateTime? ClosedTime { get; set; }

    public string ItemsJson { get; set; }

    public string ExtraDataJson { get; set; }

    public int StoreUserId { get; set; }

    public DateTime? LastModificationTime { get; set; }

    public DateTime CreationTime { get; set; }
}