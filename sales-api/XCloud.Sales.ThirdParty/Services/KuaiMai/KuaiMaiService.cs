﻿using System;
using System.Net.Http;
using Volo.Abp.DependencyInjection;
using XCloud.Sales.Application.Service;

namespace XCloud.Sales.ThirdParty.Services.KuaiMai;

[ExposeServices(typeof(KuaiMaiService))]
public class KuaiMaiService : SalesAppService
{
    private readonly HttpClient _httpClient;

    public KuaiMaiService(IHttpClientFactory httpClientFactory)
    {
        this._httpClient = httpClientFactory.CreateClient(nameof(KuaiMaiService));
        this._httpClient.BaseAddress = new Uri(KuaiMaiConstrant.Endpoint.TrimEnd('/').TrimEnd('\\'));
    }
}