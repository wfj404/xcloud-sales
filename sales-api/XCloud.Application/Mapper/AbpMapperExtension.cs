using System;
using System.Collections.Generic;
using System.Linq;
using Volo.Abp.ObjectMapping;

namespace XCloud.Application.Mapper;

public static class AbpMapperExtension
{
    public static Target MapTo<T, Target>(this T source, IObjectMapper mapper)
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));

        if (mapper == null)
            throw new ArgumentNullException(nameof(mapper));

        var target = mapper.Map<T, Target>(source);

        return target;
    }

    public static Target[] MapArrayTo<T, Target>(this IEnumerable<T> source, IObjectMapper mapper)
    {
        if (mapper == null)
            throw new ArgumentNullException(nameof(mapper));

        if (source == null)
            throw new ArgumentNullException(nameof(source));

        var data = source.Select(mapper.Map<T, Target>).ToArray();

        return data;
    }
}