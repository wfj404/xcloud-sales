using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AutoMapper;
using Volo.Abp.Collections;
using Volo.Abp.Modularity;

namespace XCloud.Application.Extension;

public static class ApplicationExtension
{
    public static void TryRemove<T, TypeToRemove>(this ITypeList<T> list)
        where TypeToRemove : T
    {
        if (list.Contains<TypeToRemove>())
        {
            list.Remove<TypeToRemove>();
        }
    }

    private static void ConfigureAbpAutoMapperOption(this ServiceConfigurationContext context,
        Action<AbpAutoMapperOptions> option)
    {
        if (option == null)
            throw new ArgumentNullException(nameof(option));

        context.Services.Configure<AbpAutoMapperOptions>(option);
    }

    public static void AddModuleAutoMapperProfile<T>(this ServiceConfigurationContext context) where T : AbpModule
    {
        context.ConfigureAbpAutoMapperOption(option => option.AddMaps<T>(false));
    }

    public static void AddAutoMapperProfile<T>(this ServiceConfigurationContext context) where T : Profile, new()
    {
        context.ConfigureAbpAutoMapperOption(option => option.AddProfile<T>(false));
    }
}