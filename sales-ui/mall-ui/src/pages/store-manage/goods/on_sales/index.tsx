import { useRequest } from 'ahooks';
import { Badge, Button, Card, Table, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import { getSkuName } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import {
  QueryStoreGoodsMappingPagingInput,
  StoreGoodsMappingDto,
} from '@/utils/swagger';
import { formatMoney } from '@/utils/utils';

import XForm from './form';
import SearchForm from './search_form';

export default () => {
  const query_request = useRequest(apiClient.mallManager.skuQueryPaging, {
    manual: true,
  });
  const data = query_request.data?.data || {};

  const [query, _query] = useState<QueryStoreGoodsMappingPagingInput>({});

  const [formData, _formData] = useState<StoreGoodsMappingDto | undefined>(
    undefined,
  );

  const queryList = (q: QueryStoreGoodsMappingPagingInput) => {
    query_request.run(q);
  };

  const columns: ColumnProps<StoreGoodsMappingDto>[] = [
    {
      title: '名称',
      render: (d, x) => `${getSkuName(x.Sku || {})}`,
    },
    {
      title: '库存',
      render: (d, x) => {
        if ((x.StockQuantity || 0) <= 0) {
          return (
            <Badge status={'error'} text={<span>{x.StockQuantity}</span>} />
          );
        }
        return x.StockQuantity;
      },
    },
    {
      title: '价格',
      render: (d, x) => {
        if (x.OverridePrice) {
          return (
            <Tag color={'green'}>门店定价：{formatMoney(x.Price || 0)}</Tag>
          );
        }
        return (
          <Tag color={'warning'}>
            <b>{formatMoney(x.Sku?.Price || 0)}</b>
          </Tag>
        );
      },
    },
    {
      title: '在售状态',
      render: (d, x) => {
        if (!x.Sku?.IsActive) {
          return <Badge status={'error'} text={'SKU不可用'} />;
        }
        if (!x.Sku?.Goods?.Published) {
          return <Badge status={'error'} text={'基础商品不可用'} />;
        }

        return (
          <>
            {x.Active && <Badge status={'success'} text={'可用'} />}
            {x.Active || <Badge status={'error'} text={'不可用'} />}
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(x);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList({
      ...query,
      Page: 1,
    });
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList({
            Page: 1,
          });
        }}
      />
      <SearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card title="在售商品">
        <Table
          rowKey={(x) => x.Id || ''}
          loading={query_request.loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            total: data.TotalCount,
            pageSize: data.PageSize,
            onChange: (e) => {
              const p: QueryStoreGoodsMappingPagingInput = {
                ...query,
                Page: e,
              };
              _query(p);
              queryList(p);
            },
          }}
        />
      </Card>
    </>
  );
};
