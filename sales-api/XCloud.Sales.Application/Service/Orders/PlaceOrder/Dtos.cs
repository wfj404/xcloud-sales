using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Orders.PlaceOrder;

public class PlaceOrderRequestItemDto : IEntityDto
{
    public string SkuId { get; set; }

    public int Quantity { get; set; }

    public string Remark { get; set; }

    public int[] ValueAddedItemIds { get; set; }
}

public class PlaceOrderRequestDto : IEntityDto
{
    public string StoreId { get; set; }

    public DateTime? PreferDeliveryStartTime { get; set; }

    public DateTime? PreferDeliveryEndTime { get; set; }

    public string PaymentMethodId { get; set; }

    public string ShippingMethodId { get; set; }

    public string DeliveryType { get; set; }

    public int UserId { get; set; }

    public int UserCouponId { get; set; }

    public string AddressId { get; set; }

    public string InvoiceId { get; set; }

    public string Remark { get; set; }

    public string PlatformId { get; set; }

    public PlaceOrderRequestItemDto[] Items { get; set; }
}

public class PlaceOrderResult : ApiResponse<PlaceOrderDataHolder>
{
    //
}

public class PlaceOrderSkuError : MultipleErrorsDto<SkuDto, string>, IEntityDto<string>
{
    public string Id { get; set; }
}

public class PlaceOrderDataHolder : MultipleErrorsDto<OrderDto, string>, IEntityDto
{
    private IList<PlaceOrderSkuError> _skuErrors;

    [NotNull]
    public IList<PlaceOrderSkuError> SkuErrors
    {
        get => _skuErrors ??= new List<PlaceOrderSkuError>();
        set => _skuErrors = value;
    }

    [NotNull]
    public PlaceOrderSkuError GetOrCreateSkuError(string skuId, SkuDto dto = default)
    {
        if (string.IsNullOrWhiteSpace(skuId))
            throw new ArgumentNullException(nameof(skuId));

        var error = GetSkuErrorById();

        if (error == null)
        {
            lock (this)
            {
                error = GetSkuErrorById();
                if (error == null)
                {
                    error = new PlaceOrderSkuError { Id = skuId, Data = dto };
                    SkuErrors.Add(error);
                }
            }
        }

        return error;

        PlaceOrderSkuError GetSkuErrorById()
        {
            return SkuErrors.FirstOrDefault(x => x.Id == skuId);
        }
    }

    private IList<string> _paymentErrors;

    [NotNull]
    public IList<string> PaymentErrors
    {
        get => _paymentErrors ??= new List<string>();
        set => _paymentErrors = value;
    }

    private IList<string> _shippingErrors;

    [NotNull]
    public IList<string> ShippingErrors
    {
        get => _shippingErrors ??= new List<string>();
        set => _shippingErrors = value;
    }

    private IList<string> _couponErrors;

    [NotNull]
    public IList<string> CouponErrors
    {
        get => _couponErrors ??= new List<string>();
        set => _couponErrors = value;
    }

    public PlaceOrderRequestDto Request { get; set; }

    public OrderDto Order { get; set; }

    public DateTime Now { get; set; }
}