using System.Threading.Tasks;
using XCloud.Core.Json;

namespace XCloud.Platform.Application.Service.Settings;

public static class SettingsExtension
{
    public static async Task SaveObjectAsync<T>(this IKeyValueService keyValueService, string name, T obj)
        where T : class
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));
        if (obj == null)
            throw new ArgumentNullException(nameof(obj));

        var json = keyValueService.XJsonDataSerializer.SerializeToString(obj);

        await keyValueService.SaveSettingsAsync(name, json);
    }

    public static async Task<T> GetObjectOrDefaultAsync<T>(this IKeyValueService keyValueService, string name)
        where T : class
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        var json = await keyValueService.GetSettingValueByNameAsync(name);

        if (!string.IsNullOrWhiteSpace(json))
        {
            var obj = keyValueService.XJsonDataSerializer.DeserializeFromStringOrDefault<T>(json,
                keyValueService.XLogger);

            if (obj != null)
                return obj;
        }

        return default;
    }
}