import React, { useState } from 'react';

import {
    closestCenter, DndContext, DragEndEvent, DragOverlay, KeyboardSensor, PointerSensor, useSensor,
    useSensors
} from '@dnd-kit/core';
import {
    arrayMove, SortableContext, sortableKeyboardCoordinates, verticalListSortingStrategy
} from '@dnd-kit/sortable';

import Draggable from './drag';
import Droppable from './drop';
import { SortableItem } from './item';

export default function App() {
  const [items, setItems] = useState(['1', '2', '3', '4']);
  const [activeId, setActiveId] = useState<string | null | undefined>(null);
  const [overId, setOverId] = useState<string | null | undefined>(null);
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    }),
  );

  const renderItem = (id: string, selected?: boolean) => {
    return (
      <div
        style={{
          padding: 10,
          border: `2px solid black`,
          margin: 10,
          background: selected ? 'red' : 'none',
        }}
      >
        {selected && <b>{id}</b>}
        {selected || <div>{id}</div>}
      </div>
    );
  };

  const getItems = () => {
    let datalist = [...items];

    if (overId) {
      const i = datalist.indexOf(overId);
      if (i >= 0) {
        datalist = [
          ...datalist.slice(0, i),
          'drop-here',
          ...datalist.slice(i, undefined),
        ];
      }
    }

    return datalist;
  };

  const itemsxx = getItems();

  return (
    <DndContext
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragStart={(e) => {
        setActiveId(e.active.id as string);
      }}
      onDragEnd={handleDragEnd}
      //onDragMove={e=>{}}
      onDragOver={(e) => {
        const { active, over } = e;
        if (!active || !over) {
          return;
        }
        if (over.id == 'drop-here') {
          return;
        }
        console.log(new Date().toString(), e.over?.rect);
        const isBelowOverItem =
          over &&
          active.rect.current.translated &&
          active.rect.current.translated.top > over.rect.top + over.rect.height;
        console.log(isBelowOverItem);

        setOverId(over.id as string);
      }}
      onDragCancel={(e) => {
        console.log('cancel', e);
      }}
    >
      <Droppable>
        <Draggable>Drag me</Draggable>
      </Droppable>
      <SortableContext
        id="blocks_list"
        items={itemsxx}
        strategy={verticalListSortingStrategy}
      >
        {itemsxx.map((id) => (
          <SortableItem key={id} id={id.toString()}>
            {renderItem(id)}
          </SortableItem>
        ))}
      </SortableContext>
      <DragOverlay>{activeId ? renderItem(activeId, true) : null}</DragOverlay>
    </DndContext>
  );

  function handleDragEnd(event: DragEndEvent) {
    console.log('end', event);
    const { active, over } = event;

    if (!over || !active) {
      return;
    }
    setOverId(over.id as string);

    if (active.id !== over.id) {
      setItems((items) => {
        const oldIndex = items.indexOf(active.id as string);
        const newIndex = items.indexOf(over.id as string);

        return arrayMove(items, oldIndex, newIndex);
      });
    }
  }
}
