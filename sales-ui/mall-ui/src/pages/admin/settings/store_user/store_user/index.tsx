import { Button, Card, message, Select, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import { history, useSnapshot } from 'umi';

import XTime from '@/components/common/time';
import RenderTime from '@/components/common/time/render_time';
import XUserAvatar from '@/components/manage/user/avatar';
import { storeState, StoreType } from '@/store';
import { isSh3H, PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { QueryUserPagingInput, StoreUserDto } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XBalance from './balance';
import XSearchForm from './search_form';
import XStatus from './update_status';

export default () => {
  const [query, _query] = useState<QueryUserPagingInput>({
    Page: 1,
  });
  const [loading, _loading] = useState(false);
  const [loadingSetGradeId, _loadingSetGradeId] = useState(0);
  const [data, _data] = useState<PagedResponse<StoreUserDto>>({});

  const app = useSnapshot(storeState) as StoreType;

  const queryList = (queryParam: QueryUserPagingInput) => {
    _loading(true);

    apiClient.mallAdmin
      .userQueryPaging({ ...queryParam })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const setUserGrade = (
    row: StoreUserDto,
    gradeId: string | undefined | null,
  ) => {
    if (!confirm('确定设置？')) {
      return;
    }
    _loadingSetGradeId(row.Id || 0);
    apiClient.mallAdmin
      .gradeSetUserGrade({
        UserId: row.Id,
        GradeId: gradeId,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('设置成功');
          queryList(query);
        });
      })
      .finally(() => {
        _loadingSetGradeId(0);
      });
  };

  const columns: ColumnProps<StoreUserDto>[] = [
    {
      title: '账号主体',
      render: (d, x: StoreUserDto) => <XUserAvatar model={x.User} />,
    },
    {
      key: 'user-grade',
      title: '会员等级',
      render: (d, x: StoreUserDto) => {
        return (
          <Select
            size={'small'}
            loading={loadingSetGradeId == x.Id}
            onChange={(e) => {
              setUserGrade(x, e as string);
            }}
            value={x.Grade?.Id}
            options={app.grades.map((x) => ({
              key: x.Id,
              label: x.Name,
              value: x.Id,
            }))}
            allowClear
            placeholder="选择会员等级"
            style={{
              minWidth: 150,
            }}
          ></Select>
        );
      },
    },
    {
      title: '手机号',
      render: (d, x: StoreUserDto) => x.User?.UserMobile?.MobilePhone || '--',
    },
    {
      key: 'balance',
      title: '账户余额',
      render: (d, x: StoreUserDto) => (
        <XBalance
          model={x}
          ok={() => {
            queryList(query);
          }}
        />
      ),
    },
    {
      title: '余额积分',
      render: (d, x: StoreUserDto) => x.Points,
    },
    {
      title: '历史积分',
      render: (d, x: StoreUserDto) => x.HistoryPoints,
    },
    {
      title: '状态',
      render: (d, x: StoreUserDto) => (
        <XStatus
          model={x}
          ok={() => {
            queryList(query);
          }}
        />
      ),
    },
    {
      title: '最近上线时间',
      render: (d, x: StoreUserDto) => {
        return <RenderTime timeStr={x.LastActivityTime} />;
      },
    },
    {
      title: '时间',
      render: (d, x: StoreUserDto) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x: StoreUserDto) => {
        return (
          <Button.Group>
            <Button
              type={'link'}
              onClick={() => {
                alert('todo');
              }}
            >
              查看
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  const columns_hide_for_3h: any[] = ['user-grade'];
  const sh3h_columns = columns.filter(
    (x) => columns_hide_for_3h.indexOf(x.key) <= 0,
  );

  useEffect(() => {
    app.queryGrades().finally(() => {
      queryList(query);
    });
  }, []);

  return (
    <>
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card
        title="商城会员"
        loading={loading}
        style={{
          marginBottom: 10,
        }}
        extra={
          <Button
            type="text"
            onClick={() => {
              history.push('/admin/settings/user/list');
            }}
          >
            查看平台用户信息
          </Button>
        }
      >
        <Table
          dataSource={data.Items || []}
          columns={isSh3H() ? sh3h_columns : columns}
          pagination={{
            pageSize: data.PageSize,
            total: data.TotalCount,
            onChange: (page, size) => {
              let p: QueryUserPagingInput = {
                ...query,
                Page: page,
              };
              _query(p);
              queryList(p);
            },
          }}
        />
      </Card>
    </>
  );
};
