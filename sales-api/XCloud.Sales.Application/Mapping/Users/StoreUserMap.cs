using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Users;

namespace XCloud.Sales.Application.Mapping.Users;

public class StoreUserMap : SalesEntityTypeConfiguration<StoreUser>
{
    public override void Configure(EntityTypeBuilder<StoreUser> builder)
    {
        builder.ToTable("sales_store_user");
        
        builder.PreConfigIntPrimaryKey();
        builder.PreConfigModificationTimeEntity();
        
        builder.Property(x => x.UserId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Balance).HasPrecision(18, 2);
        builder.Property(x => x.Points);
        builder.Property(x => x.HistoryPoints);

        base.Configure(builder);
    }
}