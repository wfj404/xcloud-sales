﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.GiftCards;
using XCloud.Sales.Application.Service.GiftCards;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/gift-card")]
public class GiftCardController : ShopBaseController
{
    private readonly IGiftCardService _prepaidCardService;
    private readonly IStoreUserService _storeUserService;

    public GiftCardController(IGiftCardService prepaidCardService, IStoreUserService storeUserService)
    {
        _prepaidCardService = prepaidCardService;
        _storeUserService = storeUserService;
    }

    [HttpPost("unused-total")]
    public async Task<ApiResponse<decimal>> QueryUnusedTotalAsync()
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageFinance);

        var total = await _prepaidCardService.QueryUnusedAmountTotalAsync();

        return new ApiResponse<decimal>(total);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<GiftCardDto>> QueryPagingAsync([FromBody] QueryGiftCardPagingInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageFinance);

        var response = await _prepaidCardService.QueryPagingAsync(dto);

        if (response.IsNotEmpty)
        {
            await this._prepaidCardService.AttachStoreUsersAsync(response.Items.ToArray());
            
            var storeUsers = response.Items.Select(x => x.StoreUser).WhereNotNull().ToArray();

            await this._storeUserService.AttachPlatformUsersAsync(storeUsers);
        }

        return response;
    }

    [HttpPost("create")]
    public async Task<ApiResponse<GiftCard>> CreateAsync([FromBody] GiftCardDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageFinance);

        var entity = await _prepaidCardService.CreateGiftCardAsync(dto);

        return new ApiResponse<GiftCard>(entity);
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateGiftCardStatusInput dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageFinance);

        await _prepaidCardService.UpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }
}