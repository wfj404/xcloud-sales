import { DeliveryRecordDto } from '@/utils/swagger';
import { Badge } from 'antd';

export default ({ data }: { data: DeliveryRecordDto }) => {
  const x = data;

  if (!x.Delivering) {
    return <Badge status={'default'} text={'待发货'} />;
  }

  if (!x.Delivered) {
    return <Badge status={'default'} text={'配送中'} />;
  } else {
    return <Badge status={'success'} text={`已送达`} />;
  }
};
