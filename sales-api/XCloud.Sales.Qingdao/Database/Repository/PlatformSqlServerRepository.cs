﻿using System;
using XCloud.EntityFrameworkCore.Repository;
using XCloud.Platform.Application.Domain;
using XCloud.Platform.Application.Repository;
using XCloud.Sales.Qingdao.Database.EntityFrameworkCore;

namespace XCloud.Sales.Qingdao.Database.Repository;

/// <summary>
/// 会员中心仓储实现
/// </summary>
public class PlatformSqlServerRepository<T> : EfRepositoryBase<ShopSqlServerDbContext, T>, IPlatformRepository<T> where T : class, IPlatformBaseEntity
{
    public PlatformSqlServerRepository(IServiceProvider provider) : base(provider) { }
}