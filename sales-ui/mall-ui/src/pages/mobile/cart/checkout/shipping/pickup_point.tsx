import { Card, ErrorBlock } from 'antd-mobile';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { flex_row_start } from '@/utils/biz';
import { LocationOnOutlined } from '@mui/icons-material';
import { Box, Typography } from '@mui/material';

export default () => {
  const store = useSnapshot(storeState) as StoreType;

  const currentStore = store.currentStore;

  if (!currentStore) {
    return <ErrorBlock title="请选择下单门店" />;
  }

  return (
    <>
      <Card title="自提点">
        <div style={{ ...flex_row_start, justifyContent: 'flex-start' }}>
          <LocationOnOutlined fontSize={'large'} />
          <Box sx={{ ml: 1 }} flexGrow={1}>
            <Typography variant="body1" component={'div'}>
              {currentStore.Name}
            </Typography>
            <Typography variant="caption" color="text.disabled">
              {currentStore.AddressDetail}
            </Typography>
          </Box>
        </div>
      </Card>
    </>
  );
};
