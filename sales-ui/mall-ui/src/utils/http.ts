import { message as antMessage } from 'antd';
import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { history } from 'umi';

import { ApiResponse, redirectToLogin } from '@/utils/biz';
import { config } from '@/utils/config';

import {
  concatUrl,
  getAccessToken,
  getSelectedStoreId,
  GlobalContainer,
} from './utils';

const message = GlobalContainer.message || antMessage;

export const httpCodeMessage: Record<number, string> = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  405: '请求方法不被允许。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

const errorHandler = (error: AxiosError) => {
  const { response } = error;
  const currentPath = history.location.pathname;
  console.log('errorHandler', error, response, currentPath);

  if (!response) {
    message.error('无法请求网络');
    return;
  }

  if (response.status === 401) {
    const store = GlobalContainer.store;
    if (store == undefined) {
      message.error('登录已经过期，即将跳转登录！').then((res) => {
        redirectToLogin();
      });
    } else {
      message.error('登录已经过期').then((res) => {
        store.setUserAuthResult({});
        store.setStoreUserAuthResult({});
        store.setStoreManagerAuthResult({});
        store.setAdminAuthResult({});
      });
    }
  } else if (response.status == 403) {
    message.error('无权限操作');
  } else {
    message.error(
      `请求错误 ${httpCodeMessage[response.status] || response.status}`,
    );
  }
};

const responseHandler = (data: AxiosResponse<ApiResponse<object>, any>) => {
  //console.log('response data:', data);
  let error = data.data?.Error;
  if (!error) {
    return;
  }
};

const interceptRequest = (request: AxiosInstance): AxiosInstance => {
  request.interceptors.request.use(
    (request) => {
      request.headers.set('x-client', 'mall-ui');

      const token = getAccessToken();
      if (token && token.length > 0) {
        request.headers.Authorization = `Bearer ${token}`;
      }

      const storeId = getSelectedStoreId();
      if (storeId && storeId.length > 0) {
        request.headers.set('x-selected-store', storeId);
      }

      return request;
    },
    (err: AxiosError) => Promise.reject(err),
  );
  return request;
};

const interceptResponse = (request: AxiosInstance): AxiosInstance => {
  request.interceptors.response.use(
    (response) => {
      response.data = response.data || {};
      responseHandler(response.data);

      return response;
    },
    (err: AxiosError) => {
      errorHandler(err);
      return Promise.reject(err);
    },
  );
  return request;
};

//--------------
export const httpClient: AxiosInstance = (function () {
  const axiosInstance = axios.create({
    baseURL: concatUrl([config.apiGateway]),
  });
  interceptRequest(axiosInstance);
  interceptResponse(axiosInstance);
  return axiosInstance;
})();
