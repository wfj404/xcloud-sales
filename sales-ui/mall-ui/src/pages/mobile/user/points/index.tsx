import { useInfiniteScroll } from 'ahooks';
import React, { useEffect, useState } from 'react';

import ScrollLoading from '@/components/common/scroll/loading';
import XHeader from '@/components/mobile/simpleHeader';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { PointsHistoryDto, QueryPointsPagingInput } from '@/utils/swagger';
import { Box } from '@mui/material';

import XBalanceAndPointsMenu from '../ucenter/balance';
import XItem from './item';

export default function () {
  const [finalQuery, _finalQuery] = React.useState<QueryPointsPagingInput>({});

  const { loading, loadingMore, mutate, noMore, data } = useInfiniteScroll<{
    list: PointsHistoryDto[];
    res?: PagedResponse<PointsHistoryDto>;
  }>(
    async (currentData) => {
      const res = await apiClient.mall.userQueryPointsPaging({
        ...finalQuery,
        Page: (currentData?.res?.PageIndex || 0) + 1,
      });

      return {
        list: res.data.Items || [],
        res: res.data,
      };
    },
    {
      target: window.document,
      isNoMore: (x) => x?.res?.IsEmpty || false,
      reloadDeps: [finalQuery],
    },
  );

  const items = data?.list || [];

  React.useEffect(() => {
    mutate({
      list: [],
    });
  }, [finalQuery]);

  return (
    <>
      <XHeader>
        <Box sx={{ my: 2 }}>
          <XBalanceAndPointsMenu />
        </Box>
        <Box sx={{ backgroundColor: 'white' }}>
          {items.map((x, index) => (
            <Box key={index}>
              <XItem model={x} />
            </Box>
          ))}
        </Box>
        <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
      </XHeader>
    </>
  );
}
