﻿using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Caching.Memory;
using XCloud.AspNetMvc.Filters;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Queue;
using XCloud.Sales.Application.Service.Logging;

namespace XCloud.Sales.Application.Framework;

[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
public class StoreAuditLogAttribute : Attribute
{
    //
}

[ExposeServices(
    typeof(StoreAuditLoggingFilter),
    typeof(IAsyncActionFilter)
)]
public class StoreAuditLoggingFilter : IAsyncActionFilter, IScopedDependency
{
    private readonly ILogger _logger;
    private readonly IMemoryCache _memoryCache;
    private readonly SalesEventBusService _salesEventBusService;
    private readonly IRequestCacheProvider _requestCacheProvider;

    public StoreAuditLoggingFilter(SalesEventBusService salesEventBusService,
        ILogger<StoreAuditLoggingFilter> logger,
        IMemoryCache memoryCache,
        IRequestCacheProvider requestCacheProvider)
    {
        _salesEventBusService = salesEventBusService;
        _logger = logger;
        _memoryCache = memoryCache;
        _requestCacheProvider = requestCacheProvider;
    }

    private bool AuditLoggingIsEnabled(ActionExecutingContext context)
    {
        if (!context.ActionDescriptor.IsControllerAction())
        {
            return false;
        }

        var actionMethod = context.ActionDescriptor.GetMethodInfo();

        return _memoryCache.GetOrCreate(actionMethod, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);

            var classAttr = actionMethod.DeclaringType.GetCustomAttributesList<StoreAuditLogAttribute>();
            var methodAttr = actionMethod.GetCustomAttributesList<StoreAuditLogAttribute>();
            var actionMethodAttr = actionMethod.GetCustomAttributesList<HttpMethodAttribute>();

            return actionMethod.IsPublic && actionMethodAttr.Any() &&
                   (classAttr.Any() || methodAttr.Any());
        });
    }

    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var actionExecutedContext = await next.Invoke();

        try
        {
            if (AuditLoggingIsEnabled(context))
            {
                var log = new ActivityLog
                {
                    UserId = _requestCacheProvider.GetObject<UserAuthResult>()?.Data?.Id,
                    AdministratorId = _requestCacheProvider.GetObject<AdminAuthResult>()?.Data?.Id,
                    StoreUserId = _requestCacheProvider.GetObject<StoreUserAuthResult>()?.Data?.Id ?? 0,
                    ManagerId = _requestCacheProvider.GetObject<StoreManagerAuthResult>()?.Data?.Id,
                    Comment = nameof(StoreAuditLoggingFilter),
                    ActivityLogTypeId = ActivityLogType.AuditLog,
                    Data = context.GetHttpContextInfoToPrint(actionExecutedContext),
                };

                await _salesEventBusService.NotifyInsertActivityLog(log);
            }
        }
        catch (Exception e)
        {
            this._logger.LogError(message: e.Message, exception: e);
        }
    }
}