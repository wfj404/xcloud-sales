﻿using JetBrains.Annotations;

namespace XCloud.Sales.Application.Service.Payments;

public static class PaymentsExtension
{
    [NotNull]
    [ItemNotNull]
    public static async Task<PaymentBillDto> GetRequiredPaidBillByIdAsync(
        this IOrderPaymentBillService orderPaymentBillService,
        string billId)
    {
        var bill = await orderPaymentBillService.GetPaidBillByIdAsync(billId);

        return bill ?? throw new EntityNotFoundException(nameof(GetRequiredPaidBillByIdAsync));
    }

    [NotNull]
    [ItemNotNull]
    public static async Task<PaymentBillDto> GetRequiredBillReadyForPaymentAsync(
        this IOrderPaymentBillService orderPaymentBillService, string billId)
    {
        var bill = await orderPaymentBillService.GetBillReadyForPaymentAsync(billId);

        return bill ?? throw new EntityNotFoundException(nameof(GetRequiredBillReadyForPaymentAsync));
    }
}