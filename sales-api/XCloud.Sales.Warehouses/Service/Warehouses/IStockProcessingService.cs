using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Domain.Entities;
using XCloud.Application.DistributedLock;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Service.Warehouses;

public interface IStockProcessingService : ISalesAppService
{
    //
}

public class StockProcessingService : SalesAppService, IStockProcessingService
{
    private readonly ISalesRepository<Stock> _salesRepository;
    private readonly ISalesRepository<StockItem> _stockItemRepository;
    private readonly IStockUsageHistoryService _stockUsageHistoryService;
    private readonly IStoreGoodsStockService _goodsStockService;
    private readonly IStockService _stockService;
    private readonly IStockItemService _stockItemService;

    public StockProcessingService(ISalesRepository<Stock> salesRepository,
        IStockUsageHistoryService stockUsageHistoryService,
        IStoreGoodsStockService goodsStockService, IStockService stockService,
        ISalesRepository<StockItem> stockItemRepository, IStockItemService stockItemService)
    {
        _stockUsageHistoryService = stockUsageHistoryService;
        _goodsStockService = goodsStockService;
        _stockService = stockService;
        _stockItemRepository = stockItemRepository;
        _stockItemService = stockItemService;
        _salesRepository = salesRepository;
    }

    public async Task RevertStockDeductionAsync(string orderId)
    {
        var db = await _salesRepository.GetDbContextAsync();
        throw new NotImplementedException();
    }

    private async Task<int> DeductStockAsync(string orderItemId, string stockItemId, int quantity)
    {
        if (string.IsNullOrWhiteSpace(orderItemId))
            throw new ArgumentNullException(nameof(orderItemId));

        if (string.IsNullOrWhiteSpace(stockItemId))
            throw new ArgumentNullException(nameof(stockItemId));

        if (quantity <= 0)
            throw new ArgumentNullException(nameof(quantity));

        var db = await _salesRepository.GetDbContextAsync();

        var entity = await db.Set<StockItem>().FirstOrDefaultAsync(x => x.Id == stockItemId);

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        var availableQuantity = entity.Quantity - entity.DeductQuantity;

        if (availableQuantity <= 0)
            return 0;

        var deductQuantity = Math.Min(quantity, availableQuantity);

        entity.DeductQuantity += deductQuantity;
        entity.LastModificationTime = Clock.Now;

        await _stockItemRepository.UpdateAsync(entity);

        var history = new StockUsageHistoryDto
        {
            StoreId = orderItemId,
            StockItemId = entity.Id,
            Quantity = deductQuantity,
        };

        await _stockUsageHistoryService.InsertAsync(history);

        return deductQuantity;
    }

    private async Task<StockItem[]> QueryStockItemsBySkuIdAsync(string combinationId)
    {
        if (string.IsNullOrWhiteSpace(combinationId))
            throw new ArgumentNullException(nameof(combinationId));

        var db = await _salesRepository.GetDbContextAsync();

        var now = Clock.Now;

        var query = from item in db.Set<StockItem>().AsNoTracking()
            join stock in db.Set<Stock>().AsNoTracking()
                on item.StockId equals stock.Id
            select new { item, stock };

        query = query.Where(x => x.stock.Approved);
        //query = query.Where(x => x.stock.ExpirationTime > now);
        query = query
            .Where(x => x.item.SkuId == combinationId)
            .Where(x => x.item.DeductQuantity < x.item.Quantity);

        var warehouseStocks = await query
            .OrderBy(x => x.stock.CreationTime)
            .Select(x => x.item)
            .ToArrayAsync();

        return warehouseStocks;
    }

    public async Task DeductStockAsync(DeductStockInput dto)
    {
        if (string.IsNullOrWhiteSpace(dto.OrderItemId) || string.IsNullOrWhiteSpace(dto.SkuId) || dto.Quantity <= 0)
            throw new ArgumentNullException(nameof(DeductStockAsync));

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(DeductStockAsync)}.{dto.SkuId}",
            expiryTime: TimeSpan.FromSeconds(5));

        var warehouseStocks = await QueryStockItemsBySkuIdAsync(dto.SkuId);

        var totalStockToDeduct = dto.Quantity;

        foreach (var stockEntity in warehouseStocks)
        {
            var deductNumber = await DeductStockAsync(dto.OrderItemId, stockEntity.Id, totalStockToDeduct);

            //stock run out
            if (deductNumber <= 0)
                break;

            //update current stock to deduct
            totalStockToDeduct -= deductNumber;

            //check if deduct is done
            if (totalStockToDeduct <= 0)
                break;
        }

        if (totalStockToDeduct > 0)
        {
            throw new UserFriendlyException("stock is not enough");
        }
    }
}