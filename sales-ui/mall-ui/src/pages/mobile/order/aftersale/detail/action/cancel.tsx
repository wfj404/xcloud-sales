import React, { useState } from 'react';

import { apiClient } from '@/utils/client';
import { AfterSalesDto } from '@/utils/swagger';
import { errorMsg, handleResponse, isStringEmpty } from '@/utils/utils';
import { LoadingButton } from '@mui/lab';
import { TextField } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function (props: {
  model: AfterSalesDto;
  ok: any;
  show: boolean;
  hide: any;
}) {
  const { model, ok, show, hide } = props;

  const [loadingSave, _loadingSave] = React.useState(false);

  const [comment, _comment] = React.useState('');

  const save = () => {
    if (isStringEmpty(comment)) {
      errorMsg('请输入取消理由');
      return;
    }

    _loadingSave(true);
    apiClient.mall
      .afterSaleCancel({
        Id: model.Id,
        Comment: comment,
      })
      .then((res) => {
        handleResponse(res, () => {
          ok && ok();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  return (
    <>
      <Dialog open={show} onClose={() => hide && hide()} fullWidth>
        <DialogTitle>取消订单</DialogTitle>
        <DialogContent>
          <DialogContentText>确认取消售后订单吗？</DialogContentText>
          <TextField
            autoFocus
            label="取消理由"
            multiline
            fullWidth
            value={comment}
            onChange={(e) => _comment(e.target.value)}
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => hide && hide()}>取消</Button>
          <LoadingButton
            onClick={() => {
              save();
            }}
            loading={loadingSave}
          >
            确认取消
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
}
