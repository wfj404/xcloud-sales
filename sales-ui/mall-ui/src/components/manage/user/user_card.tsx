import { Avatar, Space } from 'antd';

import { resolveAvatar } from '@/utils/storage';
import { UserDto } from '@/utils/swagger';
import { simpleString } from '@/utils/utils';

export default ({ model }: { model?: UserDto | null }) => {
  if (!model) {
    return null;
  }

  const avatarUrl = resolveAvatar(model.Avatar, {
    width: 100,
    height: 100,
  });

  const nickName = simpleString(
    [model.NickName, model.IdentityName, model.Id]
      .filter((x) => x && x.length > 0)
      .at(0) || '--',
    10,
  );

  return (
    <>
      <Space direction="horizontal">
        <div>
          <Avatar src={avatarUrl} size={100} />
        </div>
        <div>
          <p>昵称：{nickName}</p>
          <p>用户名：{model?.IdentityName || '--'}</p>
          <p>手机号：{model?.UserMobile?.MobilePhone || '--'}</p>
        </div>
      </Space>
    </>
  );
};
