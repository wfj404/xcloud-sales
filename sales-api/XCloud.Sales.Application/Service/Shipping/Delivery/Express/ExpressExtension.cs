﻿using XCloud.Core.Helper;
using XCloud.Platform.Application.Domain.Address;

namespace XCloud.Sales.Application.Service.Shipping.Delivery.Express;

public static class ExpressExtension
{
    private static int GetMatchScore(this DeliveryArea area, UserAddress userAddress)
    {
        if (string.IsNullOrWhiteSpace(area.ProvinceId))
        {
            return default;
        }

        var addressList = new string[]
        {
            userAddress.ProvinceCode,
            userAddress.CityCode,
            userAddress.AreaCode
        };

        var areaList = new string[]
        {
            area.ProvinceId,
            area.CityId,
            area.AreaId
        };

        var score = default(int);

        for (var i = 0; i < addressList.Length; ++i)
        {
            var areaCode = areaList[i];
            var addressCode = addressList[i];

            if (string.IsNullOrWhiteSpace(areaCode) || string.IsNullOrWhiteSpace(addressCode))
            {
                break;
            }

            if (areaCode == addressCode)
            {
                ++score;
                //next compare
                continue;
            }
            else
            {
                //stop compare
                break;
            }
        }

        return score;
    }

    public static int GetMaxMatchScore(this DeliveryArea[] areas, UserAddress userAddress)
    {
        if (userAddress == null)
            throw new ArgumentNullException(nameof(userAddress));

        if (ValidateHelper.IsEmptyCollection(areas))
        {
            return default;
        }

        return areas.Max(x => GetMatchScore(x, userAddress));
    }

    public static DeliveryRule SelectDeliveryRuleOrDefault(this DeliveryRule[] rules, UserAddress userAddress)
    {
        return rules
            .Select(x => new { rule = x, score = x.Areas.GetMaxMatchScore(userAddress) })
            .Where(x => x.score > 0 || x.rule.IsDefault)
            .MaxBy(x => x.score)?.rule;
    }
}