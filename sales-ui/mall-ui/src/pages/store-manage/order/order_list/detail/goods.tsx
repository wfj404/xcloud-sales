import { Card } from 'antd';

import { OrderDto } from '@/utils/swagger';
import { formatMoney, isArrayEmpty } from '@/utils/utils';

import XGoodsList from '../components/goodsList';

export default (props: { model: OrderDto }) => {
  const { model } = props;

  return (
    <>
      <Card
        title="商品"
        style={{ marginBottom: 10 }}
        extra={
          <div>
            总订单金额：<b>{formatMoney(model.TotalPrice || 0)}</b>
          </div>
        }
      >
        {isArrayEmpty(model.Items) || <XGoodsList items={model.Items || []} />}
      </Card>
    </>
  );
};
