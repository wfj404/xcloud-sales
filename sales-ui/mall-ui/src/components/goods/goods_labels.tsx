import { GoodsDto } from '@/utils/swagger';
import { Divider, Stack, Typography } from '@mui/material';
import Box from '@mui/material/Box';

export default ({ model }: { model: GoodsDto }) => {
  return (
    <Box sx={{}}>
      <Stack
        spacing={1}
        direction="row"
        alignItems={'center'}
        justifyContent="flex-start"
        divider={<Divider orientation="vertical" flexItem />}
      >
        {model.StickyTop && (
          <Typography
            color="text.disabled"
            variant="caption"
            sx={{
              display: 'inline',
            }}
          >
            置顶
          </Typography>
        )}
        {model.Tags?.map((x, i) => {
          return (
            <Typography
              key={i}
              color="text.disabled"
              variant="caption"
              sx={{
                display: 'inline',
              }}
            >
              {x.Name}
            </Typography>
          );
        })}
      </Stack>
    </Box>
  );
};
