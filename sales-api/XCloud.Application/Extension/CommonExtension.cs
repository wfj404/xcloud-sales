﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Volo.Abp;
using Volo.Abp.Data;
using Volo.Abp.Guids;
using XCloud.Application.Utils;
using XCloud.Core.Json;

namespace XCloud.Application.Extension;

public static class CommonExtension
{
    [CanBeNull]
    public static string GetCellValue(this string[] row, IList<string> headers, string title,
        bool required = false)
    {
        var i = headers.IndexOf(title);

        if (i >= 0 && i <= row.Length - 1)
        {
            var value = row[i];

            if (value != null)
            {
                return value;
            }
        }

        if (required)
        {
            var rowStr = string.Join(',', row);
            var headerStr = string.Join(',', headers);
            throw new ArgumentException(message: $"required:{title},row-cells:{rowStr},headers:{headerStr}");
        }

        return default;
    }

    public static string CreateGuidString(this IGuidGenerator idGenerator)
    {
        var res = idGenerator.Create().ToString().Replace("-", string.Empty);
        return res;
    }

    private static string PropertyKey<T>() => typeof(T).FullName;

    public static void SetPropertyAsJson<T>(this IHasExtraProperties data, T value,
        IJsonDataSerializer jsonDataSerializer)
    {
        var json = jsonDataSerializer.SerializeToString(value);

        data.SetProperty(PropertyKey<T>(), json, validate: false);
    }

    public static T GetPropertyFromJson<T>(this IHasExtraProperties data, IJsonDataSerializer jsonDataSerializer)
    {
        var json = data.GetProperty<string>(PropertyKey<T>());

        if (string.IsNullOrWhiteSpace(json))
        {
            return default;
        }

        return jsonDataSerializer.DeserializeFromString<T>(json);
    }

    public static string GetRequiredTypeIdentityName<T>(this CommonUtils commonUtils) where T : class
    {
        var name = commonUtils.GetTypeIdentityName<T>();

        if (string.IsNullOrWhiteSpace(name))
            throw new BusinessException(message: nameof(GetRequiredTypeIdentityName));

        return name;
    }
}