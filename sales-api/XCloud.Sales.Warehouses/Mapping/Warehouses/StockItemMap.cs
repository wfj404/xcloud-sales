﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Sales.Application.Mapping;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Mapping.Warehouses;

public class StockItemMap : SalesEntityTypeConfiguration<StockItem>
{
    public override void Configure(EntityTypeBuilder<StockItem> builder)
    {
        base.Configure(builder);
        
        builder.ToTable("sales_stock_item");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigModificationTimeEntity();
        
        builder.Property(x => x.StockId).RequiredId();
        builder.Property(x => x.SkuId).RequiredId();
        builder.Property(x => x.Quantity);
        builder.Property(x => x.Price).HasPrecision(18, 2);

        builder.Property(x => x.DeductQuantity);
    }
}