using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Mapping;

namespace XCloud.Sales.Application.Mapping;

public static class MappingExtension
{
    public static void PreConfigLimitedToStore<T>(this EntityTypeBuilder<T> builder)
        where T : class, IStoreMappingSupported
    {
        builder.Property(x => x.LimitedToStore).HasDefaultValue(false);
    }
}