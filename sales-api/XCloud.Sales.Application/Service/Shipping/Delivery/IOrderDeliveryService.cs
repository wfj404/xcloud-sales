﻿using XCloud.Application.Gis;
using XCloud.Platform.Application.Service.Address;
using XCloud.Sales.Application.Service.Orders.PlaceOrder;
using XCloud.Sales.Application.Service.Shipping.Delivery.Express;
using XCloud.Sales.Application.Service.Shipping.Delivery.ShortDistance;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Shipping.Delivery;

public interface IOrderDeliveryService : ISalesAppService
{
    Task SetDeliveryAsync(PlaceOrderDataHolder holder);
}

public class OrderDeliveryService : SalesAppService, IOrderDeliveryService
{
    private readonly IStoreUserAddressService _storeUserAddressService;
    private readonly IStoreSettingService _storeSettingService;
    private readonly IFreightTemplateService _freightTemplateService;

    public OrderDeliveryService(IStoreUserAddressService storeUserAddressService,
        IStoreSettingService storeSettingService,
        IFreightTemplateService freightTemplateService)
    {
        _storeUserAddressService = storeUserAddressService;
        _storeSettingService = storeSettingService;
        _freightTemplateService = freightTemplateService;
    }

    private async Task<UserAddressDto> GetShippingAddressOrNullAsync(PlaceOrderRequestDto dto,
        StoreUserDto storeStoreUser)
    {
        if (string.IsNullOrWhiteSpace(dto.AddressId))
            return null;

        var allAddress = await _storeUserAddressService.QueryByUserIdAsync(storeStoreUser.Id);

        var address = allAddress.FirstOrDefault(x => x.Id == dto.AddressId);

        return address;
    }

    private async Task SetShortDistanceDeliveryAsync(PlaceOrderDataHolder holder)
    {
        var settings =
            await _storeSettingService.GetSettingsAsync<StoreShortDistanceDeliverySettings>(holder.Order.StoreId);

        if (!settings.Enabled)
        {
            holder.ShippingErrors.Add("short distance delivery not enabled");
        }

        if (!settings.PayAfterShippedSupported && holder.Request.PaymentMethodId == SalesConstants.PaymentMethod.Offline)
        {
            holder.ShippingErrors.Add("after shipped payment is not supported");
        }

        if (holder.ShippingErrors.Any())
        {
            return;
        }

        if (settings.MinimalOrderPrice != null && settings.MinimalOrderPrice > holder.Order.TotalPrice)
        {
            holder.ShippingErrors.Add("minimal price error");
            return;
        }

        var distance = holder.Order.Store.GetGeoLocation()
            .TryGetDistanceInMeter(holder.Order.UserAddress.GetGeoLocation(), out var error);

        if (!string.IsNullOrWhiteSpace(error))
        {
            holder.ShippingErrors.Add(error);
            return;
        }

        distance /= 1000;

        if (settings.MaxDistance != null && settings.MaxDistance < distance)
        {
            holder.ShippingErrors.Add($"distance is too far:{(int)distance}km");
            return;
        }

        if (settings.CalculatePriceType == "free")
        {
            holder.Order.ShippingFee = decimal.Zero;
        }
        else if (settings.CalculatePriceType == "tiered")
        {
            if (settings.TieredPrice == null)
            {
                holder.ShippingErrors.Add("tiered price settings error");
            }
            else
            {
                holder.Order.ShippingFee = settings.TieredPrice.CalculatePrice((int)distance);
            }
        }
        else
        {
            holder.ShippingErrors.Add("error calculate price type");
        }
    }

    private async Task SetExpressAsync(PlaceOrderDataHolder holder)
    {
        var settings = await _storeSettingService.GetSettingsAsync<StoreExpressSettings>(holder.Order.StoreId);

        if (!settings.Enabled)
        {
            holder.ShippingErrors.Add("express not enabled");
        }

        if (!settings.PayAfterShippedSupported && holder.Request.PaymentMethodId == SalesConstants.PaymentMethod.Offline)
        {
            holder.ShippingErrors.Add("after shipped payment is not supported");
        }

        if (holder.ShippingErrors.Any())
        {
            return;
        }

        var allTemplateList = await this._freightTemplateService.QueryAllAsync(holder.Order.StoreId);
        var template = allTemplateList
            .OrderByDescending(x => x.CreationTime)
            .ThenByDescending(x => x.Id)
            .FirstOrDefault();

        if (template == null)
        {
            this.Logger.LogWarning("freight template not found");
            return;
        }

        if (template.FreeShippingMinOrderTotalPrice > decimal.Zero &&
            template.FreeShippingMinOrderTotalPrice <= holder.Order.TotalPrice)
        {
            holder.Order.ShippingFee = decimal.Zero;
            return;
        }

        template.ExcludedAreas =
            this.JsonDataSerializer.DeserializeFromString<DeliveryArea[]>(template.ExcludedAreaJson);
        template.ExcludedAreas ??= [];

        if (template.ExcludedAreas.Any() && template.ExcludedAreas.GetMaxMatchScore(holder.Order.UserAddress) > 0)
        {
            holder.ShippingErrors.Add("not shipping area");
            return;
        }

        template.Rules =
            this.JsonDataSerializer.DeserializeFromString<DeliveryRule[]>(template.RulesJson);
        template.Rules ??= [];

        var rule = template.Rules.SelectDeliveryRuleOrDefault(holder.Order.UserAddress);

        if (rule == null)
        {
            holder.ShippingErrors.Add("there is no freight template rule found for this area");
            return;
        }

        if (rule.TieredPrice == null)
        {
            holder.ShippingErrors.Add("tiered price settings is not found");
            return;
        }

        var weight = holder.Order.Items.Sum(x => x.Weight);

        weight = Math.Max(weight, 1);

        holder.Order.ShippingFee = rule.TieredPrice.CalculatePrice(weight);
    }

    public async Task SetDeliveryAsync(PlaceOrderDataHolder holder)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(holder.Request.AddressId))
                return;

            if (holder.Request.ShippingMethodId != SalesConstants.ShippingMethodType.Delivery)
                return;

            var address = await GetShippingAddressOrNullAsync(holder.Request, holder.Order.StoreUser);

            if (address == null)
            {
                holder.ShippingErrors.Add("pls select a valid shipping address");
                return;
            }

            var order = holder.Order!;
            //calculate delivery fee
            order.UserAddress = address;
            order.UserAddressId = address.Id;
            order.ShippingFee = default;
            order.ShippingMethodId = holder.Request.ShippingMethodId;
            order.ShippingStatusId = SalesConstants.ShippingStatus.NotShipping;

            order.DeliveryRecord = new DeliveryRecordDto
            {
                DeliveryType = holder.Request.DeliveryType,
                PreferDeliveryStartTime = holder.Request.PreferDeliveryStartTime,
                PreferDeliveryEndTime = holder.Request.PreferDeliveryEndTime
            };

            if (holder.Request.DeliveryType == SalesConstants.DeliveryType.ShortDistanceDelivery)
            {
                await this.SetShortDistanceDeliveryAsync(holder);
            }
            else if (holder.Request.DeliveryType == SalesConstants.DeliveryType.Express)
            {
                await this.SetExpressAsync(holder);
            }
            else
            {
                holder.ShippingErrors.Add("not supported delivery type");
            }
        }
        catch (Exception e)
        {
            this.Logger.LogError(message: nameof(SetDeliveryAsync), exception: e);
            holder.ShippingErrors.Add("failed to calculate shipping fee");
        }
    }
}