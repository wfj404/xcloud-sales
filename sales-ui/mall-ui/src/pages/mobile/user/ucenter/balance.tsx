import { useRequest } from 'ahooks';
import { useEffect, useState } from 'react';
import { history } from 'umi';

import XLoading from '@/components/common/loading/skeleton';
import { apiClient } from '@/utils/client';
import { formatMoney, isStringEmpty } from '@/utils/utils';
import {
  AccountBalanceWalletRounded,
  CurrencyExchangeRounded,
  QrCode,
} from '@mui/icons-material';
import {
  Box,
  CardActionArea,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from '@mui/material';

interface IBtn {
  title: string;
  subtitle?: string;
  icon: any;
  action?: () => void;
  pathname?: string;
}

export default () => {
  const [openQr, _openQr] = useState(false);

  const queryUserRequest = useRequest(apiClient.mall.userUserBalanceAndPoints, {
    manual: true,
  });
  const data = queryUserRequest.data?.data?.Data || {};
  const loading = queryUserRequest.loading;

  useEffect(() => {
    queryUserRequest.run({});
  }, []);

  const createButton = (item: IBtn) => {
    return (
      <CardActionArea
        sx={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          py: 2,
          height: '100%',
        }}
        onClick={() => {
          item.action && item.action();
          item.pathname &&
            history.push({
              pathname: item.pathname,
            });
        }}
      >
        <Box sx={{}}>
          {item.icon}
          <Typography variant="subtitle1" component={'div'} gutterBottom>
            {item.title}
          </Typography>
          <Typography
            variant="caption"
            color={'text.disabled'}
            sx={{
              display: 'block',
              textAlign: 'center',
              visibility: isStringEmpty(item.subtitle) ? 'hidden' : 'visible',
            }}
          >
            {item.subtitle || '--'}
          </Typography>
        </Box>
      </CardActionArea>
    );
  };

  if (loading) {
    return (
      <Box sx={{ mb: 2, mt: 1 }}>
        <XLoading />
      </Box>
    );
  }

  return (
    <>
      <Dialog
        open={openQr}
        onClose={() => {
          _openQr(false);
        }}
      >
        <DialogTitle>付款二维码</DialogTitle>
        <DialogContent>todo</DialogContent>
      </Dialog>

      <Box
        sx={{
          mx: 1,
          mb: 2,
          backgroundColor: 'white',
          //border: loading ? 'none' : '1px solid rgb(0, 127, 255)',
          borderRadius: 1,
          overflow: 'hidden',
          //display: 'none',
        }}
      >
        <Grid container>
          <Grid item xs={4}>
            {createButton({
              icon: <AccountBalanceWalletRounded fontSize="large" />,
              title: '余额',
              subtitle: `${formatMoney(data.Balance || 0)}`,
              pathname: '/ucenter/balance',
            })}
          </Grid>
          <Grid item xs={4}>
            {createButton({
              icon: <QrCode fontSize="large" />,
              title: '付款',
              action: () => {
                _openQr(true);
              },
            })}
          </Grid>
          <Grid item xs={4}>
            {createButton({
              icon: <CurrencyExchangeRounded fontSize="large" />,
              title: '积分',
              subtitle: `${data.Points == undefined ? '--' : data.Points}`,
              pathname: '/ucenter/points',
            })}
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
