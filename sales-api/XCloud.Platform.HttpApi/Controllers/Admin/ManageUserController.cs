﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/manage-user")]
public class ManageUserController : PlatformBaseController
{
    private readonly IUserAccountService _userAccountService;
    private readonly IUserMobileService _userMobileService;
    private readonly IUserRealNameService _userRealNameService;
    private readonly IUserService _userService;
    private readonly IAdminService _adminService;

    public ManageUserController(
        IUserAccountService userAccountService,
        IUserMobileService userMobileService,
        IUserService userService,
        IAdminService adminService,
        IUserRealNameService userRealNameService)
    {
        _userAccountService = userAccountService;
        _userMobileService = userMobileService;
        _userService = userService;
        _adminService = adminService;
        _userRealNameService = userRealNameService;
    }

    [HttpPost("get-profile-by-id")]
    public async Task<ApiResponse<UserDto>> GetProfileByIdAsync([FromBody] IdDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var profile = await _userService.GetByIdAsync(dto.Id);

        if (profile == null)
            return new ApiResponse<UserDto>();

        await _adminService.AttachAdminIdentityAsync(new[] { profile });

        await _userMobileService.AttachAccountMobileAsync(new[] { profile });

        await _userRealNameService.AttachRealNameAsync(new[] { profile });

        return new ApiResponse<UserDto>(profile);
    }

    [HttpPost("search-user-account")]
    public async Task<ApiResponse<UserDto>> SearchUserAccountAsync([FromBody] SearchUserAccountDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var account = await _userAccountService.GetByAccountIdentityAsync(dto.AccountIdentity);

        return new ApiResponse<UserDto>(account);
    }

    [HttpPost("user-by-ids")]
    public async Task<ApiResponse<UserDto[]>> QueryUserByIdsAsync([FromBody] string[] ids)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var data = await _userService.GetByIdsAsync(ids);

        await _userMobileService.AttachAccountMobileAsync(data);

        return new ApiResponse<UserDto[]>(data);
    }

    /// <summary>
    /// 查询user
    /// </summary>
    [HttpPost("paging")]
    public async Task<PagedResponse<UserDto>> QueryUserPagination([FromBody] QueryUserPaging dto)
    {
        dto.PageSize = 20;

        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var data = await _userService.QueryPagingAsync(dto);

        var users = data.Items.ToArray();

        await _userMobileService.AttachAccountMobileAsync(users);

        return data;
    }

    [HttpPost("set-password")]
    public async Task<ApiResponse<object>> SetPasswordAsync([FromBody] SetUserPasswordDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var user = await this._userService.EnsureUserAsync(dto.Id);

        await _userAccountService.SetPasswordAsync(dto.Id, dto.Password);

        await this.PlatformEventBusService.NotifyUserUpdatedAsync(user);

        return new ApiResponse<object>();
    }

    [HttpPost("set-mobile")]
    public async Task<ApiResponse<UserIdentity>> SetMobileAsync([FromBody] SetUserMobileDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var user = await this._userService.EnsureUserAsync(dto.Id);

        var res = await _userMobileService.SetUserMobilePhoneAsync(dto.Id, dto.Mobile);

        if (res.HasNoError())
        {
            await _userMobileService.ConfirmMobileAsync(res.Data.Id);
        }

        await this.PlatformEventBusService.NotifyUserUpdatedAsync(user);

        return res;
    }

    [HttpPost("remove-mobiles")]
    public async Task<ApiResponse<object>> RemoveMobilesAsync([FromBody] IdDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var user = await this._userService.EnsureUserAsync(dto.Id);

        await _userMobileService.RemoveUserMobilesAsync(dto.Id);

        await this.PlatformEventBusService.NotifyUserUpdatedAsync(user);

        return new ApiResponse<object>();
    }

    [HttpPost("create")]
    public async Task<ApiResponse<User>> CreateUserAsync(
        [FromBody] IdentityNameDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var res = await _userAccountService.CreateUserAccountV1Async(dto);

        await this.PlatformEventBusService.NotifyUserUpdatedAsync(res);

        return new ApiResponse<User>(res);
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateUserStatusDto dto)
    {
        var admin = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await _userAccountService.UpdateStatusAsync(dto);

        await this.PlatformEventBusService.NotifyUserUpdatedAsync(new User() { Id = dto.Id });

        return new ApiResponse<object>();
    }
}