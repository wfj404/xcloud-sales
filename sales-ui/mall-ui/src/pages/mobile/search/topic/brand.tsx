

import { QueryGoodsPaging, SearchView } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';
import { Box, Button, Typography } from '@mui/material';

export default function ({
  model,
  query,
  onChange,
}: {
  model: SearchView;
  query: QueryGoodsPaging;
  onChange: (e: QueryGoodsPaging) => void;
}) {
  const data = model.Brands || [];

  if (isArrayEmpty(data)) {
    return null;
  }

  return (
    <>
      <Box sx={{ px: 2, py: 1 }}>
        <Typography variant="overline" color="text.disabled" gutterBottom>
          热门品牌
        </Typography>
        <Box sx={{ mt: 1 }}>
          {data.map((x, index) => {
            return (
              <Button
                key={index}
                size="small"
                variant="text"
                sx={{
                  color: (theme) => theme.palette.text.primary,
                }}
                onClick={() => {
                  onChange({
                    ...query,
                    BrandId: x.Id,
                  });
                }}
              >
                {`${x.Name}`}
              </Button>
            );
          })}
        </Box>
      </Box>
    </>
  );
}
