﻿using System;
using Medallion.Threading.Redis;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Core.Configuration;

namespace XCloud.Application.Redis;

public static class RedisBuilderExtension
{
    [MethodMetric]
    internal static void AddRedisAll(this ServiceConfigurationContext context)
    {
        context.AddRedisClient();
        context.AddRedisDistributedCacheProvider();
        context.AddRedisDataProtectionKeyStore();
        context.AddRedisDistributedLockingProvider();
    }

    private static void AddRedisDataProtectionKeyStore(this ServiceConfigurationContext context)
    {
        var redisClient = context.Services.GetSingletonInstance<RedisClient>();

        var builder = context.Services.GetRequiredXCloudBuilder();
        var appName = context.Services.GetAppName();

        var dataProtectionBuilder = context.Services
            .AddDataProtection()
            .SetApplicationName(applicationName: appName)
            .AddKeyManagementOptions(option =>
            {
                option.AutoGenerateKeys = true;
                option.NewKeyLifetime = TimeSpan.FromDays(1000);
            });

        //加密私钥
        var dataProtectionDatabase = RedisDatabasePurpose.DataProtection;
        dataProtectionBuilder.PersistKeysToStackExchangeRedis(
            databaseFactory: () => redisClient.Connection.SelectDatabase(dataProtectionDatabase),
            key: $"data_protection_key:{appName}");

        builder.SetObject(dataProtectionBuilder);
    }

    private static void AddRedisClient(this ServiceConfigurationContext context)
    {
        var config = context.Services.GetConfiguration();

        var connectionString = config.GetRedisConnectionString();

        var connection = new RedisClient(connectionString);

        context.Services.AddSingleton(connection);
    }

    private static void AddRedisDistributedLockingProvider(this ServiceConfigurationContext context)
    {
        context.Services.AddSingleton<Medallion.Threading.IDistributedLockProvider>(provider =>
        {
            var connection = provider.GetRequiredService<RedisClient>().Connection;

            var db = connection.GetDatabase(RedisDatabasePurpose.DistributedLock);

            return new RedisDistributedSynchronizationProvider(db);
        });
    }

    private static void AddRedisDistributedCacheProvider(this ServiceConfigurationContext context)
    {
        var builder = context.Services.GetRequiredXCloudBuilder();

        var appName = context.Services.GetAppName();

        var redisWrapper = context.Services.GetSingletonInstance<RedisClient>();

        var redisStr = redisWrapper.ConnectionString;

        //remove default memory implementation
        context.Services.RemoveAll<IDistributedCache>();

        context.Services.AddStackExchangeRedisCache(option =>
        {
            option.InstanceName = $"rds-{appName}";

            option.ConfigurationOptions ??= new StackExchange.Redis.ConfigurationOptions();

            option.ConfigurationOptions.EndPoints.Add(redisStr);

            option.ConfigurationOptions.DefaultDatabase = RedisDatabasePurpose.Caching;
        });

        context.Services.Configure<RedisCacheOptions>(options =>
        {
            //
        });
    }
}