using Volo.Abp.Application.Dtos;

namespace XCloud.Platform.Application.Configuration;

public class PlatformOption : IEntityDto
{
    public bool AutoCreateDatabase { get; set; } = false;
}