import { apiClient } from '@/utils/client';
import { Grade, GradeDto } from '@/utils/swagger';
import { PlusOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input, InputNumber, message, Modal, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import { handleResponse, serializeThenDeserialize } from '@/utils/utils';
import { useSnapshot } from 'umi';
import { storeState, StoreType } from '@/store';

export default () => {
  const [loading, _loading] = useState(false);
  const [loadingSave, _loadingSave] = useState(false);
  const [loadingId, _loadingId] = useState('');

  const [show, _show] = useState(false);
  const [formData, _formData] = useState<Grade>({});

  const app = useSnapshot(storeState) as StoreType;

  const queryList = () => {
    _loading(true);
    app.queryGrades().finally(() => _loading(false));
  };

  const save = (formData: Grade) => {
    _loadingSave(true);

    apiClient.mallAdmin
      .gradeSave({ ...formData })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          _show(false);
          queryList();
        });
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const deletePrice = (row: Grade) => {
    if (!confirm('确定删除？')) {
      return;
    }
    _loadingId(row.Id || '');
    apiClient.mallAdmin
      .gradeUpdateStatus({
        Id: row.Id,
        IsDeleted: true,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('删除成功');
          queryList();
        });
      })
      .finally(() => {
        _loadingId('');
      });
  };

  const columns: ColumnProps<GradeDto>[] = [
    {
      title: '会员名',
      render: (d, x) => x.Name,
    },
    {
      title: '会员描述',
      render: (d, x) => x.Description,
    },
    {
      title: '排序',
      render: (d, x) => x.Sort,
    },
    {
      title: '会员人数',
      render: (d, x) => x.UserCount,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type={'link'}
              onClick={() => {
                _formData(x);
                _show(true);
              }}
            >
              编辑
            </Button>
            <Button
              type={'link'}
              loading={loadingId === x.Id}
              onClick={() => {
                deletePrice(x);
              }}
              danger
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, []);

  return (
    <>
      <Modal
        title='编辑价格'
        open={show}
        onCancel={() => {
          _show(false);
        }}
        confirmLoading={loadingSave}
        onOk={() => {
          save(formData);
        }}
      >
        <Form>
          <Form.Item label='名称'>
            <Input value={formData.Name || ''} onChange={e => {
              _formData(d => ({ ...d, Name: e.target.value }));
            }} />
          </Form.Item>
          <Form.Item label='描述'>
            <Input value={formData.Description || ''} onChange={e => {
              _formData(d => ({ ...d, Description: e.target.value }));
            }} />
          </Form.Item>
          <Form.Item label='排序'>
            <InputNumber value={formData.Sort || 0} onChange={e => {
              _formData(d => ({ ...d, Sort: e || 0 }));
            }} />
          </Form.Item>
        </Form>
      </Modal>
      <Card
        title='会员等级'
        loading={loading}
        style={{
          marginBottom: 10,
        }}
        extra={
          <Button
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              _show(true);
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          dataSource={serializeThenDeserialize(app.grades || [])}
          columns={columns}
          pagination={false}
        />
      </Card>
    </>
  );
};
