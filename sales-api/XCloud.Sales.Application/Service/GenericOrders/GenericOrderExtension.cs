﻿using JetBrains.Annotations;
using XCloud.Application.Utils;
using XCloud.Sales.Application.Domain.GenericOrders;
using XCloud.Sales.Application.Domain.Orders;

namespace XCloud.Sales.Application.Service.GenericOrders;

public static class GenericOrderExtension
{
    [NotNull]
    [ItemNotNull]
    public static async Task<GenericOrder> GetOrCreateAsync<T>(this IGenericOrderService genericOrderService,
        CommonUtils commonUtils, int userId, string orderId)
        where T : class, ICustomerOrder
    {
        var typeName = commonUtils.GetRequiredTypeIdentityName<T>();

        return await genericOrderService.GetOrCreateAsync(userId, typeName, orderId);
    }
}