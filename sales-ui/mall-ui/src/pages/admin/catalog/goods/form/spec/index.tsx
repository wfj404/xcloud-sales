import { Button, Card, Form, Input, message, Modal, Spin, Table } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { GoodsDto, SpecDto } from '@/utils/swagger';

import ValueList from './value';

export default (props: { model: GoodsDto; ok?: () => void }) => {
  const { model } = props;

  const [loading, _loading] = useState(true);
  const [data, _data] = useState<SpecDto[]>([]);

  const [formData, _formData] = useState<SpecDto | undefined>(undefined);
  const f = formData || {};
  const [loadingSave, _loadingSave] = useState(false);

  const queryList = () => {
    if (!model.Id) {
      return;
    }
    _loading(true);
    apiClient.mallAdmin
      .specListJson({ Id: model.Id })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data.Data || []);
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const deleteSpec = (record: SpecDto) => {
    if (!confirm('确定删除规格')) {
      return;
    }
    _loading(true);
    apiClient.mallAdmin
      .specDelete({ Id: record.Id })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          queryList();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryList();
  }, [model]);

  const save = (row: SpecDto) => {
    _loadingSave(true);

    row.GoodsId = model.Id;

    apiClient.mallAdmin
      .specEdit({
        ...row,
        GoodsId: model.Id,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          _formData(undefined);
          queryList();
        }
      })
      .finally(() => {
        _loadingSave(false);
      });
  };

  const columns: ColumnType<SpecDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name || '--',
    },
    {
      title: '明细',
      render: (d, record: SpecDto) => {
        return (
          <ValueList
            model={record}
            ok={() => {
              queryList();
            }}
          />
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
            <Button
              type="link"
              danger
              onClick={() => {
                deleteSpec(record);
              }}
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  return (
    <>
      <Modal
        title={'规格'}
        open={formData != undefined}
        onCancel={() => _formData(undefined)}
        onOk={() => save(f)}
      >
        <Spin spinning={loadingSave}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item
              label="名称"
              rules={[
                {
                  required: true,
                },
                {
                  max: 20,
                },
              ]}
            >
              <Input
                value={f.Name || ''}
                onChange={(e) => {
                  _formData({
                    ...formData,
                    Name: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
      <Card
        title="商品规格/属性"
        style={{
          marginBottom: 10,
        }}
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </Card>
    </>
  );
};
