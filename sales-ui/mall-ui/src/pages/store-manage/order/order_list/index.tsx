import { useRequest } from 'ahooks';
import { Button, Card, Modal, Popover, Space, Table, Tag } from 'antd';
import { ColumnType } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XUserAvatar from '@/components/manage/user/avatar';
import XDeliveryStatusTag from '@/components/status/delivery';
import XOrderStatusTag from '@/components/status/order';
import XPaymentStatusTag from '@/components/status/payment';
import { getPaymentMethod, getShippingMethod, ShippingMethod, ShippingStatus } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto, QueryOrderPagingInput } from '@/utils/swagger';
import { formatMoney, isArrayEmpty, isStringEmpty } from '@/utils/utils';
import { MessageOutlined } from '@ant-design/icons';

import XGoodsList from './components/goodsList';
import XDetail from './detail';
import XSearchForm from './search_form';

export default () => {
  const [query, _query] = useState<QueryOrderPagingInput>({
    ToService: true,
    ShippingMethodId: [ShippingMethod.Delivery, ShippingMethod.Pickup],
    DeliveryStatus: [ShippingStatus.NotShipping, ShippingStatus.Shipping],
    Page: 1,
  });

  const [detailId, _detailId] = useState<string>('');

  const query_list_request = useRequest(
    apiClient.mallManager.orderQueryOrderPaging,
    {
      manual: true,
    },
  );
  const data = query_list_request.data?.data || {};

  const queryList = () => {
    query_list_request.run({ ...query });
  };

  useEffect(() => {
    queryList();
  }, [query]);

  const renderOrderRemark = (x: OrderDto) => {
    if (isStringEmpty(x.Remark)) {
      return null;
    }

    return (
      <Popover title="备注" content={x.Remark} trigger={['hover']}>
        <MessageOutlined />
      </Popover>
    );
  };

  const columns: ColumnType<OrderDto>[] = [
    {
      title: '订单号',
      render: (d, x) => {
        return (
          <div>
            <div>
              <b>{x.OrderSn || '--'}</b>
            </div>
            <Space direction={'horizontal'}>
              <Tag color={'blue'}>{getShippingMethod(x)}</Tag>
              <Tag color={'red'}>{getPaymentMethod(x)}</Tag>
              {renderOrderRemark(x)}
            </Space>
          </div>
        );
      },
    },
    {
      title: '总金额',
      render: (d, x) => (
        <Tag color={'warning'}>
          <b>{formatMoney(x.TotalPrice || 0)}</b>
        </Tag>
      ),
    },
    {
      title: '支付状态',
      render: (d, x) => {
        return (
          <>
            <XPaymentStatusTag model={x} />
            {(x.PaidTotalPrice || 0) > 0 && (
              <p style={{ color: 'gray' }}>
                已支付{formatMoney(x.PaidTotalPrice || 0)}
              </p>
            )}
            {(x.RefundedAmount || 0) > 0 && (
              <p style={{ color: 'red' }}>
                已退款{formatMoney(x.RefundedAmount || 0)}
              </p>
            )}
          </>
        );
      },
    },
    {
      title: '交付状态',
      render: (d, x) => {
        return (
          <>
            <XDeliveryStatusTag model={x} />
          </>
        );
      },
    },
    {
      title: '订单状态',
      render: (d, x) => {
        return (
          <>
            <XOrderStatusTag model={x} />
          </>
        );
      },
    },
    {
      title: '买家信息',
      render: (d, x) => {
        return (
          <>
            <XUserAvatar model={x.StoreUser?.User} />
          </>
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      fixed: 'right',
      width: 100,
      render: (d, x) => (
        <Button.Group>
          <Button
            type="link"
            onClick={() => {
              _detailId(x.Id || '');
            }}
          >
            查看
          </Button>
        </Button.Group>
      ),
    },
  ];

  return (
    <>
      <Modal
        title={'详情'}
        open={!isStringEmpty(detailId)}
        onCancel={() => {
          _detailId('');
        }}
        footer={false}
        forceRender
        width="100%"
      >
        <XDetail detailId={detailId} />
      </Modal>
      <XSearchForm
        query={query}
        onSearch={(q: QueryOrderPagingInput) => {
          _query(q);
        }}
      />
      <Card bordered={false} loading={query_list_request.loading}>
        <Table
          rowKey={(x) => x.Id || ''}
          loading={query_list_request.loading}
          columns={columns}
          dataSource={data.Items || []}
          expandable={{
            expandedRowRender: (x) => {
              return (
                isArrayEmpty(x.Items) || (
                  <div style={{ padding: 10, border: '5px dashed orange' }}>
                    <XGoodsList items={x.Items || []} />
                  </div>
                )
              );
            },
          }}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
