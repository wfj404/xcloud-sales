﻿using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.Extension;
using XCloud.Sales.Application;
using XCloud.Sales.Qingdao.Authentication;
using XCloud.Sales.Qingdao.Database;
using XCloud.Sales.Qingdao.Job;
using XCloud.Sales.Qingdao.Localization;
using XCloud.Sales.Qingdao.Mapper;
using XCloud.Sales.Qingdao.Service.Stores;
using XCloud.Sales.Warehouses;

namespace XCloud.Sales.Qingdao;

[DependsOn(
    typeof(SalesApplicationModule),
    typeof(SalesWarehouseApplicationModule)
)]
[MethodMetric]
public class QingdaoSalesApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.ConfigureQingdaoSqlServer();
        context.ConfigQingdaoSalesLocalization();
        context.AddAutoMapperProfile<QingdaoAutoMapperConfiguration>();
        context.AddMvcPart<QingdaoSalesApplicationModule>();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //auth
        context.ReplaceAuthService();

        //store selector
        context.ReplaceStoreSelector();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        Task.Run(() => app.ApplicationServices.StartQingdaoJobsAsync()).Wait();
    }
}