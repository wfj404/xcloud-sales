﻿using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Sales.Application.Domain.Logging;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Service.Logging;

public class QueryActivityLogPagingInput : PagedRequest
{
    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public int? UserId { get; set; }

    public int? ActivityLogTypeId { get; set; }
}

public class ActivityLogDto : ActivityLog, IEntityDto
{
    public AdminDto Admin { get; set; }

    public StoreUserDto StoreUser { get; set; }
}