﻿using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Common;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Common;

public interface IKeyStoreService : IXCloudAppService
{
    Task<bool> TryCreateAsync(string key);

    Task DeleteAsync(string key);
}

public class KeyStoreService : XCloudAppService, IKeyStoreService
{
    private readonly IPlatformRepository<KeyStore> _repository;

    public KeyStoreService(IPlatformRepository<KeyStore> repository)
    {
        _repository = repository;
    }

    private string NormalizeKey(string key)
    {
        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        return key.Trim().RemoveWhitespace();
    }

    public async Task<bool> TryCreateAsync(string key)
    {
        key = this.NormalizeKey(key);

        if (await this._repository.AnyAsync(x => x.Key == key))
        {
            return false;
        }

        var entity = new KeyStore()
        {
            Id = this.GuidGenerator.CreateGuidString(),
            Key = key,
            CreationTime = this.Clock.Now
        };

        await this._repository.InsertAsync(entity);

        return true;
    }

    public async Task DeleteAsync(string key)
    {
        key = this.NormalizeKey(key);

        await this._repository.DeleteAsync(x => x.Key == key);
    }
}