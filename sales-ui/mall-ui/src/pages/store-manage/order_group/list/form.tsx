import { Button, Form, Input, message, Modal, Space, Spin } from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { apiClient } from '@/utils/client';
import { OrderGroupDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: OrderGroupDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [model, _model] = useState<OrderGroupDto>({});

  const save = (row: OrderGroupDto) => {
    row.Name = row.Name?.trim();
    if (isStringEmpty(row.Name)) {
      return;
    }

    _loading(true);

    apiClient.mallManager
      .orderGroupSave({ ...row })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...data,
    });
  }, [data]);

  useEffect(() => {
    if (!show) {
      _model({});
    }
  }, [show]);

  return (
    <>
      <Modal
        title={'批次'}
        open={show}
        onCancel={() => hide()}
        destroyOnClose
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                const res = await apiClient.mallManager.orderGroupDelete({
                  Id: model.Id,
                });
                handleResponse(res, () => {
                  hide();
                  ok();
                });
              }}
              hide={isStringEmpty(model.Id)}
            />
            <Button
              type={'primary'}
              loading={loading}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="名称" required>
              <Input
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
              {isStringEmpty(model.Name) && (
                <p style={{ color: 'red' }}>必填</p>
              )}
            </Form.Item>
            <Form.Item label="描述" rules={[{ max: 50 }]}>
              <Input.TextArea
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
