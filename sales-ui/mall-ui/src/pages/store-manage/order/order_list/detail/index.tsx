import { Col, Row, Spin } from 'antd';
import { useEffect, useState } from 'react';

import { OrderStatus, ShippingMethod } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { OrderDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

import XAction from './action';
import XAftersale from './aftersales';
import XBill from './bill';
import XDelivery from './delivery';
import XGoods from './goods';
import XNotes from './notes';
import XPickup from './pickup';
import XReview from './review';
import XSummary from './summary';

export default (props: { detailId: string }) => {
  const { detailId } = props;
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<OrderDto>({});

  const queryOrder = () => {
    if (isStringEmpty(detailId)) {
      _data({});
      return;
    }
    _loading(true);
    apiClient.mallManager
      .orderDetail({ Id: detailId })
      .then((res) => {
        handleResponse(res, () => {
          _data(res.data.Data || {});
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    queryOrder();
  }, [detailId]);

  const renderShipping = () => {
    if (data.ShippingMethodId == ShippingMethod.Pickup) {
      return <XPickup model={data} ok={() => queryOrder()} />;
    } else if (data.ShippingMethodId == ShippingMethod.Delivery) {
      return <XDelivery model={data} ok={() => queryOrder()} />;
    } else {
      return <h1>交付方式错误</h1>;
    }
  };

  return (
    <>
      <Spin spinning={loading}>
        <Row gutter={10}>
          <Col span={16}>
            {data.OrderStatusId == OrderStatus.AfterSales && (
              <XAftersale order={data} />
            )}
            <XSummary model={data} />
            <XAction
              model={data}
              ok={() => {
                queryOrder();
              }}
            />
            <XGoods model={data} />
            <XBill
              model={data}
              ok={() => {
                queryOrder();
              }}
            />
            {renderShipping()}
          </Col>
          <Col span={8}>
            <XNotes model={data} />
            <XReview model={data} />
          </Col>
        </Row>
      </Spin>
    </>
  );
};
