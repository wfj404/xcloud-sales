﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Sales.Application.Mapping;
using XCloud.Sales.Qingdao.Domain.Invoices;

namespace XCloud.Sales.Qingdao.Mapping.Invoices;

public class McsInvoiceRequestItemMap : SalesEntityTypeConfiguration<McsInvoiceRequestItem>
{
    public override void Configure(EntityTypeBuilder<McsInvoiceRequestItem> builder)
    {
        builder.ToTable("mcs_invoice_request_item");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.InvoiceRequestId).RequiredId();
        builder.Property(x => x.McsBizOrderId).RequiredId();
        builder.Property(x => x.Price).PricePrecision();
        builder.Property(x => x.Description).HasMaxLength(500);

        base.Configure(builder);
    }
}