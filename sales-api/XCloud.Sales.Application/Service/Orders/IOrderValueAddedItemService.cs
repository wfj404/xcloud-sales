using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Orders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service.Catalog;

namespace XCloud.Sales.Application.Service.Orders;

public interface IOrderValueAddedItemService : ISalesAppService
{
    Task<OrderItemDto[]> AttachOrderValueAddedItemsAsync(OrderItemDto[] data);

    Task<OrderValueAddedItemDto[]> AttachValueAddedItemsAsync(OrderValueAddedItemDto[] data);
}

public class OrderValueAddedItemService : SalesAppService, IOrderValueAddedItemService
{
    private readonly ISalesRepository<OrderValueAddedItem> _repository;

    public OrderValueAddedItemService(ISalesRepository<OrderValueAddedItem> repository)
    {
        _repository = repository;
    }

    public async Task<OrderItemDto[]> AttachOrderValueAddedItemsAsync(OrderItemDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var list = await db.Set<OrderValueAddedItem>().AsNoTracking().Where(x => ids.Contains(x.OrderItemId))
            .ToArrayAsync();

        foreach (var m in data)
        {
            var items = list.Where(x => x.OrderItemId == m.Id).ToArray();

            m.OrderValueAddedItems = items.MapArrayTo<OrderValueAddedItem, OrderValueAddedItemDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<OrderValueAddedItemDto[]> AttachValueAddedItemsAsync(OrderValueAddedItemDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Select(x => x.ValueAddedItemId).Distinct().ToArray();

        var db = await _repository.GetDbContextAsync();

        var list = await db.Set<ValueAddedItem>().AsNoTracking().WhereIdIn(ids)
            .ToArrayAsync();

        foreach (var m in data)
        {
            var item = list.FirstOrDefault(x => x.Id == m.ValueAddedItemId);

            if (item == null)
                continue;

            m.ValueAddedItem = item.MapTo<ValueAddedItem, ValueAddedItemDto>(ObjectMapper);
        }

        return data;
    }
}