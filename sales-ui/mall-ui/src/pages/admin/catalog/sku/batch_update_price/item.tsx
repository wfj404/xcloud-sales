import { Button, InputNumber, message, Space, Tag } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { SkuDto } from '@/utils/swagger';
import { formatMoney, handleResponse } from '@/utils/utils';

export default ({ item, ok }: { item: SkuDto; ok?: (d: SkuDto) => void }) => {
  const [data, _data] = useState<SkuDto | undefined>(undefined);
  const [loading, _loading] = useState(false);

  const dataToSubmit: SkuDto = data || {};

  useEffect(() => {
    _data((x) => ({
      ...x,
      ...item,
    }));
  }, [item]);

  const saveChange = () => {
    _loading(true);
    apiClient.mallAdmin
      .skuUpdateSkuPrice(dataToSubmit)
      .then((res) => {
        handleResponse(res, () => {
          message.success('修改成功');
          ok && ok(res.data.Data || dataToSubmit);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const renderDiff = () => {
    if (data == undefined) {
      return null;
    }
    if (item.Price == dataToSubmit.Price) {
      return null;
    }
    return (
      <>
        <div
          style={{
            padding: 10,
            backgroundColor: 'rgb(250,250,250)',
          }}
        >
          <p>
            <span>价格从：</span>
            <Tag color={'red'}>{formatMoney(item.Price || 0)}</Tag>
            <span>变为：</span>
            <Tag color={'green'}>{formatMoney(dataToSubmit.Price || 0)}</Tag>
          </p>
          <p>
            <Space direction={'horizontal'}>
              <Button
                loading={loading}
                type={'primary'}
                onClick={() => {
                  saveChange();
                }}
              >
                产生变更，立即保存！
              </Button>
              <Button
                type={'link'}
                danger
                onClick={() => {
                  _data({ ...item });
                }}
              >
                放弃变更
              </Button>
            </Space>
          </p>
        </div>
      </>
    );
  };

  if (item == null) {
    return <p>error</p>;
  }

  return (
    <>
      <Space direction={'horizontal'} size={'large'}>
        <div title={dataToSubmit.Id || undefined}>
          <b>{dataToSubmit.Goods?.Name || '--'}</b>
          <div>{dataToSubmit.Name || '--'}</div>
        </div>
        <InputNumber
          min={0}
          max={999999}
          value={dataToSubmit.Price || 0}
          onChange={(e) => {
            _data((x) => ({ ...x, Price: e || undefined }));
          }}
        />
      </Space>
      {renderDiff()}
    </>
  );
};
