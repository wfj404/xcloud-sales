import { Modal, Space } from 'antd-mobile';
import { useState } from 'react';

import XUploader from '@/components/upload/single_picture_form';
import { apiClient } from '@/utils/client';
import { resolveAvatar } from '@/utils/storage';
import { UserDto } from '@/utils/swagger';
import { handleResponse, successMsg } from '@/utils/utils';
import { Avatar } from '@mui/material';

import XItem from './item';

export default function (props: { model: UserDto; ok: () => void }) {
  const { model, ok } = props;

  const [loading, _loading] = useState(false);
  const [show, _show] = useState(false);

  const avatarUrl = resolveAvatar(model.Avatar, {
    width: 80,
    height: 80,
  });

  const renderUploader = () => {
    return (
      <>
        <Space direction={'vertical'}>
          <XUploader
            width={200}
            defaultImage={avatarUrl}
            data={model.Avatar || '{}'}
            ok={(e) => {
              if (!confirm('更换头像？')) {
                return;
              }
              _loading(true);
              apiClient.platform
                .userUpdateProfile({ ...model, Avatar: JSON.stringify(e) })
                .then((res) => {
                  handleResponse(res, () => {
                    _show(false);
                    successMsg('修改成功');
                    ok && ok();
                  });
                })
                .finally(() => {
                  _loading(false);
                });
            }}
          />
          {loading && <p>loading...</p>}
        </Space>
      </>
    );
  };

  return (
    <>
      <Modal
        destroyOnClose
        forceRender
        showCloseButton
        closeOnMaskClick
        visible={show}
        content={renderUploader()}
        onClose={() => {
          _show(false);
        }}
      ></Modal>
      <XItem
        title={''}
        right={
          <Avatar
            variant="square"
            sx={{
              width: 80,
              height: 80,
              borderRadius: 1,
            }}
            src={avatarUrl}
            onClick={() => {
              _show(true);
            }}
          >
            {model.NickName || model.IdentityName || '--'}
          </Avatar>
        }
      />
    </>
  );
}
