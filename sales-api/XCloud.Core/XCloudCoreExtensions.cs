﻿using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using XCloud.Core.Exceptions;

namespace XCloud.Core;

public static class XCloudCoreExtensions
{
    public static bool TryGetNonEmptyString(this IConfiguration config, string key, out string val)
    {
        if (config == null)
            throw new ArgumentNullException(nameof(config));

        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        val = config[key];

        return !string.IsNullOrWhiteSpace(val);
    }

    [NotNull]
    public static string GetRequiredConnectionString(this IConfiguration configuration, string name)
    {
        if (string.IsNullOrWhiteSpace(name))
            throw new ArgumentNullException(nameof(name));

        var connectionString = configuration.GetConnectionString(name);

        if (string.IsNullOrWhiteSpace(connectionString))
            throw new ConfigException($"{nameof(GetRequiredConnectionString)}:{name}");

        return connectionString;
    }
}