import { sumBy } from 'lodash-es';

import {
  OrderGroupOrderDto,
  OrderItemDto,
  UserAddressDto,
} from '@/utils/swagger';
import { isMobile, isStringEmpty } from '@/utils/utils';

export const calculate_total_price = (items: OrderItemDto[]) =>
  sumBy(items, (x) => (x.Quantity || 0) * (x.UnitPrice || 0));

export const has_error_in_address = (x?: UserAddressDto | null) => {
  if (!x) {
    return true;
  }

  if (isStringEmpty(x.Name)) {
    return true;
  }
  if (isStringEmpty(x.Tel)) {
    return true;
  }
  if (isStringEmpty(x.AddressDetail)) {
    return true;
  }
  if (!isMobile(x.Tel || '')) {
    return true;
  }

  return false;
};

export const has_error_in_order = (x: OrderGroupOrderDto) => {
  if (has_error_in_address(x.UserAddress)) {
    return true;
  }

  if (x.SpecificSender && has_error_in_address(x.Sender)) {
    return true;
  }

  if (!isMobile(x.UserAddress?.Tel || '')) {
    return true;
  }

  if (!x.PaymentBills || x.PaymentBills.length <= 0) {
    return true;
  }

  if (x.PaymentBills.some((x) => (x.Price || 0) <= 0)) {
    return true;
  }

  if (!x.Items || x.Items.length <= 0) {
    return true;
  }

  if (
    x.Items.some(
      (x) =>
        (x.Quantity || 0) <= 0 ||
        (x.UnitPrice || 0) <= 0 ||
        (isStringEmpty(x.Remark) && isStringEmpty(x.Sku?.Name)),
    )
  ) {
    return true;
  }

  return false;
};
