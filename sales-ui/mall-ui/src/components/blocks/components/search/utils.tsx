import { BlockItem, BlockItemProps } from '../../utils';

export interface SearchBlockProps extends BlockItemProps {
  placeholder?: string;
}

export const search_demo: BlockItem<SearchBlockProps> = {
  type: 'search',
  style: {
    marginBottom: 10,
  },
  body: {
    placeholder: '护肤',
  },
};
