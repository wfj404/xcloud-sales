﻿using System;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Sales.Qingdao.Job.Jobs;

namespace XCloud.Sales.Qingdao.Job;

public static class JobConfigurationExtension
{
    public static async Task StartQingdaoJobsAsync(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var recurringJobManager = s.ServiceProvider.GetRequiredService<IRecurringJobManager>();

        recurringJobManager.AddOrUpdate<ServiceOrderJobs>("qingdao-update-order-status",
            x => x.RefreshOrderStatusAsync(),
            Cron.MinuteInterval(5));

        await Task.CompletedTask;
    }
}