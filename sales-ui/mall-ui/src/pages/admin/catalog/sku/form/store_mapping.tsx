import { Alert, Card, InputNumber, Space, Switch, Table, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect } from 'react';
import { useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { SkuDto, StoreGoodsMappingDto } from '@/utils/swagger';

export default ({
  data,
  _data,
}: {
  data: SkuDto;
  _data: (d: SkuDto) => void;
}) => {
  const app = useSnapshot(storeState) as StoreType;

  const allStores = app.stores || [];

  const originMappings = data.StoreGoodsMapping || [];

  const mapping = allStores.map<StoreGoodsMappingDto>(
    (x): StoreGoodsMappingDto => {
      const exist = originMappings.find((d) => d.StoreId == x.Id);
      if (exist) {
        return { ...exist };
      }
      return {
        StoreId: x.Id,
        SkuId: data.Id,
        StockQuantity: 10,
        Price: 0,
        OverridePrice: false,
        Active: false,
      };
    },
  );

  const setStoreMapping = (x: StoreGoodsMappingDto[]) => {
    _data({
      ...data,
      StoreGoodsMapping: x,
    });
  };

  const columns: ColumnProps<StoreGoodsMappingDto>[] = [
    {
      title: '门店',
      render: (x: StoreGoodsMappingDto) => {
        return (
          <Tag color={'processing'}>
            {allStores.find((d) => d.Id == x.StoreId)?.Name || '--'}
          </Tag>
        );
      },
    },
    {
      title: '库存',
      render: (v, x: StoreGoodsMappingDto, i: number) => {
        return (
          <InputNumber
            value={x.StockQuantity || 0}
            onChange={(e) => {
              let data = [...mapping];
              data[i].StockQuantity = e || 0;
              setStoreMapping(data);
            }}
          />
        );
      },
    },
    {
      title: '门店定价',
      render: (v, x: StoreGoodsMappingDto, i: number) => {
        return (
          <Space direction={'vertical'} style={{ width: '100%' }}>
            <Switch
              checked={x.OverridePrice}
              onChange={(e) => {
                let data = [...mapping];
                data[i].OverridePrice = e;
                setStoreMapping(data);
              }}
            />
            <InputNumber
              disabled={!x.OverridePrice}
              value={x.Price || 0}
              onChange={(e) => {
                let data = [...mapping];
                data[i].Price = e || 0;
                setStoreMapping(data);
              }}
            />
          </Space>
        );
      },
    },
    {
      title: '门店在售',
      render: (v, x: StoreGoodsMappingDto, i: number) => {
        return (
          <Switch
            checked={x.Active}
            onChange={(e) => {
              let data = [...mapping];
              data[i].Active = e;
              setStoreMapping(data);
            }}
          />
        );
      },
    },
  ];

  useEffect(() => {
    app.queryStores();
  }, []);

  return (
    <>
      <Card title="门店在售，门店库存">
        {allStores.length <= 0 && (
          <Alert
            type={'warning'}
            message={'没有配置门店，不可以对外销售'}
          ></Alert>
        )}

        <Table
          columns={columns}
          dataSource={mapping}
          pagination={false}
          bordered
        />
      </Card>
    </>
  );
};
