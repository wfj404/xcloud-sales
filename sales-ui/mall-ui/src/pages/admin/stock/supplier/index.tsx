import { apiClient } from '@/utils/client';
import { PagedResponse } from '@/utils/biz';
import { QuerySupplierPagingInput, SupplierDto } from '@/utils/swagger';
import { Button, Card, message, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';
import XForm from './form';
import XSearchForm from './search_form';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<SupplierDto>>({
    Items: [],
    TotalCount: 0,
  });
  const [query, _query] = useState<QuerySupplierPagingInput>({
    Page: 1,
  });

  const [formData, _formData] = useState<SupplierDto | undefined>(undefined);
  const [loadingId, _loadingId] = useState<string | undefined | null>('');

  const queryList = (q: QuerySupplierPagingInput) => {
    _loading(true);
    apiClient.mallAdmin
      .supplierQueryPaging(q)
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          _data(res.data || {});
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  const deleteBrand = (row: SupplierDto) => {
    if (!confirm('删除品牌？')) {
      return;
    }
    _loadingId(row.Id);
    apiClient.mallAdmin
      .supplierUpdateStatus({
        Id: row.Id,
        IsDeleted: true,
      })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('删除成功');
          queryList(query);
        }
      })
      .finally(() => {
        _loadingId('');
      });
  };

  const columns: ColumnProps<SupplierDto>[] = [
    {
      title: '名称',
      render: (d, x) => x.Name,
    },
    {
      title: '联系人',
      render: (d, x) => x.ContactName || '--',
    },
    {
      title: '联系电话',
      render: (d, x) => x.Telephone || '--',
    },
    {
      title: '地址',
      render: (d, x) => x.Address || '--',
    },
    {
      title: '操作',
      width: 200,
      render: (d, record: SupplierDto) => {
        return (
          <Button.Group>
            <Button
              type='link'
              onClick={() => {
                _formData(record);
              }}
            >
              编辑
            </Button>
            <Button
              loading={loadingId == record.Id}
              type='link'
              danger
              onClick={() => {
                deleteBrand(record);
              }}
            >
              删除
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList(query);
  }, []);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList(query);
        }}
      />
      <XSearchForm
        query={query}
        onSearch={(q) => {
          _query(q);
          queryList(q);
        }}
      />
      <Card
        title='供应商'
        extra={
          <Button
            type='primary'
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              const q: QuerySupplierPagingInput = {
                ...query,
                Page: e,
              };
              _query(q);
              queryList(q);
            },
          }}
        />
      </Card>
    </>
  );
};
