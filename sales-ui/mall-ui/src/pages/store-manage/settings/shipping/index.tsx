import { Card, Segmented } from 'antd';
import { useState } from 'react';

import { EnvironmentOutlined, RocketOutlined, SendOutlined } from '@ant-design/icons';

import XDelivery from './delivery';
import XExpress from './express';
import XPickup from './pickup';

export default () => {
  const [tab, _tab] = useState<string>('express');

  return (
    <>
      <Segmented
        style={{ marginBottom: 10 }}
        size="large"
        value={tab}
        options={[
          {
            label: '邮寄',
            value: 'express',
            icon: <SendOutlined />,
          },
          {
            label: '同城配送',
            value: 'delivery',
            icon: <RocketOutlined />,
          },
          {
            label: '门店自提',
            value: 'pickup',
            icon: <EnvironmentOutlined />,
          },
        ]}
        onChange={(e) => {
          _tab(e as string);
        }}
      />
      <Card size="small">
        {tab == 'express' && <XExpress />}
        {tab == 'delivery' && <XDelivery />}
        {tab == 'pickup' && <XPickup />}
      </Card>
    </>
  );
};
