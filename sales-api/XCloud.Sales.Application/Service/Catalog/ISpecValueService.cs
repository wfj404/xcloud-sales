﻿using Volo.Abp.Validation;
using XCloud.Application.Mapper;
using XCloud.Core.Helper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface ISpecValueService : ISalesAppService
{
    Task<SpecValueDto[]> AttachSkusAsync(SpecValueDto[] data);

    Task<SpecValue> InsertAsync(SpecValueDto dto);

    Task<SpecValue> UpdateAsync(SpecValueDto dto);

    Task DeleteByIdAsync(int id);

    Task<SpecDto[]> AttachValuesAsync(SpecDto[] data);
}

public class SpecValueService : SalesAppService, ISpecValueService
{
    private readonly ISalesRepository<SpecValue> _repository;

    public SpecValueService(ISalesRepository<SpecValue> repository)
    {
        _repository = repository;
    }

    public async Task<SpecValue> InsertAsync(SpecValueDto dto)
    {
        var entity = dto.MapTo<SpecValueDto, SpecValue>(this.ObjectMapper);

        if (string.IsNullOrWhiteSpace(entity.Name))
            throw new ArgumentNullException(nameof(entity.Name));

        await CheckExistAsync(entity.SpecId, entity.Name);

        await this._repository.InsertAsync(entity);

        return entity;
    }

    public async Task<SpecValue> UpdateAsync(SpecValueDto dto)
    {
        if (dto.Id <= 0)
            throw new ArgumentNullException(nameof(dto.Id));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        await CheckExistAsync(entity.SpecId, dto.Name, exceptId: entity.Id);

        entity.Name = dto.Name;
        entity.AssociatedSkuId = dto.AssociatedSkuId;
        entity.AssociatedSkuCount = dto.AssociatedSkuCount;
        entity.PriceOffset = dto.PriceOffset;

        await this._repository.UpdateAsync(entity);

        return entity;
    }

    public async Task<SpecValueDto[]> AttachSkusAsync(SpecValueDto[] data)
    {
        var ids = data.Select(x => x.AssociatedSkuId).WhereNotEmpty().Distinct().ToArray();
        if (ids.Any())
        {
            var db = await this._repository.GetDbContextAsync();

            var datalist = await db.Set<Sku>().AsNoTracking().WhereIdIn(ids).ToArrayAsync();

            foreach (var m in data)
            {
                m.AssociatedSku = datalist.FirstOrDefault(x => x.Id == m.AssociatedSkuId)
                    ?.MapTo<Sku, SkuDto>(this.ObjectMapper);
            }
        }

        return data;
    }

    public async Task<SpecDto[]> AttachValuesAsync(SpecDto[] data)
    {
        if (ValidateHelper.IsEmptyCollection(data))
            return data;

        var db = await _repository.GetDbContextAsync();

        var ids = data.Ids();

        var values = await db.Set<SpecValue>().AsNoTracking()
            .Where(x => ids.Contains(x.SpecId)).ToArrayAsync();

        foreach (var m in data)
        {
            m.Values = values
                .Where(x => x.SpecId == m.Id)
                .MapArrayTo<SpecValue, SpecValueDto>(this.ObjectMapper)
                .ToArray();
        }

        return data;
    }

    private async Task CheckExistAsync(int specId, string name, int? exceptId = default)
    {
        if (specId <= 0)
            throw new ArgumentNullException(nameof(specId));

        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<SpecValue>().AsNoTracking();

        query = query.Where(x => x.SpecId == specId);

        query = query.Where(x => x.Name == name);

        if (exceptId != null && exceptId.Value > 0)
        {
            query = query.Where(x => x.Id != exceptId.Value);
        }

        if (await query.AnyAsync())
        {
            throw new AbpValidationException(message: "spec name already exist");
        }
    }

    public async Task DeleteByIdAsync(int id)
    {
        var entity = await this._repository.GetRequiredByIdAsync(id);

        entity.IsDeleted = true;

        await this._repository.UpdateAsync(entity);
    }
}