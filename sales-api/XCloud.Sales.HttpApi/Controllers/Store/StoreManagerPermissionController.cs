﻿using XCloud.Application.Extension;
using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Exceptions;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/store-manager-permission")]
public class StoreManagerPermissionController : ShopBaseController
{
    private readonly IStoreManagerPermissionService _storeManagerPermissionService;
    private readonly IStoreRoleService _storeRoleService;

    public StoreManagerPermissionController(IStoreRoleService storeRoleService,
        IStoreManagerPermissionService storeManagerPermissionService)
    {
        _storeRoleService = storeRoleService;
        _storeManagerPermissionService = storeManagerPermissionService;
    }

    [HttpPost("list-roles")]
    public async Task<ApiResponse<StoreRoleDto[]>> ListRolesAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeManagerPermissionService.CheckRequiredSuperManagerAsync(manager.Id);

        var roles = await this._storeRoleService.ListAllRolesAsync(manager.StoreId);

        await this._storeRoleService.AttachPermissionKeysAsync(roles);

        return new ApiResponse<StoreRoleDto[]>(roles);
    }

    [HttpPost("save-role")]
    public async Task<ApiResponse<object>> SaveRoleAsync([FromBody] StoreRoleDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeManagerPermissionService.CheckRequiredSuperManagerAsync(manager.Id);

        dto.StoreId = manager.StoreId;

        if (dto.IsEmptyId())
        {
            await this._storeRoleService.InsertAsync(dto);
        }
        else
        {
            await this._storeRoleService.UpdateAsync(dto);
        }

        return new ApiResponse<object>();
    }

    [HttpPost("set-role-permissions")]
    public async Task<ApiResponse<object>> SetRolePermissionsAsync([FromBody] KeyValuePair<string, string[]> dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeManagerPermissionService.CheckRequiredSuperManagerAsync(manager.Id);

        await this._storeRoleService.SetRolePermissionsAsync(dto.Key, dto.Value);

        return new ApiResponse<object>();
    }

    [HttpPost("set-manager-roles")]
    public async Task<ApiResponse<object>> SetManagerRolesAsync([FromBody] KeyValuePair<string, string[]> dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeManagerPermissionService.CheckRequiredSuperManagerAsync(manager.Id);

        await this._storeRoleService.SetManagerRolesAsync(dto.Key, dto.Value);

        return new ApiResponse<object>();
    }

    [HttpPost("delete-role")]
    public async Task<ApiResponse<object>> DeleteRoleAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this._storeManagerPermissionService.CheckRequiredSuperManagerAsync(manager.Id);

        var role = await this._storeRoleService.GetByIdAsync(dto.Id);

        if (role == null)
            throw new EntityNotFoundException(nameof(DeleteRoleAsync));

        if (role.StoreId != manager.StoreId)
            throw new PermissionRequiredException(nameof(DeleteRoleAsync));

        await this._storeRoleService.DeleteByIdAsync(role.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("my-permissions")]
    public async Task<StoreManagerGrantedPermissionResponse> GetMyPermissionAsync()
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var response =
            await this._storeManagerPermissionService.GetGrantedPermissionsAsync(manager.Id,
                new CacheStrategy() { Refresh = true });

        return response;
    }
}