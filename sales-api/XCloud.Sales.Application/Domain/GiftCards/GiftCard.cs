﻿namespace XCloud.Sales.Application.Domain.GiftCards;

public class GiftCard : SalesBaseEntity<string>, IHasCreationTime, ISoftDelete
{
    public decimal Amount { get; set; }
    public DateTime? EndTime { get; set; }
    public int UserId { get; set; }
    public bool Used { get; set; }
    public DateTime? UsedTime { get; set; }

    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }

    public DateTime CreationTime { get; set; }
}