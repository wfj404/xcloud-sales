using Volo.Abp.Modularity;
using XCloud.Sales.WebApi;

namespace XCloud.Sales.Test;

[DependsOn(typeof(SalesWebApiModule))]
public class PlatformTestModule : AbpModule
{
    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //
    }
}