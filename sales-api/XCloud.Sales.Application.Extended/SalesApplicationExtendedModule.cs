﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.Extension;
using XCloud.EntityFrameworkCore.MySql;
using XCloud.Platform.Application;
using XCloud.Sales.Application.Extended.Database;
using XCloud.Sales.Application.Extended.DatabaseMigrator;
using XCloud.Sales.Application.Extended.Mapper;
using XCloud.Sales.ThirdParty;
using XCloud.Sales.Warehouses;
using Z.EntityFramework.Extensions;

namespace XCloud.Sales.Application.Extended;

[DependsOn(
    typeof(XCloudEntityFrameworkCoreMySqlModule),
    typeof(PlatformApplicationModule),
    typeof(SalesApplicationModule),
    typeof(SalesWarehouseApplicationModule),
    typeof(SalesThirdPartyModule)
)]
[MethodMetric]
public class SalesApplicationExtendedModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        EntityFrameworkManager.IsCommunity = true;
        context.ConfigureExtendedMysql();
        context.AddDatabaseMigrator();
        context.AddAutoMapperProfile<ExtendedAutoMapperConfiguration>();
        context.AddMvcPart<SalesApplicationExtendedModule>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        using var s = context.ServiceProvider.CreateScope();
        var logger = s.ServiceProvider.GetRequiredService<ILogger<SalesApplicationExtendedModule>>();

        try
        {
            s.ServiceProvider.ExecuteDatabaseMigrator();
        }
        catch (Exception e)
        {
            logger.LogError(message: e.Message, exception: e);
        }
    }
}