using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Helper;
using XCloud.Platform.Messenger.Abstraction.Core;
using Exception = System.Exception;

namespace XCloud.Platform.Messenger.Abstraction.WebSocketImpl;

[ExposeServices(typeof(WebsocketEndpointMiddleware))]
public class WebsocketEndpointMiddleware : IScopedDependency
{
    private readonly RequestDelegate _requestDelegate;
    private readonly string _path;

    public WebsocketEndpointMiddleware(RequestDelegate requestDelegate, string path)
    {
        if (string.IsNullOrWhiteSpace(path))
            throw new ArgumentNullException(nameof(path));

        _requestDelegate = requestDelegate;
        _path = path;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (ValidateHelper.RoutePathEqual(context.Request.Path, _path) && context.WebSockets.IsWebSocketRequest)
        {
            var logger = context.RequestServices.GetRequiredService<ILogger<WebsocketEndpointMiddleware>>();

            using var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(context.RequestAborted);

            using var webSocket = await context.WebSockets.AcceptWebSocketAsync(new WebSocketAcceptContext { });

            try
            {
                var server = context.RequestServices.GetRequiredService<IMessengerServer>();

                await webSocket.SendTextAsync("hello-world", server.Encoding, context.RequestAborted);

                using var connection = new WebsocketConnection(context.RequestServices, server, webSocket);
                connection.ClientIdentity = new ClientIdentity()
                {
                    DeviceType = context.Request?.Query["device"] ?? "default-device",
                };

                await using var _ = await connection.JoinServerAsync();

                await connection.StartBlockedMessageLoopAsync();
            }
            catch (Exception e)
            {
                logger.LogError(message: e.Message, exception: e);
            }
            finally
            {
                logger.LogInformation("socket normal close");
                await webSocket.NormalCloseAsync();
            }
        }
        else
        {
            await _requestDelegate.Invoke(context);
        }
    }
}