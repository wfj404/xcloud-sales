import { Avatar, Tag } from 'antd';
import { useEffect, useState } from 'react';

import XChatBox, { ChatItem } from '@/components/common/chat';
import { apiClient } from '@/utils/client';
import { AfterSalesCommentDto, AfterSalesDto } from '@/utils/swagger';
import {
  errorMsg,
  handleResponse,
  isStringEmpty,
  successMsg,
} from '@/utils/utils';
import { UserOutlined } from '@ant-design/icons';

export default ({ model }: { model: AfterSalesDto }) => {
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<AfterSalesCommentDto[]>([]);

  const queryComment = () => {
    if (!model || isStringEmpty(model.Id)) {
      return;
    }
    _loading(true);
    apiClient.mallManager
      .afterSaleQueryCommentPaging({
        Page: 1,
        PageSize: 100,
        SkipCalculateTotalCount: true,
        AfterSalesId: model.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          const items = res.data.Items || [];
          _data((x) => [...items]);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const sendComment = (content: string) => {
    if (isStringEmpty(content)) {
      errorMsg('请输入内容');
      return;
    }
    _loading(true);
    apiClient.mallManager
      .afterSaleInsertComment({
        AfterSaleId: model.Id,
        Content: content,
        Pictures: null,
      })
      .then((res) => {
        handleResponse(res, () => {
          successMsg('发布成功');
          queryComment();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    if (model && model.Id) {
      queryComment();
    }
  }, [model]);

  const getChatItem = (x: AfterSalesCommentDto): ChatItem => {
    const nick_name = x.IsAdmin ? '卖家' : '买家';
    return {
      nick_name: (
        <Tag bordered color={x.IsAdmin ? 'blue' : 'green'}>
          {nick_name}
        </Tag>
      ),
      avatar: <Avatar icon={<UserOutlined />}>{nick_name}</Avatar>,
      content: x.Content,
      time: x.CreationTime,
    };
  };

  const datalist = data.reverse().map<ChatItem>((x) => getChatItem(x));

  return (
    <>
      <XChatBox
        loading={loading}
        datalist={datalist}
        onSend={(e) => {
          sendComment(e);
        }}
      />
    </>
  );
};
