import XShippingStatus from '@/components/status/delivery';
import XStatus from '@/components/status/order';
import XPaymentStatus from '@/components/status/payment';
import { OrderDto } from '@/utils/swagger';
import { Box, Tooltip, Typography } from '@mui/material';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';

export default function(props: { model: OrderDto }) {
  const { model } = props;

  return (
    <Box sx={{ p: 2, my: 1 }}>
      <Typography variant='h6' gutterBottom>
        订单信息
      </Typography>
      <TableContainer component={Paper}>
        <Table sx={{}} size='small'>
          <TableBody>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                订单号
              </TableCell>
              <TableCell align='right'>{model.OrderSn}</TableCell>
            </TableRow>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                订单状态
              </TableCell>
              <TableCell align='right'>
                <XStatus model={model} />
              </TableCell>
            </TableRow>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                支付状态
              </TableCell>
              <TableCell align='right'>
                <XPaymentStatus model={model} />
              </TableCell>
            </TableRow>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                交付状态
              </TableCell>
              <TableCell align='right'>
                <XShippingStatus model={model} />
              </TableCell>
            </TableRow>
            <TableRow
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                创建时间
              </TableCell>
              <TableCell align='right'>
                <Tooltip title={model.CreationTime || '--'}>
                  <span>
                    {formatRelativeTimeFromNow(model.CreationTime || '') || '--'}
                  </span>
                </Tooltip>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}
