import { Stepper } from 'antd-mobile';
import { useEffect, useState } from 'react';
import { history, useSnapshot } from 'umi';

import XSkuPrice from '@/components/goods/sku/price';
import { storeState, StoreType } from '@/store';
import {
  getSkuMaxAmountInOnePurchase,
  getSkuMinAmountInOnePurchase,
  isStoreUserLogin,
  redirectToLogin,
} from '@/utils/biz';
import { apiClient, sendActivityLog } from '@/utils/client';
import { SkuDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty, successMsg } from '@/utils/utils';
import { LoadingButton } from '@mui/lab';
import { Alert, Box, Divider, Grid, Typography } from '@mui/material';

export default function ({ selectedSku }: { selectedSku?: SkuDto }) {
  const app = useSnapshot(storeState) as StoreType;

  const [addCartCount, _addCartCount] = useState<number | undefined>(undefined);
  const [addCartLoading, _addCartLoading] = useState(false);

  const sku: SkuDto = selectedSku || {};
  const skuValid = !isStringEmpty(sku.Id);

  const minQuantity = getSkuMinAmountInOnePurchase(sku);
  const maxQuantity = Math.min(
    getSkuMaxAmountInOnePurchase(sku),
    sku.StockModel?.StockQuantity || 0,
  );

  useEffect(() => {
    if (minQuantity <= maxQuantity) {
      _addCartCount(minQuantity);
    }
  }, [minQuantity, maxQuantity]);

  const addToCart = () => {
    if (!skuValid) {
      return;
    }

    _addCartLoading(true);

    apiClient.mall
      .shoppingCartGoodsToCartV1({
        SkuId: sku.Id,
        Quantity: addCartCount,
      })
      .then((res) => {
        handleResponse(res, () => {
          sendActivityLog({
            Comment: '添加购物车',
          });

          successMsg('添加成功');

          setTimeout(() => {
            history.push('/shoppingcart');
          }, 500);
        });
      })
      .finally(() => {
        _addCartLoading(false);
      });
  };

  if (!skuValid) {
    return null;
  }

  if (sku.PriceModel == null || sku.StockModel == null) {
    return (
      <Box sx={{}}>
        <Alert variant={'filled'} severity={'warning'}>
          商品缺货
        </Alert>
      </Box>
    );
  }

  return (
    <>
      <Divider />
      <Box sx={{ padding: 2 }}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={12} md={6}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: {
                  xs: 'space-between',
                  sm: 'space-between',
                  md: 'flex-start',
                },
              }}
            >
              <Typography variant="h6" sx={{ mr: 3 }}>
                {sku.Name || '--'}
              </Typography>
              <Typography variant="subtitle1">
                <XSkuPrice model={sku} />
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: {
                  xs: 'space-between',
                  sm: 'space-between',
                  md: 'flex-end',
                },
              }}
            >
              <Stepper
                value={addCartCount}
                onChange={(e) => {
                  _addCartCount(e);
                }}
                style={{ marginRight: 15 }}
                disabled={minQuantity > maxQuantity}
                min={minQuantity}
                max={maxQuantity}
              ></Stepper>
              <LoadingButton
                variant="contained"
                color="primary"
                loading={addCartLoading}
                disabled={(addCartCount || 0) <= 0}
                onClick={(e) => {
                  if (!isStoreUserLogin(app.storeUser)) {
                    e.preventDefault();
                    if (confirm('未登录，现在去登录？')) {
                      redirectToLogin();
                    }
                    return;
                  }

                  addToCart();
                }}
              >
                加入购物车
              </LoadingButton>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
