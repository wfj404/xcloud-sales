import { ErrorBlock } from 'antd-mobile';

export default () => {
  return (
    <>
      <ErrorBlock fullPage title={'无法访问'} description={'商城账号已经被禁用'} status={'disconnected'} />
    </>
  );
};
