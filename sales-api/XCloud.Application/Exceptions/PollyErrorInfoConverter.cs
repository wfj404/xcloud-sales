﻿using System;
using Microsoft.AspNetCore.Http;
using Polly;
using Polly.CircuitBreaker;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.Application.Exceptions;

[ExposeServices(typeof(IErrorInfoConverter))]
public class PollyErrorInfoConverter : IErrorInfoConverter, ITransientDependency
{
    public int Order => int.MaxValue;

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        if (e is TimeoutException timeoutException)
        {
            return new ErrorInfo()
            {
                Message = nameof(timeoutException),
                Details = timeoutException.Message
            };
        }

        if (e is BrokenCircuitException brokenCircuitException)
        {
            return new ErrorInfo()
            {
                Message = nameof(brokenCircuitException),
                Details = brokenCircuitException.Message
            };
        }

        if (e is ExecutionRejectedException executionRejectedException)
        {
            return new ErrorInfo()
            {
                Message = nameof(executionRejectedException),
                Details = executionRejectedException.Message
            };
        }

        return null;
    }
}