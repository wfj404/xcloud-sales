import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';

export default function() {
  return (
    <Box sx={{ py: 3 }}>
      <Skeleton />
      <Skeleton animation='wave' />
      <Skeleton animation={false} />
    </Box>
  );
}
