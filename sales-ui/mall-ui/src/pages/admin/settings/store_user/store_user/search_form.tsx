import { Button, Card, Form, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { QueryUserPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';

export default (props: {
  query: QueryUserPagingInput;
  onSearch: (d: QueryUserPagingInput) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QueryUserPagingInput>({});

  const triggerSearch = (e: QueryUserPagingInput) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form({ ...query });
  }, [query]);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form
        labelCol={{
          span: 8,
        }}
      >
        <Form.Item label="激活">
          <Switch
            checked={form.Active || false}
            onChange={(e) => {
              _form({
                ...form,
                Active: e,
              });
            }}
          />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            type="primary"
            icon={<SearchOutlined />}
            onClick={() => triggerSearch(form)}
          >
            搜索
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};
