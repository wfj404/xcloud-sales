import { Button, Form, Input, message, Modal, Space, Spin } from 'antd';
import { useEffect, useState } from 'react';

import XDeleteButton from '@/components/common/delete_button';
import { apiClient } from '@/utils/client';
import { TagDto } from '@/utils/swagger';
import { handleResponse, isStringEmpty } from '@/utils/utils';

export default ({
  show,
  hide,
  data,
  ok,
}: {
  show: boolean;
  hide: () => void;
  data: TagDto;
  ok: () => void;
}) => {
  const [loading, _loading] = useState(false);
  const [model, _model] = useState<TagDto>({});

  const save = (row: TagDto) => {
    row.Name = row.Name?.trim();

    if (isStringEmpty(row.Name)) {
      return;
    }

    _loading(true);

    apiClient.mallAdmin
      .tagSaveTag({ ...row })
      .then((res) => {
        if (res.data.Error) {
          message.error(res.data.Error.Message);
        } else {
          message.success('保存成功');
          ok();
        }
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    _model({
      ...(data || {}),
    });
  }, [data]);

  return (
    <>
      <Modal
        title={'标签'}
        open={show}
        onCancel={() => hide()}
        footer={
          <Space>
            <XDeleteButton
              action={async () => {
                let res = await apiClient.mallAdmin.tagDelete({ Id: model.Id });
                handleResponse(res, () => {
                  hide();
                  ok();
                });
              }}
              hide={isStringEmpty(model.Id)}
            />
            <Button
              type={'primary'}
              onClick={() => {
                save(model);
              }}
            >
              保存
            </Button>
          </Space>
        }
      >
        <Spin spinning={loading}>
          <Form
            labelCol={{ flex: '110px' }}
            labelAlign="right"
            wrapperCol={{ flex: 1 }}
          >
            <Form.Item label="名称" required>
              <Input
                count={{
                  show: true,
                  max: 20,
                }}
                maxLength={20}
                value={model.Name || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Name: e.target.value,
                  });
                }}
              />
              {isStringEmpty(model.Name) && (
                <p style={{ color: 'red' }}>必填</p>
              )}
            </Form.Item>
            <Form.Item label="描述">
              <Input.TextArea
                maxLength={200}
                value={model.Description || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Description: e.target.value,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="提示">
              <Input
                maxLength={50}
                value={model.Alert || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Alert: e.target.value,
                  });
                }}
              />
            </Form.Item>

            <Form.Item label="连接" tooltip={'暂不可用'}>
              <Input
                maxLength={1000}
                value={model.Link || ''}
                onChange={(e) => {
                  _model({
                    ...model,
                    Link: e.target.value,
                  });
                }}
              />
            </Form.Item>
          </Form>
        </Spin>
      </Modal>
    </>
  );
};
