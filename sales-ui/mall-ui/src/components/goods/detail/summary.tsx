import { Image } from 'antd-mobile';
import { last } from 'lodash-es';

import { resolveUrl } from '@/utils/storage';
import { GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box, Divider, Stack, Typography } from '@mui/material';

export default ({ model }: { model: GoodsDto }) => {
  const renderInfos = () => {
    let infos: string[] = [
      model.Brand?.Name || '',
      model.Category?.Name || '',
      ...(model.Tags?.map((x) => x.Name || '') || []),
    ];

    infos = infos.filter((x) => !isStringEmpty(x));

    if (infos.length <= 0) {
      return null;
    }

    return (
      <>
        <Stack
          sx={{ my: 0.5 }}
          direction={'row'}
          divider={<Divider orientation="vertical" flexItem />}
          spacing={1}
        >
          {renderArea()}
          {infos.map((x, i) => (
            <Typography key={i} variant="caption" color="text.disabled">
              {x}
            </Typography>
          ))}
        </Stack>
      </>
    );
  };

  const renderArea = () => {
    if (!model.Area?.PathList) {
      return null;
    }

    const name = model.Area?.PathList?.map((x) => x.Name).join('/');

    if (isStringEmpty(name)) {
      return null;
    }

    const meta_list = model.Area.PathList?.map((x) => x.StorageMeta).filter(
      (x) => x != undefined,
    );

    const url = resolveUrl(last(meta_list), {
      width: 20,
    });

    return (
      <Stack direction={'row'} spacing={1} minWidth={0}>
        {url && <Image src={url} alt="" fit="contain" />}
        <Typography variant="caption" color="text.disabled" noWrap>
          {name}
        </Typography>
      </Stack>
    );
  };

  return (
    <>
      <Box
        sx={{
          marginBottom: '10px',
          p: 1,
        }}
      >
        <Typography variant="h4" component={'div'}>
          {model.Name || '--'}
        </Typography>
        {isStringEmpty(model.AdminComment) || (
          <Typography
            variant="overline"
            color="text.disabled"
            component={'div'}
          >
            {`${model.AdminComment}`}
          </Typography>
        )}
        {renderInfos()}
        {isStringEmpty(model.Description) || (
          <Typography
            variant="overline"
            color="text.disabled"
            component={'div'}
          >
            {model.Description}
          </Typography>
        )}
      </Box>
    </>
  );
};
