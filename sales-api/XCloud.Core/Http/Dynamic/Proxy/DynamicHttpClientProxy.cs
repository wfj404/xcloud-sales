﻿using System.Reflection;
using System.Threading;
using Castle.DynamicProxy;
using Microsoft.Extensions.Caching.Memory;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Threading;
using XCloud.Core.Configuration;
using XCloud.Core.Http.Dynamic.Definition;
using XCloud.Core.Json;

namespace XCloud.Core.Http.Dynamic.Proxy;

[ExposeServices(typeof(IInterceptor), typeof(DynamicHttpClientProxy))]
public class DynamicHttpClientProxy : IInterceptor, IDisposable, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;
    private readonly AppConfig _appConfig;
    private readonly IJsonDataSerializer _jsonDataSerializer;
    private readonly IMemoryCache _memoryCache;

    public DynamicHttpClientProxy(IMemoryCache memoryCache, AppConfig appConfig, IJsonDataSerializer jsonDataSerializer,
        IServiceProvider serviceProvider)
    {
        _memoryCache = memoryCache;
        _appConfig = appConfig;
        _jsonDataSerializer = jsonDataSerializer;
        _serviceProvider = serviceProvider;
    }

    static MethodInfo MakeRealApiCallMethod(ActionDefinition actionDefinition, Type responseType)
    {
        var originApiCallMethod = typeof(ApiCallHelper).GetPublicStaticMethods()
            .FirstOrDefault(x => x.Name == nameof(ApiCallHelper.MakeApiRequest));

        if (originApiCallMethod == null)
            throw new AbpException("请注意方法访问级别");

        var realApiCallMethod =
            originApiCallMethod.MakeGenericMethod(actionDefinition.ServiceDefinition.ServiceInterface, responseType);
        return realApiCallMethod;
    }

    MethodInfo GetCachedRealApiCallMethod(ActionDefinition actionDefinition, Type responseType)
    {
        var method = actionDefinition.ActionMethod;

        //创建泛型方法
        var parameterFlag = string.Join('|', method.GetParameters().Select(x => $"{x.ParameterType.Name}->{x.Name}"));
        var methodFlag = $"{method.DeclaringType?.FullName}.{method.IsGenericMethod}.{method.Name}.{parameterFlag}"
            .RemoveWhitespace();

        var realApiCallMethod = _memoryCache.GetOrCreate(methodFlag, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);
            return new MethodWrapper(MakeRealApiCallMethod(actionDefinition, responseType));
        })?.Method;

        return realApiCallMethod ?? throw new ArgumentNullException(nameof(realApiCallMethod));
    }

    void ParseMethodParameter(ActionDefinition actionDefinition, IInvocation invocation,
        out ApiCallDescriptor apiDescriptor)
    {
        var args = actionDefinition.GetMethodParameters(invocation.Arguments);

        ApiCallHelper.ParseParameters(args, out var requestArgs, out var token);

        if (token == null)
        {
            //如果入参没有取消token，那么尝试从abp的对象中拿（mvc环境下是请求取消）
            var tokenProvider = _serviceProvider.GetService<ICancellationTokenProvider>();
            token = tokenProvider?.Token ?? CancellationToken.None;
        }

        apiDescriptor = new ApiCallDescriptor(actionDefinition, requestArgs, token.Value);
    }

    /// <summary>
    /// 判断方法是否可以代理
    /// </summary>
    /// <param name="actionDefinition"></param>
    /// <returns></returns>
    static bool IsMethodInterceptorable(ActionDefinition actionDefinition) =>
        actionDefinition.ActionMethod.IsPublic && !actionDefinition.ActionMethod.IsGenericMethod &&
        actionDefinition.ServiceDefinition.IsValidService() &&
        actionDefinition.IsAsyncMethod();

    public void Intercept(IInvocation invocation)
    {
        var method = invocation.Method;

        var declaringType = method.DeclaringType;
        if (!declaringType.IsInterface)
            throw new AbpException("declare type should be interface");

        var serviceDefinitionCacheKey = $"{method.DeclaringType?.FullName}".RemoveWhitespace();

        var serviceDefinition = _memoryCache.GetOrCreate(serviceDefinitionCacheKey, x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(10);
            return new ServiceDefinition(declaringType);
        });

        var actionDefinition = serviceDefinition.AllMethodDefinitions.FirstOrDefault(x => x.ActionMethod == method);
        if (actionDefinition == null)
        {
            throw new NotSupportedException("不支持代理");
        }

        if (actionDefinition.IsDisposeMethod())
        {
            DisposeClient(invocation);
        }
        else if (IsMethodInterceptorable(actionDefinition))
        {
            if (actionDefinition.ParameterDefinitions.Any(x => !x.IsValidated()))
            {
                throw new DynamicHttpClientException("方法参数配置错误");
            }

            //创建调用代理
            var responseType = actionDefinition.GetTaskRealReturnType();
            if (responseType == null)
            {
                /*
                 如果返回是Task<User>那么构造一个返回Task<User>的调用方法
                 如果返回值Task，那么构造一个Task<string>的调用方法(class Task<string>:Task{})
                 */
                responseType = typeof(string);
            }

            //提取方法参数
            ParseMethodParameter(actionDefinition, invocation, out var apiDescriptor);

            //创建泛型方法
            var realApiCallMethod = GetCachedRealApiCallMethod(actionDefinition, responseType);

            //执行网络请求
            invocation.ReturnValue = realApiCallMethod.Invoke(
                obj: null,
                parameters: new object[] { _serviceProvider, _appConfig, _jsonDataSerializer, apiDescriptor });
        }
        else
        {
            throw new NotSupportedException("无法生成api调用代理");
        }
    }

    void DisposeClient(IInvocation invocation)
    {
    }

    public void Dispose()
    {
#if DEBUG
        //
#endif
    }
}