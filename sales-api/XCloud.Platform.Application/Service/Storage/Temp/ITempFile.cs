﻿using System.IO;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace XCloud.Platform.Application.Service.Storage.Temp;

public interface ITempFile : IAsyncDisposable
{
    Task<int> GetSizeAsync();

    [NotNull]
    [ItemNotNull]
    Task<Stream> OpenStreamAsync();
}

public class LocalDiskTempFile : ITempFile
{
    [NotNull] private readonly string _localPath;

    public LocalDiskTempFile([NotNull] string localPath)
    {
        _localPath = localPath;

        if (string.IsNullOrWhiteSpace(_localPath))
            throw new FileNotFoundException(nameof(_localPath));
    }

    public async Task<int> GetSizeAsync()
    {
        var f = new FileInfo(_localPath);
        if (!f.Exists)
        {
            throw new FileNotFoundException(f.FullName);
        }

        await Task.CompletedTask;

        return (int)f.Length;
    }

    public async Task<Stream> OpenStreamAsync()
    {
        var f = new FileInfo(_localPath);
        if (!f.Exists)
        {
            throw new FileNotFoundException(f.FullName);
        }

        await Task.CompletedTask;

        var s = f.OpenRead();

        return s;
    }

    public async ValueTask DisposeAsync()
    {
        await Task.CompletedTask;

        if (File.Exists(_localPath))
        {
            File.Delete(_localPath);
        }
    }
}