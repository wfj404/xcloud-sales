using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Uow;
using XCloud.Sales.Test.Service;

namespace XCloud.Sales.Test;

[TestClass]
public class TestUserAccount : TestBase
{
    [TestMethod]
    public async Task TestUserAccountAsync()
    {
        using var s = WebApplication.Services.CreateScope();

        using var uow = s.ServiceProvider.GetRequiredService<IUnitOfWorkManager>()
            .Begin(requiresNew: true, isTransactional: true);

        try
        {
            await s.ServiceProvider.GetRequiredService<TestUserAccountService>().TestUserAccountAsync();

            await uow.CompleteAsync();
        }
        catch
        {
            await uow.RollbackAsync();
            throw;
        }
    }
}