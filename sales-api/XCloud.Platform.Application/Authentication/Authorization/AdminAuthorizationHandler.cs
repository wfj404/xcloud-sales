﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Permissions;

namespace XCloud.Platform.Application.Authentication.Authorization;

public class AdminPermissionRequirement : IAuthorizationRequirement, IEntityDto
{
    public AdminDto AdminDto { get; set; }

    public string[] Permissions { get; set; }
}

public class AdminAuthorizationHandler : AuthorizationHandler<AdminPermissionRequirement>
{
    private readonly ILogger _logger;
    private readonly IAdminSecurityService _securityService;

    public AdminAuthorizationHandler(
        ILogger<AdminAuthorizationHandler> logger,
        IAdminSecurityService securityService)
    {
        _logger = logger;
        _securityService = securityService;
    }

    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
        AdminPermissionRequirement requirement)
    {
        if (context.Resource is AdminDto adminDto)
        {
            //
        }

        if (requirement.AdminDto == null)
            throw new ArgumentNullException(nameof(requirement.AdminDto));

        if (requirement.IsEmpty())
            throw new ArgumentNullException(nameof(requirement.Permissions));

        //as super admin
        if (requirement.AdminDto.IsSuperAdmin)
        {
            _logger.LogInformation("as super admin");
            context.Succeed(requirement);
            return;
        }

        //check permissions
        if (ValidateHelper.IsNotEmptyCollection(requirement.Permissions))
        {
            var policy = new CacheStrategy { Cache = true };

            var permissionResponse = await _securityService.GetGrantedPermissionsAsync(requirement.AdminDto.Id, policy);

            var requiredPermissions = requirement.Permissions
                .Where(x => !permissionResponse.IsPermissionValid(x))
                .ToArray();

            if (requiredPermissions.Any())
            {
                _logger.LogInformation($"required permissions:{string.Join(',', requiredPermissions)}");
                context.Fail();
                return;
            }
        }

        _logger.LogInformation("permission requirement meet");
        context.Succeed(requirement);
    }
}