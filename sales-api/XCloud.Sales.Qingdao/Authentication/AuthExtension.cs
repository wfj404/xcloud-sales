﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Volo.Abp.Modularity;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Core;
using XCloud.Platform.Application.Service.Permissions;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Qingdao.Authentication;

public static class AuthExtension
{
    private static string McsTokenType => "mcs";

    public static bool IsMcsToken(this HttpContext httpContext)
    {
        httpContext.Request.Headers.TryGetValue("x-3h-account-type", out var accountType);

        return accountType == McsTokenType;
    }

    public static bool IsWapToken(this HttpContext httpContext) => !httpContext.IsMcsToken();

    public static void ReplaceAuthService(this ServiceConfigurationContext context)
    {
        context.Services.Configure<AuthenticationOptions>(option =>
        {
            option.DefaultScheme = AuthConstants.Scheme.NullHandlerScheme;
        });

        context.Services.RemoveAll<IStoreManagerAuthService>()
            .AddTransient<IStoreManagerAuthService, QingdaoStoreManagerAuthService>();

        context.Services.RemoveAll<IUserAuthService>()
            .AddTransient<IUserAuthService, QingdaoUserAuthService>();

        context.Services.RemoveAll<IAdminAuthService>()
            .AddTransient<IAdminAuthService, QingdaoAdminAuthService>();

        context.Services.RemoveAll<IAdminSecurityService>()
            .AddTransient<IAdminSecurityService, QingdaoAdminSecurityService>();

        context.Services.RemoveAll<IStoreManagerPermissionService>()
            .AddTransient<IStoreManagerPermissionService, QingdaoStoreManagerPermissionService>();
    }
}