﻿using System;
using XCloud.EntityFrameworkCore.Repository;
using XCloud.Platform.Application.Domain;
using XCloud.Platform.Application.Repository;
using XCloud.Sales.Application.Extended.Database.EntityFrameworkCore;

namespace XCloud.Sales.Application.Extended.Database.Repository;

/// <summary>
/// 会员中心仓储实现
/// </summary>
public class PlatformMySqlRepository<T> : EfRepositoryBase<ShopMySqlDbContext, T>, IPlatformRepository<T> where T : class, IPlatformBaseEntity
{
    public PlatformMySqlRepository(IServiceProvider provider) : base(provider) { }
}