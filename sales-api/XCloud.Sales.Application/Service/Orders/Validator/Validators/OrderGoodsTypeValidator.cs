using XCloud.Core.Dto;
using XCloud.Core.Json;

namespace XCloud.Sales.Application.Service.Orders.Validator.Validators;

public class OrderTypeValidatorParameter : IRuleParameter
{
    public int[] SupportedOrderTypes { get; set; }
}

[ExposeServices(typeof(IOrderValidator))]
public class OrderGoodsTypeValidator : OrderValidatorBase<OrderTypeValidatorParameter>, IOrderValidator,
    ITransientDependency
{
    private readonly IJsonDataSerializer _jsonDataSerializer;

    public OrderGoodsTypeValidator(IJsonDataSerializer jsonDataSerializer)
    {
        _jsonDataSerializer = jsonDataSerializer;
    }

    public int Order => default;

    public string ValidatorType => OrderConditionValidatorTypes.OrderType;

    public Task<bool> IsConditionTypeSupportedAsync(OrderValidatorRule rule)
    {
        return Task.FromResult(ValidatorType == rule.ValidatorType);
    }

    public async Task<string> GetRuleDescriptionAsync(OrderValidatorRule rule)
    {
        await Task.CompletedTask;

        return rule.Description ?? string.Empty;
    }

    public async Task<OrderConditionValidationResponse> IsConditionMatchedAsync(
        OrderConditionCheckDto input,
        OrderValidatorRule rule)
    {
        await Task.CompletedTask;

        var response = new OrderConditionValidationResponse();

        var parameter = ParseRequiredRuleParameter(rule.RuleJson);

        if (parameter.SupportedOrderTypes == null)
            return response.SetError("order types config error");

        if (!parameter.SupportedOrderTypes.Contains(input.Order.GoodsType))
            return response.SetError("order types not supported");

        return response;
    }
}