﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Core.Extension;

namespace XCloud.AspNetMvc.Filters;

public interface ISensitiveDataClearable
{
    void ClearSensitiveData();
}

[ExposeServices(typeof(SensitiveDataResultFilter), typeof(IAsyncResultFilter))]
public class SensitiveDataResultFilter : IAsyncResultFilter, ITransientDependency
{
    private IEnumerable<T> ExtractProperty<T>(IMemoryCache memoryCache, object value)
    {
        var resultType = value.GetType();

        //api response
        if (resultType.IsEqualToGenericType(typeof(ApiResponse<>)))
        {
            var property = memoryCache.GetOrCreate(resultType,
                d => resultType.GetProperties().FirstOrDefault(x => x.Name == nameof(ApiResponse<string>.Data)));

            if (property == null)
                throw new AbpException();

            if (property.GetValue(value) is T obj)
            {
                yield return obj;
            }
        }

        //paged
        if (resultType.IsEqualToGenericType(typeof(PagedResponse<>)))
        {
            var property = memoryCache.GetOrCreate(resultType,
                d => resultType.GetProperties().FirstOrDefault(x => x.Name == nameof(PagedResponse<string>.Items)));

            if (property == null)
                throw new AbpException();

            if (property.GetValue(value) is IEnumerable enumerable)
            {
                foreach (var m in enumerable)
                {
                    if (m != null && m is T obj)
                        yield return obj;
                }
            }
        }
    }

    public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
    {
        if (context.Result is ObjectResult result && result.Value != null)
        {
            var cache = context.HttpContext.RequestServices.GetRequiredService<IMemoryCache>();
            var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<SensitiveDataResultFilter>>();
            var config = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();

            var enabled = config["app:config:filter_sensitive_data"]?.ToBool() ?? false;

            if (enabled)
            {
                logger.LogInformation($"{nameof(SensitiveDataResultFilter)}-{context.HttpContext.Request.Path}");

                foreach (var m in ExtractProperty<ISensitiveDataClearable>(cache, result.Value))
                {
                    m.ClearSensitiveData();
                }
            }
            else
            {
                logger.LogTrace(message: "sensitive data filter is disabled");
            }
        }

        await next.Invoke();
    }
}