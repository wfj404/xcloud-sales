﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Volo.Abp.Authorization;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;

namespace XCloud.Platform.Application.Exceptions.ExceptionHandler;

public class AuthenticationErrorInfoConverter : IErrorInfoConverter, ISingletonDependency
{
    public AuthenticationErrorInfoConverter()
    {
        //
    }

    public int Order => int.MaxValue;

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        if (e is PermissionRequiredException)
        {
            return new ErrorInfo
            {
                Message = "permission required",
                HttpStatusCode = HttpStatusCode.Forbidden
            };
        }
        else if (e is AbpAuthorizationException abpAuthorizationException)
        {
            return new ErrorInfo
            {
                Message = "base auth failed",
                Details = abpAuthorizationException.Message,
                HttpStatusCode = HttpStatusCode.Unauthorized
            };
        }

        return null;
    }
}