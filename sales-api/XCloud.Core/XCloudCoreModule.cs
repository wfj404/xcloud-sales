﻿global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.Extensions.DependencyInjection.Extensions;
global using Microsoft.Extensions.Logging;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using XCloud.Core.Extension;
using Castle.DynamicProxy;
using Volo.Abp.Application;
using Volo.Abp.Autofac;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;
using Volo.Abp.Timing;
using XCloud.Core.Cache;
using XCloud.Core.Configuration;
using XCloud.Core.Http;
using XCloud.Core.Json;

namespace XCloud.Core;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(AbpHttpClientModule),
    typeof(AbpDddApplicationModule)
)]
public class XCloudCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        var builder = context.Services.GetRequiredXCloudBuilder();

        //http client
        context.ConfigureHttpClient();

        //动态代理工厂
        context.Services.AddObjectAccessor(new ProxyGenerator());

        //序列化
        context.Services.AddJsonComponent(builder.Configuration);

        //数据缓存
        context.Services.RemoveAll<ICacheProvider>();
        context.Services.AddTransient<ICacheProvider, DistributeCacheProvider>();
        context.Services.AddMemoryCache().AddDistributedMemoryCache();
    }

    public override void PostConfigureServices(ServiceConfigurationContext context)
    {
        //时间
        Configure<AbpClockOptions>(option => option.Kind = DateTimeKind.Utc);
    }
}