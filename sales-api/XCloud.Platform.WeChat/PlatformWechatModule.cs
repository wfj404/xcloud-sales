﻿using Volo.Abp;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.Extension;
using XCloud.Platform.Application;
using XCloud.Platform.WeChat.Job;
using XCloud.Platform.WeChat.Mapper;

namespace XCloud.Platform.WeChat;

[DependsOn(new[]
{
    typeof(PlatformApplicationModule),
})]
[MethodMetric]
public class PlatformWechatModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.AddAutoMapperProfile<WechatAutoMapperConfiguration>();

        context.AddMvcPart<PlatformWechatModule>();
    }

    public override void OnPostApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();

        app.ApplicationServices.StartWechatJobs();
    }
}