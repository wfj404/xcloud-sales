﻿namespace XCloud.Platform.Application.Domain.Common;

public class KeyStore : PlatformBaseEntity, IHasCreationTime
{
    public string Key { get; set; }

    public DateTime CreationTime { get; set; }
}