﻿using System.Text;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using XCloud.Core.Dto;

namespace XCloud.Core.Extension;

public static class CommonExtension
{
    private static readonly IReadOnlyCollection<string> BoolStringList =
        new List<string>
        {
            "1", "true", "yes", "on", "success", "ok",
            true.ToString().ToLower()
        }.AsReadOnly();

    /// <summary>
    /// true是1，false是0
    /// </summary>
    public static int ToBoolInt(this bool data) => data ? 1 : 0;

    /// <summary>
    /// 转换为布尔值
    /// </summary>
    public static bool ToBool(this string data) =>
        BoolStringList.Contains(data.ToLower(), StringComparer.OrdinalIgnoreCase);

    /// <summary>
    /// 字典变url格式(a=1&b=3)
    /// </summary>
    public static string ToUrlQueryString(this IDictionary<string, string> dict,
        Func<string, string> modifier = default)
    {
        modifier ??= x => x;

        var d = dict
            .Where(x => !string.IsNullOrWhiteSpace(x.Key))
            .Select(x => $"{x.Key}={modifier.Invoke(x.Value ?? string.Empty)}")
            .ToArray();

        var res = string.Join("&", d);
        return res;
    }

    /// <summary>
    /// 把一个字典加入另一个字典，重复就覆盖
    /// </summary>
    public static Dictionary<K, V> AddOrUpdate<K, V>(this Dictionary<K, V> dict, Dictionary<K, V> data)
    {
        foreach (var kv in data)
        {
            dict[kv.Key] = kv.Value;
        }

        return dict;
    }

    /// <summary>
    /// 去除空格
    /// </summary>
    public static string RemoveWhitespace(this string s) => new string(s.Where(x => x != ' ').ToArray());

    /// <summary>
    /// 如果不是有效字符串就转换为null
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string EmptyAsNull(this string str)
    {
        return string.IsNullOrWhiteSpace(str?.Trim()) ? null : str;
    }

    public static string LogLevelAsString(this LogLevel level)
    {
        switch (level)
        {
            case LogLevel.Critical:
                return nameof(LogLevel.Critical);
            case LogLevel.Debug:
                return nameof(LogLevel.Debug);
            case LogLevel.Error:
                return nameof(LogLevel.Error);
            case LogLevel.Information:
                return nameof(LogLevel.Information);
            case LogLevel.None:
                return nameof(LogLevel.None);
            case LogLevel.Trace:
                return nameof(LogLevel.Trace);
            case LogLevel.Warning:
                return nameof(LogLevel.Warning);
            default:
                return string.Empty;
        }
    }

    public static void AddError(this IList<ErrorInfo> errorInfos, string message,
        string code = default,
        string details = default)
    {
        errorInfos.Add(new ErrorInfo
        {
            Message = message,
            Code = code,
            Details = details
        });
    }

    public static string ConcatUrl(this string[] paths)
    {
        if (paths == null || paths.Any(x => x == null))
            throw new ArgumentNullException(nameof(paths));

        paths = paths.Select(x => x.Trim('/', '\\').Trim()).WhereNotEmpty().ToArray();

        return string.Join('/', paths);
    }

    public static Dictionary<string, string> ConfigAsKv(this IConfiguration config)
    {
        var dict = new Dictionary<string, string>();
        foreach (var kv in config.AsEnumerable())
        {
            dict[kv.Key] = kv.Value;
        }

        return dict;
    }

    public static string ToPrintLog(this IConfiguration configuration)
    {
        var sb = new StringBuilder();

        sb.AppendLine();

        var lines = configuration.ConfigAsKv()
            .OrderBy(x => x.Key)
            .Select(x => $"{x.Key}={x.Value}")
            .ToArray();

        lines.ToList().ForEach(x => sb.AppendLine(x));

        sb.AppendLine();

        return sb.ToString();
    }

    /// <summary>
    /// 重试
    /// </summary>
    public static async Task<T> ExecuteWithRetryAsync_<T>(this Func<Task<T>> action, int retryCount,
        Func<int, TimeSpan> delay = null)
    {
        if (retryCount < 0)
            throw new ArgumentException($"{nameof(retryCount)}");
        delay ??= i => TimeSpan.Zero;

        var error = 0;
        while (true)
        {
            try
            {
                //这里一定要await，不然task内部的异常将无法抓到
                var res = await action.Invoke();
                return res;
            }
            catch
            {
                ++error;
                if (error > retryCount)
                    throw;
                var wait = delay.Invoke(error);
                if (wait.TotalMilliseconds > 0)
                    await Task.Delay(wait);
            }
        }
    }

    /// <summary>
    /// 从list中随机取出一个item
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="ran"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static T Choice<T>(this Random ran, IList<T> list)
    {
        return ran.ChoiceIndexAndItem(list).item;
    }

    /// <summary>
    /// 随机抽取
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="ran"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static (int index, T item) ChoiceIndexAndItem<T>(this Random ran, IList<T> list)
    {
        //The maxValue for the upper-bound in the Next() method is exclusive—
        //the range includes minValue, maxValue-1, and all numbers in between.
        var index = ran.RealNext(minValue: 0, maxValue: list.Count - 1);
        return (index, list[index]);
    }

    /// <summary>
    /// 带边界的随机范围
    /// </summary>
    /// <param name="ran"></param>
    /// <param name="maxValue"></param>
    /// <returns></returns>
    public static int RealNext(this Random ran, int maxValue)
    {
        //The maxValue for the upper-bound in the Next() method is exclusive—
        //the range includes minValue, maxValue-1, and all numbers in between.
        return ran.RealNext(minValue: 0, maxValue: maxValue);
    }

    /// <summary>
    /// 带边界的随机范围
    /// </summary>
    /// <param name="ran"></param>
    /// <param name="minValue"></param>
    /// <param name="maxValue"></param>
    /// <returns></returns>
    public static int RealNext(this Random ran, int minValue, int maxValue)
    {
        //The maxValue for the upper-bound in the Next() method is exclusive—
        //the range includes minValue, maxValue-1, and all numbers in between.
        return ran.Next(minValue: minValue, maxValue: maxValue + 1);
    }

    /// <summary>
    /// 随机抽取一个后从list中移除
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="ran"></param>
    /// <param name="list"></param>
    /// <returns></returns>
    public static T PopChoice<T>(this Random ran, ref List<T> list)
    {
        var data = ran.ChoiceIndexAndItem(list);
        list.RemoveAt(data.index);
        return data.item;
    }

    /// <summary>
    /// 从list中随机抽取count个元素
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="ran"></param>
    /// <param name="list"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    public static List<T> Sample<T>(this Random ran, IList<T> list, int count)
    {
        return new int[count].Select(x => ran.Choice(list)).ToList();
    }

    /// <summary>
    /// 打乱list的顺序
    /// </summary>
    public static void Shuffle<T>(this Random ran, ref List<T> list)
    {
        var data = new List<T>();
        while (list.Any())
        {
            var itm = ran.PopChoice(ref list);
            data.Add(itm);
        }

        list.AddRange(data);
    }

    /// <summary>
    /// 根据权重选择
    /// </summary>
    public static T ChoiceByWeight<T>(this Random ran, T[] source, Func<T, int> selector)
    {
        if (source == null || !source.Any())
            throw new ArgumentNullException(nameof(source));

        if (source.Count() == 1)
            return source.First();

        if (source.Any(x => selector.Invoke(x) < 1))
            throw new ArgumentException("权重不能小于1");

        var totalWeight = source.Sum(x => selector.Invoke(x));
        //这次命中的权重
        var weight = ran.RealNext(maxValue: totalWeight);

        var cur = 0;

        foreach (var s in source)
        {
            //单个权重
            var w = selector.Invoke(s);

            var start = cur;
            var end = start + w;

            //假如在此区间
            if (weight >= start && weight <= end)
            {
                return s;
            }

            cur = end;
        }

        throw new ArgumentException("权重取值异常");
    }
}