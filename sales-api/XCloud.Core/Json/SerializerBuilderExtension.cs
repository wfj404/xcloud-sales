﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using XCloud.Core.Json.NewtonsoftJson;

namespace XCloud.Core.Json;

public static class SerializerBuilderExtension
{
    internal static void AddJsonComponent(this IServiceCollection services, IConfiguration config)
    {
        var dateFormat = config.GetDateformatOrDefault();
        var jsonSetting = new JsonSerializerSettings
        {
            Converters = new List<JsonConverter>
            {
                new IsoDateTimeConverter { DateTimeFormat = dateFormat },
            },
            DateFormatString = dateFormat,
            NullValueHandling = NullValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver(),
            MaxDepth = 20
        };
        services.AddObjectAccessor(jsonSetting);
        services.AddTransient<IJsonDataSerializer, NewtonsoftJsonDataSerializer>();
    }
}