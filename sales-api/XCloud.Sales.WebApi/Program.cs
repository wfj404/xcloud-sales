using System.IO;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Volo.Abp.Modularity.PlugIns;
using XCloud.Application.Logging;
using XCloud.AspNetMvc.Configuration;
using XCloud.AspNetMvc.Nacos;
using XCloud.Core.DependencyInjection;

namespace XCloud.Sales.WebApi;

public class Program
{
    private static void TryPrintConfigLog(IConfiguration configuration, ILogger logger)
    {
        var printLog = configuration["print_config"]?.ToBool() ?? false;
        if (!printLog)
        {
            logger.LogInformation($"skip print config");
            return;
        }

        var configAsString = configuration.ToPrintLog();

        logger.LogWarning(message: $"{nameof(configuration)}:{configAsString}");
    }

    private static WebApplication BuildApp(string[] args, ILogger logger)
    {
        using var x = logger.Metric("start-up-metric");

        var builder = WebApplication.CreateBuilder(args);

        logger.LogInformation("config autofac");
        builder.Host.UseAutofac();
        builder.Host.ConfigureContainer<ContainerBuilder>(containerBuilder =>
        {
            //builder.RegisterModule(new AutofacModuleRegister());
            //use autofac api
        });

        logger.LogInformation("set up config providers");

        builder.ConfigureConfigBuilder((configBuilder, sources) =>
        {
            configBuilder.AddJsonFile(
                Path.Combine(builder.Environment.ContentRootPath, "ocelot.json"),
                optional: false);

            configBuilder.AddJsonFile(
                Path.Combine(builder.Environment.ContentRootPath, "appsettings.json"),
                optional: false);

            configBuilder.AddJsonFile(
                Path.Combine(builder.Environment.ContentRootPath,
                    $"appsettings.{builder.Environment.EnvironmentName}.json"),
                optional: true);

            //try enable nacos config provider
            configBuilder.TryAddNacosConfigProvider(NacosDefaults.DefaultSectionName);
            configBuilder.AddEnvironmentVariables();
            configBuilder.AddCommandLine(args);
        });

        TryPrintConfigLog(builder.Configuration, logger);

        builder.Services.AddApplication<SalesWebApiModule>(option =>
        {
            var env = option.Services.GetHostingEnvironment();
            var pluginPath = Path.Combine(env.ContentRootPath, "plugin");
            if (!Directory.Exists(pluginPath))
                throw new FileNotFoundException(pluginPath);

            option.PlugInSources.AddFolder(pluginPath);
        });

        logger.LogInformation("build app");
        var app = builder.Build();

        logger.LogInformation("set engine context");
        var disposer = app.Services.SetEngineContextRootServiceProvider();
        app.Lifetime.ApplicationStopping.Register(() => disposer.Dispose());

        logger.LogInformation("initialize app");
        app.InitializeApplication();

        return app;
    }

    public static void Main(string[] args)
    {
        using var factory = LoggerFactory.Create(builder => builder.AddStartupLoggerProvider());

        StaticLoggerFactoryHolder.LoggerFactory = factory;

        var logger = factory.CreateLogger<Program>();

        try
        {
            var app = BuildApp(args, logger);

            logger.LogInformation("run");

            app.Run();
        }
        catch (Exception e)
        {
            logger.LogError(exception: e, message: e.Message);
            throw;
        }
    }
}