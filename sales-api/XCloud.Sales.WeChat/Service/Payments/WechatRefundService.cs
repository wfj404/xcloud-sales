﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SKIT.FlurlHttpClient.Wechat.TenpayV3;
using SKIT.FlurlHttpClient.Wechat.TenpayV3.Models;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities;
using XCloud.Core.Extension;
using XCloud.Platform.Application.Extension;
using XCloud.Platform.WeChat;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Payments;
using XCloud.Sales.Application.Utils;

namespace XCloud.Sales.WeChat.Service.Payments;

[ExposeServices(typeof(WechatRefundService))]
public class WechatRefundService : SalesAppService
{
    private readonly OrderUtils _orderUtils;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly WechatClientFactory _wechatClientFactory;

    public WechatRefundService(OrderUtils orderUtils,
        IOrderPaymentBillService orderPaymentBillService,
        IOrderRefundBillService orderRefundBillService,
        WechatClientFactory wechatClientFactory)
    {
        _orderUtils = orderUtils;
        _orderPaymentBillService = orderPaymentBillService;
        _orderRefundBillService = orderRefundBillService;
        _wechatClientFactory = wechatClientFactory;
    }

    private async Task<GetRefundDomesticRefundByOutRefundNumberResponse> QueryRefundByOutRefundNoAsync(
        string outRefundNumber)
    {
        var wechatPaymentClient = await this._wechatClientFactory.BuildTenPayClientAsync();

        var response =
            await wechatPaymentClient.ExecuteGetRefundDomesticRefundByOutRefundNumberAsync(new()
            {
                OutRefundNumber = outRefundNumber
            });

        response.ThrowIfNotSuccess(this.JsonDataSerializer, "failed to query refund bill");

        return response;
    }

    public async Task TryUpdateRefundBillStatusAsync(string refundBillId)
    {
        var bill = await this._orderRefundBillService.GetRequiredByIdAsync(refundBillId);

        if (bill.Refunded)
        {
            return;
        }

        if (bill.CreationTime < this.Clock.Now.AddDays(-1))
        {
            return;
        }

        var response = await this.QueryRefundByOutRefundNoAsync(bill.Id);

        if (response.Status?.ToUpper() == "SUCCESS")
        {
            await this._orderRefundBillService.MarkAsRefundedAsync(new MarkRefundBillAsRefundInput()
            {
                Id = bill.Id,
                RefundId = response.RefundId,
                NotifyData = this.JsonDataSerializer.SerializeToString(response),
                Time = response.SuccessTime?.DateTime
            });
        }
        else
        {
            var json = this.JsonDataSerializer.SerializeToString(response);
            this.Logger.LogWarning(message: $"payment not finished:{json}");
        }
    }

    public async Task<CreateRefundDomesticRefundResponse> CreateRefundOrderAsync(string refundBillId)
    {
        var refundBill = await this._orderRefundBillService.GetBillReadyForRefundAsync(refundBillId);
        if (refundBill == null)
            throw new EntityNotFoundException(nameof(refundBill));

        var bill = await _orderPaymentBillService.GetRequiredPaidBillByIdAsync(refundBill.PaymentBillId);

        var wechatPaymentClient = await this._wechatClientFactory.BuildTenPayClientAsync();

        var apiGateway = await ServiceDiscoveryService.GetRequiredPublicGatewayAddressAsync();

        var request = new CreateRefundDomesticRefundRequest
        {
            OutRefundNumber = refundBill.Id,
            OutTradeNumber = bill.Id,
            TransactionId = bill.PaymentTransactionId,
            Reason = default,
            NotifyUrl = new[]
            {
                apiGateway,
                $"api/mall/wechat-mp-payment/m-{wechatPaymentClient.Credentials.MerchantId}/refund-notify"
            }.ConcatUrl(),
            Amount = new CreateRefundDomesticRefundRequest.Types.Amount
            {
                Total = default,
                Refund = _orderUtils.CastMoneyToCent(refundBill.Price)
            },
        };

        var response = await wechatPaymentClient.ExecuteCreateRefundDomesticRefundAsync(request);

        response.ThrowIfNotSuccess(this.JsonDataSerializer, "failed to create refund order");

        return response;
    }
}