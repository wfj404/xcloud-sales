﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Auditing;
using Volo.Abp.Domain.Entities;
using XCloud.Application.Model;
using XCloud.Core.Helper;

namespace XCloud.Application.Extension;

public static class QueryableExtension
{
    public static IQueryable<T> WhereIdIn<T, TKey>(this IQueryable<T> query, TKey[] ids)
        where T : IEntity<TKey>
    {
        return query.Where(x => ids.Contains(x.Id));
    }

    /// <summary>
    /// 分页
    /// </summary>
    public static IQueryable<T> QueryPage<T>(this IOrderedQueryable<T> query, int page, int pageSize)
    {
        var skip = PageHelper.GetPagedSkip(page, pageSize);

        IQueryable<T> q = query;

        if (skip > 0)
        {
            q = q.Skip(skip);
        }

        var res = q.Take(pageSize);
        return res;
    }
    
    public static IQueryable<T> WhereCreationTimeBetween<T>(this IQueryable<T> query, 
        DateTime? startTime,
        DateTime? endTime)
        where T : class, IHasCreationTime
    {
        if (startTime != null)
            query = query.Where(x => x.CreationTime >= startTime.Value);

        if (endTime != null)
            query = query.Where(x => x.CreationTime <= endTime.Value);

        return query;
    }

    public static IQueryable<T> TakeUpTo5000<T>(this IQueryable<T> query)
    {
        query = query.Take(5000);
        return query;
    }

    public static async Task<int> CountOrDefaultAsync<T>(this IQueryable<T> query, PagedRequest dto)
    {
        var count = default(int);

        dto.SkipCalculateTotalCount ??= false;

        if (!dto.SkipCalculateTotalCount.Value)
        {
            count = await query.CountAsync();
        }

        return count;
    }
}