import logoLarge from '@/assets/logo-no-background.png';
import logoSmall from '@/assets/logo-sm.png';

let apiGateway =
  [process.env.api_gateway, window.location.origin]
    .filter((x) => x && x.length > 0)
    .at(0) || '/';
//override server address
//apiGateway = 'http://localhost:6001';
//apiGateway = 'https://swjt.qdwater.com.cn/qsgj/';
//apiGateway = 'http://localhost:7888';
//apiGateway = 'http://10.0.1.50:7888';

export const config = {
  domain: process.env.app_domain || 'https://www.domain.com',
  apiGateway,
  ws_endpoint: process.env.ws_endpoint || 'wss://www.domain.com',
  external_3h_login_url: process.env.external_3h_login_url || '',
  external_token_key: process.env.external_token_key || '',
  isDev: process.env.app_dev || false,
  UMI_ENV: process.env.UMI_ENV,
  appBase: process.env.app_base,
  app: {
    name: 'sales商城',
    englishName: 'sales',
    slogan: '',
    version: '1.0.0',
    logo: {
      small: logoSmall,
      normal: logoLarge,
    },
  },
  upload: {
    maxSize: 1024 * 1024 * 1,
  },
  color: {
    gray: '#f5f7f9',
  },
};
