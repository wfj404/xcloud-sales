namespace XCloud.Sales.Application.Domain.Catalog;

public class ValueAddedItem : SalesBaseEntity
{
    public string GroupId { get; set; }

    public decimal PriceOffset { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }
}