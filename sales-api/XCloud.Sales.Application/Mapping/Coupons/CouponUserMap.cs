﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Coupons;

namespace XCloud.Sales.Application.Mapping.Coupons;

public class CouponUserMap : SalesEntityTypeConfiguration<CouponUserMapping>
{
    public override void Configure(EntityTypeBuilder<CouponUserMapping> builder)
    {
        builder.ToTable("sales_coupon_user_mapping");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.UserId);
        builder.Property(x => x.CouponId);
        builder.Property(x => x.IsUsed);
        builder.Property(x => x.UsedTime);
        builder.Property(x => x.ExpiredAt);

        builder.Property(x => x.CreationTime);

        base.Configure(builder);
    }
}