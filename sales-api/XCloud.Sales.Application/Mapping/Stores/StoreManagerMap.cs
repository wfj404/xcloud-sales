﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Mapping.Stores;

public class StoreManagerMap : SalesEntityTypeConfiguration<StoreManager>
{
    public override void Configure(EntityTypeBuilder<StoreManager> builder)
    {
        builder.ToTable("sales_store_manager");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(m => m.StoreId).RequiredId();
        builder.Property(m => m.UserId).RequiredId();
        builder.Property(x => x.Role);
        builder.Property(x => x.IsActive);
        builder.Property(x => x.IsSuperManager);

        base.Configure(builder);
    }
}