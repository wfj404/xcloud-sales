﻿using System.IO;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Application.Services;
using Volo.Abp.BlobStoring;
using XCloud.Application.DistributedLock;
using XCloud.Platform.Application.Domain.Storage;
using XCloud.Platform.Application.Service.Storage.Temp;

namespace XCloud.Platform.Application.Service.Storage;

public interface IStorageService : IApplicationService
{
    Task<StorageMeta> UploadStreamAsync(FileUploadStreamArgs args);

    Task<Stream> GetFileStreamOrNullAsync(string fileKey);
}

public class StorageService : PlatformAppService, IStorageService
{
    private readonly StorageToolbox _storageToolbox;
    private readonly IBlobContainer _blobContainer;

    public StorageService(StorageToolbox storageToolbox, IBlobContainer blobContainer)
    {
        _storageToolbox = storageToolbox;
        _blobContainer = blobContainer;
    }

    [NotNull] private string StorageProviderName => Configuration.GetDefaultStorageProvider();

    private async Task<StorageMeta> BuildStorageMetaAsync(FileUploadStreamArgs args, ITempFile tempFileKey)
    {
        //hash
        await using var streamForHash = await tempFileKey.OpenStreamAsync();
        var hash = await _storageToolbox.FileHashProvider.CalculateHashStringAsync(streamForHash);

        var model = new StorageMeta(StorageProviderName)
        {
            ContentType = args.ContentType,
            FileExtension = _storageToolbox.StorageUtils.GetNormalizedFileExtension(args.FileName),
            //length
            ResourceSize = await tempFileKey.GetSizeAsync(),
            //hash
            ResourceHash = hash,
            HashType = _storageToolbox.FileHashProvider.HashType
        };
        //file key
        model.ResourceKey = $"{model.ResourceHash}.{model.FileExtension.TrimStart('.')}";

        var extraObject = new
        {
            streamType = args.Stream.GetType().FullName
        };
        model.ExtraData = JsonDataSerializer.SerializeToString(extraObject);

        return model;
    }

    public async Task<StorageMeta> UploadStreamAsync(FileUploadStreamArgs args)
    {
        if (args == null)
            throw new ArgumentNullException(nameof(args));

        if (args.Stream == null)
            throw new ArgumentNullException(nameof(args.Stream));

        if (string.IsNullOrWhiteSpace(args.FileName))
            throw new ArgumentNullException(nameof(args.FileName));

        args.Stream.ResetPositionOrThrow();

        await using var tempFileKey = await _storageToolbox.TempFileService.CreateTempFileAsync(args.Stream);

        var model = await BuildStorageMetaAsync(args, tempFileKey);

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(StorageService)}-{nameof(UploadStreamAsync)}:{model.ResourceKey}",
            expiryTime: TimeSpan.FromSeconds(10));

        //prepare temporary file
        await using var tempFileStream = await tempFileKey.OpenStreamAsync();

        //save or update file
        await _blobContainer.SaveAsync(model.ResourceKey, tempFileStream, overrideExisting: true);

        //save or update meta
        return await _storageToolbox.StorageMetaService.SaveOrUpdateResourceMetaAsync(model);
    }

    public async Task<Stream> GetFileStreamOrNullAsync(string fileKey)
    {
        if (string.IsNullOrWhiteSpace(fileKey))
            throw new ArgumentNullException(nameof(fileKey));

        var stream = await _blobContainer.GetOrNullAsync(fileKey);

        return stream;
    }
}