﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Sales.Application.Mapping;
using XCloud.Sales.Warehouses.Domain.Warehouses;

namespace XCloud.Sales.Warehouses.Mapping.Warehouses;

public class StockMap : SalesEntityTypeConfiguration<Stock>
{
    public override void Configure(EntityTypeBuilder<Stock> builder)
    {
        base.Configure(builder);

        builder.ToTable("sales_stock");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.No).IsRequired().HasMaxLength(100);
        builder.Property(x => x.SupplierId).HasMaxLength(100);
        builder.Property(x => x.WarehouseId).HasMaxLength(100);
        builder.Property(x => x.Remark).HasMaxLength(1000);
        builder.Property(x => x.Approved);
        builder.Property(x => x.ApprovedByUserId).HasMaxLength(100);
        builder.Property(x => x.ApprovedTime);
    }
}