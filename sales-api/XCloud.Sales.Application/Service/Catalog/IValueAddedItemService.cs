using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Catalog;

public interface IValueAddedItemService : ISalesAppService
{
    Task InsertAsync(ValueAddedItemDto dto);

    Task UpdateAsync(ValueAddedItemDto dto);

    Task DeleteAsync(int id);

    Task<GoodsDto[]> AttachValueAddedItemsAsync(GoodsDto[] data);
}

public class ValueAddedItemService : SalesAppService, IValueAddedItemService
{
    private readonly ISalesRepository<ValueAddedItem> _repository;

    public ValueAddedItemService(ISalesRepository<ValueAddedItem> repository)
    {
        _repository = repository;
    }

    public async Task<GoodsDto[]> AttachValueAddedItemsAsync(GoodsDto[] data)
    {
        if (!data.Any())
            return data;

        var ids = data.Ids();

        var db = await _repository.GetDbContextAsync();

        var query = from item in db.Set<ValueAddedItem>().AsNoTracking()
            join itemGroup in db.Set<ValueAddedItemGroup>().AsNoTracking()
                on item.GroupId equals itemGroup.Id
            select new { item, itemGroup };

        var list = await query.Where(x => ids.Contains(x.itemGroup.GoodsId)).ToArrayAsync();

        foreach (var m in data)
        {
            var itemList = list.Where(x => x.itemGroup.GoodsId == m.Id).ToArray();

            m.ValueAddedItems =
                itemList.Select(x => x.item).MapArrayTo<ValueAddedItem, ValueAddedItemDto>(ObjectMapper);
        }

        return data;
    }

    private async Task<bool> CheckNameExistAsync(string groupId, string name, int? exceptId = null)
    {
        var db = await _repository.GetDbContextAsync();

        var query = db.Set<ValueAddedItem>().AsNoTracking();

        query = query.Where(x => x.GroupId == groupId && x.Name == name);

        if (exceptId != null)
            query = query.Where(x => x.Id != exceptId.Value);

        return await query.AnyAsync();
    }

    public async Task InsertAsync(ValueAddedItemDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.GroupId))
            throw new ArgumentNullException(nameof(dto.GroupId));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        if (await CheckNameExistAsync(dto.GroupId, dto.Name))
            throw new UserFriendlyException("group item name already exist");

        var entity = dto.MapTo<ValueAddedItemDto, ValueAddedItem>(ObjectMapper);

        await _repository.InsertAsync(entity);
    }

    public async Task UpdateAsync(ValueAddedItemDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (dto.Id <= 0)
            throw new ArgumentNullException(nameof(dto.Id));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        var entity = await _repository.FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.PriceOffset = dto.PriceOffset;

        if (await CheckNameExistAsync(entity.GroupId, entity.Name, entity.Id))
            throw new UserFriendlyException("group item name already exist");

        await _repository.UpdateAsync(entity);
    }

    public async Task DeleteAsync(int id)
    {
        if (id <= 0)
            throw new ArgumentNullException(nameof(id));

        await _repository.DeleteAsync(x => x.Id == id);
    }
}