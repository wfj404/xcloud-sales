﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Volo.Abp.Uow;
using XCloud.Application.Model;
using XCloud.Sales.Application.Domain.GenericOrders;
using XCloud.Sales.Application.Repository;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.GenericOrders;

namespace XCloud.Sales.Qingdao.Service;

public interface IQingdaoOrderToGenericAdaptorService : ISalesAppService
{
    Task RefreshOrderStatusAsync();
}

public class QingdaoOrderToGenericAdaptorService : SalesAppService, IQingdaoOrderToGenericAdaptorService
{
    private readonly IGenericOrderService _genericOrderService;
    private readonly ISalesRepository<GenericOrder> _repository;

    public QingdaoOrderToGenericAdaptorService(IGenericOrderService genericOrderService,
        ISalesRepository<GenericOrder> repository)
    {
        _genericOrderService = genericOrderService;
        _repository = repository;
    }

    public Task<McsCustomerServiceOrder> GetByIdAsync(string id)
    {
        throw new NotImplementedException();
    }

    public Task CreateOrUpdateGenericOrderAsync(string orderId)
    {
        throw new NotImplementedException();
    }

    public async Task RefreshOrderStatusAsync()
    {
        var key = $"qingdao.{nameof(QingdaoOrderToGenericAdaptorService)}.{nameof(RefreshOrderStatusAsync)}";

        await using var _ =
            await this.DistributedLockProvider.CreateLockAsync(resource: key,
                expiryTime: TimeSpan.FromSeconds(5));

        if (!_.IsAcquired)
        {
            this.Logger.LogInformation($"failed to get lock:{key}");
            return;
        }

        var page = 0;

        while (true)
        {
            using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

            try
            {
                var db = await this._repository.GetDbContextAsync();

                var query = db.Set<GenericOrder>().AsNoTracking();

                query = query.Where(x => !x.Finished && !x.Closed);

                query = query.OrderBy(x => x.CreationTime).ThenBy(x => x.Id);

                var datalist = await query
                    .PageBy(new PagedRequest() { Page = ++page, PageSize = 100 }.ToAbpPagedRequest())
                    .ToArrayAsync();

                if (!datalist.Any())
                {
                    break;
                }

                foreach (var m in datalist)
                {
                    //
                }

                await uow.CompleteAsync();
            }
            catch
            {
                await uow.RollbackAsync();
                throw;
            }
        }

        await Task.CompletedTask;
    }
}