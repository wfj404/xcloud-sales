﻿using Microsoft.Extensions.Logging;
using Volo.Abp.Modularity;
using XCloud.Application.Core;
using XCloud.Application.Logging;
using XCloud.Core.Configuration;
using XCloud.Sales.Configuration;

namespace XCloud.Sales.Qingdao.WebApi;

[DependsOn(
    typeof(QingdaoSalesApplicationModule),
    typeof(SalesConfigurationModule)
)]
[MethodMetric]
public class QingdaoSalesWebApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddXCloudBuilder<QingdaoSalesWebApiModule>();
        StaticLoggerFactoryHolder.LoggerFactory?.CreateLogger<QingdaoSalesWebApiModule>()
            .LogInformation("add x-cloud builder");
    }
}