﻿using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Users;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Users;

public interface IStoreUserAccountService : ISalesAppService
{
    Task UpdateStatusAsync(UpdateUserStatusInput dto);

    [NotNull]
    Task<StoreUserDto> GetOrCreateByPlatformUserIdAsync(string userId);

    Task<StoreUserDto> GetStoreUserAuthValidationDataAsync(string userId,
        CacheStrategy cacheStrategyOption);
}

public class StoreUserAccountService : SalesAppService, IStoreUserAccountService
{
    private readonly ISalesRepository<StoreUser> _repository;

    public StoreUserAccountService(ISalesRepository<StoreUser> repository)
    {
        _repository = repository;
    }

    public async Task UpdateStatusAsync(UpdateUserStatusInput dto)
    {
        var db = await _repository.GetDbContextAsync();

        var entity = await db.Set<StoreUser>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (entity == null)
            throw new EntityNotFoundException(nameof(UpdateStatusAsync));

        if (dto.IsActive != null)
            entity.IsActive = dto.IsActive.Value;

        entity.LastModificationTime = Clock.Now;

        await this._repository.UpdateAsync(entity, autoSave: true);

        await this.GetStoreUserAuthValidationDataAsync(entity.UserId,
            new CacheStrategy() { Refresh = true });
    }

    public async Task<StoreUserDto> GetOrCreateByPlatformUserIdAsync(string userId)
    {
        var db = await _repository.GetDbContextAsync();

        var set = db.Set<StoreUser>();

        var user = await set.IgnoreQueryFilters()
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync(x => x.UserId == userId);

        if (user == null)
        {
            user = new StoreUser
            {
                UserId = userId,
                CreationTime = Clock.Now,
                IsActive = true,
            };
            user.LastActivityTime = user.CreationTime;

            set.Add(user);

            await db.SaveChangesAsync();
        }

        return user.MapTo<StoreUser, StoreUserDto>(ObjectMapper);
    }

    public async Task<StoreUserDto> GetStoreUserAuthValidationDataAsync(string userId,
        CacheStrategy cacheStrategyOption)
    {
        var key = $"sales.user.by.platform.user.id:{userId}";

        var option = new CacheOption<StoreUserDto>(key, TimeSpan.FromMinutes(1)) { CacheCondition = x => x != null };
        var mallUser = await CacheProvider.ExecuteWithPolicyAsync(
            async () =>
            {
                var db = await _repository.GetDbContextAsync();

                var user = await db.Set<StoreUser>().IgnoreQueryFilters().AsNoTracking()
                    .OrderBy(x => x.CreationTime)
                    .FirstOrDefaultAsync(x => x.UserId == userId);

                return user?.MapTo<StoreUser, StoreUserDto>(ObjectMapper);
            },
            option,
            cacheStrategyOption);

        return mallUser;
    }
}