﻿using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Service.Admins;

namespace XCloud.Platform.Application.Service.Permissions;

[Serializable]
public abstract class GrantedPermissionResponse<T> : ApiResponse<T>
{
    public abstract bool HasAllPermission { get; }

    private string[] _permissions;

    [NotNull]
    public string[] Permissions
    {
        get => this._permissions ??= [];
        set => this._permissions = value;
    }

    public virtual bool IsPermissionValid(string permissionKey)
    {
        return
            this.HasAllPermission ||
            Permissions.Contains("*") ||
            Permissions.Contains(permissionKey);
    }
}

public class AdminGrantedPermissionResponse : GrantedPermissionResponse<AdminDto>
{
    public override bool HasAllPermission => this.Data?.IsSuperAdmin ?? false;
}

public class RoleDto : Domain.Security.Role, IEntityDto<string>
{
    public string[] PermissionKeys { get; set; }
}

public class QueryRolePaging : PagedRequest
{
    //
}

public class QueryPermissionPaging : PagedRequest
{
    //
}

public class PermissionDto : Domain.Security.Permission, IEntityDto<string>
{
    //
}

public class SetAdminRolesDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string[] RoleIds { get; set; }
}

public class SetPermissionToRoleDto : IEntityDto<string>
{
    public string Id { get; set; }
    public string[] PermissionKeys { get; set; }
}