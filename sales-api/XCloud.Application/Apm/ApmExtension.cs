﻿using System.Linq;
using JetBrains.Annotations;
using Microsoft.Extensions.Configuration;
using Volo.Abp.Application.Services;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Extension;

namespace XCloud.Application.Apm;

public static class ApmExtension
{
    [NotNull]
    public static ApmOption GetApmOptionOrDefault(this IConfiguration configuration)
    {
        var option = new ApmOption();

        var section = configuration.GetSection("apm");

        if (section.Exists())
        {
            section.Bind(option);
        }

        return option;
    }

    internal static bool ShouldTrace(this IOnServiceRegistredContext context)
    {
        var config = context.ImplementationType.GetCustomAttributesList<ApmAttribute>().FirstOrDefault();
        if (config != null)
        {
            return !config.Disabled;
        }

        var trace = context.ImplementationType.IsAssignableToType<IApplicationService>();

        trace &= context.ImplementationType.IsNormalPublicClass() &&
                 !context.ImplementationType.IsGenericType;

        return trace;
    }
}