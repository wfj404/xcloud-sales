using JetBrains.Annotations;
using Volo.Abp.Data;
using XCloud.Application.DistributedLock;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.Application.Authentication;

public interface IStoreUserAuthService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<StoreUserAuthResult> GetStoreUserAsync();
}

public class StoreUserAuthService : SalesAppService, IStoreUserAuthService
{
    private readonly IUserAuthService _userAuthService;
    private readonly IStoreUserAccountService _storeUserAccountService;
    private readonly IRequestCacheProvider _cacheProvider;

    public StoreUserAuthService(IRequestCacheProvider cacheProvider,
        IUserAuthService userAuthService,
        IStoreUserAccountService storeUserAccountService)
    {
        _cacheProvider = cacheProvider;
        _userAuthService = userAuthService;
        _storeUserAccountService = storeUserAccountService;
    }

    [NotNull]
    [ItemNotNull]
    private async Task<StoreUserDto> GetOrCreateStoreUserAsync(string globalUserId)
    {
        if (string.IsNullOrWhiteSpace(globalUserId))
            throw new ArgumentNullException(nameof(globalUserId));

        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource:
            $"create-mall-user-for-global-user.{globalUserId}",
            expiryTime: TimeSpan.FromSeconds(5));

        using var uow = this.UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);

        try
        {
            var storeUser = await _storeUserAccountService.GetOrCreateByPlatformUserIdAsync(globalUserId);

            //todo:notify worker to update sales user basic profile
            //update:invitation info,nick name avatar gender etc...

            await uow.CompleteAsync();

            return storeUser;
        }
        catch
        {
            await uow.RollbackAsync();
            throw;
        }
    }

    private async Task<StoreUserAuthResult> GetStoreUserImplAsync()
    {
        var res = new StoreUserAuthResult();

        var userAuthResult = await this._userAuthService.GetAuthUserAsync();

        if (userAuthResult.HasError())
        {
            res.SetProperty(nameof(userAuthResult), userAuthResult);
            return res.SetError("user auth failed");
        }

        var user = userAuthResult.User!;

        var storeUser = await _storeUserAccountService.GetStoreUserAuthValidationDataAsync(
            user.Id,
            new CacheStrategy { Cache = true });

        storeUser ??= await GetOrCreateStoreUserAsync(user.Id);

        storeUser.User = user;
        res.SetData(storeUser);

        return res;
    }

    public async Task<StoreUserAuthResult> GetStoreUserAsync()
    {
        return await this._cacheProvider.GetOrSetAsync(async () => await this.GetStoreUserImplAsync());
    }
}