﻿using System;
using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Timing;

namespace XCloud.Sales.WeChat.Job;

public static class WechatJobConfiguration
{
    public static void StartSalesWechatJobs(this IServiceProvider serviceProvider)
    {
        using var s = serviceProvider.CreateScope();

        var manager = s.ServiceProvider.GetRequiredService<IRecurringJobManager>();

        var clock = s.ServiceProvider.GetRequiredService<IClock>();

        //中午12点触发
        var utcTime = clock.Now.Date.AddHours(12).AddHours(-8);

        manager.AddOrUpdate<WechatBillJobs>("wechat-payment-verify-bills",
            x => x.VerifyPaymentBillsAsync(),
            Cron.Daily(hour: utcTime.Hour, minute: utcTime.Minute),
            new RecurringJobOptions()
            {
                TimeZone = TimeZoneInfo.Utc
            });
    }
}