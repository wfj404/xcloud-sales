using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class GoodsPictureMap : SalesEntityTypeConfiguration<GoodsPicture>
{
    public override void Configure(EntityTypeBuilder<GoodsPicture> builder)
    {
        builder.ToTable("sales_goods_picture");

        builder.PreConfigIntPrimaryKey();

        builder.Property(x => x.GoodsId).RequiredId();
        builder.Property(x => x.SkuId).HasMaxLength(100);
        builder.Property(x => x.PictureMetaId).HasMaxLength(100);
        builder.Property(x => x.FileName).HasMaxLength(500);
        builder.Property(x => x.DisplayOrder);

        base.Configure(builder);
    }
}