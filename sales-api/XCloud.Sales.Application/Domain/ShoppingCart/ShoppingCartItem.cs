namespace XCloud.Sales.Application.Domain.ShoppingCart;

public class ShoppingCartItem : SalesBaseEntity, IHasCreationTime, IHasModificationTime
{
    public int UserId { get; set; }

    public string SkuId { get; set; }

    public int Quantity { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}