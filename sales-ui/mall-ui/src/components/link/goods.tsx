import { Space } from 'antd';

import XGoodsSelector from '@/components/goods/goods_selector';
import { LinkDto } from '@/utils/biz';

export default ({
  link,
  onChange,
}: {
  link: LinkDto;
  onChange: (x: LinkDto) => void;
}) => {
  if (!link) {
    return null;
  }

  return (
    <>
      <Space
        direction={'vertical'}
        style={{
          width: '100%',
        }}
      >
        <XGoodsSelector
          selectedGoods={link.goods}
          onChange={(x) => {
            onChange({ ...link, goods: x });
          }}
        />
      </Space>
    </>
  );
};
