﻿namespace XCloud.Sales.Application.Service.Stores.Context;

public static class StoreContextExtension
{
    public static async Task<string> GetRequiredStoreIdAsync(this ICurrentStoreContext currentStoreContext)
    {
        var storeId = await currentStoreContext.GetStoreIdOrEmptyAsync();
        if (string.IsNullOrWhiteSpace(storeId))
        {
            throw new StoreIsRequiredException("please select store");
        }

        return storeId;
    }

    public static async Task<string> GetComposedStoreIdOrEmptyAsync(
        this ICurrentStoreSelectorContributor[] providerList)
    {
        foreach (var provider in providerList)
        {
            var storeId = await provider.GetStoreIdOrEmptyAsync();

            if (!string.IsNullOrWhiteSpace(storeId))
            {
                return storeId;
            }
        }

        return default;
    }
}