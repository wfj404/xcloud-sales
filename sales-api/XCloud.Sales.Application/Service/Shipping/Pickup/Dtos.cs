using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Application.Utils;
using XCloud.Platform.Application.Service.Settings;
using XCloud.Sales.Application.Domain.Shipping;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Shipping.Delivery;

namespace XCloud.Sales.Application.Service.Shipping.Pickup;

[TypeIdentityName("store-pickup-settings")]
public class StorePickupSettings : ShippingSettingsBase, ISettings
{
    private ServiceTimeDto _serviceTime;

    [NotNull]
    public ServiceTimeDto ServiceTime
    {
        get => this._serviceTime ??= new ServiceTimeDto();
        set => this._serviceTime = value;
    }
}

public class PickupRecordDto : PickupRecord, IEntityDto<string>
{
    public OrderDto Order { get; set; }
}

public class QueryPickupPagingInput : PagedRequest
{
    public bool? OrderToService { get; set; }

    public string StoreId { get; set; }

    public bool? Approved { get; set; }
}