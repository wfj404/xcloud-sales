﻿using System.Threading.Tasks;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.DistributedLock;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Model;
using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Platform.Application.Domain.Users;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Users;

public interface IUserAccountService : IXCloudAppService
{
    Task<MultipleErrorsDto<UserDto, ErrorInfo>> GetUserAuthValidationDataAsync(string userId,
        CacheStrategy cacheStrategy);

    Task<UserDto> GetOrCreateByExternalIdAsync(string externalId, string nickName = null);

    Task<UserDto> GetByAccountIdentityAsync(string accountIdentity);

    Task UpdateStatusAsync(UpdateUserStatusDto dto);

    Task<bool> IsUserIdentityNameExistAsync(IdentityNameDto dto);

    Task<ApiResponse<User>> PasswordLoginAsync(PasswordLoginDto dto);

    Task<User> CreateUserAccountV1Async(IdentityNameDto model);

    Task SetPasswordAsync(string userId, string password);

    Task<User> GetByIdentityNameAsync(IdentityNameDto dto);
}

public class UserAccountService : PlatformAppService, IUserAccountService
{
    private readonly IPlatformRepository<User> _userAccountRepository;
    private readonly IUserService _userService;

    public UserAccountService(IPlatformRepository<User> userRepo, IUserService userService)
    {
        _userAccountRepository = userRepo;
        _userService = userService;
    }

    public async Task<MultipleErrorsDto<UserDto, ErrorInfo>> GetUserAuthValidationDataAsync(string userId,
        CacheStrategy cacheStrategy)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var key = $"{nameof(IUserAccountService)}.{nameof(GetUserAuthValidationDataAsync)}.{userId}";
        var option = new CacheOption<MultipleErrorsDto<UserDto, ErrorInfo>>(key, TimeSpan.FromMinutes(3));

        var response = await this.CacheProvider.ExecuteWithPolicyAsync(async () =>
        {
            var res = new MultipleErrorsDto<UserDto, ErrorInfo>();

            var user = await this._userService.GetByIdAsync(userId);

            if (user == null || user.IsEmptyId())
            {
                res.ErrorList.AddError("user not exist");
                return res;
            }
            else
            {
                if (user.IsDeleted)
                {
                    res.ErrorList.AddError("user is deleted");
                }

                if (!user.IsActive)
                {
                    res.ErrorList.AddError("user is inactive");
                }

                res.SetData(user);
            }

            return res;
        }, option, cacheStrategy);

        return response ?? new MultipleErrorsDto<UserDto, ErrorInfo>();
    }

    public async Task<UserDto> GetOrCreateByExternalIdAsync(string externalId, string nickName = null)
    {
        if (string.IsNullOrWhiteSpace(externalId))
            throw new ArgumentNullException(nameof(externalId));

        await using var _ =
            await DistributedLockProvider.CreateRequiredLockAsync(
                resource: $"user.{nameof(GetOrCreateByExternalIdAsync)}.{externalId}",
                expiryTime: TimeSpan.FromSeconds(5));

        var db = await _userAccountRepository.GetDbContextAsync();

        var user = await db.Set<User>().AsNoTracking().IgnoreQueryFilters()
            .Where(x => x.ExternalId == externalId)
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync();

        if (user == null)
        {
            user = new User
            {
                Id = GuidGenerator.CreateGuidString(),
                ExternalId = externalId,
                CreationTime = Clock.Now,
                IsActive = true,
                IsDeleted = false
            };
            user.IdentityName = user.OriginIdentityName = user.Id;
            user.NickName = nickName ?? user.IdentityName;

            db.Set<User>().Add(user);

            await db.TrySaveChangesAsync();

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        return user.MapTo<User, UserDto>(ObjectMapper);
    }

    public async Task<UserDto> GetByAccountIdentityAsync(string accountIdentity)
    {
        if (string.IsNullOrWhiteSpace(accountIdentity))
            throw new ArgumentNullException(nameof(accountIdentity));

        var db = await _userAccountRepository.GetDbContextAsync();

        var query = from user in db.Set<User>().AsNoTracking()
            join i in db.Set<UserIdentity>().AsNoTracking()
                on user.Id equals i.UserId into identityGrouping
            from identity in identityGrouping.DefaultIfEmpty()
            select new { user, identity };

        var normalizedIdentityName = AccountUtils.NormalizeIdentityName(accountIdentity);

        var mobileIdentityType = (int)UserIdentityTypeEnum.MobilePhone;
        var emailIdentityType = (int)UserIdentityTypeEnum.Email;

        query = query.Where(x =>
            x.user.IdentityName == normalizedIdentityName ||
            (x.identity.IdentityType == mobileIdentityType && x.identity.MobilePhone == accountIdentity) ||
            (x.identity.IdentityType == emailIdentityType && x.identity.Email == accountIdentity));

        var data = await query.Select(x => x.user).FirstOrDefaultAsync();

        if (data == null)
            return null;

        return ObjectMapper.Map<User, UserDto>(data);
    }

    public virtual async Task<ApiResponse<User>> PasswordLoginAsync(PasswordLoginDto dto)
    {
        if (dto == null)
            throw new ArgumentNullException(nameof(dto));

        if (string.IsNullOrWhiteSpace(dto.IdentityName))
            throw new ArgumentNullException(nameof(dto.IdentityName));

        if (string.IsNullOrWhiteSpace(dto.Password))
            throw new ArgumentNullException(nameof(dto.Password));

        var data = new ApiResponse<User>();

        var userEntity = await this.GetByIdentityNameAsync(dto.IdentityName);

        if (userEntity == null)
        {
            data.SetError("用户不存在");
            return data;
        }

        if (userEntity.PassWord != AccountUtils.EncryptPassword(dto.Password))
        {
            data.SetError("密码错误");
            return data;
        }

        var userWithErrors =
            await this.GetUserAuthValidationDataAsync(userEntity.Id, new CacheStrategy() { Source = true });

        if (!userWithErrors.HasNoError())
        {
            var error = userWithErrors.ErrorList.Select(x => x.Message).WhereNotEmpty().First();
            data.SetError(error);
            return data;
        }

        data.SetData(userEntity);
        return data;
    }

    [UnitOfWork]
    public async Task<User> CreateUserAccountV1Async(IdentityNameDto model)
    {
        if (model == null || string.IsNullOrWhiteSpace(model.IdentityName))
            throw new ArgumentNullException(nameof(CreateUserAccountV1Async));

        var entity = new User();

        AccountUtils.NormalizeIdentityName(entity, model.IdentityName);
        entity.Id = GuidGenerator.CreateGuidString();
        entity.CreationTime = Clock.Now;
        entity.IsActive = true;
        entity.IsDeleted = false;

        if (!AccountUtils.ValidateIdentityName(entity.IdentityName, out var msg))
        {
            throw new UserFriendlyException(msg);
        }

        //检查用户名是否存在
        if (await IsUserIdentityNameExistAsync(entity.IdentityName))
        {
            throw new UserFriendlyException("用户名已存在");
        }

        await _userAccountRepository.InsertAsync(entity);

        return entity;
    }

    public virtual async Task SetPasswordAsync(string uid, string pwd)
    {
        if (string.IsNullOrWhiteSpace(uid) || string.IsNullOrWhiteSpace(pwd))
            throw new ArgumentNullException(nameof(SetPasswordAsync));

        var user = await _userAccountRepository.FirstOrDefaultAsync(x => x.Id == uid);
        if (user == null)
            throw new EntityNotFoundException("用户不存在，无法修改密码");

        user.PassWord = AccountUtils.EncryptPassword(pwd);

        var now = Clock.Now;

        user.LastPasswordUpdateTime = now;
        user.LastModificationTime = now;

        await _userAccountRepository.UpdateAsync(user, autoSave: true);

        await this.GetUserAuthValidationDataAsync(user.Id, new CacheStrategy() { Refresh = true });
    }

    public async Task UpdateStatusAsync(UpdateUserStatusDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto?.Id))
            throw new ArgumentNullException(nameof(UpdateStatusAsync));

        var db = await _userAccountRepository.GetDbContextAsync();

        var entity = await db.Set<User>().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(entity));

        if (dto.IsDeleted != null)
        {
            entity.IsDeleted = dto.IsDeleted.Value;
        }

        if (dto.IsActive != null)
        {
            entity.IsActive = dto.IsActive.Value;
        }

        entity.LastModificationTime = Clock.Now;

        await _userAccountRepository.UpdateAsync(entity, autoSave: true);

        await this.GetUserAuthValidationDataAsync(entity.Id, new CacheStrategy() { Refresh = true });
    }

    public virtual async Task<User> GetByIdentityNameAsync(IdentityNameDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto?.IdentityName))
            throw new ArgumentNullException(nameof(GetByIdentityNameAsync));

        var normalizedIdentityName = AccountUtils.NormalizeIdentityName(dto.IdentityName);

        var res = await _userAccountRepository.FirstOrDefaultAsync(x => x.IdentityName == normalizedIdentityName);

        return res;
    }

    public virtual async Task<bool> IsUserIdentityNameExistAsync(IdentityNameDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto?.IdentityName))
            throw new ArgumentNullException(nameof(GetByIdentityNameAsync));

        var normalizedIdentityName = AccountUtils.NormalizeIdentityName(dto.IdentityName);

        var db = await _userAccountRepository.GetDbContextAsync();

        var res = await db.Set<User>().IgnoreQueryFilters().AnyAsync(x => x.IdentityName == normalizedIdentityName);

        return res;
    }
}