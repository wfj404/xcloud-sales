import { CSSProperties, forwardRef, ReactNode } from 'react';

import { Close } from '@mui/icons-material';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Fab,
  Slide,
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { TransitionProps } from '@mui/material/transitions';
import useMediaQuery from '@mui/material/useMediaQuery';
import Zoom from '@mui/material/Zoom';

const Transition = forwardRef(
  (
    props: TransitionProps & {
      children: any;
    },
    ref: React.Ref<unknown>,
  ) => {
    return (
      <Slide direction="up" ref={ref} {...props}>
        {props.children}
      </Slide>
    );
  },
);

export default ({
  open,
  onClose,
  children,
  title,
  actionRender,
  bodyRender,
  showCloseButton,
  bodyStyle,
}: {
  open: boolean;
  onClose: () => void;
  children?: ReactNode;
  title?: ReactNode;
  actionRender?: () => ReactNode;
  bodyRender?: () => ReactNode;
  showCloseButton?: boolean;
  bodyStyle?: CSSProperties;
}) => {
  const theme = useTheme();
  const largeScreen = useMediaQuery(theme.breakpoints.up('sm'));

  const renderCloseButton = () => {
    if (!showCloseButton) {
      return null;
    }
    return (
      <>
        <div
          style={{
            position: 'relative',
          }}
        >
          <div
            style={{
              position: 'absolute',
              display: 'inline',
              right: '20px',
              top: '20px',
              zIndex: 1,
            }}
          >
            <Zoom in={open} style={{ transitionDelay: open ? '300ms' : '0ms' }}>
              <Fab
                sx={{}}
                size="small"
                color="error"
                onClick={() => {
                  onClose && onClose();
                }}
              >
                <Close />
              </Fab>
            </Zoom>
          </div>
        </div>
      </>
    );
  };

  const renderBody = () => {
    if (bodyRender) {
      return bodyRender();
    }
    return (
      <DialogContent>
        <div style={{ minHeight: 300 }}>{children}</div>
      </DialogContent>
    );
  };

  const renderActionAreas = () => {
    if (!actionRender) {
      return null;
    }
    return <DialogActions>{actionRender()}</DialogActions>;
  };

  return (
    <>
      <Dialog
        open={open}
        onClose={() => {
          onClose();
        }}
        TransitionComponent={Transition}
        sx={{
          m: 0,
          p: 0,
        }}
        disableEscapeKeyDown={true}
        keepMounted={false}
        scroll="body"
        maxWidth={largeScreen ? 'sm' : undefined}
        fullWidth={largeScreen}
        fullScreen={!largeScreen}
      >
        <div
          style={{
            minHeight: '100%',
            ...bodyStyle,
          }}
        >
          {renderCloseButton()}
          {title != undefined && <DialogTitle>{title}</DialogTitle>}
          {renderBody()}
          {renderActionAreas()}
        </div>
      </Dialog>
    </>
  );
};
