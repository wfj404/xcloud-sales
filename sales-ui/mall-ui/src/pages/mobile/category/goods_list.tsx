import { useInfiniteScroll } from 'ahooks';
import { useEffect, useState } from 'react';

import ScrollLoading from '@/components/common/scroll/loading';
import XItem from '@/components/goods/card/item';
import XDetailPreview from '@/components/goods/detail/detailPreview';
import { PagedResponse } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Zoom } from '@mui/material';
import Box from '@mui/material/Box';

export default function (props: { categoryId?: string | null }) {
  const { categoryId } = props;
  const [detailId, _detailId] = useState<string | null | undefined>(undefined);

  const { loading, data, noMore, loadingMore, loadMore, reload, mutate } =
    useInfiniteScroll<{
      list: GoodsDto[];
      res?: PagedResponse<GoodsDto>;
    }>(
      async (current) => {
        if (isStringEmpty(categoryId)) {
          return {
            list: [],
          };
        }

        let res = await apiClient.mall.searchSearchGoodsList({
          CategoryId: categoryId,
          Page: (current?.res?.PageIndex || 0) + 1,
        });

        return {
          list: res.data.Items || [],
          res: res.data,
        };
      },
      {
        reloadDeps: [categoryId],
        target: window.document,
        isNoMore: (x) => x?.res?.IsEmpty || false,
      },
    );

  const items = data?.list || [];

  useEffect(() => {
    mutate({
      list: [],
    });
  }, [categoryId]);

  return (
    <>
      <XDetailPreview
        detailId={detailId}
        onClose={() => {
          _detailId(undefined);
        }}
      />
      <Box sx={{}}>
        {items?.map((x, index) => {
          return (
            <Box
              key={index}
              sx={{
                mb: 2,
              }}
              onClick={() => {
                _detailId(x.Id);
              }}
            >
              <Zoom in style={{}}>
                <div>
                  <XItem model={x} count={2} lazy={true} />
                </div>
              </Zoom>
            </Box>
          );
        })}
        <ScrollLoading loading={loading || loadingMore} hasMore={!noMore} />
      </Box>
    </>
  );
}
