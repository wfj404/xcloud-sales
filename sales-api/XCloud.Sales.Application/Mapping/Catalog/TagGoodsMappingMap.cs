﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class TagGoodsMappingMap : SalesEntityTypeConfiguration<TagGoodsMapping>
{
    public override void Configure(EntityTypeBuilder<TagGoodsMapping> builder)
    {
        builder.ToTable("sales_tag_goods_mapping");

        builder.PreConfigIntPrimaryKey();
        
        builder.Property(x => x.TagId).IsRequired().HasMaxLength(100);
        builder.Property(x => x.GoodsId).RequiredId();

        base.Configure(builder);
    }
}