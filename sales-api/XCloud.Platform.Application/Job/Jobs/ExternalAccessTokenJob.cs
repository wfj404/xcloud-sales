﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Application.DistributedLock;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Job.Jobs;

[UnitOfWork]
[ExposeServices(typeof(ExternalAccessTokenJob))]
public class ExternalAccessTokenJob : PlatformAppService
{
    private readonly IExternalAccessTokenService _userExternalAccessTokenService;

    public ExternalAccessTokenJob(IExternalAccessTokenService userExternalAccessTokenService)
    {
        _userExternalAccessTokenService = userExternalAccessTokenService;
    }

    public virtual async Task CleanExpiredAccessTokenAsync()
    {
        await using var _ = await DistributedLockProvider.CreateRequiredLockAsync(
            resource: $"{nameof(ExternalAccessTokenJob)}.{nameof(CleanExpiredAccessTokenAsync)}",
            TimeSpan.FromSeconds(5));

        await _userExternalAccessTokenService.CleanExpiredTokenAsync();
    }
}