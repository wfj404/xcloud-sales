using System.Linq.Expressions;
using Volo.Abp.Application.Dtos;
using XCloud.Application.Model;
using XCloud.Application.Service;
using XCloud.Sales.Application.Domain;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service;

public interface ISalesStringCrudAppService<TEntity, TEntityDto, in TPagingRequest> :
    IXCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, string>
    where TEntity : SalesBaseEntity<string>
    where TEntityDto : class, IEntityDto<string>
    where TPagingRequest : PagedRequest
{
    //
}

public abstract class SalesStringCrudAppService<TEntity, TEntityDto, TPagingRequest> :
    XCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, string>,
    ISalesStringCrudAppService<TEntity, TEntityDto, TPagingRequest>
    where TEntity : SalesBaseEntity<string>
    where TEntityDto : class, IEntityDto<string>
    where TPagingRequest : PagedRequest
{
    protected SalesStringCrudAppService(ISalesRepository<TEntity> repository) : base(repository)
    {
        //
    }

    protected override Expression<Func<TEntity, bool>> BuildPrimaryKeyEqualExpression(string id)
    {
        return x => x.Id == id;
    }

    protected override bool IsEmptyKey(string id)
    {
        return string.IsNullOrWhiteSpace(id);
    }

    protected override void TrySetPrimaryKey(TEntity entity)
    {
        entity.Id = GuidGenerator.CreateGuidString();
    }

    protected override async Task<IOrderedQueryable<TEntity>> GetPagingOrderedQueryableAsync(IQueryable<TEntity> query,
        TPagingRequest dto)
    {
        await Task.CompletedTask;
        return query.OrderByDescending(x => x.Id);
    }
}

//--------------------------------------------------

public interface ISalesIntCrudAppService<TEntity, TEntityDto, in TPagingRequest> :
    IXCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, int>
    where TEntity : SalesBaseEntity
    where TEntityDto : class, IEntityDto<int>
    where TPagingRequest : PagedRequest
{
    //
}

public abstract class SalesIntCrudAppService<TEntity, TEntityDto, TPagingRequest> :
    XCloudCrudAppService<TEntity, TEntityDto, TPagingRequest, int>,
    ISalesIntCrudAppService<TEntity, TEntityDto, TPagingRequest>
    where TEntity : SalesBaseEntity
    where TEntityDto : class, IEntityDto<int>
    where TPagingRequest : PagedRequest
{
    protected SalesIntCrudAppService(ISalesRepository<TEntity> repository) : base(repository)
    {
        //
    }

    protected override Expression<Func<TEntity, bool>> BuildPrimaryKeyEqualExpression(int id)
    {
        return x => x.Id == id;
    }

    protected override bool IsEmptyKey(int id)
    {
        return id <= 0;
    }

    protected override async Task<IOrderedQueryable<TEntity>> GetPagingOrderedQueryableAsync(IQueryable<TEntity> query,
        TPagingRequest dto)
    {
        await Task.CompletedTask;
        return query.OrderByDescending(x => x.Id);
    }
}