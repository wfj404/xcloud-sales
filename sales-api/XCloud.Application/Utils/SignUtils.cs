﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Extension;
using XCloud.Core.Helper;
using XCloud.Core.Security.Hash;

namespace XCloud.Application.Utils;

public class SignException : BusinessException
{
    public SignException(string message) : base(message: message)
    {
        //
    }
}

[ExposeServices(typeof(SignUtils))]
public class SignUtils : ITransientDependency
{
    public void ValidateTimestamp(IDictionary<string, string> dict, string key, int maxDeviationSeconds)
    {
        if (dict == null)
            throw new ArgumentNullException(nameof(dict));

        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        if (!dict.TryGetValue(key, out var timestamp) ||
            !int.TryParse(timestamp, out var clientTimestamp))
        {
            throw new SignException("缺少时间戳");
        }

        var serverTimestamp = Com.GetTimestampInSecond();
        //取绝对值
        if (Math.Abs(serverTimestamp - clientTimestamp) > maxDeviationSeconds)
        {
            throw new SignException("请求时间戳已经过期")
                .WithData(nameof(serverTimestamp), serverTimestamp)
                .WithData(nameof(clientTimestamp), clientTimestamp);
        }
    }

    public void ValidateSign(IDictionary<string, string> dict, string key, string salt = null)
    {
        if (!dict.TryGetValue(key, out var clientSign) || string.IsNullOrWhiteSpace(clientSign))
        {
            throw new SignException("请求被拦截，获取不到签名")
                .WithData(nameof(key), key);
        }

        salt ??= "default-salt";

        var stringToCompute = this.GetStringToCompute(dict, new[] { key });

        stringToCompute = $"{stringToCompute}@{salt}";

        var serverSign = this.Md5Hash(stringToCompute);

        if (!string.Equals(clientSign, serverSign, StringComparison.OrdinalIgnoreCase))
        {
            throw new SignException("签名错误")
                .WithData(nameof(stringToCompute), stringToCompute)
                .WithData(nameof(clientSign), clientSign)
                .WithData(nameof(serverSign), serverSign);
        }
    }

    public string GetStringToCompute(IDictionary<string, string> dict, string[] ignoredKeys = default)
    {
        if (dict == null)
            throw new ArgumentNullException(nameof(dict));

        ignoredKeys ??= Array.Empty<string>();

        dict = dict
            .Where(x => !ignoredKeys.Contains(x.Key))
            .Where(x => !string.IsNullOrWhiteSpace(x.Key) && !string.IsNullOrWhiteSpace(x.Value))
            .ToDictionaryUpdateDuplicateKey(x => x.Key, x => x.Value);

        var sortedDict = new SortedDictionary<string, string>(dict, StringComparer.Ordinal);

        var queryString = sortedDict.ToUrlQueryString();

        return queryString;
    }

    public string Md5Hash(string strToCompute)
    {
        if (string.IsNullOrWhiteSpace(strToCompute))
            throw new ArgumentNullException(nameof(strToCompute));

        var hash = Md5Helper.Encrypt(strToCompute, encoding: Encoding.UTF8);

        return hash;
    }
}