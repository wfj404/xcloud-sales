﻿using JetBrains.Annotations;
using XCloud.Application.DistributedLock;
using XCloud.Application.Mapper;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreManagerAccountService : ISalesAppService
{
    Task UpdateStatusAsync(UpdateStoreManagerStatusInput dto);

    Task<StoreManagerDto> GetStoreManagerAuthValidationDataAsync(string storeId, string userId,
        CacheStrategy cacheStrategyOption);

    [NotNull]
    Task<StoreManagerDto> GetOrCreateByUserIdAsync(string storeId, string userId);
}

public class StoreManagerAccountService : SalesAppService, IStoreManagerAccountService
{
    private readonly ISalesRepository<StoreManager> _storeManagerRepository;

    public StoreManagerAccountService(ISalesRepository<StoreManager> storeManagerRepository)
    {
        _storeManagerRepository = storeManagerRepository;
    }

    public async Task<StoreManagerDto> GetOrCreateByUserIdAsync(string storeId, string userId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var db = await this._storeManagerRepository.GetDbContextAsync();

        var manager = await db.Set<StoreManager>().IgnoreQueryFilters().AsNoTracking()
            .OrderBy(x => x.CreationTime)
            .FirstOrDefaultAsync(x => x.StoreId == storeId && x.UserId == userId);

        if (manager == null)
        {
            await using var _ = await this.DistributedLockProvider.CreateRequiredLockAsync(
                resource: $"get.or.create.store.manager.{storeId}.{userId}",
                expiryTime: TimeSpan.FromSeconds(5));

            manager = new StoreManager()
            {
                Id = this.GuidGenerator.CreateGuidString(),
                StoreId = storeId,
                UserId = userId,
                Role = StoreManagerRoles.StoreManager,
                IsActive = true,
                IsSuperManager = false,
                CreationTime = this.Clock.Now
            };

            manager = await _storeManagerRepository.InsertAsync(manager, autoSave: true);
        }

        return manager.MapTo<StoreManager, StoreManagerDto>(ObjectMapper);
    }

    public async Task<StoreManagerDto> GetStoreManagerAuthValidationDataAsync(string storeId, string userId,
        CacheStrategy cacheStrategyOption)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var key = $"sales.store-manager.by.store-global.user:{storeId},{userId}";

        var storeManager = await CacheProvider.ExecuteWithPolicyAsync(
            async () =>
            {
                var db = await _storeManagerRepository.GetDbContextAsync();

                var joinedQuery =
                    from manager in db.Set<StoreManager>().AsNoTracking()
                    join store in db.Set<Store>().AsNoTracking()
                        on manager.StoreId equals store.Id
                    select new
                    {
                        manager,
                        store
                    };

                joinedQuery = joinedQuery.Where(x => x.store.Id == storeId);
                joinedQuery = joinedQuery.Where(x => x.manager.UserId == userId);

                var data = await joinedQuery
                    .OrderBy(x => x.manager.CreationTime)
                    .FirstOrDefaultAsync();

                if (data == null)
                    return null;

                var storeManager = data.manager.MapTo<StoreManager, StoreManagerDto>(ObjectMapper);

                storeManager.Store = data.store.MapTo<Store, StoreDto>(ObjectMapper);

                return storeManager;
            },
            new CacheOption<StoreManagerDto>(key, TimeSpan.FromMinutes(1)) { CacheCondition = x => x != null },
            cacheStrategyOption);

        return storeManager;
    }

    public async Task UpdateStatusAsync(UpdateStoreManagerStatusInput dto)
    {
        var db = await _storeManagerRepository.GetDbContextAsync();

        var manager = await db.Set<StoreManager>().FirstOrDefaultAsync(x => x.Id == dto.ManagerId);
        if (manager == null)
            throw new EntityNotFoundException(nameof(UpdateStatusAsync));

        if (dto.IsActive != null)
        {
            manager.IsActive = dto.IsActive.Value;
        }

        if (dto.IsSuperManager != null)
        {
            manager.IsSuperManager = dto.IsSuperManager.Value;
        }

        await this._storeManagerRepository.UpdateAsync(manager, autoSave: true);

        await this.GetStoreManagerAuthValidationDataAsync(manager.StoreId, manager.UserId,
            new CacheStrategy() { Refresh = true });
    }
}