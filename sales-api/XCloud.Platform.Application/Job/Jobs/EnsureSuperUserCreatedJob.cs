using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Job.Jobs;

[UnitOfWork]
[ExposeServices(typeof(EnsureSuperUserCreatedJob))]
public class EnsureSuperUserCreatedJob : PlatformAppService
{
    private readonly EnsureSuperUserCreatedService _ensureSuperUserCreatedService;

    public EnsureSuperUserCreatedJob(EnsureSuperUserCreatedService ensureSuperUserCreatedService)
    {
        _ensureSuperUserCreatedService = ensureSuperUserCreatedService;
    }

    public async Task EnsureSuperUserCreatedAsync()
    {
        using var uow = UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);
        try
        {
            await _ensureSuperUserCreatedService.EnsureSuperUserCreatedAsync();

            await uow.CompleteAsync();
        }
        catch (UserFriendlyException e)
        {
            await uow.RollbackAsync();
            Logger.LogWarning(message: e.Message, exception: e);
        }
        catch (Exception e)
        {
            await uow.RollbackAsync();
            Logger.LogError(message: e.Message, exception: e);
        }
    }
}