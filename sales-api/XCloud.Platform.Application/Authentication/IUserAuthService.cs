﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using XCloud.Application.Service;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Extension;
using XCloud.Platform.Application.Service;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.Application.Authentication;

public interface IUserAuthService : IXCloudAppService
{
    [NotNull]
    [ItemNotNull]
    Task<UserAuthResult> GetAuthUserAsync();
}

public class UserAuthService : PlatformAppService, IUserAuthService
{
    private readonly IUserAccountService _userAccountService;
    private readonly IRequestCacheProvider _cacheProvider;
    private readonly IHttpPrincipalService _httpPrincipalService;

    public UserAuthService(IRequestCacheProvider cacheProvider,
        IHttpPrincipalService httpPrincipalService, IUserAccountService userAccountService)
    {
        _cacheProvider = cacheProvider;
        _httpPrincipalService = httpPrincipalService;
        _userAccountService = userAccountService;
    }

    private async Task<UserAuthResult> GetAuthUserImplAsync()
    {
        var res = new UserAuthResult();

        var info = await this._httpPrincipalService.GetPrincipalInfoAsync();

        if (info.HasError())
        {
            res.SetError(info.Error.Message);
            return res;
        }

        if (info.TokenCreationTime == null)
        {
            res.SetError(nameof(info.TokenCreationTime));
            return res;
        }

        if (string.IsNullOrWhiteSpace(info.SubjectId))
        {
            res.SetError(nameof(info.SubjectId));
            return res;
        }

        var userWithErrors =
            await this._userAccountService.GetUserAuthValidationDataAsync(info.SubjectId,
                new CacheStrategy() { Cache = true });

        if (!userWithErrors.HasNoError())
        {
            var errors = userWithErrors.ErrorList.Select(x => $"{x.Code}:{x.Message}").ToArray();
            res.SetError(string.Join(',', errors));
            return res;
        }

        var user = userWithErrors.Data;

        if (user == null)
        {
            res.SetError("user not exist");
            return res;
        }

        //check token if is expired
        if (user.LastPasswordUpdateTime != null &&
            user.LastPasswordUpdateTime.Value > info.TokenCreationTime.Value)
        {
            res.TokenIsExpired = true;
            return res.SetError("your login is expired");
        }

        user.HideSensitiveInformation();

        res.SetData(user);

        return res;
    }

    public async Task<UserAuthResult> GetAuthUserAsync()
    {
        return await this._cacheProvider.GetOrSetAsync(async () => await this.GetAuthUserImplAsync());
    }
}