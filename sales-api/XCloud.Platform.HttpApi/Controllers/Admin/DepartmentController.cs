﻿using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Domain.Dept;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Platform.Application.Service.Dept;
using XCloud.Platform.Application.Service.Roles;

namespace XCloud.Platform.HttpApi.Controllers.Admin;

[Route("/api/sys/department")]
public class DepartmentController : PlatformBaseController
{
    private readonly IDepartmentService _departmentService;
    private readonly IDepartmentMappingService _departmentMappingService;
    private readonly IAdminService _adminService;
    private readonly IAdminRoleService _adminRoleService;
    private readonly IRoleService _roleService;

    public DepartmentController(IDepartmentService departmentService,
        IDepartmentMappingService departmentMappingService,
        IAdminService adminService, IAdminRoleService adminRoleService, IRoleService roleService)
    {
        _departmentService = departmentService;
        _departmentMappingService = departmentMappingService;
        _adminService = adminService;
        _adminRoleService = adminRoleService;
        _roleService = roleService;
    }

    [HttpPost("tree")]
    public async Task<ApiResponse<IList<AntDesignTreeNode<DepartmentDto>>>> DepartmentTreeAsync()
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var datalist = await this._departmentService.ListAllAsync();

        await this._departmentMappingService.AttachMemberCountAsync(datalist);

        var tree = datalist
            .BuildAntTreeV2(
                idSelector: x => x.Id,
                parentIdSelector: x => x.ParentId,
                titleSelector: x => x.Name ?? "--").ToArray();

        return new ApiResponse<IList<AntDesignTreeNode<DepartmentDto>>>(tree);
    }

    [HttpPost("get-children-by-parent-id")]
    public async Task<ApiResponse<DepartmentDto[]>> GetChildrenByParentIdAsync([FromBody] IdDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var children = await this._departmentService.QueryByParentIdAsync(dto.Id);

        return new ApiResponse<DepartmentDto[]>(children);
    }

    [HttpPost("save")]
    public async Task<ApiResponse<Department>> SaveAsync([FromBody] DepartmentDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        if (dto.IsEmptyId())
        {
            var entity = await this._departmentService.InsertAsync(dto);

            return new ApiResponse<Department>(entity);
        }
        else
        {
            var entity = await this._departmentService.UpdateAsync(dto);

            return new ApiResponse<Department>(entity);
        }
    }

    [HttpPost("delete-by-id")]
    public async Task<ApiResponse<object>> DeleteByIdAsync([FromBody] IdDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._departmentService.DeleteByIdAsync(dto.Id);

        return new ApiResponse<object>();
    }

    [NonAction]
    private async Task<AdminDto[]> AttachDetailsAsync(AdminDto[] data)
    {
        if (data.Any())
        {
            await this._departmentMappingService.AttachDepartmentsAsync(data);
            await this._adminService.AttachUsersAsync(data);
            await this._adminRoleService.AttachRolesAsync(data);
            var roles = data.SelectMany(x => x.Roles).ToArray();
            await this._roleService.AttachPermissionAsync(roles);
        }

        return data;
    }

    [HttpPost("list-department-admins")]
    public async Task<ApiResponse<AdminDto[]>> ListDepartmentAdminsAsync([FromBody] IdDto dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var datalist = await this._departmentMappingService.GetDepartmentAdminsAsync(dto.Id);

        datalist = await this.AttachDetailsAsync(datalist);

        return new ApiResponse<AdminDto[]>(datalist);
    }

    [HttpPost("dangling-admin")]
    public async Task<ApiResponse<AdminDto[]>> DanglingAdminAsync()
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        var datalist = await this._departmentMappingService.GetDanglingAdminsAsync();

        datalist = await this.AttachDetailsAsync(datalist);

        return new ApiResponse<AdminDto[]>(datalist);
    }

    [HttpPost("set-admin-departments")]
    public async Task<ApiResponse<object>> SetAdminDepartmentsAsync([FromBody] KeyValuePair<string, string[]> dto)
    {
        var admin = await this.AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._departmentMappingService.SetAdminDepartmentsAsync(dto.Key, dto.Value);

        return new ApiResponse<object>();
    }
}