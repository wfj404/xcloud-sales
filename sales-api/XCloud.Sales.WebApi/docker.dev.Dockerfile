﻿#编译结果copy到runtime容器运行
FROM mcr.microsoft.com/dotnet/aspnet:8.0

COPY ./XCloud.Sales.WebApi/bin/out /app
WORKDIR /app

ENTRYPOINT ["dotnet","XCloud.Sales.WebApi.dll"]