import React, { ReactNode } from 'react';

import { Paper } from '@mui/material';

export default function ({ children }: { children: ReactNode }) {
  return (
    <>
      <Paper
        sx={{
          overflow: 'hidden',
          borderRadius: '12px',
          cursor: 'pointer',
          border: (theme) => `1px dashed ${theme.palette.primary.main}`,
          borderColor: 'transparent',
          '&:hover': {
            borderColor: (t) => t.palette.primary.main,
          },
        }}
        //className="hover:scale-105 hover:shadow-sm"
        elevation={0}
      >
        {children}
      </Paper>
    </>
  );
}
