import { useMemo } from 'react';

import emptyImage from '@/assets/empty.svg';
import XImage from '@/components/common/image';
import { getGoodsMainPictureUrl } from '@/utils/biz';
import { GoodsDto } from '@/utils/swagger';
import { isStringEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

export default (props: { model: GoodsDto; lazy?: boolean }) => {
  const { model, lazy } = props;

  if (isStringEmpty(model?.Id)) {
    return null;
  }

  const url =
    useMemo(() => {
      return getGoodsMainPictureUrl(model, {
        width: 250,
        height: 250,
      });
    }, [model.GoodsPictures]) || emptyImage;

  return (
    <>
      <Box sx={{}}>
        <XImage
          rate={4 / 3}
          lazy={lazy}
          src={url}
          fit={'cover'}
          width={'100%'}
          placeholder={<img src={emptyImage} width="100%" alt="" />}
          style={{
            width: '100%',
          }}
          alt={''}
        />
      </Box>
    </>
  );
};
