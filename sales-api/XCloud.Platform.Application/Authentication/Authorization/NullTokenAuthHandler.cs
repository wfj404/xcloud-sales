using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;

namespace XCloud.Platform.Application.Authentication.Authorization;

public class NullTokenAuthHandlerOption : AuthenticationSchemeOptions
{
    public string Key { get; set; }
}

/// <summary>
/// signin-handler->sign out-handler->auth-handler
/// </summary>
public class NullTokenAuthHandler : SignInAuthenticationHandler<NullTokenAuthHandlerOption>,
    IAuthenticationHandler,
    IAuthenticationSignInHandler,
    IAuthenticationSignOutHandler
{
    public NullTokenAuthHandler(IOptionsMonitor<NullTokenAuthHandlerOption> options, UrlEncoder encoder)
        : base(options, NullLoggerFactory.Instance, encoder)
    {
        //
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        await Task.CompletedTask;

        return AuthenticateResult.Fail(nameof(NullTokenAuthHandler));
    }

    protected override Task HandleSignInAsync(ClaimsPrincipal user, AuthenticationProperties properties)
    {
        return Task.CompletedTask;
    }

    protected override Task HandleSignOutAsync(AuthenticationProperties properties)
    {
        return Task.CompletedTask;
    }
}