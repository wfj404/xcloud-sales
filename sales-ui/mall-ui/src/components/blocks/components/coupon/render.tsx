import { useRequest } from 'ahooks';
import { take } from 'lodash-es';
import { useEffect } from 'react';

import XLoading from '@/components/common/loading/dots';
import { apiClient } from '@/utils/client';
import { Box } from '@mui/material';

import XTitle from '../title/render';
import XItem from './item';
import { CouponBlockProps } from './utils';

export default ({ data }: { data: CouponBlockProps }) => {
  if (!data) {
    return null;
  }

  const count = data.count || 10;

  const getCouponRequest = useRequest(apiClient.mall.couponQueryCouponPaging, {
    manual: true,
  });

  const coupon_list = take(getCouponRequest.data?.data?.Items || [], count);

  useEffect(() => {
    getCouponRequest.run({
      Page: 1,
      PageSize: count,
      SkipCalculateTotalCount: true,
    });
  }, []);

  const renderCouponList = () => {
    if (coupon_list.length <= 0) {
      return <p>暂无优惠券可以领取</p>;
    }

    return coupon_list.map((x, i) => {
      return (
        <Box key={i} sx={{ mb: 1 }}>
          <XItem
            data={x}
            onUpdate={() => {
              getCouponRequest.run({
                Page: 1,
                PageSize: count,
                SkipCalculateTotalCount: true,
              });
            }}
          />
        </Box>
      );
    });
  };

  if (getCouponRequest.loading) {
    return <XLoading />;
  }

  if (coupon_list.length <= 0 && data.hide_if_empty) {
    return null;
  }

  return (
    <>
      <XTitle
        data={{
          title: data.title,
        }}
      />
      <Box sx={{ px: 1 }}>{renderCouponList()}</Box>
    </>
  );
};
