import { useRequest } from 'ahooks';
import { Card, Col, Row, Skeleton, Statistic, Tooltip } from 'antd';
import { ReactNode, useEffect } from 'react';
import { history } from 'umi';

import { apiClient } from '@/utils/client';
import { isStringEmpty } from '@/utils/utils';
import { CreditCardOutlined, UserOutlined } from '@ant-design/icons';

const InfoCard = (item: {
  title?: string;
  tip?: string;
  count?: number | null;
  action?: () => void;
  pathname?: string;
  precision?: number;
  prefix?: ReactNode;
  suffix?: ReactNode;
  color?: string;
}) => {
  let content = (
    <Statistic
      title={item.title || '--'}
      value={item.count || 0}
      precision={item.precision}
      valueStyle={{ color: item.color || '#3f8600' }}
      prefix={item.prefix}
      suffix={item.suffix}
    />
  );

  if (!isStringEmpty(item.tip)) {
    content = <Tooltip title={item.tip}>{content}</Tooltip>;
  }

  return (
    <Card
      style={{
        cursor: 'pointer',
      }}
      bordered={false}
      extra={undefined}
      onClick={() => {
        item.action && item.action();
        item.pathname &&
          history.push({
            pathname: item.pathname,
          });
      }}
    >
      {content}
    </Card>
  );
};

export default () => {
  const request = useRequest(apiClient.mallAdmin.reportDashBoardCounter, {
    manual: true,
  });

  const data = request.data?.data?.Data || {};

  useEffect(() => {
    request.run({});
  }, []);

  if (request.loading) {
    return <Skeleton active />;
  }

  return (
    <>
      <div style={{}}>
        <Row gutter={10} style={{ marginBottom: 10 }}>
          <Col span={4}>
            {InfoCard({
              title: '今日上线人数',
              count: data.ActiveUser,
              prefix: <UserOutlined />,
            })}
          </Col>
          <Col span={4}></Col>
          <Col span={4}></Col>
          <Col span={4}></Col>
          <Col span={4}></Col>
          <Col span={4}></Col>
        </Row>
        <Row gutter={10} style={{ marginBottom: 10 }}>
          <Col span={4}>
            {InfoCard({
              title: '商品数量',
              count: data.Goods,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '品牌数量',
              count: data.Brand,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '类目数量',
              count: data.Category,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '优惠券数量',
              count: data.Coupon,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '店铺活动',
              count: data.Promotion,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '处理中订单',
              count: data.Orders,
            })}
          </Col>
        </Row>
        <Row gutter={10} style={{ marginBottom: 10 }}>
          <Col span={4}>
            {InfoCard({
              title: '售后中',
              count: data.AfterSale,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '充值卡余额',
              tip: '所有用户账户余额汇总',
              count: data.Balance,
            })}
          </Col>
          <Col span={4}>
            {InfoCard({
              title: '未使用预售卡',
              tip: '所有未使用预售卡金额汇总',
              count: data.PrepaidCard,
              prefix: <CreditCardOutlined />,
            })}
          </Col>
          <Col span={4}></Col>
          <Col span={4}></Col>
          <Col span={4}></Col>
        </Row>
      </div>
    </>
  );
};
