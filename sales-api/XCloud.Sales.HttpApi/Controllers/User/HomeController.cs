using XCloud.Core.Cache;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Dto;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/home")]
public class HomeController : ShopBaseController
{
    public HomeController()
    {
        //
    }

    [HttpPost("home-page-blocks")]
    public async Task<ApiResponse<HomePageBlocksDto>> HomePageBlocksAsync()
    {
        var data = await this.SettingService.GetSettingsAsync<HomePageBlocksDto>(new CacheStrategy() { Cache = true });

        data ??= new HomePageBlocksDto();

        return new ApiResponse<HomePageBlocksDto>(data);
    }
}