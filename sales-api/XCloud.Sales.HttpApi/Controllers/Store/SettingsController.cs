using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/settings")]
public class SettingsController : ShopBaseController
{
    private readonly IStoreSettingService _storeSettingService;

    public SettingsController(IStoreSettingService storeSettingService)
    {
        _storeSettingService = storeSettingService;
    }
}