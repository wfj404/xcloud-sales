﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Job.Jobs;

[UnitOfWork]
[ExposeServices(typeof(ServiceOrderJobs))]
public class ServiceOrderJobs : SalesAppService
{
    private readonly IQingdaoOrderToGenericAdaptorService _qingdaoOrderToGenericAdaptorService;

    public ServiceOrderJobs(IQingdaoOrderToGenericAdaptorService qingdaoOrderToGenericAdaptorService)
    {
        _qingdaoOrderToGenericAdaptorService = qingdaoOrderToGenericAdaptorService;
    }

    public async Task RefreshOrderStatusAsync()
    {
        await this._qingdaoOrderToGenericAdaptorService.RefreshOrderStatusAsync();
    }
}