﻿using System.IO;
using System.Threading.Tasks;
using XCloud.Core.Extension;
using XCloud.Sales.WeChat.Service.Payments;

namespace XCloud.Sales.Test;

[TestClass]
public class WechatTest
{
    [TestMethod]
    public async Task ParseWechatBillsAsync()
    {
        var excelPath = Path.Combine(Directory.GetCurrentDirectory(), "ALL.xlsx");

        File.Exists(excelPath).Should().BeTrue();

        var datalist = await WechatBillParserHelper.FromExcelAsync(excelPath);
    }
}