﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Qc.YilianyunSdk;
using Volo.Abp.DependencyInjection;
using XCloud.Core.Extension;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.ThirdParty.Services.YiLianYun;

[ExposeServices(typeof(YiLianServiceFactory))]
public class YiLianServiceFactory : IScopedDependency
{
    private readonly IStoreSettingService _storeSettingService;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly IWebHostEnvironment _webHostEnvironment;

    public YiLianServiceFactory(IStoreSettingService storeSettingService, IHttpClientFactory httpClientFactory,
        IWebHostEnvironment webHostEnvironment)
    {
        _storeSettingService = storeSettingService;
        _httpClientFactory = httpClientFactory;
        _webHostEnvironment = webHostEnvironment;
    }

    public async Task<YilianyunService> CreateYiLianYunServiceAsync([NotNull] string storeId)
    {
        var settings = await this._storeSettingService.GetSettingsAsync<YiLianYunConfigDto>(storeId);

        return await this.CreateYiLianYunServiceAsync(settings);
    }

    public async Task<YilianyunService> CreateYiLianYunServiceAsync([NotNull] YiLianYunConfigDto settings)
    {
        await Task.CompletedTask;

        var path = Path.Combine(this._webHostEnvironment.ContentRootPath, "data", "yi-lian-yun", settings.ClientId);
        new DirectoryInfo(path).CreateIfNotExist();

        var config = new YilianyunConfig()
        {
            ClientId = settings.ClientId,
            ClientSecret = settings.ClientSecret,
            YilianyunClientType = YilianyunClientType.自有应用,
            SaveTokenDirPath = path
        };

        var options = new OptionsWrapper<YilianyunConfig>(config);

        return new YilianyunService(
            _httpClientFactory: this._httpClientFactory,
            configOptions: options,
            yilianyunSdkHook: new DefaultYilianyunSdkHook(options));
    }
}