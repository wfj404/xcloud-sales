using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Volo.Abp;
using XCloud.AspNetMvc.Configuration;
using XCloud.Sales.WebApi;
using XCloud.Sales.Test.Service;

namespace XCloud.Sales.Test;

[TestClass]
public abstract class TestBase
{
    private string GetRequiredRootPath()
    {
        var dir =
            new DirectoryInfo(Directory.GetCurrentDirectory()).Parent?.Parent?.Parent?.Parent?.FullName;

        if (string.IsNullOrWhiteSpace(dir))
            throw new FileNotFoundException(nameof(GetRequiredRootPath));

        var path = Path.Combine(dir, typeof(SalesWebApiModule).Assembly.GetName().Name ?? string.Empty);

        if (!Directory.Exists(path))
            throw new AbpException(nameof(GetRequiredRootPath));

        return path;
    }

    protected WebApplication WebApplication;

    [TestInitialize]
    public void TestInit()
    {
        var path = GetRequiredRootPath();

        var builder = WebApplication.CreateBuilder(new WebApplicationOptions
        {
            ContentRootPath = path,
            WebRootPath = Path.Combine(path, "wwwroot")
        });

        builder.Host.UseAutofac();
        builder.WebHost.UseTestServer();

        builder.ConfigureConfigBuilder((configBuilder, sources) =>
        {
            configBuilder.AddJsonFile(
                Path.Combine(path, "appsettings.json"),
                optional: false);
        });

        builder.Services.AddApplication<PlatformTestModule>();

        var server = builder.Build();

        server.InitializeApplication();

        server.Start();

        WebApplication = server;
    }

    [TestCleanup]
    public async Task TestCleanup()
    {
        if (WebApplication != null)
        {
            await WebApplication.StopAsync();
            await using (WebApplication)
            {
                //
            }
        }
    }

    //[TestMethod]
    public void TestMethod1()
    {
        var builder = WebHost.CreateDefaultBuilder();

        builder.UseTestServer();

        builder.ConfigureAppConfiguration((context, configurationBuilder) =>
        {
            //
        });

        builder.ConfigureServices(services => { services.AddSingleton<TestUserAccountService>(); });

        builder.Configure(app =>
        {
            //
        });

        using var server = builder.Build();

        server.Start();

        using var s = server.Services.CreateScope();

        s.ServiceProvider.GetRequiredService<TestUserAccountService>();
    }
}