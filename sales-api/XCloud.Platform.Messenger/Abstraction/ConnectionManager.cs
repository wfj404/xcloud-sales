﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace XCloud.Platform.Messenger.Abstraction;

public class ConnectionManager
{
    private readonly ConcurrentDictionary<string, IConnection> _connections =
        new ConcurrentDictionary<string, IConnection>();

    private string BuildKey(IConnection connection)
    {
        return $"{connection.ClientIdentity.ConnectionId}@{connection.ClientIdentity.SubjectId}";
    }

    public IReadOnlyList<IConnection> AsReadOnlyList()
    {
        return _connections.Values.ToArray();
    }

    public void AddConnection(IConnection con)
    {
        if (con == null)
            throw new ArgumentNullException(nameof(con));

        _connections[BuildKey(con)] = con;
    }

    public void RemoveConnection(IConnection con)
    {
        _connections.TryRemove(BuildKey(con), out var _);

        var items = _connections.Where(x => x.Value == con).ToArray();
        foreach (var m in items)
        {
            _connections.TryRemove(m.Key, out var _);
        }
    }
}