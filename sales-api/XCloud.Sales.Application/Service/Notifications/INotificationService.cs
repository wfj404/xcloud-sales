﻿using XCloud.Application.Model;
using XCloud.EntityFrameworkCore.Extensions;
using XCloud.Sales.Application.Domain.Notifications;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Notifications;

public interface
    INotificationService : ISalesStringCrudAppService<Notification, NotificationDto, QueryNotificationPagingInput>
{
    Task DeleteAllByUserIdAsync(int userId);

    Task UpdateStatusAsync(UpdateNotificationStatusInput dto);

    Task<int> UnreadCountAsync(int userId, CacheStrategy cacheStrategyOption);
}

public class NotificationService :
    SalesStringCrudAppService<Notification, NotificationDto, QueryNotificationPagingInput>,
    INotificationService
{
    private readonly ISalesRepository<Notification> _repository;

    public NotificationService(ISalesRepository<Notification> repository) : base(repository)
    {
        _repository = repository;
    }

    public async Task DeleteAllByUserIdAsync(int userId)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(userId));

        while (true)
        {
            using var uow = UnitOfWorkManager.Begin(requiresNew: true, isTransactional: true);
            try
            {
                var db = await Repository.GetDbContextAsync();

                var set = db.Set<Notification>();

                var list = await set
                    .Where(x => x.UserId == userId)
                    .OrderByDescending(x => x.CreationTime)
                    .Take(200)
                    .ToArrayAsync();

                if (!list.Any())
                    break;

                set.RemoveRange(list);
                await db.TrySaveChangesAsync();
                await uow.CompleteAsync();
            }
            catch (Exception e)
            {
                await uow.RollbackAsync();
                throw;
            }
        }
    }

    protected override async Task SetFieldsBeforeInsertAsync(Notification entity)
    {
        await Task.CompletedTask;

        entity.Id = GuidGenerator.CreateGuidString();
        entity.Read = false;
        entity.ReadTime = default;
        entity.CreationTime = Clock.Now;
        entity.LastModificationTime = entity.CreationTime;
    }

    public async Task UpdateStatusAsync(UpdateNotificationStatusInput dto)
    {
        var db = await _repository.GetDbContextAsync();
        var set = db.Set<Notification>();

        var entity = await set.FirstOrDefaultAsync(x => x.Id == dto.Id);

        if (entity == null)
            throw new EntityNotFoundException(nameof(UpdateStatusAsync));

        if (dto.Read != null)
        {
            if (!entity.Read && entity.ReadTime == null && dto.Read.Value)
                entity.ReadTime = Clock.Now;

            entity.Read = dto.Read.Value;
        }

        entity.LastModificationTime = Clock.Now;

        await db.TrySaveChangesAsync();
    }

    protected override async Task<IQueryable<Notification>> GetPagingFilteredQueryableAsync(DbContext db,
        QueryNotificationPagingInput dto)
    {
        await Task.CompletedTask;

        var query = db.Set<Notification>().AsNoTracking();

        query = query.Where(x => x.UserId == dto.UserId);

        if (!string.IsNullOrWhiteSpace(dto.AppKey))
            query = query.Where(x => x.App == dto.AppKey);

        return query;
    }

    public override Task<PagedResponse<NotificationDto>> QueryPagingAsync(QueryNotificationPagingInput dto)
    {
        if (dto.UserId <= 0)
            throw new ArgumentNullException(nameof(dto.UserId));

        return base.QueryPagingAsync(dto);
    }

    public async Task<int> UnreadCountAsync(int userId, CacheStrategy cacheStrategyOption)
    {
        var key = $"platform.user.{userId}.notification.count";
        var option = new CacheOption<int>(key, TimeSpan.FromMinutes(30));
        var count = await CacheProvider.ExecuteWithPolicyAsync(() => UnreadCountAsync(userId), option,
            cacheStrategyOption);
        return count;
    }

    public async Task<int> UnreadCountAsync(int userId)
    {
        if (userId <= 0)
            throw new ArgumentNullException(nameof(UnreadCountAsync));

        var db = await _repository.GetDbContextAsync();

        var query = from notice in db.Set<Notification>().AsNoTracking()
            select new { notice };

        var count = await query
            .Where(x => x.notice.UserId == userId)
            .Where(x => !x.notice.Read)
            .CountAsync();

        return count;
    }
}