import { useRequest } from 'ahooks';
import { Alert, Card, message, Select, Space, Spin, Tooltip } from 'antd';
import { useEffect } from 'react';

import { apiClient } from '@/utils/client';
import { SkuDto, SpecDto, SpecItemDto } from '@/utils/swagger';
import { isArrayEmpty } from '@/utils/utils';

const fingerPrint = (x: SpecItemDto) => `${x.SpecId}=${x.SpecValueId}`;

export default ({
  model,
  ok,
}: {
  model: SkuDto;
  ok: (e: SpecItemDto[]) => void;
}) => {
  const specItems = model.SpecItemList || [];

  const query_goods_specs_request = useRequest(
    apiClient.mallAdmin.specListJson,
    {
      manual: true,
    },
  );
  const all_specs: SpecDto[] = query_goods_specs_request.data?.data?.Data || [];

  const all_spec_items: SpecItemDto[] = all_specs
    .flatMap((x) => x.Values || [])
    .map<SpecItemDto>((x) => ({
      SpecId: x.SpecId,
      SpecValueId: x.Id,
    }));

  const querySpecs = () => {
    if (!model.GoodsId) {
      return;
    }
    query_goods_specs_request.run({
      Id: model.GoodsId,
    });
  };

  const setCombinationItems = (e: SpecItemDto[]) => {
    const allFingerPrints = all_spec_items.map((x) => fingerPrint(x));
    const validSpecItems = e.filter(
      (x) => allFingerPrints.indexOf(fingerPrint(x)) >= 0,
    );
    ok && ok(validSpecItems);
  };

  useEffect(() => {
    querySpecs();
  }, []);

  const renderForm = () => {
    if (query_goods_specs_request.loading) {
      return <Spin spinning />;
    }
    if (isArrayEmpty(all_specs)) {
      return <Alert message="请先配置规格"></Alert>;
    }
    return (
      <>
        <Space direction="vertical">
          {all_specs.map((x, i) => {
            const items = x.Values || [];

            const selectedValueId = specItems.find(
              (d) => d.SpecId == x.Id,
            )?.SpecValueId;

            const valueValid = items.some((d) => d.Id == selectedValueId);

            return (
              <div key={i} style={{ marginBottom: 10 }}>
                <Space direction="horizontal">
                  <b>{x.Name || '--'}</b>
                  <Tooltip title={valueValid ? undefined : '请选择有效选项'}>
                    <Select
                      placeholder="请选择"
                      options={items.map((d) => ({
                        label: d.Name,
                        value: d.Id,
                      }))}
                      allowClear
                      onClear={() => {
                        setCombinationItems([]);
                      }}
                      onChange={(e) => {
                        const otherSpecs = specItems.filter(
                          (d) => d.SpecId != x.Id,
                        );
                        if (e <= 0) {
                          setCombinationItems([...otherSpecs]);
                        } else {
                          setCombinationItems([
                            ...otherSpecs,
                            {
                              SpecId: x.Id,
                              SpecValueId: e,
                            },
                          ]);
                        }
                      }}
                      value={
                        valueValid ? selectedValueId || undefined : undefined
                      }
                      status={valueValid ? undefined : 'error'}
                      style={{ width: 300 }}
                    ></Select>
                  </Tooltip>
                </Space>
              </div>
            );
          })}
        </Space>
      </>
    );
  };

  return (
    <>
      <Card title="规格参数">{renderForm()}</Card>
    </>
  );
};
