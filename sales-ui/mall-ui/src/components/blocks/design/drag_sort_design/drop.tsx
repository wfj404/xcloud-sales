import { CSSProperties, ReactNode } from 'react';

import { UniqueIdentifier, useDroppable } from '@dnd-kit/core';

export default ({
  id,
  children,
}: {
  id: UniqueIdentifier;
  children?: ReactNode;
}) => {
  const { setNodeRef, isOver } = useDroppable({ id });
  const styles: CSSProperties = {
    minHeight: 500,
    border: isOver ? '3px dashed orange' : 'none',
  };
  return (
    <>
      <div ref={setNodeRef} style={{ ...styles }}>
        {children == undefined ? id : children}
      </div>
    </>
  );
};
