﻿using System;
using Volo.Abp.Application.Dtos;

namespace XCloud.Sales.WeChat.Service.Payments;

public class WechatBillFileRecord : IEntityDto
{
    public DateTime TradeTime { get; set; }
    public string DeviceId { get; set; }
    public string GoodsName { get; set; }
    public string AppId { get; set; }
    public string OpenId { get; set; }
    public string MchId { get; set; }

    public string OutTradeNo { get; set; }
    public string TransactionId { get; set; }
    public string TradeType { get; set; }
    public string TradeStatus { get; set; }
    public decimal BillPrice { get; set; }

    public string OutRefundNo { get; set; }
    public string RefundId { get; set; }
    public decimal RefundPrice { get; set; }
    public string RefundType { get; set; }
    public string RefundStatus { get; set; }

    public bool IsPayment => !this.IsRefund;

    public bool IsRefund => this.OutRefundNo?.Length >= 2;
}

public class WechatPaymentPrepayOrder : IEntityDto
{
    public string PrepayId { get; set; }
}