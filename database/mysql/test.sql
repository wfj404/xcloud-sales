CREATE TABLE `sales_external_sku`
(
    `Id`            VARCHAR(255) PRIMARY KEY,
    `ExternalSkuId` VARCHAR(255)   NOT NULL,
    `GoodsName`     VARCHAR(255)   NOT NULL,
    `SkuName`       VARCHAR(255)   NOT NULL,
    `Price`         DECIMAL(10, 2) NOT NULL,
    `DataJson`      LONGTEXT,
    `SystemName`    VARCHAR(255),
    `CreationTime`  DATETIME       NOT NULL
);

CREATE TABLE `sales_external_sku_mapping`
(
    `Id`            VARCHAR(255) PRIMARY KEY,
    `SystemName`    VARCHAR(255) NOT NULL,
    `ExternalSkuId` VARCHAR(255) NOT NULL,
    `SkuId`         VARCHAR(255) NOT NULL
);