﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Volo.Abp.Http;
using XCloud.Core.Extension;
using XCloud.Core.Http;
using XCloud.Sales.Application.Framework;
using XCloud.Sales.Qingdao.Configuration;

namespace XCloud.Sales.Qingdao.Controllers.Sh3h;

[StoreAuditLog]
[Route("api/mall-3h/common")]
public class Sh3hCommonController : ShopBaseController
{
    private readonly HttpClient _httpClient;

    public Sh3hCommonController(IHttpClientFactory httpClientFactory)
    {
        this._httpClient = httpClientFactory.CreateClient(nameof(Sh3hCommonController));
    }

    [HttpGet("forward-to/mcs-biz-api/v3/bm/customer/{phone}/address-info/{page}/{size}")]
    public async Task<IActionResult> GetCustomerAddressInfoAsync(
        [FromRoute] string phone,
        [FromRoute] int page,
        [FromRoute] int size)
    {
        var sb = new StringBuilder();
        try
        {
            sb.AppendLine(nameof(GetCustomerAddressInfoAsync));

            var config = this.Configuration.GetRequired3HConfig();
            var url = new string[]
            {
                config.McsBmApiUrl,
                $"mcs-biz-api/v3/bm/customer/{phone}/address-info/{page}/{size}"
            }.ConcatUrl();

            sb.AppendLine("url");
            sb.AppendLine(url);

            using var res = await this._httpClient.GetAsync(url);

            res.EnsureSuccessStatusCode();

            var contentType = res.Headers.GetContentTypeOrDefault() ?? MimeTypes.Application.Json;

            sb.AppendLine(nameof(contentType));
            sb.AppendLine(contentType);

            var content = await res.Content.ReadAsStringAsync();

            sb.AppendLine("response body:");
            sb.AppendLine(content);

            return Content(content: content, contentType: contentType);
        }
        finally
        {
            this.Logger.LogInformation(message: sb.ToString());
        }
    }
}