﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using XCloud.Core.Helper;

namespace XCloud.Sales.WeChat.Service.Payments;

public static class ExcelHelper
{
    private static IList<ICell[]> GetSheetRows(ISheet sheet)
    {
        var dataList = new List<ICell[]>();

        for (var i = 0; i <= sheet.LastRowNum; ++i)
        {
            //zero based row
            var row = sheet.GetRow(i);

            if (row == null)
                break;

            var rowData = row.Cells.ToArray();

            dataList.Add(rowData);
        }

        return dataList;
    }

    private static IEnumerable<ISheet> GetAllSheets(XSSFWorkbook wb)
    {
        for (var sheetIndex = 0; sheetIndex <= wb.NumberOfSheets - 1; ++sheetIndex)
        {
            var sheet = wb.GetSheetAt(sheetIndex);

            if (sheet == null)
                continue;

            yield return sheet;
        }
    }

    public static async Task<IDictionary<string, IList<ICell[]>>> FromExcelAsync(Stream stream,
        string[] sheetNames = default)
    {
        var dict = new Dictionary<string, IList<ICell[]>>();

        using var wb = new XSSFWorkbook(stream);

        var sheetList = GetAllSheets(wb).ToArray();

        if (sheetNames != null && ValidateHelper.IsNotEmptyCollection(sheetNames))
        {
            sheetList = sheetList.Where(x => sheetNames.Contains(x.SheetName)).ToArray();
        }

        foreach (var sheet in sheetList)
        {
            dict[sheet.SheetName] = GetSheetRows(sheet);
        }

        await Task.CompletedTask;

        return dict;
    }
}