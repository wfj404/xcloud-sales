import { useRequest } from 'ahooks';
import { Button, Divider, Form, message, Switch } from 'antd';
import { useEffect, useState } from 'react';

import { apiClient } from '@/utils/client';
import { StoreExpressSettings } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XTemplate from './template';

export default () => {
  const querySettings = useRequest(
    apiClient.mallManager.deliverySettingsGetExpressSettings,
    {
      manual: true,
      onSuccess: (e) => {
        _settings(e.data?.Data || {});
      },
    },
  );
  const [settings, _settings] = useState<StoreExpressSettings>({});
  const [loadingSaveSettings, _loadingSaveSettings] = useState(false);

  const saveSettings = () => {
    _loadingSaveSettings(true);
    apiClient.mallManager
      .deliverySettingsSaveExpressSettings({
        ...settings,
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          querySettings.run({});
        });
      })
      .finally(() => {
        _loadingSaveSettings(false);
      });
  };

  useEffect(() => {
    querySettings.run({});
  }, []);

  return (
    <>
      <div style={{}}>
        <div
          style={{
            marginBottom: 20,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <h2>邮寄</h2>
          <Button
            type={'primary'}
            loading={loadingSaveSettings}
            onClick={() => {
              saveSettings();
            }}
          >
            保存设置
          </Button>
        </div>
        <Form
          labelCol={{ span: 3 }}
          style={{
            marginBottom: 10,
          }}
        >
          <Form.Item
            label={'支持快递邮寄'}
            tooltip={'用户可在购买商品时选择该配送方式'}
          >
            <Switch
              loading={loadingSaveSettings || querySettings.loading}
              checked={settings.Enabled}
              onChange={(e) => {
                _settings({
                  ...settings,
                  Enabled: e,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'支持货到付款'} tooltip={'需要上门收款'}>
            <Switch
              checked={settings.PayAfterShippedSupported}
              onChange={(e) => {
                _settings({
                  ...settings,
                  PayAfterShippedSupported: e,
                });
              }}
            />
          </Form.Item>
        </Form>
        <Divider type="horizontal" />
        <XTemplate />
      </div>
    </>
  );
};
