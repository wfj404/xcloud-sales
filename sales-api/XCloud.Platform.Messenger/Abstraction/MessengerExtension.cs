﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Extensions.DependencyInjection;
using XCloud.Platform.Messenger.Abstraction.MessageHandler;
using XCloud.Platform.Messenger.Service;

namespace XCloud.Platform.Messenger.Abstraction;

public static class MessengerExtension
{
    public static OnlineStatusDto ToOnlineStatusDto(this IConnection con)
    {
        var res = new OnlineStatusDto
        {
            UserId = con.ClientIdentity.SubjectId,
            DeviceId = con.ClientIdentity.DeviceType,
            PingTime = con.ClientIdentity.PingTime,
            ServerInstanceId = con.Server.ServerInstanceId,
        };
        return res;
    }

    [CanBeNull]
    public static IMessageHandler GetMessageHandlerOrNull(this IServiceProvider serviceProvider, string type)
    {
        var handlerList = serviceProvider.GetServices<IMessageHandler>().ToArray();
        var handler = handlerList
            .OrderByDescending(x => x.Sort)
            .FirstOrDefault(x => x.MessageType == type);
        return handler;
    }
}