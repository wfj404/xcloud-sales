import { useRequest } from 'ahooks';
import { Checkbox, Radio, Select } from 'antd';
import React, { useEffect } from 'react';

import { apiClient } from '@/utils/client';
import { TagDto } from '@/utils/swagger';

export default ({
  value,
  onChange,
  single,
  single_select,
}: {
  value?: string[];
  onChange?: (e: TagDto[]) => void;
  single?: boolean;
  single_select?: boolean;
}) => {
  const queryTag = useRequest(apiClient.mallAdmin.tagListTags, {
    manual: true,
  });

  const tagList = queryTag.data?.data?.Data || [];

  useEffect(() => {
    queryTag.run({});
  }, []);

  if (single) {
    return (
      <Radio.Group
        value={value?.at(0)}
        onChange={(e) => {
          const tags = tagList.filter((x) => x.Id == e.target.value);
          onChange && onChange(tags);
        }}
        options={tagList.map((x, i) => ({
          label: x.Name || '--',
          value: x.Id || '',
        }))}
      ></Radio.Group>
    );
  }

  if (single_select) {
    return (
      <Select
        style={{ minWidth: 200 }}
        placeholder="选择标签"
        value={value?.at(0)}
        onChange={(e) => {
          const tags = tagList.filter((x) => x.Id == e);
          onChange && onChange(tags);
        }}
        allowClear
        onClear={() => {
          onChange && onChange([]);
        }}
        options={tagList.map((x, i) => ({
          label: x.Name || '--',
          value: x.Id || '',
        }))}
      />
    );
  }

  return (
    <>
      <Checkbox.Group
        value={value}
        onChange={(e) => {
          const tags = tagList.filter((x) => e.indexOf(x.Id || '') >= 0);
          onChange && onChange(tags);
        }}
        options={tagList.map((x, i) => ({
          label: x.Name || '--',
          value: x.Id || '',
        }))}
      ></Checkbox.Group>
    </>
  );
};
