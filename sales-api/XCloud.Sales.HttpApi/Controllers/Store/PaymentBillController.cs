using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Core;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Payments;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/order-payment-bill")]
public class PaymentBillController : ShopBaseController
{
    private readonly IOrderPaymentProcessingService _orderPaymentProcessingService;
    private readonly IOrderPaymentBillService _orderPaymentBillService;
    private readonly IOrderRefundBillService _orderRefundBillService;
    private readonly IOrderService _orderService;
    private readonly IOrderAutoProcessService _orderAutoProcessService;

    public PaymentBillController(
        IOrderPaymentBillService orderPaymentBillService,
        IOrderService orderService,
        IOrderPaymentProcessingService orderPaymentProcessingService,
        IOrderAutoProcessService orderAutoProcessService,
        IOrderRefundBillService orderRefundBillService)
    {
        _orderPaymentBillService = orderPaymentBillService;
        _orderService = orderService;
        _orderPaymentProcessingService = orderPaymentProcessingService;
        _orderAutoProcessService = orderAutoProcessService;
        _orderRefundBillService = orderRefundBillService;
    }

    [HttpPost("create-order-bill")]
    public async Task<ApiResponse<PaymentBillDto>> CreateOrderBillAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var order = await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var bill = await _orderPaymentBillService.CreateOrderPayBillAsync(new CreateOrderPayBillInput
            { OrderId = order.Id });

        return new ApiResponse<PaymentBillDto>(bill);
    }

    [HttpPost("create-offline-payment")]
    public async Task<ApiResponse<object>> CreateOfflinePaymentAsync([FromBody] KeyValuePair<string, decimal?> dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        var order = await _orderService.EnsureStoreOrderAsync(dto.Key, manager.StoreId);

        var bill = await _orderPaymentBillService.CreateOrderPayBillAsync(new CreateOrderPayBillInput
        {
            OrderId = order.Id,
            PriceOrNull = null
        });

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await _orderPaymentBillService.MarkAsPaidAsync(new MarkBillAsPayedInput
        {
            Id = bill.Id,
            PaymentMethod = SalesConstants.PaymentChannel.Manual
        });
        //commit to ensure next step can read the latest data

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        //trigger order payment status update
        await _orderPaymentProcessingService.TryUpdatePaymentStatusAsync(new IdDto { Id = order.Id });

        await _orderAutoProcessService.AutoProcessOrderAsync(order.Id);

        return new ApiResponse<object>();
    }

    [HttpPost("list-order-bill")]
    public async Task<ApiResponse<PaymentBillDto[]>> ListOrderBillAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var bills = await _orderPaymentBillService.GetPaidBillsAsync(dto.Id);

        await this._orderRefundBillService.AttachRefundedBillsAsync(bills);

        return new ApiResponse<PaymentBillDto[]>(bills);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<PaymentBillDto>> QueryPagingAsync([FromBody] QueryOrderBillPagingInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        dto.StoreId = manager.StoreId;

        var response = await _orderPaymentBillService.QueryPagingAsync(dto);

        await this._orderRefundBillService.AttachRefundedBillsAsync(response.Items.ToArray());

        await _orderPaymentBillService.AttachOrderAsync(response.Items.ToArray());

        var orders = response.Items.Where(x => x.Order != null).Select(x => x.Order).ToArray();

        await _orderService.AttachStoreUsersAsync(orders);

        return response;
    }
}