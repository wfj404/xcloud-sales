import { useRequest } from 'ahooks';
import { Button, Input, message, Modal, Space, Tag } from 'antd';
import { useState } from 'react';

import { apiClient } from '@/utils/client';
import { UserDto } from '@/utils/swagger';
import { handleResponse, isMobile, isStringEmpty } from '@/utils/utils';
import { MobileOutlined } from '@ant-design/icons';

export default ({ model, ok }: { model: UserDto; ok: () => void }) => {
  const record = model;

  const [open, _open] = useState(false);
  const [mobile, _mobile] = useState<string>('');

  if (!record) {
    return null;
  }

  const removeMobileRequest = useRequest(
    apiClient.sys.manageUserRemoveMobiles,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {
          message.success('移除成功');
          ok();
        });
      },
    },
  );

  const removeMobile = () => {
    if (!confirm('确定移除手机号？')) {
      return;
    }
    removeMobileRequest.run({ Id: record.Id });
  };

  const updateMobileRequest = useRequest(apiClient.sys.manageUserSetMobile, {
    manual: true,
    onSuccess(data, params) {
      handleResponse(data, () => {
        message.success('设置成功');
        ok();
      });
    },
  });

  const updateMobile = () => {
    if (!mobile || isStringEmpty(mobile)) {
      message.info('放弃设置手机号码');
      return;
    }
    if (!/^(?:(?:\+|00)86)?1[3-9]\d{9}$/.test(mobile)) {
      message.error('手机号码格式不正确');
      return;
    }
    updateMobileRequest.run({
      Id: record.Id,
      Mobile: mobile,
    });
  };

  return (
    <>
      <Modal
        open={open}
        onCancel={() => _open(false)}
        confirmLoading={updateMobileRequest.loading}
        onOk={() => {
          updateMobile();
        }}
        title="设置手机号"
      >
        <Input
          width={'100%'}
          size="large"
          maxLength={11}
          placeholder="输入完整手机号"
          value={mobile}
          onChange={(e) => {
            _mobile(e.target.value);
          }}
          status={isMobile(mobile) ? undefined : 'error'}
        />
      </Modal>
      <Space direction={'vertical'}>
        {isStringEmpty(record.UserMobile?.MobilePhone) || (
          <Tag
            icon={<MobileOutlined />}
            color={'green'}
            closable
            onClose={(e) => {
              e.preventDefault();
              removeMobile();
            }}
          >
            {record.UserMobile?.MobilePhone}
          </Tag>
        )}

        <Button
          type="link"
          onClick={() => {
            _open(true);
          }}
        >
          绑定手机
        </Button>
      </Space>
    </>
  );
};
