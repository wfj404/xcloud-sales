﻿
```csharp
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RedLockNet;
using RedLockNet.SERedis;
using RedLockNet.SERedis.Configuration;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Redis;

namespace XCloud.Application.DistributedLock;

public class RedLockDistributedLock : IDistributedLock
{
    private readonly IRedLock _redLock;

    public RedLockDistributedLock(IRedLock redLock)
    {
        if (redLock == null)
            throw new ArgumentNullException(nameof(redLock));

        _redLock = redLock;
    }

    public bool IsAcquired => this._redLock.IsAcquired;

    public async ValueTask DisposeAsync()
    {
        await ValueTask.CompletedTask;

        await using (this._redLock)
        {
            //
        }
    }
}

[ExposeServices(typeof(IDistributedLockProvider))]
public class RedLockDistributedLockProvider : IDistributedLockProvider, ISingletonDependency
{
    /// <summary>
    /// string resource, 锁定对象
    /// TimeSpan expiryTime,锁过期时间 
    /// TimeSpan waitTime, 最长等待锁时间
    /// TimeSpan retryTime,重试间隔
    /// </summary>
    public RedLockFactory RedLockFactory { get; }

    public RedLockDistributedLockProvider(RedisClient redisClient, ILoggerFactory loggerFactory)
    {
        var multiplexers = new List<RedLockMultiplexer>
        {
            new RedLockMultiplexer(redisClient.Connection)
        };

        foreach (var m in multiplexers)
        {
            m.RedisDatabase = (int)RedisConsts.DistributedLock;
        }

        RedLockFactory = RedLockFactory.Create(multiplexers, loggerFactory);
    }

    public async Task<IDistributedLock> CreateLockAsync(string key, TimeSpan? expired)
    {
        if (string.IsNullOrWhiteSpace(key))
            throw new ArgumentNullException(nameof(key));

        var timeout = expired ?? TimeSpan.FromSeconds(3);

        var redisLock = await this.RedLockFactory.CreateLockAsync(key, timeout);

        return new RedLockDistributedLock(redisLock);
    }

    public int DisposeOrder => default;

    public void Dispose()
    {
        RedLockFactory?.Dispose();
    }
}
```