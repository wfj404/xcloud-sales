import { useRequest } from 'ahooks';
import { Card } from 'antd';
import * as echarts from 'echarts';
import { sumBy } from 'lodash-es';
import { useEffect, useRef } from 'react';

import { apiClient } from '@/utils/client';
import { timezoneOffset } from '@/utils/dayjs';
import { UserActivityGroupByHourResponse } from '@/utils/swagger';

export default () => {
  const reportRequest = useRequest(
    apiClient.mallAdmin.reportUserActivityGroupByHour,
    {
      manual: true,
    },
  );

  const data =
    reportRequest.data?.data.Data?.map<UserActivityGroupByHourResponse>(
      (x) => ({
        ...x,
        Hour: ((x.Hour || 0) + timezoneOffset) % 24,
      }),
    ) || [];

  const chart1Ref = useRef<HTMLDivElement>(null);

  const groupByHour = () => {
    const groupedData: UserActivityGroupByHourResponse[] = [];
    for (let i = 0; i <= 24; ++i) {
      const hour_data_list = data.filter((x) => x.Hour == i);
      groupedData.push({
        Hour: i,
        Count: sumBy(hour_data_list, (d) => d.Count || 0),
      });
    }
    return groupedData;
  };

  const finalData = groupByHour();

  useEffect(() => {
    if (chart1Ref.current == null) {
      return;
    }
    let chart = echarts.init(chart1Ref.current);
    chart.setOption({
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
      },
      xAxis: [
        {
          type: 'category',
          data: finalData.map((x) => x.Hour || 0),
          axisTick: {
            alignWithLabel: true,
          },
        },
      ],
      yAxis: [
        {
          type: 'value',
        },
      ],
      series: [
        {
          name: '活跃度',
          type: 'bar',
          barWidth: '60%',
          data: finalData.map((x) => x.Count || 0),
        },
      ],
    });
  }, [finalData]);

  useEffect(() => {
    reportRequest.run({});
  }, []);

  return (
    <>
      <Card
        title="用户活跃时间段"
        style={{ marginBottom: 10 }}
        loading={reportRequest.loading}
      >
        <div style={{ minHeight: 400 }} ref={chart1Ref}></div>
      </Card>
    </>
  );
};
