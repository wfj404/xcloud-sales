﻿using System.Linq;
using XCloud.Core.Helper;
using XCloud.Sales.Qingdao.Configuration;

namespace XCloud.Sales.Qingdao.Service;

public static class QingdaoServiceExtension
{
    public static bool IsStoreOrganization(this WapOrganization organization, Sh3HConfig config)
    {
        return ValidateHelper.IsEmptyCollection(config.StoreOrgCodeList) ||
               config.StoreOrgCodeList.Contains(organization.organizationCode);
    }
}