﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Caching.Memory;
using XCloud.Core.Dto;
using XCloud.Core.Helper;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Platform.Application.Framework;
using XCloud.Platform.Application.Service.Users;

namespace XCloud.Platform.HttpApi.Controllers;

[Route("/api/platform/user")]
public class UserController : PlatformBaseController
{
    private readonly IUserService _userService;
    private readonly IWebHostEnvironment _webHostEnvironment;
    private readonly IMemoryCache _memoryCache;
    private readonly IUserMobileService _userMobileService;

    public UserController(
        IWebHostEnvironment webHostEnvironment,
        IMemoryCache memoryCache,
        IUserMobileService userMobileService,
        IUserService userService1)
    {
        _webHostEnvironment = webHostEnvironment;
        _memoryCache = memoryCache;
        _userMobileService = userMobileService;
        _userService = userService1;
    }

    private readonly Random _rand = new Random((int)DateTime.UtcNow.Ticks);

    [HttpGet("random-avatar")]
    public async Task<IActionResult> RandomAvatars()
    {
        var dir = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot", "images", "avatars");

        var files = _memoryCache.GetOrCreate($"{nameof(RandomAvatars)}.avatars", x =>
        {
            x.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30);
            return Directory.GetFiles(dir, "*.svg");
        });

        if (ValidateHelper.IsEmptyCollection(files))
            return NotFound();

        var theOne = _rand.Choice(files);

        var theOnePath = Path.Combine(dir, theOne);

        var bs = await System.IO.File.ReadAllBytesAsync(theOnePath);

        return File(bs, contentType: "image/svg+xml");
    }

    [HttpPost("profile")]
    public async Task<ApiResponse<UserDto>> UserProfile()
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        var user = await this._userService.GetByIdAsync(loginUser.Id);

        if (user == null)
        {
            return new ApiResponse<UserDto>();
        }

        user.UserMobile = await _userMobileService.GetMobileByUserIdAsync(loginUser.Id);

        return new ApiResponse<UserDto>().SetData(user);
    }

    /// <summary>
    /// 更新个人信息
    /// </summary>
    [HttpPost("update-profile")]
    public async Task<ApiResponse<object>> UpdateProfile([FromBody] UserDto model)
    {
        var loginUser = await UserAuthService.GetRequiredAuthedUserAsync();

        model.Id = loginUser.Id;

        await _userService.UpdateProfileAsync(model);

        await RequiredCurrentUnitOfWork.SaveChangesAsync();

        await this.PlatformEventBusService.NotifyUserUpdatedAsync(model);

        return new ApiResponse<object>();
    }
}