﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using XCloud.Application.Extension;
using XCloud.Application.Mapper;
using XCloud.Application.Service;
using XCloud.Platform.Application.Domain.Dept;
using XCloud.Platform.Application.Repository;

namespace XCloud.Platform.Application.Service.Dept;

public interface IDepartmentService : IXCloudAppService
{
    Task<DepartmentDto[]> ListAllAsync();

    Task<Department> InsertAsync([NotNull] DepartmentDto dto);

    Task<Department> UpdateAsync([NotNull] DepartmentDto dto);

    Task DeleteByIdAsync([NotNull] string id);

    [ItemCanBeNull]
    Task<DepartmentDto> GetByIdAsync([NotNull] string id);

    Task<DepartmentDto[]> QueryByParentIdAsync([CanBeNull] string parentId = default);
}

public class DepartmentService : XCloudAppService, IDepartmentService
{
    private readonly IPlatformRepository<Department> _repository;

    public DepartmentService(IPlatformRepository<Department> repository)
    {
        _repository = repository;
    }

    public async Task<Department> InsertAsync(DepartmentDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        dto.ParentId ??= string.Empty;
        dto.ParentId = dto.ParentId.Trim();

        if (!string.IsNullOrWhiteSpace(dto.ParentId))
        {
            var parent = await this.GetByIdAsync(dto.ParentId);
            if (parent == null)
                throw new EntityNotFoundException(nameof(parent));
        }

        var entity = dto.MapTo<DepartmentDto, Department>(this.ObjectMapper);

        entity.Id = this.GuidGenerator.CreateGuidString();
        entity.CreationTime = this.Clock.Now;

        await this._repository.InsertAsync(entity);

        return entity;
    }

    public async Task<Department> UpdateAsync(DepartmentDto dto)
    {
        if (string.IsNullOrWhiteSpace(dto.Id))
            throw new ArgumentNullException(nameof(dto.Id));

        if (string.IsNullOrWhiteSpace(dto.Name))
            throw new ArgumentNullException(nameof(dto.Name));

        var entity = await this._repository.GetRequiredByIdAsync(dto.Id);

        entity.Name = dto.Name;
        entity.Description = dto.Description;
        entity.DeptType = dto.DeptType;

        await this._repository.UpdateAsync(entity);

        return entity;
    }

    public async Task DeleteByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var children = await this.QueryByParentIdAsync(id);

        if (children.Any())
            throw new UserFriendlyException("sub department exist");

        await this._repository.DeleteAsync(x => x.Id == id);
    }

    public async Task<DepartmentDto> GetByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await this._repository.FirstOrDefaultAsync(x => x.Id == id);

        return entity?.MapTo<Department, DepartmentDto>(this.ObjectMapper);
    }

    public async Task<DepartmentDto[]> ListAllAsync()
    {
        var db = await this._repository.GetDbContextAsync();

        var datalist = await db.Set<Department>().AsNoTracking().OrderBy(x => x.CreationTime).ToArrayAsync();

        return datalist.MapArrayTo<Department, DepartmentDto>(this.ObjectMapper);
    }

    public async Task<DepartmentDto[]> QueryByParentIdAsync(string parentId)
    {
        var db = await this._repository.GetDbContextAsync();

        var query = db.Set<Department>().AsNoTracking();

        if (string.IsNullOrWhiteSpace(parentId))
        {
            query = query.Where(x => x.ParentId == null || x.ParentId == string.Empty);
        }
        else
        {
            query = query.Where(x => x.ParentId == parentId);
        }

        var datalist = await query.OrderBy(x => x.CreationTime).TakeUpTo5000().ToArrayAsync();

        return datalist.MapArrayTo<Department, DepartmentDto>(this.ObjectMapper);
    }
}