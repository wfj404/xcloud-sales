﻿using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.AfterSale;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Users;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/after-sale")]
public class AfterSaleController : ShopBaseController
{
    private readonly IOrderService _orderService;
    private readonly IOrderItemService _orderItemService;
    private readonly IAfterSaleService _afterSaleService;
    private readonly IAfterSaleProcessingService _afterSaleProcessingService;
    private readonly IAfterSaleCommentService _afterSaleCommentService;
    private readonly ISkuService _skuService;
    private readonly IGoodsPictureService _goodsPictureService;
    private readonly IStoreUserService _storeUserService;

    public AfterSaleController(IOrderService orderService,
        IAfterSaleService afterSaleService,
        IAfterSaleCommentService afterSaleCommentService,
        IAfterSaleProcessingService afterSaleProcessingService,
        ISkuService skuService, IGoodsPictureService goodsPictureService,
        IStoreUserService storeUserService, IOrderItemService orderItemService)
    {
        _afterSaleService = afterSaleService;
        _afterSaleCommentService = afterSaleCommentService;
        _afterSaleProcessingService = afterSaleProcessingService;
        _skuService = skuService;
        _goodsPictureService = goodsPictureService;
        _storeUserService = storeUserService;
        _orderItemService = orderItemService;
        _orderService = orderService;
    }

    [HttpPost("add-comment")]
    public async Task<ApiResponse<object>> InsertCommentAsync([FromBody] AfterSalesCommentDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.AfterSaleId, manager.StoreId);

        dto.IsAdmin = true;

        await _afterSaleCommentService.InsertAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("comment-paging")]
    public async Task<PagedResponse<AfterSalesCommentDto>> QueryCommentPagingAsync(
        [FromBody] QueryAfterSalesCommentPaging dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.AfterSalesId, manager.StoreId);

        var response = await _afterSaleCommentService.QueryPagingAsync(dto);

        return response;
    }

    [HttpPost("update-status")]
    public async Task<ApiResponse<object>> UpdateStatusAsync([FromBody] UpdateAfterSaleStatusDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        await _afterSaleService.UpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("dangerously-update-status")]
    public async Task<ApiResponse<object>> UpdateAfterSalesStatusAsync(
        [FromBody] DangerouslyUpdateAfterSalesStatusDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        await _afterSaleProcessingService.DangerouslyUpdateStatusAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("cancel")]
    public async Task<ApiResponse<object>> CancelAsync([FromBody] CancelAfterSaleDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        await _afterSaleProcessingService.CancelAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("complete")]
    public async Task<ApiResponse<object>> CompleteAsync([FromBody] CompleteAfterSaleDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        await _afterSaleProcessingService.CompleteAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("approve")]
    public async Task<ApiResponse<object>> ApproveAsync([FromBody] ApproveAfterSaleDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        await _afterSaleProcessingService.ApproveAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("reject")]
    public async Task<ApiResponse<object>> RejectAsync([FromBody] RejectAfterSaleInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        await _afterSaleProcessingService.RejectAsync(dto);

        return new ApiResponse<object>();
    }

    [NonAction]
    private async Task AttachDataAsync(AfterSalesDto[] data)
    {
        if (!data.Any())
            return;

        await _afterSaleService.AttachOrderAsync(data);
        await _afterSaleService.AttachItemsAsync(data);

        var orders = data.Select(x => x.Order).WhereNotNull().ToArray();

        await this._orderService.AttachStoreUsersAsync(orders);

        var mallUsers = orders.Select(x => x.StoreUser).WhereNotNull().ToArray();

        await _storeUserService.AttachPlatformUsersAsync(mallUsers);

        var afterSalesItems = data.SelectMany(x => x.Items).ToArray();

        if (!afterSalesItems.Any())
            return;

        await _afterSaleService.AttachOrderItemsAsync(afterSalesItems);

        var orderItems = afterSalesItems.Select(x => x.OrderItem).WhereNotNull().ToArray();

        if (!orderItems.Any())
            return;

        await _orderItemService.AttachOrderItemSkuAsync(orderItems);
        var skus = orderItems.Select(x => x.Sku).WhereNotNull().ToArray();
        await _skuService.AttachGoodsAsync(skus);

        var goods = skus.Select(x => x.Goods).WhereNotNull().ToArray();

        if (!goods.Any())
            return;

        await _goodsPictureService.AttachImagesAsync(goods);
    }

    [HttpPost("get-by-order-id")]
    public async Task<ApiResponse<AfterSalesDto>> GetByOrderIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _orderService.EnsureStoreOrderAsync(dto.Id, manager.StoreId);

        var afterSalesDto = await _afterSaleService.QueryByOrderIdAsync(dto.Id);

        if (afterSalesDto == null)
            throw new EntityNotFoundException();

        await AttachDataAsync(new[] { afterSalesDto });

        return new ApiResponse<AfterSalesDto>(afterSalesDto);
    }

    [HttpPost("by-id")]
    public async Task<ApiResponse<AfterSalesDto>> QueryByIdAsync([FromBody] IdDto dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        await _afterSaleService.EnsureStoreAfterSalesAsync(dto.Id, manager.StoreId);

        var afterSalesDto = await _afterSaleService.GetByIdAsync(dto.Id);

        if (afterSalesDto == null)
            throw new EntityNotFoundException();

        await AttachDataAsync(new[] { afterSalesDto });

        return new ApiResponse<AfterSalesDto>(afterSalesDto);
    }

    [HttpPost("paging")]
    public async Task<PagedResponse<AfterSalesDto>> QueryPagingAsync([FromBody] QueryAfterSalePaging dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager,
            SalesPermissions.ManageOrders);

        dto.PageSize = 20;
        dto.IsDeleted = null;
        dto.StoreId = manager.StoreId;

        var response = await _afterSaleService.QueryPagingAsync(dto);

        await AttachDataAsync(response.Items.ToArray());

        return response;
    }
}