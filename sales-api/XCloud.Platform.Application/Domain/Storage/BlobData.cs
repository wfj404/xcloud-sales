﻿namespace XCloud.Platform.Application.Domain.Storage;

public class BlobData : PlatformBaseEntity, IHasCreationTime, IHasModificationTime
{
    public string Name { get; set; }

    public byte[] Content { get; set; }

    public string Base64Text { get; set; }

    public int Format { get; set; }

    public DateTime CreationTime { get; set; }

    public DateTime? LastModificationTime { get; set; }
}