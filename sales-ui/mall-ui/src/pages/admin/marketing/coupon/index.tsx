import { useRequest } from 'ahooks';
import { Badge, Button, Card, Table, Tag } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useEffect, useState } from 'react';

import XTime from '@/components/common/time';
import XRenderTime from '@/components/common/time/render_time';
import { apiClient } from '@/utils/client';
import { CouponDto, QueryCouponPagingInput } from '@/utils/swagger';
import { formatMoney, handleResponse, isStringEmpty } from '@/utils/utils';
import { PlusOutlined } from '@ant-design/icons';

import XForm from './form';

export default () => {
  const queryCouponRequest = useRequest(
    apiClient.mallAdmin.couponQueryCouponPaging,
    {
      manual: true,
      onSuccess(data, params) {
        handleResponse(data, () => {});
      },
    },
  );

  const data = queryCouponRequest.data?.data || {};
  const [query, _query] = useState<QueryCouponPagingInput>({
    Page: 1,
  });

  const [formData, _formData] = useState<CouponDto | undefined>(undefined);

  const queryList = () => {
    queryCouponRequest.run({ ...query });
  };

  const columns: ColumnProps<CouponDto>[] = [
    {
      title: '名称',
      render: (d, x) => (
        <>
          <b>{x.Name || '--'}</b>
          {isStringEmpty(x.Description) || <p>{x.Description}</p>}
        </>
      ),
    },
    {
      title: '金额',
      render: (d, x) => (
        <Tag color={'warning'}>
          <b>{formatMoney(x.Price || 0)}</b>
        </Tag>
      ),
    },
    {
      title: '数量',
      render: (d, x) => x.Amount || 0,
    },
    {
      title: '领取限制',
      render: (d, x) => {
        if (!x.AccountIssuedLimitCount) {
          return null;
        }
        return <b>{x.AccountIssuedLimitCount}</b>;
      },
    },
    {
      title: '领取后有效期',
      render: (d, x) => {
        if (!x.ExpiredInDays) {
          return null;
        }
        return <b>{`${x.ExpiredInDays}天`}</b>;
      },
    },
    {
      title: '时间范围',
      render: (d, x) => {
        return (
          <>
            {x.StartTime && x.StartTime.length > 0 && (
              <div>
                开始时间：
                <XRenderTime timeStr={x.StartTime} />
              </div>
            )}
            {x.EndTime && x.EndTime.length > 0 && (
              <div>
                结束时间：
                <XRenderTime timeStr={x.EndTime} />
              </div>
            )}
          </>
        );
      },
    },
    {
      title: '领取数量',
      render: (d, x) => x.IssuedAmount || 0,
    },
    {
      title: '使用数量',
      render: (d, x) => x.UsedAmount || 0,
    },
    {
      title: '状态',
      render: (d, x) => {
        return x.IsActive ? (
          <Badge text={'可用'} status={'success'} />
        ) : (
          <Badge text={'不可用'} status={'error'} />
        );
      },
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <Button.Group>
            <Button
              type="link"
              onClick={() => {
                _formData(x);
              }}
            >
              编辑
            </Button>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, [query]);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title="优惠券"
        extra={
          <Button
            icon={<PlusOutlined />}
            type="primary"
            onClick={() => {
              _formData({});
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          loading={queryCouponRequest.loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
