import { useRequest } from 'ahooks';
import { Spin } from 'antd';
import React, { ReactNode, useEffect } from 'react';
import { useLocation, useSnapshot } from 'umi';

import { storeState, StoreType } from '@/store';
import { isUserLogin } from '@/utils/biz';
import { apiClient } from '@/utils/client';
import { hasAccessToken } from '@/utils/utils';

import UserDeleted from './deleted';
import UserDisabled from './disabled';
import UserNotLogin from './not_login';
import { UserAuthResult } from '@/utils/swagger';

export default function({ children }: { children: ReactNode }) {
  const app = useSnapshot(storeState) as StoreType;
  const location = useLocation();

  const model = app.userAuthResult?.User || {};

  const query_login_request = useRequest(apiClient.platform.authUserAuth, {
    manual: true,
    onSuccess(data, params) {
      const result: UserAuthResult = data.data || {};
      app.setUserAuthResult(result);
    },
  });

  useEffect(() => {
    if (hasAccessToken()) {
      query_login_request.run();
    } else {
      app.setUserAuthResult(undefined);
    }
  }, [location.pathname]);

  if (!isUserLogin(model) && query_login_request.loading) {
    return (
      <Spin
        spinning={query_login_request.loading}
        size='large'
        fullscreen
        tip={<p>加载登录信息...</p>}
      />
    );
  }

  if (!isUserLogin(model)) {
    return (
      <UserNotLogin />
    );
  }

  if (model.IsDeleted) {
    return <UserDeleted />;
  }

  if (!model.IsActive) {
    return (
      <UserDisabled />
    );
  }

  return <>{children}</>;
}
