import { BlockItem, BlockItemProps } from '@/components/blocks/utils';
import { Box, Typography } from '@mui/material';

export default ({ block }: { block: BlockItem<BlockItemProps> }) => {
  return (
    <>
      <Box sx={{ p: 3, backgroundColor: 'rgb(240,240,240)', border: '3px dashed green' }}>
        <Typography variant='caption' color='text.disabled' fontSize={20}>
          拖放至此
        </Typography>
      </Box>
    </>
  );
};
