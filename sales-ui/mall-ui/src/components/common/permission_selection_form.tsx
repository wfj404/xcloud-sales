import { Card, Checkbox } from 'antd';

import { PermissionDto, PermissionGroupDto } from '@/utils/biz';

export default ({
  selectedKeys,
  onChange,
  permissions,
}: {
  selectedKeys: string[];
  onChange: (d: string[]) => void;
  permissions: PermissionGroupDto[];
}) => {
  const renderCheckbox = (d: PermissionDto) => {
    return (
      <Checkbox
        checked={selectedKeys.indexOf(d.key) >= 0}
        onChange={(e) => {
          let items = selectedKeys.filter((x) => x != d.key);
          if (e.target.checked) {
            items.push(d.key);
          }
          onChange(items);
        }}
      >
        {d.name}
      </Checkbox>
    );
  };

  const renderGroup = (x: PermissionGroupDto) => {
    let all = x.permissions.every((d) => selectedKeys.indexOf(d.key) >= 0);

    return (
      <Card
        size="small"
        title={x.name}
        style={{ marginBottom: 10 }}
        extra={
          <Checkbox
            checked={all}
            onChange={(e) => {
              let allGroupPermissions = x.permissions.map((x) => x.key);
              let items = selectedKeys.filter(
                (x) => allGroupPermissions.indexOf(x) < 0,
              );
              if (e.target.checked) {
                items = [...items, ...allGroupPermissions];
              }
              onChange(items);
            }}
          >
            全选
          </Checkbox>
        }
      >
        {x.permissions.map((d, i) => {
          return (
            <span style={{ marginRight: 10, marginBottom: 10 }} key={i}>
              {renderCheckbox(d)}
            </span>
          );
        })}
      </Card>
    );
  };

  return (
    <>
      <div style={{}}>
        {permissions.map((x, index) => {
          return <div key={index}>{renderGroup(x)}</div>;
        })}
      </div>
    </>
  );
};
