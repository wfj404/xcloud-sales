using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Application.Mapping;
using XCloud.Platform.Application.Domain.Security;

namespace XCloud.Platform.Application.Mapping.Security;

public class PermissionMap : PlatformEntityTypeConfiguration<Permission>
{
    public override void Configure(EntityTypeBuilder<Permission> builder)
    {
        base.Configure(builder);

        builder.ToTable("sys_permission");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.PermissionKey).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.Group).HasMaxLength(100);
        builder.Property(x => x.AppKey).HasMaxLength(100);
        builder.Property(x => x.CreationTime);
    }
}