﻿using System.Collections.Generic;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Logging;

namespace XCloud.AspNetMvc.Core;

[ExposeServices(typeof(ILoggingExtraPropertiesProvider))]
public class AspNetCoreLoggingExtraPropertiesProvider : ILoggingExtraPropertiesProvider, ITransientDependency
{
    private readonly IWebHelper _webHelper;

    public AspNetCoreLoggingExtraPropertiesProvider(IWebHelper webHelper)
    {
        _webHelper = webHelper;
    }

    public IDictionary<string, string> GetExtraProperties()
    {
        var dict = new Dictionary<string, string>();

        var context = this._webHelper.HttpContext;

        if (context != null)
        {
            dict["request_url"] = context.Request.Path;
            dict["http_method"] = context.Request.Method;
            dict["x_request_ip"] = this._webHelper.GetCurrentIpAddressOrNull();

            if (context.Request.Headers.TryGetValue("client_name", out var name))
            {
                dict["client_name"] = name;
            }
        }

        return dict;
    }
}