﻿using Microsoft.AspNetCore.Http;
using XCloud.Core.Dto;
using XCloud.Core.Exceptions;
using XCloud.Sales.Application.Service.Stores.Context;

namespace XCloud.Sales.Application.Core;

[ExposeServices(typeof(IErrorInfoConverter), typeof(XCloudSalesErrorInfoConverter))]
public class XCloudSalesErrorInfoConverter : IErrorInfoConverter, ISingletonDependency
{
    public int Order => int.MaxValue;

    public ErrorInfo ConvertToErrorInfoOrNull(HttpContext httpContext, Exception e)
    {
        if (e is StoreIsRequiredException storeIsRequiredException)
        {
            return new ErrorInfo()
            {
                Message = storeIsRequiredException.Message ?? nameof(StoreIsRequiredException),
                Code = "store-is-required"
            };
        }

        if (e is SalesException salesException)
        {
            return new ErrorInfo()
            {
                Message = salesException.Message ?? nameof(SalesException),
                Code = "sales"
            };
        }

        return null;
    }
}