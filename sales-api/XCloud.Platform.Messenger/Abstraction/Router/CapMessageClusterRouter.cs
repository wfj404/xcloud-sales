﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DotNetCore.CAP;
using DotNetCore.CAP.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Queue;
using XCloud.Core.Extension;
using XCloud.Core.Json;
using XCloud.Platform.Messenger.Abstraction.Core;
using XCloud.Platform.Messenger.Queue;

namespace XCloud.Platform.Messenger.Abstraction.Router;

public interface IMessageFromCap
{
    Task MessageFromCapAsync(MessageDto messageDto);
}

[ExposeServices(typeof(IMessageClusterRouter), typeof(IMessageFromCap))]
public class CapMessageClusterRouter : IMessageClusterRouter, IMessageFromCap, ISingletonDependency
{
    private readonly ICapPublisher _capPublisher;
    private readonly IJsonDataSerializer _jsonDataSerializer;
    private readonly ILogger _logger;

    public CapMessageClusterRouter(ICapPublisher capPublisher,
        IJsonDataSerializer jsonDataSerializer,
        ILogger<CapMessageClusterRouter> logger)
    {
        _capPublisher = capPublisher;
        _jsonDataSerializer = jsonDataSerializer;
        _logger = logger;
    }

    public void Dispose()
    {
        // TODO release managed resources here
        this._logger.LogInformation(message: $"{nameof(CapMessageClusterRouter)}.{nameof(Dispose)}");
    }

    public async Task RouteToUserAsync(string userId, MessageDto data)
    {
        await this.BroadCastAsync(data);
    }

    public async Task BroadCastAsync(MessageDto dto)
    {
        await this._capPublisher.PublishAsync(MessengerMessageTopic.BroadCastKey, dto);
    }


    private Func<MessageDto, Task> _callback;

    public Task SubscribeFromMessageRouterAsync(string serverInstanceKey, Func<MessageDto, Task> callback)
    {
        this._callback = callback;
        return Task.CompletedTask;
    }

    public async Task MessageFromCapAsync(MessageDto messageDto)
    {
        var json = this._jsonDataSerializer.SerializeToString(messageDto);
        this._logger.LogInformation($"message-from-cap:{json}");

        if (this._callback != null)
        {
            await this._callback.Invoke(messageDto);
        }
    }
}

[ExposeServices(typeof(ICapConsumerServiceSelectorContributor))]
public class MessageCapConsumerServiceSelectorContributor : ICapConsumerServiceSelectorContributor, ITransientDependency
{
    private readonly IMessengerServer _server;
    private readonly CapOptions _capOptions;

    public MessageCapConsumerServiceSelectorContributor(IMessengerServer server, IOptions<CapOptions> capOptions)
    {
        _server = server;
        this._capOptions = capOptions.Value ?? throw new ArgumentNullException(nameof(capOptions));
    }

    public IEnumerable<ConsumerExecutorDescriptor> FindCapConsumers()
    {
        var implType = typeof(CapMessageClusterRouter);
        var serviceType = typeof(IMessageFromCap);

        implType.IsAssignableTo<IMessageFromCap>().Should().BeTrue();

        var method = implType.GetMethod(nameof(IMessageFromCap.MessageFromCapAsync), new[] { typeof(MessageDto) }) ??
                     throw new AbpException($"{nameof(IMessageFromCap.MessageFromCapAsync)} not found");

        var parameterList = method.GetParameters().Select(x => new ParameterDescriptor()
        {
            Name = x.Name ?? throw new AbpException(message: "param name"),
            ParameterType = x.ParameterType,
            IsFromCap = x.GetCustomAttributes(attributeType: typeof(FromCapAttribute), inherit: true).Any()
        }).ToArray();

        yield return new ConsumerExecutorDescriptor()
        {
            ServiceTypeInfo = serviceType.GetTypeInfo(),
            ImplTypeInfo = implType.GetTypeInfo(),
            MethodInfo = method,
            Parameters = parameterList,
            Attribute = new CapSubscribeAttribute(MessengerMessageTopic.BroadCastKey)
            {
                Group = $"consumer.group.{this._server.ServerInstanceId}"
            },
            ClassAttribute = default,
            TopicNamePrefix = this._capOptions.TopicNamePrefix,
        };
    }
}