import 'react-js-cron/dist/styles.css';

import { Button, Card, Form, Input, List, Modal } from 'antd';
import { pullAt } from 'lodash-es';
import { useState } from 'react';
import { Cron } from 'react-js-cron';

import { CronRule } from '@/utils/swagger';

export default ({
  data,
  ok,
}: {
  data: CronRule[];
  ok: (d: CronRule[]) => void;
}) => {
  const [rule, _rule] = useState<CronRule | undefined>(undefined);

  return (
    <>
      <Modal
        title={'周期性规则'}
        width={'50%'}
        open={rule != undefined}
        onOk={() => {
          const rules = data || [];
          ok([...rules, rule || {}]);
          _rule(undefined);
        }}
        onCancel={() => {
          _rule(undefined);
        }}
      >
        <Form>
          <Form.Item label={'名称'} tooltip={'比如：工作日'}>
            <Input
              value={rule?.Name || ''}
              onChange={(e) => {
                _rule({
                  ...rule,
                  Name: e.target.value,
                });
              }}
            />
          </Form.Item>
          <Form.Item label={'Cron表达式'}>
            <Cron
              locale={{
                everyText: '每',
              }}
              allowedDropdowns={['period', 'months', 'month-days', 'week-days']}
              allowedPeriods={['year', 'month', 'week', 'day']}
              value={rule?.Cron || ''}
              setValue={(e: string) => {
                _rule({
                  ...rule,
                  Cron: e,
                });
              }}
            />
          </Form.Item>
        </Form>
      </Modal>

      <Card
        size="small"
        title={'周期性规则'}
        extra={
          <Button
            onClick={() => {
              _rule({
                Name: '正常工作日',
                Cron: '0 0 * * 1-5',
              });
            }}
            type={'link'}
          >
            添加
          </Button>
        }
      >
        <List
          dataSource={data}
          renderItem={(x: CronRule, i) => {
            return (
              <List.Item
                actions={[
                  <Button
                    type={'link'}
                    danger
                    onClick={() => {
                      let rules = data || [];
                      rules = [...rules];
                      pullAt(rules, i);
                      ok(rules);
                    }}
                  >
                    删除
                  </Button>,
                ]}
              >
                <List.Item.Meta
                  title={x.Name || '--'}
                  description={x.Cron || '--'}
                ></List.Item.Meta>
              </List.Item>
            );
          }}
        ></List>
      </Card>
    </>
  );
};
