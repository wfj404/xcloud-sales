﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using NPOI.SS.UserModel;
using XCloud.Application.Extension;
using XCloud.Platform.WeChat;

namespace XCloud.Sales.WeChat.Service.Payments;

public static class WechatBillParserHelper
{
    private static DateTime ParseWechatBillDateTime([NotNull] string time) => DateTime.ParseExact(
        s: time,
        format: "yyyy-MM-dd HH:mm:ss",
        provider: CultureInfo.InvariantCulture,
        style: DateTimeStyles.None);

    private static IEnumerable<WechatBillFileRecord> ParseBills([NotNull] IList<string[]> dataRows)
    {
        if (dataRows.Count < 3)
            throw new ArgumentException(message: "error data format");

        var headers = dataRows[0].ToList();

        for (var i = 0 + 1; i <= dataRows.Count - 1 - 2; ++i)
        {
            //zero based row
            var row = dataRows[i];

            var data = new WechatBillFileRecord
            {
                TradeTime = ParseWechatBillDateTime(row.GetCellValue(headers, "交易时间", required: true)!),
                DeviceId = row.GetCellValue(headers, "设备号"),
                GoodsName = row.GetCellValue(headers, "商品名称"),
                MchId = row.GetCellValue(headers, "商户号"),
                AppId = row.GetCellValue(headers, "公众账号ID"),
                OpenId = row.GetCellValue(headers, "用户标识"),

                OutTradeNo = row.GetCellValue(headers, "商户订单号"),
                TransactionId = row.GetCellValue(headers, "微信订单号"),
                TradeType = row.GetCellValue(headers, "交易类型"),
                TradeStatus = row.GetCellValue(headers, "交易状态"),
                BillPrice = decimal.Parse(row.GetCellValue(headers, "订单金额", required: true)!),

                OutRefundNo = row.GetCellValue(headers, "商户退款单号"),
                RefundId = row.GetCellValue(headers, "微信退款单号"),
                RefundStatus = row.GetCellValue(headers, "退款状态"),
                RefundType = row.GetCellValue(headers, "退款类型"),
                RefundPrice = decimal.Parse(row.GetCellValue(headers, "退款金额", required: true)!),
            };

            yield return data;
        }
    }

    private static string[] ParseLine(string x) => x.Split(',').Select(d => d.NormalizeWechatBillsField()).ToArray();

    public static async Task<WechatBillFileRecord[]> FromTextAsync(string billPath)
    {
        if (string.IsNullOrWhiteSpace(billPath))
            throw new ArgumentNullException(nameof(billPath));

        var billText = await File.ReadAllTextAsync(billPath);

        var lines = billText.Trim().Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

        var dataList = lines.Select(ParseLine).ToList();

        return ParseBills(dataList).ToArray();
    }

    private static string[] GetCellStringValue(ICell[] cells)
    {
        return cells.Select(x => x.StringCellValue?.NormalizeWechatBillsField() ?? string.Empty).ToArray();
    }

    public static async Task<WechatBillFileRecord[]> FromExcelAsync(string billPath)
    {
        if (string.IsNullOrWhiteSpace(billPath))
            throw new ArgumentNullException(nameof(billPath));

        await using var fs = File.OpenRead(billPath);

        var data = await ExcelHelper.FromExcelAsync(fs);

        var dataList = data.GetOrDefault("ALL")?.Select(GetCellStringValue).ToList();

        if (dataList == null)
            throw new WechatException("sheet not found");

        return ParseBills(dataList).ToArray();
    }
}