import { resolveUrl } from '@/utils/storage';
import { isStringEmpty } from '@/utils/utils';
import { Box } from '@mui/material';

import { VideoBlockProps } from './utils';

export default ({ data }: { data: VideoBlockProps }) => {
  if (!data) {
    return null;
  }

  const getVideoUrl = (): string | undefined | null => {
    if (data.video_url && !isStringEmpty(data.video_url)) {
      return data.video_url;
    }

    return resolveUrl(data.storage);
  };

  const url = getVideoUrl();

  if (!url || isStringEmpty(url)) {
    return <h5>视频不存在</h5>;
  }

  const renderVideoSource = () => {
    if (url.toLowerCase().endsWith('.mp4')) {
      return <source src={url} type="video/mp4" />;
    }
    return undefined;
  };

  const video_source = renderVideoSource();

  if (!video_source) {
    return (
      <>
        <h5>不支持的视频格式</h5>
        <p>{url}</p>
      </>
    );
  }

  return (
    <>
      <Box sx={{}}>
        <video
          poster={resolveUrl(data.poster) || undefined}
          loop={data.loop}
          autoPlay={data.auto_play}
          controls={data.controls}
          width={'100%'}
          height={'auto'}
          preload={'none'}
        >
          {video_source}
          <p>Your browser doesn't support HTML5 video.</p>
        </video>
      </Box>
    </>
  );
};
