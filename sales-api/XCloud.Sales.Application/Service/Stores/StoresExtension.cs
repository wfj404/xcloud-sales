﻿using JetBrains.Annotations;
using XCloud.Application.Gis;
using XCloud.Sales.Application.Domain.Stores;

namespace XCloud.Sales.Application.Service.Stores;

public static class StoresExtension
{
    [NotNull]
    public static GeoLocationDto GetGeoLocation(this Store store)
    {
        return new GeoLocationDto()
        {
            Lon = store.Lon,
            Lat = store.Lat
        };
    }
}