﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Grades;

namespace XCloud.Sales.Application.Mapping.Grades;

public class GradeMap : SalesEntityTypeConfiguration<Grade>
{
    public override void Configure(EntityTypeBuilder<Grade> builder)
    {
        builder.ToTable("sales_grade");
        
        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpSoftDeleteEntity();
        
        builder.Property(u => u.Name).IsRequired().HasMaxLength(100);
        builder.Property(u => u.Description).HasMaxLength(1000);
        builder.Property(u => u.UserCount);
        builder.Property(x => x.Sort);

        base.Configure(builder);
    }
}