﻿namespace XCloud.Sales.Application.Domain.Grades;

public class Grade : SalesBaseEntity<string>, ISoftDelete
{
    public string Name { get; set; }

    public string Description { get; set; }
        
    public int UserCount { get; set; }
        
    public int Sort { get; set; }
        
    public bool IsDeleted { get; set; }
}