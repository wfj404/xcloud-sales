﻿global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Threading.Tasks;
global using Microsoft.AspNetCore.Mvc;
global using XCloud.Sales.Application.Framework;
global using Volo.Abp.Domain.Entities;
global using XCloud.Core.Extension;
global using XCloud.Platform.Application.Service.Permissions;
using Volo.Abp.Modularity;
using XCloud.AspNetMvc.Extension;
using XCloud.Sales.Application;
using XCloud.Sales.Warehouses;

namespace XCloud.Sales.HttpApi;

[DependsOn(
    //sales
    typeof(SalesApplicationModule),
    typeof(SalesWarehouseApplicationModule)
)]
public class SalesHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        context.AddMvcPart<SalesHttpApiModule>();
    }
}