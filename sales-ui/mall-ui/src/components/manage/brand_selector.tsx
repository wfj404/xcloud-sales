import { useRequest } from 'ahooks';
import { Select } from 'antd';
import React, { useEffect } from 'react';

import { apiClient } from '@/utils/client';
import { BrandDto } from '@/utils/swagger';
import { getPinyinShortStyle, isStringEmpty } from '@/utils/utils';

export default ({
  value,
  onChange,
}: {
  value?: string | null;
  onChange?: (e: BrandDto | undefined) => void;
}) => {
  const queryBrand = useRequest(apiClient.mallAdmin.brandListBrand, {
    manual: true,
  });

  const brandList = queryBrand.data?.data?.Data || [];

  useEffect(() => {
    queryBrand.run({});
  }, []);

  return (
    <>
      <Select
        loading={queryBrand.loading}
        value={value || undefined}
        onChange={(e) => {
          onChange && onChange(brandList.find((x) => x.Id == e));
        }}
        placeholder="输入拼音缩写可以搜索..."
        allowClear
        showSearch
        filterOption={(input, option) => {
          let labelStr = option?.label as unknown as string;
          if (isStringEmpty(input) || isStringEmpty(labelStr)) {
            return false;
          }
          return labelStr.toLowerCase().indexOf(input.toLowerCase()) >= 0;
        }}
        options={brandList.map((x, i) => ({
          label: `${x.Name}-${getPinyinShortStyle(x.Name || '') || 'N/A'}`,
          value: x.Id || '',
          key: i,
        }))}
      ></Select>
    </>
  );
};
