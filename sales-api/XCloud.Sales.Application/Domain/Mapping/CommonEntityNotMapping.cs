namespace XCloud.Sales.Application.Domain.Mapping;

public interface IStoreNotMappingSupported : IMappingSupported
{
    bool ExcludeStore { get; set; }
}

public class CommonEntityNotMapping : SalesBaseEntity, IMappingTable
{
    public string EntityId { get; set; }

    public string EntityName { get; set; }

    public string TargetId { get; set; }

    public string TargetName { get; set; }
}