﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Volo.Abp.Application.Dtos;
using XCloud.Core.Dto;
using XCloud.Core.Helper;

namespace XCloud.Application.Model;

public class PagedRequest : IEntityDto
{
    public bool? SkipCalculateTotalCount { get; set; }

    public int Page { get; set; } = 1;

    public int PageSize { get; set; } = 20;

    public PagedResultRequestDto ToAbpPagedRequest()
    {
        ThrowIfException();

        return new PagedResultRequestDto
        {
            SkipCount = PageHelper.GetPagedSkip(Page, PageSize),
            MaxResultCount = PageSize
        };
    }

    public void ThrowIfException(int? maxPageSize = null)
    {
        maxPageSize ??= 1000;

        PageHelper.EnsurePage(Page);
        PageHelper.EnsurePageSize(PageSize);
        PageHelper.EnsureMaxPageSize(PageSize, maxPageSize.Value);
    }

    public static implicit operator PagedResultRequestDto(PagedRequest dto) => dto.ToAbpPagedRequest();
}

public class PagedResponse<T> : ApiResponse<T>, IPagedResult<T>
{
    public PagedResponse()
    {
        //
    }

    public PagedResponse(IEnumerable<T> source, PagedRequest dto, int totalCount) : this(dto.Page, dto.PageSize)
    {
        TotalCount = totalCount;
        Items = source.ToArray();
    }

    private PagedResponse(int page, int pageSize) : this()
    {
        PageHelper.EnsurePage(page);
        PageHelper.EnsurePageSize(pageSize);

        PageIndex = page;
        PageSize = pageSize;
    }

    public int PageSize { get; set; }

    public int PageIndex { get; set; }

    public long TotalCount { get; set; }

    private IReadOnlyList<T> _items;

    [NotNull]
    public IReadOnlyList<T> Items
    {
        get => _items ??= Array.Empty<T>();
        set => _items = value;
    }


    public bool IsNotEmpty => ValidateHelper.IsNotEmptyCollection(Items);

    public bool IsEmpty => !IsNotEmpty;

    public int TotalPages
    {
        get
        {
            if (PageSize <= 0 || TotalCount <= 0)
            {
                return default;
            }

            var totalPages = (int)(TotalCount / PageSize);

            if (TotalCount % PageSize == 0)
            {
                return totalPages;
            }
            else
            {
                return totalPages + 1;
            }
        }
    }
}