﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.ShoppingCart;

namespace XCloud.Sales.Application.Mapping.ShoppingCart;

public class ShoppingCartItemMap : SalesEntityTypeConfiguration<ShoppingCartItem>
{
    public override void Configure(EntityTypeBuilder<ShoppingCartItem> builder)
    {
        builder.ToTable("sales_shopping_cart_item");

        builder.PreConfigIntPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigModificationTimeEntity();

        builder.Property(x => x.UserId);
        builder.Property(x => x.SkuId).RequiredId();
        builder.Property(x => x.Quantity);

        base.Configure(builder);
    }
}