import { apiClient } from '@/utils/client';
import { AdminPermissionList, AdminPermissionWithGroup } from '@/utils/biz';
import { RoleDto } from '@/utils/swagger';
import { Button, message, Modal, Space, Tag } from 'antd';
import { useEffect, useState } from 'react';
import { handleResponse } from '@/utils/utils';
import PermissionForm from '@/components/common/permission_selection_form';

export default ({ model, ok }: { model: RoleDto; ok: () => void }) => {
  const [show, _show] = useState(false);
  const [loading, _loading] = useState(false);
  const [keys, _keys] = useState<string[]>([]);

  useEffect(() => {
    _keys(model.PermissionKeys || []);
  }, [model]);

  const savePermissions = (keys: string[]) => {
    _loading(true);
    apiClient.sys
      .manageRoleSetRolePermissions({
        Id: model.Id,
        PermissionKeys: keys || [],
      })
      .then((res) => {
        handleResponse(res, () => {
          message.success('保存成功');
          _show(false);
          ok && ok();
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  const renderPermissions = () => {
    let keys = model.PermissionKeys || [];
    let permissions = AdminPermissionList.filter((x) => keys.indexOf(x.key) >= 0);
    return (
      <div>
        {permissions.map((x, i) => (
          <Tag
            style={{
              marginRight: 5,
              marginBottom: 5,
            }}
            key={i}
          >
            {x.name}
          </Tag>
        ))}
      </div>
    );
  };

  return (
    <>
      <div>
        <Space direction={'horizontal'}>
          {renderPermissions()}
          <Button
            type={'link'}
            onClick={() => {
              _show(true);
            }}
          >
            修改
          </Button>
        </Space>
        <Modal
          title={'绑定权限'}
          confirmLoading={loading}
          open={show}
          onCancel={() => {
            _show(false);
          }}
          onOk={() => {
            savePermissions(keys);
          }}
        >
          <PermissionForm selectedKeys={keys} onChange={x => _keys(x)} permissions={AdminPermissionWithGroup} />
        </Modal>
      </div>
    </>
  );
};
