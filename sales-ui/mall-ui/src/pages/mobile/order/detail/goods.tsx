import { OrderDto } from '@/utils/swagger';
import { Box, Divider, Typography } from '@mui/material';
import XOrderItemList from '../components/orderItemList';
import FooterItem from '../components/footer_item';
import { formatRelativeTimeFromNow } from '@/utils/dayjs';

export default function(props: { model: OrderDto }) {
  const { model } = props;

  return (
    <>
      <Box sx={{ p: 2, my: 1 }}>
        <Typography variant='h6' gutterBottom>
          商品信息
        </Typography>
        <XOrderItemList model={model} />
        <Divider />
        {(model.CouponPrice || 0) > 0 && (<FooterItem left={'优惠券'}
                                                      right={`-${model.CouponPrice}元`} />)}
        {(model.PromotionPriceOffset || 0) != 0 && (<FooterItem left={'活动优惠'}
                                                                right={`${model.PromotionPriceOffset}元`} />)}

        {(model.ShippingFee || 0) > 0 && (<FooterItem left={'运费'}
                                                      right={`${model.ShippingFee}元`} />)}
        {(model.PackageFee || 0) > 0 && (<FooterItem left={'打包费'}
                                                     right={`${model.PackageFee}元`} />)}
        {(model.SellerPriceOffset || 0) != 0 && (<FooterItem left={'卖家改价'}
                                                             right={`${model.SellerPriceOffset}元`} />)}
        <FooterItem left={model.CreationTime && formatRelativeTimeFromNow(model.CreationTime)}
                    right={`总计：${model.TotalPrice}元`}
                    primary />
      </Box>
    </>
  );
}
