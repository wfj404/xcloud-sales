﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using XCloud.AspNetMvc.Controller;
using XCloud.AspNetMvc.ModelBinder;
using XCloud.Core.Http.Dynamic;

namespace XCloud.Sales.Configuration.Controllers;

public class Post : IEntityDto<string>
{
    public string Id { get; set; }
    public string Title { get; set; }
    public int ReadCount { get; set; }
    public DateTime CreateTimeUtc { get; set; }
}

[DynamicHttpClient("baidu", "api/test")]
public interface IPostService : IApplicationService
{
    [HttpGet("query/{id}/name")]
    Task<string> Query([FromQuery] string name, [FromRoute] string id);

    [HttpPost("put")]
    Task<string> Put([FromBody] Post p);

    [HttpPost("delete")]
    Task<string> Delete([FromForm] string id);

    [HttpPost("void-call")]
    Task Vcall([FromBody] Post p, [InjectCancellationToken] CancellationToken token);

    [HttpPost("post-data")]
    Task<Post> Postdata([FromBody] Post p);
}

[Route("api/gateway/test")]
public class DynamicHttpClientTestController : XCloudBaseController
{
    [HttpPost("body-post")]
    public async Task<Post> post_post([FromServices] ILogger<DynamicHttpClientTestController> logger,
        [FromServices] IPostService postService,
        [InjectCancellationToken] CancellationToken token,
        [FromBody] Post data)
    {
        string response = null;
        response = await postService.Query("xx", "dd");
        logger.LogInformation(response);
        response = await postService.Put(new Post { });
        logger.LogInformation(response);
        response = await postService.Delete("ddss");
        logger.LogInformation(response);

        await postService.Vcall(new Post { }, token);
        await postService.Postdata(new Post { });

        logger.LogInformation(message: this.Clock.Now.ToString());

        return data;
    }
}