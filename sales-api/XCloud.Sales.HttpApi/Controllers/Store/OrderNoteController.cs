﻿using XCloud.Application.Service;
using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Service.Orders;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Store;

[StoreAuditLog]
[Route("api/mall-manager/order-note")]
public class OrderNoteController : ShopBaseController
{
    private readonly IOrderNoteService _orderNoteService;
    private readonly IOrderService _orderService;

    public OrderNoteController(IOrderNoteService orderNoteService, IOrderService orderService)
    {
        _orderNoteService = orderNoteService;
        _orderService = orderService;
    }

    [HttpPost("list-order-notes")]
    public async Task<ApiResponse<OrderNoteDto[]>> QueryOrderNotesAsync([FromBody] QueryOrderNotesInput dto)
    {
        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.OrderId, manager.StoreId);

        var notes = await _orderNoteService.QueryOrderNotesAsync(dto);

        return new ApiResponse<OrderNoteDto[]>(notes);
    }

    [HttpPost("add-order-note")]
    public async Task<ApiResponse<object>> AddOrderNoteAsync([FromBody] OrderNoteDto dto)
    {
        dto.OrderNoteExtended = new OrderNoteExtended()
        {
            NoteType = OrderNoteExtendedType.Message
        };

        var manager = await StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await _orderService.EnsureStoreOrderAsync(dto.OrderId, manager.StoreId);

        await _orderNoteService.InsertAsync(dto);

        return new ApiResponse<object>();
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto<int> dto)
    {
        var manager = await this.StoreManagerAuthService.GetRequiredStoreManagerAsync();

        await this.StoreManagerPermissionService.CheckRequiredPermissionAsync(manager.Id,
            SalesPermissions.ManageOrders);

        var note = await this._orderNoteService.GetRequiredByIdAsync(dto.Id);

        var order = await _orderService.EnsureStoreOrderAsync(note.OrderId, manager.StoreId);

        await this._orderNoteService.DeleteByIdAsync(note.Id);

        return new ApiResponse<object>();
    }
}