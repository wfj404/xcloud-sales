using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Extension;
using XCloud.AspNetMvc.RequestCache;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication;
using XCloud.Platform.Application.Service.Admins;
using XCloud.Sales.Application.Service;
using XCloud.Sales.Qingdao.Service;

namespace XCloud.Sales.Qingdao.Authentication;

[DisableConventionalRegistration]
public class QingdaoAdminAuthService : SalesAppService, IAdminAuthService
{
    private readonly IQingdaoAdminAccountService _qingdaoAdminAccountService;
    private readonly IRequestCacheProvider _cacheProvider;
    private readonly IUserAuthService _userAuthService;

    public QingdaoAdminAuthService(IRequestCacheProvider cacheProvider,
        IUserAuthService userAuthService,
        IQingdaoAdminAccountService qingdaoAdminAccountService)
    {
        _cacheProvider = cacheProvider;
        _userAuthService = userAuthService;
        _qingdaoAdminAccountService = qingdaoAdminAccountService;
    }

    public async Task<AdminAuthResult> GetAuthAdminAsync()
    {
        return await this._cacheProvider.GetOrSetAsync(async () =>
        {
            var response = new AdminAuthResult();

            try
            {
                var userAuthResult = await this._userAuthService.GetAuthUserAsync();

                var user = userAuthResult.User;

                if (userAuthResult.HasError() || user == null)
                {
                    return response.SetError(message: $"user auth error",
                        details: this.JsonDataSerializer.SerializeToString(userAuthResult));
                }

                var wapAdminDto = userAuthResult.GetPropertyFromJson<WapAdminDto>(this.JsonDataSerializer);

                if (wapAdminDto == null)
                    return response.SetError("token is invalid");

                wapAdminDto.organization ??= new WapOrganization();

                var admin = await this._qingdaoAdminAccountService.PrepareAdminAsync(user, wapAdminDto);

                response.SetData(admin);

                return response;
            }
            catch (UserFriendlyException e)
            {
                response.SetError(e.Message);
                return response;
            }
        });
    }
}