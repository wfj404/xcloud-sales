﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Coupons;

namespace XCloud.Sales.Application.Mapping.Coupons;

public class CouponMap : SalesEntityTypeConfiguration<Coupon>
{
    public override void Configure(EntityTypeBuilder<Coupon> builder)
    {
        builder.ToTable("sales_coupon");

        builder.PreConfigAbpCreationTimeEntity();
        builder.PreConfigAbpSoftDeleteEntity();
        builder.PreConfigStringPrimaryKey();

        builder.PreConfigLimitedToStore();

        builder.Property(p => p.Name).IsRequired().HasMaxLength(100);
        builder.Property(p => p.Description).HasMaxLength(500);
        builder.Property(x => x.CouponType);
        builder.Property(p => p.Price).PricePrecision();
        builder.Property(x => x.StartTime);
        builder.Property(x => x.EndTime);
        builder.Property(x => x.ExpiredInDays);
        builder.Property(x => x.OrderConditionRulesJson);

        builder.Property(x => x.Amount);
        builder.Property(x => x.AccountIssuedLimitCount);

        builder.Property(x => x.IsActive);
        builder.Property(x => x.CreationTime);
        builder.Property(x => x.IsDeleted);

        base.Configure(builder);
    }
}