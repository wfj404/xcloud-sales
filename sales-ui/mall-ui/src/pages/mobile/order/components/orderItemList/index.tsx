import { useMemo } from 'react';

import EmptyUrl from '@/assets/empty.svg';
import XImage from '@/components/common/image';
import { getSkuMainPictureUrl, getSkuName } from '@/utils/biz';
import { OrderDto, OrderItemDto } from '@/utils/swagger';
import { Box, Grid, Typography } from '@mui/material';

const OrderGoodsItem = ({ item }: { item: OrderItemDto }) => {
  const url = useMemo(() => {
    return getSkuMainPictureUrl(item.Sku || {}, {
      width: 300,
      height: 300,
    });
  }, [item.Sku]);

  return (
    <>
      <Box
        sx={{
          p: 1,
        }}
      >
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <XImage
              rate={1}
              src={url || EmptyUrl}
              fit={'cover'}
              height={100}
              width={100}
              placeholder={<img src={EmptyUrl} width="100%" alt="" />}
              style={{
                borderRadius: 4,
              }}
              alt={''}
            />
          </Grid>
          <Grid item xs={8}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}
            >
              <Box sx={{}}>
                <Typography variant="body1">
                  {getSkuName(item.Sku || {})}
                </Typography>
                <Typography
                  variant="caption"
                  color="text.disabled"
                  sx={{ display: 'inline' }}
                >
                  {`${item.UnitPrice} x ${item.Quantity} = ${item.TotalPrice}`}
                </Typography>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default function (props: { model: OrderDto }) {
  const { model } = props;

  return (
    <Box sx={{}}>
      {model.Items?.map((x, index) => <OrderGoodsItem key={index} item={x} />)}
    </Box>
  );
}
