using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Catalog;

namespace XCloud.Sales.Application.Mapping.Catalog;

public class ValueAddedItemGroupMap : SalesEntityTypeConfiguration<ValueAddedItemGroup>
{
    public override void Configure(EntityTypeBuilder<ValueAddedItemGroup> builder)
    {
        builder.ToTable("sales_value_added_item_group");

        builder.PreConfigStringPrimaryKey();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.IsRequired);
        builder.Property(x => x.MultipleChoice);
        builder.Property(x => x.GoodsId).RequiredId();

        base.Configure(builder);
    }
}