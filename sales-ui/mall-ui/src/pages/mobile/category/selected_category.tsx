import { useEffect, useState } from 'react';

import XLoading from '@/components/common/loading/dots';
import { apiClient } from '@/utils/client';
import { CategoryDto } from '@/utils/swagger';
import { handleResponse, isArrayEmpty, isStringEmpty } from '@/utils/utils';
import { Box, Button, Typography } from '@mui/material';

export default (props: {
  model: CategoryDto;
  selectedSubCategoryId?: string | null;
  onSubCategorySelect: (d: string | undefined | null) => void;
}) => {
  const { model, onSubCategorySelect, selectedSubCategoryId } = props;
  const [loading, _loading] = useState(false);
  const [data, _data] = useState<CategoryDto[]>([]);

  const querySubCategories = () => {
    if (model && model.Id) {
    } else {
      return;
    }
    _data((x) => []);
    _loading(true);
    apiClient.mall
      .categoryByParent({
        Id: model.Id,
      })
      .then((res) => {
        handleResponse(res, () => {
          _data((x) => res.data.Data || []);
        });
      })
      .finally(() => {
        _loading(false);
      });
  };

  useEffect(() => {
    querySubCategories();
  }, [model]);

  const renderSubCategories = () => {
    if (loading) {
      return <XLoading />;
    }
    if (isArrayEmpty(data)) {
      return null;
    }
    return (
      <Box sx={{}}>
        {data.map((x, index) => (
          <Button
            sx={{ mr: 1, mb: 1, padding: '2px 5px' }}
            key={index}
            variant={selectedSubCategoryId == x.Id ? 'contained' : 'text'}
            color="primary"
            size="small"
            onClick={() => {
              if (x.Id != selectedSubCategoryId) {
                onSubCategorySelect(x.Id);
              } else {
                onSubCategorySelect(undefined);
              }
            }}
          >
            {x.Name || '--'}
          </Button>
        ))}
      </Box>
    );
  };

  return (
    <>
      <Box sx={{ mb: 3, p: 1 }}>
        <Typography variant="h4" gutterBottom component={'div'}>
          {model.Name || '--'}
        </Typography>
        {isStringEmpty(model.Description) || (
          <Typography
            variant="overline"
            gutterBottom
            color="text.disabled"
            component={'div'}
          >
            {model.Description}
          </Typography>
        )}
        {renderSubCategories()}
      </Box>
    </>
  );
};
