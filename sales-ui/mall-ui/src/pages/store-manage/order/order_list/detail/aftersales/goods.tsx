import { Card, Table } from 'antd';
import { ColumnType } from 'antd/es/table';

import { getSkuName } from '@/utils/biz';
import { AfterSalesDto, AfterSalesItemDto } from '@/utils/swagger';

export default (props: { model: AfterSalesDto }) => {
  const { model } = props;

  const columns: ColumnType<AfterSalesItemDto>[] = [
    {
      title: '商品规格',
      render: (x: AfterSalesItemDto) => {
        const item = x.OrderItem || {};
        return `${getSkuName(item.Sku || {})}`;
      },
    },
    {
      title: '购入单价',
      render: (x: AfterSalesItemDto) => {
        return x.OrderItem?.BasePrice || '--';
      },
    },
    {
      title: '售后数量',
      render: (x: AfterSalesItemDto) => x.Quantity,
    },
  ];

  return (
    <>
      <Card title="商品" style={{ marginBottom: 10 }}>
        <Table
          rowKey={(x) => x.Id || ''}
          columns={columns}
          dataSource={model.Items || []}
          pagination={false}
        />
      </Card>
    </>
  );
};
