using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;
using XCloud.Application.Localization;

namespace XCloud.Platform.Application.Localization;

public static class ConfigurationExtension
{
    public static void ConfigPlatformLocalization(this ServiceConfigurationContext context)
    {
        context.Services.Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<PlatformApplicationModule>(
                baseNamespace: typeof(PlatformApplicationModule).Namespace);
        });
        context.Services.Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<PlatformResource>(defaultCultureName: "zh-Hans")
                .AddVirtualJson("/Localization/Resources/Platform")
                .AddBaseTypes(typeof(ApplicationSharedResource));
        });
    }
}