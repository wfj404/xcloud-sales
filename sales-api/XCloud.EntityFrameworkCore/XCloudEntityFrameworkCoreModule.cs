﻿using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Guids;
using Volo.Abp.Modularity;
using XCloud.Core;

namespace XCloud.EntityFrameworkCore;

[DependsOn(
    typeof(XCloudCoreModule),
    typeof(AbpEntityFrameworkCoreModule)
)]
public class XCloudEntityFrameworkCoreModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        this.Configure<AbpSequentialGuidGeneratorOptions>(option =>
        {
            //guid type
            option.DefaultSequentialGuidType = default;
        });
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //
    }
}