﻿using XCloud.Application.Extension;
using XCloud.Application.Model;
using XCloud.Core.Dto;
using XCloud.Platform.Application.Authentication.Extension;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Grades;
using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.HttpApi.Controllers.Admin;

[StoreAuditLog]
[Route("api/mall-admin/sku")]
public class SkuController : ShopBaseController
{
    private readonly IGoodsService _goodsService;
    private readonly ISkuService _skuService;
    private readonly ISpecValueCombinationService _specValueCombinationService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly IGradeGoodsPriceService _goodsPriceService;
    private readonly IGoodsPictureService _goodsPictureService;

    public SkuController(IGoodsService goodsService,
        ISkuService skuService,
        IStoreGoodsMappingService storeGoodsMappingService,
        IGradeGoodsPriceService goodsPriceService,
        IGoodsPictureService goodsPictureService,
        ISpecValueCombinationService specValueCombinationService)
    {
        _goodsService = goodsService;
        _skuService = skuService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _goodsPriceService = goodsPriceService;
        _goodsPictureService = goodsPictureService;
        _specValueCombinationService = specValueCombinationService;
    }

    [HttpPost("query-by-page-order-by-creation-time")]
    public async Task<ApiResponse<SkuDto[]>> QueryByPageOrderByCreationTimeAsync([FromBody] PagedRequest dto)
    {
        dto.PageSize = 100;

        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var skus = await this._skuService.QueryByPageOrderByCreationTimeAsync(dto.Page, dto.PageSize);

        await this._skuService.AttachGoodsAsync(skus);

        return new ApiResponse<SkuDto[]>(skus);
    }

    [HttpPost("update-sku-price")]
    public async Task<ApiResponse<SkuDto>> UpdateSkuPriceAsync([FromBody] SkuDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await this._skuService.UpdatePriceAsync(dto.Id, dto.Price);

        var sku = await this._skuService.GetByIdAsync(dto.Id);

        if (sku != null)
        {
            await this._skuService.AttachGoodsAsync(new[] { sku });
        }

        return new ApiResponse<SkuDto>(sku);
    }

    [HttpPost("query-sku-for-selection")]
    public async Task<ApiResponse<SkuDto[]>> QuerySkuForSelectionAsync([FromBody] QuerySkuForSelectionInput dto)
    {
        dto.Take = 30;

        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var goodsSpecs = await _skuService.ListForSelectionAsync(dto);

        await _skuService.AttachGoodsAsync(goodsSpecs);

        return new ApiResponse<SkuDto[]>(goodsSpecs);
    }

    [HttpPost("get-by-id")]
    public async Task<ApiResponse<SkuDto>> GetByIdAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        var data = await _skuService.GetByIdAsync(dto.Id);

        if (data == null)
            throw new EntityNotFoundException();

        await AttachSkuDetailAsync(new[] { data });

        return new ApiResponse<SkuDto>(data);
    }

    [NonAction]
    private async Task AttachSkuDetailAsync(SkuDto[] skus)
    {
        _specValueCombinationService.AttachSpecItemList(skus);
        var items = skus.SelectMany(x => x.SpecItemList).ToArray();
        await _specValueCombinationService.AttachSpecAndValuesAsync(items);
        await _specValueCombinationService.AttachSpecCombinationErrorsAsync(skus);

        await _goodsPictureService.AttachImagesAsync(skus);

        await _storeGoodsMappingService.AttachActiveStoreMappingsWithStoreAsync(skus);

        await _goodsPriceService.AttachGradePricesAsync(skus);
        var gradePrices = skus.SelectMany(x => x.GradePrices).ToArray();
        await _goodsPriceService.AttachGradesAsync(gradePrices);

        await _skuService.AttachGoodsAsync(skus);
        var goods = skus.Select(x => x.Goods).Where(x => x != null).ToArray();
        await _goodsPictureService.AttachImagesAsync(goods);
    }

    [HttpPost("delete")]
    public async Task<ApiResponse<object>> DeleteAsync([FromBody] IdDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var entity = await _skuService.GetByIdAsync(dto.Id);

        if (entity != null)
        {
            await _skuService.DeleteByIdAsync(dto);

            await SalesEventBusService.NotifyRefreshGoodsInfo(new Goods() { Id = entity.GoodsId });
        }

        return new ApiResponse<object>();
    }

    [HttpPost("save-v1")]
    public virtual async Task<ApiResponse<Sku>> SaveSkusAsync([FromBody] SkuDto dto)
    {
        var storeAdministrator = await AdminAuthService.GetRequiredAuthedAdminAsync();

        await AdminSecurityService.CheckRequiredPermissionAsync(storeAdministrator,
            SalesPermissions.ManageCatalog);

        var goods = await _goodsService.GetByIdAsync(dto.GoodsId);
        if (goods == null)
            throw new EntityNotFoundException(nameof(SaveSkusAsync));

        var res = default(Sku);

        if (!dto.IsEmptyId())
        {
            res = await _skuService.UpdateAsync(dto);
        }
        else
        {
            res = await _skuService.InsertAsync(dto);

            await RequiredCurrentUnitOfWork.SaveChangesAsync();
        }

        if (dto.GradePrices != null)
        {
            await _goodsPriceService.SaveGradePriceAsync(res.Id, dto.GradePrices);
        }

        if (dto.StoreGoodsMapping != null)
        {
            await this._storeGoodsMappingService.SaveGoodsStoreMappingAsync(new SaveGoodsStoreMappingDto()
            {
                SkuId = res.Id,
                Mappings = dto.StoreGoodsMapping
            });
        }

        if (dto.SpecItemList != null)
        {
            await this._specValueCombinationService.SetSpecCombinationAsync(res.Id, dto.SpecItemList);
        }

        await SalesEventBusService.NotifyRefreshGoodsInfo(goods);

        return new ApiResponse<Sku>(res);
    }
}