using Microsoft.EntityFrameworkCore.Metadata.Builders;
using XCloud.Sales.Application.Domain.Shipping;

namespace XCloud.Sales.Application.Mapping.Shipping;

public class FreightTemplateMap : SalesEntityTypeConfiguration<FreightTemplate>
{
    public override void Configure(EntityTypeBuilder<FreightTemplate> builder)
    {
        builder.ToTable("sales_freight_template");

        builder.PreConfigStringPrimaryKey();
        builder.PreConfigAbpCreationTimeEntity();

        builder.Property(x => x.Name).IsRequired().HasMaxLength(100);
        builder.Property(x => x.Description).HasMaxLength(500);
        builder.Property(x => x.StoreId).RequiredId();
        builder.Property(x => x.ShipInHours);
        builder.Property(x => x.FreeShippingMinOrderTotalPrice).PricePrecision();
        builder.Property(x => x.CalculateType);
        builder.Property(x => x.RulesJson);
        builder.Property(x => x.ExcludedAreaJson);
        builder.Property(x => x.Sort);

        base.Configure(builder);
    }
}