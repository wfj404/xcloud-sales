﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SkiaSharp;
using Volo.Abp.DependencyInjection;
using XCloud.Application.Service;
using XCloud.Core.Helper;

namespace XCloud.Application.Images;

public interface IImageProcessingService : IXCloudAppService
{
    Task<byte[]> ResizeToPngAsync(byte[] originBytes, int maxWidth, int maxHeight);
}

[ExposeServices(typeof(IImageProcessingService))]
public class ImageProcessingService : XCloudAppService, IImageProcessingService
{
    /// <summary>
    ///     https://www.javaroad.cn/questions/294295
    ///     //bug,todo
    ///     我在Xamarin.Forms项目中使用SkiaSharp中的SKBitmap.Resize（）方法来调整图像大小以供显示 .
    ///     我遇到的问题是在iOS上拍照时，当照片以肖像拍摄时，图像会以正面朝上显示 .
    ///     在Android上拍照，从Android和iOS设备上的照片库导入可以保持方向，但在iOS上拍照不会 .
    ///     如果我没有使用SkiaSharp调整图像大小（仅显示图像而不调整大小），则图像将以正确的方向显示 .
    ///     然而，这不是解决方案，因为图像需要调整大小 . 以下是我的代码 -
    /// </summary>
    public async Task<byte[]> ResizeToPngAsync(byte[] originBytes, int maxWidth, int maxHeight)
    {
        if (ValidateHelper.IsEmptyCollection(originBytes))
            throw new ArgumentNullException(nameof(originBytes));

        if (maxWidth <= 0 && maxHeight <= 0)
            return originBytes;

        using var img = SKBitmap.Decode(buffer: originBytes);

        if (maxWidth == img.Width && maxHeight == img.Height)
            return originBytes;

        var finalSize = Com.CalculateMaxSize(
            new Size(width: img.Width, height: img.Height),
            maxHeight: maxHeight,
            maxWidth: maxWidth);

        try
        {
            using var resizedBitmap = img.Resize(
                new SKImageInfo(width: finalSize.Width, height: finalSize.Height),
                SKFilterQuality.Medium);

            //In order to exclude saving pictures in low quality at the time of installation, we will set the value of this parameter to 80 (as by default)
            using var data = resizedBitmap.Encode(SKEncodedImageFormat.Png, 80);

            var resizedBytes = data.ToArray();

            return resizedBytes;
        }
        catch (Exception e)
        {
            await Task.CompletedTask;

            Logger.LogWarning(message: e.Message, exception: e);

            return originBytes;
        }
    }
}