﻿using JetBrains.Annotations;
using XCloud.Application.Mapper;
using XCloud.Platform.Application.Exceptions;
using XCloud.Platform.Application.Service.Users;
using XCloud.Sales.Application.Domain.Stores;
using XCloud.Sales.Application.Repository;

namespace XCloud.Sales.Application.Service.Stores;

public interface IStoreManagerService : ISalesAppService
{
    [NotNull]
    [ItemNotNull]
    Task<StoreManagerDto> EnsureStoreManagerAsync([NotNull] string managerId, [NotNull] string storeId);

    Task<StoreManagerDto[]> ListActiveManagersByUserIdAsync(string userId);

    Task<StoreManagerDto[]> AttachUsersAsync(StoreManagerDto[] data);

    Task<StoreManagerDto[]> AttachStoresAsync(StoreManagerDto[] data);

    Task<StoreManagerDto[]> QueryByStoreIdAsync(string storeId);

    Task<StoreManagerDto> GetByIdAsync(string id);
}

public class StoreManagerService : SalesAppService,
    IStoreManagerService
{
    private readonly ISalesRepository<StoreManager> _storeManagerRepository;
    private readonly IUserMobileService _userMobileService;
    private readonly IUserService _userService;

    public StoreManagerService(ISalesRepository<StoreManager> storeManagerRepository,
        IUserMobileService userMobileService,
        IUserService userService)
    {
        _storeManagerRepository = storeManagerRepository;
        _userMobileService = userMobileService;
        _userService = userService;
    }

    public async Task<StoreManagerDto> GetByIdAsync(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentNullException(nameof(id));

        var entity = await this._storeManagerRepository.FirstOrDefaultAsync(x => x.Id == id);

        return entity?.MapTo<StoreManager, StoreManagerDto>(this.ObjectMapper);
    }

    public async Task<StoreManagerDto> EnsureStoreManagerAsync(string managerId, string storeId)
    {
        if (string.IsNullOrWhiteSpace(storeId))
            throw new ArgumentNullException(nameof(storeId));

        var manager = await this.GetByIdAsync(managerId);

        if (manager == null)
            throw new EntityNotFoundException(nameof(EnsureStoreManagerAsync));

        if (manager.StoreId != storeId)
            throw new PermissionRequiredException("wrong store manager");

        return manager;
    }

    public async Task<StoreManagerDto[]> ListActiveManagersByUserIdAsync(string userId)
    {
        if (string.IsNullOrWhiteSpace(userId))
            throw new ArgumentNullException(nameof(userId));

        var managers = await _storeManagerRepository.QueryManyAsync(x => x.UserId == userId && x.IsActive);

        return managers.MapArrayTo<StoreManager, StoreManagerDto>(ObjectMapper);
    }

    public async Task<StoreManagerDto[]> AttachStoresAsync(StoreManagerDto[] data)
    {
        if (!data.Any())
            return data;

        var db = await _storeManagerRepository.GetDbContextAsync();

        var storeIds = data.Select(x => x.StoreId).WhereNotEmpty().Distinct().ToArray();

        var stores = await db.Set<Store>().AsNoTracking().WhereIdIn(storeIds).ToArrayAsync();

        foreach (var m in data)
        {
            var store = stores.FirstOrDefault(x => x.Id == m.StoreId);
            if (store == null)
                continue;

            m.Store = store.MapTo<Store, StoreDto>(ObjectMapper);
        }

        return data;
    }

    public async Task<StoreManagerDto[]> AttachUsersAsync(StoreManagerDto[] data)
    {
        if (!data.Any())
            return data;

        var userIds = data.Select(x => x.UserId).WhereNotEmpty().Distinct().ToArray();

        var users = await _userService.GetByIdsAsync(userIds);

        await _userMobileService.AttachAccountMobileAsync(users);

        foreach (var m in data)
        {
            m.User = users.FirstOrDefault(x => x.Id == m.UserId);
        }

        return data;
    }

    public async Task<StoreManagerDto[]> QueryByStoreIdAsync(string storeId)
    {
        var db = await _storeManagerRepository.GetDbContextAsync();

        var data = await db.Set<StoreManager>().AsNoTracking()
            .Where(x => x.StoreId == storeId)
            .OrderByDescending(x => x.CreationTime)
            .TakeUpTo5000()
            .ToArrayAsync();

        return data.MapArrayTo<StoreManager, StoreManagerDto>(ObjectMapper);
    }
}