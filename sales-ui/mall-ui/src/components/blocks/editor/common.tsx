import { Form, Switch } from 'antd';
import { BlockItem, BlockItemProps } from '@/components/blocks/utils';

export default ({ item, onChange }: {
  item: BlockItem<BlockItemProps>,
  onChange: (x: BlockItem<BlockItemProps>) => void
}) => {

  if (!item || !onChange) {
    return null;
  }

  return <>
    <Form labelCol={{
      span: 3,
    }}>
      <Form.Item label={'隐藏组件'}>
        <Switch checked={item.hide} onChange={e => {
          onChange({ ...item, hide: e });
        }} />
      </Form.Item>
    </Form>
  </>;
};
