import { ImageViewer } from 'antd-mobile';
import { useState } from 'react';

import { isStringEmpty } from '@/utils/utils';

import XImage, { XImageProps } from './image';

export interface XImagePreviewProps extends XImageProps {
  previewUrl?: string;
}

export default (props: XImagePreviewProps) => {
  const { previewUrl } = props;

  const [preview, _preview] = useState<boolean>(false);

  return (
    <>
      <div
        style={{
          display: 'block',
          width: '100%',
        }}
        onClick={() => {
          _preview(true);
        }}
      >
        <ImageViewer.Multi
          visible={preview && !isStringEmpty(previewUrl)}
          images={[previewUrl || '']}
          onClose={() => {
            _preview(false);
          }}
        />
        <XImage {...props} />
      </div>
    </>
  );
};
