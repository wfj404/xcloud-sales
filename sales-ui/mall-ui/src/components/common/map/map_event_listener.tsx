import L from 'leaflet';
import { useMapEvents } from 'react-leaflet';

export default ({ events }: { events: L.LeafletEventHandlerFnMap }) => {
  const map = useMapEvents({
    ...events,
  });
  return <></>;
};
