namespace XCloud.Application.Service;

public static class CrudHelper
{
    public static bool IsEmptyString(string str)
    {
        return string.IsNullOrWhiteSpace(str);
    }

    public static bool IsEmptyInt(int i)
    {
        return i <= 0;
    }

    public static bool IsEmptyLong(long i)
    {
        return i <= 0;
    }
}