﻿using XCloud.Sales.Application.Service.Stores;

namespace XCloud.Sales.Application.Domain.Stores;

public class StoreManager : SalesBaseEntity<string>, IHasCreationTime
{
    public string StoreId { get; set; }

    public string UserId { get; set; }

    public int Role { get; set; } = StoreManagerRoles.StoreManager;

    public bool IsActive { get; set; }

    public bool IsSuperManager { get; set; }

    public DateTime CreationTime { get; set; }

    public static implicit operator string(StoreManager manager) => manager?.Id;
}