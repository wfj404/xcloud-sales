import { QueryActivityLogPagingInput } from '@/utils/swagger';
import { SearchOutlined } from '@ant-design/icons';
import { Button, Card, Col, DatePicker, Form, Row, Select } from 'antd';
import { useEffect, useState } from 'react';
import utils from './utils';
import { dateTimeFormat, parseAsDayjs, timezoneOffset } from '@/utils/dayjs';

export default (props: {
  query: QueryActivityLogPagingInput;
  onSearch: (d: QueryActivityLogPagingInput) => void;
}) => {
  const { query, onSearch } = props;

  const [form, _form] = useState<QueryActivityLogPagingInput>({});

  const triggerSearch = (e: QueryActivityLogPagingInput) => {
    e.Page = 1;
    onSearch && onSearch(e);
  };

  useEffect(() => {
    _form(query);
  }, []);

  return (
    <Card bordered={false} style={{ marginBottom: 10 }}>
      <Form>
        <Row gutter={10}>
          <Col span={6}>
            <Form.Item label='时间'>
              <DatePicker.RangePicker
                value={[
                  parseAsDayjs(form.StartTime)?.add(timezoneOffset, 'hour') || null,
                  parseAsDayjs(form.EndTime)?.add(timezoneOffset, 'hour') || null,
                ]}
                onChange={e => {
                  _form(x => ({
                    ...x,
                    StartTime: e?.at(0)?.add(-timezoneOffset, 'hour').format(dateTimeFormat),
                    EndTime: e?.at(1)?.add(-timezoneOffset, 'hour').format(dateTimeFormat),
                  }));
                }}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item label='日志类型'>
              <Select
                placeholder={'日志类型'}
                allowClear
                options={utils.ActivityLogTypesDescription.map(x => ({
                  value: x.id,
                  label: x.name,
                }))}
                value={form.ActivityLogTypeId}
                onChange={e => {
                  _form(x => ({
                    ...x,
                    ActivityLogTypeId: e,
                  }));
                }}
                onClear={() => {
                  _form(x => ({
                    ...x,
                    ActivityLogTypeId: undefined,
                  }));
                }}
              ></Select>
            </Form.Item>
          </Col>
          <Col span={6}>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button type='primary'
                      htmlType='submit'
                      icon={<SearchOutlined />}
                      onClick={() => triggerSearch(form)}>
                搜索
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
