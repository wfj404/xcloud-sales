﻿using XCloud.Core.Dto;
using XCloud.Sales.Application.Authentication;
using XCloud.Sales.Application.Extension;
using XCloud.Sales.Application.Service.Catalog;
using XCloud.Sales.Application.Service.Stores;
using XCloud.Sales.Application.Service.Stores.Context;
using XCloud.Sales.Application.Service.Users;
using XCloud.Sales.Application.View;

namespace XCloud.Sales.HttpApi.Controllers.User;

[Route("api/mall/sku")]
public class SkuController : ShopBaseController
{
    private readonly SkuViewService _skuViewService;
    private readonly IStoreGoodsMappingService _storeGoodsMappingService;
    private readonly IPriceCalculationService _priceCalculationService;
    private readonly ISpecValueCombinationService _specValueCombinationService;

    public SkuController(SkuViewService skuViewService,
        IStoreGoodsMappingService storeGoodsMappingService,
        IPriceCalculationService priceCalculationService,
        ISpecValueCombinationService specValueCombinationService)
    {
        _skuViewService = skuViewService;
        _storeGoodsMappingService = storeGoodsMappingService;
        _priceCalculationService = priceCalculationService;
        _specValueCombinationService = specValueCombinationService;
    }

    [HttpPost("spec-menu-group")]
    public async Task<ApiResponse<SpecMenuGroupResponse>> GetSpecMenuGroupAsync(
        [FromBody] QuerySkuBySelectedItemsDto dto)
    {
        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        dto.StoreId = storeId;

        var response = await this._skuViewService.GetSpecMenuGroupAsync(dto);

        var skus = new List<SkuDto>()
            .AddManyItems(response.FilteredSkuList)
            .WhereNotNull()
            .ToArray();

        var loginUser = await StoreUserAuthService.GetStoreUserOrNullAsync();

        await this.HandleSkusAsync(skus, storeId, loginUser);

        return new ApiResponse<SpecMenuGroupResponse>(response);
    }

    [NonAction]
    private async Task<SkuDto[]> HandleSkusAsync(SkuDto[] skus, string storeId, StoreUserDto loginUser = default)
    {
        var specItems = skus.Select(x => x.SpecItemList).WhereNotNull()
            .SelectMany(x => x).ToArray();

        await this._specValueCombinationService.AttachSpecAndValuesAsync(specItems);

        var settings = await SettingService.GetCachedMallSettingsAsync();

        if (loginUser != null || settings.GoodsSettings.DisplayPriceForGuest)
        {
            await _priceCalculationService.AttachPriceModelAsync(skus, storeId, loginUser?.Id);
        }
        else
        {
            skus.HidePrice();
        }

        await _storeGoodsMappingService.AttachStoreQuantityAsync(skus, storeId);

        skus = skus.Where(x => x.StockModel != null).ToArray();

        return skus;
    }

    [Obsolete]
    [HttpPost("get-sku-by-goods")]
    public async Task<ApiResponse<SkuDto[]>> GetSkusByGoodsAsync([FromBody] IdDto dto)
    {
        var storeId = await CurrentStoreContext.GetRequiredStoreIdAsync();

        var skus = await this._skuViewService.GetActiveSkusByGoodsIdAsync(dto.Id);

        if (!skus.Any())
        {
            return new ApiResponse<SkuDto[]>();
        }

        var loginUser = await StoreUserAuthService.GetStoreUserOrNullAsync();

        skus = await this.HandleSkusAsync(skus, storeId, loginUser);

        return new ApiResponse<SkuDto[]>(skus);
    }
}