﻿using FluentMigrator;
using XCloud.Platform.Application.Domain.Common;
using XCloud.Sales.Application.Domain.Catalog;
using XCloud.Sales.Application.Domain.Common;
using XCloud.Sales.Application.Domain.Payments;

namespace XCloud.Sales.Application.Extended.DatabaseMigrator;

[Migration(1, description: nameof(DatabaseMigrations))]
public class DatabaseMigrations : AutoReversingMigration
{
    public override void Up()
    {
        //
    }
}

[Migration(2, description: nameof(SpecValueAddAssociatedSkuCount))]
public class SpecValueAddAssociatedSkuCount : AutoReversingMigration
{
    public override void Up()
    {
        Create.Column(nameof(SpecValue.AssociatedSkuCount))
            .OnTable("sales_spec_value")
            .AsInt32()
            .NotNullable()
            .WithDefaultValue(0)
            .WithColumnDescription("物料数量");
    }
}

[Migration(3, description: nameof(AddKeyStoreTable))]
public class AddKeyStoreTable : AutoReversingMigration
{
    public override void Up()
    {
        Create.Table("sys_key_store")
            .WithColumn(nameof(KeyStore.Id)).AsString(100).NotNullable().PrimaryKey()
            .WithColumn(nameof(KeyStore.Key)).AsString(200).NotNullable()
            .WithColumn(nameof(KeyStore.CreationTime)).AsDateTime().NotNullable();
    }
}

[Migration(4, description: nameof(ModifyOrderAfterSales))]
public class ModifyOrderAfterSales : Migration
{
    public override void Up()
    {
        this.Delete.Column("IsAfterSales").FromTable("sales_order");
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(5, description: nameof(BillModify))]
public class BillModify : Migration
{
    public override void Up()
    {
        this.Delete.Column("OrderId").Column("Approved").Column("ApprovedTime").Column("LastModificationTime")
            .FromTable("sales_refund_bill");

        this.Create.Column(nameof(RefundBill.Description)).OnTable("sales_refund_bill").AsString(size: 500).Nullable();

        this.Create.Column(nameof(PaymentBill.Description)).OnTable("sales_payment_bill").AsString(size: 500)
            .Nullable();
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(6, description: nameof(CreateAreaTable))]
public class CreateAreaTable : Migration
{
    public override void Up()
    {
        Create.Table("sys_area")
            .WithColumn(nameof(Area.Id)).AsString(100).NotNullable().PrimaryKey()
            .WithColumn(nameof(Area.Name)).AsString(100).NotNullable()
            .WithColumn(nameof(Area.MetaId)).AsString(100).Nullable()
            .WithColumn(nameof(Area.ParentId)).AsString(100).NotNullable()
            .WithColumn(nameof(Area.CreationTime)).AsDateTime().NotNullable();
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(8, description: nameof(GoodsAddCountryArea))]
public class GoodsAddCountryArea : Migration
{
    public override void Up()
    {
        Alter.Table("sales_goods")
            .AddColumn(nameof(Goods.AreaId)).AsString(100).Nullable().WithColumnDescription("原产地");
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(9, description: nameof(AlterAreaTable))]
public class AlterAreaTable : Migration
{
    public override void Up()
    {
        Alter.Table("sys_area")
            .AddColumn(nameof(Area.NodePathJson)).AsCustom("text").Nullable();
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(10, description: nameof(DropTreeGroupKey))]
public class DropTreeGroupKey : Migration
{
    public override void Up()
    {
        Delete.Column("TreeGroupKey").FromTable("sys_dept");
        Delete.Column("TreeGroupKey").FromTable("sys_area");

        Rename.Column("ParentCategoryId").OnTable("sales_category").To("ParentId");
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(11, description: nameof(AddAreaType))]
public class AddAreaType : Migration
{
    public override void Up()
    {
        Alter.Table("sys_area").AddColumn(nameof(Area.AreaType)).AsString(100).Nullable();
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}

[Migration(12, description: nameof(AddOrderGroupTable))]
public class AddOrderGroupTable : Migration
{
    public override void Up()
    {
        Create.Table("sales_order_group")
            .WithColumn(nameof(OrderGroup.Id)).AsString(100).NotNullable().PrimaryKey()
            .WithColumn(nameof(OrderGroup.StoreId)).AsString(100).NotNullable()
            .WithColumn(nameof(OrderGroup.Name)).AsString(100).NotNullable()
            .WithColumn(nameof(OrderGroup.Description)).AsString(500).Nullable()
            .WithColumn(nameof(OrderGroup.OrderDataJson)).AsCustom("text").Nullable()
            .WithColumn(nameof(OrderGroup.CreationTime)).AsDateTime().NotNullable();
    }

    public override void Down()
    {
        throw new System.NotImplementedException();
    }
}