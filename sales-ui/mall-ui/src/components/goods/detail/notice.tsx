import { Alert, Box } from '@mui/material';
import { useSnapshot } from 'umi';
import { storeState, StoreType } from '@/store';
import { isStringEmpty } from '@/utils/utils';

export default () => {
  const app = useSnapshot(storeState) as StoreType;

  if (isStringEmpty(app.mallSettings.GoodsDetailNotice)) {
    return null;
  }

  return (
    <>
      <Box sx={{ mb: 3 }}>
        <Alert>{app.mallSettings.GoodsDetailNotice}</Alert>
      </Box>
    </>
  );
};
