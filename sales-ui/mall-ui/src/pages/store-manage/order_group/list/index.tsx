import { useRequest } from 'ahooks';
import { Button, Card, message, Space, Table } from 'antd';
import { ColumnProps } from 'antd/es/table';
import qs from 'querystring';
import { useEffect, useState } from 'react';
import { Link } from 'umi';

import XDeleteButton from '@/components/common/delete_button';
import XTime from '@/components/common/time';
import { apiClient } from '@/utils/client';
import { dateFormat, myDayjs, timezoneOffset } from '@/utils/dayjs';
import { OrderGroupDto, QueryOrderGroupPagingInput } from '@/utils/swagger';
import { handleResponse } from '@/utils/utils';

import XForm from './form';

export default () => {
  const queryRequest = useRequest(apiClient.mallManager.orderGroupQueryPaging, {
    manual: true,
  });

  const loading = queryRequest.loading;
  const data = queryRequest.data?.data || {};

  const [query, _query] = useState<QueryOrderGroupPagingInput>({
    Page: 1,
  });

  const [formData, _formData] = useState<OrderGroupDto | undefined>(undefined);

  const queryList = () => {
    queryRequest.run({ ...query });
  };

  const columns: ColumnProps<OrderGroupDto>[] = [
    {
      title: '名称',
      render: (d, x) => {
        return (
          <Link
            to={{
              pathname: '/store-manage/order-group-detail',
              search: qs.stringify({ id: x.Id }),
            }}
            target="_blank"
          >
            {x.Name}
          </Link>
        );
      },
    },
    {
      title: '描述',
      render: (d, x) => x.Description || '--',
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {
        return (
          <>
            <Space direction="horizontal">
              <XDeleteButton
                action={async () => {
                  const res = await apiClient.mallManager.orderGroupDelete({
                    Id: x?.Id,
                  });
                  handleResponse(res, () => {
                    message.success('删除成功');
                    queryList();
                  });
                }}
              />
            </Space>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    queryList();
  }, [query]);

  return (
    <>
      <XForm
        show={formData != undefined}
        hide={() => _formData(undefined)}
        data={formData || {}}
        ok={() => {
          _formData(undefined);
          queryList();
        }}
      />
      <Card
        title="订单批次"
        extra={
          <Button
            type="primary"
            onClick={() => {
              _formData({
                Name: `订单批次-${myDayjs.utc().add(timezoneOffset, 'hour').format(dateFormat)}`,
                Description: undefined,
              });
            }}
          >
            新增
          </Button>
        }
      >
        <Table
          rowKey={(x) => x.Id || ''}
          loading={loading}
          columns={columns}
          dataSource={data.Items || []}
          pagination={{
            showSizeChanger: false,
            pageSize: 20,
            current: query.Page,
            total: data.TotalCount,
            onChange: (e) => {
              _query({
                ...query,
                Page: e,
              });
            },
          }}
        />
      </Card>
    </>
  );
};
