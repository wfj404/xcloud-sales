import XTime from '@/components/common/time';
import { apiClient } from '@/utils/client';
import { PagedResponse } from '@/utils/biz';
import { OrderReviewDto, QueryOrderReviewPaging } from '@/utils/swagger';
import { Badge, Button, message, Modal, Rate, Tooltip } from 'antd';
import { useEffect, useState } from 'react';
import SearchForm from './search_form';
import ReviewItem from './items';
import { ProColumns, ProTable } from '@ant-design/pro-components';
import PreviewGroup from '@/components/common/preview_group';
import { handleResponse } from '@/utils/utils';
import OrderDetail from '../order_list/detail';

export default () => {
  const [loading, _loading] = useState(true);
  const [data, _data] = useState<PagedResponse<OrderReviewDto>>({});
  const [query, _query] = useState<QueryOrderReviewPaging>({});

  const [loadingId, _loadingId] = useState<string | undefined | null>(undefined);

  const [previewOrderId, _previewOrderId] = useState<string | undefined>(undefined);

  const approveReview = (d: OrderReviewDto) => {
    if (!confirm('确定审批?')) {
      return;
    }
    _loadingId(d.Id);
    apiClient.mallManager.orderReviewApproveReview({ Id: d.Id }).then(res => {
      handleResponse(res, () => {
        message.success('审批成功');
        queryList(query);
      });
    }).finally(() => _loadingId(undefined));
  };

  const queryList = (q: QueryOrderReviewPaging) => {
    _loading(true);
    apiClient.mallManager
      .orderReviewQueryOrderReviewPaging(q)
      .then((res) => {
        _data(res.data || {});
      })
      .finally(() => {
        _loading(false);
      });
  };

  const columns: ProColumns<OrderReviewDto>[] = [
    {
      title: '',
      render: (d, x) => {
        return <>
          <div>
            <Button
              type='link'
              onClick={() => {
                _previewOrderId(x.OrderId || '');
              }}
            >
              查看相关订单
            </Button>
          </div>
        </>;
      },
    },
    {
      title: '评论',
      render: (d, x) => {
        return <>
          <div>{x.ReviewText || '--'}</div>
        </>;
      },
    },
    {
      title: '图片',
      render: (d, x) => {

        if (x.StorageMeta == null || x.StorageMeta.length <= 0) {
          return null;
        }

        return <PreviewGroup data={x.StorageMeta} count={10} />;
      },
    },
    {
      title: '评分',
      render: (d, x) => <Rate value={x.Rating || 0} allowHalf disabled />,
    },
    {
      title: '时间',
      render: (d, x) => <XTime model={x} />,
    },
    {
      title: '操作',
      width: 200,
      render: (d, x) => {

        if (x.Approved) {
          return <Badge text={'已审批'} color={'green'} />;
        }

        return (
          <Button.Group>
            <Tooltip title={'批准后可以公开展示'}>
              <Button
                loading={loadingId == x.Id}
                type='link'
                onClick={() => {
                  approveReview(x);
                }}
              >
                批准此评论
              </Button>
            </Tooltip>
          </Button.Group>
        );
      },
    },
  ];

  useEffect(() => {
    queryList({
      ...query,
      Page: 1,
    });
  }, []);

  return (
    <>
      <Modal title={'订单'}
             open={previewOrderId != undefined}
             footer={false}
             width={'95%'}
             onCancel={() => _previewOrderId(undefined)}>
        {previewOrderId && <OrderDetail detailId={previewOrderId} />}
      </Modal>
      <SearchForm query={query} onSearch={(q) => {
        _query(q);
        queryList(q);
      }} />
      <ProTable
        search={false}
        headerTitle={'订单评论'}
        options={false}
        //toolBarRender={false}
        rowKey={(x) => x.Id || ''}
        loading={loading}
        columns={columns}
        dataSource={data.Items || []}
        expandable={{
          expandedRowRender: x => <ReviewItem review={x || {}} />,
        }}
        pagination={{
          total: data.TotalCount,
          pageSize: data.PageSize,
          onChange: (e) => {
            let p = {
              ...query,
              Page: e,
            };
            _query(p);
            queryList(p);
          },
        }}
      />
    </>
  );
};
